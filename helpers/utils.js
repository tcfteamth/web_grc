import axios from 'axios';
import { config } from '../utils';

const axiosInstance = axios.create();

const apiGatewayInstance = axios.create({
  baseURL: config.apiGatewayBaseUrl,
});

export { axiosInstance, apiGatewayInstance };
