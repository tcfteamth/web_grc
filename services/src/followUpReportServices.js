export default class FollowUpReportServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getFollowUpReportGraph(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    startDueDate = '',
    endDueDate = '',
    startCloseDate = '',
    endCloseDate = '',
    status = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/report/graph?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&startDueDate=${startDueDate}&endDueDate=${endDueDate}&startCloseDate=${startCloseDate}&endCloseDate=${endCloseDate}&status=${status}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getFollowUpReportSummary(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    startDueDate = '',
    endDueDate = '',
    startCloseDate = '',
    endCloseDate = '',
    status = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/report/summary?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&startDueDate=${startDueDate}&endDueDate=${endDueDate}&startCloseDate=${startCloseDate}&endCloseDate=${endCloseDate}&status=${status}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async exportFollowUpReport(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    status = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/report/export?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&status=${status}`,
    );
  }

  async getFollowUpReportStatusList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/report/filter/status-list`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
