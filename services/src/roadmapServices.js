export default class RoadmapServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getDepartmentList(bu = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments?bu=${bu}`,
    );
  }

  async getDepartmentOldList(bu,year) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/old?bu=${bu}&year=${year}`,
    );
  }
  
  async getDepartmentByBu(bu) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/?bu=${bu}`,
    );
  }
  async getDivisionById(departmentId) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/divisions/${departmentId}`,
    );
  }
  async getDivisionOldByNoAndYear(departmentNo,year) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/divisionsOld?departmentNo=${departmentNo}&year=${year}`,
    );
  }
  
  async getShiftByDepartmentId(departmentId) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/getShiftByDepartmentId/${departmentId}`,
    );
  }
  async getShiftOldByDepartmentNo(departmentNo,year) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/shiftOld?departmentNo=${departmentNo}&year=${year}`,
    );
  }
  async getDivisionByDivisionNo(divisionNo) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/getDivisionByDivisionNo/${divisionNo}`,
    );
  }
  async getShiftByShiftNo(shiftNo) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/getShiftByShiftNo/${shiftNo}`,
    );
  }
  
  // get list lv2
  async getLvlTwoList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-db-masters/getAllLvl2`,
    );
  }

  async getLvlThreeList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/lv3List`,
    );
  }

  async getLvlFourList(lvlThreeNo) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/lv4List/${lvlThreeNo}`,
    );
  }

  async getLvlList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/listLv3AndLv4`,
    );
  }

  async submitCreateRoadmap(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/roadmaps`,
      formData,
    );
  }

  async updateRoadmap(token, formData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;
    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/roadmaps`,
      formData,
    );
  }

  // delete roadmap
  async submitDeleteRoadmap(id) {
    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/roadmaps/${id}`,
    );
  }
  async getUserList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/users?page=1&size=100000${''
      }`,
    );
  }
  async getRoadmapList(
    token,
    page = 1,
    searchRoadmap = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    roadmapType = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    year = '',
    processAnd = '',
    processOr = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    // let roadmapText;

    // if (searchRoadmap) {
    //   roadmapText = searchRoadmap;
    // } else {
    //   roadmapText = '';
    // }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roadmaps?page=${page}&size=50&search=${searchRoadmap}&lv3=${lvl3}&lv4=${lvl4}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&roadmapType=${roadmapType}&status=${status}&year=${year}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}`,
    );
  }

  // get edit roadmap
  async getEditRoadmapById(id) {
    return this.axiosInstance.get(`${this.endpoint}/api/manage/roadmaps/${id}`);
  }

  async getAssesor(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/users/UsersByDivisionId`,
    );
  }

  async submitRoadmaps(token, roadmapIdsData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    const data = {
      roadmapIds: roadmapIdsData,
    };

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/users/submitRoadmaps`,
      data,
    );
  }

  async acceptRoadmaps(token, acceptRoadmapsData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    const data = {
      roadmaps: acceptRoadmapsData,
    };

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/users/acceptRoadmaps`,
      data,
    );
  }

  async submitRequest(token, requestData) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/roadmaps/request/`,
      requestData,
    );
  }

  async submitReadRequest(token, requestData) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/roadmaps/request/requests`,
      requestData,
    );
  }

  async getRequestList(
    token,
    page = 1,
    searchRequestValue = '',
    roadmapType = '',
    indicator = '',
    year = '',
    isRead = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    const searchValue = searchRequestValue
      ? `&search=${searchRequestValue}`
      : '';

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roadmaps/request/requests?size=10&page=${page}${searchValue}&indicator=${indicator}&roadmapType=${roadmapType}&year=${year}&isRead=${isRead}`,
    );
  }

  async getBUList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/bu-list`,
    );
  }

  async getBUOldList(token,year) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/buOld-list?year=${year}`,
    );
  }

  async getRoadmapTypeList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/roadmap-type-list`,
    );
  }

  async getOwnerTaskList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/owner-task-list`,
    );
  }

  async getRateList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(`${this.endpoint}/api/manage/rates`);
  }

  async getAllStatus(token, tab = 'TODO', type = 'ROADMAP') {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/status-list?tab=${tab}&type=${type}`,
    );
  }

  async getDivisionWithoutId(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/divisions/filter-divisions`,
    );
  }

  async getShiftWithoutId(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/getShiftByDepartmentId`,
    );
  }
  
  async getYearList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/dashboard/year-list`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }

  async exportRoadmap(
    token,
    page = 1,
    searchRoadmap = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    roadmapType = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    year = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roadmaps/export?search=${searchRoadmap}&lv3=${lvl3}&lv4=${lvl4}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&roadmapType=${roadmapType}&status=${status}&year=${year}`,
    );
  }
}
