export default class DatabaseCosoServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getCosoDBList(searchProcessDBText) {
    let searchText;
    console.log('searchProcessDBText', searchProcessDBText);
    if (searchProcessDBText) {
      searchText = searchProcessDBText;
    } else {
      searchText = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-db-masters?${
        searchText && `search=${searchText}`
      }`,
    );
  }

  // import excel
  async importExcelDatabase(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/coso-db-masters/import`,
      formData,
    );
  }

  // export excel
  async exportExcelDatabase(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-db-masters/export`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
