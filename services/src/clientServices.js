export default class ClientService {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getProcessDBList(page) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters?page=${
        page || 1
      }&size=10`,
    );
  }

  async getRoleList(page) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles?page=${page || 1}&size=10`,
    );
  }

  async getRoleListById(roleType, id, position) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/${roleType}?${
        roleType === 'SPECIAL'
          ? `roleId=${id}`
          : `positionLevel=${position || 0}`
      }`,
    );
  }

  async getAuthoritiesList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/authorities`,
    );
  }

  async getRoleType(type) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/roleTypes?type=${type}`,
    );
  }

  async getRoleSpacial() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/roleSpecials`,
    );
  }

  async getPositionList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/positionLevels`,
    );
  }

  async getRoleSystemList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/roleSystems`,
    );
  }

  // TODO
  async getUser(roleId) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/roles/users?page=1&size=100000${
        roleId != null ? `&roleId=${roleId}` : ''
      }`,
    );
  }

  async createNewRole(data) {
    return this.axiosInstance.post(`${this.endpoint}/api/manage/roles`, data);
  }

  async editRole(data) {
    return this.axiosInstance.put(`${this.endpoint}/api/manage/roles`, data);
  }

  async getAlert(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/toDoList/list`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
