import { Tab } from 'semantic-ui-react';

export default class ReAssessmentServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getReAssessmentList(
    token,
    page = 1,
    searchAssessmentText = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/reassessments/list?page=${page}&size=50&search=${searchAssessmentText}&lv3=${lvl3}&lv4=${lvl4}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}`,
    );
  }


  async createReAssessment(token, assessmentData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/reassessments/createReAssessment`,
      assessmentData,
    );
  } 
}
