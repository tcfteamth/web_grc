import { config, axiosInstance } from '../../utils';
import ClientServices from './clientServices';
import AuthenServices from './authenServices';
import ProcessDBServices from './processDBServices';
import RoadmapServices from './roadmapServices';
import AssessmentServices from './assessmentServices';
import ReAssessmentServices from './reassessmentServices';
import DatabaseCosoServices from './databaseCosoServices';
import RoadmapCosoServices from './roadmapCosoServices';
import AssessmentCosoServices from './assessmentCosoServices';
import DashboardServices from './dashboardServices';
import ReportServices from './reportServices';
import FollowUpServices from './followUpServices';
import SettingTaskServices from './settingtaskServices';
import DashboardFollowUpServices from './dashboardFollowUpService';
import FollowUpReportServices from './followUpReportServices';

const endpoint = config.apiGatewayBaseUrl;

class Services {
  constructor(baseUrl) {
    this.axiosInstance = axiosInstance;

    this.axiosInstance.defaults.headers.post['Content-Type'] =
      'application/json; charset=UTF-8';
    this.axiosInstance.defaults.headers.get['Content-Type'] =
      'application/json; charset=UTF-8';
    this.axiosInstance.defaults.headers.put['Content-Type'] =
      'application/json; charset=UTF-8';
    this.axiosInstance.defaults.headers.delete['Content-Type'] =
      'application/json; charset=UTF-8';

    this.clientServices = new ClientServices(this.axiosInstance, endpoint);
    this.authenServices = new AuthenServices(this.axiosInstance, endpoint);
    this.processDBServices = new ProcessDBServices(
      this.axiosInstance,
      endpoint,
    );
    this.roadmapServices = new RoadmapServices(this.axiosInstance, endpoint);
    this.assessmentServices = new AssessmentServices(
      this.axiosInstance,
      endpoint,
    );   
    this.reassessmentServices = new ReAssessmentServices(
      this.axiosInstance,
      endpoint,
    );
    this.databaseCosoServices = new DatabaseCosoServices(
      this.axiosInstance,
      endpoint,
    );
    this.roadmapCosoServices = new RoadmapCosoServices(
      this.axiosInstance,
      endpoint,
    );
    this.assessmentCosoServices = new AssessmentCosoServices(
      this.axiosInstance,
      endpoint,
    );
    this.dashboardServices = new DashboardServices(
      this.axiosInstance,
      endpoint,
    );
    this.reportServices = new ReportServices(this.axiosInstance, endpoint);
    this.followUpServices = new FollowUpServices(this.axiosInstance, endpoint);
    this.settingTaskServices = new SettingTaskServices(
      this.axiosInstance,
      endpoint,
    );
    this.dashboardFollowUpServices = new DashboardFollowUpServices(
      this.axiosInstance,
      endpoint,
    );
    this.followUpReportServices = new FollowUpReportServices(
      this.axiosInstance,
      endpoint,
    );
  }
}

export default Services;
