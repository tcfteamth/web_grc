import { Tab } from 'semantic-ui-react';

export default class AssessmentServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getAssessmentList(
    token,
    page = 1,
    searchAssessmentText = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    roadmapType = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    overallRatingId = '',
    year = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/toDoList?page=${page}&size=50&search=${searchAssessmentText}&lv3=${lvl3}&lv4=${lvl4}&roadMapType=${roadmapType}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&overallRatingId=${overallRatingId}&year=${year}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getAssessmentDetail(token, assessmentId, tab,mode) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    if(mode === 'view'){
      return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/view?assessmentId=${assessmentId}&tab=${tab}`,
      );
    }else{
      return this.axiosInstance.get(
        `${this.endpoint}/api/manage/assessments/assessmentDetail?assessmentId=${assessmentId}&tab=${tab}`,
      );
    }
}

  async updateAssessment(token, assessmentData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/assessments/updateAssessment`,
      assessmentData,
    );
  }

  async getAssessmentComment(token, assessmentId) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/all-comment-histories?assessmentId=${assessmentId}`,
    );
  }

  async getPartnerList(token, searchText = '') {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/users/partnerList?search=${searchText}&page=1&size=10`,
    );
  }

  async rejectToStaff(token, assessmentId) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/assessments/rejectToStaff?pdcId=${assessmentId}`,
    );
  }

  async deleteComment(token, ids) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/assessments/comments?ids=${ids}`,
    );
  }

  async uploadAssessmentFiles(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/assessments/lvl7/files`,
      data,
    );
  }

  async uploadWorkflowFile(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/assessments/lvl4/files`,
      data,
    );
  }

  async deleteWorkflowFiles(token, workflowFileId) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/assessments/lvl4/files?ids=${workflowFileId}`,
    );
  }

  async deleteAssessmentFiles(token, assessmentFileId) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/assessments/lvl7/files?ids=${assessmentFileId}`,
    );
  }

  async getYearlist(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/copy-list/year-list`,
    );
  }

  async getCloneAssessmentList(
    token,
    departmentNo,
    page = 1,
    selectedYear = '',
    divisionNo = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/copy-list?divisionNo=${divisionNo}&departmentNo=${departmentNo}&year=${selectedYear}&page=${page}&size=5`,
    );
  }

  async getSelectedCloneAssessment(token, cloneAssessmentData) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/copy-prototype?no=${cloneAssessmentData.no}&id=${cloneAssessmentData.id}`,
    );
  }

  async getAllStatusCount(token, departmentId) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/all-status-count?departmentId=${departmentId}`,
    );
  }

  async getIAFindingDetail(token, indicator) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/ia-finding?indicator=${indicator}`,
    );
  }

  async submitIAFindingDetail(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/assessments/assessmentDetail/ia-finding/check`,
      data,
    );
  }

  // export excel
  async exportExcelIafinding(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/ia-finding/export`,
    );
  }

  // import excel
  async importExcelIafinding(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/import/ia-finding`,
      formData,
    );
  }

  async exportAssessmentDetail(assessmentId) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/assessmentDetail?assessmentId=${assessmentId}`,
    );
  }

  async getRiskAndControlListMapping(
    page = 1,
    size = '',
    filterNo = '',
    search = '',
    isStandard = '',
  ) {
    return this.axiosInstance.get(
      `${
        this.endpoint
      }/api/manage/rc-masters?page=${page}&size=${size}&search=${search}&filterNo=${filterNo}&formatFP=${true}&isStandard=${isStandard}`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
