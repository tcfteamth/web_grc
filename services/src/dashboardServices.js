export default class ProcessDBServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getProcessingStatus(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/dashboard/processing-status?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getOverAllRating(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/dashboard/overall-rating?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getAllRating(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/dashboard/all-rating?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getManageFilterList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/filter/filter-list`,
    );
  }

  async getManageFilterBUList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(`${this.endpoint}/api/manage/filter/bu-list`);
  }

  async getManageFilterDepartmentList(token, bu = '') {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/filter/department-list?bu=${bu}`,
    );
  }

  async getManageFilterDivisionList(bu = '', departmentId = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/filter/division-list?bu=${bu}&departmentId=${departmentId}`,
    );
  }

  async getManageFilterShiftList(bu = '', departmentId = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/filter/shift-list?bu=${bu}&departmentId=${departmentId}`,
    );
  }

  async getAreaList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-db-masters/areaList`,
    );
  }

  async getAllWaitDMAccpetList(
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/dashboard/all-waiting-dm?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
