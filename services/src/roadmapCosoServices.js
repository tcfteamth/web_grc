export default class RoadmapServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  // get list departments
  async getDepartmentList() {
    return this.axiosInstance.get(`${this.endpoint}/api/manage/departments`);
  }

  // get list divisions
  async getDivisionById(departmentId) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/departments/divisions/${departmentId}`,
    );
  }

  // get sub process
  async getSubProcessList(divisionId, token) {
    console.log('divisionId', divisionId);
    // this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-roadmaps/get-csa-ids?divisionNo=${divisionId}`,
    );
  }
 // get sub process
 async getShiftSubProcessList(shiftId, token) {
  console.log('shiftId', shiftId);
  // this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
  return this.axiosInstance.get(
    `${this.endpoint}/api/manage/coso-roadmaps/get-csa-ids?shiftNo=${shiftId}`,
  );
}
  // get list lv2
  async getLvlTwoList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-db-masters/getAllLvl2`,
    );
  }

  // get list lv3
  async getLvlThreeList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/lv3List`,
    );
  }

  // get list lv4
  async getLvlFourList(lvlThreeNo) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/lv4List/${lvlThreeNo}`,
    );
  }

  // create roadmap
  async submitCreateRoadmap(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/coso-roadmaps`,
      formData,
    );
  }

  // delete roadmap
  async submitDeleteRoadmap(id) {
    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/coso-roadmaps/${id}`,
    );
  }

  // get edit roadmap
  async getEditRoadmapById(id) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-roadmaps/${id}`,
    );
  }

  //  services coso roadmap list
  async getRoadmapCOSOList(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    let roadmapText;

    if (searchValue) {
      roadmapText = searchValue;
    } else {
      roadmapText = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-roadmaps?page=${page}&size=20&search=${roadmapText}&questionNo=${lvl2}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&component=${area}&year=${year}`,
    );
  }

  async getAssesor(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/users/UsersByDivisionId`,
    );
  }

  async submitRoadmaps(token, roadmapIdsData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    const data = {
      roadmapIds: roadmapIdsData,
    };

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/users/coso-submitRoadmaps`,
      data,
    );
  }

  async acceptRoadmaps(token, acceptRoadmapsData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    const data = {
      roadmaps: acceptRoadmapsData,
    };

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/users/coso-acceptRoadmaps`,
      data,
    );
  }

  //  services coso roadmap list
  async exportCosoRoadmap(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    let roadmapText;

    if (searchValue) {
      roadmapText = searchValue;
    } else {
      roadmapText = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-roadmaps/export?search=${roadmapText}&questionNo=${lvl2}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&component=${area}&year=${year}`,
    );
  }

  async getRoadmapCosoList(token, id, year, divisionId,shiftId, noLvl2, remark) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${
        this.endpoint
      }/api/manage/coso-roadmaps?roadmapId=${id}&year=${year}&divisionId=${divisionId}&shiftId=${shiftId}&noLvl2=${noLvl2}${
        remark && `&remark=${remark}`
      }`,
    );
  }

  async updateRoadmapCoso(
    token,
    id,
    year,
    divisionId,
    noLvl2,
    remark,
    csaPdcIds,
  ) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${
        this.endpoint
      }/api/manage/coso-roadmaps?roadmapId=${id}&year=${year}&divisionId=${divisionId}&noLvl2=${noLvl2}
      ${remark == null ? '' : `&remark=${remark}`}
      ${csaPdcIds && `&csaPdcIds=${csaPdcIds}`}`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
