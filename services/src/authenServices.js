
export default class AuthenServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async login() {

  }

  async changePassword() {

  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
