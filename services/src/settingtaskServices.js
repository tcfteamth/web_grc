export default class settingTaskServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async submitActiveReAssessment() {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/schedules/re-assessment`,
    );
  }

  async submitActiveFollowUp(token) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(`${this.endpoint}/api/manage/follow-up`);
  }

  async submitActiveCOSOReAssessment() {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/coso-schedules/re-assessment`,
    );
  }

  // Get List tag
  async getTagList(token, page) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/tags?page=1&size=10`,
    );
  }

  // Create Tag
  async submitCreateTag(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/tags`,
      formData,
    );
  }
  async submitCreateManualAssign(token,formData) {

    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/users/createManualAssign`,
      formData,
    );
  }
  // Edit Tag
  async submitUpdateTag(token, id, name, pdmIds) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/tags/${id}?name=${name}&pdmIds=${pdmIds}`,
    );
  }

    // Get List Manual Table
    async getManualTableList(token, page) {
      this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
  
      return this.axiosInstance.get(
        `${this.endpoint}/api/manage/users/getManualTable?page=${page}&size=10`,
      );
    }
  // get edit roadmap
  async editTagById(id) {
    return this.axiosInstance.get(`${this.endpoint}/api/manage/tags/${id}`);
  }

  // delete roadmap
  async submitDeleteTag(id) {
    return this.axiosInstance.delete(`${this.endpoint}/api/manage/tags/${id}`);
  }
  
  async submitDeleteManualAssign(id) {
    return this.axiosInstance.delete(`${this.endpoint}/api/manage/users/manualAssign/${id}`);
  }
  async checkReAssessmentInProgress() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/schedules/re-assessment/in-progress`,
    );
  }

  async checkMailFollow() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/notifications/tasks`,
    );
  }

  async checkMailCOSOFollow() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/notifications/coso-tasks`,
    );
  }

  async toggleMailFollow(taskName) {
    const formData = new FormData();
    formData.append('taskName', taskName);

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/notifications/activate-task`,
      formData,
    );
  }
  
  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
