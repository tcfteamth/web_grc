export default class AssessmentServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  //  services coso Assessment list
  async getAssessmentCosoList(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = '',
    owner = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    let assessmentText;

    if (searchValue) {
      assessmentText = searchValue;
    } else {
      assessmentText = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/toDoList?page=${
        page || 1
      }&size=10&search=${assessmentText}&questionNo=${lvl2}&bu=${bu}&roadmapType=${roadmapType}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&component=${area}&year=${year}&ownerTask=${owner}`,
    );
  }

  //  services coso summaryList
  async getAssessmentCosoSummaryList(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = '',
    owner = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    let assessmentText;

    if (searchValue) {
      assessmentText = searchValue;
    } else {
      assessmentText = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/summaryList?page=${
        page || 1
      }&size=10&search=${assessmentText}&questionNo=${lvl2}&bu=${bu}&roadmapType=${roadmapType}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&component=${area}&year=${year}&ownerTask=${owner}`,
    );
  }

  async getAssessmentCosoDetail(token, assessmentId, tab) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/assessmentDetail?assessmentId=${assessmentId}&tab=${tab}`,
    );
  }

  async updateAssessmentCosoDetail(token, assessmentData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/coso-assessments/updateAssessment`,
      assessmentData,
    );
  }

  async submitAssessment(token, roadmapIdsData) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    const data = {
      roadmapIds: roadmapIdsData,
    };

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/users/coso-submitRoadmaps`,
      data,
    );
  }

  async uploadAssessmentCosoFiles(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/assessments/lvl2/files`,
      data,
    );
  }

  async getAssessmentComment(token, assessmentId) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/all-comment-histories?assessmentId=${assessmentId}`,
    );
  }

  async uploadAssessmentFiles(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/coso-assessments/lvl2/files`,
      data,
    );
  }

  async rejectToStaff(token, assessmentId) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/coso-assessments/rejectToStaff?pdcId=${assessmentId}`,
    );
  }

  async deleteCosoAssessmentFiles(token, assessmentFileId) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/coso-assessments/lvl2/files?ids=${assessmentFileId}`,
    );
  }

  async deleteCosoComment(token, commentId) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/coso-assessments/comment?id=${commentId}`,
    );
  }

  async getDocumentStorage(token,divisionNo) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/master/files?divisionNo=${divisionNo}`,
    );
  }

  async addDocumentStorageFile(token,data,divisionNo) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/coso-assessments/master/file?divisionNo=${divisionNo}`,
      data,
    );
  }

  async deleteDocumentStorageFile(token, id, divisionNo) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/coso-assessments/master/file?id=${id}&divisionNo=${divisionNo}`,
    );
  }

  async downloadAllAsZip(token,divisionNo) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/master/files/zip?divisionNo=${divisionNo}`,{responseType: 'blob'}
    );
  }

  async exportCosoAssessment(token, id) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/export?assessmentId=${id}`,
    );
  }

  async exportCOSOSummary(
    token,
    searchValue = '',
    year = '',
    questionNo = '',
    component = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    ownerTask = '',
  ) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/coso-assessments/exportSummary?search=${searchValue}&year=${year}&questionNo=${questionNo}&component=${component}&roadmapType=${roadmapType}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&ownerTask=${ownerTask}`,
    );
  }
  
  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
