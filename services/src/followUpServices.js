export default class FollowUpServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getFollowUpTodoList(
    token,
    page = 1,
    size = 10,
    searchText = '',
    year = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    department = '',
    division = '',
    shift = '',
    startDate = '',
    endDate = '',
    status = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/to-do-list?page=${page}&size=${size}&search=${searchText}&year=${year}&lv3=${lvl3}&lv4=${lvl4}&startDueDate=${startDate}&endDueDate=${endDate}&status=${status}&bu=${bu}&departmentNo=${department}&divisionNo=${division}&shiftNo=${shift}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getFollowUpSummaryList(
    token,
    page = 1,
    size = 10,
    searchText = '',
    year = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    department = '',
    division = '',
    shift = '',
    startDate = '',
    endDate = '',
    status = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/summary-list?page=${page}&size=${size}&search=${searchText}&year=${year}&lv3=${lvl3}&lv4=${lvl4}&startDueDate=${startDate}&endDueDate=${endDate}&status=${status}&bu=${bu}&departmentNo=${department}&divisionNo=${division}&shiftNo=${shift}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getTotalStatusCount(
    token,
    searchText = '',
    year = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    department = '',
    division = '',
    shift = '',
    startDate = '',
    endDate = '',
    status = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/total-status-count?${
        searchText && `search=${searchText}`
      }&year=${year}${lvl3 && `&lv3=${lvl3}`}${
        lvl4 && `&lv4=${lvl4}`
      }&startDueDate=${startDate}&endDueDate=${endDate}&status=${status}&bu=${bu}&departmentNo=${department}&divisionNo=${division}&shiftNo=${shift}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getFollowUpDetail(token, followUpId) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/detail?followUpId=${followUpId}`,
    );
  }

  async getFollowUpDetailStatus(token, followUpId) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/detail/status-list?followUpId=${followUpId}`,
    );
  }

  async updateFollowUpDetail(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/follow-up/detail`,
      data,
    );
  }

  async submitFollowUp(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/follow-up/submit`,
      data,
    );
  }

  async getFollowUpYearList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/filter/year`,
    );
  }

  async getFollowUpStatusList(tab = 'TODO') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/filter/status-list?tab=${tab}`,
    );
  }

  async getFollowUpFilterList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/filter-list`,
    );
  }

  async saveFileFollowDetail(token, data) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/follow-up/files`,
      data,
    );
  }

  async submitDeleteFollowDocFiles(id) {
    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/follow-up/files?ids=${id}`,
    );
  }

  async getFollowUpComment(id) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/comments?followUpId=${id}`,
    );
  }

  async saveFollowUpDetail(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/follow-up/detail/save`,
      data,
    );
  }

  async exportFollowUpSummary(
    token,
    searchText = '',
    year = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    department = '',
    division = '',
    shift = '',
    startDate = '',
    endDate = '',
    status = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/summary-list/export?search=${searchText}&lv4=${lvl4}&startDueDate=${startDate}&endDueDate=${endDate}&status=${status}&year=${year}&lv3=${lvl3}&bu=${bu}&departmentNo=${department}&divisionNo=${division}&shiftNo=${shift}`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }

  async getDepartmentFollowUpVP(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/filter/follow-up/department`,
    );
  }
}
