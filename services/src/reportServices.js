export default class ReportServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getMainGraph(year = new Date().getFullYear(), bu = '', processAnd = '', processOr = '', objectives = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/main-graph?year=${year}&bu=${bu}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getSummaryBU(year = new Date().getFullYear(), bu = '', processAnd = '', processOr = '', objectives = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/summary-bu?year=${year}&bu=${bu}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getSummaryDepartment(
    year = new Date().getFullYear(),
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/summary-department?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getManageRole(year = new Date().getFullYear(), bu = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/manage-role?year=${year}&bu=${bu}`,
    );
  }

  async getDetailReport(year = new Date().getFullYear(), bu = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/?year=${year}&bu=${bu}`,
    );
  }

  async saveReport(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/report/status/save`,
      data,
    );
  }

  async deleteComment(commentIds) {
    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/report/comments/delete?ids=${commentIds}`,
    );
  }

  async exportReportExcel(
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
  ) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/export-summary-department?year=${year}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}`,
    );
  }

  async getPublishedList(year = '') {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/report/status?year=${year}`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
