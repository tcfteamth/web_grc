export default class DashboardFollowUpModel {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getDashboardFollowUp(
    token,
    year = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    processAnd = '',
    processOr = '',
    objectives = ''
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/dashboard?year=${year}&bu=${bu}&divisionNo=${divisionNo}&departmentNo=${departmentNo}&tagIdsAnd=${processAnd}&tagIdsOr=${processOr}&objectives=${objectives}`,
    );
  }

  async getDashboardFilterList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/dashboard/filter`,
    );
  }

  async getReportFollowUpFilterList(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/follow-up/report/filter`,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }
}
