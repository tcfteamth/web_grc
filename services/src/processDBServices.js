export default class ProcessDBServices {
  constructor(axiosInstance, endpoint) {
    this.endpoint = endpoint;
    this.axiosInstance = axiosInstance;
  }

  async getProcessDBList(searchProcessDBText, lvl2No, lvl3No, lvl4No, tagIdsAnd,tagIdsOr) {
    let processText;
    let filterNo = lvl4No ? lvl4No : lvl3No ? lvl3No : lvl2No ? lvl2No : null;

    if (searchProcessDBText) {
      processText = searchProcessDBText;
    } else {
      processText = '';
    }

    if (filterNo == null) {
      filterNo = '';
    }

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters?${
        processText && `&search=${processText}`
      }${filterNo && `&filterNo=${filterNo}`}${tagIdsAnd && `&tagIdsAnd=${tagIdsAnd}`}${tagIdsOr && `&tagIdsOr=${tagIdsOr}`}`,
    );
  }

  async getProcessDBById(id) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/${id}`,
    );
  }

  async getRiskAndControlList(
    token,
    page = 1,
    searchText = '',
    fullPath = '',
    type = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    const search = searchText ? `&search=${searchText}` : '';
    const path = fullPath && `&filterNo=${fullPath}`;
    const objType = type && `&objectiveType=${type}`;

    return this.axiosInstance.get(
      `${
        this.endpoint
      }/api/manage/rc-masters?page=${page}&size=${10}${search}${path}${objType}`,
    );
  }

  async getRiskAndControlByNoLvl(noLvl) {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/rc-masters/${noLvl}`,
    );
  }

  async hideSubprocess(token, id) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;
    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/process-db-masters/disabled?id=${id}`,
    );
  }

  async getComplianceList(
    token,
    page = 1,
    searchText = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${
        this.endpoint
      }/api/manage/compliance-universe?page=${page}&size=${10}&search=${searchText}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}`,
    );
  }

  // importExcelCompliance;
  async importExcelCompliance(formData) {
    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/import/compliance`,
      formData,
    );
  }

  // importExcelCompliance;
  async exportExcelCompliance(token) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/compliance-universe/export`,
    );
  }

  // exportExcelRiskAndControl;
  async exportExcelRiskAndControl(
    token,
    lvl4 = '',
    type = '',
    searchText = '',
  ) {
    const search = searchText ? `&search=${searchText}` : '';
    const lvl = lvl4 && `&lv4No=${lvl4}`;
    const objType = type && `&objectiveType=${type}`;

    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/rc-masters/export?${search}${lvl}${objType}`,
    );
  }

  // importExcelCompliance;
  async exportExcelProcessDataBase(
    token,
    searchText = '',
    lvl2No = '',
    lvl3No = '',
    lvl4No = '',
  ) {
    let filterNo = lvl4No ? lvl4No : lvl3No ? lvl3No : lvl2No ? lvl2No : null;

    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/process-db-masters/export?${
        searchText && `&search=${searchText}`
      }${filterNo && `&filterNo=${filterNo}`}`,
    );
  }

  async getRiskAndControlById(token, id) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/rc-masters/riskControlDetail?processDBMasterId=${id}`,
    );
  }

  async createRiskAndControl(token, data) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;
    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/rc-masters/updateRiskControl`,
      data,
    );
  }

  async getIAFindingList(
    token,
    page = 1,
    searchText = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    type = '',
  ) {
    this.axiosInstance.defaults.headers.get.Authorization = `Bearer ${token}`;

    return this.axiosInstance.get(
      `${
        this.endpoint
      }/api/manage/ia-finding/list?page=${page}&size=${10}&search=${searchText}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&type=${type}
      `,
    );
  }

  changeEndpoint(newEndpoint) {
    this.endpoint = newEndpoint;
  }

  async createProcessMaster(token, data) {
    this.axiosInstance.defaults.headers.post.Authorization = `Bearer ${token}`;

    return this.axiosInstance.post(
      `${this.endpoint}/api/manage/process-db-masters`,
      data,
    );
  }

  async updateProcessMaster(token, data) {
    this.axiosInstance.defaults.headers.put.Authorization = `Bearer ${token}`;

    return this.axiosInstance.put(
      `${this.endpoint}/api/manage/process-db-masters`,
      data,
    );
  }

  async getTagList() {
    return this.axiosInstance.get(
      `${this.endpoint}/api/manage/tags?page=1&size=1000`,
    );
  }

  // delete list risk and control;
  async onDeleteRiskAndControlList(token, id) {
    this.axiosInstance.defaults.headers.delete.Authorization = `Bearer ${token}`;

    return this.axiosInstance.delete(
      `${this.endpoint}/api/manage/process-db-masters/${id}`,
    );
  }
}
