import React from "react";
import { Box } from "../components/element";

export default function withCenter(WrappedComponent) {
  return (props) => (
    <Box flex={1} center alignCenter>
      <WrappedComponent {...props} />
    </Box>
  );
}
