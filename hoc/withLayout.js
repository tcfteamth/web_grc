import React from 'react';
import styled from 'styled-components';
import { inject } from 'mobx-react';
import { compose, lifecycle, withHandlers, withState } from 'recompose';
import { ToastMessage, ToastContainer } from 'react-toastr';
import { AppSidemenu, AppHeader, BottomBar } from '../components';
import {
  Box,
  NewLoading,
  TimeoutModal,
  TokenExpiredModal,
} from '../components/element';
import { withApp } from '.';
import { sizes, colors } from '../utils';

const PageWrapper = styled(Box)`
  min-height: 100vh;
  padding-top: 90px;
  width: 100%;
  background-color: ${colors.primaryBackground};

  @media print {
    padding-top: 0px;
  }
`;

const ContentWrapper = styled(Box)`
  // padding: ${sizes.xl} ${sizes.md} ${sizes.xl} ${sizes.xl};
  margin: 32px !important;
  border-radius: 6px;
  padding-bottom: 90px;

  @media print {
    padding-bottom: 0px;
    margin: 0 32px !important;
  }
`;

const ToastMessageFactory = React.createFactory(ToastMessage.animation);
let _toastContainer;

const enhance = compose(withApp);

// const index = (WrappedComponent) => ((props) => (
//   <div>
//     <NewLoading />
//     <AppSidemenu {...props} />
//     <AppHeader {...props} />
//     <PageWrapper className="PageWrapper" alignStretch>
//       <ContentWrapper>
//         <WrappedComponent {...props} />
//       </ContentWrapper>
//       <BottomBar {...props} />
//     </PageWrapper>
//   </div>
// ));

// export default withApp(index);

export default (WrappedComponent) =>
  enhance((props) => (
    <div>
      <NewLoading />
      <div className="no-print">
        <AppSidemenu {...props} />
        <AppHeader {...props} />
      </div>
      <PageWrapper className="PageWrapper" id="change-pageWrapper" alignStretch>
        <ContentWrapper>
          <WrappedComponent {...props} />
        </ContentWrapper>
        {/* <BottomBar {...props} /> */}
      </PageWrapper>
      <TimeoutModal />
      <TokenExpiredModal />
    </div>
  ));
