import React, { useEffect, Component, useState } from 'react';
import { Provider } from 'mobx-react';
import Head from 'next/head';
import Router from 'next/router';
import storejs from 'store';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import dbFontStyle from '../styles/dbFont.css';
import calendarStyle from '../styles/calendar.css';
import customStyle from '../styles/custom.css';
import { initAuthStore, initDataContext, initTimeoutStore } from '../contexts';
import { assesorDropdownStore } from '../model';
import { config } from '../utils';
import 'react-datepicker/src/stylesheets/datepicker.scss';

const withApp = (WrappedComponent) => {
  const App = (props) => {
    const timeoutStore = useLocalStore(() => initTimeoutStore);
    const authStore = initAuthStore();

    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
      (async () => {
        try {
          if (Router.pathname !== '/welcome' && !authStore.accessToken) {
            window.location.href = `${config.baseUrlLogin}`;
          }

          await new Promise([
            assesorDropdownStore.getAssesor(authStore.accessToken),
          ]);
          timeoutStore.isTimeout();
        } catch (e) {
          console.log(e);
        } finally {
          console.log('finally');
          setIsLoaded(true);
        }
      })();
    }, []);

    return (
      <main>
        <Head>
          <meta name="msvalidate.01" content="875018E7F9D8C9FCEC9F48FF3FA5D466" />
          <title>CSA CONNECT</title>
          <meta name="msvalidate.01" content="875018E7F9D8C9FCEC9F48FF3FA5D466" />
          <meta name="robots" content="noindex" />
          <link
            href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.css"
            rel="stylesheet"
          />
          <style dangerouslySetInnerHTML={{ __html: customStyle }} />
          <style dangerouslySetInnerHTML={{ __html: calendarStyle }} />
          <style dangerouslySetInnerHTML={{ __html: dbFontStyle }} />
          <link
            href="/static/vendor/node_modules/froala-editor/css/froala_style.min.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/froala-editor/css/froala_editor.pkgd.min.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/font-awesome/css/font-awesome.min.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/animate.css/animate.min.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/toastr/build/toastr.min.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/react-dates/lib/css/_datepicker.css"
            rel="stylesheet"
          />
          <link
            href="/static/vendor/node_modules/react-select/dist/react-select.css"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/react-draft-wysiwyg@1.13.2/dist/react-draft-wysiwyg.css"
          />
          <link
            rel="stylesheet"
            href="/static/vendor/node_modules/react-date-range/dist/styles.css"
          />
          <link
            rel="stylesheet"
            href="/static/vendor/node_modules/react-date-range/dist/theme/default.css"
          />
          <link
            rel="stylesheet"
            href="/static/vendor/node_modules/react-step-progress-bar/styles.css"
          />
          <script src="/static/vendor/node_modules/jquery/dist/jquery.min.js" />
          <script src="/static/vendor/node_modules/froala-editor/js/froala_editor.pkgd.min.js" />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js" />
          <script src="https://cdn.jsdelivr.net/npm/flatpickr" />
          <script src="https://unpkg.com/react/umd/react.production.min.js" />
          <script src="https://unpkg.com/react-dom/umd/react-dom.production.min.js" />
          <script src="https://unpkg.com/prop-types/prop-types.min.js" />
          <script src="https://unpkg.com/recharts/umd/Recharts.min.js" />
          <script src="https://cdn.jsdelivr.net/npm/react-datepicker@3.1.3/dist/react-datepicker.min.css" />
          <meta name="robots" content="noindex" ></meta>
        </Head>
        {isLoaded ? <WrappedComponent {...props} /> : <div>Loading</div>}
        <meta name="robots" content="noindex" ></meta>
      </main>
    );
  };

  App.getInitialProps = async ({ req }) => {
    const isBrowser = process.browser;
    const authStore = initAuthStore(!isBrowser);
    return { isBrowser, authStore };
  };

  return App;
};

export default withApp;
 