import React, { useState, useEffect } from 'react';
import {
  Image,
  Grid,
  Loader,
  Dimmer,
  Card,
  Button,
  Header,
} from 'semantic-ui-react';
import { observer, useObserver, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import Router from 'next/router';
import { withLayout } from '../../hoc';
import {
  RoadMapAssessment,
  RoadMapRequests,
  RoadMapAssessmentAdmin,
  RoadMapRequestsAdmin,
  RoadMapAssessmentViewOnly,
} from '../../components/RoadMapPage';
import {
  ButtonAll,
  Text,
  DropdownGroup,
  ButtonBorder,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { dataListRoadMapRe } from '../../utils/static';
import { initRoadmapContext } from '../../contexts/roadmap';
import { initDataContext } from '../../contexts';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;
const SearchInput = styled.input`
  width: 180px;
  font-size: 20px;
  color: #b1b3aa;
`;
const LineSearch = styled.div`
  flex-direction: row;
  justify-content: flex-start;
  display: flex;
  align-items: center;
  border-bottom: 2px solid #b1b3aa;
  margin-left: 24px;
  width: 217px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;
const Cards = styled(Card)`
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  width: 100% !important;
  padding: 20px 16px !important;
  min-hight: 80px;
  @media only screen and (min-width: 768px) {
    hight: 100px;
  }
`;
const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'sort 1', value: 1 },
  { text: 'sort 2', value: 2 },
  { text: 'sort 3', value: 3 },
];
const index = () => {
  const dataContext = initDataContext();
  const roadmapContext = useLocalStore(() => initRoadmapContext);
  const [isOpen, setIsOpen] = useState('Assessment');
  const [isShowFillter, setIsShowFillter] = useState(false);
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };
  const renderTabAssessment = () => <RoadMapAssessmentAdmin />;
  const renderTabRequests = () => (
    <RoadMapRequestsAdmin data={dataListRoadMapRe} />
  );

  return useObserver(() => (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>

      <Div between>
        <Header className="click" onClick={() => Router.back()}>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              Roadmap
            </Text>
          </Header.Content>
        </Header>
        <Header>
          <Header.Content>
            <Text
              style={{ textAlign: 'center' }}
              fontSize={sizes.xl}
              color={colors.primary}
            >
              ประจำปี 2563
            </Text>
          </Header.Content>
        </Header>
      </Div>
      <div style={{ paddingTop: 32 }}>
        <Grid style={{ margin: 0 }}>
          <Grid.Row columns={4} style={{ padding: 0 }}>
            <Grid.Column style={{ padding: 0 }}>
              <Cards>
                <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                  <Grid.Column floated="left">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Image
                        src="../../static/images/new@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.pink}
                        >
                          2020
                        </Text>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.pink}
                        >
                          New Assessment
                        </Text>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column floated="right">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.xl}
                        color={colors.primaryBlack}
                      >
                        20
                      </Text>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Cards>
            </Grid.Column>
            <Grid.Column width={4} style={{ padding: '0px 8px' }}>
              <Cards>
                <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                  <Grid.Column floated="left">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Image
                        src="../../static/images/re@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.orange}
                        >
                          2020
                        </Text>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.orange}
                        >
                          New Assessment
                        </Text>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column floated="right">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.xl}
                        color={colors.primaryBlack}
                      >
                        20
                      </Text>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Cards>
            </Grid.Column>
            <Grid.Column width={4} style={{ paddingRight: 8, paddingLeft: 0 }}>
              <Cards>
                <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                  <Grid.Column floated="left">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Image
                        src="../../static/images/2021@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.textBrightGray}
                        >
                          2020
                        </Text>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.textBrightGray}
                        >
                          ASSESSMENT
                        </Text>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column floated="right">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.xl}
                        color={colors.primaryBlack}
                      >
                        20
                      </Text>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Cards>
            </Grid.Column>
            <Grid.Column width={4} style={{ padding: 0 }}>
              <Cards>
                <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                  <Grid.Column floated="left">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Image
                        src="../../static/images/2022@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.textDarkGray}
                        >
                          2020
                        </Text>
                        <Text
                          fontWeight="bold"
                          fontSize={sizes.s}
                          color={colors.textDarkGray}
                        >
                          ASSESSMENT
                        </Text>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column floated="right">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.xl}
                        color={colors.primaryBlack}
                      >
                        -
                      </Text>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Cards>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ padding: '24px 0px 16px' }}>
            <Grid.Column
              width={4}
              style={{ padding: 0, display: 'flex', alignItems: 'center' }}
            >
              <Button.Group>
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    padding: '10px 20px',
                    backgroundColor: `${
                      isOpen === 'Assessment'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => handelOpen('Assessment')}
                >
                  <Text
                    fontWeight="med"
                    fontSize={sizes.s}
                    color={
                      isOpen === 'Assessment'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    ASSESSMENT
                  </Text>
                  <Text
                    fontSize={sizes.s}
                    style={{ paddingLeft: 8 }}
                    color={
                      isOpen === 'Assessment'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    (70)
                  </Text>
                </Button>
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    padding: '10px 20px',
                    backgroundColor: `${
                      isOpen === 'Requests'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => handelOpen('Requests')}
                >
                  <Text
                    fontWeight="med"
                    fontSize={sizes.s}
                    style={{ paddingLeft: 8 }}
                    color={
                      isOpen === 'Requests'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                    style={{ display: 'flex', alignContent: 'center' }}
                  >
                    REQUESTS{' '}
                  </Text>
                  <Text
                    fontSize={sizes.s}
                    color={
                      isOpen === 'Requests'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    (5)
                  </Text>
                </Button>
              </Button.Group>
            </Grid.Column>

            <Grid.Column
              floated="right"
              width={10}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                padding: 0,
              }}
            >
              {/* <Text
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ paddingRight: 8 }}
              >
                Filter
              </Text>
              <DropdownGroup
                width={168}
                options={sortBy}
                textDefault="All Assessment"
              /> */}
              <ButtonBorder
                textWeight="bold"
                style={{ marginLeft: 3 }}
                textSize={sizes.xxs}
                width={156}
                options={sortBy}
                text="Advanced Filter"
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  isShowFillter
                    ? '../../static/images/fillter-white@3x.png'
                    : '../../static/images/fillter@3x.png'
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => handelOpenFillter(!isShowFillter)}
              />
              <LineSearch>
                <Image
                  src="../../static/images/iconSearch@3x.png"
                  style={{ marginRight: 12, width: 18, height: 18 }}
                />
                <div className="ui transparent input">
                  <SearchInput type="text" placeholder="Search " />
                </div>
              </LineSearch>
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Grid style={{ margin: 0, padding: 0 }}>
                <Div between style={{ padding: 0 }}>
                  <Text fontWeight="med" fontSize={sizes.s} color="#00aeef">
                    Advanced Filter
                  </Text>
                  <div
                    className="click"
                    onClick={() => handelOpenFillter(!isShowFillter)}
                  >
                    <Image
                      src="../../static/images/x-close@3x.png"
                      style={{
                        width: 18,
                        height: 18,
                        padding: 0,
                        marginTop: -8,
                        position: 'relative',
                      }}
                    />
                  </div>
                </Div>
                <Grid.Row columns={5}>
                  <Grid.Column style={{ paddingLeft: 0 }}>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Assessment
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault="All Assessment"
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Assessor
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault="All Assessment"
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Roadmap
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault="All Assessment"
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      CSA
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault="All Assessment"
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Remark
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault="All Assessment"
                      border
                    />
                  </Grid.Column>
                </Grid.Row>
                <div style={{ padding: 0 }}>
                  <ButtonAll width={96} color="#1b1464" text="Filter" />
                </div>
              </Grid>
            </CardFillter>
          )}
          <Grid.Row style={{ padding: 0 }}>
            {isOpen === 'Assessment'
              ? renderTabAssessment()
              : renderTabRequests()}
          </Grid.Row>
        </Grid>
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
