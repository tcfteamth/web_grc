import React, { useState, useEffect } from 'react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { Image, Grid, Header } from 'semantic-ui-react';
import styled from 'styled-components';
import { initRoadmapContext } from '../../contexts/roadmap';
import { withLayout } from '../../hoc';
import {
  RoadMapAssessment,
  RoadMapAssessmentAdmin,
  RoadMapAssessmentViewOnly,
} from '../../components/COSORoadMapPage';
import {
  ButtonAll,
  Text,
  DropdownGroup,
  RoadmapTypeDropdown,
  ComponentAreaDropdown,
  DropdownSelect,
  YearDropdown,
  StatusDropdown,
  DivisionDropdown,
  ShiftDropdown,
  DepartmentDropdown,
  ProcessLvl2Dropdown,
  BUDropdown,
  ButtonBorder,
  CardEmpty,
  SearchInput,
  Box,
} from '../../components/element';
import { CreateRoadmapModal } from '../../components/COSORoadMapPage/modal';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { RoadmapCOSOModel } from '../../model/COSO';
import useWindowDimensions from '../../hook/useWindowDimensions';
import DropdownAll from '../../components/element/Dropdown';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'sort 1', value: 1 },
  { text: 'sort 2', value: 2 },
  { text: 'sort 3', value: 3 },
];

const index = () => {
  const roadmapContext = useLocalStore(() => initRoadmapContext);
  const roadmapModel = useLocalStore(() => new RoadmapCOSOModel());
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();
  const [isLoading, setIsLoading] = useState(false);
  const [isShowFillter, setIsShowFillter] = useState(false);
  const [active, setActive] = useState(false);

  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };

  const setAlertModal = () => {
    setActive(true);
  };

  const handleCloseCreateRoadmapModal = (status) => {
    setActive(status);
  };

  useEffect(() => {
    roadmapModel.filterModel.setField(
      'selectedIsYear',
      new Date().getFullYear(),
    );
  }, [roadmapModel]);

  useEffect(() => {
    const currentYear = new Date().getFullYear();
    roadmapModel.getRoadmapCOSOList(
      authContext.accessToken,
      1,
      roadmapModel.searchRoadmapText,
      roadmapModel.filterModel.selectedLvl2,
      roadmapModel.filterModel.selectedBu,
      roadmapModel.filterModel.selectedDepartment.no,
      roadmapModel.filterModel.selectedDivision,
      roadmapModel.filterModel.selectedShift,
      roadmapModel.filterModel.selectedStatus,
      roadmapModel.filterModel.selectedArea,
      currentYear,
    );
    return function cleanup() {
      roadmapContext.clearSearchText();
    };
  }, []);

  // render data ตาม role ของ user
  // NOTE: need to refactor
  const renderTabAssessment = (roadmapList) => {
    if (authContext.roles.isAdmin) {
      return <RoadMapAssessmentAdmin roadmapData={roadmapList} />;
    }
    if (authContext.roles.isDM) {
      return <RoadMapAssessment roadmapData={roadmapList} />;
    }
    return <RoadMapAssessmentViewOnly roadmapData={roadmapList} />;
  };

  const renderEmptyCard = () => {
    if (authContext.roleName === 'SUPER-ADMIN') {
      return (
        <Grid.Row style={{ padding: 0 }}>
          <CardEmpty
            handelOnClick={() =>
              setAlertModal('Create RoadMap', '', '', '', 'Create Now')
            }
            Icon="../../static/images/iconRoadmap@3x.png"
            textTitle="No Roadmap"
            textButton="Create Roadmap "
          />
        </Grid.Row>
      );
    }

    return (
      <Grid.Row style={{ padding: 0 }}>
        <CardEmpty
          Icon="../../static/images/iconRoadmap@3x.png"
          textTitle="No Roadmap"
        />
      </Grid.Row>
    );
  };

  const renderTab = () => {
    if (
      authContext.roleName === 'SUPER-ADMIN' ||
      authContext.roleName === 'DM'
    ) {
      return (
        <Grid.Column
          width={4}
          computer={8}
          tablet={8}
          mobile={16}
          style={{ padding: 0, display: 'flex', alignItems: 'center' }}
        >
          <Text fontWeight="med" fontSize={sizes.s} color={colors.textBlack}>
            QUESTION
          </Text>
        </Grid.Column>
      );
    }

    return (
      <Grid.Column
        width={4}
        computer={8}
        tablet={8}
        mobile={16}
        style={{ padding: 0, display: 'flex', alignItems: 'center' }}
      >
        <Div center>
          <Text
            fontSize={sizes.s}
            fontWeight="bold"
            color={colors.primaryBlack}
          >
            QUESTION LIST
          </Text>
        </Div>
      </Grid.Column>
    );
  };

  return useObserver(() => (
    <div>
      <Div between>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              COSO Roadmap
            </Text>
          </Header.Content>
        </Header>
      </Div>
      <div>
        <Grid style={{ margin: 0, paddingTop: 16 }}>
          <Grid.Row style={{ padding: '24px 0px 16px' }}>
            {renderTab()}
            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: screenWidth < 768 ? 'flex-start' : 'flex-end',
                padding: 0,
              }}
            >
              <ButtonBorder
                textWeight="med"
                textSize={sizes.xs}
                width={screenWidth < 768 ? 80 : 156}
                options={sortBy}
                text="Advanced Filter"
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  screenWidth >= 768
                    ? isShowFillter
                      ? '../../static/images/fillter-white@3x.png'
                      : '../../static/images/fillter@3x.png'
                    : ''
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => {
                  handelOpenFillter(!isShowFillter);
                  if (isShowFillter) {
                    roadmapModel.filterModel.resetFilter();
                    roadmapModel.getRoadmapCOSOList(
                      authContext.accessToken,
                      1,
                      roadmapModel.searchRoadmapText,
                    );
                  }
                }}
              />

              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  roadmapModel.setSearchRoadmapText(e.target.value);

                  if (!roadmapModel.searchRoadmapText) {
                    roadmapModel.getRoadmapCOSOList(
                      authContext.accessToken,
                      1,
                      roadmapModel.searchRoadmapText,
                    );
                  }
                }}
                onEnterKeydown={(e) => {
                  // keycode 13 คือ ปุ่ม enter
                  if (e.keyCode === 13) {
                    roadmapModel.getRoadmapCOSOList(
                      authContext.accessToken,
                      1,
                      roadmapModel.searchRoadmapText,
                    );
                  }
                }}
                width={300}
                value={roadmapModel.searchRoadmapText}
              />
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Box
                horizontal="row"
                alignCenter
                spaceBetween
                style={{ padding: 0 }}
              >
                <Text fontWeight="med" fontSize={sizes.s} color="#00aeef">
                  Advanced Filter
                </Text>
                <div>
                  <Image
                    className="click"
                    onClick={() => {
                      handelOpenFillter(!isShowFillter);
                      if (isShowFillter) {
                        roadmapModel.filterModel.resetFilter();
                        roadmapModel.getRoadmapCOSOList(
                          authContext.accessToken,
                          1,
                          roadmapModel.searchRoadmapText,
                        );
                      }
                    }}
                    src="../../static/images/x-close@3x.png"
                    style={{
                      width: 18,
                      height: 18,
                      padding: 0,
                      marginTop: -8,
                      position: 'relative',
                    }}
                  />
                </div>
              </Box>
              <Grid
                style={{ paddingLeft: 16, paddingRight: 8, paddingBottom: 16 }}
              >
                <Grid.Row>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Year
                    </Text>
                    <YearDropdown
                      placeholder="Select Year"
                      handleOnChange={(year) => {
                        roadmapModel.filterModel.setField(
                          'selectedIsYear',
                          year,
                        );
                      }}
                      value={roadmapModel.filterModel.selectedIsYear}
                      model={[roadmapModel]}
                    />
                  </Grid.Column>
                  {authContext.roles.isAdmin && (
                    <>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          BU
                        </Text>
                        <BUDropdown
                          handleOnChange={(bu) => {
                            roadmapModel.filterModel.setField('selectedBu', bu);
                          }}
                          value={roadmapModel.filterModel.selectedBu}
                          disabled={roadmapModel.filterModel.isBuDisabled}
                          defaultValue="Select BU"
                          placeholder="Select BU"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Department
                        </Text>
                        <DepartmentDropdown
                          handleOnChange={(department) => {
                            roadmapModel.filterModel.setField(
                              'selectedDepartment',
                              department,
                            );
                          }}
                          value={
                            roadmapModel.filterModel.selectedDepartment.value
                          }
                          disabled={
                            roadmapModel.filterModel.isDepartmentDisabled
                          }
                          selectedBu={roadmapModel.filterModel.selectedBu}
                          defaultValue="Select Department"
                          placeholder="Select Department"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Division
                        </Text>
                        <DivisionDropdown
                          handleOnChange={(divisionNo) => {
                            if (roadmapModel.filterModel.resetBuMistake) {
                              roadmapModel.filterModel.setField(
                                'selectedBu',
                                '',
                              );
                            }
                            roadmapModel.filterModel.setField(
                              'selectedDivision',
                              divisionNo,
                            );
                          }}
                          value={roadmapModel.filterModel.selectedDivision}
                          selectedDepartmentId={
                            roadmapModel.filterModel.selectedDepartment.value
                          }
                          defaultValue="Select Division"
                          placeholder="Select Division"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Shift
                        </Text>
                        <ShiftDropdown
                          handleOnChange={(shiftNo) => {
                            if (roadmapModel.filterModel.resetBuMistake) {
                              roadmapModel.filterModel.setField(
                                'selectedBu',
                                '',
                              );
                            }
                           
                              roadmapModel.filterModel.setField(
                                'selectedShift',
                                shiftNo,
                              );
                           
                          }}
                          value={
                            roadmapModel.filterModel.selectedShift 
                          }
                          selectedDepartmentId={
                            roadmapModel.filterModel.selectedDepartment.value
                          }
                          defaultValue="Select Shift"
                          placeholder="Select Shift"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Status
                        </Text>
                        <StatusDropdown
                          handleOnChange={(status) => {
                            roadmapModel.filterModel.setField(
                              'selectedStatus',
                              status,
                            );
                          }}
                          tab={roadmapModel.filterModel.selectedTab}
                          type="ROADMAP"
                          value={roadmapModel.filterModel.selectedStatus}
                          defaultValue="Select Status"
                          placeholder="Select Status"
                        />
                      </Grid.Column>
                    </>
                  )}
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Question
                    </Text>

                    <ProcessLvl2Dropdown
                      handleOnChange={(lvl2) => {
                        roadmapModel.filterModel.setField('selectedLvl2', lvl2);
                      }}
                      value={roadmapModel.filterModel.selectedLvl2}
                      defaultValue="Select Question"
                      placeholder="Select Question"
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Component Area
                    </Text>
                    <ComponentAreaDropdown
                      handleOnChange={(area) => {
                        roadmapModel.filterModel.setField('selectedArea', area);
                      }}
                      type="ROADMAP"
                      value={roadmapModel.filterModel.selectedArea}
                      defaultValue="Select Area"
                      placeholder="Select Area"
                    />
                  </Grid.Column>
                </Grid.Row>
                <div style={{ padding: 0, display: 'flex' }}>
                  <ButtonAll
                    width={96}
                    textSize={sizes.xs}
                    color="#1b1464"
                    text="Filter"
                    onClick={() => {
                      roadmapModel.getRoadmapCOSOList(
                        authContext.accessToken,
                        1,
                        roadmapModel.searchRoadmapText,
                        roadmapModel.filterModel.selectedLvl2,
                        roadmapModel.filterModel.selectedBu,
                        roadmapModel.filterModel.selectedDepartment.no,
                        roadmapModel.filterModel.selectedDivision,
                        roadmapModel.filterModel.selectedShift,
                        roadmapModel.filterModel.selectedStatus,
                        roadmapModel.filterModel.selectedArea,
                        roadmapModel.filterModel.selectedIsYear,
                      );
                    }}
                  />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => {
                      if (isShowFillter) {
                        roadmapModel.filterModel.resetFilterRoadmap();
                        roadmapModel.getRoadmapCOSOList(
                          authContext.accessToken,
                          1,
                          roadmapModel.searchRoadmapText,
                        );
                      }
                    }}
                  />
                </div>
              </Grid>
            </CardFillter>
          )}

          {roadmapModel.list && roadmapModel.list.length !== 0 ? (
            <Grid.Row style={{ padding: 0 }}>
              {renderTabAssessment(roadmapModel)}
            </Grid.Row>
          ) : (
            renderEmptyCard()
          )}
        </Grid>
        {active && (
          <CreateRoadmapModal
            active={active}
            onClose={() => handleCloseCreateRoadmapModal(false)}
            // onCheck={() => setIsCheck(isCheck)}
          />
        )}
        {/* call Back */}
        {/* <ModalGlobal
          open={isCheck}
          title="This process (Level 2) already exists."
          content="Please create a new roadmap again."
          onSubmit={() => handleCheckModal(false)}
          // submitText="YES"
          cancelText="Close"
          onClose={() => handleCloseCheckModal(false)}
        /> */}
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
