import React, { useState, useEffect } from 'react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { Image, Grid, Loader, Dimmer, Button, Select } from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import useWindowDimensions from '../../hook/useWindowDimensions';
import {
  ButtonAll,
  Text,
  ButtonBorder,
  CardEmpty,
  SearchInput,
  BUDropdown,  
  BottomBarReAssessment,
  RoadmapTypeDropdown,
  ProcessLvl3Dropdown,
  ProcessLvl4Dropdown,
  DepartmentDropdown,
  DivisionDropdown,
  ShiftDropdown,
  StatusDropdown,
  RateDropdown,
  OwenerTaskDropdown,
  YearDropdown,
  YearListDropdown,
  DropdownSelect
} from '../../components/element';
import {
  ReAssessmentToDoList,

} from '../../components/ReassessmentPage';
import { withLayout } from '../../hoc';
import { ReAssessmentListModel } from '../../model/ReAssessmentModel';
import { initAuthStore } from '../../contexts';
import { ROLES } from '../../contexts/AuthContext';
import { apiGatewayInstance } from '../../helpers/utils';
import { manageBUDropdownStore, yearListDropdownStore  } from '../../model';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const sortBy = [
  { text: 'text 1', value: 0 },
  { text: 'text 2', value: 1 },
  { text: 'text 3', value: 2 },
  { text: 'text 4', value: 3 },
];

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const index = (props) => {
  const [isOpen, setIsOpen] = useState('ToDoList');
  const { width } = useWindowDimensions();
  const [isShowFillter, setIsShowFillter] = useState(false);
  const reassessmentModel = useLocalStore(() => new ReAssessmentListModel());
  const authContext = initAuthStore();
  const [isLoading, setIsLoading] = useState(false);
  const [isFirstTime, setIsFirstTime] = useState(true);
  const [defaultYear, setDetaultYear] = useState('');


  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };

  const getReAssessmentList = async () => {
    try {
      await reassessmentModel.getReAssessmentList(
        authContext.accessToken,
        1,
        reassessmentModel.searchAssessmentText,
        reassessmentModel.filterModel.selectedLvl3,
        reassessmentModel.filterModel.selectedLvl4,
        reassessmentModel.filterModel.selectedBu,
        reassessmentModel.filterModel.selectedDepartment.no,
        reassessmentModel.filterModel.selectedDivision,
        reassessmentModel.filterModel.selectedShift
      );
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    (async () => {
      try {
        const yearResult = await yearListDropdownStore.getYearListFillter(
          authContext.accessToken,
        );
        
        setDetaultYear(yearResult[1].value);
        reassessmentModel.filterModel.setField('selectedYear', yearResult[1].value);

        await getReAssessmentList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  const renderToDoList = () => {
    
      return (
        <ReAssessmentToDoList
          assessmentData={reassessmentModel}
          role={ROLES.staff}
        />
      );
    
  };

  const saveData = async () => {
    try {
      const result = await assessmentDetailModel.updateAssessmentDetail(
        authContext.accessToken,
      );

      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const selectedDepartment = () => {
    if (authContext.roles.isIaFinding) {
      return reassessmentModel.filterModel.selectedDepartment.value;
    }

    if (
      authContext.roles.isIcAgent &&
      reassessmentModel.filterModel.selectedTab === 'SUMMARY'
    ) {
      return reassessmentModel.filterModel.selectedDepartment.value;
    }

    if (
      !authContext.roles.isAdmin &&
      !authContext.roles.isCompliance &&
      reassessmentModel.filterModel.selectedTab === 'SUMMARY'
    ) {
      return reassessmentModel.departmentId;
    }

    if (
      !authContext.roles.isAdmin &&
      !authContext.roles.isCompliance &&
      reassessmentModel.filterModel.selectedTab === 'TODO'
    ) {
      return reassessmentModel.departmentId;
    }

    return reassessmentModel.filterModel.selectedDepartment.value;
  };

  const buFilter = () => {
    if (
      authContext.roles.isAdmin ||
      authContext.roles.isCompliance ||
      authContext.roles.isIaFinding
    ) {
      return true;
    }

    return false;
  };

  const departmentFilter = () => {
    if (
      authContext.roles.isIcAgent &&
      reassessmentModel.filterModel.selectedTab === 'SUMMARY'
    ) {
      return true;
    }

    if (
      authContext.roles.isAdmin ||
      authContext.roles.isCompliance ||
      authContext.roles.isIaFinding
    ) {
      return true;
    }

    return false;
  };

  const resetFilter = () => {

      reassessmentModel.filterModel.resetReAssessment();

  };

  return useObserver(() => (
    <div>
      <Dimmer active={isLoading}>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Grid>
        <Grid.Row>
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={width < 768 ? sizes.xl : sizes.xxl}
            >
              Control Re Assessment
            </Text>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <div style={{ paddingTop: width <= 768 ? 8 : 32 }}>
        <Grid style={{ margin: 0 }}>
          <Grid.Row>
            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              style={{ padding: 0, marginBottom: width < 768 && 16 }}
            >
              <Button.Group
                style={{
                  width: width < 768 && '100%',
                  boxShadow: '0 0px 10px 0 rgba(48, 48, 48, 0.1)',
                  borderRadius: '6px 6px 0px 6px',
                }}
              >
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '10px 24px',
                    backgroundColor: `${
                      isOpen === 'ToDoList'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => {
                    handelOpen('ToDoList');
                    reassessmentModel.setField('tab', 'TODO');
                    reassessmentModel.filterModel.setField('selectedTab', 'TODO');
            
                      reassessmentModel.filterModel.resetAssessment();
                 
                  }}
                >
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={
                      colors.backgroundPrimary
                      
                    }
                  >
                    RE ASSESSMENT LIST
                  </Text>
                </Button>
                
              </Button.Group>
            </Grid.Column>

            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              floated="right"
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: width < 768 ? 'space-between' : 'flex-end',
                padding: width < 768 ? '8px 0px 0px' : 0,
              }}
            >
              <ButtonBorder
                textWeight="med"
                style={{ margin: '0px 24px 0px 0px' }}
                textSize={sizes.xs}
                width={width < 768 ? 56 : 156}
                options={sortBy}
                text={width < 768 ? '' : 'Advanced Filter'}
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  isShowFillter
                    ? '../../static/images/fillter-white@3x.png'
                    : '../../static/images/fillter@3x.png'
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => {
                  handelOpenFillter(!isShowFillter);               
                    reassessmentModel.filterModel.resetReAssessment();
                  if (isShowFillter) {
                    reassessmentModel.getAssessmentApprovementWithSearch(
                        authContext.accessToken,
                      );
                  }
                }}
              />
              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  reassessmentModel.setField(
                    'searchAssessmentText',
                    e.target.value,
                  );
                  if (!reassessmentModel.searchAssessmentText) {
                    reassessmentModel.getAssessmentApprovementWithSearch(
                      authContext.accessToken,
                    );
                  }
                }}
                value={reassessmentModel.searchAssessmentText}
                onEnterKeydown={(e) => {
                  if (e.keyCode === 13) {
                    reassessmentModel.getAssessmentApprovementWithSearch(
                        authContext.accessToken,
                      );
                  }
                }}
              />
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Grid style={{ margin: 0, padding: 0 }}>
                <Div between style={{ padding: 0 }}>
                  <Text fontWeight="med" fontSize={sizes.s} color="#00aeef">
                    Advanced Filter
                  </Text>
                  <div
                    className="click"
                    onClick={() => {
                      reassessmentModel.filterModel.resetReAssessment();
                      handelOpenFillter(!isShowFillter);
                      reassessmentModel.getAssessmentApprovementWithSearch(
                          authContext.accessToken,
                          1,
                        );
                    }}
                  >
                    <Image
                      src="../../static/images/x-close@3x.png"
                      style={{
                        width: 18,
                        height: 18,
                        padding: 0,
                        marginTop: -8,
                        position: 'relative',
                      }}
                    />
                  </div>
                </Div>
                <Grid.Row>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process (Level 3)
                    </Text>
                    <ProcessLvl3Dropdown
                      handleOnChange={(lvl3) => {
                        console.log(lvl3);
                        reassessmentModel.filterModel.setField(
                          'selectedLvl3',
                          lvl3,
                        );

                        reassessmentModel.filterModel.setField(
                          'selectedLvl4',
                          '',
                        );
                      }}
                      value={reassessmentModel.filterModel.selectedLvl3}
                      defaultValue="Select Process (Level 3)"
                      placeholder="Select Process (Level 3)"
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Sub-Process (Level 4)
                    </Text>
                    <ProcessLvl4Dropdown
                      handleOnChange={(lvl4) => {
                        reassessmentModel.filterModel.setField(
                          'selectedLvl4',
                          lvl4,
                        );
                      }}
                      value={reassessmentModel.filterModel.selectedLvl4}
                      disabled={!reassessmentModel.filterModel.selectedLvl3}
                      selectedLVl3={reassessmentModel.filterModel.selectedLvl3}
                      defaultValue="Select Process (Level 4)"
                      placeholder="Select Process (Level 4)"
                    />
                  </Grid.Column>
                  {buFilter() && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        BU
                      </Text>
                      <BUDropdown
                        handleOnChange={(bu) => {
                          reassessmentModel.filterModel.setField(
                            'selectedBu',
                            bu,
                          );

                          reassessmentModel.filterModel.setField(
                            'selectedDepartment',
                            '',
                          );

                          reassessmentModel.filterModel.setField(
                            'selectedDivision',
                            '',
                          );
                        }}
                        value={reassessmentModel.filterModel.selectedBu}
                        disabled={reassessmentModel.filterModel.isBuDisabled}
                        defaultValue="Select BU"
                        placeholder="Select BU"
                      />
                    </Grid.Column>
                  )}
                  {departmentFilter() && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Department
                      </Text>
                      <DepartmentDropdown
                        handleOnChange={(department) => {
                          reassessmentModel.filterModel.setField(
                            'selectedDepartment',
                            department,
                          );

                          reassessmentModel.filterModel.setField(
                            'selectedDivision',
                            '',
                          );
                        }}
                        value={
                          reassessmentModel.filterModel.selectedDepartment.value
                        }
                        disabled={
                          reassessmentModel.filterModel.isDepartmentDisabled
                        }
                        selectedBu={reassessmentModel.filterModel.selectedBu}
                        defaultValue="Select Department"
                        placeholder="Select Department"
                      />
                    </Grid.Column>
                  )}
                  {(reassessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    authContext.roles.isVP ||
                    authContext.roles.isAdmin ||
                    authContext.roles.isIaFinding) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Division
                      </Text>
                      <DivisionDropdown
                        handleOnChange={(divisionNo) => {
                          if (reassessmentModel.filterModel.resetBuMistake) {
                            reassessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          reassessmentModel.filterModel.setField(
                            'selectedDivision',
                            divisionNo,
                          );
                        }}
                        value={reassessmentModel.filterModel.selectedDivision}
                        selectedDepartmentId={selectedDepartment()}
                        defaultValue="Select Division"
                        placeholder="Select Division"
                      />
                    </Grid.Column>
                  )}
                  {(reassessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    authContext.roles.isVP ||
                    authContext.roles.isAdmin ||
                    authContext.roles.isIaFinding) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Shift
                      </Text>
                      <ShiftDropdown
                        handleOnChange={(shiftNo) => {
                          if (reassessmentModel.filterModel.resetBuMistake) {
                            reassessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          reassessmentModel.filterModel.setField(
                            'selectedShift',
                            shiftNo,
                          );
                        }}
                        value={reassessmentModel.filterModel.selectedShift}
                        selectedDepartmentId={selectedDepartment()}
                        defaultValue="Select Shift"
                        placeholder="Select Shift"
                      />
                    </Grid.Column>
                  )}
                </Grid.Row>
                <div style={{ padding: 0, display: 'flex' }}>
                  <ButtonAll
                    width={96}
                    color="#1b1464"
                    text="Filter"
                    onClick={() => {
                        getReAssessmentList();
                    }}
                  />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => {
                      if (isShowFillter) {
                          resetFilter();
                          getReAssessmentList();
                      }
                    }}
                  />
                </div>
              </Grid>
            </CardFillter>
          )}
          {reassessmentModel.assessmentList.length !== 0  ? (
            <Grid.Row style={{ padding: 0 }}>
              {renderToDoList() }
            </Grid.Row>
          ) : (
            <Grid.Row style={{ padding: 0 }}>
              <CardEmpty
                Icon="../../static/images/iconAssessment@3x.png"
                textTitle="No Assessment"
              />
            </Grid.Row>
          )}
        </Grid>
      </div>
      <BottomBarReAssessment
          isCreateRoadmapDisabled={reassessmentModel.isCreateRoadmapDisabled}
          reassessmentModel={reassessmentModel}
        />
    </div>
  ));
};

export default withLayout(observer(index));
