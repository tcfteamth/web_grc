import React, { useState, useEffect, useMemo } from 'react';
import { withLayout } from '../../hoc';
import { Image, Grid, Radio, Form, TextArea, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import Router from 'next/router';

import {
  ChartPie,
  ChartDonut,
  ProgressBarStatus,
  BottomBar,
  ButtonAll,
  Text,
  ButtonBorder,
  RadioBox,
  YearListDropdown,
  DepartmentDropdown,
  DivisionDropdown,
  BUDropdown,
  TabScrollBar,
  CsaResultReport,
  CsaCorrectiveReport,
  CsaEnhancementReport,
  CheckBoxAll,
  BottomBarReport,
  ManageBuDropdown,
  ManageDepartmentDropdown,
  AcknowledgeModal,
  ManageDivisionDropdown,
  CardEmpty,
  PublishedModal,
  DropdownSelect
} from '../../components/element';
import { colors, sizes } from '../../utils';
import {
  DetailReportModel,
  MainGraphModel,
  ManageRoleModel,
  PublishedModel,
  SaveReportModel,
  SummaryBUModel,
  SummaryDepartmentModel,
} from '../../model/Report';
import { ManageFilterListModel } from '../../model/DashboardModel';
import FilterModel from '../../model/FilterModel';
import { initAuthStore } from '../../contexts';
import request from '../../services';
import { taglistDropdownStore } from '../../model';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  align-items: center;
  justify-content: center;
`;
const TableBody = TableHeader.extend`
  padding: 0px !important;
  margin: 0px 40px;
  height: 50px !important;
  border-top: ${(props) => props.borderTop && `1px solid ${colors.grayLight}`};
  justify-content: ${(props) => (props.center ? `center` : 'space-between')};
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  background-color: ${(props) => props.color || colors.backgroundPrimary};
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.padding || 0}px;
  border-right: ${(props) => props.border && `1px solid #FFF`};
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  display: inline-block;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  min-height: 50px;
  width: 100%;
  padding: ${(props) => props.padding || 24}px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-top: ${(props) => props.top || 10}px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};

  @media only screen and (min-width: 800px) {
    width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
  }
`;

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  border-bottom: 1px solid #eaeaea;
  display: inline-block;
  padding: ${(props) => (props.border ? `0px 24px` : '24px')};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
`;

const CardTabBody = styled.div`
  border-top: 1px solid #eaeaea;
  background-color: ${colors.backgroundPrimary};
  border-radius: 0px 0px 0px 6px !important;
  min-height: 200px;
  width: 100% !important;
  padding: 24px !important;
`;

const NewCardBody = styled.div`
  background-color: ${colors.backgroundPrimary};
  width: 100%;
  border-top: 1px solid #eaeaea;
  border-radius: 6px 6px 0px 0px !important;
  padding: 24px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const LineGraph = styled.div`
  height: 18px;
  padding: 0px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${(props) => props.width && props.width}%;
  background-color: ${(props) => props.bgcolor || colors.primaryBackground};
`;

const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
  margin-right: 16px;
`;

const Oval = styled.div`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  margin-right: 8px;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
`;

const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-bottom: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px !important;
  width: 100%;
`;

const headers = [
  {
    key: 'Department',
    render: 'Department',
    color: colors.grayLight,
    width: 20,
  },
  { key: 'Good', render: 'Good', color: '#00b140', width: 15 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 15 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 15 },
  {
    key: 'Total Sub-process',
    render: 'Total Sub-process',
    color: colors.primaryBlack,
    width: 15,
  },
  // { key: 'Graph', render: 'Graph', color: colors.grayLight, width: 35 },
  {
    key: 'No. of Enhancement by Control',
    render: 'No. of Enhancement by Control',
    color: colors.textPurple,
    width: 20,
  },
];

const headersDivsion = [
  {
    key: 'Division',
    render: 'Division / Shift',
    color: colors.grayLight,
    width: 20,
  },
  { key: 'Good', render: 'Good', color: '#00b140', width: 15 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 15 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 15 },
  {
    key: 'Total Sub-process',
    render: 'Total Sub-process',
    color: colors.primaryBlack,
    width: 15,
  },
  {
    key: 'No. of Enhancement by Control',
    render: 'No. of Enhancement by Control',
    color: colors.textPurple,
    width: 20,
  },
];

const dataPieList = {
  dataPie: [30, 30, 40],
  color: [colors.green, colors.yellow, colors.red],
};

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const index = (props) => {
  const [activeCommentBox, setActiveCommentBox] = useState(false);
  const mainGraphModel = useLocalStore(() => new MainGraphModel());
  const manageFilterListModel = useLocalStore(
    () => new ManageFilterListModel(),
  );
  const summaryBUModel = useLocalStore(() => new SummaryBUModel());
  const summaryDepartmentModel = useLocalStore(
    () => new SummaryDepartmentModel(),
  );
  const filterModel = useLocalStore(() => new FilterModel());
  const manageRoleModel = useLocalStore(() => new ManageRoleModel());
  const detailReportModel = useLocalStore(() => new DetailReportModel());
  const saveReportModel = useLocalStore(() => new SaveReportModel());
  const publishedModel = useLocalStore(() => new PublishedModel());
  const authContext = initAuthStore();
  const [activeAcknowledgeModal, setActiveAcknowledgeModal] = useState(false);
  const [isGetReport, setIsGetReport] = useState(false);
  const [isFilterReport, setIsFilterReport] = useState(false);
  const [commentText, setCommentText] = useState('');
  const [acknowledgeSign, setAcknowledeSign] = useState(false);
  const [isPrintMode, setIsPrintMode] = useState(false);
  const [activePublishedModal, setActivePublishedModal] = useState(false);
  const [isYearChange, setIsYearChange] = useState(false);

  const tagListDropdown = useLocalStore(() => taglistDropdownStore);
  const [tagsList, setTagList] = useState();

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };

  const handleProcessTagSelection = (e,data,field) => {
    let result = filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    filterModel.setField(
      field,
      result,
    );
  }

  const getReportCsaData = async (year, bu, processAnd = [],processOr = [],objectives = []) => {
    try {
      await new Promise([
        mainGraphModel.getMainGraph(year, bu, processAnd, processOr, objectives),
        summaryBUModel.getSummaryBu(year, bu, processAnd, processOr, objectives),
        detailReportModel.getDetailReport(year, bu),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const getSummaryDepartment = async (year, bu, departmentNo, divisionNo , processAnd = [],processOr = [],objectives = []) => {
    try {
      await new Promise([
        summaryDepartmentModel.getSummaryDepartment(
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const getManageServices = async () => {
    try {
      await new Promise([
        manageFilterListModel.getManageFilterList(authContext.accessToken),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const saveReport = async () => {
    try {
      const saveReportData = {
        bu: filterModel.selectedBu,
        year: filterModel.selectedYear,
        toggle: detailReportModel.toggle,
        summary: detailReportModel.executiveSummary,
        comment: commentText || null,
        acknowledge: detailReportModel.acknowledge,
        acknowledgeSign,
      };

      const result = await saveReportModel.saveReport(
        authContext.accessToken,
        saveReportData,
      );

      const resultCommentDeleted = await detailReportModel.submitDeleteComment();

      const resultGetReportData = await getReportCsaData(
        filterModel.selectedYear,
        filterModel.selectedBu,
        filterModel.selectedDepartment.no,
        filterModel.selectedDivision,
        filterModel.selectedProcessAnd,
        filterModel.selectedProcessOr,
        filterModel.selectedObjectives
      );

      const resultGetReportStatus = await publishedModel.getPublishedList(
        filterModel.selectedYear,
      );

      setCommentText('');
      detailReportModel.setField('deleteCommentIds', []);
      // Router.reload();
    } catch (e) {
      console.log(e);
    }
  };

  const filterReport = async () => {
    await manageRoleModel.getManageRole(
      filterModel.selectedYear,
      filterModel.selectedBu,
    );

    await getReportCsaData(
      filterModel.selectedYear,
      filterModel.selectedBu,
      filterModel.selectedDepartment.no,
      filterModel.selectedDivision,
      filterModel.selectedProcessAnd,
      filterModel.selectedProcessOr,
      filterModel.selectedObjectives
    );
    setIsFilterReport(true);
    setIsGetReport(false);
  };

  const clearFilterReport = () => {
    if (authContext.roles.isAdmin || authContext.roles.isSEVP) {
      filterModel.resetFilterReportAdmin();
    } else if (authContext.roles.isVP) {
      filterModel.resetFilterReportVP();
    } else {
      filterModel.resetFilterReportDMAndStaff();
    }

    setIsFilterReport(false);
    setIsGetReport(false);
  };

  const isDeleteCommnetEnable = (cenDelete, id) => {
    if (cenDelete) {
      return (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
        >
          <Image
            height={18}
            width={18}
            src="../../static/images/delete-gray@3x.png"
            className="click"
            onClick={() => detailReportModel.deleteComment(id)}
          />
        </div>
      );
    }
  };

  const handleChangeAcknowledge = (e) => {
    detailReportModel.setField('acknowledge', e.target.value);
  };

  const handleChangeComment = useMemo(() => {
    return (
      <CardComment style={{ marginTop: '16px' }}>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
          className="click"
          onClick={() => {
            setActiveCommentBox(false);
            setCommentText('');
          }}
        >
          <Text
            fontSize={sizes.l}
            fontWeight="bold"
            color={colors.textPurple}
            className="upper"
          >
            Comment
          </Text>
          <Image
            src="../../static/images/close-x@3x.png"
            width={12}
            height={12}
          />
        </div>

        <Form style={{ paddingTop: 16 }}>
          <TextArea
            style={{ minHeight: 200 }}
            placeholder="Enter Comment "
            onChange={(e) => setCommentText(e.target.value)}
            value={commentText}
            maxLength={3000}
          />
        </Form>
        <div style={{ textAlign: 'right', marginTop: '8px' }}>
          <Text
            color={colors.textlightGray}
            style={{
              paddingLeft: 8,
              fontStyle: 'italic',
            }}
            fontSize={sizes.xxs}
          >
            {`${authContext.currentUser.firstNameEn} ${authContext.currentUser.lastNameEn}`}
          </Text>
        </div>
      </CardComment>
    );
  }, [commentText]);

  const isShowPublic = () => {
    if (authContext.roles.isAdmin) {
      return true;
    } else if (manageRoleModel.showPublic) {
      return true;
    } else {
      return false;
    }
  };

  const isShowAcknowledge = () => {
    if (detailReportModel.acknowledgeName) {
      return (
        <>
          <Text
            color={colors.textlightGray}
            style={{
              marginRight: 8,
              fontStyle: 'italic',
            }}
            fontSize={sizes.xs}
          >
            Signed by:
          </Text>
          <Text fontWeight="med" color={colors.primaryBlack}>
            {detailReportModel.acknowledgeName}
          </Text>
        </>
      );
    }

    if (detailReportModel.showButtonSign) {
      return (
        <>
          <div style={{ marginRight: '8px' }}>
            <Text fontWeight="med" color={colors.red} fontSize={sizes.l}>
              Please acknowledge this report by check this box
            </Text>
          </div>
          <div style={{ marginRight: '8px' }}>
            <CheckBoxAll
              onChange={() => {
                if (!acknowledgeSign) {
                  setActiveAcknowledgeModal(true);
                } else {
                  setAcknowledeSign(false);
                }
              }}
              checked={acknowledgeSign || detailReportModel.acknowledgeSign}
              disabled={detailReportModel.acknowledgeSign}
            />
          </div>
        </>
      );
    }

    return null;
  };

  const handelExport = async () => {
    try {
      const result = await detailReportModel.exportExcel(
        filterModel.selectedYear,
        filterModel.selectedBu,
        filterModel.selectedDepartment.no,
        filterModel.selectedDivision,
      );

      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  // get manage filter
  useEffect(() => {
    getManageServices();
  }, []);

  useEffect(() => {
    (async () => {
      try {
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  // get main graph
  // useEffect(() => {
  //   if (!isFilterReport) {
  //     manageRoleModel.getManageRole(
  //       filterModel.selectedYear,
  //       filterModel.selectedBu,
  //     );
  //   }
  // }, [filterModel.selectedBu, filterModel.selectedYear]);

  useEffect(() => {
    const currentYear = new Date().getFullYear();
    filterModel.setField('selectedYear', currentYear);
  }, []);

  useEffect(() => {
    (async () => {
      try {
        await publishedModel.getPublishedList(filterModel.selectedYear);
      } catch (e) {
        console.log(e);
      }
    })();
  }, [filterModel.selectedYear]);

  return useObserver(() => (
    <div>
      <div>
        <Card className="no-print">
          <Div between>
            <Text
              fontWeight={'bold'}
              fontSize={sizes.xl}
              color={colors.primaryBlack}
            >
              CSA Report
            </Text>
            {authContext.roles.isAdmin && (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <ButtonAll
                  text="Report Status"
                  onClick={() => setActivePublishedModal(true)}
                />
                {detailReportModel.enableToggle && isFilterReport && (
                  <div
                    style={{
                      marginLeft: '16px',
                      display: 'flex',
                      alignItems: 'center',
                      flexDirection: 'column',
                    }}
                  >
                    <Radio
                      toggle
                      value={detailReportModel.toggle}
                      onChange={() =>
                        detailReportModel.setField(
                          'toggle',
                          !detailReportModel.toggle,
                        )
                      }
                      checked={detailReportModel.toggle}
                    />
                    <Text>Published</Text>
                  </div>
                )}
              </div>
            )}
          </Div>

          <Div top={28}>
            {manageFilterListModel.list.length > 0 &&
              manageFilterListModel.list.map((filter) => {
                if (filter.filterName === 'year') {
                  return (
                    <Div col width={16} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Year
                      </Text>
                      <Div items={'flex-Start'}>
                        <YearListDropdown
                          placeholder="Select Year"
                          handleOnChange={(year) => {
                            filterModel.setField('selectedYear', year);
                          }}
                          value={filterModel.selectedYear}
                          disabled={filter.isDisable}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'BU') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        BU
                      </Text>
                      <Div items={'flex-Start'}>
                        <ManageBuDropdown
                          placeholder="Select BU"
                          handleOnChange={(bu) => {
                            filterModel.setField('selectedBu', bu.value);

                            filterModel.setField('selectedDepartment', '');
                            filterModel.setField('selectedDivision', '');
                          }}
                          value={filterModel.selectedBu}
                          isDefaultValue={filter.isDefaultValue}
                          disabled={
                            filter.isDisable || filterModel.isBuDisabled
                          }
                          filterModel={filterModel}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'Department') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Department
                      </Text>
                      <Div items={'flex-Start'}>
                        <ManageDepartmentDropdown
                          placeholder="Select Department"
                          handleOnChange={(department) => {
                            filterModel.setField(
                              'selectedDepartment',
                              department,
                            );

                            filterModel.setField('selectedDivision', '');
                          }}
                          value={filterModel.selectedDepartment.value}
                          selectedBu={filterModel.selectedBu}
                          isDefaultValue={filter.isDefaultValue}
                          disabled={
                            !filterModel.selectedBu ||
                            filter.isDisable ||
                            filterModel.isDepartmentDisabled
                          }
                          filterModel={filterModel}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'Division') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Divisions
                      </Text>
                      <Div items={'flex-Start'}>
                        <ManageDivisionDropdown
                          defaultValue="Select Division"
                          placeholder="Select Division"
                          value={filterModel.selectedDivision}
                          selectedDepartmentId={
                            filterModel.selectedDepartment.value
                          }
                          selectedBu={filterModel.selectedBu}
                          handleOnChange={(divisionNo) => {
                            filterModel.setField(
                              'selectedDivision',
                              divisionNo,
                            );
                          }}
                          disabled={!filterModel.selectedBu || filter.isDisable}
                        />
                      </Div>
                    </Div>
                  );
                }
              })}
            <Div col width={10} style={{ marginTop: '22px' }}>
              <Div items="flex-Start">
                <ButtonAll
                  text="Filter"
                  onClick={filterReport}
                  disabled={!filterModel.isSelectedReportFilter}
                />
                <ButtonBorder text="Clear" handelOnClick={clearFilterReport} />
              </Div>
            </Div>
          </Div>
          <Div top={28}>
            <Div col width={25} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Process Tag (AND)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Tag"
                  options={tagsList}
                  value={filterModel.selectedProcessAnd}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
                />
              </Div>
            </Div>
            <Div col width={25} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Process Tag (OR)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Tag"
                  options={tagsList}
                  value={filterModel.selectedProcessOr}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
                />
              </Div>
            </Div>
            <Div col width={26} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Objective (AND)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Objective"
                  options={objectiveList}
                  value={filterModel.selectedObjectives}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedObjectives')}
                />
              </Div>
            </Div>
          </Div>
        </Card>

        {isFilterReport && (
          <>
            {isShowPublic() ? (
              <>
                <Grid
                  style={{ margin: '16px 0px' }}
                  className="print-pagebreak pt"
                >
                  <CardTabHeader>
                    <Text
                      fontSize={sizes.l}
                      fontWeight="bold"
                      color={colors.textPurple}
                    >
                      {filterModel.selectedBu && `Executive Summary`}
                    </Text>
                  </CardTabHeader>
                  <CardTabBody>
                    <Form>
                      <TextArea
                        style={{ minHeight: 400, fontSize: sizes.m }}
                        placeholder={
                          detailReportModel.enableExecutiveSummary &&
                          'Enter Executive Summary'
                        }
                        onChange={(e) =>
                          detailReportModel.setField(
                            'executiveSummary',
                            e.target.value,
                          )
                        }
                        value={detailReportModel.executiveSummary}
                        disabled={!detailReportModel.enableExecutiveSummary}
                        maxLength={3000}
                      />
                    </Form>
                  </CardTabBody>
                </Grid>
                {manageRoleModel.showMainGraph &&
                  (mainGraphModel.GC || mainGraphModel.BU) && (
                    <Grid
                      style={{ margin: '16px 0px' }}
                      doubling
                      columns={2}
                      centered
                    >
                      <Grid.Row style={{ padding: 0 }}>
                        <CardTabHeader border>
                          <Div center padding={24}>
                            <Text
                              fontSize={sizes.l}
                              fontWeight="bold"
                              color={colors.textPurple}
                            >
                              CSA Result
                            </Text>
                          </Div>
                        </CardTabHeader>
                      </Grid.Row>
                      <Grid.Row
                        style={{ padding: 0, backgroundColor: 'white' }}
                      >
                        <Grid.Column>
                          <Text
                            style={{
                              marginBottom: 16,
                              marginTop: 16,
                              textAlign: 'center',
                            }}
                            fontSize={sizes.l}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                          >
                            Overall GC
                          </Text>
                          <Div center col>
                            <Div>
                              {mainGraphModel.GC && (
                                <ChartPie
                                  data={mainGraphModel.gcPercentList}
                                  backgroundColor={dataPieList.color}
                                />
                              )}
                            </Div>
                            <Div mid>
                              <SectionLine>
                                <Oval bgcolor={colors.green} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  GOOD
                                </Text>
                              </SectionLine>
                              <SectionLine>
                                <Oval bgcolor={colors.yellow} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  FAIR
                                </Text>
                              </SectionLine>
                              <SectionLine>
                                <Oval bgcolor={colors.red} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  POOR
                                </Text>
                              </SectionLine>
                            </Div>
                          </Div>
                        </Grid.Column>

                        <Grid.Column>
                          <Text
                            className="upper"
                            style={{
                              marginBottom: 16,
                              marginTop: 16,
                              textAlign: 'center',
                            }}
                            fontSize={sizes.l}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                          >
                            {filterModel.selectedBu}
                          </Text>
                          <Div center col>
                            <Div>
                              {mainGraphModel.BU && (
                                <ChartPie
                                  data={mainGraphModel.buPercentList}
                                  backgroundColor={dataPieList.color}
                                />
                              )}
                            </Div>
                            <Div mid>
                              <SectionLine>
                                <Oval bgcolor={colors.green} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  GOOD
                                </Text>
                              </SectionLine>
                              <SectionLine>
                                <Oval bgcolor={colors.yellow} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  FAIR
                                </Text>
                              </SectionLine>
                              <SectionLine>
                                <Oval bgcolor={colors.red} />
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={'bold'}
                                  color={colors.primaryBlack}
                                >
                                  POOR
                                </Text>
                              </SectionLine>
                            </Div>
                          </Div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  )}

                {manageRoleModel.showSummaryBu &&
                  summaryBUModel.departmentList && (
                    <Grid
                      style={{ margin: '16px 0px' }}
                      className="print-pagebreak pt"
                    >
                      <Grid.Row
                        style={{ padding: 0, backgroundColor: 'white' }}
                      >
                        <Grid.Column>
                          {/* Card Right  */}
                          <Div center col top={24} bottom={24}>
                            <Text
                              style={{ marginBottom: 24 }}
                              fontSize={sizes.l}
                              fontWeight="bold"
                              color={colors.primaryBlack}
                            >
                              CSA Rating by Sub-process
                            </Text>
                            <TableHeader span={12}>
                              {summaryBUModel.departmentList &&
                                headers.map(({ render, key, color, width }) => (
                                  <Cell
                                    center
                                    border
                                    key={key}
                                    color={color}
                                    width={width}
                                  >
                                    <Text
                                      fontSize={sizes.xxs}
                                      fontWeight="bold"
                                      color={
                                        render === 'Department' ||
                                        render === 'Graph'
                                          ? colors.primaryBlack
                                          : colors.primaryBackground
                                      }
                                    >
                                      {render}
                                    </Text>
                                  </Cell>
                                ))}
                            </TableHeader>
                            {summaryBUModel.departmentList.map((department) => (
                              <TableBody
                                span={12}
                                key={department.indicator}
                                borderTop
                              >
                                <Cell width={20}>
                                  <Popup
                                    trigger={
                                      <Text
                                        fontSize={sizes.s}
                                        color={colors.primaryBlack}
                                        fontWeight="med"
                                      >
                                        {department.indicator}
                                      </Text>
                                    }
                                    content={`${department.divEn || '-'} ${department.shiftEn || ''}`}
                                    size="small"
                                  />
                                </Cell>
                                <Cell width={15} borderTop>
                                  <Text
                                    fontSize={sizes.s}
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                  >
                                    {department.good > 0
                                      ? department.good
                                      : '-'}
                                  </Text>
                                </Cell>
                                <Cell width={15} borderTop>
                                  <Text
                                    fontSize={sizes.s}
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                  >
                                    {department.fair > 0
                                      ? department.fair
                                      : '-'}
                                  </Text>
                                </Cell>
                                <Cell width={15} borderTop>
                                  <Text
                                    fontSize={sizes.s}
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                  >
                                    {department.poor > 0
                                      ? department.poor
                                      : '-'}
                                  </Text>
                                </Cell>
                                <Cell width={15} borderTop>
                                  <Text
                                    fontSize={sizes.s}
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                  >
                                    {department.total > 0
                                      ? department.total
                                      : '-'}
                                  </Text>
                                </Cell>
                                <Cell borderTop width={20}>
                                  <Text
                                    fontSize={sizes.s}
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                  >
                                    {department.enhance > 0
                                      ? department.enhance
                                      : '-'}
                                  </Text>
                                </Cell>
                              </TableBody>
                            ))}
                          </Div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  )}

                {isGetReport &&
                  summaryDepartmentModel.departmentList &&
                  summaryDepartmentModel.departmentList.map(
                    (departmentListModel) => (
                      <>
                        <Grid
                          style={{ margin: '16px 0px' }}
                          doubling
                          columns={2}
                          className="print-pagebreak pt"
                        >
                          <Grid.Row
                            style={{ padding: 0, backgroundColor: 'white' }}
                          >
                            <Grid.Column width={4}>
                              <Div center col padding={24}>
                                <Text
                                  style={{ marginBottom: 24 }}
                                  fontSize={sizes.l}
                                  fontWeight="bold"
                                  color={colors.primaryBlack}
                                >
                                  NO. OF SUB-PROCESS
                                </Text>
                                <Div center col>
                                  <Div>
                                    {departmentListModel.department.graph && (
                                      <ChartPie
                                        data={
                                          departmentListModel.department
                                            .departmentGraphPercent
                                        }
                                        backgroundColor={dataPieList.color}
                                      />
                                    )}
                                  </Div>
                                  <Div mid>
                                    <SectionLine>
                                      <Oval bgcolor={colors.green} />
                                      <Text
                                        fontSize={sizes.xxs}
                                        fontWeight={'bold'}
                                        color={colors.primaryBlack}
                                      >
                                        GOOD
                                      </Text>
                                    </SectionLine>
                                    <SectionLine>
                                      <Oval bgcolor={colors.yellow} />
                                      <Text
                                        fontSize={sizes.xxs}
                                        fontWeight={'bold'}
                                        color={colors.primaryBlack}
                                      >
                                        FAIR
                                      </Text>
                                    </SectionLine>
                                    <SectionLine>
                                      <Oval bgcolor={colors.red} />
                                      <Text
                                        fontSize={sizes.xxs}
                                        fontWeight={'bold'}
                                        color={colors.primaryBlack}
                                      >
                                        POOR
                                      </Text>
                                    </SectionLine>
                                  </Div>
                                </Div>
                              </Div>
                            </Grid.Column>
                            <Grid.Column width={12}>
                              <Div center col top={24} bottom={24}>
                                <Text
                                  style={{ marginBottom: 24 }}
                                  fontSize={sizes.l}
                                  fontWeight="bold"
                                  color={colors.primaryBlack}
                                >
                                  CSA Rating by Sub-process
                                </Text>
                                <TableHeader span={12}>
                                  {headersDivsion.map(
                                    ({ render, key, color, width }) => (
                                      <Cell
                                        center
                                        border
                                        key={key}
                                        color={color}
                                        width={width}
                                      >
                                        <Text
                                          fontSize={sizes.xxxs}
                                          fontWeight="bold"
                                          color={
                                            render === 'Division / Shift' ||
                                            render === 'Graph'
                                              ? colors.primaryBlack
                                              : colors.primaryBackground
                                          }
                                        >
                                          {render}
                                        </Text>
                                      </Cell>
                                    ),
                                  )}
                                </TableHeader>
                                {departmentListModel.department.table &&
                                  departmentListModel.department.table.map(
                                    (dt) => (
                                      <TableBody
                                        span={12}
                                        key={dt.indicator}
                                        borderTop
                                      >
                                        <Cell width={20}>
                                          <Popup
                                            trigger={
                                              <Text
                                                fontSize={sizes.s}
                                                color={colors.primaryBlack}
                                                fontWeight="med"
                                              >
                                                {dt.indicator}
                                              </Text>
                                            }
                                            content={`${dt.divEn || '-'} ${dt.shiftEn || ''}`}
                                            size="small"
                                          />
                                        </Cell>
                                        <Cell width={15} borderTop>
                                          <Text
                                            fontSize={sizes.s}
                                            color={colors.primaryBlack}
                                            fontWeight="med"
                                          >
                                            {dt.good > 0 ? dt.good : '-'}
                                          </Text>
                                        </Cell>
                                        <Cell width={15} borderTop>
                                          <Text
                                            fontSize={sizes.s}
                                            color={colors.primaryBlack}
                                            fontWeight="med"
                                          >
                                            {dt.fair > 0 ? dt.fair : '-'}
                                          </Text>
                                        </Cell>
                                        <Cell width={15} borderTop>
                                          <Text
                                            fontSize={sizes.s}
                                            color={colors.primaryBlack}
                                            fontWeight="med"
                                          >
                                            {dt.poor > 0 ? dt.poor : '-'}
                                          </Text>
                                        </Cell>
                                        <Cell width={15} borderTop>
                                          <Text
                                            fontSize={sizes.s}
                                            color={colors.primaryBlack}
                                            fontWeight="med"
                                          >
                                            {dt.total > 0 ? dt.total : '-'}
                                          </Text>
                                        </Cell>
                                        <Cell borderTop width={20}>
                                          <Text
                                            fontSize={sizes.s}
                                            color={colors.primaryBlack}
                                            fontWeight="med"
                                          >
                                            {dt.enhance > 0 ? dt.enhance : '-'}
                                          </Text>
                                        </Cell>
                                      </TableBody>
                                    ),
                                  )}
                              </Div>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>

                        {summaryDepartmentModel.departmentList && (
                          <Grid
                            style={{ margin: '16px 0px' }}
                            className="print-pagebreak pt"
                          >
                            <Grid.Row style={{ padding: 0 }}>
                              <CsaResultReport
                                departmentListModel={departmentListModel}
                                filterModel={filterModel}
                              />
                            </Grid.Row>
                          </Grid>
                        )}

                        <CsaCorrectiveReport
                          departmentListModel={departmentListModel}
                        />

                        <CsaEnhancementReport
                          departmentListModel={departmentListModel}
                        />
                      </>
                    ),
                  )}

                <div className="print-pagebreak pt">
                  {isGetReport &&
                    detailReportModel.commentDTOS &&
                    detailReportModel.commentDTOS.map((commentDTO) => (
                      <CardComment style={{ marginTop: '16px' }}>
                        {isDeleteCommnetEnable(
                          commentDTO.canDelete,
                          commentDTO.id,
                        )}
                        <Form style={{ paddingTop: 16 }}>
                          <TextArea
                            style={{ minHeight: 100 }}
                            value={commentDTO.comment}
                            disabled
                          />
                        </Form>
                        <div
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                          }}
                        >
                          <div
                            style={{
                              display: 'flex',
                              fontStyle: 'italic',
                            }}
                          >
                            <Text
                              style={{ fontStyle: 'italic' }}
                              color={colors.textlightGray}
                              fontSize={sizes.xs}
                            >
                              By{' '}
                            </Text>
                            <Text
                              color={colors.textlightGray}
                              style={{
                                paddingLeft: 8,
                                fontStyle: 'italic',
                              }}
                              fontSize={sizes.xxs}
                            >
                              {`${commentDTO.name} - ${commentDTO.role}`}
                            </Text>
                          </div>
                        </div>
                      </CardComment>
                    ))}
                </div>

                {isGetReport && activeCommentBox && handleChangeComment}

                {isGetReport &&
                  !activeCommentBox &&
                  detailReportModel.enableComment && (
                    <Grid className="no-print">
                      <Grid.Row centered>
                        <ButtonBorder
                          width={230}
                          height={40}
                          fontSize={sizes.s}
                          textColor={colors.textSky}
                          borderColor={colors.textSky}
                          textUpper
                          textWeight={'med'}
                          text="Add Comment"
                          handelOnClick={() => setActiveCommentBox(true)}
                        />
                      </Grid.Row>
                    </Grid>
                  )}

                {isGetReport && (
                  <Grid
                    style={{ margin: '16px 0px' }}
                    className="print-pagebreak"
                  >
                    <Grid.Row>
                      <NewCardBody>
                        <Text
                          fontSize={sizes.l}
                          fontWeight="bold"
                          color={colors.textPurple}
                        >
                          Acknowledgment
                        </Text>
                        <Form style={{ paddingTop: 16 }}>
                          <TextArea
                            placeholder="Enter Acknowledgment"
                            style={{ minHeight: 200, fontSize: sizes.m }}
                            disabled={
                              !detailReportModel.enableAcknowledgment ||
                              detailReportModel.acknowledgeSign
                            }
                            onChange={handleChangeAcknowledge}
                            value={detailReportModel.acknowledge}
                            maxLength={3000}
                          />
                        </Form>
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'flex-end',
                            paddingTop: 16,
                          }}
                        >
                          {isShowAcknowledge()}
                        </div>
                      </NewCardBody>
                    </Grid.Row>
                  </Grid>
                )}

                {!isGetReport && (
                  <Grid className="no-print">
                    <Grid.Row centered>
                      <ButtonBorder
                        width={230}
                        height={40}
                        fontSize={sizes.s}
                        textColor={colors.textSky}
                        borderColor={colors.textSky}
                        textUpper
                        textWeight={'med'}
                        text="Get detail report and submit acknowledgement"
                        handelOnClick={() => {
                          getSummaryDepartment(
                            filterModel.selectedYear,
                            filterModel.selectedBu,
                            filterModel.selectedDepartment.no,
                            filterModel.selectedDivision,
                            filterModel.selectedProcessAnd,
                            filterModel.selectedProcessOr,
                            filterModel.selectedObjectives
                          );
                          setIsGetReport(true);
                        }}
                      />
                    </Grid.Row>
                  </Grid>
                )}
              </>
            ) : (
              <Grid.Row style={{ marginTop: '24px' }} className="no-print">
                <CardEmpty textTitle="No Report Data" />
              </Grid.Row>
            )}
          </>
        )}

        <AcknowledgeModal
          active={activeAcknowledgeModal}
          onClose={() => setActiveAcknowledgeModal(false)}
          onSubmit={() => {
            setAcknowledeSign(true);
            setActiveAcknowledgeModal(false);
          }}
        />

        <PublishedModal
          active={activePublishedModal}
          onClose={() => setActivePublishedModal(false)}
          publishedModel={publishedModel}
        />

        {/* <div style={{ marginTop: '32px'}}> */}
        <BottomBarReport
          onSaveReport={saveReport}
          detailReportModel={detailReportModel}
          manageRoleModel={manageRoleModel}
          filterModel={filterModel}
          isFilterReport={isFilterReport}
          setIsPrintMode={setIsPrintMode}
          reportEx={handelExport}
        />
        {/* </div> */}
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
