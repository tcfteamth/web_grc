import React, { useState, useEffect } from 'react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { Image, Grid, Button, Header } from 'semantic-ui-react';
import styled from 'styled-components';
import _, { result } from 'lodash';
import { colors, sizes } from '../../utils';
import {
  ButtonAll,
  Text,
  DropdownGroup,
  YearDropdown,
  ButtonBorder,
  SearchInput,
  Box,
  StatusDropdown,
  DivisionDropdown,
  ShiftDropdown,
  DepartmentDropdown,
  ProcessLvl2Dropdown,
  BUDropdown,
  ComponentAreaDropdown,
  OwenerTaskDropdown,
  RoadmapTypeDropdown,
} from '../../components/element';
import {
  AssessmentTodoListDM,
  AssessmentTodoListVP,
  AssessmentSummaryList,
  AssessmentTodoList,
  AssessmentApproveListAdmin,
} from '../../components/COSOAssessment';

import { withLayout } from '../../hoc';
import {
  AssessmentCOSOModel,
  AssessmentSummaryCOSOModel,
} from '../../model/COSO';
import { initAuthStore } from '../../contexts';
import { apiGatewayInstance } from '../../helpers/utils';
import useWindowDimensions from '../../hook/useWindowDimensions';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const sortBy = [
  { text: 'text 1', value: 0 },
  { text: 'text 2', value: 1 },
  { text: 'text 3', value: 2 },
  { text: 'text 4', value: 3 },
];

const index = () => {
  const authContext = initAuthStore();
  const [isOpen, setIsOpen] = useState('ToDoList');
  const [isShowFillter, setIsShowFillter] = useState(false);
  const { width: screenWidth } = useWindowDimensions();
  const assessmentSummaryModel = useLocalStore(
    () => new AssessmentSummaryCOSOModel(),
  );
  const assessmentModel = useLocalStore(() => new AssessmentCOSOModel());

  useEffect(() => {
    assessmentModel.getAssessmentCOSOTodoList(authContext.accessToken);
    assessmentModel.getAssessmentSummary(authContext.accessToken);
  }, []);

  useEffect(() => {
    assessmentModel.filterModel.setField(
      'selectedIsYear',
      new Date().getFullYear(),
    );
  }, []);

  const handelOpen = (value) => {
    setIsOpen(value);
  };

  const resultTodoList = (page = 1) => {
    assessmentModel.getAssessmentCOSOTodoList(
      authContext.accessToken,
      1,
      assessmentModel.searchAssessmentText,
      assessmentModel.filterModel.selectedLvl2,
      assessmentModel.filterModel.selectedRoadmapType,
      assessmentModel.filterModel.selectedBu,
      assessmentModel.filterModel.selectedDepartment.no,
      assessmentModel.filterModel.selectedDivision,
      assessmentModel.filterModel.selectedShift,
      assessmentModel.filterModel.selectedStatus,
      assessmentModel.filterModel.selectedArea,
      assessmentModel.filterModel.selectedIsYear,
      assessmentModel.filterModel.selectedOwner,
    );
  };

  const resultSummary = (page = 1) => {
    assessmentModel.getAssessmentSummary(
      authContext.accessToken,
      page,
      assessmentModel.searchAssessmentText,
      assessmentModel.filterModel.selectedLvl2,
      assessmentModel.filterModel.selectedRoadmapType,
      assessmentModel.filterModel.selectedBu,
      assessmentModel.filterModel.selectedDepartment.no,
      assessmentModel.filterModel.selectedDivision,
      assessmentModel.filterModel.selectedShift,
      assessmentModel.filterModel.selectedStatus,
      assessmentModel.filterModel.selectedArea,
      assessmentModel.filterModel.selectedIsYear,
      assessmentModel.filterModel.selectedOwner,
    );
  };

  const renderToDoList = () => {
    if (authContext.roles.isAdmin) {
      return <AssessmentApproveListAdmin assessmentData={assessmentModel} />;
    } else if (authContext.roles.isDM) {
      return <AssessmentTodoListDM assessmentData={assessmentModel} />;
    } else if (authContext.roles.isVP || authContext.roles.isEVP) {
      return <AssessmentTodoListVP assessmentData={assessmentModel} />;
    } else {
      return <AssessmentTodoList assessmentData={assessmentModel} />;
    }
  };

  const renderSummary = () => (
    <AssessmentSummaryList
      assessmentData={assessmentModel}
      tab="SUMMARY"
      resultSummary={resultSummary}
    />
  );

  return useObserver(() => (
    <div>
      <Box horizontal="row" spaceBetween>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              COSO Assessment
            </Text>
          </Header.Content>
        </Header>
      </Box>
      <div style={{ paddingTop: screenWidth < 768 ? 0 : 32 }}>
        <Grid style={{ margin: 0 }}>
          <Grid.Row>
            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              style={{ padding: 0, marginBottom: screenWidth < 768 && 16 }}
            >
              <Button.Group
                style={{
                  width: screenWidth < 768 && '100%',
                  boxShadow: '0 0px 10px 0 rgba(48, 48, 48, 0.1)',
                  borderRadius: '6px 6px 0px 6px',
                }}
              >
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '10px 24px',
                    backgroundColor: `${
                      isOpen === 'ToDoList'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => {
                    handelOpen('ToDoList');
                    assessmentModel.setField('tab', 'TODO');
                    assessmentModel.filterModel.setField('selectedTab', 'TODO');
                    assessmentModel.filterModel.resetSummaryAssessment();
                    assessmentModel.filterModel.resetTodoListAssessment();
                  }}
                >
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={
                      isOpen === 'ToDoList'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    TO DO LIST
                  </Text>
                </Button>
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '10px 24px',
                    backgroundColor: `${
                      isOpen === 'Summary'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => {
                    handelOpen('Summary');
                    assessmentModel.setField('tab', 'SUMMARY');
                    assessmentModel.filterModel.setField(
                      'selectedTab',
                      'SUMMARY',
                    );
                    assessmentModel.filterModel.resetSummaryAssessment();
                    assessmentModel.filterModel.resetTodoListAssessment();
                  }}
                >
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={
                      isOpen === 'Summary'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    SUMMARY
                  </Text>
                </Button>
              </Button.Group>
            </Grid.Column>
            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: screenWidth < 768 ? 'flex-start' : 'flex-end',
                padding: 0,
              }}
            >
              <ButtonBorder
                textWeight="med"
                textSize={sizes.xs}
                width={screenWidth < 768 ? 80 : 156}
                options={sortBy}
                text="Advanced Filter"
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  screenWidth >= 768
                    ? isShowFillter
                      ? '../../static/images/fillter-white@3x.png'
                      : '../../static/images/fillter@3x.png'
                    : ''
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => {
                  setIsShowFillter(!isShowFillter);
                  assessmentModel.filterModel.resetSummaryAssessment();
                  assessmentModel.filterModel.resetTodoListAssessment();
                }}
              />

              <SearchInput
                style={{ paddingLeft: 16 }}
                placeholder="Search"
                onChange={(e) => {
                  assessmentModel.setSearchAssessmentText(e.target.value);
                  if (!assessmentModel.searchAssessmentText) {
                    if (isOpen === 'ToDoList') {
                      assessmentModel.getAssessmentCOSOTodoList(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                      );
                    } else {
                      assessmentModel.getAssessmentSummary(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                      );
                    }
                  }
                }}
                onEnterKeydown={(e) => {
                  // keycode 13 คือ ปุ่ม enter
                  if (e.keyCode === 13) {
                    if (isOpen === 'ToDoList') {
                      assessmentModel.getAssessmentCOSOTodoList(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                      );
                    } else {
                      assessmentModel.getAssessmentSummary(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                      );
                    }
                  }
                }}
                width={300}
                value={assessmentModel.searchAssessmentText}
              />
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Box
                horizontal="row"
                alignCenter
                spaceBetween
                style={{ padding: 0 }}
              >
                <Text fontWeight="med" fontSize={sizes.s} color="#00aeef">
                  Advanced Filter
                </Text>
                <div>
                  <Image
                    className="click"
                    onClick={() => {
                      setIsShowFillter(!isShowFillter);
                      assessmentModel.filterModel.resetSummaryAssessment();
                      assessmentModel.filterModel.resetTodoListAssessment();
                    }}
                    src="../../static/images/x-close@3x.png"
                    style={{
                      width: 18,
                      height: 18,
                      padding: 0,
                      marginTop: -8,
                      position: 'relative',
                    }}
                  />
                </div>
              </Box>
              <Grid
                style={{ paddingLeft: 16, paddingRight: 8, paddingBottom: 16 }}
              >
                <Grid.Row>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 2 }}
                    >
                      Year
                    </Text>
                    <YearDropdown
                      placeholder="Select Year"
                      handleOnChange={(year) => {
                        assessmentModel.filterModel.setField(
                          'selectedIsYear',
                          year,
                        );
                      }}
                      value={assessmentModel.filterModel.selectedIsYear}
                      model={[assessmentModel]}
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 2 }}
                    >
                      Question
                    </Text>
                    <ProcessLvl2Dropdown
                      handleOnChange={(lvl2) => {
                        assessmentModel.filterModel.setField(
                          'selectedLvl2',
                          lvl2,
                        );
                      }}
                      value={assessmentModel.filterModel.selectedLvl2}
                      defaultValue="Select Question"
                      placeholder="Select Question"
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 2 }}
                    >
                      Component Area
                    </Text>
                    <ComponentAreaDropdown
                      handleOnChange={(status) => {
                        assessmentModel.filterModel.setField(
                          'selectedArea',
                          status,
                        );
                      }}
                      type="ROADMAP"
                      value={assessmentModel.filterModel.selectedArea}
                      defaultValue="Select Status"
                      placeholder="Select Status"
                    />
                  </Grid.Column>

                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Roadmap
                    </Text>
                    <RoadmapTypeDropdown
                      handleOnChange={(type) => {
                        assessmentModel.filterModel.setField(
                          'selectedRoadmapType',
                          type,
                        );
                      }}
                      value={assessmentModel.filterModel.selectedRoadmapType}
                      defaultValue="Select Roadmap "
                      placeholder="Select Roadmap "
                    />
                  </Grid.Column>

                  {(authContext.roles.isAdmin ) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 2 }}
                      >
                        BU
                      </Text>
                      <BUDropdown
                        handleOnChange={(bu) => {
                          assessmentModel.filterModel.setField(
                            'selectedBu',
                            bu,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedBu}
                        disabled={assessmentModel.filterModel.isBuDisabled}
                        defaultValue="Select BU"
                        placeholder="Select BU"
                      />
                    </Grid.Column>
                  )}

                  {(authContext.roles.isAdmin )&& (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 2 }}
                      >
                        Department
                      </Text>
                      <DepartmentDropdown
                        handleOnChange={(department) => {
                          assessmentModel.filterModel.setField(
                            'selectedDepartment',
                            department,
                          );
                        }}
                        value={
                          assessmentModel.filterModel.selectedDepartment.value
                        }
                        disabled={
                          assessmentModel.filterModel.isDepartmentDisabled
                        }
                        selectedBu={assessmentModel.filterModel.selectedBu}
                        defaultValue="Select Department"
                        placeholder="Select Department"
                      />
                    </Grid.Column>
                  )}

                  {(authContext.roles.isAdmin )? (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 2 }}
                      >
                        Division
                      </Text>
                      <DivisionDropdown
                        handleOnChange={(divisionNo) => {
                          if (assessmentModel.filterModel.resetBuMistake) {
                            assessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          assessmentModel.filterModel.setField(
                            'selectedDivision',
                            divisionNo,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedDivision}
                        selectedDepartmentId={
                          assessmentModel.filterModel.selectedDepartment.value
                        }
                        defaultValue="Select Division"
                        placeholder="Select Division"
                      />
                    </Grid.Column>
                  ) : (
                    <>
                      {assessmentModel.filterModel.selectedTab ===
                        'SUMMARY' && (
                        <Grid.Column
                          tablet={8}
                          computer={4}
                          mobile={16}
                          style={{ paddingLeft: 0, marginTop: 6 }}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                            style={{ paddingBottom: 2 }}
                          >
                            Division
                          </Text>
                          <DivisionDropdown
                            handleOnChange={(divisionNo) => {
                              if (assessmentModel.filterModel.resetBuMistake) {
                                assessmentModel.filterModel.setField(
                                  'selectedBu',
                                  '',
                                );
                              }
                              assessmentModel.filterModel.setField(
                                'selectedDivision',
                                divisionNo,
                              );
                            }}
                            value={assessmentModel.filterModel.selectedDivision}
                            selectedDepartmentId={
                              assessmentModel.filterModel.selectedDepartment
                                .value
                            }
                            defaultValue="Select Division"
                            placeholder="Select Division"
                          />
                        </Grid.Column>
                      )}
                    </>
                  )}
                  {(authContext.roles.isAdmin ) ? (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 2 }}
                      >
                        Shift
                      </Text>
                      <ShiftDropdown
                        handleOnChange={(shiftNo) => {
                          if (assessmentModel.filterModel.resetBuMistake) {
                            assessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          assessmentModel.filterModel.setField(
                            'selectedShift',
                            shiftNo,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedShift}
                        selectedDepartmentId={
                          assessmentModel.filterModel.selectedDepartment.value
                        }
                        defaultValue="Select Shift"
                        placeholder="Select Shift"
                      />
                    </Grid.Column>
                  ) : (
                    <>
                      {assessmentModel.filterModel.selectedTab ===
                        'SUMMARY' && (
                        <Grid.Column
                          tablet={8}
                          computer={4}
                          mobile={16}
                          style={{ paddingLeft: 0, marginTop: 6 }}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                            style={{ paddingBottom: 2 }}
                          >
                            Shift
                          </Text>
                          <ShiftDropdown
                            handleOnChange={(shiftNo) => {
                              if (assessmentModel.filterModel.resetBuMistake) {
                                assessmentModel.filterModel.setField(
                                  'selectedBu',
                                  '',
                                );
                              }
                              assessmentModel.filterModel.setField(
                                'selectedShift',
                                shiftNo,
                              );
                            }}
                            value={assessmentModel.filterModel.selectedShift}
                            selectedDepartmentId={
                              assessmentModel.filterModel.selectedDepartment
                                .value
                            }
                            defaultValue="Select Shift"
                            placeholder="Select Shift"
                          />
                        </Grid.Column>
                      )}
                    </>
                  )}

                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 2 }}
                    >
                      Status
                    </Text>
                    <StatusDropdown
                      handleOnChange={(status) => {
                        assessmentModel.filterModel.setField(
                          'selectedStatus',
                          status,
                        );
                      }}
                      tab={assessmentModel.filterModel.selectedTab}
                      type="ASSESSMENT"
                      value={assessmentModel.filterModel.selectedStatus}
                      defaultValue="Select Roadmap"
                      placeholder="Select Roadmap"
                    />
                  </Grid.Column>

                  {assessmentModel.filterModel.selectedTab === 'SUMMARY' && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 2 }}
                      >
                        Task owner
                      </Text>
                      <OwenerTaskDropdown
                        handleOnChange={(owner) => {
                          assessmentModel.filterModel.setField(
                            'selectedOwner',
                            owner,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedOwner}
                        defaultValue="Select Task owner"
                        placeholder="Select Task owner"
                      />
                    </Grid.Column>
                  )}
                </Grid.Row>

                <div style={{ padding: 0, display: 'flex' }}>
                  <ButtonAll
                    width={96}
                    textSize={sizes.xs}
                    color="#1b1464"
                    text="Filter"
                    onClick={() => {
                      if (isOpen === 'ToDoList') {
                        resultTodoList();
                      } else {
                        resultSummary();
                      }
                    }}
                  />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => {
                      if (isShowFillter) {
                        if (isOpen === 'ToDoList') {
                          assessmentModel.filterModel.resetSummaryAssessment();
                          assessmentModel.filterModel.resetTodoListAssessment();
                          assessmentModel.getAssessmentCOSOTodoList(
                            authContext.accessToken,
                            1,
                            assessmentModel.searchAssessmentText &&
                              assessmentModel.searchAssessmentText,
                          );
                        } else {
                          assessmentModel.filterModel.resetTodoListAssessment();
                          assessmentModel.filterModel.resetSummaryAssessment();
                          assessmentModel.getAssessmentSummary(
                            authContext.accessToken,
                            1,
                            assessmentModel.searchAssessmentText &&
                              assessmentModel.searchAssessmentText,
                          );
                        }
                      }
                    }}
                  />
                </div>
              </Grid>
            </CardFillter>
          )}

          <Grid.Row style={{ padding: 0 }}>
            {isOpen === 'ToDoList' ? renderToDoList() : renderSummary()}
          </Grid.Row>
        </Grid>
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
