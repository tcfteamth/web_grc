import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Grid, Loader, Dimmer, Segment } from "semantic-ui-react";
import Link from "next/link";
import { Text ,Box} from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListAssess, dataListFollow } from "../../utils/static";
import styled from "styled-components";
import _ from "lodash";


const index = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {}, []);

  return (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>

      <Box center alignCenter>
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          Assessment Coming soon
        </Text>
      </Box>
    </div>
  );
};

export default withLayout(observer(index));
