import React, { useState, useEffect } from 'react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import Files from 'react-files';
import Router from 'next/router';
import styled from 'styled-components';
import moment from 'moment';
import { Image, Grid, Popup, TextArea, Form } from 'semantic-ui-react';
import {
  Text,
  BottomBarCosoAssessment,
  Box,
  StatusAll,
  ModalGlobal,
  ButtonAdd,
  RadioBox,
  InputAll,
  ButtonAll,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { withLayout } from '../../hoc';
import { AttachFilesModal, DocumentStorageModal } from '../../components/COSOAssessment/modal';
import { AssessmentDetailCOSOModel } from '../../model/COSO';
import { initAuthStore } from '../../contexts';
import {
  APPROVE_ASSESSMENT_READY,
  APPROVE_ASSESSMENT_STATUS,
} from '../../model/COSO/Assessment/ApproveAssessmentCOSOModel';
import { SaveModal } from '../../components/AssessmentPage/modal';

const customStyle = {
  styleAddFile: {
    height: 38,
    marginLeft: 16,
    minWidth: 100,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
  styleTextArea: {
    borderRadius: `6px 6px 0px 6px`,
    padding: 8,
    height: 'auto',
    minHeight: 200,
    border: `1px solid ${colors.black}`,
  },
  styleError: {
    borderRadius: `6px 6px 0px 6px`,
    padding: 8,
    minHeight: 200,
    border: `1px solid ${colors.red}`,
  },
};

const DividerLine = styled.div`
  height: 1px;
  width: 100%;
  margin: 16px 0px;
  background-color: ${colors.btGray};
`;

const Card = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-bottom: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px !important;
  width: 100%;
`;

const index = (props) => {
  const authContext = initAuthStore();
  const [saveModal, setSaveModal] = useState(false);
  const [activeDocumentStorageModal, setActiveDocumentStorageModal] = useState(false);
  const assessmentDetailModel = useLocalStore(
    () => new AssessmentDetailCOSOModel(),
  );
  const [confirm, setConfirm] = useState(false);
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [errorMessage, setErrorMessage] = useState();

  useEffect(() => {
    if (Router.query.Role === 'VP') {
      assessmentDetailModel.setField('role', 'VP');
    }

    assessmentDetailModel.setField('tab', Router.query.tab);

    console.log('Router.query.tab', Router.query.tab);

    assessmentDetailModel.getAssessmentComment(
      authContext.accessToken,
      Router.query.id,
      Router.query.tab,
    );
  }, [Router.query]);

  useEffect(() => {
    assessmentDetailModel.getAssessmentDetail(
      authContext.accessToken,
      Router.query.id,
      Router.query.tab
    );
  }, []);

  const deleteComment = async (id) => {
    if (id) {
      try {
        const result = await assessmentDetailModel.submitDeleteComment(
          authContext.accessToken,
          id,
        );
        Router.reload();
        return result;
      } catch (e) {
        console.log(e);
      }
    }
  };

  const saveData = async () => {
    try {
      const result = await assessmentDetailModel.updateAssessmentDetail(
        authContext.accessToken,
        assessmentDetailModel.tab,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handelCallBack = () => {
    setTimeout(() => {
      Router.back();
      setConfirm(false);
    }, 500);
  };

  const handleSaveStorage = () => {
    assessmentDetailModel.saveSelectedFiles();
    setActiveDocumentStorageModal(false);
  };

  useEffect(() => {
    // if (!assessmentDetailModel.canCheckReadyToSubmit) {
    //   assessmentDetailModel.setField('isReadyToSubmit', false);
    // }
  }, [assessmentDetailModel.isSetAllData]);

  return useObserver(() => (
    <div>
      <div
        className="click"
        style={{ width: '10%', display: 'flex' }}
        onClick={() => setConfirm(true)}
      >
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text color={colors.primaryBlack} fontWeight="bold" fontSize={sizes.xl}>
          Back
        </Text>
      </div>
      <div style={{ width: '100%', marginTop: 32 }}>
        <Grid.Row>
          <Card>
            <div>
              <Box horizontal="row" spaceBetween>
                <Box horizontal="row">
                  <Text
                    style={{ marginRight: 16 }}
                    fontSize={sizes.xs}
                    color={colors.textlightGray}
                  >
                    Internal Control Evaluation for 56-1 of 2020
                  </Text>
                  <div>
                    <StatusAll
                      child={{ status: assessmentDetailModel.status }}
                    />
                  </div>
                </Box>
                <Box horizontal="row" spaceBetween>
                  <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                    {assessmentDetailModel.area}
                  </Text>
                  <div
                    style={{
                      margin: '0px 8px',
                      height: 20,
                      width: 1,
                      backgroundColor: colors.btGray,
                    }}
                  />
                  <Text color={colors.textlightGray} fontSize={sizes.xs}>
                    <span style={{ paddingRight: 8 }}>Last Update :</span>
                    {moment(assessmentDetailModel.updateDate)
                      .locale('th')
                      .format('L')}
                  </Text>
                </Box>
              </Box>
              <Text
                style={{ lineHeight: 1.5, paddingTop: 24 }}
                fontWeight="bold"
                fontSize={sizes.xl}
                color={colors.primaryBlack}
              >
                {`${assessmentDetailModel.no} ${assessmentDetailModel.name}`}
              </Text>
              <Box>
                {assessmentDetailModel.objects.map((item) => (
                  <>
                    {item.type === 'COSO' && (
                      <Box horizontal="row">
                        <Text
                          style={{ paddingLeft: 32, paddingTop: 16 }}
                          fontWeight="med"
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          {`${item.no} ${item.name}`}
                        </Text>
                      </Box>
                    )}
                  </>
                ))}
              </Box>
            </div>
            <DividerLine />
            <div>
              <Grid>
                <Grid.Row columns={4}>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Assessor
                    </Text>

                    <Box horizontal="row">
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      <div>
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info
                            ? assessmentDetailModel.info.worker.name
                            : '-'}
                        </Text>
                        {assessmentDetailModel.info && (
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info.worker.position}
                          </Text>
                        )}
                        <div style={{ display: 'flex' }}>
                          <Image
                            src="../../static/images/iconDate@3x.png"
                            style={{
                              width: 18,
                              height: 18,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              moment(
                                assessmentDetailModel.info.worker.date,
                              ).format('DD/MM/YYYY')}
                          </Text>
                        </div>
                      </div>
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Reviewer
                    </Text>
                    <Box horizontal="row">
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info &&
                      assessmentDetailModel.info.reviewer.name ? (
                        <div>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.name}
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.position}
                          </Text>
                          <div style={{ display: 'flex' }}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                width: 18,
                                height: 18,
                                marginRight: 8,
                                marginTop: 2,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                                moment(
                                  assessmentDetailModel.info.reviewer.date,
                                ).format('DD/MM/YYYY')}
                            </Text>
                          </div>
                        </div>
                      ) : (
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          -
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Approver
                    </Text>

                    <Box horizontal="row">
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info &&
                      assessmentDetailModel.info.approver.name ? (
                        <div>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.name}
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.position}
                          </Text>
                          <div style={{ display: 'flex' }}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                width: 18,
                                height: 18,
                                marginRight: 8,
                                marginTop: 2,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                                moment(
                                  assessmentDetailModel.info.approver.date,
                                ).format('DD/MM/YYYY')}
                            </Text>
                          </div>
                        </div>
                      ) : (
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          -
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Division / Shift
                    </Text>
                    <Box horizontal="row" alignCenter>
                      <Image
                        src="../../static/images/iconWork@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info && (
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info.indicator}
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </Card>
        </Grid.Row>

        <Grid.Row style={{ marginTop: 16 }}>
          <Card>
            <Box horizontal="row" spaceBetween>
              <Box horizontal="row">
                <div style={{ display: 'flex' }}>
                  <Text
                    color={colors.primaryBlack}
                    fontSize={sizes.xxs}
                    fontWeight="med"
                    className="upper"
                  >
                    Approach
                  </Text>
                  <Text color={colors.red}> * </Text>
                </div>
                <Popup
                  size="huge"
                  position="top left"
                  style={{
                    borderRadius: '6px 6px 0px 6px',
                    marginLeft: -16,
                    marginTop: 16,
                  }}
                  trigger={
                    <Image
                      className="click"
                      src="../../static/images/iconRemark@3x.png"
                      style={{ marginLeft: 8, width: 18, height: 18 }}
                    />
                  }
                  content={
                    <Text color={colors.primaryBlack} fontSize={sizes.s}>
                      Suitable methods for achieving the results of the question
                    </Text>
                  }
                />
              </Box>
              <Box>
                <Text
                  color={colors.textlightGray}
                  fontSize={sizes.xxs}
                  style={{ fontStyle: 'italic' }}
                >
                  {assessmentDetailModel.lastEditBy &&
                    `Edit by  ${assessmentDetailModel.lastEditBy}`}
                </Text>
              </Box>
            </Box>
            {assessmentDetailModel.role !== 'VP' ? (
              <Form>
                <TextArea
                  style={
                    !assessmentDetailModel.validateCOSO.isSetApproach
                      ? customStyle.styleTextArea
                      : customStyle.styleError
                  }
                  placeholder="Key Approach"
                  value={assessmentDetailModel.approach}
                  onChange={(e) =>
                    assessmentDetailModel.setField('approach', e.target.value)
                  }
                  disabled={!assessmentDetailModel.canUpdate.wording}
                />
              </Form>
            ) : (
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.newInitiative}
              </Text>
            )}
          </Card>
        </Grid.Row>

        <Grid.Row style={{ marginTop: 16 }}>
          <Card>
            <Box horizontal="row">
              <div style={{ display: 'flex' }}>
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  Deploy
                </Text>
                <Text color={colors.red}> * </Text>
              </div>
              <Popup
                size="huge"
                position="top left"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  marginLeft: -16,
                  marginTop: 16,
                }}
                trigger={
                  <Image
                    className="click"
                    src="../../static/images/iconRemark@3x.png"
                    style={{ marginLeft: 8, width: 18, height: 18 }}
                  />
                }
                content={
                  <Text color={colors.primaryBlack} fontSize={sizes.s}>
                    How to regularly apply the methods throughout the
                    organization
                  </Text>
                }
              />
            </Box>
            {assessmentDetailModel.role !== 'VP' ? (
              <Form>
                <TextArea
                  style={
                    !assessmentDetailModel.validateCOSO.isSetDeploy
                      ? customStyle.styleTextArea
                      : customStyle.styleError
                  }
                  placeholder="Key Deploy"
                  value={assessmentDetailModel.deploy}
                  onChange={(e) =>
                    assessmentDetailModel.setField('deploy', e.target.value)
                  }
                  disabled={!assessmentDetailModel.canUpdate.wording}
                />
              </Form>
            ) : (
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.newInitiative}
              </Text>
            )}
          </Card>
        </Grid.Row>

        {/* Role support  and owner */}
        <Grid.Row style={{ marginTop: 16 }}>
          <Card>
            <Box horizontal="row">
              <div style={{ display: 'flex' }}>
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  New Initiative
                </Text>
                {/* <Text color={colors.red}> * </Text> */}
              </div>

              <Popup
                size="huge"
                position="top left"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  marginLeft: -16,
                  marginTop: 16,
                }}
                trigger={
                  <Image
                    className="click"
                    src="../../static/images/iconRemark@3x.png"
                    style={{ marginLeft: 8, width: 18, height: 18 }}
                  />
                }
                content={
                  <Text color={colors.primaryBlack} fontSize={sizes.s}>
                    Identify
                    <strong style={{ paddingRight: 4, paddingLeft: 4 }}>
                      significant changes or developments
                    </strong>
                    that occur in work processes or internal control system in
                    this year
                  </Text>
                }
              />
            </Box>
            {assessmentDetailModel.role !== 'VP' ? (
              <Form>
                <TextArea
                  style={customStyle.styleTextArea}
                  placeholder="Key New Initiative "
                  value={assessmentDetailModel.newInitiative}
                  onChange={(e) =>
                    assessmentDetailModel.setField(
                      'newInitiative',
                      e.target.value,
                    )
                  }
                  disabled={!assessmentDetailModel.canUpdate.wording}
                />
              </Form>
            ) : (
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.newInitiative}
              </Text>
            )}
          </Card>
        </Grid.Row>

        <Grid.Row style={{ marginTop: 16 }}>
          <Card style={ 
            !assessmentDetailModel.validateCOSO.isEvidence && assessmentDetailModel.validateCOSO.isSetEvidence ?
              { border: '1px solid red' } : {}} >
              <div style={{ display: 'flex' }}>
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  Document / Evidence
                </Text>
                <Text color={colors.red}> * </Text>
              </div>
            {assessmentDetailModel.role !== 'VP' ? (
              <Box padding={24} horizontal="row" style={{ paddingTop: 8}}>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                  }}
                >
                  {assessmentDetailModel.displayFiles.map((file, index) => (
                    <div style={{ paddingRight: 8 }}>
                      <ButtonAdd
                        text={`${file.title || file.name}`}
                        textSize={sizes.xs}
                        href={file.filePath || false}
                        onClickRemove={() =>
                          assessmentDetailModel.deleteAssessmentFiles(
                            file.id,
                            file.filesType,
                          )
                        }
                      />
                    </div>
                  ))}
                  <ButtonAll
                    width={100}
                    height={32}
                    onClick={() => { 
                      assessmentDetailModel.initTempFiles()
                      setActiveDocumentStorageModal(true)
                    }}
                    color={colors.primary}
                    textColor={colors.backgroundPrimary}
                    textUpper
                    textWeight="med"
                    text="Document Storage"
                    disabled={!assessmentDetailModel.canUpdate.wording}
                  />
                </div>
              </Box>
            ) : (
              <Box horizontal="row" alignCenter style={{ paddingTop: 8 }}>
                {assessmentDetailModel.displayFiles.map((file, index) => (
                  <div style={{ marginTop: '-2px' }}>
                    <ButtonAdd
                      text={`${file.title || file.name}`}
                      textSize={sizes.xs}
                      href={file.fullPath || file.filePath}
                      disabled
                    />
                  </div>
                ))}
              </Box>
            )}
          </Card>
        </Grid.Row>

        <Grid.Row>
          {assessmentDetailModel.commentList.map((item) => (
            <>
              {item.comment && (
                <Card>
                  <Box style={{ margin: '8px 4px' }}>
                    <Box horizontal="row" spaceBetween>
                      <Text
                        color={colors.primaryBlack}
                        fontWeight="med"
                        fontSize={sizes.xxs}
                      >
                        {item.commentFrom}
                      </Text>
                      <div style={{ display: 'flex' }}>
                        <Text
                          className="italic"
                          color={colors.textlightGray}
                          fontSize={sizes.xxs}
                        >
                          {item.commentBy}
                        </Text>
                        <div
                          className="click"
                          onClick={() => deleteComment(item.ids)}
                        >
                          <Image
                            height={18}
                            width={18}
                            style={{ marginLeft: 16 }}
                            src="/static/images/delete-gray@3x.png"
                          />
                        </div>
                      </div>
                    </Box>
                    <Box style={{ paddingTop: 8 }}>
                      <Text color={colors.primaryBlack} fontSize={sizes.xs}>
                        {item.comment}
                      </Text>
                    </Box>
                  </Box>
                </Card>
              )}
            </>
          ))}

          {assessmentDetailModel.tab !== 'SUMMARY' && (
            <Card>
              <Box style={{ paddingTop: 16 }}>
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  Comment
                </Text>

                <Form>
                  <TextArea
                    style={customStyle.styleTextArea}
                    placeholder="Key Comment"
                    value={assessmentDetailModel.comment}
                    onChange={(e) =>
                      assessmentDetailModel.setField('comment', e.target.value)
                    }
                  />
                </Form>
              </Box>
            </Card>
          )}
        </Grid.Row>

        <Grid.Row style={{ marginTop: 16 }}>
          <Card>
            <Grid columns="equal" style={{ margin: 0 }}>
              <Grid.Column computer={2} style={{ paddingTop: 0 }}>
                <Text
                  color={colors.primary}
                  fontWeight="bold"
                  fontSize={sizes.xs}
                >
                  Sub-Process
                </Text>
              </Grid.Column>
              {assessmentDetailModel.objects.length !== 0 ? (
                <Grid.Column computer={14} style={{ paddingTop: 0 }}>
                  <Box horizontal="row">
                    {assessmentDetailModel.objects.map((item) => (
                      <>
                        {item.type === 'CSA' && (
                          <Box
                            horizontal="row"
                            alignCenter
                            style={{ margin: 4, paddingRight: 16 }}
                          >
                            <Popup
                              size="huge"
                              position="top center"
                              style={{
                                borderRadius: '6px 6px 0px 6px',
                                marginTop: 16,
                              }}
                              trigger={
                                <a
                                  href={`/Assessment/assessmentDetail?id=${item.id}&assessmentId=${item.fullPath}`}
                                  target="_blank"
                                  rel="noopener noreferrer"
                                >
                                  <Box
                                    horizontal="row"
                                    alignCenter
                                    className="click"
                                  >
                                    <Image
                                      className="click"
                                      src="../../static/images/zing@3x.png"
                                      style={{
                                        marginRight: 2,
                                        width: 18,
                                        height: 18,
                                      }}
                                    />
                                    <Text
                                      color={colors.textSky}
                                      fontSize={sizes.xs}
                                    >
                                      {item.fullPath}
                                    </Text>
                                  </Box>
                                </a>
                              }
                              content={
                                <Box>
                                  <Text
                                    color={colors.primaryBlack}
                                    fontWeight="med"
                                    fontSize={sizes.s}
                                  >
                                    <span>{item.fullPath}</span>
                                    {' ' + item.name}
                                  </Text>
                                  <Text fontSize={sizes.s}>{item.area}</Text>
                                </Box>
                              }
                            />
                          </Box>
                        )}
                      </>
                    ))}
                  </Box>
                </Grid.Column>
              ) : (
                <Text
                  color={colors.primary}
                  fontWeight="bold"
                  fontSize={sizes.xs}
                >
                  -
                </Text>
              )}
            </Grid>
          </Card>
        </Grid.Row>

        {/* Reviewed by role VP */}
        <Grid.Row>
          {authContext.roles.isAdmin &&
            assessmentDetailModel.approveStatus.status !==
              APPROVE_ASSESSMENT_READY.accept &&
            assessmentDetailModel.status !==
              APPROVE_ASSESSMENT_STATUS.reject.minor &&
            assessmentDetailModel.status !==
              APPROVE_ASSESSMENT_STATUS.reject.major && (
              <div style={{ display: 'flex', marginTop: '16px' }}>
                <div style={{ marginRight: '16px', display: 'flex' }}>
                  <RadioBox
                    onChange={() =>
                      assessmentDetailModel.approveStatus.setId(
                        APPROVE_ASSESSMENT_STATUS.reject.major,
                        assessmentDetailModel.Id,
                      )
                    }
                    checked={
                      assessmentDetailModel.approveStatus.isSelectedMajor
                    }
                    disabled={assessmentDetailModel.isReadyToSubmit}
                  />
                  <Text
                    className="upper"
                    fontSize={sizes.s}
                    fontWeight="med"
                    color={colors.textBlack}
                    style={{ paddingLeft: 8 }}
                  >
                    MAJOR CHANGE
                  </Text>
                </div>

                <div style={{ marginRight: '16px', display: 'flex' }}>
                  <RadioBox
                    onChange={() =>
                      assessmentDetailModel.approveStatus.setId(
                        APPROVE_ASSESSMENT_STATUS.reject.minor,
                        assessmentDetailModel.Id,
                      )
                    }
                    checked={
                      assessmentDetailModel.approveStatus.isSelectedMinor
                    }
                    disabled={assessmentDetailModel.isReadyToSubmit}
                  />
                  <Text
                    className="upper"
                    fontSize={sizes.s}
                    fontWeight="med"
                    color={colors.textBlack}
                    style={{ paddingLeft: 8 }}
                  >
                    MINOR CHANGE
                  </Text>
                </div>
              </div>
            )}
        </Grid.Row>
      </div>

      <BottomBarCosoAssessment
        page="COSOAssessmentDetail"
        saveButtonData={() => setSaveModal(true)}
        readyToSubmitData={assessmentDetailModel}
        assessmentData={assessmentDetailModel}
      />

      {/* Save */}
      <SaveModal
        active={saveModal}
        onClose={() => setSaveModal(false)}
        onSubmit={saveData}
      />

      {/* call Back */}
      <ModalGlobal
        open={confirmDelete}
        title="Are you sure !"
        content="You want to delete this item?"
        onSubmit={deleteComment}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirmDelete(false)}
      />

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to leave this page?"
        content="Your changes have not been saved"
        onSubmit={handelCallBack}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />

      {/* add file  */}
      {/* <AttachFilesModal
        active={activeDocumentStorageModal}
        onClose={() => setActiveDocumentStorageModal(false)}
        onAttatchFiles={(title, files) => {
          assessmentDetailModel.onAddAttatchFiles(title, files);
        }}
      /> */}

      <DocumentStorageModal
        active={activeDocumentStorageModal}
        onClose={() => setActiveDocumentStorageModal(false)}
        onAttatchFiles={(title, files) => {
          assessmentDetailModel.uploadFilesToStorage(authContext.accessToken,title, files);
        }}
        assessmentData={assessmentDetailModel}
        onSave={handleSaveStorage}
      />
    </div>
  ));
};

export default withLayout(observer(index));
