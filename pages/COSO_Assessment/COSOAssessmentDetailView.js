import React, { useState, useEffect } from 'react';
import { Image, Grid, Popup } from 'semantic-ui-react';
import Router from 'next/router';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import moment from 'moment';
import {
  Text,
  Box,
  StatusAll,
  ModalGlobal,
  ButtonAdd,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { withLayout } from '../../hoc';
import { AssessmentDetailCOSOModel } from '../../model/COSO';
import { initAuthStore } from '../../contexts';

const DividerLine = styled.div`
  height: 1px;
  width: 100%;
  margin: 16px 0px;
  background-color: ${(props) =>
    props.primary ? `${colors.primary}` : `${colors.btGray}`};
`;

const Card = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-bottom: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px !important;
  width: 100%;
`;

const index = (props) => {
  const authContext = initAuthStore();
  const assessmentDetailModel = useLocalStore(
    () => new AssessmentDetailCOSOModel(),
  );
  const [confirm, setConfirm] = useState(false);

  useEffect(() => {
    assessmentDetailModel.getAssessmentDetail(
      authContext.accessToken,
      Router.query.id,
    );
  }, []);

  useEffect(() => {
    assessmentDetailModel.getAssessmentComment(
      authContext.accessToken,
      Router.query.id,
    );
  }, [Router.query]);

  const handelCallBack = () => {
    setTimeout(() => {
      Router.back();
      setConfirm(false);
    }, 500);
  };

  return useObserver(() => (
    <div>
      <div
        className="click"
        style={{ width: '10%', display: 'flex' }}
        onClick={() => setConfirm(true)}
      >
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text color={colors.primaryBlack} fontWeight="bold" fontSize={sizes.xl}>
          Back
        </Text>
      </div>
      <div style={{ width: '100%', marginTop: 32 }}>
        <Grid.Row>
          <Card>
            <div>
              <Box horizontal="row" spaceBetween>
                <Box horizontal="row">
                  <Text
                    style={{ marginRight: 16 }}
                    fontSize={sizes.xs}
                    color={colors.textlightGray}
                  >
                    Internal Control Evaluation for 56-1 of 2020
                  </Text>
                  <div>
                    <StatusAll
                      child={{ status: assessmentDetailModel.status }}
                    />
                  </div>
                </Box>
                <Box horizontal="row" spaceBetween>
                  <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                    {assessmentDetailModel.area}
                  </Text>
                  <div
                    style={{
                      margin: '0px 8px',
                      height: 20,
                      width: 1,
                      backgroundColor: colors.btGray,
                    }}
                  />
                  <Text color={colors.textlightGray} fontSize={sizes.xs}>
                    <span style={{ paddingRight: 8 }}>Last Update :</span>
                    {moment(assessmentDetailModel.updateDate)
                      .locale('th')
                      .format('L')}
                  </Text>
                </Box>
              </Box>
              <Text
                style={{ lineHeight: 1.5, paddingTop: 24 }}
                fontWeight="bold"
                fontSize={sizes.xl}
                color={colors.primaryBlack}
              >
                {`${assessmentDetailModel.no} ${assessmentDetailModel.name}`}
              </Text>
              <Box>
                {assessmentDetailModel.objects.map((item) => (
                  <Box horizontal="row">
                    <Text
                      style={{ paddingLeft: 32, paddingTop: 16 }}
                      fontWeight="med"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      {`${item.no} ${item.name}`}
                    </Text>
                  </Box>
                ))}
              </Box>
            </div>
            <DividerLine />
            <div>
              <Grid>
                <Grid.Row columns={4}>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Assessor
                    </Text>

                    <Box horizontal="row" alignCenter>
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />

                      <div>
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info
                            ? assessmentDetailModel.info.worker.name
                            : '-'}
                        </Text>

                        {assessmentDetailModel.info && (
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info.worker.position}
                          </Text>
                        )}
                        <div style={{ display: 'flex' }}>
                          <Image
                            src="../../static/images/iconDate@3x.png"
                            style={{
                              width: 18,
                              height: 18,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              moment(
                                assessmentDetailModel.info.worker.date,
                              ).format('DD/MM/YYYY')}
                          </Text>
                        </div>
                      </div>
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Reviewer
                    </Text>
                    <Box horizontal="row" alignCenter>
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info &&
                      assessmentDetailModel.info.reviewer.name ? (
                        <div>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.name}
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.position}
                          </Text>
                          <div style={{ display: 'flex' }}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                width: 18,
                                height: 18,
                                marginRight: 8,
                                marginTop: 2,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                                moment(
                                  assessmentDetailModel.info.reviewer.date,
                                ).format('DD/MM/YYYY')}
                            </Text>
                          </div>
                        </div>
                      ) : (
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          -
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Approver
                    </Text>

                    <Box horizontal="row" alignCenter>
                      <Image
                        src="../../static/images/user@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info &&
                      assessmentDetailModel.info.approver.name ? (
                        <div>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.name}
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.position}
                          </Text>
                          <div style={{ display: 'flex' }}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                width: 18,
                                height: 18,
                                marginRight: 8,
                                marginTop: 2,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                                moment(
                                  assessmentDetailModel.info.approver.date,
                                ).format('DD/MM/YYYY')}
                            </Text>
                          </div>
                        </div>
                      ) : (
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          -
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                    >
                      Division / Shift
                    </Text>
                    <Box horizontal="row" alignCenter>
                      <Image
                        src="../../static/images/iconWork@3x.png"
                        style={{
                          width: 32,
                          height: 32,
                          marginRight: 8,
                          marginTop: 2,
                        }}
                      />
                      {assessmentDetailModel.info && (
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info.indicator}
                        </Text>
                      )}
                    </Box>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </Card>
        </Grid.Row>
        <Grid.Row style={{ marginTop: 16 }}>
          <Card>
            <Box>
              <Box horizontal="row" spaceBetween>
                <Box horizontal="row">
                  <Text
                    color={colors.primaryBlack}
                    fontSize={sizes.xxs}
                    fontWeight="med"
                    className="upper"
                  >
                    Approach
                  </Text>
                  {/* <Box horizontal="row">
                    <Image
                      src="../../static/images/zing@3x.png"
                      style={{ margin: '0px 8px', width: 18, height: 18 }}
                    />
                    <Text color={colors.textSky} fontSize={sizes.xxs}>
                      2019
                    </Text>
                  </Box> */}
                </Box>
              </Box>
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.approach}
              </Text>
            </Box>
            <Box style={{ paddingTop: 16 }}>
              <Box horizontal="row">
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  Deploy
                </Text>
                {/* <Box horizontal="row">
                  <Image
                    src="../../static/images/zing@3x.png"
                    style={{ margin: '0px 8px', width: 18, height: 18 }}
                  />
                  <Text color={colors.textSky} fontSize={sizes.xxs}>
                    2019
                  </Text>
                </Box> */}
              </Box>
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.deploy}
              </Text>
            </Box>
            <Box style={{ paddingTop: 16 }}>
              <Box horizontal="row">
                <Text
                  color={colors.primaryBlack}
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  className="upper"
                >
                  New Initiative
                </Text>
                {/* <Box horizontal="row">
                  <Image
                    src="../../static/images/zing@3x.png"
                    style={{ margin: '0px 8px', width: 18, height: 18 }}
                  />
                  <Text color={colors.textSky} fontSize={sizes.xxs}>
                    2019
                  </Text>
                </Box> */}
              </Box>
              <Text color={colors.textlightGray} fontSize={sizes.xs}>
                {assessmentDetailModel.newInitiative}
              </Text>
            </Box>
            <Box style={{ paddingTop: 16 }}>
              <Text
                fontWeight="med"
                className="upper"
                color={colors.primaryBlack}
                fontSize={sizes.xxs}
              >
                Document / Evidence
              </Text>
              <Box horizontal="row" alignCenter style={{ paddingTop: 16 }}>
                {assessmentDetailModel.displayFiles.map((file, index) => (
                  <div style={{ marginTop: '-2px' }}>
                    <ButtonAdd
                      text={`${file.title || file.name}`}
                      textSize={sizes.xs}
                      href={file.fullPath || file.filePath}
                      disabled
                    />
                  </div>
                ))}
              </Box>
            </Box>
            {assessmentDetailModel.commentList.length > 0 && (
              <DividerLine primary />
            )}
            {assessmentDetailModel.commentList.map((item) => (
              <Box style={{ margin: '8px 4px' }}>
                <Box horizontal="row" spaceBetween>
                  <Text
                    color={colors.primaryBlack}
                    fontWeight="med"
                    fontSize={sizes.xxs}
                  >
                    {item.commentFrom}
                  </Text>
                  <Text
                    className="italic"
                    color={colors.textlightGray}
                    fontSize={sizes.xxs}
                  >
                    {item.commentBy}
                  </Text>
                </Box>
                <Box style={{ paddingTop: 8 }}>
                  <Text color={colors.primaryBlack} fontSize={sizes.xs}>
                    {item.comment}
                  </Text>
                </Box>
              </Box>
            ))}
            <DividerLine primary />
            <Box>
              <Grid columns="equal" style={{ margin: 0 }}>
                <Grid.Column computer={2} style={{ paddingTop: 0 }}>
                  <Text
                    color={colors.primary}
                    fontWeight="bold"
                    fontSize={sizes.xs}
                  >
                    Sub-Process
                  </Text>
                </Grid.Column>
                {assessmentDetailModel.objects.length !== 0 ? (
                  <Grid.Column computer={14} style={{ paddingTop: 0 }}>
                    <Box horizontal="row">
                      {assessmentDetailModel.objects.map((item) => (
                        <Box
                          horizontal="row"
                          alignCenter
                          style={{ margin: 4, paddingRight: 16 }}
                        >
                          <Popup
                            size="huge"
                            position="top right"
                            style={{
                              borderRadius: '6px 6px 0px 6px',
                              marginRight: -16,
                              marginTop: 16,
                            }}
                            trigger={
                              <Image
                                className="click"
                                src="../../static/images/zing@3x.png"
                                style={{
                                  marginRight: 2,
                                  width: 18,
                                  height: 18,
                                }}
                              />
                            }
                            content={
                              <Box>
                                <Text
                                  color={colors.primaryBlack}
                                  fontWeight="med"
                                  fontSize={sizes.s}
                                >
                                  {item.no}
                                  {item.name}
                                </Text>
                                <Text fontSize={sizes.s}>{item.area}</Text>
                              </Box>
                            }
                          />
                          <Text color={colors.textSky} fontSize={sizes.xs}>
                            {item.no}
                          </Text>
                        </Box>
                      ))}
                    </Box>
                  </Grid.Column>
                ) : (
                  '-'
                )}
              </Grid>
            </Box>
          </Card>
        </Grid.Row>
      </div>

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to leave this page?"
        content="Your changes have not been saved"
        onSubmit={handelCallBack}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
    </div>
  ));
};

export default withLayout(observer(index));
