import React, { useEffect } from 'react';
import Router from 'next/router';
import { withApp } from '../hoc';

const index = () => {
  useEffect(() => {
    // redirect ไปที่หน้า dashboard
    Router.push('/databasePage/dashboard');
  }, []);

  return (
    <div />
  );
};

export default withApp(index);
