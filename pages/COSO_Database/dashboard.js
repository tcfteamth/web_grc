import React, { useState, useEffect } from 'react';
import { Grid, Loader, Dimmer, Header, Button } from 'semantic-ui-react';
import { useLocalStore } from 'mobx-react-lite';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import { withLayout } from '../../hoc';
import {
  Text,
  Box,
  CardEmpty,
  SearchInput,
  BottomBarCosoDatabase,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { COSOCardDetail, COSOAll } from '../../components/COSODatabase';
import { DatabaseModel } from '../../model/COSO';
import useWindowDimensions from '../../hook/useWindowDimensions';

const ButtonStyle = styled(Button)`
  width: 16%;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 16px !important;
  border-right: ${(props) => props.borderRight && '1px solid #f6f6f6'};
  box-shadow: ${(props) =>
    props.none ? 'none' : `0 0px 10px 0 rgba(48, 48, 48, 0.1)`}!important;
`;

const index = (props) => {
  const cosoDBModel = useLocalStore(() => new DatabaseModel());
  const [isOpen, setIsOpen] = useState('All');
  const [year, setYear] = useState();
  const { width: screenWidth } = useWindowDimensions();

  const handelOpen = (value) => {
    setIsOpen(value);
  };

  useEffect(() => {
    cosoDBModel.getCOSODatabaseList();
    setYear(new Date().getFullYear());
  }, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Box horizontal="row" alignCenter spaceBetween>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              COSO Database
            </Text>
          </Header.Content>
        </Header>
        {/* <Header>
          <Header.Content>
            <Box horizontal="row" alignCenter>
              <Text fontSize={sizes.xl} color={colors.primary}>
                Year {year}
              </Text>
            </Box>
          </Header.Content>
        </Header> */}
      </Box>
      <div style={{ paddingTop: 32 }}>
        <Grid style={{ margin: 0 }}>
          <Grid.Row>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={8}
              style={{ padding: 0, flexDirection: 'row', alignSelf: 'center' }}
            >
              <Box horizontal="row" alignCenter selfCenter>
                <Text
                  fontWeight="med"
                  fontSize={sizes.s}
                  color={colors.textDarkGray}
                >
                  QUESTION LIST
                </Text>
              </Box>
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={8}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent:
                  screenWidth < 768 ? 'space-between' : 'flex-end',
                padding: 0,
              }}
            >
              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  cosoDBModel.setSearchDatabaseText(e.target.value);

                  if (!cosoDBModel.searchDatabaseText) {
                    cosoDBModel.getCOSODatabaseList();
                  }
                }}
                onEnterKeydown={(e) => {
                  // keycode 13 คือ ปุ่ม enter
                  if (e.keyCode === 13) {
                    cosoDBModel.getCOSODatabaseList();
                  }
                }}
                width={300}
                value={cosoDBModel.searchDatabaseText}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Button.Group
          className="w-highlight"
          style={{
            width: '100%',
            flexWrap: 'wrap',
            height: screenWidth <= 768 ? 60 : 48,
            boxShadow: '0 0px 10px 0 rgba(48, 48, 48, 0.1)',
            borderRadius: '6px 6px 0px 6px',
          }}
        >
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'All' ? '#0057b8' : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('All')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={isOpen === 'All' ? colors.backgroundPrimary : '#0057b8'}
            >
              All
            </Text>
          </ButtonStyle>
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'Control Environment'
                  ? '#0057b8'
                  : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('Control Environment')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={
                isOpen === 'Control Environment'
                  ? colors.backgroundPrimary
                  : '#0057b8'
              }
            >
              Control Environment
            </Text>
          </ButtonStyle>
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'Risk Assessment'
                  ? '#0057b8'
                  : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('Risk Assessment')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={
                isOpen === 'Risk Assessment'
                  ? colors.backgroundPrimary
                  : '#0057b8'
              }
            >
              Risk Assessment
            </Text>
          </ButtonStyle>
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'Control Activities'
                  ? '#0057b8'
                  : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('Control Activities')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={
                isOpen === 'Control Activities'
                  ? colors.backgroundPrimary
                  : '#0057b8'
              }
            >
              Control Activities
            </Text>
          </ButtonStyle>
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'Information and Communication'
                  ? '#0057b8'
                  : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('Information and Communication')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={
                isOpen === 'Information and Communication'
                  ? colors.backgroundPrimary
                  : '#0057b8'
              }
            >
              Information and Communication
            </Text>
          </ButtonStyle>
          <ButtonStyle
            style={{
              backgroundColor: `${
                isOpen === 'Monitoring activities'
                  ? '#0057b8'
                  : colors.backgroundPrimary
              }`,
            }}
            onClick={() => handelOpen('Monitoring activities')}
          >
            <Text
              fontWeight="bold"
              fontSize={sizes.xs}
              color={
                isOpen === 'Monitoring activities'
                  ? colors.backgroundPrimary
                  : '#0057b8'
              }
            >
              Monitoring Activities
            </Text>
          </ButtonStyle>
        </Button.Group>
      </div>

      <Grid.Row style={{ padding: 0 }}>
        {cosoDBModel.cosoDB.map((item) => (
          <div>
            {isOpen === 'All' && <COSOAll databaseDetail={item} />}
            {isOpen === item.area && <COSOCardDetail databaseDetail={item} />}
          </div>
        ))}

        {cosoDBModel.cosoDB.length < 1 && (
          <div style={{ marginTop: 16 }}>
            <CardEmpty
              Icon="../../static/images/iconDatabase2@3x.png"
              textTitle="No Database Name"
            />
          </div>
        )}
      </Grid.Row>

      <BottomBarCosoDatabase cosoDB={cosoDBModel} />
    </div>
  );
};

export default withLayout(observer(index));
