import React, { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Button, Message, Grid, Image, Input, Form } from 'semantic-ui-react';
import { inject, observer } from 'mobx-react';
import _ from 'lodash';
import Router from 'next/router';
import storejs from 'store';
import styled from 'styled-components';
import { withApp } from '../hoc';
import { colors, sizes, config } from '../utils';
import { Text, TitleText, InputAll, Modal } from '../components/element';
import { initAuthStore } from '../contexts';

const Container = styled.div`
  background: url('../static/images/login-bg-2-2@3x.png') no-repeat center
    center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  background-color: ${colors.primaryBackground} !important;
  width: 100% !important;
  height: 100% !important;
  position: absolute !important;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ImageCard = styled.div`
  position: relative !important;
  width: 692px;
  z-index: 2;
  @media only screen and (min-width: 768px) {
    width: 480px;
  }
`;
const ShadownCard = styled.div`
  width: 712px;
  min-height: 480px;
  border-radius: 6px 6px 0px 6px !important;
  padding: 52px 122px 38px 122px;
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fefeff;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  z-index: 999;
  position: relative;
`;

const customStyle = {
  styleInput: {
    height: '38px',
    width: '100%',
    minWidth: '469px',
    fontSize: '22px',
  },
  styleInputError: {
    border: `1px solid ${colors.primary}`,
    height: '38px',
    width: '100%',
    fontSize: '16px',
  },
};
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding: ${(props) => props.padding || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;
const LineHeader = styled.div`
  border-left: 2px solid #eaeaea !important;
  height: 72px !important;
  margin: 24px;
`;

const SaveButtom = styled(Button)`
  background-color: ${colors.primary} !important;
  text-align: center;
  border-radius: 6px 6px 0px 6px !important;
  width: 227px !important;
  height: 48px !important;
`;

const index = (props) => {
  const { register, handleSubmit, watch, errors } = useForm();
  const authContext = initAuthStore();
  const [active, setActive] = useState(false);

  const onSubmit = () => {
    // const token = storejs.get("accessToken");
    // authContext.loginAD(token);
    // authContext
    //   .loginAD(token)
    //   .then((response) => {
    //     console.log(response);
    //   })
    //   .catch((error) => console.log(error));
  };

  return (
    <Container>
      <Grid container>
        <Grid.Row style={{ justifyContent: 'center' }}>
          <ShadownCard>
            <a href={`${config.baseUrlLogin}`}>
              <SaveButtom type="submit">
                <Text
                  fontSize={sizes.l}
                  fontWeight="bold"
                  color={colors.primaryBackground}
                >
                  LOG IN
                </Text>
              </SaveButtom>
            </a>
          </ShadownCard>
        </Grid.Row>
      </Grid>
      <Image
        src="../static/images/login-bg-1-2@3x.png"
        style={{
          width: 'auto',
          right: 0,
          height: 584,
          position: 'absolute',
        }}
      />
      <Modal
        open={active}
        title="Forgot Password"
        status={{ page: 'login' }}
        ContentComponent={() => (
          <Div mid center>
            <Text
              fontSize={sizes.s}
              color={colors.textDarkBlack}
              style={{ lineHeight: 'normal' }}
            >
              {''}
            </Text>
          </Div>
        )}
        onClose={() => setActive(false)}
      />
    </Container>
  );
};

export default withApp(index);
