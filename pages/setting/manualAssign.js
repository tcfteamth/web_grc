import React, { useState, useEffect } from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';
import { observer, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { Text, BottomBarManualTable } from '../../components/element';
import { colors, sizes } from '../../utils';

import { ManualAssignList } from '../../components/SettingPage';
import { withLayout } from '../../hoc';
import { initAuthStore } from '../../contexts';
import { SettingManualTableModel } from '../../model/ManualTable';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = () => {
  const settingModel = useLocalStore(() => new SettingManualTableModel());
  const authContext = initAuthStore();

  useEffect(() => {
    settingModel.getManualTableList(authContext.accessToken,1);
  }, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div between>
        <Text
          color={colors.primaryBlack}
          fontWeight="bold"
          fontSize={sizes.xxl}
        >
          Manual Assign
        </Text>
      </Div>
      <div style={{ paddingTop: 32 }}>
        <ManualAssignList settingModel={settingModel} />
      </div>

      <BottomBarManualTable page="ManualTable" />
    </div>
  );
};

export default withLayout(observer(index));
