import { useLocalStore, useObserver } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import { Grid, Radio } from 'semantic-ui-react';
import { ButtonAll, ModalGlobal, Text } from '../../components/element';
import { initAuthStore } from '../../contexts';
import { withLayout } from '../../hoc';
import { SettingTaskModel } from '../../model/SettingTaskModel';
import { colors, sizes } from '../../utils';
import Router from 'next/router';

const SettingTask = () => {
  const [activeReAssessmentModal, setActiveReAssessmentModal] = useState(false);
  const [activeFollowUpModal, setActiveFollowUpModal] = useState(false);

  const [isMailCOSO, setIsMailCOSO] = useState(false);

  const [
    activeCOSOReAssessmentModal,
    setActiveCOSOReAssessmentModal,
  ] = useState(false);

  const [
    activeSuccessReAssessmentModal,
    setActiveSuccessReAssessmentModal,
  ] = useState(false);
  const [activeSuccessFollowUpModal, setActiveSuccessFollowUpModal] = useState(
    false,
  );
  const [
    activeSuccessCOSOReAssessmentModal,
    setActiveSuccessCOSOReAssessmentModal,
  ] = useState(false);
  const [
    activeSuccessMailFollowModal,
    setActiveSuccessMailFollowModal,
  ] = useState(false);

  const settingTaskModel = useLocalStore(() => new SettingTaskModel());
  const authContext = initAuthStore();
  const [isEmailActive, setIsEmailActive] = useState({
    isActive: false,
    taskName: '',
  });

  const submitActiveFollowUp = async () => {
    try {
      setActiveFollowUpModal(false);

      await settingTaskModel.activeFollowUp(authContext.accessToken);

      setActiveSuccessFollowUpModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  const submitActiveReAssessment = async () => {
    try {
      setActiveReAssessmentModal(false);

      await settingTaskModel.activeReAssessment();

      setActiveSuccessReAssessmentModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  const submitActiveCOSOReAssessment = async () => {
    try {
      setActiveCOSOReAssessmentModal(false);

      await settingTaskModel.activeCOSOReAssessment();

      setActiveSuccessCOSOReAssessmentModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  const submitActiveEmailFollow = async () => {
    try {
      await settingTaskModel.toggleMailFollow();
      await settingTaskModel.checkMailFollow();
      await settingTaskModel.checkMailCOSOFollow();

      settingTaskModel.setField('submitEmailFollow', '');

      setActiveSuccessMailFollowModal(false);
      setIsMailCOSO(false);

      Router.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const toggleRoadmapMailFollow = async (mailFollow) => {
    settingTaskModel.setField('submitEmailFollow', mailFollow.taskName);

    setIsEmailActive({
      ...isEmailActive,
      isActive: mailFollow.active,
      taskName: mailFollow.name,
    });

    setActiveSuccessMailFollowModal(true);
  };

  const toggleCOSOEmailFollow = (m) => {
    settingTaskModel.setField('submitEmailFollow', m.taskName);

    setIsEmailActive({
      ...isEmailActive,
      isActive: m.active,
      taskName: m.name,
    });

    setIsMailCOSO(true);
  };

  useEffect(() => {
    (async () => {
      try {
        await settingTaskModel.checkReAssessmentInProgress();
        await settingTaskModel.checkMailFollow();
        await settingTaskModel.checkMailCOSOFollow();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  return useObserver(() => (
    <Grid>
      <Grid.Row>
        <Grid.Column computer={16}>
          <Text
            color={colors.primaryBlack}
            fontWeight="bold"
            fontSize={sizes.xl}
          >
            CSA
          </Text>
        </Grid.Column>
        <Grid>
          <Grid.Row style={{ marginTop: 16, marginLeft: 32 }}>
            <Grid.Column computer={16}>
              <Text
                color={colors.primaryBlack}
                fontWeight="bold"
                fontSize={sizes.xl}
              >
                Re-Assessment (re-assessment จะรันได้ต้องปิด assessment
                ทั้งหมดของปีล่าสุดก่อน)
              </Text>
            </Grid.Column>
            <Grid.Column computer={16} style={{ marginTop: 8 }}>
              <ButtonAll
                text="Active Re-Assessment"
                onClick={() => setActiveReAssessmentModal(true)}
                disabled={!settingTaskModel.reAssessmentInPorgressStatus}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ marginTop: 8, marginLeft: 32 }}>
            <Grid.Column computer={16}>
              <Text
                color={colors.primaryBlack}
                fontWeight="bold"
                fontSize={sizes.xl}
              >
                Follow Up (จะรันเฉพาะ assessment ที่ Complete แล้วเท่านั้น)
              </Text>
            </Grid.Column>
            <Grid.Column computer={16} style={{ marginTop: 8 }}>
              <ButtonAll
                text="Active Follow Up"
                onClick={() => setActiveFollowUpModal(true)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Row>

      {settingTaskModel.mailFollow?.map((m) => (
        <Grid.Row style={{ marginLeft: 18 }}>
          <Grid.Column floated="left" width={5}>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xl}
            >
              {m.taskName} - Mail follow
            </Text>
          </Grid.Column>
          <Grid.Column
            floated="right"
            width={11}
            style={{
              fontSize: 14,
              fontWeight: 600,
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
            }}
          >
            <Radio
              toggle
              value={m.taskName}
              onChange={() => toggleRoadmapMailFollow(m)}
              checked={m.active || m.inProgress}
              style={{ marginRight: 10 }}
              // disabled={m.inProgress}
            />
            {m.active || m.inProgress
              ? m.active
                ? 'Active'
                : 'On Progess'
              : 'Inactive'}{' '}
            : {m.activeDate == null ? '--/--/----' : m.activeDate}
          </Grid.Column>
        </Grid.Row>
      ))}

      <Grid.Row style={{ marginTop: 20 }}>
        <Grid.Column computer={16}>
          <Text
            color={colors.primaryBlack}
            fontWeight="bold"
            fontSize={sizes.xl}
          >
            COSO
          </Text>
        </Grid.Column>
        <Grid>
          <Grid.Row style={{ marginTop: 16, marginLeft: 32 }}>
            <Grid.Column computer={16}>
              <Text
                color={colors.primaryBlack}
                fontWeight="bold"
                fontSize={sizes.xl}
              >
                Re-Assessment (จะรันเฉพาะ assessment ที่ Closed แล้วเท่านั้น)
              </Text>
            </Grid.Column>
            <Grid.Column computer={16} style={{ marginTop: 8 }}>
              <ButtonAll
                text="Active COSO Re-Assessment"
                onClick={() => setActiveCOSOReAssessmentModal(true)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Row>
      {settingTaskModel.mailFollowCOSO?.map((m) => (
        <Grid.Row style={{ marginLeft: 18 }}>
          <Grid.Column floated="left" width={5}>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xl}
            >
              {m.taskName} - Mail follow
            </Text>
          </Grid.Column>
          <Grid.Column
            floated="right"
            width={11}
            style={{
              fontSize: 14,
              fontWeight: 600,
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
            }}
          >
            <Radio
              toggle
              value={m.taskName}
              onChange={() => toggleCOSOEmailFollow(m)}
              checked={m.active || m.inProgress}
              style={{ marginRight: 10 }}
              // disabled={m.inProgress}
            />
            {m.active || m.inProgress
              ? m.active
                ? 'Active'
                : 'On Progess'
              : 'Inactive'}{' '}
            : {m.activeDate == null ? '--/--/----' : m.activeDate}
          </Grid.Column>
        </Grid.Row>
      ))}

      <ModalGlobal
        open={activeReAssessmentModal}
        title="Do you want to active re-assessment?"
        content="Please ensure that you have close all assessment task"
        onSubmit={submitActiveReAssessment}
        submitText="Active Re-Assessment"
        cancelText="Cancel"
        onClose={() => setActiveReAssessmentModal(false)}
      />

      <ModalGlobal
        open={activeFollowUpModal}
        title="Do you want to active Follow Up?"
        // content="Please ensure that you have close all followup task"
        onSubmit={submitActiveFollowUp}
        submitText="Active Follow Up"
        cancelText="Cancel"
        onClose={() => setActiveFollowUpModal(false)}
      />

      <ModalGlobal
        open={activeCOSOReAssessmentModal}
        title="Do you want to active coso re-assessment?"
        content="Please ensure that you have close all assessment task"
        onSubmit={submitActiveCOSOReAssessment}
        submitText="Active COSO Re-Assessment"
        cancelText="Cancel"
        onClose={() => setActiveCOSOReAssessmentModal(false)}
      />

      <ModalGlobal
        open={activeSuccessFollowUpModal}
        title="Actived Follow Up"
        // content="Successfully"
        content={settingTaskModel.isActiveFollowUpSuccess}
        onClose={() => {
          setActiveSuccessFollowUpModal(false);
        }}
      />

      <ModalGlobal
        open={activeSuccessReAssessmentModal}
        title="Actived Re-Asessment"
        // content="Successfully"
        content={settingTaskModel.isReAssessmentSuccess}
        onClose={() => {
          setActiveSuccessReAssessmentModal(false);
        }}
      />

      <ModalGlobal
        open={activeSuccessCOSOReAssessmentModal}
        title="Actived COSO Re-Asessment"
        // content="Successfully"
        content={settingTaskModel.isCOSOReAssessmentSuccess}
        onClose={() => {
          setActiveSuccessCOSOReAssessmentModal(false);
        }}
      />

      <ModalGlobal
        open={activeSuccessMailFollowModal}
        title={`${isEmailActive.isActive ? 'Inactive' : 'Active'} ${
          isEmailActive.taskName
        } CSA Email Follow`}
        content={
          isEmailActive.isActive
            ? 'Inactive email'
            : 'Warning!!! This submit will send email to owner task. Do you want to submit?'
        }
        onClose={() => {
          settingTaskModel.setField('submitEmailFollow', '');
          setActiveSuccessMailFollowModal(false);
        }}
        onSubmit={submitActiveEmailFollow}
        submitText="SUBMIT"
        cancelText="Cancel"
      />

      <ModalGlobal
        open={isMailCOSO}
        title={`${isEmailActive.isActive ? 'Inactive' : 'Active'} ${
          isEmailActive.taskName
        } CSA Email Follow`}
        content={
          isEmailActive.isActive
            ? 'Inactive email'
            : 'Warning!!! This submit will send email to owner task. Do you want to submit?'
        }
        submitText="SUBMIT"
        cancelText="Cancel"
        onSubmit={submitActiveEmailFollow}
        onClose={() => {
          settingTaskModel.setField('submitEmailFollow', '');
          setIsMailCOSO(false);
        }}
      />
    </Grid>
  ));
};

export default withLayout(SettingTask);
