import React, { useState, useEffect } from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';
import { observer, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { Text, BottomBarSetting } from '../../components/element';
import { colors, sizes } from '../../utils';

import { TagList } from '../../components/SettingPage';
import { withLayout } from '../../hoc';
import { initAuthStore } from '../../contexts';
import { SettingTagModel } from '../../model/SettingDatabase';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = () => {
  const settingModel = useLocalStore(() => new SettingTagModel());
  const authContext = initAuthStore();

  useEffect(() => {
    settingModel.getTagList(authContext.accessToken);
  }, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div between>
        <Text
          color={colors.primaryBlack}
          fontWeight="bold"
          fontSize={sizes.xxl}
        >
          Tag Management
        </Text>
      </Div>
      <div style={{ paddingTop: 32 }}>
        <TagList settingModel={settingModel} />
      </div>

      <BottomBarSetting page="SettingDatabase" />
    </div>
  );
};

export default withLayout(observer(index));
