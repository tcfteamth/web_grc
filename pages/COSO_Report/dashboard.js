import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Grid, Loader, Dimmer, Segment } from "semantic-ui-react";
import Link from "next/link";
import { Text } from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListAssess, dataListFollow } from "../../utils/static";
import styled from "styled-components";

import _ from "lodash";

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;
const ImageItem = styled(Image)`
  @media only screen and (min-width: 768px) {
    width: auto;
    height: 100px !important;
  }
  @media only screen and (min-width: 1440px) {
    width: auto;
    height: 250px !important;
  }
`;
const CardItem = styled(Segment)`
  width: 100%;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 32px 25px !important;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 230px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 327px;
  }
`;

const index = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {}, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16} mid>
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          Report Coming soon
        </Text>
      </Div>
    </div>
  );
};

export default withLayout(observer(index));
