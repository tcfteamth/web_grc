import React, { useState } from 'react';
import { withLayout } from '../../hoc';
import { Image, Grid, Loader, Dimmer, Header } from 'semantic-ui-react';
import Link from 'next/link';
import {
  AssessmentApproveList,
  CardResultAssessmentVP,
} from '../../components/AssessmentPage';
import {
  ButtonAll,
  Text,
  DropdownGroup,
  ButtonBorder,
  BottomBarAssessment,
} from '../../components/element';
import { observer } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import _ from 'lodash';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const LineSearch = styled.div`
  flex-direction: row;
  justify-content: flex-start;
  display: flex;
  align-items: center;
  border-bottom: 2px solid #bababa;
  margin-left: 24px;
  width: 217px;
`;
const SearchInput = styled.input`
  width: 180px;
  font-size: 20px;
  color: #bababa;
`;
const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const sortBy = [
  { text: 'text 1', value: 0 },
  { text: 'text 2', value: 1 },
  { text: 'text 3', value: 2 },
  { text: 'text 4', value: 3 },
];

const index = (props) => {
  const [isShowFillter, setIsShowFillter] = useState(false);
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };

  return (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div between>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight={'bold'}
              fontSize={sizes.xxl}
            >
              Control Self-Assessment
            </Text>
          </Header.Content>
        </Header>
        <Header>
          <Header.Content>
            <Div center>
              <Text fontSize={sizes.s} fontWeight="med" color={colors.textSky}>
                VIEW DASHBOARD
              </Text>
              <Image
                width={24}
                height={24}
                style={{ marginLeft: 16 }}
                src="../../static/images/dashboard-sky.png"
              />
              <div
                style={{
                  margin: '0px 24px',
                  height: 32,
                  width: 1,
                  backgroundColor: colors.btGray,
                }}
              />
              <Text fontSize={sizes.xl} color={colors.primary}>
                ประจำปี 2020
              </Text>
            </Div>
          </Header.Content>
        </Header>
      </Div>
      <div style={{ paddingTop: 32 }}>
        <Grid style={{ margin: 0 }}>
          <Grid.Row style={{ padding: 0 }}>
            <CardResultAssessmentVP />
          </Grid.Row>

          <Grid.Row columns={2} style={{ padding: 0, margin: '32px 0px 16px' }}>
            <Grid.Column
              width={4}
              style={{
                display: 'flex',
                alignItems: 'center',
                padding: 0,
              }}
            >
              <Text
                fontSize={sizes.s}
                color={colors.primaryBlack}
                fontWeight="bold"
                style={{ marginRight: 8 }}
              >
                APPROVE LIST
              </Text>
              <Text fontSize={sizes.s} color={'#53565a'}>
                (50รายการ)
              </Text>
            </Grid.Column>
            <Grid.Column
              width={12}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                padding: 0,
              }}
            >
              {/* <Text
                fontWeight={'med'}
                fontSize={sizes.s}
                style={{ paddingRight: 8 }}
              >
                Filter
              </Text>
              <DropdownGroup
                width={168}
                options={sortBy}
                textDefault={'All Assessment'}
                fontWeight={600}
              /> */}
              <ButtonBorder
                textWeight={'med'}
                textSize={sizes.xxs}
                width={156}
                options={sortBy}
                text={'Advanced Filter'}
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  isShowFillter
                    ? '../../static/images/fillter-white@3x.png'
                    : '../../static/images/fillter@3x.png'
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => handelOpenFillter(!isShowFillter)}
              />

              <LineSearch>
                <Image
                  src="../../static/images/iconSearch@3x.png"
                  style={{ marginRight: 12, width: 18, height: 18 }}
                />
                <div className="ui transparent input">
                  <SearchInput type="text" placeholder="Search" />
                </div>
              </LineSearch>
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Grid style={{ margin: 0, padding: 0 }}>
                <Div between style={{ padding: 0 }}>
                  <Text fontWeight={'med'} fontSize={sizes.s} color={'#00aeef'}>
                    Advanced Filter
                  </Text>
                  <div
                    className="click"
                    onClick={() => handelOpenFillter(!isShowFillter)}
                  >
                    <Image
                      src="../../static/images/x-close@3x.png"
                      style={{
                        width: 18,
                        height: 18,
                        padding: 0,
                        marginTop: -8,
                        position: 'relative',
                      }}
                    />
                  </div>
                </Div>
                <Grid.Row columns={5}>
                  <Grid.Column style={{ paddingLeft: 0 }}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Assessment
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault={'All Assessment'}
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Assessor
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault={'All Assessment'}
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Roadmap
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault={'All Assessment'}
                      border
                    />
                  </Grid.Column>
                  {/* <Grid.Column>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      CSA
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault={'All Assessment'}
                      border
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Remark
                    </Text>
                    <DropdownGroup
                      options={sortBy}
                      textDefault={'All Assessment'}
                      border
                    />
                  </Grid.Column> */}
                </Grid.Row>
                <div style={{ padding: 0 }}>
                  <ButtonAll width={96} color="#1b1464" text="Filter" />
                </div>
              </Grid>
            </CardFillter>
          )}
          <Grid.Row style={{ padding: 0 }}>
            <AssessmentApproveList />
          </Grid.Row>
        </Grid>
      </div>
      <BottomBarAssessment page={'AssessmentListVP'} />
    </div>
  );
};

export default withLayout(observer(index));
