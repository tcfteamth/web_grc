import React, { useState, useEffect } from 'react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { Image, Grid, Loader, Dimmer, Button, Select } from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import useWindowDimensions from '../../hook/useWindowDimensions';
import {
  ButtonAll,
  Text,
  ButtonBorder,
  CardEmpty,
  SearchInput,
  BUDropdown,
  RoadmapTypeDropdown,
  ProcessLvl3Dropdown,
  ProcessLvl4Dropdown,
  DepartmentDropdown,
  DivisionDropdown,
  ShiftDropdown,
  StatusDropdown,
  RateDropdown,
  OwenerTaskDropdown,
  YearDropdown,
  YearListDropdown,
  DropdownSelect
} from '../../components/element';
import {
  AssessmentSummary,
  AssessmentToDoList,
  AssessmentToDoListDM,
  AssessmentApproveList,
  AssessmentApproveListAdmin,
  CardResultAssessmentVP,
} from '../../components/AssessmentPage';
import { withLayout } from '../../hoc';
import { AssessmentListModel } from '../../model/AssessmentModel';
import { initAuthStore } from '../../contexts';
import { ROLES } from '../../contexts/AuthContext';
import { apiGatewayInstance } from '../../helpers/utils';
import { manageBUDropdownStore, yearListDropdownStore , taglistDropdownStore } from '../../model';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const sortBy = [
  { text: 'text 1', value: 0 },
  { text: 'text 2', value: 1 },
  { text: 'text 3', value: 2 },
  { text: 'text 4', value: 3 },
];

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const index = (props) => {
  const [isOpen, setIsOpen] = useState('ToDoList');
  const { width } = useWindowDimensions();
  const [isShowFillter, setIsShowFillter] = useState(false);
  const assessmentModel = useLocalStore(() => new AssessmentListModel());
  const authContext = initAuthStore();
  const [dataSummary, setDataSummary] = useState();
  const [countSummary, setCountSummary] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isFirstTime, setIsFirstTime] = useState(true);
  const [defaultYear, setDetaultYear] = useState('');

  
  const tagListDropdown = useLocalStore(() => taglistDropdownStore);
  const [tagsList, setTagList] = useState();

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };

  const handleProcessTagSelection = (e,data,field) => {
    let result = assessmentModel.filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    assessmentModel.filterModel.setField(
      field,
      result,
    );
  }

  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };

  const initData = async (
    token,
    page = 1,
    searchAssessmentText = '',
    lvl3 = '',
    lvl4 = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    owner = '',
    year = defaultYear,
    processAnd = [],
    processOr = [],
    objectives = []
  ) => {
    setIsLoading(true);
    const processAndStr = processAnd.map(x => x.value).join(',')
    const processOrStr = processOr.map(x => x.value).join(',')
    const objectivesStr = objectives.map(x => x.value).join(',')
    await apiGatewayInstance
      .get(
        `/api/manage/assessments/summaryList?page=${page}&size=10&search=${searchAssessmentText}&lv3=${lvl3}&lv4=${lvl4}&roadmapType=${roadmapType}&bu=${bu}&departmentNo=${departmentNo}&divisionNo=${divisionNo}&shiftNo=${shiftNo}&status=${status}&ownerTask=${owner}&year=${year}&tagIdsAnd=${processAndStr}&tagIdsOr=${processOrStr}&objectives=${objectivesStr}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((response) => {
        setIsLoading(false);
        setDataSummary(response.data);
        setCountSummary(response.data.assessments.length);
      })
      .catch((error) => {
        setIsLoading(false);
        console.log(error);
      });
  };

  const getAssessmentList = async (year) => {
    try {
      await assessmentModel.getAssessmentList(
        authContext.accessToken,
        1,
        assessmentModel.searchAssessmentText,
        assessmentModel.filterModel.selectedLvl3,
        assessmentModel.filterModel.selectedLvl4,
        assessmentModel.filterModel.selectedBu,
        assessmentModel.filterModel.selectedRoadmapType,
        assessmentModel.filterModel.selectedDepartment.no,
        assessmentModel.filterModel.selectedDivision,
        assessmentModel.filterModel.selectedShift,
        assessmentModel.filterModel.selectedStatus,
        assessmentModel.filterModel.selectedRate,
        year,
        assessmentModel.filterModel.selectedProcessAnd,
        assessmentModel.filterModel.selectedProcessOr,
        assessmentModel.filterModel.selectedObjectives
      );
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    (async () => {
      try {

        // if (authContext.roles.isIcAgent) {
        //   const bu = await manageBUDropdownStore.getManageFilterBUList(
        //     authContext.accessToken,
        //   );


        //   assessmentModel.filterModel.setField('selectedBu', bu[0].value);
        // }


        const yearResult = await yearListDropdownStore.getYearListFillter(
          authContext.accessToken,
        );
        
        setDetaultYear(yearResult[1].value);
        assessmentModel.filterModel.setField('selectedYear', yearResult[1].value);
        await initData(
          authContext.accessToken,
          1,
          assessmentModel.searchAssessmentText,
          assessmentModel.filterModel.selectedLvl3,
          assessmentModel.filterModel.selectedLvl4,
          assessmentModel.filterModel.selectedRoadmapType,
          assessmentModel.filterModel.selectedBu,
          assessmentModel.filterModel.selectedDepartment.no,
          assessmentModel.filterModel.selectedDivision,
          assessmentModel.filterModel.selectedShift,
          assessmentModel.filterModel.selectedStatus,
          assessmentModel.filterModel.selectedOwner,
          yearResult[1].value,
          assessmentModel.filterModel.selectedProcessAnd,
          assessmentModel.filterModel.selectedProcessOr,
          assessmentModel.filterModel.selectedObjectives
        );

        await getAssessmentList(yearResult[1].value);
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  const renderToDoList = () => {
    if (authContext.roles.isAdmin) {
      return <AssessmentApproveListAdmin assessmentData={assessmentModel} />;
    } else if (authContext.roles.isDM) {
      return (
        <AssessmentToDoListDM
          assessmentData={assessmentModel}
          role={ROLES.dm}
        />
      );
    } else if (authContext.roles.isVP || authContext.roles.isEVP) {
      return (
        <AssessmentApproveList
          assessmentData={assessmentModel}
          role={ROLES.vp}
        />
      );
    } else {
      return (
        <AssessmentToDoList
          assessmentData={assessmentModel}
          role={ROLES.staff}
        />
      );
    }
  };

  const renderSummary = () => (
    <AssessmentSummary
      dataSummary={dataSummary}
      tab="SUMMARY"
      onChangePage={(activePage) =>
        initData(
          authContext.accessToken,
          activePage,
          assessmentModel.searchAssessmentText,
          assessmentModel.filterModel.selectedLvl3,
          assessmentModel.filterModel.selectedLvl4,
          assessmentModel.filterModel.selectedRoadmapType,
          assessmentModel.filterModel.selectedBu,
          assessmentModel.filterModel.selectedDepartment.no,
          assessmentModel.filterModel.selectedDivision,
          assessmentModel.filterModel.selectedShift,
          assessmentModel.filterModel.selectedStatus,
          assessmentModel.filterModel.selectedOwner,
          assessmentModel.filterModel.selectedYear,         
          assessmentModel.filterModel.selectedProcessAnd,
          assessmentModel.filterModel.selectedProcessOr,
          assessmentModel.filterModel.selectedObjectives
        )
      }
    />
  );

  const selectedDepartment = () => {
    if (authContext.roles.isIaFinding) {
      return assessmentModel.filterModel.selectedDepartment.value;
    }

    // if (
    //   authContext.roles.isIcAgent &&
    //   assessmentModel.filterModel.selectedTab === 'SUMMARY'
    // ) {
    //   return assessmentModel.filterModel.selectedDepartment.value;
    // }


    if (
      !authContext.roles.isIcAgent &&
      !authContext.roles.isAdmin &&
      !authContext.roles.isCompliance &&
      assessmentModel.filterModel.selectedTab === 'SUMMARY'
    ) {
      return assessmentModel.departmentId;
    }

    if (
      !authContext.roles.isIcAgent &&
      !authContext.roles.isAdmin &&
      !authContext.roles.isCompliance &&
      assessmentModel.filterModel.selectedTab === 'TODO'
    ) {
      return assessmentModel.departmentId;
    }

    return assessmentModel.filterModel.selectedDepartment.value;
  };

  const buFilter = () => {
    if (
      authContext.roles.isIcAgent ||
      authContext.roles.isAdmin ||
      authContext.roles.isCompliance ||
      authContext.roles.isIaFinding
    ) {
      return true;
    }

    return false;
  };

  const departmentFilter = () => {
    // if (
    //   authContext.roles.isIcAgent &&
    //   assessmentModel.filterModel.selectedTab === 'SUMMARY'
    // ) {
    //   return true;
    // }

    if (
      authContext.roles.isIcAgent ||
      authContext.roles.isAdmin ||
      authContext.roles.isCompliance ||
      authContext.roles.isIaFinding
    ) {
      return true;
    }

    return false;
  };

  const resetFilter = () => {
    // if (authContext.roles.isIcAgent) {
    //   assessmentModel.filterModel.resetAssessment();
    // } else {
      assessmentModel.filterModel.resetTodoListAssessment();
      assessmentModel.filterModel.resetSummaryAssessment();
    // }
  };

  return useObserver(() => (
    <div>
      <Dimmer active={isLoading}>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Grid>
        <Grid.Row>
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={width < 768 ? sizes.xl : sizes.xxl}
            >
              Control Self-Assessment
            </Text>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <div style={{ paddingTop: width <= 768 ? 8 : 32 }}>
        {authContext.roles.isVP && assessmentModel.departmentId && (
          <CardResultAssessmentVP deparmentId={assessmentModel.departmentId} />
        )}

        <Grid style={{ margin: 0 }}>
          <Grid.Row>
            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              style={{ padding: 0, marginBottom: width < 768 && 16 }}
            >
              <Button.Group
                style={{
                  width: width < 768 && '100%',
                  boxShadow: '0 0px 10px 0 rgba(48, 48, 48, 0.1)',
                  borderRadius: '6px 6px 0px 6px',
                }}
              >
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '10px 24px',
                    backgroundColor: `${
                      isOpen === 'ToDoList'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => {
                    handelOpen('ToDoList');
                    assessmentModel.setField('tab', 'TODO');
                    assessmentModel.filterModel.setField('selectedTab', 'TODO');
                    // if (authContext.roles.isIcAgent) {
                    //   assessmentModel.filterModel.resetAssessment();
                    // } else {
                      assessmentModel.filterModel.resetSummaryAssessment();
                    // }
                  }}
                >
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={
                      isOpen === 'ToDoList'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    TO DO LIST
                  </Text>
                </Button>
                <Button
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '10px 24px',
                    backgroundColor: `${
                      isOpen === 'Summary'
                        ? '#0057b8'
                        : colors.backgroundPrimary
                    }`,
                  }}
                  onClick={() => {
                    handelOpen('Summary');
                    assessmentModel.setField('tab', 'SUMMARY');
                    assessmentModel.filterModel.setField(
                      'selectedTab',
                      'SUMMARY',
                    );
                    // if (authContext.roles.isIcAgent) {
                    //   assessmentModel.filterModel.resetAssessment();
                    // } else {
                      assessmentModel.filterModel.resetTodoListAssessment();
                    // }
                  }}
                >
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={
                      isOpen === 'Summary'
                        ? colors.backgroundPrimary
                        : '#0057b8'
                    }
                  >
                    SUMMARY
                  </Text>
                </Button>
              </Button.Group>
            </Grid.Column>

            <Grid.Column
              computer={8}
              tablet={8}
              mobile={16}
              floated="right"
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: width < 768 ? 'space-between' : 'flex-end',
                padding: width < 768 ? '8px 0px 0px' : 0,
              }}
            >
              <ButtonBorder
                textWeight="med"
                style={{ margin: '0px 24px 0px 0px' }}
                textSize={sizes.xs}
                width={width < 768 ? 56 : 156}
                options={sortBy}
                text={width < 768 ? '' : 'Advanced Filter'}
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  isShowFillter
                    ? '../../static/images/fillter-white@3x.png'
                    : '../../static/images/fillter@3x.png'
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => {
                  handelOpenFillter(!isShowFillter);

                  // if (authContext.roles.isIcAgent) {
                  //   assessmentModel.filterModel.resetAssessment();
                  // } else {
                    assessmentModel.filterModel.resetTodoListAssessment();
                    assessmentModel.filterModel.resetSummaryAssessment();
                  // }

                  if (isShowFillter) {
                    if (assessmentModel.filterModel.selectedTab === 'TODO') {
                      assessmentModel.getAssessmentApprovementWithSearch(
                        authContext.accessToken,
                      );
                    } else {
                      initData(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                        assessmentModel.filterModel.selectedLvl3,
                        assessmentModel.filterModel.selectedLvl4,
                        assessmentModel.filterModel.selectedRoadmapType,
                        assessmentModel.filterModel.selectedBu,
                        assessmentModel.filterModel.selectedDepartment.no,
                        assessmentModel.filterModel.selectedDivision,
                        assessmentModel.filterModel.selectedShift,
                        assessmentModel.filterModel.selectedStatus,
                        assessmentModel.filterModel.selectedOwner,
                        defaultYear,
                        assessmentModel.filterModel.selectedProcessAnd,
                        assessmentModel.filterModel.selectedProcessOr,
                        assessmentModel.filterModel.selectedObjectives
                      );
                    }
                  }
                }}
              />
              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  assessmentModel.setField(
                    'searchAssessmentText',
                    e.target.value,
                  );
                  if (!assessmentModel.searchAssessmentText) {
                    assessmentModel.getAssessmentApprovementWithSearch(
                      authContext.accessToken,
                    );

                    initData(
                      authContext.accessToken,
                      1,
                      assessmentModel.searchAssessmentText,
                    );
                  }
                }}
                value={assessmentModel.searchAssessmentText}
                onEnterKeydown={(e) => {
                  if (e.keyCode === 13) {
                    if (assessmentModel.tab === 'TODO') {
                      assessmentModel.getAssessmentApprovementWithSearch(
                        authContext.accessToken,
                      );
                    }

                    if (assessmentModel.tab === 'SUMMARY') {
                      initData(
                        authContext.accessToken,
                        1,
                        assessmentModel.searchAssessmentText,
                      );
                    }
                  }
                }}
              />
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Grid style={{ margin: 0, padding: 0 }}>
                <Div between style={{ padding: 0 }}>
                  <Text fontWeight="med" fontSize={sizes.s} color="#00aeef">
                    Advanced Filter
                  </Text>
                  <div
                    className="click"
                    onClick={() => {
                      assessmentModel.filterModel.resetTodoListAssessment();
                      handelOpenFillter(!isShowFillter);
                      if (assessmentModel.filterModel.selectedTab === 'TODO') {
                        assessmentModel.getAssessmentApprovementWithSearch(
                          authContext.accessToken,
                          1,
                        );
                      } else {
                        initData(authContext.accessToken, 1);
                      }
                    }}
                  >
                    <Image
                      src="../../static/images/x-close@3x.png"
                      style={{
                        width: 18,
                        height: 18,
                        padding: 0,
                        marginTop: -8,
                        position: 'relative',
                      }}
                    />
                  </div>
                </Div>
                <Grid.Row>
                  {/* {authContext.roles.isAdmin && ( */}
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Year
                    </Text>
                    <YearDropdown
                      handleOnChange={(year) => {
                        assessmentModel.filterModel.setField(
                          'selectedYear',
                          year,
                        );
                        assessmentModel.filterModel.setField(
                          'selectedBu',
                          '',
                        );

                        assessmentModel.filterModel.setField(
                          'selectedDepartment',
                          '',
                        );

                        assessmentModel.filterModel.setField(
                          'selectedDivision',
                          '',
                        );
                        assessmentModel.filterModel.setField(
                          'selectedShift',
                          '',
                        );
                      }}
                      value={assessmentModel.filterModel.selectedYear}
                      model={[assessmentModel]}
                      placeholder="Select Year"
                    />
                  </Grid.Column>
                  {/* // )} */}
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process (Level 3)
                    </Text>
                    <ProcessLvl3Dropdown
                      handleOnChange={(lvl3) => {
                        console.log(lvl3);
                        assessmentModel.filterModel.setField(
                          'selectedLvl3',
                          lvl3,
                        );

                        assessmentModel.filterModel.setField(
                          'selectedLvl4',
                          '',
                        );
                      }}
                      value={assessmentModel.filterModel.selectedLvl3}
                      defaultValue="Select Process (Level 3)"
                      placeholder="Select Process (Level 3)"
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Sub-Process (Level 4)
                    </Text>
                    <ProcessLvl4Dropdown
                      handleOnChange={(lvl4) => {
                        assessmentModel.filterModel.setField(
                          'selectedLvl4',
                          lvl4,
                        );
                      }}
                      value={assessmentModel.filterModel.selectedLvl4}
                      disabled={!assessmentModel.filterModel.selectedLvl3}
                      selectedLVl3={assessmentModel.filterModel.selectedLvl3}
                      defaultValue="Select Process (Level 4)"
                      placeholder="Select Process (Level 4)"
                    />
                  </Grid.Column>
                  {(assessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    (!authContext.roles.isAdmin &&
                      !authContext.roles.isVP)) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Roadmap
                      </Text>
                      <RoadmapTypeDropdown
                        handleOnChange={(type) => {
                          assessmentModel.filterModel.setField(
                            'selectedRoadmapType',
                            type,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedRoadmapType}
                        defaultValue="Select Roadmap Type"
                        placeholder="Select Roadmap Type"
                      />
                    </Grid.Column>
                  )}
                  {buFilter() && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        BU
                      </Text>
                      <BUDropdown
                        handleOnChange={(bu) => {
                          assessmentModel.filterModel.setField(
                            'selectedBu',
                            bu,
                          );

                          assessmentModel.filterModel.setField(
                            'selectedDepartment',
                            '',
                          );

                          assessmentModel.filterModel.setField(
                            'selectedDivision',
                            '',
                          );
                          assessmentModel.filterModel.setField(
                            'selectedShift',
                            '',
                          );
                        }}
                        value={assessmentModel.filterModel.selectedBu}
                        disabled={assessmentModel.filterModel.isBuDisabled}
                        selectedYear={assessmentModel.filterModel.selectedYear}
                        defaultValue="Select BU"
                        placeholder="Select BU"
                      />
                    </Grid.Column>
                  )}
                  {departmentFilter() && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Department
                      </Text>
                      <DepartmentDropdown
                        handleOnChange={(department) => {
                          assessmentModel.filterModel.setField(
                            'selectedDepartment',
                            department,
                          );

                          assessmentModel.filterModel.setField(
                            'selectedDivision',
                            '',
                          );
                        }}
                        value={
                          assessmentModel.filterModel.selectedDepartment.value
                        }
                        disabled={
                          assessmentModel.filterModel.isDepartmentDisabled
                        }
                        selectedBu={assessmentModel.filterModel.selectedBu}
                        selectedYear={assessmentModel.filterModel.selectedYear}
                        defaultValue="Select Department"
                        placeholder="Select Department"
                      />
                    </Grid.Column>
                  )}
                  {(assessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    authContext.roles.isVP ||
                    authContext.roles.isAdmin ||
                    authContext.roles.isIcAgent ||
                    authContext.roles.isIaFinding) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Division
                      </Text>
                      <DivisionDropdown
                        handleOnChange={(divisionNo) => {
                          if (assessmentModel.filterModel.resetBuMistake) {
                            assessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          assessmentModel.filterModel.setField(
                            'selectedDivision',
                            divisionNo,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedDivision}
                        selectedDepartmentId={selectedDepartment()}
                        selectedYear={assessmentModel.filterModel.selectedYear}
                        selectedDepartmentNo={assessmentModel.filterModel.selectedDepartment.no}
                        defaultValue="Select Division"
                        placeholder="Select Division"
                      />
                    </Grid.Column>
                  )}
                  {(assessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    authContext.roles.isVP ||
                    authContext.roles.isAdmin ||
                    authContext.roles.isIcAgent ||
                    authContext.roles.isIaFinding) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Shift
                      </Text>
                      <ShiftDropdown
                        handleOnChange={(shiftNo) => {
                          if (assessmentModel.filterModel.resetBuMistake) {
                            assessmentModel.filterModel.setField(
                              'selectedBu',
                              '',
                            );
                          }
                          assessmentModel.filterModel.setField(
                            'selectedShift',
                            shiftNo,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedShift}
                        selectedDepartmentId={selectedDepartment()}
                        selectedDepartmentNo={assessmentModel.filterModel.selectedDepartment.no}
                        selectedYear={assessmentModel.filterModel.selectedYear}
                        defaultValue="Select Shift"
                        placeholder="Select Shift"
                      />
                    </Grid.Column>
                  )}
                  {(assessmentModel.filterModel.selectedTab === 'SUMMARY' ||
                    !authContext.roles.isVP) && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Status
                      </Text>
                      <StatusDropdown
                        handleOnChange={(status) => {
                          assessmentModel.filterModel.setField(
                            'selectedStatus',
                            status,
                          );
                        }}
                        tab={assessmentModel.filterModel.selectedTab}
                        type="ASSESSMENT"
                        value={assessmentModel.filterModel.selectedStatus}
                        defaultValue="Select Status"
                        placeholder="Select Status"
                      />
                    </Grid.Column>
                  )}
                  {assessmentModel.filterModel.selectedTab === 'TODO' &&
                    (authContext.roles.isVP || authContext.roles.isAdmin) && (
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Overall Rating
                        </Text>
                        <RateDropdown
                          handleOnChange={(rate) => {
                            assessmentModel.filterModel.setField(
                              'selectedRate',
                              rate,
                            );
                          }}
                          value={assessmentModel.filterModel.selectedRate}
                          defaultValue="Select Overall Rate"
                          placeholder="Select Overall Rate"
                        />
                      </Grid.Column>
                    )}
                  {assessmentModel.filterModel.selectedTab === 'SUMMARY' && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Task Owner
                      </Text>
                      <OwenerTaskDropdown
                        handleOnChange={(owner) => {
                          assessmentModel.filterModel.setField(
                            'selectedOwner',
                            owner,
                          );
                        }}
                        value={assessmentModel.filterModel.selectedOwner}
                        defaultValue="Select Owner"
                        placeholder="Select Owner"
                      />
                    </Grid.Column>
                  )}
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process Tag (AND)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={assessmentModel.filterModel.selectedProcessAnd}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process Tag (OR)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={assessmentModel.filterModel.selectedProcessOr}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Objective (AND)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Objective"
                      options={objectiveList}
                      value={assessmentModel.filterModel.selectedObjectives}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedObjectives')}
                    />
                  </Grid.Column>
                </Grid.Row>
                <div style={{ padding: 0, display: 'flex' }}>
                  <ButtonAll
                    width={96}
                    color="#1b1464"
                    text="Filter"
                    onClick={() => {
                      if (assessmentModel.filterModel.selectedTab === 'TODO') {
                        getAssessmentList(assessmentModel.filterModel.selectedYear);
                      } else {
                        initData(
                          authContext.accessToken,
                          1,
                          assessmentModel.searchAssessmentText,
                          assessmentModel.filterModel.selectedLvl3,
                          assessmentModel.filterModel.selectedLvl4,
                          assessmentModel.filterModel.selectedRoadmapType,
                          assessmentModel.filterModel.selectedBu,
                          assessmentModel.filterModel.selectedDepartment.no,
                          assessmentModel.filterModel.selectedDivision,
                          assessmentModel.filterModel.selectedShift,
                          assessmentModel.filterModel.selectedStatus,
                          assessmentModel.filterModel.selectedOwner,
                          assessmentModel.filterModel.selectedYear,
                          assessmentModel.filterModel.selectedProcessAnd,
                          assessmentModel.filterModel.selectedProcessOr,
                          assessmentModel.filterModel.selectedObjectives
                        );
                      }
                    }}
                  />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => {
                      if (isShowFillter) {
                        if (
                          assessmentModel.filterModel.selectedTab === 'TODO'
                        ) {
                          resetFilter();
                          assessmentModel.getAssessmentApprovementWithSearch(
                            authContext.accessToken,
                          );
                        } else {
                          resetFilter();
                          initData(
                            authContext.accessToken,
                            1,
                            assessmentModel.searchAssessmentText,
                            assessmentModel.filterModel.selectedLvl3,
                            assessmentModel.filterModel.selectedLvl4,
                            assessmentModel.filterModel.selectedRoadmapType,
                            assessmentModel.filterModel.selectedBu,
                            assessmentModel.filterModel.selectedDepartment.no,
                            assessmentModel.filterModel.selectedDivision,
                            assessmentModel.filterModel.selectedStatus,
                            assessmentModel.filterModel.selectedOwner,
                            defaultYear,
                            assessmentModel.filterModel.selectedProcessAnd,
                            assessmentModel.filterModel.selectedProcessOr,
                            assessmentModel.filterModel.selectedObjectives
                          );
                        }
                      }
                    }}
                  />
                </div>
              </Grid>
            </CardFillter>
          )}
          {assessmentModel.assessmentList.length !== 0 || countSummary !== 0 ? (
            <Grid.Row style={{ padding: 0 }}>
              {isOpen === 'ToDoList' ? renderToDoList() : renderSummary()}
            </Grid.Row>
          ) : (
            <Grid.Row style={{ padding: 0 }}>
              <CardEmpty
                Icon="../../static/images/iconAssessment@3x.png"
                textTitle="No Assessment"
              />
            </Grid.Row>
          )}
        </Grid>
      </div>

      {/* {assessmentModel.totalPage > 1 && (
        <Grid>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={assessmentModel.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
                assessmentModel.getAssessmentList(
                  authContext.accessToken,
                  activePage,
                )
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )} */}
      {/* Role Page */}
      {/* <div>
        <Grid style={{ padding: 20 }}>
          <Grid.Row>
            <ButtonAll
              width={96}
              color={colors.textSky}
              text="Role VP"
              onClick={() => handelPage("/Assessment/assessmentListVP")}
            />
            <ButtonAll
              width={96}
              color="#1b1464"
              text="Role DM "
              onClick={() => handelPage("/Assessment/assessmentListDM")}
            />
            <ButtonAll width={96} color={colors.textSky} text="Role Admin" />
            <ButtonAll width={96} color="#1b1464" text="Role Staff" />
          </Grid.Row>
        </Grid>
      </div> */}
      {/* Role Page */}
    </div>
  ));
};

export default withLayout(observer(index));
