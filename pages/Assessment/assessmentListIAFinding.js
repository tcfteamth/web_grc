import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { observer, useLocalStore } from 'mobx-react-lite';
import storejs from 'store';
import styled from 'styled-components';
import { Loader, Dimmer } from 'semantic-ui-react';
import { withLayout } from '../../hoc';
import { FindingListAssessment } from '../../components/AssessmentPage';
import { Text } from '../../components/element';
import { colors, sizes } from '../../utils';
import { IAFindingDetailModel } from '../../model/AssessmentModel';
import { initAuthStore } from '../../contexts';

const Card = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
`;

const index = (props) => {
  const router = useRouter();
  const iafindingDetailModel = useLocalStore(() => new IAFindingDetailModel());
  const authContext = initAuthStore();
  const iafinding = storejs.get('iafinding');

  useEffect(() => {
    iafindingDetailModel.getIAFindingDetail(
      authContext.accessToken,
      iafinding.indicator,
    );
  }, []);

  return (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Card>
        <Text fontSize={sizes.xl} fontWeight="bold" color={colors.primaryBlack}>
          {`IA Finding ${iafinding.indicator}`}
        </Text>
        <Div top={4}>
          <Text fontSize={sizes.xs} color={colors.textlightGray}>
            CSA :
          </Text>
          <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
            {`${iafinding.assessmentNo} ${iafinding.assessmentName}`}
          </Text>
        </Div>
      </Card>
      <FindingListAssessment iafindingDetailModel={iafindingDetailModel} />
    </div>
  );
};

export default withLayout(observer(index));
