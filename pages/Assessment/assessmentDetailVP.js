import React, { useState } from 'react';
import { withLayout } from '../../hoc';
import {
  Image,
  Grid,
  Loader,
  Dimmer,
  Button,
  Segment,
  Popup,
} from 'semantic-ui-react';
import Router from 'next/router';
import { Text, ButtonAll } from '../../components/element';
import {
  CardMenuAssessmentVP,
  CardMenuWorkFlow,
} from '../../components/AssessmentPage';
import { observer } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';

const CardItem = styled(Segment)`
  width: 100%;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 24px !important;
  flex-direction: column;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 272px;
  }

  @media only screen and (min-width: 1024px) {
    min-height: 318px;
  }
  @media only screen and (min-width: 1300px) {
    min-height: 299px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 280px;
  }
`;

const CardTab = styled.div`
  margin-left: -26px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const TabLeft = styled.div`
  height: 48px;
  width: 48px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 1px;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  border-radius: ${(props) =>
    props.rightTop
      ? '0px 6px 0px 0px'
      : props.leftBottom
      ? '0px 0px 6px 0px'
      : '0px'};
`;

const CardContainer = styled.div`
  margin-bottom: 98px;
  padding-left: 16px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const TabBack = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  border-radius: 0px 6px 0px 0px !important;
`;

const index = (props) => {
  const [isOpen, setIsOpen] = useState(2);
  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return (
    <Div>
      <CardTab>
        <TabLeft bgcolor={'#333333'} rightTop>
          <Text
            color={colors.primaryBackground}
            fontWeight={'bold'}
            fontSize={sizes.s}
          >
            R1
          </Text>
        </TabLeft>
        <TabLeft bgcolor={'#00b140'} leftBottom>
          <Text
            color={colors.primaryBackground}
            fontWeight={'bold'}
            fontSize={sizes.s}
          >
            R2
          </Text>
        </TabLeft>
      </CardTab>
      <CardContainer>
        <Dimmer>
          <Loader>
            <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
              Loading
            </Text>
          </Loader>
        </Dimmer>
        <div
          className="click"
          style={{ width: '10%' }}
          onClick={() => Router.back()}
        >
          <TabBack>
            <Image
              src="../../static/images/black@3x.png"
              style={{ marginRight: 12, width: 18, height: 18 }}
            />
            <Text
              color={colors.primaryBlack}
              fontWeight={'bold'}
              fontSize={sizes.xl}
            >
              Back
            </Text>
          </TabBack>
        </div>
        <div>
          <Grid columns="equal" style={{ padding: 0, margin: 0 }}>
            <Grid.Row style={{ paddingTop: 32 }}>
              <Grid.Column tablet={16} computer={8} style={{ padding: 2 }}>
                <CardItem>
                  <Text fontSize={sizes.xxs} color={colors.textlightGray}>
                    บริษัท พีทีที โกลบอล เคมิคอล จำกัด (มหาชน)
                  </Text>
                  <Text fontSize={sizes.xxs} color={colors.textlightGray}>
                    การประเมินการควบคุมภายในด้วยตนเอง (Control Self-Assessment)
                  </Text>
                  <Div between top={24}>
                    <ButtonAll
                      textSize={sizes.xxxs}
                      textWeight={'bold'}
                      height={24}
                      text={'Preparing '}
                      textColor={colors.textpurplepink}
                      color={colors.purplepink}
                    />

                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Image
                        src="../../static/images/re@3x.png"
                        style={{ width: 18, height: 18, marginRight: 8 }}
                      />
                      <Text fontSize={sizes.xxs} color={colors.textDarkBlack}>
                        Re-Assessment
                      </Text>
                      <div
                        style={{
                          margin: '0px 8px',
                          height: 20,
                          width: 1,
                          backgroundColor: colors.btGray,
                        }}
                      />
                      <Text color={colors.textlightGray} fontSize={sizes.xxs}>
                        Last Update : 03/01/2563
                      </Text>
                    </div>
                  </Div>

                  <Text
                    style={{ lineHeight: 1, paddingTop: 24 }}
                    fontWeight={'bold'}
                    fontSize={sizes.xl}
                    color={colors.primaryBlack}
                  >
                    1.4.4.2 Develop and monitor compliance with internal control
                    policies and procedures
                  </Text>
                </CardItem>
              </Grid.Column>
              <Grid.Column tablet={16} computer={8} style={{ padding: 2 }}>
                <CardItem>
                  <Grid>
                    <Grid.Row columns={2}>
                      <Grid.Column>
                        <Text
                          fontWeight={'bold'}
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          Assessor
                        </Text>

                        <Div top={8}>
                          <Image
                            src="../../static/images/user@3x.png"
                            style={{
                              width: 32,
                              height: 32,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />
                          <div>
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                            >
                              Busarakham
                            </Text>
                            <Text
                              fontSize={14}
                              color={colors.textDarkBlack}
                              style={{ marginTop: -4 }}
                            >
                              Senior Analyst
                            </Text>
                            <Div center top={8}>
                              <Image
                                src="../../static/images/iconDate@3x.png"
                                style={{
                                  marginRight: 8,
                                  width: 18,
                                  height: 18,
                                }}
                              />
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                              >
                                01/04/2020
                              </Text>
                            </Div>
                          </div>
                        </Div>
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontWeight={'bold'}
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          Reviewer
                        </Text>
                        <Div top={8}>
                          <Image
                            src="../../static/images/user@3x.png"
                            style={{
                              width: 32,
                              height: 32,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />
                          <div>
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                            >
                              Patcharamon
                            </Text>
                            <Text
                              fontSize={14}
                              color={colors.textDarkBlack}
                              style={{ marginTop: -4 }}
                            >
                              Division Manager (S-RC-IC)
                            </Text>
                            <Div center top={8}>
                              <Image
                                src="../../static/images/iconDate@3x.png"
                                style={{
                                  marginRight: 8,
                                  width: 18,
                                  height: 18,
                                }}
                              />
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                              >
                                01/04/2020
                              </Text>
                            </Div>
                          </div>
                        </Div>
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                      <Grid.Column>
                        <Text
                          fontWeight={'bold'}
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          Approver
                        </Text>

                        <Div top={8}>
                          <Image
                            src="../../static/images/user@3x.png"
                            style={{
                              width: 32,
                              height: 32,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />
                          <div>
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                            >
                              Chusak
                            </Text>
                            <Text
                              fontSize={14}
                              color={colors.textDarkBlack}
                              style={{ marginTop: -4 }}
                            >
                              Vice President (S-RC)
                            </Text>
                            <Div center top={8}>
                              <Image
                                src="../../static/images/iconDate@3x.png"
                                style={{
                                  marginRight: 8,
                                  width: 18,
                                  height: 18,
                                }}
                              />
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                              >
                                01/04/2020
                              </Text>
                            </Div>
                          </div>
                        </Div>
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontWeight={'bold'}
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          Division
                        </Text>
                        <Div top={8} center>
                          <Image
                            src="../../static/images/iconWork@3x.png"
                            style={{
                              width: 32,
                              height: 32,
                              marginRight: 8,
                              marginTop: 2,
                            }}
                          />

                          <Text fontSize={sizes.s} color={colors.primaryBlack}>
                            S-RC-IC
                          </Text>
                        </Div>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </CardItem>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
              <Grid.Column
                width={8}
                floated="left"
                style={{ padding: '0px 4px 0px 0px' }}
              >
                <Button.Group fluid>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 1 ? colors.textSky : colors.backgroundPrimary
                      }`,
                    }}
                    onClick={() => handelOpen(1)}
                  >
                    <Text
                      fontWeight={'bold'}
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 1 ? colors.backgroundPrimary : colors.textSky
                      }
                    >
                      Workflow
                    </Text>
                  </Button>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 2 ? colors.textSky : colors.backgroundPrimary
                      }`,
                    }}
                    onClick={() => handelOpen(2)}
                  >
                    <Text
                      fontWeight={'bold'}
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 2 ? colors.backgroundPrimary : colors.textSky
                      }
                    >
                      Assessment
                    </Text>
                  </Button>
                </Button.Group>
              </Grid.Column>
              <Grid.Column
                width={8}
                floated="right"
                style={{ paddingRight: 0 }}
              >
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                  }}
                >
                  <Popup
                    position="top right"
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      minWidth: 100,
                      height: 32,
                      padding: '4px 15px',
                      borderRadius: '6px 6px 0px 6px',
                      alignItems: 'center',
                    }}
                    trigger={
                      <Button
                        style={{
                          backgroundColor: colors.textPurple,
                          padding: '7px 24px',
                          borderRadius: '6px 6px 0px 6px',
                        }}
                      >
                        <Image
                          src="../../static/images/Finding@3x.png"
                          style={{ width: 24, height: 24 }}
                        />
                      </Button>
                    }
                    flowing
                    basic
                    hoverable
                  >
                    <Div mid center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={'med'}
                        color={colors.primary}
                      >
                        AI Filnding
                      </Text>
                    </Div>
                  </Popup>

                  <Popup
                    position="top right"
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      minWidth: 100,
                      height: 32,
                      padding: '4px 15px',
                      borderRadius: '6px 6px 0px 6px',
                      alignItems: 'center',
                    }}
                    trigger={
                      <Button
                        style={{
                          backgroundColor: colors.textSky,
                          padding: '7px 24px',
                          borderRadius: '6px 6px 0px 6px',
                        }}
                        onClick={() =>
                          (window.location.href =
                            '/Assessment/assessmentListVP')
                        }
                      >
                        <Image
                          src="../../static/images/Compliance-universe@3x.png"
                          style={{ width: 24, height: 24 }}
                        />
                      </Button>
                    }
                    flowing
                    basic
                    hoverable
                  >
                    <Div mid center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={'med'}
                        color={colors.primary}
                      >
                        Compliance Universe
                      </Text>
                    </Div>
                  </Popup>
                  <Popup
                    position="top right"
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      minWidth: 100,
                      height: 32,
                      padding: '4px 15px',
                      borderRadius: '6px 6px 0px 6px',
                      alignItems: 'center',
                    }}
                    trigger={
                      <Button
                        style={{
                          backgroundColor: colors.orange,
                          padding: '7px 24px',
                          borderRadius: '6px 6px 0px 6px',
                        }}
                      >
                        <Image
                          src="../../static/images/risk-white@3x.png"
                          style={{ width: 24, height: 24 }}
                        />
                      </Button>
                    }
                    flowing
                    basic
                    hoverable
                  >
                    <Div mid center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={'med'}
                        color={colors.primary}
                      >
                        Risk and Control Library
                      </Text>
                    </Div>
                  </Popup>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {isOpen === 2 ? <CardMenuAssessmentVP /> : <CardMenuWorkFlow />}
            </Grid.Row>
          </Grid>
        </div>
      </CardContainer>
    </Div>
  );
};

export default withLayout(observer(index));
