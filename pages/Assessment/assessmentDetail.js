import React, { useState, useEffect } from 'react';
import {
  Image,
  Grid,
  Button,
  Segment,
  Popup,
  Form,
  TextArea,
} from 'semantic-ui-react';
import loadingStore from '../../contexts/LoadingStore';
import Router, { useRouter } from 'next/router';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import moment from 'moment';
import {
  Text,
  BottomBarAssessment,
  RadioBox,
  StatusAll,
  ModalGlobal,
  ButtonBorder,
  RoadmapNextYear,
  Box,
  CheckBoxAll,
} from '../../components/element';
import {
  CardMenuAssessment,
  CardMenuWorkFlow,
} from '../../components/AssessmentPage';
import { colors, sizes } from '../../utils';
import { withLayout } from '../../hoc';
import {
  AssessmentDetailModel,
  RiskAndControlMappingModel,
} from '../../model/AssessmentModel';
import { initAuthStore } from '../../contexts';
import {
  APPROVE_ASSESSMENT_READY,
  APPROVE_ASSESSMENT_STATUS,
} from '../../model/AssessmentModel/ApproveAssessmentModel';
import { CloneAssessmentModal } from '../../components/AssessmentPage/modal';
import Link from 'next/link';

const CardItem = styled(Segment)`
  width: 100% !important;
  min-width: 100% !important;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 24px !important;
  flex-direction: column;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 272px;
  }

  @media only screen and (min-width: 1024px) {
    min-height: 318px;
  }
  @media only screen and (min-width: 1300px) {
    min-height: 299px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 280px;
  }
`;

const CardTab = styled.div`
  margin-left: -24px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  position: fixed;
`;

const TabLeft = styled.div`
  height: 36px;
  width: 36px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 1px;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  border-radius: ${(props) =>
    props.rightTop
      ? '0px 6px 0px 0px'
      : props.leftBottom
      ? '0px 0px 6px 0px'
      : '0px'};
`;

const CardContainer = styled.div`
  margin-bottom: 98px;
  padding-left: 16px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const TabBack = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  border-radius: 0px 6px 0px 0px !important;
`;

const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-bottom: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px !important;
  width: 100%;
`;

const customStyle = {
  styleTextArea: {
    borderRadius: `6px 6px 0px 6px`,
    padding: 8,
    height: 'auto',
    minHeight: 200,
    border: `1px solid ${colors.backgroundSecondary}`,
  },
};

const index = (props) => {
  const authContext = initAuthStore();
  const assessmentDetailModel = useLocalStore(
    () => new AssessmentDetailModel(),
  );
  const [isOpen, setIsOpen] = useState('Assessment');
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [isIds, setIds] = useState();
  const [isTab, setTab] = useState('');
  const [confirm, setConfirm] = useState(false);

  const rcMappingControl = useLocalStore(
    () => new RiskAndControlMappingModel(),
  );

  useEffect(() => {
    if (Router.query.tab === 'SUMMARY') {
      assessmentDetailModel.setField('tab', 'SUMMARY');
    }

    assessmentDetailModel.getAssessmentDetail(
      authContext.accessToken,
      Router.query.id,
      Router.query.tab,
      Router.query.mode
    );
  }, []);

  useEffect(() => {
    assessmentDetailModel.getAssessmentComment(
      authContext.accessToken,
      Router.query.id
    );

    setTab(Router.query.tab);
  }, [Router.query]);

  const saveData = async () => {
    try {
      const result = await assessmentDetailModel.updateAssessmentDetail(
        authContext.accessToken,
      );

      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const setAlertModal = (ids, btnRight) => {
    setIds(ids);
    setConfirmDelete(btnRight);
  };

  const handelDeleteComment = async () => {
    setConfirmDelete(false);
    if (isIds) {
      loadingStore.setIsLoading(true);
      try {
        const result = await assessmentDetailModel.deleteComment(
          authContext.accessToken,
          isIds,
        );

        await assessmentDetailModel.getAssessmentComment(
          authContext.accessToken,
          Router.query.id,
        );

        loadingStore.setIsLoading(false);

        return result;
      } catch (e) {
        console.log(e);
      }
    }
  };

  const handelCallBack = () => {
    setTimeout(() => {
      Router.back();
      setConfirm(false);
    }, 500);
  };

  // ใช้สำหรับ role ic-agent และ compliance universe ในการแสดงกล่อง comment
  const enableCommentBox = () => {
    const isAuthority =
      authContext.roles.isIcAgent || authContext.roles.isCompliance;

    if (isAuthority && isTab === 'SUMMARY') {
      return true;
    }

    return false;
  };

  const [toggle, setToggle] = useState('');

  // navigate ไปที่ obj และ risk
  const ScrollToSuggestion = ({ children, navId, type, no }) => {
    const handleClickNavigate = (event) => {
      const anchor = (event.target.ownerDocument || document).querySelector(
        `#${navId}`,
      );

      if (anchor) {
        anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
    };

    return (
      <div
        onClick={handleClickNavigate}
        role="presentation"
        onMouseEnter={() => type === 'O' && setToggle(navId)}
        // onMouseLeave={() => setToggle('')}
      >
        {children}
      </div>
    );
  };

  const roleAuthorityIAFinding = () => {
    if (
      !authContext.roles.isAdmin &&
      !authContext.roles.isDM &&
      !authContext.roles.isVP &&
      !authContext.roles.isIcAgent &&
      !authContext.roles.isCompliance
    ) {
      return false;
    }

    return true;
  };

  useEffect(() => {
    if (!assessmentDetailModel.isSetAllObjectData) {
      assessmentDetailModel.setField('isReadyToSubmit', false);
    }
  }, [assessmentDetailModel.isSetAllObjectData]);

  // useEffect(() => {
  //   rcMappingControl.getRiskAndControlListMapping(
  //     1,
  //     50,
  //     assessmentDetailModel.standardFullPath || '',
  //   );
  // }, [assessmentDetailModel.standardFullPath]);

  return useObserver(() => (
    <Div>
      <CardTab>
        {assessmentDetailModel &&
          assessmentDetailModel.navigationModel.map((obj) => (
            <>
              {obj.type === 'O' && (
                <>
                  <ScrollToSuggestion
                    navId={obj.id}
                    type={obj.type}
                    no={obj.no}
                  >
                    <TabLeft bgcolor={obj.overAllRatingColor} className="click">
                      <Text
                        color={colors.primaryBackground}
                        fontWeight="bold"
                        fontSize={sizes.xxs}
                      >
                        {`${obj.no}`}
                      </Text>
                    </TabLeft>
                  </ScrollToSuggestion>
                </>
              )}
              {obj.type === 'R' && obj.trackRiskNav === toggle && (
                <>
                  <ScrollToSuggestion
                    navId={obj.id}
                    type={obj.type}
                    no={obj.no}
                  >
                    <TabLeft bgcolor={obj.overAllRatingColor} className="click">
                      <Text
                        color={colors.primaryBackground}
                        fontWeight="bold"
                        fontSize={sizes.xxs}
                      >
                        {`${obj.no}`}
                      </Text>
                    </TabLeft>
                  </ScrollToSuggestion>
                </>
              )}
            </>
          ))}
      </CardTab>

      <CardContainer>
        <div
          className="click"
          style={{ width: '10%', display: 'flex' }}
          onClick={() => setConfirm(true)}
        >
          <Image
            src="../../static/images/black@3x.png"
            style={{ marginRight: 12, width: 18, height: 18 }}
          />
          <Text
            color={colors.primaryBlack}
            fontWeight="bold"
            fontSize={sizes.xl}
          >
            Back
          </Text>
        </div>

        <div style={{ width: '100%', marginTop: 32 }}>
          <Grid style={{ padding: 0, margin: 0 }}>
            <Grid.Column
              tablet={16}
              computer={8}
              style={{ padding: 2, width: '100%' }}
            >
              <CardItem>
                <Text fontSize={sizes.xs} color={colors.textlightGray}>
                  PTT Global Chemical Public Company Limited
                </Text>
                <Text fontSize={sizes.xs} color={colors.textlightGray}>
                  Control Self-Assessment
                </Text>
                <Div between top={24}>
                  <StatusAll child={assessmentDetailModel} />
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    {assessmentDetailModel.roadMapImage ? (
                      <Image
                        src={assessmentDetailModel.roadMapImage}
                        style={{ width: 18, height: 18, marginRight: 8 }}
                      />
                    ) : (
                      <RoadmapNextYear
                        year={assessmentDetailModel.roadmapType}
                      />
                    )}

                    <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                      {assessmentDetailModel.roadMapText}
                    </Text>
                    <div
                      style={{
                        margin: '0px 8px',
                        height: 20,
                        width: 1,
                        backgroundColor: colors.btGray,
                      }}
                    />
                    <Text color={colors.textlightGray} fontSize={sizes.xs}>
                      Last Update :{' '}
                      {moment(assessmentDetailModel.updateDate)
                        .locale('th')
                        .format('L')}
                    </Text>
                  </div>
                </Div>

                <Text
                  style={{ lineHeight: 1, paddingTop: 24 }}
                  fontWeight="bold"
                  fontSize={sizes.xl}
                  color={colors.primaryBlack}
                >
                  {`${assessmentDetailModel.no} ${assessmentDetailModel.name}`}
                </Text>
              </CardItem>
            </Grid.Column>
            <Grid.Column
              tablet={16}
              computer={8}
              style={{ padding: 2, width: '100%' }}
            >
              <CardItem>
                <Grid>
                  <Grid.Row columns={2}>
                    <Grid.Column>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.s}
                        color={colors.primaryBlack}
                      >
                        Assessor
                      </Text>

                      <Div top={8}>
                        <Image
                          src="../../static/images/user@3x.png"
                          style={{
                            width: 32,
                            height: 32,
                            marginRight: 8,
                            marginTop: 2,
                          }}
                        />
                        <div>
                          <Text fontSize={sizes.s} color={colors.primaryBlack}>
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.worker.name}
                          </Text>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.worker.position}
                          </Text>
                          <Div center top={8}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                marginRight: 8,
                                width: 18,
                                height: 18,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                              assessmentDetailModel.info.worker.date
                                ? moment(assessmentDetailModel.info.worker.date)
                                    .locale('th')
                                    .format('L')
                                : '-'}
                            </Text>
                          </Div>
                        </div>
                      </Div>
                    </Grid.Column>
                    <Grid.Column>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.s}
                        color={colors.primaryBlack}
                      >
                        Reviewer
                      </Text>
                      <Div top={8}>
                        <Image
                          src="../../static/images/user@3x.png"
                          style={{
                            width: 32,
                            height: 32,
                            marginRight: 8,
                            marginTop: 2,
                          }}
                        />
                        <div>
                          <Text fontSize={sizes.s} color={colors.primaryBlack}>
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.name}
                          </Text>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.position}
                          </Text>
                          <Div center top={8}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                marginRight: 8,
                                width: 18,
                                height: 18,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                              assessmentDetailModel.info.reviewer.date
                                ? moment(
                                    assessmentDetailModel.info.reviewer.date,
                                  )
                                    .locale('th')
                                    .format('L')
                                : '-'}
                            </Text>
                          </Div>
                        </div>
                      </Div>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row columns={2}>
                    <Grid.Column>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.s}
                        color={colors.primaryBlack}
                      >
                        Approver
                      </Text>

                      <Div top={8}>
                        <Image
                          src="../../static/images/user@3x.png"
                          style={{
                            width: 32,
                            height: 32,
                            marginRight: 8,
                            marginTop: 2,
                          }}
                        />
                        <div>
                          <Text fontSize={sizes.s} color={colors.primaryBlack}>
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.name}
                          </Text>
                          <Text
                            fontSize={14}
                            color={colors.textDarkBlack}
                            style={{ marginTop: -4 }}
                          >
                            {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.position}
                          </Text>
                          <Div center top={8}>
                            <Image
                              src="../../static/images/iconDate@3x.png"
                              style={{
                                marginRight: 8,
                                width: 18,
                                height: 18,
                              }}
                            />
                            <Text
                              fontSize={sizes.xxs}
                              color={colors.primaryBlack}
                            >
                              {assessmentDetailModel.info &&
                              assessmentDetailModel.info.approver.date
                                ? moment(
                                    assessmentDetailModel.info.approver.date,
                                  )
                                    .locale('th')
                                    .format('L')
                                : '-'}
                            </Text>
                          </Div>
                        </div>
                      </Div>
                    </Grid.Column>
                    {assessmentDetailModel.divEn && <Grid.Column>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.s}
                        color={colors.primaryBlack}
                      >
                        Division
                      </Text>
                      <Div top={8} center>
                        <Image
                          src="../../static/images/iconWork@3x.png"
                          style={{
                            width: 32,
                            height: 32,
                            marginRight: 8,
                            marginTop: 2,
                          }}
                        />

                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info &&
                            assessmentDetailModel.info.indicator}
                        </Text>
                      </Div>
                      <Text fontSize={sizes.s} color={colors.primaryBlack}>
                        {`${assessmentDetailModel.divEn || '-'} `}
                      </Text>
                    </Grid.Column>}
                    {assessmentDetailModel.shiftEn && !assessmentDetailModel.divEn && <Grid.Column>
                      <Text
                        fontWeight="bold"
                        fontSize={sizes.s}
                        color={colors.primaryBlack}
                      >
                        Shift
                      </Text>
                      <Div top={8} center>
                        <Image
                          src="../../static/images/iconWork@3x.png"
                          style={{
                            width: 32,
                            height: 32,
                            marginRight: 8,
                            marginTop: 2,
                          }}
                        />

                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {assessmentDetailModel.info &&
                            assessmentDetailModel.info.indicator}
                        </Text>
                      </Div>
                      <Text fontSize={sizes.s} color={colors.primaryBlack}>
                        {` ${assessmentDetailModel.shiftEn || ''}`}
                      </Text>
                    </Grid.Column>}
                  </Grid.Row>
                </Grid>
              </CardItem>
            </Grid.Column>
            <Grid.Row columns={2}>
              <Grid.Column
                width={8}
                floated="left"
                style={{ padding: '0px 4px 0px 0px' }}
              >
                <Button.Group fluid>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 'Workflow'
                          ? colors.textSky
                          : colors.backgroundPrimary
                      }`,
                      border: assessmentDetailModel
                        .validateAssessmentDetailModel.validateWorkflowFiles
                        ? '2px solid red'
                        : 'none',
                    }}
                    onClick={() => setIsOpen('Workflow')}
                  >
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 'Workflow'
                          ? colors.backgroundPrimary
                          : colors.textSky
                      }
                    >
                      Workflow
                    </Text>
                  </Button>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 'Assessment'
                          ? colors.textSky
                          : colors.backgroundPrimary
                      }`,
                    }}
                    onClick={() => setIsOpen('Assessment')}
                  >
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 'Assessment'
                          ? colors.backgroundPrimary
                          : colors.textSky
                      }
                    >
                      Assessment
                    </Text>
                  </Button>
                </Button.Group>
              </Grid.Column>
              <Grid.Column
                width={8}
                floated="right"
                style={{ paddingRight: 0 }}
                verticalAlign="middle"
              >
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}
                >
                  {assessmentDetailModel.lastYearPath && (
                    <Popup
                      position="top center"
                      trigger={
                        <a
                          href={assessmentDetailModel.lastYearPath}
                          target="_blank"
                          rel="noopener noreferrer"
                          className="click"
                          style={{ marginRight: '16px' }}
                        >
                          <ButtonBorder
                            icon="../../static/images/iconList-active@3x.png"
                            iconSize={24}
                            borderColor={colors.primary}
                          />
                        </a>
                      }
                      content={
                        <Text
                          fontSize={sizes.xxs}
                          color={colors.primary}
                          fontWeight="med"
                        >
                          CSA Last Year
                        </Text>
                      }
                    />
                  )}

                  <div>
                    {authContext.roles.isAdmin &&
                      assessmentDetailModel.sideButton.iaFinding && (
                        <Popup
                          position="top right"
                          style={{
                            display: 'flex',
                            justifyContent: 'center',
                            minWidth: 100,
                            height: 32,
                            padding: '4px 15px',
                            borderRadius: '6px 6px 0px 6px',
                            alignItems: 'center',
                          }}
                          trigger={
                            <div>
                              <Link
                                href={`/Assessment/assessmentListIAFinding`}
                              >
                                <a target="_blank">
                                  <Button
                                    style={{
                                      backgroundColor: colors.textPurple,
                                      padding: '7px 24px',
                                      borderRadius: '6px 6px 0px 6px',
                                    }}
                                  >
                                    <Image
                                      src="../../static/images/Finding@3x.png"
                                      style={{ width: 24, height: 24 }}
                                    />
                                  </Button>
                                </a>
                              </Link>
                            </div>
                          }
                          flowing
                          basic
                          hoverable
                        >
                          <Div mid center>
                            <Text
                              fontSize={sizes.xxs}
                              fontWeight="med"
                              color={colors.primary}
                            >
                              IA Finding
                            </Text>
                          </Div>
                        </Popup>
                      )}
                  </div>

                  {/* {assessmentDetailModel.sideButton.compliance && (
                    <Popup
                      position="top right"
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        minWidth: 100,
                        height: 32,
                        padding: '4px 15px',
                        borderRadius: '6px 6px 0px 6px',
                        alignItems: 'center',
                      }}
                      trigger={
                        <div>
                          <Link
                            href={`/databasePage/complianceMenagment?division=${assessmentDetailModel.divisionNo}`}
                          >
                            <a target="_blank">
                              <Button
                                style={{
                                  backgroundColor: colors.textSky,
                                  padding: '7px 24px',
                                  borderRadius: '6px 6px 0px 6px',
                                }}
                              >
                                <Image
                                  src="../../static/images/Compliance-universe@3x.png"
                                  style={{ width: 24, height: 24 }}
                                />
                              </Button>
                            </a>
                          </Link>
                        </div>
                      }
                      flowing
                      basic
                      hoverable
                    >
                      <Div mid center>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="med"
                          color={colors.primary}
                        >
                          Compliance Universe
                        </Text>
                      </Div>
                    </Popup>
                  )} */}
                  {assessmentDetailModel.sideButton.rcLibrary && (
                    <Popup
                      position="top right"
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        minWidth: 100,
                        height: 32,
                        padding: '4px 15px',
                        borderRadius: '6px 6px 0px 6px',
                        alignItems: 'center',
                      }}
                      trigger={
                        <div>
                          <Link
                            href={`/databasePage/riskAndControlManagment?noLvl=${assessmentDetailModel.no}`}
                          >
                            <a target="_blank">
                              <Button
                                style={{
                                  backgroundColor: colors.orange,
                                  padding: '7px 24px',
                                  borderRadius: '6px 6px 0px 6px',
                                }}
                              >
                                <Image
                                  src="../../static/images/risk-white@3x.png"
                                  style={{ width: 24, height: 24 }}
                                />
                              </Button>
                            </a>
                          </Link>
                        </div>
                      }
                      flowing
                      basic
                      hoverable
                    >
                      <Div mid center>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="med"
                          color={colors.primary}
                        >
                          Risk and Control Library
                        </Text>
                      </Div>
                    </Popup>
                  )}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {assessmentDetailModel && isOpen === 'Assessment' ? (
                <>
                  {assessmentDetailModel.objects.map(
                    (objectItem, objectIndex) => (
                      <div style={{ width: '100%', marginBottom: '24px' }}>
                        <CardMenuAssessment
                          assessmentDetailModel={assessmentDetailModel}
                          // data={riskandControl}
                          objects={objectItem}
                          objectIndex={`obj${objectIndex + 1}`}
                        />
                      </div>
                    ),
                  )}
                </>
              ) : (
                <CardMenuWorkFlow
                  workflow={assessmentDetailModel.workflow}
                  assessmentDetailModel={assessmentDetailModel}
                />
              )}
            </Grid.Row>

            <Grid.Row centered>
              {assessmentDetailModel.departmentNo && (
                <CloneAssessmentModal
                  assessmentDetailModel={assessmentDetailModel}
                  disabled={!assessmentDetailModel.canUpdate.add}
                />
              )}
            </Grid.Row>

            {isOpen === 'Assessment' && (
              <Grid.Row>
                {assessmentDetailModel.commentList.length > 0 &&
                  assessmentDetailModel.commentList.map((item) => (
                    <CardComment style={{ marginTop: '16px' }}>
                      <Box horizontal="row" spaceBetween>
                        <Text
                          color={colors.primaryBlack}
                          fontSize={sizes.xs}
                          fontWeight="med"
                        >
                          {item.commentFrom}
                        </Text>
                        <div
                          className="click"
                          onClick={() => setAlertModal(item.id, true)}
                        >
                          <Image
                            height={18}
                            width={18}
                            style={{ marginLeft: 16 }}
                            src="/static/images/delete-gray@3x.png"
                          />
                        </div>
                      </Box>
                      <Form>
                        <TextArea autoHeight value={item.comment} />
                      </Form>
                      <div
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'flex-end',
                        }}
                      >
                        <div
                          style={{
                            display: 'flex',
                            fontStyle: 'italic',
                          }}
                        >
                          <Text
                            style={{ fontStyle: 'italic' }}
                            color={colors.textlightGray}
                            fontSize={sizes.xs}
                          >
                            By{' '}
                          </Text>
                          <Text
                            color={colors.textlightGray}
                            style={{
                              paddingLeft: 8,
                              fontStyle: 'italic',
                            }}
                            fontSize={sizes.xxs}
                          >
                            {`${item.commentBy}`}
                          </Text>
                        </div>
                      </div>
                    </CardComment>
                  ))}
              </Grid.Row>
            )}

            {(isTab !== 'SUMMARY' || enableCommentBox()) && (
              <Grid.Row>
                {isOpen === 'Assessment' && (
                  <>
                    <CardComment>
                      <Text
                        color={colors.primaryBlack}
                        fontSize={sizes.xs}
                        fontWeight="med"
                        className="upper"
                      >
                        Comment
                      </Text>
                      <Form>
                        <TextArea
                          style={customStyle.styleTextArea}
                          placeholder="Key Comment"
                          onChange={(e) =>
                            assessmentDetailModel.setField(
                              'assessmentComment',
                              e.target.value,
                            )
                          }
                          value={assessmentDetailModel.assessmentComment}
                        />
                      </Form>
                    </CardComment>
                  </>
                )}
              </Grid.Row>
            )}
            <Grid.Row>
              {authContext.roles.isAdmin &&
                !Router.query.tab &&
                assessmentDetailModel.approveStatus.status !==
                  APPROVE_ASSESSMENT_READY.accept &&
                assessmentDetailModel.status !==
                  APPROVE_ASSESSMENT_STATUS.reject.minor &&
                assessmentDetailModel.status !==
                  APPROVE_ASSESSMENT_STATUS.reject.major && (
                  <div style={{ display: 'flex', marginTop: '16px' }}>
                    <div style={{ marginRight: '16px', display: 'flex' }}>
                      <RadioBox
                        onChange={() =>
                          assessmentDetailModel.approveStatus.setId(
                            APPROVE_ASSESSMENT_STATUS.reject.major,
                            assessmentDetailModel.Id,
                          )
                        }
                        checked={
                          assessmentDetailModel.approveStatus.isSelectedMajor
                        }
                        disabled={assessmentDetailModel.isReadyToSubmit}
                      />
                      <Text
                        className="upper"
                        fontSize={sizes.s}
                        fontWeight="med"
                        color={colors.textBlack}
                        style={{ paddingLeft: 8 }}
                      >
                        MAJOR CHANGE
                      </Text>
                    </div>

                    <div style={{ marginRight: '16px', display: 'flex' }}>
                      <RadioBox
                        onChange={() =>
                          assessmentDetailModel.approveStatus.setId(
                            APPROVE_ASSESSMENT_STATUS.reject.minor,
                            assessmentDetailModel.Id,
                          )
                        }
                        checked={
                          assessmentDetailModel.approveStatus.isSelectedMinor
                        }
                        disabled={assessmentDetailModel.isReadyToSubmit}
                      />
                      <Text
                        className="upper"
                        fontSize={sizes.s}
                        fontWeight="med"
                        color={colors.textBlack}
                        style={{ paddingLeft: 8 }}
                      >
                        MINOR CHANGE
                      </Text>
                    </div>
                  </div>
                )}
                {authContext.roles.isAdmin &&
                <div style={{ marginTop: '16px', display: 'flex', position: 'absolute', right:'0px'}}>
                  <div style={{ marginRight: '16px', display: 'flex' }}>
                    <CheckBoxAll
                    onChange={() => {
                        assessmentDetailModel.setField(
                          'verifyCompliance',
                          !assessmentDetailModel.verifyCompliance,
                        );
                      }}
                    checked={assessmentDetailModel.verifyCompliance}
                    />
                    <Text
                      className="upper"
                      fontSize={sizes.s}
                      fontWeight="med"
                      color={colors.textBlack}
                      style={{ paddingLeft: 8 }}
                    >
                      Verify Compliance objective
                    </Text>
                  </div>
                </div>
                }
            </Grid.Row>
          </Grid>
        </div>
      </CardContainer>

      <BottomBarAssessment
        page="AssessmentDetail"
        saveButtonData={saveData}
        readyToSubmitData={assessmentDetailModel}
        assessmentDetailModel={assessmentDetailModel}
      />
      {/* call Back */}
      <ModalGlobal
        open={confirmDelete}
        title="Are you sure !"
        content="You want to delete this item?"
        onSubmit={handelDeleteComment}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirmDelete(false)}
      />

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to leave this page?"
        content="Please ensure that you have saved the information before you leave."
        onSubmit={handelCallBack}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
    </Div>
  ));
};

export default withLayout(observer(index));
