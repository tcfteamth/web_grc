import React, { useState, useEffect } from 'react';
import {
  Grid,
  Loader,
  Dimmer,
  Icon,
  Header,
  Popup,
  Image,
} from 'semantic-ui-react';
import Router from 'next/router';
import { useLocalStore, useObserver, observer } from 'mobx-react';
import styled from 'styled-components';
import _ from 'lodash';
import { withLayout } from '../../hoc';
import {
  Text,
  DropdownAll,
  BottomBarDatabase,
  SearchInput,
  ButtonAll,
  ButtonBorder,
} from '../../components/element';
import request from '../../services';
import { RiskAndControlList } from '../../components/DatabasePage';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { RiskAndControlModel } from '../../model/ProcessDBModel';

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.right ? 'flex-end' : props.between ? 'space-between' : 'flex-start'};
`;

const OBJECT_TYPES = [
  {
    key: 'Operation',
    label: 'ด้านการดำเนินงาน (Operations)',
    value: 'ด้านการดำเนินงาน (Operations)',
  },
  {
    key: 'Compliance',
    label: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
    value: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
  },
  {
    key: 'Reporting',
    label: 'ด้านการรายงาน (Reporting)',
    value: 'ด้านการรายงาน (Reporting)',
  },
];

const index = () => {
  const riskAndControlModel = useLocalStore(() => new RiskAndControlModel());
  const authContext = initAuthStore();
  const [lvlFourOptions, setLvlFourOptions] = useState();

  const filter = () => {
    if (riskAndControlModel.mode === 'list') {
      riskAndControlModel.getRiskAndControlList(
        authContext.accessToken,
        riskAndControlModel.page,
        riskAndControlModel.searchText,
        riskAndControlModel.fullPath,
        riskAndControlModel.objectType.key,
      );
    } else {
      riskAndControlModel.getRiskAndControlByNoLvl(
        authContext.accessToken,
        riskAndControlModel.page,
        riskAndControlModel.searchText,
      );
    }
  };
  const getLvlList = async (no) => {
    await request.roadmapServices
      .getLvlList()
      .then((response) => {
        const lvlFourOptions = response.data.listLv4.map((lvlFour) => ({
          value: lvlFour.id,
          id: lvlFour.fullPath,
          label: `${lvlFour.fullPath} ${
            lvlFour.nameTh ? lvlFour.nameTh : lvlFour.nameEn
          }`,
        }));
        if (no) {
          riskAndControlModel.setField(
            'lvl4',
            lvlFourOptions.find((e) => e.id === no),
          );
        }
        setLvlFourOptions(lvlFourOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const clearFilter = () => {
    riskAndControlModel.filterModel.resetComplianceFilter();
    riskAndControlModel.setField('searchText', '');
    riskAndControlModel.setField('lvl4', '');
    riskAndControlModel.setField('fullPath', '');
    riskAndControlModel.setField('objectType', '');
    riskAndControlModel.getRiskAndControlList(authContext.accessToken, 1);
    getLvlList();
  };

  useEffect(() => {
    if (!Router.query.noLvl) {
      riskAndControlModel.setField('mode', 'list');
      riskAndControlModel.getRiskAndControlList(
        authContext.accessToken,
        riskAndControlModel.page,
      );
    }
  }, [riskAndControlModel.page]);

  useEffect(() => {
    if (Router.query.noLvl) {
      riskAndControlModel.setField('mode', 'noLvl');
      riskAndControlModel.getRiskAndControlByNoLvl(Router.query.noLvl);
    }
    getLvlList(Router.query.noLvl);
  }, []);

  useEffect(() => {
    getLvlList(Router.query.noLvl);
  }, [riskAndControlModel]);

  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <div
        className="click"
        style={{ width: '10%' }}
        onClick={() => Router.back()}
      >
        <Header>
          <Icon name="angle left" />
          <Header.Content>
            <Text color={colors.primaryBlack} fontSize={sizes.xxl}>
              Back
            </Text>
          </Header.Content>
        </Header>
      </div>
      <div>
        <Card top={32}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column floated="left">
                <Text
                  fontWeight="bold"
                  fontSize={sizes.xl}
                  color={colors.primaryBlack}
                >
                  Risk and Control Library
                </Text>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column computer={8}>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Sub-Process (Level 4)
                </Text>
                <DropdownAll
                  placeholder="Select Sub-Process"
                  options={lvlFourOptions && lvlFourOptions}
                  value={riskAndControlModel.lvl4}
                  handleOnChange={(e) => {
                    riskAndControlModel.setField('lvl4', e);
                    riskAndControlModel.setField('fullPath', e.id);
                  }}
                />
              </Grid.Column>
              <Grid.Column computer={8}>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Objective Type
                </Text>
                <DropdownAll
                  placeholder="Select Objective Type"
                  options={OBJECT_TYPES}
                  handleOnChange={(e) => {
                    riskAndControlModel.setField('objectType', e);
                  }}
                  value={riskAndControlModel.objectType}
                />
              </Grid.Column>

              <Grid.Column computer={2}>
                <div style={{ display: 'flex', marginTop: 8 }}>
                  <ButtonAll text="Filter" onClick={() => filter()} />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => clearFilter()}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Card>

        <Div right center top={24}>
          <SearchInput
            placeholder="Search"
            onChange={(e) => {
              riskAndControlModel.setField('searchText', e.target.value);
              if (!riskAndControlModel.searchText) {
                filter();
              }
            }}
            onEnterKeydown={(e) => {
              if (e.keyCode === 13) {
                filter();
              }
            }}
            value={riskAndControlModel.searchText}
          />
          <Popup
            trigger={
              <Image
                src="../../static/images/iconRemark@3x.png"
                style={{ marginRight: 12, width: 18, height: 18 }}
              />
            }
            content="Search for Sub-Process(Level 4)"
            position="top right"
          />
        </Div>

        <div style={{ paddingTop: 16 }}>
          <RiskAndControlList
            token={authContext.accessToken}
            riskAndControlModel={riskAndControlModel}
            modeType={riskAndControlModel.mode}
          />
        </div>
      </div>

      <BottomBarDatabase page="riskAndControl" RCModel={riskAndControlModel} />
    </div>
  ));
};

export default withLayout(observer(index));
