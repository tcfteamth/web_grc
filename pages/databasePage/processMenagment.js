import React, { useState, useEffect } from 'react';
import {
  Grid,
  Loader,
  Dimmer,
  Header,
  Icon,
  Image,
  Popup,
} from 'semantic-ui-react';
import Router from 'next/router';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';

import { withLayout } from '../../hoc';
import {
  Text,
  DropdownAll,
  BottomBarDatabase,
  SearchInput,
  ButtonAll,
  ButtonBorder,
  DropdownSelect,
} from '../../components/element';
import { AdminProcessList } from '../../components/DatabasePage';
import { colors, sizes } from '../../utils';
import { initProcessDBContext, initAuthStore } from '../../contexts';
import { taglistDropdownStore } from '../../model';

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 16}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? 'center'
        : props.right
        ? 'flex-end'
        : props.mid
        ? 'center'
        : props.between
        ? 'space-between'
        : 'flex-start'};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const CardBox = styled.div`
  display: flex;
  flexdirection: row;
  alignitems: center;
  min-hight: 38px;
  margin-bottom: 16px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Cardtop = styled.div`
  border-radius: ${(props) =>
    props.right
      ? '0px 6px 6px 0px'
      : props.center
      ? '0px 0px 0px 0px'
      : props.left
      ? '6px 0px 0px 6px'
      : '2px'};
  border-left: ${(props) => (props.center ? '1px solid #eaeaea' : 'none')};
  border-right: ${(props) => (props.center ? '1px solid #eaeaea' : 'none')};
  display: flex;
  padding: 8px 16px;
  flex-direction: row;
  align-items: center;
  align-self: center;
  justify-content: center;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  min-width: 269px;
  min-height: 39px;
`;

const index = () => {
  const processDBContext = useLocalStore(() => initProcessDBContext);
  const [selectLvl2, setSelectLvl2] = useState();
  const [selectLvl3, setSelectLvl3] = useState();
  const [selectLvl4, setSelectLvl4] = useState();
  
  const authContext = initAuthStore();
  const lvl2s = [];
  const lvl3s = [];
  const lvl4s = [];
  const tagListDropdown = useLocalStore(() => taglistDropdownStore);
  const [tagsList, setTagList] = useState();
  processDBContext.processList?.map((lvl1) =>
    lvl1.children.map((lvl2) =>
      lvl2s.push({
        key: lvl2.no + ' ' + lvl2.name,
        label: lvl2.no + ' ' + lvl2.name,
        value: lvl2.no,
        lvl: lvl2.lvl,
        children: lvl2.children,
      }),
    ),
  );

  lvl2s?.map((lvl2) =>
    lvl2.children.map((lvl3) =>
      lvl3s.push({
        key: lvl3.no + ' ' + lvl3.name,
        label: lvl3.no + ' ' + lvl3.name,
        value: lvl3.no,
        lvl: lvl3.lvl,
        fromLvl2: lvl2.value,
        children: lvl3.children,
      }),
    ),
  );

  lvl3s?.map((lvl3) =>
    lvl3.children.map((lvl4) =>
      lvl4s.push({
        key: lvl4.no + ' ' + lvl4.name,
        label: lvl4.no + ' ' + lvl4.name,
        value: lvl4.no,
        lvl: lvl4.lvl,
        fromLvl3: lvl3.value,
      }),
    ),
  );

  const resetFitler = (lvl) => {
    if (lvl === 2) {
      setSelectLvl3(null);
      setSelectLvl4(null);
      processDBContext.setSelectLvl3No('');
      processDBContext.setSelectLvl4No('');
    } else if (lvl === 3) {
      setSelectLvl4(null);
    }
  };

  const handelOpen = (value) => {
    processDBContext.setLvlOneType(value);
  };

  const submitFilter = async () => {
    await processDBContext.getProcessList();
    await getTagList();
    await processDBContext.setLvlOneType(
      parseInt(processDBContext.NewlvlOneType, 10),
    );
  };

  const clearFilter = async () => {
    setSelectLvl2(null);
    setSelectLvl3(null);
    setSelectLvl4(null);

    processDBContext.clearfiltter();
    await processDBContext.getProcessList();
    await processDBContext.setLvlOneType(1);
    await getTagList();
  };

  useEffect(() => {
    (async () => {
      await getTagList();
    })();
    processDBContext.setLvlOneType(1);
    return function cleanup() {
      processDBContext.clearSearchText();
    };
    
  }, []);

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };
  const handleProcessTagSelection = (e,data,field) => {
    let result = processDBContext[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    processDBContext.setField(
      field,
      result,
    );
  }
  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Header
        className="click"
        style={{ width: '10%' }}
        onClick={() => Router.back()}
      >
        <Icon name="angle left" />
        {/* <Image src="../../static/images/black.png" style={{ marginRight: 8 }} /> */}
        <Header.Content>
          <Text color={colors.primaryBlack} fontSize={sizes.xxl}>
            Back
          </Text>
        </Header.Content>
      </Header>
      <div>
        <Card top={32}>
          <div style={{ padding: 8, paddingBottom: 28 }}>
            <Text
              fontWeight="bold"
              fontSize={sizes.xl}
              color={colors.primaryBlack}
            >
              Process Database
            </Text>
          </div>
          <Grid style={{ padding: 0, margin: 0 }}>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={3}
              style={{ padding: 8 }}
            >
              <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Key Process (Level 2)
              </Text>
              <DropdownAll
                placeholder="Key Process (Level 2)"
                options={lvl2s}
                handleOnChange={(e) => {
                  processDBContext.setSelectLvl2No(e.value);
                  setSelectLvl2(e);
                  resetFitler(e.lvl);
                }}
                value={selectLvl2}
              />
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={3}
              style={{ padding: 8 }}
            >
              <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Process (Level 3)
              </Text>
              <DropdownAll
                placeholder="Process (Level 3)"
                options={
                  selectLvl2?.value
                    ? lvl3s.filter((lvl3) => lvl3.fromLvl2 === selectLvl2.value)
                    : lvl3s
                }
                handleOnChange={(e) => {
                  processDBContext.setSelectLvl3No(e.value);
                  setSelectLvl3(e);
                  resetFitler(e.lvl);
                }}
                value={selectLvl3}
              />
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={3}
              style={{ padding: 8 }}
            >
              <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Sub-Process (Level 4)
              </Text>

              <DropdownAll
                placeholder="Sub-Process (Level 4)"
                options={
                  selectLvl3?.value
                    ? lvl4s.filter((lvl4) => lvl4.fromLvl3 === selectLvl3.value)
                    : selectLvl2?.value
                    ? lvl4s.filter(
                        (lvl4) =>
                          lvl4.fromLvl3.substring(0, 3) === selectLvl2.value,
                      )
                    : lvl4s
                }
                handleOnChange={(e) => {
                  processDBContext.setSelectLvl4No(e.value);
                  setSelectLvl4(e);
                }}
                value={selectLvl4}
              />
            </Grid.Column>
            <Grid.Column
                 mobile={16}
                 tablet={8}
                 computer={3}
                    
              style={{ padding: 8 }}
                  >
                     <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                      Process Tag (AND)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={processDBContext.selectedProcessAnd}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
                    />
                  </Grid.Column>
                  <Grid.Column
                    mobile={16}
                    tablet={8}
                    computer={3}
                    
              style={{ padding: 8 }}
                  >
                         <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                      Process Tag (OR)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={processDBContext.selectedProcessOr}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
                    />
                  </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={4}
              style={{ paddingTop: 36, display: 'flex' }}
            >
              <ButtonAll
                onClick={submitFilter}
                width={96}
                color="#1b1464"
                text="Filter"
              />
              <ButtonBorder
                borderColor={colors.primary}
                textColor={colors.primary}
                text="Clear"
                handelOnClick={() => clearFilter()}
              />
            </Grid.Column>
          </Grid>
        </Card>

        <Div marginTop={16} between>
          <CardBox className="click">
            <Cardtop
              bgcolor={
                processDBContext.lvlOneType === 1
                  ? colors.textSky
                  : colors.backgroundPrimary
              }
              left
              onClick={() => handelOpen(1)}
            >
              <Text
                fontWeight={processDBContext.lvlOneType === 1 ? 'bold' : 'med'}
                fontSize={sizes.xs}
                style={{ textTransform: 'uppercase' }}
                color={
                  processDBContext.lvlOneType === 1
                    ? colors.backgroundPrimary
                    : colors.textSky
                }
              >
                1 - Management Process
              </Text>
            </Cardtop>
            <Cardtop
              bgcolor={
                processDBContext.lvlOneType === 2
                  ? colors.textSky
                  : colors.backgroundPrimary
              }
              center
              onClick={() => handelOpen(2)}
            >
              <Text
                fontWeight={processDBContext.lvlOneType === 2 ? 'bold' : 'med'}
                fontSize={sizes.xs}
                style={{ textTransform: 'uppercase' }}
                color={
                  processDBContext.lvlOneType === 2
                    ? colors.backgroundPrimary
                    : colors.textSky
                }
              >
                2 - Core Process
              </Text>
            </Cardtop>
            <Cardtop
              bgcolor={
                processDBContext.lvlOneType === 3
                  ? colors.textSky
                  : colors.backgroundPrimary
              }
              right
              onClick={() => handelOpen(3)}
            >
              <Text
                fontWeight={processDBContext.lvlOneType === 3 ? 'bold' : 'med'}
                fontSize={sizes.xs}
                style={{ textTransform: 'uppercase' }}
                color={
                  processDBContext.lvlOneType === 3
                    ? colors.backgroundPrimary
                    : colors.textSky
                }
              >
                3 - Support process
              </Text>
            </Cardtop>
          </CardBox>

          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <Div>
              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  processDBContext.setSearchProcessText(e.target.value);

                  if (processDBContext.searchProcessText.length === 0) {
                    processDBContext.getProcessList();
                  }
                }}
                value={processDBContext.searchProcessText}
                onEnterKeydown={(e) => {
                  if (e.keyCode === 13) {
                    processDBContext.getProcessList();
                  }
                }}
              />
            </Div>
            <Popup
              trigger={
                <Image
                  src="../../static/images/iconRemark@3x.png"
                  style={{ marginRight: 12, width: 18, height: 18 }}
                />
              }
              content="Search for Sub-Process(Level 4)"
              position="top right"
            />
          </div>
        </Div>
      </div>
      <AdminProcessList />
      <BottomBarDatabase page="processAdmin" RCModel={processDBContext} />
    </div>
  ));
};

export default withLayout(observer(index));
