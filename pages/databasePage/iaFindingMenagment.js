import React, { useState, useEffect } from 'react';
import { withLayout } from '../../hoc';
import {
  Image,
  Grid,
  Loader,
  Dimmer,
  Header,
  Icon,
  Popup,
} from 'semantic-ui-react';
import Link from 'next/link';
import Router from 'next/router';
import {
  Text,
  Dropdown,
  DropdownAll,
  BottomBarDatabase,
  SearchInput,
  BUDropdown,
  DepartmentDropdown,
  DivisionDropdown,
  ButtonAll,
  ButtonBorder,
  IAFindingTypeList,
} from '../../components/element';
import { FindingList } from '../../components/DatabasePage';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import { initAuthStore, initDataContext } from '../../contexts';
import { ListCSAReport, listType } from '../../utils/static';
import styled from 'styled-components';
import _ from 'lodash';
import { IAFindingListModel } from '../../model/ProcessDBModel';

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? 'center'
        : props.right
        ? 'flex-end'
        : props.mid
        ? 'center'
        : props.between
        ? 'space-between'
        : 'flex-start'};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const searchEnableField = [
  'Search For',
  '- Finding/Recommendation for CSA assessment',
  '- Finding Description',
  '- Finding Rating',
  '- Audit Engagement Name',
  '- Root Cause',
  '- Recommendation Description',
  '- Recommendation State',
  '- Team Lead',
  '- Team Member',
  '- Impact',
];

const index = (props) => {
  const authContext = initAuthStore();
  const iaFindingListModel = useLocalStore(() => new IAFindingListModel());

  const handelOpen = (value) => {
    console.log('value', value);
    setIsOpen(value);
  };

  const filterIAFinding = () => {
    iaFindingListModel.getIAFindingList(
      authContext.accessToken,
      1,
      iaFindingListModel.searchText,
      iaFindingListModel.filterModel.selectedBu,
      iaFindingListModel.filterModel.selectedDepartment.no,
      iaFindingListModel.filterModel.selectedDivision,
      iaFindingListModel.filterModel.selectedIAFindingType,
    );
  };

  const clearFilter = () => {
    iaFindingListModel.filterModel.resetFilterIAFinding();
    iaFindingListModel.setField('searchText', '');

    iaFindingListModel.getIAFindingList(authContext.accessToken, 1);
  };

  useEffect(() => {
    iaFindingListModel.getIAFindingList(authContext.accessToken);
  }, []);

  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <Header
        className="click"
        style={{ width: '10%' }}
        onClick={() => Router.back()}
      >
        <Icon name="angle left" />
        <Header.Content>
          <Text color={colors.primaryBlack} fontSize={sizes.xxl}>
            Back
          </Text>
        </Header.Content>
      </Header>

      <div>
        <Card top={32}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column floated="left">
                <Text
                  fontWeight="bold"
                  fontSize={sizes.xl}
                  color={colors.primaryBlack}
                >
                  IA Finding
                </Text>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  BU
                </Text>
                <BUDropdown
                  handleOnChange={(bu) => {
                    iaFindingListModel.filterModel.setField('selectedBu', bu);
                  }}
                  value={iaFindingListModel.filterModel.selectedBu}
                  disabled={iaFindingListModel.filterModel.isBuDisabled}
                  defaultValue="Select BU"
                  placeholder="Select BU"
                />
              </Grid.Column>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Department
                </Text>
                <DepartmentDropdown
                  handleOnChange={(department) => {
                    iaFindingListModel.filterModel.setField(
                      'selectedDepartment',
                      department,
                    );
                  }}
                  value={
                    iaFindingListModel.filterModel.selectedDepartment.value
                  }
                  disabled={iaFindingListModel.filterModel.isDepartmentDisabled}
                  selectedBu={iaFindingListModel.filterModel.selectedBu}
                  defaultValue="Select Department"
                  placeholder="Select Department"
                />
              </Grid.Column>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Division
                </Text>
                <DivisionDropdown
                  handleOnChange={(divisionNo) => {
                    if (iaFindingListModel.filterModel.resetBuMistake) {
                      iaFindingListModel.filterModel.setField('selectedBu', '');
                    }
                    iaFindingListModel.filterModel.setField(
                      'selectedDivision',
                      divisionNo,
                    );
                  }}
                  value={iaFindingListModel.filterModel.selectedDivision}
                  selectedDepartmentId={
                    iaFindingListModel.filterModel.selectedDepartment.value
                  }
                  defaultValue="Select Division"
                  placeholder="Select Division"
                />
              </Grid.Column>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Type
                </Text>
                <IAFindingTypeList
                  handleOnChange={(type) =>
                    iaFindingListModel.filterModel.setField(
                      'selectedIAFindingType',
                      type.value,
                    )
                  }
                  value={iaFindingListModel.filterModel.selectedIAFindingType}
                  defaultValue="Select Type"
                  placeholder="Select Type"
                />
              </Grid.Column>
              <Grid.Column>
                <div style={{ display: 'flex', marginTop: '30px' }}>
                  <ButtonAll text="Filter" onClick={() => filterIAFinding()} />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => clearFilter()}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Card>
        <Div right center top={24}>
          <SearchInput
            placeholder="Search"
            onChange={(e) => {
              iaFindingListModel.setField('searchText', e.target.value);

              if (!iaFindingListModel.searchText) {
                filterIAFinding();
              }
            }}
            onEnterKeydown={(e) => {
              if (e.keyCode === 13) {
                filterIAFinding();
              }
            }}
            value={iaFindingListModel.searchText}
          />
          <Popup
            trigger={
              <Image
                src="../../static/images/iconRemark@3x.png"
                style={{ marginRight: 12, width: 18, height: 18 }}
              />
            }
            position="top right"
          >
            {searchEnableField.map((field) => (
              <Text>{field}</Text>
            ))}
          </Popup>
        </Div>
      </div>
      <div style={{ marginTop: 16 }}>
        <FindingList iaFindingListModel={iaFindingListModel} />
        <BottomBarDatabase page="iaFinding" />
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
