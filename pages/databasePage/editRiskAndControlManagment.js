import React, { useEffect, useState } from 'react';
import { Grid, Loader, Dimmer, Icon, Header } from 'semantic-ui-react';
import Router from 'next/router';
import { useLocalStore, useObserver, observer } from 'mobx-react';
import styled from 'styled-components';
import _ from 'lodash';
import { withLayout } from '../../hoc';
import { initAuthStore } from '../../contexts';
import {
  Text,
  ProcessLvl4Dropdown,
  ProcessLvl3Dropdown,
  DropdownAll,
  BottomBarDatabase,
  ModalGlobal,
} from '../../components/element';
import { CreateRiskAndControl } from '../../components/DatabasePage';
import { colors, sizes } from '../../utils';
import { CRUDRiskAndControlModel } from '../../model/ProcessDBModel';
import request from '../../services';

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: flex-start;

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const index = () => {
  const editRCModel = useLocalStore(() => new CRUDRiskAndControlModel());
  const authContext = initAuthStore();
  const [confirm, setConfirm] = useState(false);

  const [lvlFourList, setLvlFourList] = useState();

  const getLvlFourList = async (lvlThreeNo) => {
    await request.roadmapServices
      .getLvlFourList(lvlThreeNo)
      .then((response) => {
        const lvlFourOptions = response.data.map((lvlFour) => ({
          value: lvlFour.no,
          label: `${lvlFour.no} ${lvlFour.name}`,
          id: lvlFour.id,
        }));
        setLvlFourList(lvlFourOptions);
        editRCModel.setField(
          'lvl4',
          lvlFourOptions.find((e) => e.value === editRCModel.no),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [lvlThreeList, setLvlThreeList] = useState();

  const getLvlThreeList = async () => {
    await request.roadmapServices
      .getLvlThreeList()
      .then((response) => {
        const lvlThreeOptions = response.data.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.no} ${lvlThree.name}`,
          id: lvlThree.id,
        }));
        setLvlThreeList(lvlThreeOptions);
        editRCModel.setField(
          'lvl3',
          lvlThreeOptions.find((e) => e.value === editRCModel.lvl3),
        );
        getLvlFourList(editRCModel.lvl3.value);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleAddObject = async (selectedLvl4) => {
    if (selectedLvl4) {
      editRCModel.addObject();
    }
  };

  const handelCallBack = () => {
    setTimeout(() => {
      Router.back();
      setConfirm(false);
    }, 500);
  };

  const handelInit = async () => {
    await editRCModel.getRiskAndControlById(
      authContext.accessToken,
      editRCModel.id,
    );
    await getLvlThreeList();
  };

  useEffect(() => {
    const getId = window.location.search;
    const id = new URLSearchParams(getId);
    editRCModel.setField('id', id.get('id'));
    handelInit();
  }, []);

  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>

      <div
        className="click"
        style={{ width: '10%' }}
        onClick={() => setConfirm(true)}
      >
        <Header>
          <Icon name="angle left" />
          <Header.Content>
            <Text color={colors.primaryBlack} fontSize={sizes.xxl}>
              Back
            </Text>
          </Header.Content>
        </Header>
      </div>
      <div>
        <Card top={32}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column floated="left">
                <Text
                  fontWeight="bold"
                  fontSize={sizes.xl}
                  color={colors.primaryBlack}
                >
                  Edit Risk and Control
                </Text>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Grid>
            <Grid.Row>
              <Grid.Column
                tablet={8}
                computer={8}
                mobile={16}
                style={{ marginTop: 6 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Process Lv.3
                </Text>
                <DropdownAll
                  placeholder="Select Process"
                  options={lvlThreeList}
                  value={editRCModel.lvl3}
                  handleOnChange={(e) => {
                    editRCModel.setField('lvl3', e);
                    getLvlFourList(editRCModel.lvl3.value);
                    editRCModel.setField('lvl4', '');
                  }}
                  isDisabled
                />
              </Grid.Column>
              <Grid.Column
                tablet={8}
                computer={8}
                mobile={16}
                style={{ paddingLeft: 0, marginTop: 6 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Sub-Process (Level 4)
                </Text>
                <DropdownAll
                  placeholder="Select Sub-Process"
                  options={lvlFourList && lvlFourList}
                  value={editRCModel.lvl4}
                  handleOnChange={(e) => {
                    editRCModel.setField('id', e.id);
                    editRCModel.setField('lvl4', e);
                    handelInit();
                  }}
                  isDisabled
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Card>

        {editRCModel &&
          editRCModel.objects.map((objectItem, objectIndex) => (
            <div style={{ width: '100%', marginBottom: '24px' }}>
              <CreateRiskAndControl
                objects={objectItem}
                objectIndex={`obj${objectIndex + 1}`}
                RCModel={editRCModel}
              />
            </div>
          ))}
      </div>

      <BottomBarDatabase page="editRiskAndControl" RCModel={editRCModel} />

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to leave this page?"
        content="Please ensure that you have saved the information before you leave."
        onSubmit={handelCallBack}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
    </div>
  ));
};

export default withLayout(observer(index));
