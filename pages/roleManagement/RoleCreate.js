/* eslint-disable react/jsx-fragments */
import React, { useState, useEffect } from 'react';
import {
  Header,
  Image,
  Grid,
  Loader,
  Dimmer,
  Icon,
  Pagination,
  Form,
} from 'semantic-ui-react';
import Router from 'next/router';
import Link from 'next/link';
import { observer, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import {
  Text,
  CheckBoxAll,
  RadioBox,
  Modal,
  DropdownAll,
  Cell,
  BottomBarRole,
  InputAll,
} from '../../components/element';
import { initDataContext, initAuthStore } from '../../contexts';
import { colors, sizes } from '../../utils';
import { menuRole, ListStaff, sortTypes } from '../../utils/static';
import { withLayout } from '../../hoc';
import { RoleModel } from '../../model/Role';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50px;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  padding: 10px 24px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #d9d9d6;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 1px 0px;
  height: 10%;
  border-top: ${(props) => (props.line === 0 ? 'none' : '1px solid #d9d9d6')};
  border-bottom: none;
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  display: inline-block;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  min-height: 50px;
  width: 100%;
  padding: ${(props) => props.padding || 24}px;
`;
const CardTable = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  width: 100%;
  display: inline-block;
  border-radius: 6px 6px 0px 0px !important;
`;
const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  padding: 16px 24px !important;
  width: 100%;
  background-color: ${colors.primary};
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const CardTab = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  display: inline-block;
  padding: 0px !important;
  background-color: ${colors.backgroundPrimary};
  display: flex;
  width: 100%;
  flex-direction: column;
`;

const TableScroll = styled.div`
  height: 275px;
  overflow-x: hidden;
  overflow-y: auto;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const headers = [
  { key: 'menu', render: 'ชื่อเมนู', width: 25 },
  { key: 'View', render: 'View', width: 75 },
];

const headersList = [
  { key: 'no', render: 'No.', width: 5 },
  { key: 'name', render: 'ชื่อ - สกุล', width: 75 },
  { key: 'date', render: 'วันที่', width: 15 },
];

const index = (props) => {
  const roleModel = useLocalStore(() => new RoleModel());
  const authContext = initAuthStore();
  const dataContext = initDataContext();
  const [searchStaff, setSearchStaff] = useState();
  const [active, setActive] = useState(false);
  const [authorityIdList, setAuthorityIdList] = useState([]);

  const handleCheckboxAuthority = (id) => {
    const isDuplicated = authorityIdList.find(
      (authorityId) => authorityId === id,
    );

    if (!isDuplicated) {
      setAuthorityIdList((preveState) => [...preveState, id]);
      roleModel.authoritiesId.push(id);
    } else {
      const filterDuplicatedId = authorityIdList.filter(
        (authorityId) => authorityId !== id,
      );
      setAuthorityIdList(filterDuplicatedId);
      roleModel.authoritiesId = filterDuplicatedId;
    }
  };

  const handleBack = async () => {
    if (!dataContext.name && dataContext.roleId.length === 0) {
      Router.back();
    } else {
      setActive(true);
    }
  };

  useEffect(() => {
    roleModel.getAuthoritiesList(authContext.accessToken);
    roleModel.getRoleSpacialList(authContext.accessToken);
    roleModel.getUserRoleList(authContext.accessToken);
    roleModel.getRoleType('CREATE');
    roleModel.getPositionList(authContext.accessToken);
    roleModel.getRoleSystemList(authContext.accessToken);
  }, []);

  const renderCheckboxList = (authorities) => (
    <React.Fragment>
      {authorities.map((authorityItem) => (
        <Cell width={75} center key={authorityItem.id}>
          <CheckBoxAll
            defaultChecked={authorityItem.isSelect}
            onChange={() => handleCheckboxAuthority(authorityItem.id)}
          />
        </Cell>
      ))}
    </React.Fragment>
  );

  const renderMenuList = () => (
    <React.Fragment>
      {roleModel.authoritiesList &&
        roleModel.authoritiesList.map((item, index) => (
          <TableBody span={12} key={index} line={index}>
            <Cell width={25}>
              <Text
                fontWeight="bold"
                fontSize={sizes.s}
                color={colors.primaryBlack}
              >
                {item.type}
              </Text>
            </Cell>
            {renderCheckboxList(item.authorities)}
          </TableBody>
        ))}
    </React.Fragment>
  );

  return (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div className="click" style={{ width: '10%' }}>
        <Header>
          <a href="/roleManagement/RoleList">
            <Icon name="angle left" />
            <Header.Content>
              <Text color={colors.primaryBlack} fontSize={sizes.xxl}>
                Back
              </Text>
            </Header.Content>
          </a>
        </Header>
      </Div>

      <Grid style={{ margin: '18px 0' }}>
        <Grid.Row>
          <Card>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <div style={{ display: 'flex' }}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 6 }}
                    >
                      Role Title
                    </Text>
                    <Text color={colors.red}> * </Text>
                  </div>
                  <InputAll
                    handleOnChange={(e) => {
                      roleModel.setField('roleName', e.target.value);
                    }}
                    maxLength={50}
                    value={roleModel.roleName}
                    placeholder="Key Role Name"
                  />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column>
                  <div style={{ display: 'flex' }}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 6 }}
                    >
                      Role Type
                    </Text>
                  </div>
                  <DropdownAll
                    handleOnChange={(e) => {
                      roleModel.setField('roleType', e);
                      roleModel.setField('roleTypeValue', e.value);
                    }}
                    options={roleModel.roleTypeList}
                    value={roleModel.roleType}
                    placeholder="Select Role Type"
                  />
                </Grid.Column>
              </Grid.Row>

              {roleModel.roleType && roleModel.roleType.value === 'SPECIAL' && (
                <>
                  <Grid.Row>
                    <Grid.Column floated="left" width={8}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Role
                      </Text>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    {roleModel.roleSpacial.map((item) => (
                      <Grid.Column computer={3}>
                        <RadioBox
                          label={item.label}
                          checked={item.status}
                          onChange={() => {
                            roleModel.setRole(item.value);
                          }}
                        />
                      </Grid.Column>
                    ))}
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column floated="left">
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        User
                      </Text>
                      <DropdownAll
                        handleOnChange={(process) => {
                          roleModel.setField('roleUser', process);
                          roleModel.setFieldId(process);
                        }}
                        options={roleModel.roleUserList}
                        value={roleModel.roleUser}
                        defaultValue="Select Users "
                        placeholder="Select Users "
                        isMulti
                      />
                    </Grid.Column>
                  </Grid.Row>
                </>
              )}

              {roleModel.roleType.value === 'GENERAL' && (
                <Grid.Row>
                  <Grid.Column computer={8}>
                    <div style={{ display: 'flex' }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 6 }}
                      >
                        Position Level
                      </Text>
                    </div>
                    <DropdownAll
                      handleOnChange={(e) => {
                        roleModel.setField('selectPosition', e);
                      }}
                      options={roleModel.positionList}
                      value={roleModel.selectPosition}
                      placeholder="Select Position Level"
                    />
                  </Grid.Column>
                  <Grid.Column computer={8}>
                    <div style={{ display: 'flex' }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 6 }}
                      >
                        Role Systems
                      </Text>
                    </div>
                    <DropdownAll
                      handleOnChange={(e) => {
                        roleModel.setField('selectRoleSystem', e);
                      }}
                      options={roleModel.roleSystemList}
                      value={roleModel.selectRoleSystem}
                      placeholder="Select Role Systems"
                    />
                  </Grid.Column>
                </Grid.Row>
              )}
            </Grid>
          </Card>
        </Grid.Row>
      </Grid>

      <Modal
        // open={active}
        title="STAFF LIST"
        status={{ page: 'isSubmit', text: 'Create Now' }}
        ContentComponent={() => (
          <Grid style={{ margin: 0 }}>
            <Grid.Row>
              <Grid.Column width={8} style={{ padding: 0 }}>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 6 }}
                >
                  ค้นหา STAFF
                </Text>

                <DropdownAll
                  options={sortTypes}
                  value={searchStaff}
                  placeholder="เลือก Vendor Type"
                  handleOnChange={(selected) => setSearchStaff(selected)}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <CardTable>
                <TableHeader span={12}>
                  {headersList.map(({ render, key, width }, index) => (
                    <Cell key={key} width={width}>
                      <Div>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="med"
                          color={colors.textGray}
                        >
                          {render}
                        </Text>
                        {index === 1 && (
                          <Image
                            src="../../static/images/iconDown-gray.png"
                            style={{
                              marginLeft: 8,
                              width: 18,
                              height: 18,
                            }}
                          />
                        )}
                      </Div>
                    </Cell>
                  ))}
                </TableHeader>

                <CardTab>
                  <TableScroll>
                    <div>
                      {ListStaff &&
                        ListStaff.map((item, index) => (
                          <TableBody span={12} key={index} line={index}>
                            <Cell width={5}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                              >
                                {item.id}
                              </Text>
                            </Cell>
                            <Cell width={75}>
                              <Text
                                fontSize={sizes.xxs}
                                fontWeight="bold"
                                color={colors.primaryBlack}
                              >
                                {item.firstName} {item.lastName}
                              </Text>
                            </Cell>
                            <Cell width={15}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                                style={{ paddingLeft: 8 }}
                              >
                                {item.updateDate}
                              </Text>
                            </Cell>
                          </TableBody>
                        ))}
                    </div>
                  </TableScroll>
                </CardTab>
              </CardTable>
            </Grid.Row>
          </Grid>
        )}
        onClose={() => setActive(false)}
      />

      <Modal
        open={active}
        title="ต้องการย้อนกลับ"
        status={{ page: 'isSubmit', text: 'Yes' }}
        ContentComponent={() => (
          <Div center>
            <Text
              fontSize={sizes.s}
              color={colors.textDarkBlack}
              style={{ lineHeight: 'normal' }}
            >
              ผู้ใช้งานต้องการจะย้อนกลับไปใช่หรือไม่
            </Text>
          </Div>
        )}
        onClose={() => setActive(false)}
        onSubmit={() => Router.back()}
      />
      <BottomBarRole page="createRole" roleModel={roleModel} />
    </div>
  );
};

export default withLayout(observer(index));
