/* eslint-disable react/jsx-fragments */
import React, { useState, useEffect } from 'react';
import { Header, Grid, Loader, Dimmer, Icon } from 'semantic-ui-react';
import Link from 'next/link';
import { toJS } from 'mobx';
import Router, { useRouter } from 'next/router';
import { observer, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import loadingStore from '../../contexts/LoadingStore';
import {
  Text,
  CheckBoxAll,
  InputAll,
  DropdownAll,
  BottomBarRole,
  Modal,
  RadioBox,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import { withLayout } from '../../hoc';
import request from '../../services';
import { RoleModel } from '../../model/Role';
import {
  userRoleListStore,
  userTypeListStore,
  roleSpacialStore,
} from '../../model';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50px;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  padding: 10px 24px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #d9d9d6;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 1px 0px;
  height: 10%;
  border-top: ${(props) => (props.line === 0 ? 'none' : '1px solid #d9d9d6')};
  border-bottom: none;
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.center ? 'center' : 'start')};
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  display: inline-block;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  min-height: 50px;
  width: 100%;
  padding: ${(props) => props.padding || 24}px;
`;
const CardTable = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  width: 100%;
  display: inline-block;
`;
const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  padding: 16px 24px !important;
  width: 100%;
  background-color: ${colors.primary};
  display: flex;
`;
const CardTab = CardTabHeader.extend`
  border-radius: 0px 0px 0px 6px !important;
  display: inline-block;
  padding: 0px !important;
  background-color: ${colors.backgroundPrimary};
  display: flex;
  flex-direction: column;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const headers = [
  { key: 'menu', render: 'ชื่อเมนู', width: 25 },
  { key: 'View', render: 'View', width: 75 },
];

const index = (props) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();
  const roleModel = useLocalStore(() => new RoleModel());
  const [active, setActive] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleCheckboxAuthority = (id) => {
    const indexArr = roleModel.authoritiesId.indexOf(id);
    if (indexArr > -1) {
      roleModel.authoritiesId.splice(indexArr, 1);
    } else {
      roleModel.authoritiesId.push(id);
    }
    roleModel.handleCheckboxAuthorityId();
  };

  const handleRoleType = async (token) => {
    const optionsResult = await userTypeListStore.getRoleTypeList(token);
    optionsResult.find((list) => {
      if (list.value === roleModel.roleType) {
        roleModel.setField('roleType', list);
      }
    });
  };

  const handlePositionLevel = async (token) => {
    const optionsResult = await userTypeListStore.getPositionList(token);
    const result = await optionsResult.find(
      (list) => list.value === roleModel.selectPosition,
    );
    roleModel.setField('selectPosition', result);
  };

  const handleRoleSystem = async (token) => {
    const optionsResult = await userTypeListStore.getRoleSystemList(token);
    const result = await optionsResult.find(
      (list) => list.value === roleModel.selectRoleSystem,
    );
    roleModel.setField('selectRoleSystem', result);
  };

  const handleRoleSpacialList = async (token) => {
    const optionsResult = await roleSpacialStore.getRoleSpacialList(token);
    optionsResult.find((list) => {
      if (list.value === 'isAdmin' && roleModel.isAdmin === true) {
        roleModel.setRole(list.value);
      } else if (
        list.value === 'isCompliance' &&
        roleModel.isCompliance === true
      ) {
        roleModel.setRole(list.value);
      } else if (list.value === 'isIcAgent' && roleModel.isIcAgent === true) {
        roleModel.setRole(list.value);
      } else if (
        list.value === 'isIaFinding' &&
        roleModel.isIaFinding === true
      ) {
        roleModel.setRole(list.value);
      }
    });
  };

  useEffect(() => {
    (async () => {
      try {
        dataContext.page = 'roleEdit';
        await setIsLoading(true);
        const getId = window.location.search;
        const id = new URLSearchParams(getId);
        await roleModel.getAuthoritiesList(authContext.accessToken);
        await roleModel.getRoleType(authContext.accessToken);
        await roleModel.getRoleSpacialList(authContext.accessToken);
        await roleModel.getRoleListById(id.get('roleType'), id.get('id'));
        await handleRoleSpacialList(authContext.accessToken);

        if (id.get('roleType') === 'GENERAL') {
          await roleModel.getPositionList(authContext.accessToken);
          await roleModel.getRoleSystemList(authContext.accessToken);
        } else {
          await roleModel.getUserRoleList(
            authContext.accessToken,
            id.get('id'),
          );
        }

        await handleRoleType(authContext.accessToken);
        await handlePositionLevel(authContext.accessToken);
        await handleRoleSystem(authContext.accessToken);
        await setIsLoading(false);
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  // ใช้ตอน เลือก positionLevel และดึงค่าใหม่ทั้งหน้า
  const refreshData = async (positionLevel) => {
    await setIsLoading(true);
    const getId = window.location.search;
    const id = new URLSearchParams(getId);
    await roleModel.getAuthoritiesList(authContext.accessToken);
    await roleModel.getRoleType(authContext.accessToken);
    await roleModel.getRoleSpacialList(authContext.accessToken);
    await roleModel.getRoleListById(
      id.get('roleType'),
      id.get('id'),
      positionLevel,
    );
    await handleRoleSpacialList(authContext.accessToken);

    if (id.get('roleType') === 'GENERAL') {
      await roleModel.getPositionList(authContext.accessToken);
      await roleModel.getRoleSystemList(authContext.accessToken);
    } else {
      await roleModel.getUserRoleList(authContext.accessToken, id.get('id'));
    }

    await handleRoleType('UPDATE');
    await handlePositionLevel(authContext.accessToken);
    await handleRoleSystem(authContext.accessToken);
    await setIsLoading(false);
  };

  const renderCheckboxList = (authorities) => (
    <React.Fragment>
      {authorities.map((authorityItem) => (
        <Cell width={75} center key={authorityItem.id}>
          <CheckBoxAll
            checked={authorityItem.isSelect}
            onChange={() => handleCheckboxAuthority(authorityItem.id)}
          />
        </Cell>
      ))}
    </React.Fragment>
  );

  const renderMenuList = () => (
    <React.Fragment>
      {roleModel.authoritiesList &&
        roleModel.authoritiesList.map((item, index) => (
          <TableBody span={12} key={index} line={index}>
            <Cell width={25}>
              <Text
                fontWeight="bold"
                fontSize={sizes.s}
                color={colors.primaryBlack}
              >
                {item.type}
              </Text>
            </Cell>
            {renderCheckboxList(item.authorities)}
          </TableBody>
        ))}
    </React.Fragment>
  );
  return (
    <div>
      <Dimmer active={isLoading}>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <a href="/roleManagement/RoleList" className="click">
        <Header>
          <Icon name="angle left" />
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              Back
            </Text>
          </Header.Content>
        </Header>
      </a>

      <Grid style={{ margin: '18px 0' }}>
        <Grid.Row>
          <Card>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column style={{ paddingLeft: 14 }}>
                  <div style={{ display: 'flex' }}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 6 }}
                    >
                      Role Title
                    </Text>
                    <Text color={colors.red}> * </Text>
                  </div>
                  <InputAll
                    handleOnChange={(e) => {
                      roleModel.setField('roleName', e.target.value);
                    }}
                    maxLength={30}
                    value={roleModel.roleName}
                  />
                  <Text color={colors.red}>{dataContext.status}</Text>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <div style={{ display: 'flex' }}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 6 }}
                    >
                      Role Type
                    </Text>
                  </div>
                  <DropdownAll
                    handleOnChange={(e) => {
                      roleModel.setField('roleType', e);
                    }}
                    options={roleModel.roleTypeList}
                    value={roleModel.roleType}
                    placeholder="Select Role Type"
                    isDisabled
                  />
                </Grid.Column>
              </Grid.Row>
              {roleModel.roleType && roleModel.roleType.value === 'SPECIAL' && (
                <>
                  <Grid.Row>
                    <Grid.Column floated="left" width={8}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Role
                      </Text>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    {roleModel.roleSpacial.map((item) => (
                      <Grid.Column computer={3}>
                        <RadioBox
                          label={item.label}
                          checked={item.status}
                          onChange={() => {
                            roleModel.setRole(item.value);
                          }}
                        />
                      </Grid.Column>
                    ))}
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column floated="left">
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        User
                      </Text>

                      <DropdownAll
                        handleOnChange={(process) => {
                          console.log('process', process);
                          roleModel.setField('roleUser', process);
                          roleModel.setFieldId(process);
                        }}
                        options={roleModel.roleUserList}
                        value={roleModel.roleUser}
                        defaultValue="Select Users "
                        placeholder="Select Users "
                        isMulti
                      />
                    </Grid.Column>
                  </Grid.Row>
                </>
              )}
              {roleModel.roleType && roleModel.roleType.value === 'GENERAL' && (
                <Grid.Row>
                  <Grid.Column computer={8}>
                    <div style={{ display: 'flex' }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 6 }}
                      >
                        Position Level
                      </Text>
                    </div>
                    <DropdownAll
                      handleOnChange={(e) => {
                        roleModel.setField('selectPosition', e);
                        refreshData(e.value);
                      }}
                      options={roleModel.positionList}
                      value={roleModel.selectPosition}
                      placeholder="Select Position Level"
                    />
                  </Grid.Column>
                  <Grid.Column computer={8}>
                    <div style={{ display: 'flex' }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 6 }}
                      >
                        Role Systems
                      </Text>
                    </div>
                    <DropdownAll
                      handleOnChange={(e) => {
                        roleModel.setField('selectRoleSystem', e);
                      }}
                      options={roleModel.roleSystemList}
                      value={roleModel.selectRoleSystem}
                      placeholder="Select Role Systems"
                    />
                  </Grid.Column>
                </Grid.Row>
              )}
            </Grid>
          </Card>
        </Grid.Row>
      </Grid>

      <Modal
        open={active}
        title="ต้องการย้อนกลับ"
        status={{ page: 'isSubmit', text: 'Yes' }}
        ContentComponent={() => (
          <Div center>
            <Text
              fontSize={sizes.s}
              color={colors.textDarkBlack}
              style={{ lineHeight: 'normal' }}
            >
              ผู้ใช้งานต้องการจะย้อนกลับไปใช่หรือไม่
            </Text>
          </Div>
        )}
        onClose={() => setActive(false)}
        onSubmit={() => Router.back()}
      />
      <BottomBarRole page="editRole" roleModel={roleModel} />
    </div>
  );
};

export default withLayout(observer(index));
