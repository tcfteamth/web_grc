import React, { useState, useEffect } from 'react';
import {
  Header,
  Image,
  Grid,
  GridRow,
  Loader,
  Dimmer,
  Icon,
  Pagination,
  Popup,
} from 'semantic-ui-react';
import Link from 'next/link';
import { observer, useLocalStore } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import moment from 'moment';
import { withLayout } from '../../hoc';
import { Text, CardEmpty, BottomBarRole, Cell } from '../../components/element';
import { colors, sizes } from '../../utils';
import { handleRoleAccessMenu } from '../../utils/roleAccessment';
import request from '../../services';
import { RoleModel } from '../../model/Role';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50px;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  padding: 10px 24px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #d9d9d6;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 1px 0px;
  border-top: ${(props) => (props.line === 0 ? 'none' : '1px solid #d9d9d6')};
  border-bottom: none;
`;

const CardTab = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: 0px !important;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: column;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const headers = [
  { key: 'No.', render: 'No.', width: 5 },
  { key: 'Role Title', render: 'Role Title', width: 20 },
  { key: 'CountMember', render: 'จำนวนสมาชิก', width: 35 },
  { key: 'UpdateDate', render: 'Update Date', width: 30 },
  { key: 'Edit', render: 'Edit', width: 10 },
];

const index = (props) => {
  const roleModel = useLocalStore(() => new RoleModel());

  const handleOnSorting = async (role) => {
    // if (role === 'Role Title') {
    //   const sorting = roleListGroup.sort((a, b) =>
    //     a.roleName.localeCompare(b.roleName),
    //   );
    //   setRoleListGroup(sorting);
    // } else if (role === 'CountMember') {
    //   const sorting = roleListGroup.sort();
    //   setRoleListGroup(sorting);
    // } else if (role === 'UpdateDate') {
    //   const sorting = roleListGroup.sort();
    //   setRoleListGroup(sorting);
    // }
  };

  useEffect(() => {
    roleModel.getRoleList();
  }, []);

  return (
    <div>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>

      <div>
        <Header>
          <Text
            color={colors.primaryBlack}
            fontWeight="bold"
            fontSize={sizes.xxl}
            styled={{ lineHeight: 'normal' }}
          >
            Role Management
          </Text>
        </Header>
        {roleModel.roleList && roleModel.roleList.length > 0 ? (
          <Grid style={{ marginTop: 32 }}>
            <CardTab>
              <TableHeader span={12}>
                {headers.map(({ render, key, width }, index) => (
                  <Cell key={key} width={width} center={index === 2}>
                    <Div center>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                      >
                        {render}
                      </Text>
                      <Popup
                        trigger={
                          (key === 'Role Title' ||
                            key === 'CountMember' ||
                            key === 'UpdateDate') && (
                            <div onClick={() => handleOnSorting(key)}>
                              <Image
                                className="click"
                                src="../../static/images/iconDown-gray@3x.png"
                                style={{ marginLeft: 8, width: 18, height: 18 }}
                              />
                            </div>
                          )
                        }
                        content={
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.textlightGray}
                          >
                            {`Sorting ${render}`}
                          </Text>
                        }
                        size="mini"
                      />
                    </Div>
                  </Cell>
                ))}
                {handleRoleAccessMenu('ROLE_MANAGEMENT_EDIT') && (
                  <Cell width={10}>
                    <Div center>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                      >
                        Edit
                      </Text>
                    </Div>
                  </Cell>
                )}
              </TableHeader>
              <div>
                {roleModel.roleList &&
                  roleModel.roleList.map((item, index) => (
                    <TableBody span={12} key={item.roleId} line={index}>
                      <Cell width={5}>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {index + 1}
                        </Text>
                      </Cell>
                      <Cell width={20}>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {item.roleName}
                        </Text>
                      </Cell>
                      <Cell width={35} center>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {item.amount}
                        </Text>
                      </Cell>
                      <Cell width={30}>
                        <Text
                          fontSize={sizes.xs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          {moment(item.updateDate).locale('th').format('L')}
                        </Text>
                      </Cell>

                      <Cell width={10}>
                        {/* <Link
                          href={`/roleManagement/RoleEdit?id=${item.roleId}`}
                        > */}

                        <Link
                          href={{
                            pathname: '/roleManagement/RoleEdit',
                            query: {
                              id: item.roleId,
                              roleType: item.roleType,
                            },
                          }}
                        >
                          <Image
                            className="click"
                            src="../../static/images/edit@3x.png"
                            style={{ width: 18, height: 18 }}
                          />
                        </Link>
                      </Cell>
                    </TableBody>
                  ))}
              </div>
            </CardTab>
          </Grid>
        ) : (
          <div style={{ marginTop: 32 }}>
            <CardEmpty
              style={{ marginTop: 32 }}
              Icon="../../static/images/roleInactive@3x.png"
              textTitle="No Role"
            />
          </div>
        )}
      </div>

      <Grid>
        <Grid.Row
          style={{
            justifyContent: 'flex-end',
            marginRight: 13,
            marginTop: 16,
          }}
        >
          <Pagination
            style={{
              fontSize: sizes.xs,
              fontWeight: 'bold',
              borderColor: colors.backgroundSecondary,
            }}
            defaultActivePage={roleModel.page}
            firstItem={null}
            lastItem={null}
            onPageChange={(e, { activePage }) => {
              roleModel.getRoleList(activePage);
            }}
            nextItem={{
              content: <Icon name="angle right" />,
              icon: true,
            }}
            prevItem={{
              content: <Icon name="angle left" />,
              icon: true,
            }}
            totalPages={roleModel.totalPage}
          />
        </Grid.Row>
      </Grid>

      <BottomBarRole page="createList" roleModel={roleModel} />
    </div>
  );
};

export default withLayout(observer(index));
