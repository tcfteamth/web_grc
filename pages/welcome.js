import React, { useEffect, useState } from 'react';
import Router, { useRouter } from 'next/router';
// import { Modal, Header } from 'semantic-ui-react';
import { Grid, Table, Radio, Image } from 'semantic-ui-react';
import { observer, useObserver, useLocalStore } from 'mobx-react-lite';
import { Modal, CheckBoxAll, Text } from '../components/element';
import { withLayout, withApp } from '../hoc';
import { colors, sizes } from '../utils';
import { initAuthStore } from '../contexts';
import { SelectRolesModel } from '../model/Login';
import { SelectRolesModal } from '../components/Login';
import styled from 'styled-components';

const Container = styled.div`
  background: url('../static/images/login-bg-2-2@3x.png') no-repeat center
    center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  background-color: ${colors.primaryBackground} !important;
  width: 100% !important;
  height: 100% !important;
  position: absolute !important;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ImageCard = styled.div`
  position: relative !important;
  width: 692px;
  z-index: 2;
  @media only screen and (min-width: 768px) {
    width: 480px;
  }
`;
const ShadownCard = styled.div`
  width: 712px;
  min-height: 480px;
  border-radius: 6px 6px 0px 6px !important;
  padding: 52px 122px 38px 122px;
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fefeff;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  z-index: 999;
  position: relative;
`;

const index = () => {
  const authContext = initAuthStore();
  const selectRolesModel = useLocalStore(() => new SelectRolesModel());
  const router = useRouter();
  const { accessToken } = router.query;

  const handleSubmitRole = async () => {
    try {
      const response = await authContext.loginAD(
        accessToken,
        selectRolesModel.submitRole.roleId,
        selectRolesModel.submitRole.roleSys,
      );
      return response;
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    selectRolesModel.getSelectRoles(accessToken);
  }, [accessToken]);

  return useObserver(() => (
    <Container>
      <Grid container>
        <Grid.Row style={{ justifyContent: 'center' }}>
          <Text fontSize={sizes.xxxxl} fontWeight="bold" color={colors.primary}>
            Welcome
          </Text>
          <SelectRolesModal
            active={selectRolesModel.activeModal}
            selectRolesModel={selectRolesModel}
            roles={selectRolesModel.roles}
            onSubmit={handleSubmitRole}
          />
        </Grid.Row>
      </Grid>
      <Image
        src="../static/images/login-bg-1-2@3x.png"
        style={{
          width: 'auto',
          right: 200,
          height: 584,
          position: 'absolute',
        }}
      />
    </Container>
  ));
};

export default withApp(index);
