import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';
import htmlescape from 'htmlescape';

const { HOST_AUTHSERVICE, HOST_APIGATEWAY, FROALA_KEY } = process.env;
const env = { HOST_AUTHSERVICE, HOST_APIGATEWAY, FROALA_KEY };

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const props = await Document.getInitialProps(ctx);
    return props;
  }

  render() {
    const sheet = new ServerStyleSheet();
    const main = sheet.collectStyles(<Main />);
    const styleTags = sheet.getStyleElement();
    return (
      <html>
        <Head> {styleTags}</Head>
        <body>
          <div className="root">{main}</div>
          <script
            dangerouslySetInnerHTML={{ __html: '__ENV__ = ' + htmlescape(env) }}
          />
          <NextScript />
        </body>
      </html>
    );
  }
}
