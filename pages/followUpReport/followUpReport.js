import React, { useState, useEffect } from 'react';
import { withLayout } from '../../hoc';
import { Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import {
  ChartPie,
  Text,
  BottomBarReport,
  AcknowledgeModal,
  CardEmpty,
  PublishedModal,
  FollowUpSummaryReport,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import {
  DetailReportModel,
  ManageRoleModel,
  PublishedModel,
  SummaryDepartmentModel,
} from '../../model/Report';
import { ManageFilterListModel } from '../../model/DashboardModel';
import FilterModel from '../../model/FilterModel';
import { initAuthStore } from '../../contexts';
import {
  FollowUpReportModel,
  SummaryReportListModel,
} from '../../model/FollowUpReportModel';
import { FollowUpFilterListModel } from '../../model/FollowUpModel';
import { FollowUpReportContainerFilter } from '../../components/FollowUpPage';

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  border-bottom: 1px solid #eaeaea;
  display: inline-block;
  padding: ${(props) => (props.border ? `0px 24px` : '24px')};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
  margin-right: 16px;
`;

const Oval = styled.div`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  margin-right: 8px;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
`;

const headers = [
  {
    key: 'Department',
    render: 'Department',
    color: colors.grayLight,
    width: 20,
  },
  { key: 'Good', render: 'Good', color: '#00b140', width: 15 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 15 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 15 },
  {
    key: 'Total Sub-process',
    render: 'Total Sub-process',
    color: colors.primaryBlack,
    width: 15,
  },
  // { key: 'Graph', render: 'Graph', color: colors.grayLight, width: 35 },
  {
    key: 'No. of Enhancement by Control',
    render: 'No. of Enhancement by Control',
    color: colors.textPurple,
    width: 20,
  },
];

const headersDivsion = [
  {
    key: 'Division',
    render: 'Division',
    color: colors.grayLight,
    width: 20,
  },
  { key: 'Good', render: 'Good', color: '#00b140', width: 15 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 15 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 15 },
  {
    key: 'Total Sub-process',
    render: 'Total Sub-process',
    color: colors.primaryBlack,
    width: 15,
  },
  {
    key: 'No. of Enhancement by Control',
    render: 'No. of Enhancement by Control',
    color: colors.textPurple,
    width: 20,
  },
];

const dataPieList = {
  dataPie: [30, 30, 40],
  color: [colors.red, colors.pink, colors.orange, colors.greenMint],
};

const index = (props) => {
  const manageFilterListModel = useLocalStore(
    () => new ManageFilterListModel(),
  );
  const filterModel = useLocalStore(() => new FilterModel());
  const manageRoleModel = useLocalStore(() => new ManageRoleModel());
  const detailReportModel = useLocalStore(() => new DetailReportModel());
  const publishedModel = useLocalStore(() => new PublishedModel());
  const followUpReportModel = useLocalStore(() => new FollowUpReportModel());
  const summaryReportListModel = useLocalStore(
    () => new SummaryReportListModel(),
  );
  const followUpFilterListModel = useLocalStore(
    () => new FollowUpFilterListModel(),
  );
  const authContext = initAuthStore();
  const [isGetReport, setIsGetReport] = useState(false);
  const [isFilterReport, setIsFilterReport] = useState(false);

  const getReportCsaData = async (
    year,
    bu,
    departmentNo,
    divisionNo,
    startDueDate,
    endDueDate,
    startCloseDate,
    endCloseDate,
    status,
    processAnd = [],
    processOr = [],
    objectives = []
  ) => {
    try {
      await new Promise([
        followUpReportModel.getReportGraphs(
          authContext.accessToken,
          year,
          bu,
          departmentNo,
          divisionNo,
          startDueDate,
          endDueDate,
          startCloseDate,
          endCloseDate,
          status,
          processAnd,
          processOr,
          objectives
        ),
        summaryReportListModel.getSummaryReportList(
          authContext.accessToken,
          year,
          bu,
          departmentNo,
          divisionNo,
          startDueDate,
          endDueDate,
          startCloseDate,
          endCloseDate,
          status,
          processAnd,
          processOr,
          objectives
        ),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const getManageServices = async () => {
    try {
      await new Promise([
        followUpFilterListModel.getReportFollowUpFilterList(
          authContext.accessToken,
        ),
        manageFilterListModel.getManageFilterList(authContext.accessToken),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const filterReport = async () => {
    await manageRoleModel.getManageRole(
      filterModel.selectedYear,
      filterModel.selectedBu,
    );

    await getReportCsaData(
      filterModel.selectedYear,
      filterModel.selectedBu,
      filterModel.selectedDepartment.no,
      filterModel.selectedDivision,
      filterModel.convertStartDueDate,
      filterModel.convertEndDueDate,
      filterModel.convertStartCloseDate,
      filterModel.convertEndCloseDate,
      filterModel.selectedStatus,
      filterModel.selectedProcessAnd,
      filterModel.selectedProcessOr,
      filterModel.selectedObjectives
    );

    setIsFilterReport(true);
    setIsGetReport(false);
  };

  const clearFilterReport = () => {
    if (authContext.roles.isAdmin || authContext.roles.isSEVP) {
      filterModel.resetFilterFollowUpReportAdmin();
    } else if (authContext.roles.isVP || authContext.roles.isIcAgent) {
      filterModel.resetFilterFollowUpReportVP();
    } else {
      filterModel.resetFilterFollowUpReportDMAndStaff();
    }

    setIsFilterReport(false);
    setIsGetReport(false);
  };

  const isShowPublic = () => {
    if (authContext.roles.isAdmin) {
      return true;
    } else if (manageRoleModel.showPublic) {
      return true;
    } else {
      return false;
    }
  };

  const handelExport = async () => {
    try {
      const result = await followUpReportModel.exportFollowUpReport(
        authContext.accessToken,
        filterModel.selectedYear,
        filterModel.selectedBu,
        filterModel.selectedDepartment.no,
        filterModel.selectedDivision,
        filterModel.selectedStatus,
      );

      window.open(result.config.url, '_blank');
    } catch (e) {
      console.log(e);
    }
  };

  // get manage filter
  useEffect(() => {
    getManageServices();
  }, []);

  useEffect(() => {
    const currentYear = new Date().getFullYear();
    filterModel.setField('selectedYear', currentYear);
  }, []);

  return useObserver(() => (
    <div>
      <div>
        <FollowUpReportContainerFilter
          filterModel={filterModel}
          followUpFilterListModel={followUpFilterListModel}
          onSubmit={filterReport}
          onClear={clearFilterReport}
          name="Follow Up Report"
        />

        {isFilterReport && (
          <>
            {isShowPublic() ? (
              <>
                <Grid
                  style={{ margin: '16px 0px' }}
                  doubling
                  columns={2}
                  centered
                  className="print-pagebreak pt"
                >
                  <Grid.Row style={{ padding: 0 }}>
                    <CardTabHeader border>
                      <Div center padding={24}>
                        <Text
                          fontSize={sizes.l}
                          fontWeight="bold"
                          color={colors.textPurple}
                        >
                          Follow Up Result
                        </Text>
                      </Div>
                    </CardTabHeader>
                  </Grid.Row>
                  <Grid.Row style={{ padding: 0, backgroundColor: 'white' }}>
                    {followUpReportModel.reportGraphs?.map((graph) => (
                      <Grid.Column>
                        <Text
                          style={{
                            marginBottom: 16,
                            marginTop: 16,
                            textAlign: 'center',
                            textTransform: 'capitalize',
                          }}
                          fontSize={sizes.l}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          {graph.fullname} - Total {graph.sumCountGraphDTO}
                        </Text>
                        <Div center col>
                          <Div>
                            <ChartPie
                              data={graph.percentGraphDTO}
                              backgroundColor={dataPieList.color}
                              labels={['Overdue', 'Open', 'Postpone', 'Closed']}
                              dataCount={graph.countGraphDTO}
                            />
                          </Div>
                          <Div mid>
                            {graph.graphDTO.map((gDTO, gDTOIndex) => (
                              <SectionLine>
                                <Oval bgcolor={dataPieList.color[gDTOIndex]} />
                                <Text
                                  style={{ paddingLeft: 8 }}
                                  color={colors.primaryBlack}
                                  className="upper"
                                  fontWeight="bold"
                                  fontSize={sizes.xxs}
                                >
                                  {gDTO.status}
                                </Text>
                              </SectionLine>
                            ))}
                          </Div>
                        </Div>
                      </Grid.Column>
                    ))}
                  </Grid.Row>
                </Grid>

                {summaryReportListModel.summaryReportList?.map((summary) => (
                  <>
                    <FollowUpSummaryReport
                      type="corrective"
                      name={summary.name}
                      reportData={summary.correctiveDTOS}
                    />

                    <FollowUpSummaryReport
                      type="enhance"
                      name={summary.name}
                      reportData={summary.enhancementDTOS}
                    />
                  </>
                ))}
              </>
            ) : (
              <Grid.Row style={{ marginTop: '24px' }} className="no-print">
                <CardEmpty textTitle="No Report Data" />
              </Grid.Row>
            )}
          </>
        )}

        {/* <div style={{ marginTop: '32px'}}> */}
        <BottomBarReport
          detailReportModel={detailReportModel}
          manageRoleModel={manageRoleModel}
          filterModel={filterModel}
          isFilterReport={isFilterReport}
          reportEx={handelExport}
        />
        {/* </div> */}
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
