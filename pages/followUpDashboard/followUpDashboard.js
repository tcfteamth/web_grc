import React, { useState, useEffect } from 'react';
import { Grid, Image, Popup } from 'semantic-ui-react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { ChartPie, Text } from '../../components/element';
import { colors, sizes } from '../../utils';
import { ManageFilterListModel } from '../../model/DashboardModel';
import { initAuthStore } from '../../contexts';
import FilterModel from '../../model/FilterModel';
import { withLayout } from '../../hoc';
import { FollowUpDashboardModel } from '../../model/DashboardFollowUpModel';
import { FollowUpDashboardContainerFilter } from '../../components/FollowUpPage';
import { FollowUpFilterListModel } from '../../model/FollowUpModel';
import { followUpYearListStore, manageBUDropdownStore } from '../../model';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  align-items: center;
  justify-content: center;
`;
const TableBody = TableHeader.extend`
  padding: 0px !important;
  margin: 0px 40px;
  height: 50px !important;
  border-top: ${(props) => props.borderTop && `1px solid ${colors.grayLight}`};
  justify-content: ${(props) => (props.center ? `center` : 'space-between')};
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  background-color: ${(props) => props.color || colors.backgroundPrimary};
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.padding || 0}px;
  border-right: ${(props) => props.border && `1px solid #FFF`};
`;

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: ${(props) => (props.border ? `0px 24px` : ' 24px')};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: row;
`;
const CardTabBody = CardTabHeader.extend`
  border-top: 1px solid #d9d9d6;
  border-radius: 0px 0px 0px 6px !important;
  min-height: 350px;
  padding: 0px 24px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
`;

const Oval = styled.div`
  width: ${(props) => props.width || 12}px;
  height: ${(props) => props.height || 12}px;
  border-radius: 50%;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  color: ${(props) => props.color || colors.primaryBlack};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${sizes.xxs}px;
`;

const headersDonut2 = [
  { key: 'Division', render: 'Division', color: colors.grayLight, width: 20 },
  { key: 'Over Due', render: 'Over Due', color: colors.red, width: 20 },
  { key: 'Open', render: 'Open', color: colors.pink, width: 20 },
  { key: 'Postpone', render: 'Postpone', color: colors.orange, width: 20 },
  { key: 'Closed', render: 'Closed', color: colors.greenMint, width: 20 },
];

const dataPieList = {
  color: [colors.red, colors.pink, colors.orange, colors.greenMint],
};

const index = (props) => {
  const authContext = initAuthStore();

  const manageFilterListModel = useLocalStore(
    () => new ManageFilterListModel(),
  );
  const filterModel = useLocalStore(() => new FilterModel());

  const followUpDashboardModel = useLocalStore(
    () => new FollowUpDashboardModel(),
  );
  const followUpFilterListModel = useLocalStore(
    () => new FollowUpFilterListModel(),
  );
  const [isFirstTime, setIsFirstTime] = useState(true);

  const isStaff = () => {
    if (
      !authContext.roles.isAdmin &&
      !authContext.roles.isDM &&
      !authContext.roles.isVP &&
      !authContext.roles.isIcAgent &&
      !authContext.roles.isCompliance
    ) {
      return true;
    }

    return false;
  };

  const getDashboardData = async (
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    processAnd = [],
    processOr = [],
    objectives = []
  ) => {
    try {
      await new Promise([
        followUpDashboardModel.getDashboardFollowUp(
          token,
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const clearFilterDashboard = () => {
    if (authContext.roles.isAdmin) {
      filterModel.resetFilterDBFAdmin();
    } else if (authContext.roles.isVP || authContext.roles.isIcAgent) {
      filterModel.resetFilterDBFVP();
    } else {
      filterModel.resetFilterDBFDM();
    }
  };

  const renderGraphDTO = () => {
    return followUpDashboardModel.dashboardList.map(
      (dashboard, dashboardIndex) => (
        <Div
          col
          mid
          center
          width={50}
          border={dashboardIndex !== 1}
          padding={12}
        >
          {dashboard.isAllGraphNoData ? (
            <Text
              color={colors.textlightGray}
              fontWeight="med"
              fontSize={sizes.xl}
            >
              No Data
            </Text>
          ) : (
            <>
              <ChartPie
                data={dashboard.percentGraphDTO}
                backgroundColor={dataPieList.color}
                labels={['Overdue', 'Open', 'Postpone', 'Closed']}
              />

              <Div between>
                {dashboard.graphDTO.map((graph) => (
                  <SectionLine>
                    <Text color={colors.primaryBlack}>
                      {`${graph.percent.toFixed(2)}%`}
                    </Text>
                  </SectionLine>
                ))}
              </Div>

              <Div between>
                {dashboard.graphDTO.map((graph) => (
                  <SectionLine>
                    <Text color={colors.primaryBlack}>
                      {`Total ${graph.count}`}
                    </Text>
                  </SectionLine>
                ))}
              </Div>

              <Div between>
                {dashboard.graphDTO.map((graph, graphIndex) => (
                  <SectionLine>
                    <Oval bgcolor={dataPieList.color[graphIndex]} />
                    <Text
                      style={{ paddingLeft: 8 }}
                      color={colors.primaryBlack}
                      className="upper"
                      fontWeight="bold"
                      fontSize={sizes.xxs}
                    >
                      {graph.status}
                    </Text>
                  </SectionLine>
                ))}
              </Div>
            </>
          )}
        </Div>
      ),
    );
  };

  // get managefilter แยกกับ useeffect ตัวอื่น
  // เพราะต้องการไว้ใช้กับกลุ่ม admin/VP และ dm/staff ด้วย
  useEffect(() => {
    (async () => {
      try {
        await manageFilterListModel.getFollowUpDashboardManageFilterList();
        await followUpFilterListModel.getDashboardFilterList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    if (authContext.roles.isDM || isStaff()) {
      if (isFirstTime && filterModel.selectedDepartment.no) {
        setIsFirstTime(false);

        getDashboardData(
          authContext.accessToken,
          filterModel.selectedYear,
          filterModel.selectedBu,
          filterModel.selectedDepartment.no,
          filterModel.selectedDivision,
          filterModel.selectedProcessAnd,
          filterModel.selectedProcessOr,
          filterModel.selectedObjectives
        );
      }
    }
  }, [filterModel.selectedDepartment.no]);

  useEffect(() => {
    if (authContext.roles.isAdmin) {
      if (isFirstTime && filterModel.selectedYear) {
        setIsFirstTime(false);

        getDashboardData(
          authContext.accessToken,
          filterModel.selectedYear,
          filterModel.selectedBu,
          filterModel.selectedDepartment.no,
          filterModel.selectedDivision,
          filterModel.selectedProcessAnd,
          filterModel.selectedProcessOr,
          filterModel.selectedObjectives
        );
      }
    }
  }, [filterModel.selectedYear]);

  useEffect(() => {
    if (authContext.roles.isVP || authContext.roles.isEVP) {
      if (isFirstTime && filterModel.selectedBu) {
        setIsFirstTime(false);

        getDashboardData(
          authContext.accessToken,
          filterModel.selectedYear,
          filterModel.selectedBu,
          filterModel.selectedDepartment.no,
          filterModel.selectedDivision,
          filterModel.selectedProcessAnd,
          filterModel.selectedProcessOr,
          filterModel.selectedObjectives
        );
      }
    }
  }, [filterModel.selectedBu]);

  useEffect(() => {
    (async () => {
      if (authContext.roles.isIcAgent) {
        if (filterModel.selectedBu) {
          const bu = await manageBUDropdownStore.getManageFilterBUList(
            authContext.accessToken,
          );

          filterModel.setField('selectedBu', bu[0].value);

          getDashboardData(
            authContext.accessToken,
            filterModel.selectedYear,
            filterModel.selectedBu,
            filterModel.selectedDepartment.no,
            filterModel.selectedDivision,
            filterModel.selectedProcessAnd,
            filterModel.selectedProcessOr,
            filterModel.selectedObjectives
          );
        }
      }
    })();
  }, [filterModel.selectedBu]);

  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <div>
        <FollowUpDashboardContainerFilter
          followUpFilterListModel={followUpFilterListModel}
          filterModel={filterModel}
          onSubmit={getDashboardData}
          onClear={clearFilterDashboard}
          name="Follow Up Dashboard"
        />

        <Grid style={{ marginTop: 24 }}>
          <CardTabHeader border>
            <Div center width={50} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                Corrective Action Status
              </Text>

              <Popup
                trigger={
                  <Image
                    width={18}
                    style={{ marginLeft: 8 }}
                    src="../../static/images/iconRemark@3x.png"
                  />
                }
                content={
                  <>
                    <Text>Closed : After DM submitted</Text>
                    <Text>Postpone: After VP Approved</Text>
                  </>
                }
              />
            </Div>
            <Div center width={50} padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                Enhancement Status
              </Text>

              <Popup
                trigger={
                  <Image
                    width={18}
                    style={{ marginLeft: 8 }}
                    src="../../static/images/iconRemark@3x.png"
                  />
                }
                content={
                  <>
                    <Text>Closed : After DM submitted</Text>
                    <Text>Postpone: After VP Approved</Text>
                  </>
                }
              />
            </Div>
          </CardTabHeader>

          <CardTabBody between>
            {followUpDashboardModel.dashboardList && renderGraphDTO()}
          </CardTabBody>
        </Grid>

        <Grid style={{ marginTop: 32 }} doubling columns={2}>
          <Grid.Row>
            {followUpDashboardModel.dashboardList?.map((dashboard, index) => (
              <Grid.Column>
                <CardTabHeader border style={{ margin: '0' }}>
                  <Div center padding={24}>
                    <Text
                      fontSize={sizes.l}
                      fontWeight="bold"
                      color={colors.textPurple}
                    >
                      {dashboard.name === 'corrective'
                        ? 'Corrective Action Status'
                        : 'Enhancement Status'}
                    </Text>
                  </Div>
                </CardTabHeader>

                <CardTabBody between style={{ margin: '0' }}>
                  <Div center col padding={24}>
                    <TableHeader span={12}>
                      {headersDonut2.map(({ render, key, color, width }) => (
                        <Cell
                          center
                          border
                          key={key}
                          color={color}
                          width={width}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={
                              render === 'Division'
                                ? colors.primaryBlack
                                : colors.primaryBackground
                            }
                          >
                            {render}
                          </Text>
                        </Cell>
                      ))}
                    </TableHeader>

                    {dashboard.tableDTO.map((table) => (
                      <TableBody span={12} key={index} borderTop>
                        <Cell width={20}>
                        <Popup
                          trigger={
                            <Text
                              fontSize={sizes.xxs}
                              fontWeight={table.indicator.length <= 6 && 'bold'}
                              color={colors.primaryBlack}
                            >
                              {table.indicator}
                          </Text>
                          }
                          content={`${table.divEn || '-'} ${table.shiftEn || ''}`}
                          size="small"
                        />
                        </Cell>
                        <Cell width={20}>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {table.overDue || '-'}
                          </Text>
                        </Cell>
                        <Cell width={20}>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {table.open || '-'}
                          </Text>
                        </Cell>
                        <Cell width={20}>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {table.postpone || '-'}
                          </Text>
                        </Cell>
                        <Cell width={20}>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {table.closed || '-'}
                          </Text>
                        </Cell>
                      </TableBody>
                    ))}
                  </Div>
                </CardTabBody>
              </Grid.Column>
            ))}
          </Grid.Row>
        </Grid>
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
