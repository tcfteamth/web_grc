import React, { useState, useEffect } from 'react';
import { Image, Grid, Button, Popup } from 'semantic-ui-react';
import Router from 'next/router';
import Link from 'next/link';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import {
  Text,
  ModalGlobal,
  BottomBarFollowDetail,
  CardComment,
  CommentBox,
} from '../../components/element';
import { colors, sizes } from '../../utils';
import { withLayout } from '../../hoc';
import { AssessmentDetailModel } from '../../model/AssessmentModel';
import { initAuthStore } from '../../contexts';
import { FollowUpDetailModel } from '../../model/FollowUpModel';
import {
  CardFollowUpWorkFlow,
  FollowUpCardDetail,
  FollowUpInfoDetail,
  FollowUpMainDetail,
} from '../../components/FollowUpPage';

const CardContainer = styled.div`
  margin-bottom: 98px;
  padding-left: 16px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = (props) => {
  const authContext = initAuthStore();
  const assessmentDetailModel = useLocalStore(
    () => new AssessmentDetailModel(),
  );
  const [isOpen, setIsOpen] = useState('Assessment');
  const [confirm, setConfirm] = useState(false);
  const followUpDetailModel = useLocalStore(() => new FollowUpDetailModel());
  const authStore = initAuthStore();

  const handelCallBack = () => {
    setTimeout(() => {
      Router.back();
      setConfirm(false);
    }, 500);
  };

  useEffect(() => {
    (async () => {
      try {
        await followUpDetailModel.getFollowUpDetail(
          authStore.accessToken,
          Router.query.id,
        );

        await followUpDetailModel.getFollowUpCommentList(Router.query.id);
      } catch (e) {
        console.log(e);
      }
    })();
  }, [Router.query.id]);

  return useObserver(() => (
    <Div>
      <CardContainer>
        <div
          className="click"
          style={{ width: '10%', display: 'flex' }}
          onClick={() => setConfirm(true)}
        >
          <Image
            src="../../static/images/black@3x.png"
            style={{ marginRight: 12, width: 18, height: 18 }}
          />
          <Text
            color={colors.primaryBlack}
            fontWeight="bold"
            fontSize={sizes.xl}
          >
            Back
          </Text>
        </div>

        <div style={{ width: '100%', marginTop: 32 }}>
          <Grid style={{ padding: 0, margin: 0 }}>
            <Grid.Column
              tablet={16}
              computer={8}
              style={{ padding: 2, width: '100%' }}
            >
              {followUpDetailModel && (
                <FollowUpCardDetail followUpDetailModel={followUpDetailModel} />
              )}
            </Grid.Column>
            <Grid.Column
              tablet={16}
              computer={8}
              style={{ padding: 2, width: '100%' }}
            >
              {followUpDetailModel && (
                <FollowUpInfoDetail followUpDetailModel={followUpDetailModel} />
              )}
            </Grid.Column>
            <Grid.Row columns={2}>
              <Grid.Column
                width={8}
                floated="left"
                style={{ padding: '0px 4px 0px 0px' }}
              >
                <Button.Group fluid>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 'Workflow'
                          ? colors.textSky
                          : colors.backgroundPrimary
                      }`,
                      border: assessmentDetailModel
                        .validateAssessmentDetailModel.validateWorkflowFiles
                        ? '2px solid red'
                        : 'none',
                    }}
                    onClick={() => setIsOpen('Workflow')}
                  >
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 'Workflow'
                          ? colors.backgroundPrimary
                          : colors.textSky
                      }
                    >
                      Workflow
                    </Text>
                  </Button>
                  <Button
                    style={{
                      padding: '10px 0px',
                      backgroundColor: `${
                        isOpen === 'Assessment'
                          ? colors.textSky
                          : colors.backgroundPrimary
                      }`,
                    }}
                    onClick={() => setIsOpen('Assessment')}
                  >
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.s}
                      style={{ textTransform: 'uppercase' }}
                      color={
                        isOpen === 'Assessment'
                          ? colors.backgroundPrimary
                          : colors.textSky
                      }
                    >
                      Assessment
                    </Text>
                  </Button>
                </Button.Group>
              </Grid.Column>
              <Grid.Column
                width={8}
                floated="right"
                style={{ paddingRight: 0 }}
              >
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}
                >
                  {followUpDetailModel.lastYearPath && (
                    <div className="click" style={{ marginRight: '16px' }}>
                      <a
                        href={followUpDetailModel.lastYearPath}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <Text fontSize={sizes.xs} color={colors.textSky}>
                          Assessment
                        </Text>
                      </a>
                    </div>
                  )}

                  {authContext.roles.isAdmin && assessmentDetailModel.info && (
                    <Popup
                      position="top right"
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        minWidth: 100,
                        height: 32,
                        padding: '4px 15px',
                        borderRadius: '6px 6px 0px 6px',
                        alignItems: 'center',
                      }}
                      trigger={
                        <Link href={`/Assessment/assessmentListIAFinding`}>
                          <a target="_blank">
                            <Button
                              style={{
                                backgroundColor: colors.textPurple,
                                padding: '7px 24px',
                                borderRadius: '6px 6px 0px 6px',
                              }}
                            >
                              <Image
                                src="../../static/images/Finding@3x.png"
                                style={{ width: 24, height: 24 }}
                              />
                            </Button>
                          </a>
                        </Link>
                      }
                      flowing
                      basic
                      hoverable
                    >
                      <Div mid center>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="med"
                          color={colors.primary}
                        >
                          IA Finding
                        </Text>
                      </Div>
                    </Popup>
                  )}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {followUpDetailModel && isOpen === 'Assessment' ? (
                <FollowUpMainDetail
                  followUpDetailModel={followUpDetailModel}
                  followUpId={Router.query.id}
                />
              ) : (
                <CardFollowUpWorkFlow
                  followUpDetailModel={followUpDetailModel}
                />
              )}
            </Grid.Row>

            {followUpDetailModel.commentList?.map((comment) => (
              <Grid.Row style={{ width: '100%' }}>
                <CardComment commentDetail={comment} />
              </Grid.Row>
            ))}

            <CommentBox
              onChange={(text) =>
                followUpDetailModel.setField('commentText', text)
              }
              value={followUpDetailModel.commentText}
            />

            <BottomBarFollowDetail followUpDetailModel={followUpDetailModel} />
          </Grid>
        </div>
      </CardContainer>

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to leave this page?"
        content="Please ensure that you have saved the information before you leave."
        onSubmit={handelCallBack}
        submitText={'YES'}
        cancelText={'NO'}
        onClose={() => setConfirm(false)}
      />
    </Div>
  ));
};

export default withLayout(observer(index));
