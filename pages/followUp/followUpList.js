import React, { useState, useEffect } from 'react';
import { Header } from 'semantic-ui-react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import Link from 'next/link';
import styled from 'styled-components';
import { ButtonBorder, Text, SearchInput } from '../../components/element';
import {
  FollowUpTodoList,
  FollowUpSummary,
  FollowUpCardsContainer,
  FollowUpApproveList,
  ContainerFilter,
} from '../../components/FollowUpPage';
import { colors, sizes } from '../../utils';
import { ListAssess, ListSummary } from '../../utils/static';
import {
  FollowUpListModel,
  FollowUpSummaryModel,
} from '../../model/FollowUpModel';
import { initAuthStore } from '../../contexts';
import { withLayout } from '../../hoc';
import { manageBUDropdownStore } from '../../model';

const CardBox = styled.div`
  display: flex;
  flexdirection: row;
  alignitems: center;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const Cardtop = styled.div`
  border-radius: ${(props) =>
    props.right ? '0px 6px 6px 0px' : props.left ? '6px 0px 0px 6px' : '2px'};
  height: 38px;
  min-width: 120px;
  display: flex;
  padding: 8px 16px;
  flex-direction: column;
  align-items: center;
  align-self: center;
  justify-content: center;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'Agent', value: 1 },
  { text: 'Team', value: 2 },
  { text: 'Walk in', value: 3 },
];

const index = (props) => {
  const authContext = initAuthStore();
  const [activeTab, setActiveTab] = useState('TODO');
  const [isShowFillter, setIsShowFillter] = useState(false);
  const followUpModel = useLocalStore(() => new FollowUpListModel());
  const followUpSummaryModel = useLocalStore(() => new FollowUpSummaryModel());

  const handelOpen = (value) => {
    console.log('value', value);
    setActiveTab(value);
  };
  const handelOpenFillter = (value) => {
    console.log('value', value);
    setIsShowFillter(value);
  };

  const getFollowUpTodoList = async () => {
    try {
      await followUpModel.getFollowUpTodoList(
        authContext.accessToken,
        1,
        10,
        followUpModel.filterModel.searchText,
        '',
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
        followUpModel.filterModel.selectedProcessAnd,
        followUpModel.filterModel.selectedProcessOr,
        followUpModel.filterModel.selectedObjectives,
      );
    } catch (e) {
      console.log(e);
    }
  };

  const getFollowUpSummaryList = async () => {
    try {
      await followUpSummaryModel.getFollowUpSummaryList(
        authContext.accessToken,
        1,
        10,
        followUpModel.filterModel.searchText,
        followUpModel.filterModel.selectedYear,
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
        followUpModel.filterModel.selectedProcessAnd,
        followUpModel.filterModel.selectedProcessOr,
        followUpModel.filterModel.selectedObjectives,
      );
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    (async () => {
      try {
        if (!authContext.roles.isEVP) {
          await new Promise([
            getFollowUpTodoList(),
            getFollowUpSummaryList(),
            followUpModel.getFollowUpTotalStatusCount(authContext.accessToken),
          ]);
        }
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      if (authContext.roles.isEVP) {
        const bu = await manageBUDropdownStore.getManageFilterBUList(
          authContext.accessToken,
        );

        followUpModel.filterModel.setField('selectedBu', bu[0].value);

        await followUpModel.getFollowUpTodoList(
          authContext.accessToken,
          1,
          10,
          followUpModel.filterModel.searchText,
          '',
          followUpModel.filterModel.selectedLvl3,
          followUpModel.filterModel.selectedLvl4,
          followUpModel.filterModel.selectedBu,
          followUpModel.filterModel.selectedDepartment.no,
          followUpModel.filterModel.selectedDivision,
          followUpModel.filterModel.selectedShift,
          followUpModel.filterModel.isSetStartDueDate,
          followUpModel.filterModel.isSetEndDueDate,
          followUpModel.filterModel.selectedStatus,
        );

        await followUpSummaryModel.getFollowUpSummaryList(
          authContext.accessToken,
          1,
          10,
          followUpModel.filterModel.searchText,
          followUpModel.filterModel.selectedYear,
          followUpModel.filterModel.selectedLvl3,
          followUpModel.filterModel.selectedLvl4,
          followUpModel.filterModel.selectedBu,
          followUpModel.filterModel.selectedDepartment.no,
          followUpModel.filterModel.selectedDivision,
          followUpModel.filterModel.selectedShift,
          followUpModel.filterModel.isSetStartDueDate,
          followUpModel.filterModel.isSetEndDueDate,
          followUpModel.filterModel.selectedStatus,
        );
      }
    })();
  }, []);

  const renderTabTodoList = () => {
    if (
      authContext.roles.isAdmin ||
      authContext.roles.isVP ||
      authContext.roles.isEVP
    ) {
      return (
        <FollowUpApproveList followUpModel={followUpModel} data={ListAssess} />
      );
    }

    return <FollowUpTodoList followUpModel={followUpModel} data={ListAssess} />;
  };

  const renderTabSummary = () => (
    <FollowUpSummary
      data={ListSummary}
      followUpSummaryModel={followUpSummaryModel}
      followUpModel={followUpModel}
    />
  );

  return useObserver(() => (
    <div style={{ marginBottom: 96 }}>
      <Div bottom={24} between>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xl}
            >
              Follow Up
            </Text>
          </Header.Content>
        </Header>
      </Div>

      {followUpModel.totalStatusCount && (
        <FollowUpCardsContainer followUpModel={followUpModel} />
      )}

      <Div marginTop={32} between>
        <CardBox className="click">
          <Cardtop
            bgcolor={
              activeTab === 'TODO' ? '#0057b8' : colors.backgroundPrimary
            }
            left
            onClick={() => handelOpen('TODO')}
          >
            <Div>
              <Text
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ textTransform: 'uppercase' }}
                color={
                  activeTab === 'TODO' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                TO DO LIST
              </Text>
              <Text
                fontWeight="med"
                fontSize={sizes.s}
                style={{ paddingLeft: 8 }}
                color={
                  activeTab === 'TODO' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                {`(${followUpModel?.totalContent ?? 0})`}
              </Text>
            </Div>
          </Cardtop>
          <Cardtop
            bgcolor={
              activeTab === 'SUMMARY' ? '#0057b8' : colors.backgroundPrimary
            }
            right
            onClick={() => handelOpen('SUMMARY')}
          >
            <Div>
              <Text
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ textTransform: 'uppercase' }}
                color={
                  activeTab === 'SUMMARY' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                Summary
              </Text>
              <Text
                fontWeight={'med'}
                fontSize={sizes.s}
                style={{ paddingLeft: 8 }}
                color={
                  activeTab === 'SUMMARY' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                {`(${followUpSummaryModel?.totalContent ?? 0})`}
              </Text>
            </Div>
          </Cardtop>
        </CardBox>

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        >
          <Div>
            <ButtonBorder
              textWeight="bold"
              textSize={sizes.xxs}
              width={156}
              options={sortBy}
              text="Advanced Filter"
              color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
              icon={
                isShowFillter
                  ? '../../static/images/fillter-white@3x.png'
                  : '../../static/images/fillter@3x.png'
              }
              textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
              borderColor="#00aeef"
              handelOnClick={() => handelOpenFillter(!isShowFillter)}
            />
          </Div>
          <Div>
            <SearchInput
              placeholder="Search"
              onChange={(e) => {
                followUpModel.filterModel.setField(
                  'searchText',
                  e.target.value,
                );
              }}
              value={followUpModel.filterModel.searchText}
              onEnterKeydown={(e) => {
                if (e.keyCode === 13) {
                  if (activeTab === 'TODO') {
                    getFollowUpTodoList();
                  } else {
                    getFollowUpSummaryList();
                  }
                }
              }}
            />
          </Div>
        </div>
      </Div>

      {isShowFillter && (
        <ContainerFilter
          followUpModel={followUpModel}
          activeTab={activeTab}
          followUpSummaryModel={followUpSummaryModel}
          onClose={() => setIsShowFillter(false)}
        />
      )}

      {followUpModel.followUpList && activeTab === 'TODO'
        ? renderTabTodoList()
        : renderTabSummary()}
    </div>
  ));
};

export default withLayout(observer(index));
