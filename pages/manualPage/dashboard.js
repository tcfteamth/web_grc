import React, { useState, useEffect } from 'react';
import { Image, Grid, Loader, Dimmer, Segment } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { Text } from '../../components/element';
import { colors, sizes } from '../../utils';
import { ListAssess, dataListFollow } from '../../utils/static';

import { withLayout } from '../../hoc';
import { initAuthStore } from '../../contexts';

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;
const ImageItem = styled(Image)`
  @media only screen and (min-width: 768px) {
    width: auto;
    height: 100px !important;
  }
  @media only screen and (min-width: 1440px) {
    width: auto;
    height: 250px !important;
  }
`;
const CardItem = styled(Segment)`
  width: 100%;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 32px 25px !important;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 230px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 372px;
  }
`;

const index = (props) => {
  const [screenWidth, setScreenWidth] = useState();
  const authContext = initAuthStore();

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div between>
        <Text
          color={colors.primaryBlack}
          fontWeight="bold"
          fontSize={sizes.xxl}
        >
          Manual
        </Text>
      </Div>
      <div style={{ paddingTop: 32 }}>
        <Grid columns="equal">
          <Grid.Row>
            <Grid.Column>
              <a
                href="https://pttgcgroup.sharepoint.com/:f:/s/dept_S-RC/EiFNRfJU8lRPnclZqgGwv5AB-UBe-sXgzdW5y_iTESMrvg?e=CCZDEk"
                target="_blank"
                rel="noopener noreferrer"
              >
                <CardItem className=" click">
                  <ImageItem src="../../static/images/process-database-4-x@3x.png" />
                  <Text
                    color={colors.textSky}
                    fontWeight="bold"
                    fontSize={screenWidth === 1366 ? sizes.m : sizes.l}
                  >
                    Manual
                  </Text>
                </CardItem>
              </a>
            </Grid.Column>
            {/* <Grid.Column>
              <a
                href="https://pttgcgroup.sharepoint.com/:f:/s/dept_S-RC/El_Py0RdRaJCrdRfCQsJ2pMBJEdyT91WyJOz1APqZ7n7kQ?e=1EFadT"
                target="_blank"
                rel="noopener noreferrer"
              >
                <CardItem className=" click">
                  <ImageItem src="../../static/images/risk-control-4-x@3x.png" />
                  <Text
                    color={colors.textSky}
                    fontWeight="bold"
                    fontSize={screenWidth === 1366 ? sizes.m : sizes.l}
                    style={{ alignItem: 'center' }}
                  >
                    Video Tutorial
                  </Text>
                </CardItem>
              </a>
            </Grid.Column> */}
            {/* <Grid.Column>
              <a
                href="https://pttgcgroup.sharepoint.com/:f:/s/dept_S-RC/EiUOfcYm3RFNshG4T5_JMvAB1o45oTKfZLtFCLUOxeGOvw?e=wT3SIb"
                target="_blank"
                rel="noopener noreferrer"
              >
                <CardItem className=" click">
                  <ImageItem src="../../static/images/compliance-universe-4-x@3x.png" />
                  <Text
                    color={colors.textSky}
                    fontWeight="bold"
                    fontSize={screenWidth === 1366 ? sizes.m : sizes.l}
                  >
                    Internal Control Content
                  </Text>
                </CardItem>
              </a>
            </Grid.Column> */}
            <Grid.Column></Grid.Column>
            <Grid.Column></Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </div>
  );
};

export default withLayout(observer(index));
