import React, { useState, useEffect } from 'react';
import { Image, Grid, Card, Button, Header, Popup } from 'semantic-ui-react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { withLayout } from '../../hoc';
import {
  RoadMapAssessment,
  RoadMapRequests,
  RoadMapAssessmentAdmin,
  RoadMapAssessmentViewOnly,
  RoadMapRequestsAdmin,
} from '../../components/RoadMapPage';
import {
  ButtonAll,
  Text,
  ButtonBorder,
  CardEmpty,
  SearchInput,
  BottomBarRoadmap,
  ProcessLvl3Dropdown,
  ProcessLvl4Dropdown,
  RoadmapTypeDropdown,
  BUDropdown,
  DepartmentDropdown,
  DivisionDropdown,
  ShiftDropdown,
  StatusDropdown,
  YearDropdown,
  DropdownAll,
  DropdownSelect,
  NextYearAssessment,
} from '../../components/element';
import { CreateRoadmapModal } from '../../components/RoadMapPage/modal';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { RoadmapListModel, yearListDropdownStore,taglistDropdownStore } from '../../model';
import { RequestModel } from '../../model/Roadmap';
import useWindowDimensions from '../../hook/useWindowDimensions';

const CardFillter = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px !important;
  margin-bottom: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const Cards = styled(Card)`
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  width: 100% !important;
  min-hight: 80px;
  @media only screen and (min-width: 320px) {
    hight: 100px;
    padding: 10px !important;
  }
  @media only screen and (min-width: 768px) {
    hight: 100px;
    padding: 20px 16px !important;
  }
`;

const statusAdd = [
  { label: 'True', value: 'true' },
  { label: 'False', value: 'false' },
];

const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'sort 1', value: 1 },
  { text: 'sort 2', value: 2 },
  { text: 'sort 3', value: 3 },
];

const index = () => {
  const roadmapModel = useLocalStore(() => new RoadmapListModel());
  const requestModel = useLocalStore(() => new RequestModel());
  const authContext = initAuthStore();

  const [isOpen, setIsOpen] = useState('Assessment');
  const [isShowFillter, setIsShowFillter] = useState(false);
  const [active, setActive] = useState(false);

  const { width: screenWidth } = useWindowDimensions();
  const [defaultYear, setDefaultYear] = useState('');
  const tagListDropdown = useLocalStore(() => taglistDropdownStore);
  const [tagsList, setTagList] = useState();
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenFillter = (value) => {
    setIsShowFillter(value);
  };

  const setAlertModal = () => {
    setActive(true);
  };

  const handleCloseCreateRoadmapModal = (status) => {
    setActive(status);
  };

  const handleFailedCreateRoadmapModal = (status) => {
    setIsShowModal(status);
  };

  const [roadmapListData, setRoadmapListData] = useState();
  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };
  const getRoadmapList = async (year) => {
    try {
      await roadmapModel.getRoadmapList(
        authContext.accessToken,
        1,
        roadmapModel.searchRoadmapText,
        roadmapModel.filterModel.selectedLvl3,
        roadmapModel.filterModel.selectedLvl4,
        roadmapModel.filterModel.selectedBu,
        roadmapModel.filterModel.selectedRoadmapType,
        roadmapModel.filterModel.selectedDepartment.no,
        roadmapModel.filterModel.selectedDivision,
        roadmapModel.filterModel.selectedShift,
        roadmapModel.filterModel.selectedStatus,
        year,
        roadmapModel.filterModel.selectedProcessAnd,
        roadmapModel.filterModel.selectedProcessOr
      );
    } catch (e) {
      console.log(e);
    }
  };

  const getRequestList = async (year) => {
    try {
      await requestModel.getRequestList(
        authContext.accessToken,
        1,
        requestModel.filterModel.searchText,
        requestModel.filterModel.selectedRoadmapType,
        requestModel.filterModel.selectedDivision,
        year,
        requestModel.filterModel.selectedRead.value,
      );
    } catch (e) {
      console.log(e);
    }
  };
  const handleProcessTagSelection = (e,data,field) => {
    let result = roadmapModel.filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    requestModel.filterModel.setField(
      field,
      result,
    );
  }
  useEffect(() => {
    (async () => {
      try {
        const year = await yearListDropdownStore.getYearListFillter(
          authContext.accessToken,
        );

        setDefaultYear(year[1].value);
        roadmapModel.filterModel.setField('selectedYear', year[1].value);
        await getRoadmapList(year[1].value);
        await getRequestList(year[1].value);
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  // render data ตาม role ของ user
  const renderTabAssessment = (roadmapList) => {
    if (authContext.roles.isAdmin) {
      return <RoadMapAssessmentAdmin roadmapData={roadmapList} />;
    }
    if (authContext.roles.isDM) {
      return (
        <RoadMapAssessment
          roadmapData={roadmapList}
          requestModel={requestModel}
        />
      );
    }
    return <RoadMapAssessmentViewOnly roadmapData={roadmapList} />;
  };

  const renderBottombar = () => {
    if (authContext.roles.isAdmin) {
      return (
        <BottomBarRoadmap
          page="roadmapAdminAess"
          isSubmitToDmDisabled={roadmapModel.isSubmitToDmDisabled}
          roadmapModel={roadmapModel}
        />
      );
    }

    if (authContext.roles.isDM) {
      return (
        <BottomBarRoadmap
          page="listAssessmenrt"
          isAcceptDisabled={roadmapModel.isAcceptOrRejectDisabled}
          requestModel={requestModel}
          roadmapModel={roadmapModel}
        />
      );
    }
  };

  const renderTabRequests = () => {
    if (authContext.roles.isAdmin) {
      return <RoadMapRequestsAdmin requestModel={requestModel} />;
    }

    if (authContext.roles.isDM) {
      return <RoadMapRequests requestModel={requestModel} />;
    }
  };

  const renderEmptyCard = () => {
    if (authContext.roleName === 'SUPER-ADMIN') {
      return (
        <Grid.Row style={{ padding: 0 }}>
          <CardEmpty
            handelOnClick={() =>
              setAlertModal('Create RoadMap', '', '', '', 'Create Now')
            }
            Icon="../../static/images/iconRoadmap@3x.png"
            textTitle="No Roadmap"
            textButton="Create Roadmap "
          />
        </Grid.Row>
      );
    }

    return (
      <Grid.Row style={{ padding: 0 }}>
        <CardEmpty
          Icon="../../static/images/iconRoadmap@3x.png"
          textTitle="No Roadmap"
        />
      </Grid.Row>
    );
  };

  const renderTab = () => {
    if (authContext.roles.isDM || authContext.roles.isAdmin) {
      return (
        <Grid.Column
          tablet={8}
          computer={8}
          mobile={16}
          style={{ padding: 0, display: 'flex', alignItems: 'center' }}
        >
          <Button.Group style={{ width: screenWidth < 768 && '100%' }}>
            <Button
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                padding: '10px 20px',
                backgroundColor: `${
                  isOpen === 'Assessment' ? '#0057b8' : colors.backgroundPrimary
                }`,
              }}
              onClick={() => {
                handelOpen('Assessment');
              }}
            >
              <Text
                fontWeight="med"
                fontSize={sizes.s}
                color={
                  isOpen === 'Assessment' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                ASSESSMENT
              </Text>
              <Text
                fontSize={sizes.s}
                style={{ paddingLeft: 8 }}
                color={
                  isOpen === 'Assessment' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                {roadmapListData && `(${roadmapListData.length})`}
              </Text>
            </Button>
            <Button
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                padding: '10px 20px',
                backgroundColor: `${
                  isOpen === 'Requests' ? '#0057b8' : colors.backgroundPrimary
                }`,
              }}
              onClick={() => handelOpen('Requests')}
            >
              <Text
                fontWeight="med"
                fontSize={sizes.s}
                color={
                  isOpen === 'Requests' ? colors.backgroundPrimary : '#0057b8'
                }
                style={{ display: 'flex', alignContent: 'center' }}
              >
                REQUESTS{' '}
              </Text>
              <Text
                fontSize={sizes.s}
                style={{ paddingLeft: 8 }}
                color={
                  isOpen === 'Requests' ? colors.backgroundPrimary : '#0057b8'
                }
              >
                {`(${requestModel.totalContent})`}
              </Text>
            </Button>
          </Button.Group>
        </Grid.Column>
      );
    }

    return (
      <Grid.Column
        width={4}
        style={{ padding: 0, display: 'flex', alignItems: 'center' }}
      >
        <Div center>
          <Text
            fontSize={sizes.s}
            fontWeight="bold"
            color={colors.primaryBlack}
          >
            ASSESSMENT LIST
          </Text>
          <Text
            fontSize={sizes.s}
            color={colors.primaryBlack}
            style={{ paddingLeft: 8 }}
          >
            {roadmapListData && `(${roadmapListData.length} รายการ)`}
          </Text>
        </Div>
      </Grid.Column>
    );
  };

  return useObserver(() => (
    <div>
      <Div between>
        <Header>
          <Header.Content>
            <Text
              color={colors.primaryBlack}
              fontWeight="bold"
              fontSize={sizes.xxl}
            >
              CSA Roadmap
            </Text>
          </Header.Content>
        </Header>
        {/* <Header>
          <Header.Content>
            <Text
              style={{ textAlign: 'center' }}
              fontSize={sizes.xl}
              color={colors.primary}
            >
              Year 2020
            </Text>
          </Header.Content>
        </Header> */}
      </Div>
      <div style={{ marginTop: 32 }}>
        {/* // card header  */}
        <Grid
          style={{
            justifyContent: 'space-around',
            padding:
              screenWidth <= 425
                ? '0 10px'
                : screenWidth <= 768
                ? '0 6px'
                : '0 4px',
            marginTop: 16,
          }}
        >
          <Grid.Column
            computer={5}
            mobile={5}
            tablet={5}
            style={{ padding: 0 }}
          >
            <Cards>
              <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                <Grid.Column floated="left">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    {screenWidth < 768 ? (
                      <Popup
                        wide="very"
                        style={{
                          borderRadius: '6px 6px 0px 6px',
                          marginRight: -10,
                        }}
                        trigger={
                          <Image
                            src="../../static/images/new@3x.png"
                            style={{ width: 40, height: 40 }}
                          />
                        }
                        content={
                          <Text
                            fontWeight="bold"
                            fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                            color={colors.pink}
                          >
                            NEW ASSESSMENT
                          </Text>
                        }
                      />
                    ) : (
                      <Image
                        src="../../static/images/new@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                    )}
                    {screenWidth >= 768 && (
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                          color={colors.pink}
                        >
                          NEW ASSESSMENT
                        </Text>
                      </div>
                    )}
                  </div>
                </Grid.Column>
                <Grid.Column floated="right">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.xl}
                      color={colors.primaryBlack}
                    >
                      {roadmapModel && roadmapModel.totalNewAssessment
                        ? roadmapModel.totalNewAssessment
                        : 0}
                    </Text>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Cards>
          </Grid.Column>
          <Grid.Column
            computer={5}
            mobile={5}
            tablet={5}
            style={{ padding: 0 }}
          >
            <Cards>
              <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                <Grid.Column floated="left">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    {screenWidth < 768 ? (
                      <Popup
                        wide="very"
                        style={{
                          borderRadius: '6px 6px 0px 6px',
                          marginRight: -10,
                        }}
                        position="top center"
                        trigger={
                          <Image
                            src="../../static/images/re@3x.png"
                            style={{ width: 40, height: 40 }}
                          />
                        }
                        content={
                          <Text
                            fontWeight="bold"
                            fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                            color={colors.orange}
                          >
                            RE ASSESSMENT
                          </Text>
                        }
                      />
                    ) : (
                      <Image
                        src="../../static/images/re@3x.png"
                        style={{ width: 40, height: 40 }}
                      />
                    )}
                    {screenWidth >= 768 && (
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                          color={colors.orange}
                        >
                          RE ASSESSMENT
                        </Text>
                      </div>
                    )}
                  </div>
                </Grid.Column>
                <Grid.Column floated="right">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.xl}
                      color={colors.primaryBlack}
                    >
                      {roadmapModel && roadmapModel.totalReAssessment
                        ? roadmapModel.totalReAssessment
                        : 0}
                    </Text>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Cards>
          </Grid.Column>
          <Grid.Column
            computer={5}
            mobile={5}
            tablet={5}
            style={{ padding: 0 }}
          >
            <Cards>
              <Grid.Row style={{ display: 'flex', alignItems: 'center' }}>
                <Grid.Column floated="left">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    {screenWidth < 1024 ? (
                      <Popup
                        wide="very"
                        style={{
                          borderRadius: '6px 6px 0px 6px',
                          marginLeft: -10,
                        }}
                        position="top right"
                        trigger={
                          <NextYearAssessment
                            year={new Date().getFullYear() + 1}
                          />
                        }
                        content={
                          <Text
                            fontWeight="bold"
                            fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                            color={colors.textBrightGray}
                          >
                            ASSESSMENT
                          </Text>
                        }
                      />
                    ) : (
                      <NextYearAssessment year={new Date().getFullYear() + 1} />
                    )}
                    {screenWidth >= 768 && (
                      <div style={{ alignItems: 'center', paddingLeft: 16 }}>
                        <Text
                          fontWeight="bold"
                          fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                          color={colors.textBrightGray}
                        >
                          ASSESSMENT
                        </Text>
                      </div>
                    )}
                  </div>
                </Grid.Column>
                <Grid.Column floated="right">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.xl}
                      color={colors.primaryBlack}
                    >
                      {roadmapModel && roadmapModel.totalNextYearAssessment
                        ? roadmapModel.totalNextYearAssessment
                        : 0}
                    </Text>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Cards>
          </Grid.Column>
        </Grid>

        <Grid style={{ margin: 0, paddingTop: 16 }}>
          <Grid.Row>
            {renderTab()}
            <Grid.Column
              floated="right"
              tablet={8}
              computer={8}
              mobile={16}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent:
                  screenWidth < 768 ? 'space-between' : 'flex-end',
                padding: screenWidth < 768 ? '8px 0px 0px' : 0,
              }}
            >
              <ButtonBorder
                textWeight="med"
                style={{ marginRight: 8 }}
                textSize={sizes.xs}
                width={screenWidth < 768 ? 56 : 156}
                options={sortBy}
                text={screenWidth < 768 ? '' : 'Advanced Filter'}
                color={isShowFillter ? '#00aeef' : colors.backgroundPrimary}
                icon={
                  isShowFillter
                    ? '../../static/images/fillter-white@3x.png'
                    : '../../static/images/fillter@3x.png'
                }
                textColor={isShowFillter ? colors.backgroundPrimary : '#00aeef'}
                borderColor="#00aeef"
                handelOnClick={() => {
                  handelOpenFillter(!isShowFillter);
                  if (isShowFillter) {
                    roadmapModel.filterModel.resetFilter();
                    if (isOpen === 'Assessment') {
                      roadmapModel.getRoadmapList(
                        authContext.accessToken,
                        1,
                        roadmapModel.searchRoadmapText,
                        roadmapModel.filterModel.selectedLvl3,
                        roadmapModel.filterModel.selectedLvl4,
                        roadmapModel.filterModel.selectedBu,
                        roadmapModel.filterModel.selectedRoadmapType,
                        roadmapModel.filterModel.selectedDepartment.no,
                        roadmapModel.filterModel.selectedDivision,
                        roadmapModel.filterModel.selectedShift,
                        roadmapModel.filterModel.selectedStatus,
                        defaultYear,                
                        roadmapModel.filterModel.selectedProcessAnd,
                        roadmapModel.filterModel.selectedProcessOr
                      );
                    } else {
                      requestModel.getRequestList(
                        authContext.accessToken,
                        1,
                        requestModel.filterModel.searchText,
                        requestModel.filterModel.selectedRoadmapType,
                        requestModel.filterModel.selectedDivision,
                        requestModel.filterModel.selectedYear,
                        requestModel.filterModel.selectedRead.value,
                      );
                    }
                  }
                }}
              />
              <SearchInput
                placeholder="Search"
                onChange={(e) => {
                  if (isOpen === 'Assessment') {
                    roadmapModel.setSearchRoadmapText(e.target.value);
                  } else {
                    requestModel.filterModel.setField(
                      'searchText',
                      e.target.value,
                    );
                  }

                  if (!roadmapModel.searchRoadmapText) {
                    if (isOpen === 'Assessment') {
                      roadmapModel.getRoadmapList(
                        authContext.accessToken,
                        1,
                        roadmapModel.searchRoadmapText,
                        roadmapModel.filterModel.selectedLvl3,
                        roadmapModel.filterModel.selectedLvl4,
                        roadmapModel.filterModel.selectedBu,
                        roadmapModel.filterModel.selectedRoadmapType,
                        roadmapModel.filterModel.selectedDepartment.no,
                        roadmapModel.filterModel.selectedDivision,
                        roadmapModel.filterModel.selectedShift,
                        roadmapModel.filterModel.selectedStatus, defaultYear
                      );
                    } else {
                      requestModel.getRequestList(
                        authContext.accessToken,
                        1,
                        requestModel.filterModel.searchText,
                        requestModel.filterModel.selectedRoadmapType,
                        requestModel.filterModel.selectedDivision,
                        requestModel.filterModel.selectedYear,
                        requestModel.filterModel.selectedRead.value,
                      );
                    }
                  }
                }}
                onEnterKeydown={(e) => {
                  // keycode 13 คือ ปุ่ม enter
                  if (e.keyCode === 13) {
                    if (isOpen === 'Assessment') {
                      roadmapModel.getRoadmapList(
                        authContext.accessToken,
                        1,
                        roadmapModel.searchRoadmapText,
                        roadmapModel.filterModel.selectedLvl3,
                        roadmapModel.filterModel.selectedLvl4,
                        roadmapModel.filterModel.selectedBu,
                        roadmapModel.filterModel.selectedRoadmapType,
                        roadmapModel.filterModel.selectedDepartment.no,
                        roadmapModel.filterModel.selectedDivision,
                        roadmapModel.filterModel.selectedShift,
                        roadmapModel.filterModel.selectedStatus, defaultYear
                      );
                    } else {
                      requestModel.getRequestList(
                        authContext.accessToken,
                        1,
                        requestModel.filterModel.searchText,
                        requestModel.filterModel.selectedRoadmapType,
                        requestModel.filterModel.selectedDivision,
                        requestModel.filterModel.selectedYear,
                        requestModel.filterModel.selectedRead.value,
                      );
                    }
                  }
                }}
                value={
                  isOpen === 'Assessment'
                    ? roadmapModel.searchRoadmapText
                    : requestModel.filterModel.searchText
                }
              />
            </Grid.Column>
          </Grid.Row>

          {isShowFillter && (
            <CardFillter>
              <Grid style={{ margin: 0, padding: 0 }}>
                <Div between style={{ padding: 0 }}>
                  <Text
                    fontWeight="med"
                    fontSize={sizes.s}
                    color={colors.textSky}
                  >
                    Advanced Filter
                  </Text>
                  <div
                    className="click"
                    onClick={() => {
                      handelOpenFillter(!isShowFillter);
                      roadmapModel.filterModel.resetFilter();
                      if (isOpen === 'Assessment') {
                        roadmapModel.getRoadmapList(
                          authContext.accessToken,
                          1,
                          roadmapModel.searchRoadmapText,
                        );
                      } else {
                        requestModel.getRequestList(
                          authContext.accessToken,
                          1,
                          roadmapModel.searchRoadmapText,
                        );
                      }
                    }}
                  >
                    <Image
                      src="../../static/images/x-close@3x.png"
                      style={{
                        width: 18,
                        height: 18,
                        padding: 0,
                        marginTop: -8,
                        position: 'relative',
                      }}
                    />
                  </div>
                </Div>

                <Grid.Row>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Year
                    </Text>
                    <YearDropdown
                      placeholder="Select Year"
                      handleOnChange={(year) => {
                        if (isOpen === 'Assessment') {
                          roadmapModel.filterModel.setField(
                            'selectedYear',
                            year,
                          );
                        } else {
                          requestModel.filterModel.setField(
                            'selectedYear',
                            year,
                          );
                        }
                      }}
                      value={
                        isOpen === 'Assessment'
                          ? roadmapModel.filterModel.selectedYear
                          : requestModel.filterModel.selectedYear
                      }
                      model={[roadmapModel, requestModel]}
                    />
                  </Grid.Column>
                  {isOpen === 'Assessment' && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Process (Level 3)
                      </Text>
                      <ProcessLvl3Dropdown
                        handleOnChange={(lvl3) => {
                          console.log(lvl3);
                          roadmapModel.filterModel.setField(
                            'selectedLvl3',
                            lvl3,
                          );

                          roadmapModel.filterModel.setField('selectedLvl4', '');
                        }}
                        value={roadmapModel.filterModel.selectedLvl3}
                        defaultValue="Select Process (Level 3)"
                        placeholder="Select Process (Level 3)"
                      />
                    </Grid.Column>
                  )}

                  {isOpen === 'Assessment' && (
                    <Grid.Column
                      tablet={8}
                      computer={4}
                      mobile={16}
                      style={{ paddingLeft: 0, marginTop: 6 }}
                    >
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                        style={{ paddingBottom: 8 }}
                      >
                        Sub-Process (Level 4)
                      </Text>
                      <ProcessLvl4Dropdown
                        handleOnChange={(lvl4) => {
                          roadmapModel.filterModel.setField(
                            'selectedLvl4',
                            lvl4,
                          );
                        }}
                        value={roadmapModel.filterModel.selectedLvl4}
                        disabled={!roadmapModel.filterModel.selectedLvl3}
                        selectedLVl3={roadmapModel.filterModel.selectedLvl3}
                        defaultValue="Select Process (Level 4)"
                        placeholder="Select Process (Level 4)"
                      />
                    </Grid.Column>
                  )}
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Roadmap
                    </Text>
                    <RoadmapTypeDropdown
                      handleOnChange={(type) => {
                        if (isOpen === 'Assessment') {
                          roadmapModel.filterModel.setField(
                            'selectedRoadmapType',
                            type,
                          );
                        } else {
                          requestModel.filterModel.setField(
                            'selectedRoadmapType',
                            type,
                          );
                        }
                      }}
                      value={
                        isOpen === 'Assessment'
                          ? roadmapModel.filterModel.selectedRoadmapType
                          : requestModel.filterModel.selectedRoadmapType
                      }
                      defaultValue="Select Roadmap Type"
                      placeholder="Select Roadmap Type"
                    />
                  </Grid.Column>
                  {authContext.roles.isAdmin && (
                    <>
                      {isOpen === 'Assessment' && (
                        <Grid.Column
                          tablet={8}
                          computer={4}
                          mobile={16}
                          style={{ paddingLeft: 0, marginTop: 6 }}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                            style={{ paddingBottom: 8 }}
                          >
                            BU
                          </Text>
                          <BUDropdown
                            handleOnChange={(bu) => {
                              roadmapModel.filterModel.setField(
                                'selectedBu',
                                bu,
                              );
                            }}
                            value={roadmapModel.filterModel.selectedBu}
                            disabled={roadmapModel.filterModel.isBuDisabled}
                            selectedYear={roadmapModel.filterModel.selectedYear}
                            defaultValue="Select BU"
                            placeholder="Select BU"
                          />
                        </Grid.Column>
                      )}

                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Department
                        </Text>
                        <DepartmentDropdown
                          handleOnChange={(department) => {
                            if (isOpen === 'Assessment') {
                              roadmapModel.filterModel.setField(
                                'selectedDepartment',
                                department,
                              );
                            } else {
                              requestModel.filterModel.setField(
                                'selectedDepartment',
                                department,
                              );
                            }
                          }}
                          value={
                            isOpen === 'Assessment'
                              ? roadmapModel.filterModel.selectedDepartment
                                  .value
                              : requestModel.filterModel.selectedDepartment
                                  .value
                          }
                          disabled={
                            roadmapModel.filterModel.isDepartmentDisabled
                          }
                          selectedBu={roadmapModel.filterModel.selectedBu}
                          selectedYear={roadmapModel.filterModel.selectedYear}
                          defaultValue="Select Department"
                          placeholder="Select Department"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Division
                        </Text>
                        <DivisionDropdown
                          handleOnChange={(divisionNo) => {
                            if (roadmapModel.filterModel.resetBuMistake) {
                              roadmapModel.filterModel.setField(
                                'selectedBu',
                                '',
                              );
                            }
                            if (isOpen === 'Assessment') {
                              roadmapModel.filterModel.setField(
                                'selectedDivision',
                                divisionNo,
                              );
                            } else {
                              requestModel.filterModel.setField(
                                'selectedDivision',
                                divisionNo,
                              );
                            }
                          }}
                          value={
                            roadmapModel.filterModel.selectedDivision ||
                            requestModel.filterModel.selectedDivision
                          }
                          selectedDepartmentId={
                            roadmapModel.filterModel.selectedDepartment.value
                          }
                          selectedYear={roadmapModel.filterModel.selectedYear}
                          selectedDepartmentNo={roadmapModel.filterModel.selectedDepartment.no}
                          defaultValue="Select Division"
                          placeholder="Select Division"
                        />
                      </Grid.Column>
                      <Grid.Column
                        tablet={8}
                        computer={4}
                        mobile={16}
                        style={{ paddingLeft: 0, marginTop: 6 }}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Shift
                        </Text>
                        <ShiftDropdown
                          handleOnChange={(shiftNo) => {
                            if (roadmapModel.filterModel.resetBuMistake) {
                              roadmapModel.filterModel.setField(
                                'selectedBu',
                                '',
                              );
                            }
                            if (isOpen === 'Assessment') {
                              roadmapModel.filterModel.setField(
                                'selectedShift',
                                shiftNo,
                              );
                            } else {
                              requestModel.filterModel.setField(
                                'selectedShift',
                                shiftNo,
                              );
                            }
                          }}
                          value={
                            roadmapModel.filterModel.selectedShift ||
                            requestModel.filterModel.selectedShift
                          }
                          selectedDepartmentId={
                            roadmapModel.filterModel.selectedDepartment.value
                          }
                          selectedYear={roadmapModel.filterModel.selectedYear}
                          selectedDepartmentNo={roadmapModel.filterModel.selectedDepartment.no}
                          defaultValue="Select Shift"
                          placeholder="Select Shift"
                        />
                      </Grid.Column>
                      {isOpen === 'Assessment' && (
                        <Grid.Column
                          tablet={8}
                          computer={4}
                          mobile={16}
                          style={{ paddingLeft: 0, marginTop: 6 }}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                            style={{ paddingBottom: 8 }}
                          >
                            Status
                          </Text>
                          <StatusDropdown
                            handleOnChange={(status) => {
                              roadmapModel.filterModel.setField(
                                'selectedStatus',
                                status,
                              );
                            }}
                            tab={roadmapModel.filterModel.selectedTab}
                            type="ROADMAP"
                            value={roadmapModel.filterModel.selectedStatus}
                            defaultValue="Select Status"
                            placeholder="Select Status"
                          />
                        </Grid.Column>
                      )}
                      <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process Tag (AND)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={roadmapModel.filterModel.selectedProcessAnd}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
                    />
                  </Grid.Column>
                  <Grid.Column
                    tablet={8}
                    computer={4}
                    mobile={16}
                    style={{ paddingLeft: 0, marginTop: 6 }}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                      style={{ paddingBottom: 8 }}
                    >
                      Process Tag (OR)
                    </Text>
                    <DropdownSelect
                      placeholder="Select Tag"
                      options={tagsList}
                      value={roadmapModel.filterModel.selectedProcessOr}
                      isSearchable
                      isMulti
                      handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
                    />
                  </Grid.Column>
                      {isOpen !== 'Assessment' && (
                        <Grid.Column
                          tablet={8}
                          computer={4}
                          mobile={16}
                          style={{ paddingLeft: 0, marginTop: 6 }}
                        >
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                            style={{ paddingBottom: 8 }}
                          >
                            Status Add Request
                          </Text>
                          <DropdownAll
                            options={statusAdd}
                            handleOnChange={(status) => {
                              requestModel.filterModel.setField(
                                'selectedRead',
                                status,
                              );
                            }}
                            value={requestModel.filterModel.selectedRead}
                            defaultValue="Select Status Add"
                            placeholder="Select Status Add"
                          />
                        </Grid.Column>
                      )}
                    </>
                  )}
                </Grid.Row>
                <div style={{ padding: 0, display: 'flex' }}>
                  <ButtonAll
                    width={96}
                    textSize={sizes.xs}
                    color="#1b1464"
                    text="Filter"
                    onClick={() => {
                      if (isOpen === 'Assessment') {
                        roadmapModel.getRoadmapList(
                          authContext.accessToken,
                          1,
                          roadmapModel.searchRoadmapText,
                          roadmapModel.filterModel.selectedLvl3,
                          roadmapModel.filterModel.selectedLvl4,
                          roadmapModel.filterModel.selectedBu,
                          roadmapModel.filterModel.selectedRoadmapType,
                          roadmapModel.filterModel.selectedDepartment.no,
                          roadmapModel.filterModel.selectedDivision,
                          roadmapModel.filterModel.selectedShift,
                          roadmapModel.filterModel.selectedStatus,
                          roadmapModel.filterModel.selectedYear,
                          roadmapModel.filterModel.selectedProcessAnd,
                          roadmapModel.filterModel.selectedProcessOr
                        );
                      } else {
                        requestModel.getRequestList(
                          authContext.accessToken,
                          1,
                          requestModel.filterModel.searchText,
                          requestModel.filterModel.selectedRoadmapType,
                          requestModel.filterModel.selectedDivision,
                          roadmapModel.filterModel.selectedShift,
                          requestModel.filterModel.selectedYear,
                          requestModel.filterModel.selectedRead.value,
                        );
                      }
                    }}
                  />
                  <ButtonBorder
                    borderColor={colors.primary}
                    textColor={colors.primary}
                    text="Clear"
                    handelOnClick={() => {
                      if (isShowFillter) {
                        if (isOpen === 'Assessment') {
                          roadmapModel.filterModel.resetFilter();
                          // roadmapModel.getRoadmapList(
                          //   authContext.accessToken,
                          //   1,
                          // );
                          getRoadmapList(defaultYear);
                        } else {
                          requestModel.filterModel.resetRequest();
                          // requestModel.getRequestList(
                          //   authContext.accessToken,
                          //   1,
                          // );
                          getRequestList(defaultYear);
                        }
                      }
                    }}
                  />
                </div>
              </Grid>
            </CardFillter>
          )}

          {(roadmapModel.list && roadmapModel.list.length !== 0) ||
          requestModel.requests.length !== 0 ? (
            <Grid.Row style={{ padding: 0 }}>
              {isOpen === 'Assessment'
                ? renderTabAssessment(roadmapModel)
                : renderTabRequests()}
            </Grid.Row>
          ) : (
            renderEmptyCard()
          )}
          {renderBottombar()}
        </Grid>
        {active && (
          <CreateRoadmapModal
            active={active}
            onClose={() => handleCloseCreateRoadmapModal(false)}
            showModal={() => handleFailedCreateRoadmapModal(false)}
          />
        )}
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
