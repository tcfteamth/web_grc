import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import {
  Image,
  Grid,
  Loader,
  Dimmer,
  Select,
  GridRow,
  Pagination,
} from "semantic-ui-react";
import Link from "next/link";
import {
  BottomBar,
  ButtonAll,
  Text,
  DropdownGroup,
  InputDropdown,
  InputAll,
  ButtonBorder,
} from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListReport } from "../../utils/static";
import styled from "styled-components";
import _ from "lodash";
import { DateRange, Calendar } from "react-date-range";

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50px;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  padding: 10px 24px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #d9d9d6;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : "8.3")}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 1px 0px;
  height: 10%;
  border-top: ${(props) => (props.line === 0 ? "none" : `1px solid #d9d9d6`)};
  border-bottom: none;
`;

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.group ? `space-around` : `start`)};
`;

const CardTab = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: 0px !important;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: column;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? "center" : "flex-start")};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Line = styled.div`
  border: 1px solid #000;
  width: 8px;
  margin-top: 20px;
`;
const TableScroll = styled.div`
  height: 400px;
  overflow-x: hidden;
  overflow-y: auto;
`;

const headers = [
  { key: "No.", render: "No.", width: 5 },
  { key: "Process Name", render: "Process Name", width: 30 },
  { key: "Division", render: "Division", width: 10 },
  { key: "Status", render: "Status", width: 10 },
  { key: "Progress", render: "Progress", width: 20 },
  { key: "Problem", render: "Problem", width: 20 },
];

const sortBy = [
  { text: "Latest", value: 0 },
  { text: "Agent", value: 1 },
  { text: "Team", value: 2 },
  { text: "Walk in", value: 3 },
];

const index = (props) => {
  const [date, setDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);

  const onChangeCloseDate = (date) => {
    console.log(date);
    setStartDate(moment(date[0].startDate).format("DD/MM/YYYY"));
    setEndDate(moment(date[0].endDate).format("DD/MM/YYYY"));
  };
  useEffect(() => {
    onChangeCloseDate(state);
  }, [state]);

  return (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16}>
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          Back
        </Text>
      </Div>
      <div>
        <Card top={32}>
          <Div between>
            <Text
              fontWeight={"bold"}
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              CSA Dashboard
            </Text>
            <ButtonBorder
              text="Export"
              textUpper
              width={150}
              textColor={colors.greenExport}
              fontSize={sizes.s}
              borderColor={colors.greenExport}
              icon={"../../static/images/excel@3x.png"}
            />
          </Div>
          <Div between top={28}>
            <Div>
              <Div col width={20} right={16}>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Year
                </Text>
                <Div items={"flex-Start"}>
                  <InputDropdown
                    minWidth={60}
                    placeholder="เลือกผู้รับผิดชอบ"
                  />
                </Div>
              </Div>
              <Div col width={20} right={16}>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Department
                </Text>
                <Div items={"flex-Start"}>
                  <InputDropdown
                    minWidth={60}
                    placeholder="เลือกผู้รับผิดชอบ"
                  />
                </Div>
              </Div>
              <Div col width={20}>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Divisions
                </Text>
                <Div items={"flex-Start"}>
                  <InputDropdown
                    minWidth={60}
                    placeholder="เลือกผู้รับผิดชอบ"
                  />
                </Div>
              </Div>
            </Div>
            <Div col width={20}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Sort by
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
          </Div>
        </Card>

        <Grid style={{ marginTop: 12 }}>
          <CardTab>
            <TableHeader span={12}>
              {headers.map(({ render, key, width }, index) => (
                <Cell key={key} width={width}>
                  <Div center>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.textGray}
                    >
                      {render}
                    </Text>
                    {index != 0 && (
                      <Image
                        src="../../static/images/change@3x.png"
                        style={{ marginleft: 16, width: 18, height: 18 }}
                      />
                    )}
                  </Div>
                </Cell>
              ))}
            </TableHeader>
            <TableScroll>
              <div>
                {ListReport &&
                  ListReport.map((item, index) => (
                    <TableBody span={12} key={index} line={index}>
                      <Cell width={5}>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.id}
                        </Text>
                      </Cell>
                      <Cell width={30}>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.text}
                        </Text>
                      </Cell>
                      <Cell>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.division}
                        </Text>
                      </Cell>
                      <Cell>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.status}
                        </Text>
                      </Cell>
                      <Cell width={20}>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.progress}
                        </Text>
                      </Cell>
                      <Cell width={20}>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.problem}
                        </Text>
                      </Cell>
                    </TableBody>
                  ))}
              </div>
            </TableScroll>
          </CardTab>
        </Grid>
      </div>
    </div>
  );
};

export default withLayout(observer(index));
