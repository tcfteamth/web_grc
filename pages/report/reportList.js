import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import {
  Image,
  Grid,
  Pagination,
  GridRow,
  Loader,
  Dimmer,
} from "semantic-ui-react";
import Link from "next/link";
import {
  BottomBar,
  ButtonAll,
  Text,
  DropdownGroup,
  ButtonBorder,
} from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListAssess, dataListFollow } from "../../utils/static";
import styled from "styled-components";

import _ from "lodash";

const Card = styled.div`
    display: inline-block;
    margin-top: 32px;
    min-height: 200px;
    width: 100%;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    padding: 24px !important;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 1440px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  align-items: center;
  margin-top: ${(props) => props.top || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const CardItem = styled.div`
  width: 100%;
  min-height: 96px;
  background-color: ${(props) => props.bgColor || colors.primary};
  border-radius: 6px 6px 0px 6px !important;
  margin: 4px;
  display: flex;
  padding: 20px 56px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const index = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {}, []);

  return (
    <div style={{ marginBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16} between>
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          CSA Result
        </Text>
      </Div>
      <Card>
        <Text color={colors.textPurple} fontWeight={"med"} fontSize={sizes.xl}>
          Report
        </Text>
        <Div between top={16}>
          <Link href="/report/reportDashboard">
            <CardItem
              between
              bgColor={colors.blueBgLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Dashboard@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text
                color={colors.textSky}
                fontWeight={"bold"}
                fontSize={sizes.l}
              >
                Dashboard
              </Text>
            </CardItem>
          </Link>
          <Link href="/report/reportCSAReport">
            <CardItem
              between
              bgColor={colors.pinkLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Report@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text color={colors.pink} fontWeight={"bold"} fontSize={sizes.l}>
                Report
              </Text>
            </CardItem>
          </Link>
          <Link href="/report/reportExport">
            <CardItem
              between
              bgColor={colors.greenMintLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Export-Data@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text
                color={colors.greenMint}
                fontWeight={"bold"}
                fontSize={sizes.l}
              >
                Export Data
              </Text>
            </CardItem>
          </Link>
        </Div>
      </Card>

      <Card>
        <Text color={colors.textPurple} fontWeight={"med"} fontSize={sizes.xl}>
          Follow Up Action
        </Text>
        <Div between top={16}>
          <Link href="/report/reportFollowUpDashboard">
            <CardItem
              between
              bgColor={colors.blueBgLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Dashboard@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text
                color={colors.textSky}
                fontWeight={"bold"}
                fontSize={sizes.l}
              >
                Dashboard
              </Text>
            </CardItem>
          </Link>
          <Link href="/report/reportFollowUpReport">
            <CardItem
              between
              bgColor={colors.pinkLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Report@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text color={colors.pink} fontWeight={"bold"} fontSize={sizes.l}>
                Report
              </Text>
            </CardItem>
          </Link>
          <Link href="/report/reportExport">
            <CardItem
              between
              bgColor={colors.greenMintLight}
              className="upper click"
            >
              <Image
                src="../../static/images/Export-Data@3x.png"
                style={{ width: 56, height: 56 }}
              />
              <Text
                color={colors.greenMint}
                fontWeight={"bold"}
                fontSize={sizes.l}
              >
                Export Data
              </Text>
            </CardItem>
          </Link>
        </Div>
      </Card>
    </div>
  );
};

export default withLayout(observer(index));
