import React, { useState, useEffect } from 'react';
import { withLayout } from '../../hoc';
import { Image, Grid, Loader, Dimmer } from 'semantic-ui-react';
import Link from 'next/link';
import {
  ChartPie,
  ChartDonut,
  ChartBubble,
  ProgressBarStatus,
  ChartHorizontalBar,
  BottomBar,
  ButtonAll,
  Text,
  DropdownGroup,
  InputDropdown,
  InputAll,
  ButtonBorder,
} from '../../components/element';
import { dataCardonut, dataCardStatus, dataCardPie } from '../../utils/static';
import { observer } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import _ from 'lodash';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  align-items: center;
  justify-content: center;
`;
const TableBody = TableHeader.extend`
  padding: 0px !important;
  margin: 0px 40px;
  height: 50px !important;
  border-top: ${(props) => props.borderTop && `1px solid ${colors.grayLight}`};
  justify-content: ${(props) => (props.center ? `center` : 'space-between')};
`;
const Cell = styled.div`
  display: flex;
  min-height: 30px !important;
  width: ${(props) => props.width || 10}%;
  background-color: ${(props) => props.color || colors.backgroundPrimary};
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.padding || 0}px;
  border-right: ${(props) => props.border && `1px solid #FFF`};
`;
const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? 'center'
        : props.right
        ? 'flex-end'
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: ${(props) => (props.border ? `0px 24px` : ' 24px')};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: row;
`;
const CardTabBody = CardTabHeader.extend`
  border-top: 1px solid #d9d9d6;
  border-radius: ${(props) =>
    props.borderLeft ? `0px 0px 0px 6px` : `0px`} !important;
  min-height: 50px;
  padding: 0px 24px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  height: ${(props) => props.height && props.height};
  width: ${(props) => props.width || 100}% !important;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Line = styled.div`
  border: 1px solid #d9d9d6;
  height: 100%;
  width: 1px;
`;
const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
`;

const Oval = styled.div`
  width: ${(props) => props.width || 12}px;
  height: ${(props) => props.height || 12}px;
  border-radius: 50%;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
`;
const LineGraph = styled.div`
  height: 18px;
  padding: 0px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${(props) => props.width && props.width}%;
  background-color: ${(props) => props.bgcolor || colors.primaryBackground};
`;

const headers = [
  { key: '1', render: '', width: 40 },
  { key: '2', render: '31 Jan', width: 30 },
  { key: '3', render: '30 Apr', width: 30 },
];
const dataCSA = [
  {
    id: 1,
    text: 'Due Date 31 Dec ',
    total: 18,
    good: 16,
  },
  {
    id: 2,
    text: 'Due Date 31 Mar',
    total: 7,
    good: 30,
  },
  {
    id: 3,
    text: 'Due Date 30 Jun',
    total: 8,
    good: 16,
  },
  {
    id: 4,
    text: 'Total Corrective',
    total: 8,
    good: 80,
  },
];

const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'Agent', value: 1 },
  { text: 'Team', value: 2 },
  { text: 'Walk in', value: 3 },
];

const dataCSAList = {
  data: [30, 30, 40, 10, 10],
  color: ['#1b1464', '#002247', '#013a7a', '#0057b8', '#00aeef'],
};
const dataPieList = {
  dataPie: [30, 30, 40],
  color: [colors.green, colors.yellow, colors.red],
};

const index = (props) => {
  const [date, setDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: 'selection',
    },
  ]);

  const onChangeCloseDate = (date) => {
    console.log(date);
    setStartDate(moment(date[0].startDate).format('DD/MM/YYYY'));
    setEndDate(moment(date[0].endDate).format('DD/MM/YYYY'));

    // console.log(date);
    // console.log(
    //   moment(date)
    //     .locale('th')
    //     .format('l'),
    // );
    // popperDateHide();
  };
  let datasummary = [];
  let total = 0;
  const handleSum = () => {
    console.log('dataCSA', dataCSA);
    dataCSA.map((item, index) => {
      console.log(item.total, index);
      datasummary = { total: total + item.total[index] };
    });
    let rObj = 0;
    let reformattedArray = dataCSA.map((obj, i) => {
      rObj = rObj + obj.total[i];
      return rObj;
    });
    console.log('rObj', rObj);
    console.log('datasummary', datasummary);
  };
  useEffect(() => {
    onChangeCloseDate(state);
  }, [state]);

  useEffect(() => {
    handleSum();
  }, []);

  return (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16}>
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text
          color={colors.primaryBlack}
          fontWeight={'bold'}
          fontSize={sizes.xl}
        >
          Back
        </Text>
      </Div>
      <div>
        <Card top={32}>
          <Div between>
            <Text
              fontWeight={'bold'}
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              Follow Up Action Dashboard
            </Text>
            <ButtonBorder
              text="Export"
              width={150}
              textColor={colors.redExport}
              fontSize={sizes.s}
              borderColor={colors.redExport}
              icon={'../../static/images/pdf@3x.png'}
            />
          </Div>
          <Div top={28}>
            <Div col width={16} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Year
              </Text>
              <Div items={'flex-Start'}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
            <Div col width={20} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Department
              </Text>
              <Div items={'flex-Start'}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
            <Div col width={20}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Divisions
              </Text>
              <Div items={'flex-Start'}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
          </Div>
        </Card>

        <Grid style={{ marginTop: 32 }}>
          <CardTabHeader border>
            <Div center between padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                KPI = 86% (6/7)
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
          </CardTabHeader>
          <CardTabBody between>
            {/* Card Left  */}
            <Div center col width={30} border padding={24}>
              <TableHeader span={12}>
                {headers.map(({ render, key, color, width }) => (
                  <Cell
                    border
                    key={key}
                    color={
                      key === '1' ? colors.textGrayLight : colors.primaryBlack
                    }
                    width={width}
                  >
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={
                        render === 'Dept'
                          ? colors.primaryBlack
                          : colors.primaryBackground
                      }
                    >
                      {render}
                    </Text>
                  </Cell>
                ))}
              </TableHeader>
              {dataCSA &&
                dataCSA.map((item, index) => (
                  <TableBody span={12} key={index} borderTop>
                    <Cell width={40}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === 'Total Corrective' && 'bold'}
                        color={colors.primaryBlack}
                      >
                        {item.text}
                      </Text>
                    </Cell>
                    <Cell width={30} borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === 'Total Corrective' && 'bold'}
                        color={colors.primaryBlack}
                      >
                        {item.good > 0 ? item.good : '-'}
                      </Text>
                    </Cell>
                    <Cell width={30} borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === 'Total Corrective' && 'bold'}
                        color={colors.primaryBlack}
                      >
                        {item.fair > 0 ? item.fair : '-'}
                      </Text>
                    </Cell>
                  </TableBody>
                ))}
            </Div>

            {/* Card Right  */}
            <Div height={328} width={70} padding={24}>
              <ChartBubble />
            </Div>
          </CardTabBody>
          <CardTabBody around>
            <Div center col width={23} padding={15}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                15 Apr (YTD)
              </Text>
              <ChartPie
                data={dataPieList.dataPie}
                backgroundColor={dataPieList.color}
              />
            </Div>
            <Div center col width={23} padding={15}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                15 Apr (YTD)
              </Text>
              <ChartPie
                data={dataPieList.dataPie}
                backgroundColor={dataPieList.color}
              />
            </Div>
            <Div center col width={23} padding={15}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                15 Apr (YTD)
              </Text>
              <ChartPie
                data={dataPieList.dataPie}
                backgroundColor={dataPieList.color}
              />
            </Div>
            <Div center col width={23} padding={15}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                15 Apr (YTD)
              </Text>
              <ChartPie
                data={dataPieList.dataPie}
                backgroundColor={dataPieList.color}
              />
            </Div>
          </CardTabBody>
          <CardTabBody borderLeft>
            <Div center mid>
              {dataCardStatus.map((item) => (
                <SectionLine
                  bottom={1}
                  style={{ paddingRight: 16 }}
                  className="upper"
                >
                  <Oval bgcolor={item.color} />
                  <Text
                    style={{ paddingLeft: 16 }}
                    fontSize={sizes.xxs}
                    fontWeight={'bold'}
                    color={colors.primaryBlack}
                  >
                    {item.text}
                  </Text>
                </SectionLine>
              ))}
            </Div>
          </CardTabBody>
        </Grid>
        <Grid style={{ marginTop: 24 }}>
          <CardTabHeader border>
            <Div center between width={50} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                YTD-Division
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
            <Div center between width={50} padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                YTD-Objective
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
          </CardTabHeader>
          <CardTabBody between>
            <Div center width={50} border padding={5}>
              <ChartHorizontalBar />
            </Div>
            <Div center width={50} padding={5}>
              <ChartHorizontalBar />
            </Div>
          </CardTabBody>
          <CardTabBody borderLeft>
            <Div center mid>
              {dataCardStatus.map((item) => (
                <SectionLine
                  bottom={1}
                  style={{ paddingRight: 16 }}
                  className="upper"
                >
                  <Oval bgcolor={item.color} />
                  <Text
                    style={{ paddingLeft: 16 }}
                    fontSize={sizes.xxs}
                    fontWeight={'bold'}
                    color={colors.primaryBlack}
                  >
                    {item.text}
                  </Text>
                </SectionLine>
              ))}
            </Div>
          </CardTabBody>
        </Grid>
      </div>
    </div>
  );
};

export default withLayout(observer(index));
