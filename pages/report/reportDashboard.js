import React, { useState, useEffect } from 'react';
import { withLayout } from '../../hoc';
import { Image, Grid, Loader, Dimmer, Table, Popup } from 'semantic-ui-react';
import Link from 'next/link';
import {
  ChartPie,
  ChartDonut,
  ProgressBarStatus,
  BottomBar,
  ButtonAll,
  Text,
  DropdownGroup,
  InputDropdown,
  InputAll,
  ButtonBorder,
  ProgressBarStatusTran,
  DepartmentDropdown,
  DivisionDropdown,
  YearListDropdown,
  CardEmpty,
  BUDropdown,
  ManageDepartmentDropdown,
  ManageBuDropdown,
  PublishedModal,
  WaitDmAcceptModal,
  DropdownSelect
} from '../../components/element';
import { dataCardonut, dataCardPie } from '../../utils/static';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import _ from 'lodash';
import {
  AllRatingDashboardModel,
  AllWaitDmAcceptModel,
  ManageFilterListModel,
  OverAllRatingDashboardModel,
  ProcessingDashboardModel,
} from '../../model/DashboardModel';
import { initAuthStore } from '../../contexts';
import FilterModel from '../../model/FilterModel';
import { manageBUDropdownStore, taglistDropdownStore } from '../../model';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  align-items: center;
  justify-content: center;
`;
const TableBody = TableHeader.extend`
  padding: 0px !important;
  margin: 0px 40px;
  height: 50px !important;
  border-top: ${(props) => props.borderTop && `1px solid ${colors.grayLight}`};
  justify-content: ${(props) => (props.center ? `center` : 'space-between')};
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  background-color: ${(props) => props.color || colors.backgroundPrimary};
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.padding || 0}px;
  border-right: ${(props) => props.border && `1px solid #FFF`};
`;
const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? 'center'
        : props.right
        ? 'flex-end'
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: ${(props) => (props.border ? `0px 24px` : ' 24px')};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: row;
`;
const CardTabBody = CardTabHeader.extend`
  border-top: 1px solid #d9d9d6;
  border-radius: 0px 0px 0px 6px !important;
  min-height: 350px;
  padding: 0px 24px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const HoverBox = styled.div`
  &:hover {
  }
`;

const Line = styled.div`
  border: 1px solid #d9d9d6;
  height: 100%;
  width: 1px;
`;
const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
`;

const Oval = styled.div`
  width: ${(props) => props.width || 12}px;
  height: ${(props) => props.height || 12}px;
  border-radius: 50%;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  color: ${(props) => props.color || colors.primaryBlack};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${sizes.xxs}px;
`;
const LineGraph = styled.div`
  height: 18px;
  padding: 0px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${(props) => props.width && props.width}%;
  background-color: ${(props) => props.bgcolor || colors.primaryBackground};
`;

const NoDataCard = styled.div`
  width: 1150px;
  height: 268px;
  border-radius: 6px;
  box-shadow: 0 2px 10px 0 var(--black-4);
  background-color: #fefeff;
`;

const headersDonut2 = [
  { key: 'Division', render: 'Division', color: colors.grayLight, width: 15 },
  { key: 'Good', render: 'Good', color: '#00b140', width: 15 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 15 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 15 },
  { key: 'Total', render: 'Total', color: colors.primaryBlack, width: 15 },
  { key: 'Graph', render: 'Graph', color: colors.grayLight, width: 25 },
];

const headersDonut3 = [
  { key: 'Division', render: 'Division', color: colors.grayLight, width: 10 },
  { key: 'Good', render: 'Good', color: '#00b140', width: 10 },
  { key: 'Fair', render: 'Fair', color: '#ffc72c', width: 10 },
  { key: 'Poor', render: 'Poor', color: '#e4002b', width: 10 },
  { key: 'Total', render: 'Total', color: colors.primaryBlack, width: 10 },
  { key: 'Graph', render: 'Graph', color: colors.grayLight, width: 30 },
  {
    key: 'Enhancement',
    render: 'Enhancement',
    color: colors.textPurple,
    width: 20,
  },
];

const dataCSA = [
  {
    id: 1,
    text: 'S-RC-IC',
    total: 18,
    good: 16,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 2,
    text: 'S-RC-GRC',
    total: 7,
    good: 30,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 3,
    text: 'S-RC-RM',
    total: 8,
    good: 16,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 4,
    text: 'S-RC-SG',
    total: 8,
    good: 80,
    fair: 10,
    poor: 2,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 4,
    text: 'S-RC',
    total: 8,
    good: 16,
    fair: 0,
    poor: 1,
    Graph: 0,
    EnhancemEnt: 0,
  },
];

const sortBy = [
  { text: 'Latest', value: 0 },
  { text: 'Agent', value: 1 },
  { text: 'Team', value: 2 },
  { text: 'Walk in', value: 3 },
];

const dataCSAList = {
  data: [30, 30, 40, 10, 10],
  color: ['#8c0d83', '#7144a8', '#467bc4', '#20b7e2', '#00ebfa'],
};
const dataPieList = {
  dataPie: [30, 30, 40],
  color: [colors.green, colors.yellow, colors.red],
};

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const index = (props) => {
  const [date, setDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: 'selection',
    },
  ]);

  const onChangeCloseDate = (date) => {
    console.log(date);
    setStartDate(moment(date[0].startDate).format('DD/MM/YYYY'));
    setEndDate(moment(date[0].endDate).format('DD/MM/YYYY'));

    // console.log(date);
    // console.log(
    //   moment(date)
    //     .locale('th')
    //     .format('l'),
    // );
    // popperDateHide();
  };

  let datasummary = [];

  let total = 0;

  const handleSum = () => {
    dataCSA.map((item, index) => {
      datasummary = { total: total + item.total[index] };
    });
    let rObj = 0;
    let reformattedArray = dataCSA.map((obj, i) => {
      rObj = rObj + obj.total[i];
      return rObj;
    });
    console.log('rObj', rObj);
    console.log('datasummary', datasummary);
  };

  useEffect(() => {
    onChangeCloseDate(state);
  }, [state]);

  useEffect(() => {
    handleSum();
  }, []);

  const [donut1, setDonut1] = useState(true);
  const [donut2, setDonut2] = useState(true);
  const [donut3, setDonut3] = useState(false);
  const [activeWaitDMModal, setActiveWaitDMModal] = useState(false);
  const authContext = initAuthStore();
  const processingDashboardModel = useLocalStore(
    () => new ProcessingDashboardModel(),
  );
  const overAllRatingDashBoardModel = useLocalStore(
    () => new OverAllRatingDashboardModel(),
  );
  const allRatingDashboardModel = useLocalStore(
    () => new AllRatingDashboardModel(),
  );
  const manageFilterListModel = useLocalStore(
    () => new ManageFilterListModel(),
  );
  const filterModel = useLocalStore(() => new FilterModel());
  const allWaitDmAcceptModel = useLocalStore(() => new AllWaitDmAcceptModel());

  const tagListDropdown = useLocalStore(() => taglistDropdownStore);
  const [tagsList, setTagList] = useState();

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };

  const handleProcessTagSelection = (e,data,field) => {
    let result = filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    filterModel.setField(
      field,
      result,
    );
  }

  const isStaff = () => {
    if (
      !authContext.roles.isAdmin &&
      !authContext.roles.isIaFinding &&
      !authContext.roles.isDM &&
      !authContext.roles.isVP &&
      !authContext.roles.isIcAgent &&
      !authContext.roles.isCompliance
    ) {
      return true;
    }

    return false;
  };

  const getDashboardData = async (
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    processAnd = [],
    processOr = [],
    objectives = []
  ) => {
    try {
      await new Promise([
        processingDashboardModel.getProcessingStatus(
          token,
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
        overAllRatingDashBoardModel.getOverAllRating(
          token,
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
        allRatingDashboardModel.getAllRating(
          token,
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
        allWaitDmAcceptModel.getAllWaitDmAccept(
          year,
          bu,
          departmentNo,
          divisionNo,
          processAnd,
          processOr,
          objectives
        ),
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  const filterDashboard = () => {
    getDashboardData(
      authContext.accessToken,
      filterModel.selectedYear,
      filterModel.selectedBu,
      filterModel.selectedDepartment.no,
      filterModel.selectedDivision,
      filterModel.selectedProcessAnd,
      filterModel.selectedProcessOr,
      filterModel.selectedObjectives
    );
  };

  const clearFilterDashboard = () => {
    if (authContext.roles.isAdmin || authContext.roles.isIaFinding || authContext.roles.isCompliance) {
      filterModel.resetDashboardAdmin();
    } else if (authContext.roles.isVP || authContext.roles.isIcAgent) {
      filterModel.resetDashboardVP();
    } else {
      filterModel.resetDashboardDMAndStaff();
    }
    setDonut2(false);
    setDonut3(false);
    getDashboardData(
      authContext.accessToken,
      filterModel.selectedYear,
      filterModel.selectedBu,
      filterModel.selectedDepartment.no,
      filterModel.selectedDivision,
      filterModel.selectedProcessAnd,
      filterModel.selectedProcessOr,
      filterModel.selectedObjectives
    );
  };

  const getManageFilter = async () => {
    try {
      await manageFilterListModel.getManageFilterList(authContext.accessToken);
    } catch (e) {
      console.log(e);
    }
  };

  // get managefilter แยกกับ useeffect ตัวอื่น
  // เพราะต้องการไว้ใช้กับกลุ่ม admin/VP และ dm/staff ด้วย
  useEffect(() => {
    getManageFilter();
  }, []);

  useEffect(() => {
    (async () => {
      try {
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    if (authContext.roles.isDM || isStaff()) {
      if (filterModel.selectedDepartment.no) {
        // set current year to default filter
        const currentYear = new Date().getFullYear();
        filterModel.setField('selectedYear', currentYear);

        getDashboardData(
          authContext.accessToken,
          filterModel.selectedYear,
          filterModel.selectedBu,
          filterModel.selectedDepartment.no,
          filterModel.selectedDivision,
          filterModel.selectedProcessAnd,
          filterModel.selectedProcessOr,
          filterModel.selectedObjectives
        );
      }
    }
  }, [filterModel.selectedDepartment.no]);

  useEffect(() => {
    if (authContext.roles.isAdmin || authContext.roles.isIaFinding || authContext.roles.isCompliance ) {
      // set current year to default filter
      const currentYear = new Date().getFullYear();
      filterModel.setField('selectedYear', currentYear);

      getDashboardData(
        authContext.accessToken,
        filterModel.selectedYear,
        filterModel.selectedBu,
        filterModel.selectedDepartment.no,
        filterModel.selectedDivision,
        filterModel.selectedProcessAnd,
        filterModel.selectedProcessOr,
        filterModel.selectedObjectives
      );
    }
  }, []);

  useEffect(() => {
    if (authContext.roles.isVP || authContext.roles.isEVP) {
      if (filterModel.selectedBu) {
        // set current year to default filter
        const currentYear = new Date().getFullYear();
        filterModel.setField('selectedYear', currentYear);

        getDashboardData(
          authContext.accessToken,
          filterModel.selectedYear,
          filterModel.selectedBu,
          filterModel.selectedDepartment.no,
          filterModel.selectedDivision,
          filterModel.selectedProcessAnd,
          filterModel.selectedProcessOr,
          filterModel.selectedObjectives
        );
      }
    }
  }, [filterModel.selectedBu]);

  useEffect(() => {
    (async () => {
      if (authContext.roles.isIcAgent) {
        if (filterModel.selectedBu) {
          const bu = await manageBUDropdownStore.getManageFilterBUList(
            authContext.accessToken,
          );

          filterModel.setField('selectedBu', bu[0].value);
          // set current year to default filter
          const currentYear = new Date().getFullYear();
          filterModel.setField('selectedYear', currentYear);

          getDashboardData(
            authContext.accessToken,
            filterModel.selectedYear,
            filterModel.selectedBu,
            filterModel.selectedDepartment.no,
            filterModel.selectedDivision,
            filterModel.selectedProcessAnd,
            filterModel.selectedProcessOr,
            filterModel.selectedObjectives
          );
        }
      }
    })();
  }, [filterModel.selectedBu]);

  return useObserver(() => (
    <div style={{ paddingBottom: 96 }}>
      <div>
        <Card top={32}>
          <Div between>
            <Text
              fontWeight={'bold'}
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              CSA Dashboard
            </Text>
            {/* <ButtonBorder
              text="Export"
              width={150}
              textColor={colors.redExport}
              fontSize={sizes.s}
              borderColor={colors.redExport}
              icon={'../../static/images/pdf@3x.png'}
            /> */}
          </Div>
          <Div top={28}>
            {manageFilterListModel.list.length > 0 &&
              manageFilterListModel.list.map((filter) => {
                if (filter.filterName === 'year') {
                  return (
                    <Div col width={16} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Year
                      </Text>
                      <Div items={'flex-Start'}>
                        <YearListDropdown
                          placeholder="Select Year"
                          handleOnChange={(year) => {
                            filterModel.setField('selectedYear', year);
                          }}
                          value={filterModel.selectedYear}
                          disabled={filter.isDisable}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'BU') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        BU
                      </Text>
                      <Div items={'flex-Start'}>
                        <ManageBuDropdown
                          placeholder="Select BU"
                          handleOnChange={(bu) => {
                            filterModel.setField('selectedBu', bu.value);

                            filterModel.setField('selectedDepartment', '');
                            filterModel.setField('selectedDivision', '');
                          }}
                          value={filterModel.selectedBu}
                          isDefaultValue={filter.isDefaultValue}
                          disabled={
                            filter.isDisable || filterModel.isBuDisabled
                          }
                          filterModel={filterModel}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'Department') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Department
                      </Text>
                      <Div items={'flex-Start'}>
                        <ManageDepartmentDropdown
                          placeholder="Select Department"
                          handleOnChange={(department) => {
                            filterModel.setField(
                              'selectedDepartment',
                              department,
                            );

                            filterModel.setField('selectedDivision', '');
                          }}
                          value={filterModel.selectedDepartment.value}
                          selectedBu={filterModel.selectedBu}
                          isDefaultValue={filter.isDefaultValue}
                          disabled={
                            filter.isDisable || filterModel.isDepartmentDisabled
                          }
                          filterModel={filterModel}
                        />
                      </Div>
                    </Div>
                  );
                }

                if (filter.filterName === 'Division') {
                  return (
                    <Div col width={20} right={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Divisions
                      </Text>
                      <Div items={'flex-Start'}>
                        <DivisionDropdown
                          defaultValue="Select Division"
                          placeholder="Select Division"
                          handleOnChange={(divisionNo) => {
                            filterModel.setField(
                              'selectedDivision',
                              divisionNo,
                            );

                            if (
                              authContext.roles.isAdmin ||
                              authContext.roles.isIaFinding ||
                              authContext.roles.isCompliance
                            ) {
                              filterModel.resetDivisionMistake();
                            }
                          }}
                          value={filterModel.selectedDivision}
                          selectedDepartmentId={
                            filterModel.selectedDepartment.value
                          }
                          disabled={filter.isDisable}
                        />
                      </Div>
                    </Div>
                  );
                }
              })}
            <Div col width={10} style={{ marginTop: '22px' }}>
              <Div items={'flex-Start'}>
                <ButtonAll text="Filter" onClick={() => filterDashboard()} />
                <ButtonBorder
                  borderColor={colors.primary}
                  textColor={colors.primary}
                  text="Clear"
                  handelOnClick={() => clearFilterDashboard()}
                />
              </Div>
            </Div>
          </Div>
          <Div top={28}>
            <Div col width={25} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Process Tag (AND)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Tag"
                  options={tagsList}
                  value={filterModel.selectedProcessAnd}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
                />
              </Div>
            </Div>
            <Div col width={25} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Process Tag (OR)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Tag"
                  options={tagsList}
                  value={filterModel.selectedProcessOr}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
                />
              </Div>
            </Div>
            <Div col width={26} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Objective (AND)
              </Text>
              <Div items={'flex-Start'}>
                <DropdownSelect
                  placeholder="Select Objective"
                  options={objectiveList}
                  value={filterModel.selectedObjectives}
                  isSearchable
                  isMulti
                  handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedObjectives')}
                />
              </Div>
            </Div>
          </Div>
        </Card>

        <Grid style={{ marginTop: 24 }}>
          <CardTabHeader border>
            <Div center between width={50} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                CSA Status
              </Text>

              <ButtonAll
                text="Wait DM Accept"
                onClick={() => setActiveWaitDMModal(true)}
              />
            </Div>
            <Div center width={25} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                CSA Result
              </Text>
              <Popup
                trigger={
                  <Image
                    width={18}
                    style={{ marginLeft: 8 }}
                    src="../../static/images/iconRemark@3x.png"
                  />
                }
                content={<Text>After VP Approved</Text>}
              />
            </Div>
            <Div center width={25} padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                CSA Result
              </Text>
              <Popup
                trigger={
                  <Image
                    width={18}
                    style={{ marginLeft: 8 }}
                    src="../../static/images/iconRemark@3x.png"
                  />
                }
                content={<Text>After VP Approved</Text>}
              />
            </Div>
          </CardTabHeader>

          <CardTabBody between>
            <Div
              center
              width={50}
              border
              padding={24}
              className="click"
              onClick={() => setDonut1(!donut1)}
            >
              {processingDashboardModel.donutGraph &&
              !processingDashboardModel.isAllNoPercent ? (
                <>
                  <Div width={60}>
                    <ChartDonut
                      data={processingDashboardModel.percentDonutGraphList}
                      label={
                        processingDashboardModel.donutGraphNameWithColorsList
                      }
                      backgroundColor={dataCSAList.color}
                    />
                  </Div>
                  <Div col>
                    {processingDashboardModel.donutGraphNameWithColorsList.map(
                      (dg) => (
                        <SectionLine>
                          <Oval bgcolor={dg.color} />
                          <Text
                            style={{ paddingLeft: 8 }}
                            fontSize={sizes.s}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                          >
                            {`${dg.name} -`}
                          </Text>
                          <Text
                            color={colors.primaryBlack}
                          >{`Total ${dg.count}`}</Text>
                        </SectionLine>
                      ),
                    )}
                    <Text
                      color={colors.textlightGray}
                      fontWeight={'med'}
                      fontSize={sizes.s}
                    >
                      NO. OF SUB-PROCESS
                    </Text>
                  </Div>
                </>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    color={colors.textlightGray}
                    fontWeight={'med'}
                    fontSize={sizes.xl}
                  >
                    No Data
                  </Text>
                </div>
              )}
            </Div>
            <Div
              center
              width={25}
              border
              className="click"
              onClick={() => {
                setDonut2(!donut2);
                setDonut3(false);
              }}
            >
              {overAllRatingDashBoardModel.pieGraph &&
              !overAllRatingDashBoardModel.isAllNoPercent ? (
                <Div center col border padding={24}>
                  <Text
                    className="upper"
                    style={{ marginBottom: 24 }}
                    fontSize={sizes.s}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    No. of sub-process
                  </Text>
                  {overAllRatingDashBoardModel.pieGraph && (
                    <Div center col>
                      <Div>
                        <ChartPie
                          data={
                            overAllRatingDashBoardModel.percentOverAllRatingList
                          }
                          backgroundColor={dataPieList.color}
                        />
                      </Div>

                      <Div between>
                        {overAllRatingDashBoardModel.pieGraph.map((pg) => (
                          <SectionLine>
                            <Text color={colors.primaryBlack}>
                              {`${pg.percent.toFixed(2)}%`}
                            </Text>
                          </SectionLine>
                        ))}
                      </Div>

                      <Div between>
                        {overAllRatingDashBoardModel.pieGraph.map((pg) => (
                          <SectionLine>
                            <Text color={colors.primaryBlack}>
                              {`Total ${pg.count}`}
                            </Text>
                          </SectionLine>
                        ))}
                      </Div>

                      <Div between>
                        <SectionLine>
                          <Oval bgcolor={colors.green} />
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                            style={{ paddingLeft: 8 }}
                          >
                            GOOD
                          </Text>
                        </SectionLine>
                        <SectionLine>
                          <Oval bgcolor={colors.yellow} />
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                            style={{ paddingLeft: 8 }}
                          >
                            FAIR
                          </Text>
                        </SectionLine>
                        <SectionLine>
                          <Oval bgcolor={colors.red} />
                          <Text
                            style={{ paddingLeft: 8 }}
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                          >
                            POOR
                          </Text>
                        </SectionLine>
                      </Div>

                      <Div mid>
                        <Text>Click to see more detail</Text>
                      </Div>
                    </Div>
                  )}
                </Div>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    color={colors.textlightGray}
                    fontWeight={'med'}
                    fontSize={sizes.xl}
                  >
                    No Data
                  </Text>
                </div>
              )}
            </Div>
            <Div
              center
              width={25}
              className="click"
              onClick={() => {
                setDonut3(!donut3);
                setDonut2(false);
              }}
            >
              {allRatingDashboardModel.pieGraph &&
              !allRatingDashboardModel.isAllNoPercent ? (
                <Div center col padding={24}>
                  <Text
                    className="upper"
                    style={{ marginBottom: 24 }}
                    fontSize={sizes.s}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    No. of Control
                  </Text>
                  {allRatingDashboardModel.pieGraph && (
                    <Div center col>
                      <Div>
                        <Div>
                          <ChartPie
                            data={
                              allRatingDashboardModel.percentOverAllRatingList
                            }
                            backgroundColor={dataPieList.color}
                          />
                        </Div>
                      </Div>

                      <Div between>
                        {allRatingDashboardModel.pieGraph.map((pg) => (
                          <SectionLine>
                            <Text color={colors.primaryBlack}>
                              {`${pg.percent.toFixed(2)}%`}
                            </Text>
                          </SectionLine>
                        ))}
                      </Div>

                      <Div between>
                        {allRatingDashboardModel.pieGraph.map((pg) => (
                          <SectionLine>
                            <Text color={colors.primaryBlack}>
                              {`Total ${pg.count}`}
                            </Text>
                          </SectionLine>
                        ))}
                      </Div>

                      <Div between>
                        <SectionLine>
                          <Oval bgcolor={colors.green} />
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                            style={{ paddingLeft: 8 }}
                          >
                            GOOD
                          </Text>
                        </SectionLine>
                        <SectionLine>
                          <Oval bgcolor={colors.yellow} />
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                            style={{ paddingLeft: 8 }}
                          >
                            FAIR
                          </Text>
                        </SectionLine>
                        <SectionLine>
                          <Oval bgcolor={colors.red} />
                          <Text
                            style={{ paddingLeft: 8 }}
                            fontSize={sizes.xxs}
                            fontWeight={'bold'}
                            color={colors.primaryBlack}
                          >
                            POOR
                          </Text>
                        </SectionLine>
                      </Div>

                      <Div mid>
                        <Text>Click to see more detail</Text>
                      </Div>
                    </Div>
                  )}
                </Div>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    color={colors.textlightGray}
                    fontWeight="med"
                    fontSize={sizes.xl}
                  >
                    No Data
                  </Text>
                </div>
              )}
            </Div>
          </CardTabBody>
        </Grid>

        <Grid style={{ marginTop: 32 }} doubling columns={2}>
          <Grid.Row>
            {donut1 &&
              (processingDashboardModel.dotGraph ? (
                <Grid.Column>
                  <CardTabHeader border style={{ margin: '0' }}>
                    <Div center between padding={24}>
                      <Text
                        fontSize={sizes.l}
                        fontWeight="bold"
                        color={colors.textPurple}
                      >
                        CSA Status
                      </Text>
                      <Text
                        fontSize={sizes.l}
                        fontWeight="bold"
                        color={colors.textPurple}
                      >
                        {`Total - ${processingDashboardModel.allProcessTotal}`}
                      </Text>
                    </Div>
                  </CardTabHeader>
                  <CardTabBody style={{ margin: '0' }}>
                    <Div center col>
                      <Div padding={24}>
                        <ButtonBorder
                          width={170}
                          borderSize={1}
                          borderColor={'#d9d9d6'}
                          text={`${
                            processingDashboardModel.dotGraph
                              ? processingDashboardModel.dotGraph.department
                              : 0
                          } Departments`}
                          textWeight="bold"
                        />
                        <ButtonBorder
                          width={170}
                          borderSize={1}
                          borderColor={'#d9d9d6'}
                          text={`${
                            processingDashboardModel.dotGraph
                              ? processingDashboardModel.dotGraph.division
                              : 0
                          } Divisions`}
                          textWeight="bold"
                        />
                      </Div>
 
                      <Div col style={{ padding: '0px 24px' }}>
                        {processingDashboardModel.dotGraph.list.map(
                          (dgList) => (
                            <Div bottom={8}>
                              <Div width={25}>
                                <Popup
                                  trigger={
                                  <Text
                                    fontWeight={
                                      dgList.indicator.length <= 6 && 'bold'
                                    }
                                    fontSize={sizes.xxs}
                                    color={colors.primaryBlack}
                                  >
                                    {dgList.indicator}
                                  </Text>
                                  }
                                  content={`${dgList.divEn || '-'} ${dgList.shiftEn || ''}`}
                                  size="small"
                                />
                              </Div>
                              <Div width={65} right={40}>
                                {/* <ProgressBarStatus percent={25} data={10} /> */}
                                <ProgressBarStatusTran
                                  data={dgList.summaryStatus}
                                  mode="dashboard"
                                />
                              </Div>
                              <Div right width={10}>
                                <Oval
                                  width={24}
                                  height={24}
                                  bgcolor={colors.primaryBlack}
                                  color="white"
                                >
                                  {dgList.total}
                                </Oval>
                              </Div>
                            </Div>
                          ),
                        )}
                      </Div>
                      <Div between top={10}>
                        <Div width={15} />
                        <Div width={10}>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Preparing By DM
                          </Text>
                        </Div>
                        <Div center width={10}>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Approving By VP
                          </Text>
                        </Div>
                        <Div center width={10}>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Verifying BY ADMIN
                          </Text>
                        </Div>
                        <Div center width={10}>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Completed
                          </Text>
                        </Div>
                        <Div center width={10}>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Total
                          </Text>
                        </Div>
                      </Div>
                    </Div>
                  </CardTabBody>
                </Grid.Column>
              ) : (
                <Grid.Column>
                  <CardEmpty textTitle="No Process Data" />
                </Grid.Column>
              ))}

            {donut2 &&
              (overAllRatingDashBoardModel.detail ? (
                <Grid.Column>
                  <CardTabHeader border style={{ margin: '0' }}>
                    <Div center padding={24}>
                      <Text
                        fontSize={sizes.l}
                        fontWeight="bold"
                        color={colors.textPurple}
                      >
                        Rating by sub-process
                      </Text>
                      <Popup
                        trigger={
                          <Image
                            width={18}
                            style={{ marginLeft: 8 }}
                            src="../../static/images/iconRemark@3x.png"
                          />
                        }
                        content={<Text>After VP Approved</Text>}
                      />
                    </Div>
                  </CardTabHeader>
                  <CardTabBody between style={{ margin: '0' }}>
                    {/* Card Right  */}
                    <Div center col padding={24}>
                      <TableHeader span={12}>
                        {headersDonut2.map(({ render, key, color, width }) => (
                          <Cell
                            center
                            border
                            key={key}
                            color={color}
                            width={width}
                          >
                            <Text
                              fontSize={sizes.xxs}
                              fontWeight="bold"
                              color={
                                render === 'Division' || render === 'Graph'
                                  ? colors.primaryBlack
                                  : colors.primaryBackground
                              }
                            >
                              {render}
                            </Text>
                          </Cell>
                        ))}
                      </TableHeader>
                      {overAllRatingDashBoardModel.detail &&
                        overAllRatingDashBoardModel.detail.map(
                          (item, index) => (
                            <TableBody span={12} key={index} borderTop>
                              <Cell>
                                <Popup
                                  trigger={
                                    <Text
                                      fontSize={sizes.xxs}
                                      fontWeight={
                                        item.indicator.length <= 6 && 'bold'
                                      }
                                      color={colors.primaryBlack}
                                    >
                                      {item.indicator}
                                    </Text>
                                  }
                                  content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                                  size="small"
                                />
                              </Cell>
                              {item.summaryRate.map((summary) => (
                                <>
                                  <Cell borderTop>
                                    <Text
                                      fontSize={sizes.xxs}
                                      fontWeight={
                                        item.text === 'S-RC' && 'bold'
                                      }
                                      color={colors.primaryBlack}
                                    >
                                      {summary.count > 0 ? summary.count : '-'}
                                    </Text>
                                  </Cell>
                                </>
                              ))}
                              <Cell borderTop>
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={item.text === 'S-RC' && 'bold'}
                                  color={colors.primaryBlack}
                                >
                                  {item.total > 0 ? item.total : '-'}
                                </Text>
                              </Cell>

                              <Cell borderTop width={20} padding={4}>
                                <Div>
                                  {item.summaryRate.map((summary) => {
                                    return (
                                      summary.count > 0 && (
                                        <LineGraph
                                          width={summary.count}
                                          bgcolor={summary.renderGraphColor}
                                        >
                                          <Text
                                            fontSize={sizes.xxs}
                                            color={colors.primaryBackground}
                                          >
                                            {summary.count}
                                          </Text>
                                        </LineGraph>
                                      )
                                    );
                                  })}
                                </Div>
                              </Cell>
                            </TableBody>
                          ),
                        )}
                    </Div>
                  </CardTabBody>
                </Grid.Column>
              ) : (
                <Grid.Column>
                  <CardEmpty textTitle="No OverAllRating Data" />
                </Grid.Column>
              ))}

            {donut3 &&
              (allRatingDashboardModel.detail ? (
                <Grid.Column>
                  <CardTabHeader border style={{ margin: '0' }}>
                    <Div center padding={24}>
                      <Text
                        fontSize={sizes.l}
                        fontWeight="bold"
                        color={colors.textPurple}
                      >
                        Rating by control
                      </Text>
                      <Popup
                        trigger={
                          <Image
                            width={18}
                            style={{ marginLeft: 8 }}
                            src="../../static/images/iconRemark@3x.png"
                          />
                        }
                        content={<Text>After VP Approved</Text>}
                      />
                    </Div>
                  </CardTabHeader>
                  <CardTabBody between style={{ margin: '0' }}>
                    {/* Card Right  */}
                    <Div center col padding={24}>
                      <TableHeader span={12}>
                        {headersDonut3.map(({ render, key, color, width }) => (
                          <Cell
                            center
                            border
                            key={key}
                            color={color}
                            width={width}
                          >
                            <Text
                              fontSize={sizes.xxxs}
                              fontWeight="bold"
                              color={
                                render === 'Division' || render === 'Graph'
                                  ? colors.primaryBlack
                                  : colors.primaryBackground
                              }
                            >
                              {render}
                            </Text>
                          </Cell>
                        ))}
                      </TableHeader>
                      {allRatingDashboardModel.detail &&
                        allRatingDashboardModel.detail.map((item, index) => (
                          <TableBody span={12} key={index} borderTop>
                            <Cell>  {/* Division */}
                              <Popup
                                trigger={
                                  <Text
                                    fontSize={sizes.xxs}
                                    fontWeight={item.text === 'S-RC' && 'bold'}
                                    color={colors.primaryBlack}
                                  >
                                    {item.indicator}
                                  </Text>
                                }
                                content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                                size="small"
                              />
                            </Cell>
                            {item.summaryRate.map((summary) => (
                              <>
                                <Cell borderTop> {/* Good ,Fair , Poor */}
                                  <Text
                                    fontSize={sizes.xxs}
                                    fontWeight={item.text === 'S-RC' && 'bold'}
                                    color={colors.primaryBlack}
                                  >
                                    {summary.count > 0 ? summary.count : '-'}
                                  </Text>
                                </Cell>
                              </>
                            ))}
                            <Cell borderTop> {/* Total */}
                              <Text
                                fontSize={sizes.xxs}
                                fontWeight={item.text === 'S-RC' && 'bold'}
                                color={colors.primaryBlack}
                              >
                                {item.total > 0 ? item.total : '-'}
                              </Text>
                            </Cell>

                            <Cell borderTop width={35} padding={4}> {/* Graph */}
                              <Div>
                                {item.summaryRate.map((summary) => {
                                  return (
                                    summary.count > 0 && (
                                      <LineGraph
                                        width={summary.count}
                                        bgcolor={summary.renderGraphColor}
                                      >
                                        <Text
                                          fontSize={sizes.xxs}
                                          color={colors.primaryBackground}
                                        >
                                          {summary.count}
                                        </Text>
                                      </LineGraph>
                                    )
                                  );
                                })}
                              </Div>
                            </Cell>
                            <Cell borderTop width={15}> {/* Enhancement */}
                              {item.goodWithEnhanceList.map((good) => (
                                <Text
                                  fontSize={sizes.xxs}
                                  fontWeight={item.text === 'S-RC' && 'bold'}
                                  color={colors.primaryBlack}
                                >
                                  {good}
                                </Text>
                              ))}
                            </Cell>
                          </TableBody>
                        ))}
                        <TableBody span={12} key={index} borderTop>
                            <Cell>  {/* Division */}
                              <Popup
                                trigger={
                                  <Text
                                    fontSize={sizes.xxs}
                                    fontWeight={'bold'}
                                    color={colors.primaryBlack}
                                  >
                                    {'Total'}
                                  </Text>
                                }
                                size="small"
                              />
                            </Cell>
                            {allRatingDashboardModel.detail[0].summaryRate.map((summary,index) => (
                              <>
                                <Cell borderTop> {/* Good ,Fair , Poor */}
                                  <Text
                                    fontSize={sizes.xxs}
                                    fontWeight={'bold'}
                                    color={colors.primaryBlack}
                                  >
                                    {allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => item.summaryRate[index].count).reduce((a, b) => a + b, 0) || '-'}
                                  </Text>
                                </Cell>
                              </>
                            ))}
                            <Cell borderTop> {/* Total */}
                              <Text
                                fontSize={sizes.xxs}
                                fontWeight={'bold'}
                                color={colors.primaryBlack}
                              >
                                {allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => item.total).reduce((a, b) => a + b, 0) || '-'}
                              </Text>
                            </Cell>

                            <Cell borderTop width={35} padding={4}> {/* Graph */}
                              <Div>
                                {allRatingDashboardModel.detail[0].summaryRate.map((summary,index) => {
                                  return (
                                    allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => item.summaryRate[index].count).reduce((a, b) => a + b, 0) > 0 && (
                                      <LineGraph
                                        width={allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => item.summaryRate[index].count).reduce((a, b) => a + b, 0)}
                                        bgcolor={summary.renderGraphColor}
                                      >
                                        <Text
                                          fontSize={sizes.xxs}
                                          color={colors.primaryBackground}
                                        >
                                          {allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => item.summaryRate[index].count).reduce((a, b) => a + b, 0)}
                                        </Text>
                                      </LineGraph>
                                    )
                                  );
                                })}
                              </Div>
                            </Cell>
                            <Cell borderTop width={15}> {/* Enhancement */}
                              <Text
                                fontSize={sizes.xxs}
                                fontWeight={'bold'}
                                color={colors.primaryBlack}
                              >
                                {allRatingDashboardModel.detail.filter(item => item.indicator.split('-').length == 2 || item.noDepartment).map(item => parseInt(item.goodWithEnhanceList)).filter(item => !Number.isNaN(item)).reduce((a, b) => a + b, 0) || '-'}
                              </Text>
                            </Cell>
                          </TableBody>
                    </Div>
                  </CardTabBody>
                </Grid.Column>
              ) : (
                <Grid.Column>
                  <CardEmpty textTitle="No AllRating Data" />
                </Grid.Column>
              ))}
          </Grid.Row>
        </Grid>

        <WaitDmAcceptModal
          active={activeWaitDMModal}
          onClose={() => setActiveWaitDMModal(false)}
          allWaitDmAcceptModel={allWaitDmAcceptModel}
          year={filterModel.selectedYear}
        />
      </div>
    </div>
  ));
};

export default withLayout(observer(index));
