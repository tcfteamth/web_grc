import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Loader, Dimmer } from "semantic-ui-react";
import Link from "next/link";
// import Router, { useRouter } from "next/router";
import { ButtonAll, Text, ButtonBorder } from "../../components/element";
import {
  ReportCSAPageAction,
  ReportCSAPageRusult,
} from "../../components/reportPage";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListCSAReport } from "../../utils/static";
import styled from "styled-components";
import _ from "lodash";

const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  overflow-x: auto;
  overflow-y: hidden;
`;

const CardTab = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  border-top: ${(props) => (props.top ? "" : `1px solid #d9d9d6`)};
  width: auto;
`;

const CardRow = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 50px;
  width: ${(props) => props.width || 100}%;
  border-top: ${(props) => (props.top ? "" : `1px solid #d9d9d6`)};
`;

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? "center" : "flex-start")};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Line = styled.div`
  border: 1px solid #000;
  width: 8px;
  margin-top: 20px;
`;

const headers = [
  { key: "Sub-Process", render: "Sub-Process", width: 300 },
  { key: "ความเสี่ยง", render: "ความเสี่ยง", width: 300 },
  { key: "การควบคุม", render: "การควบคุม", width: 300 },
  { key: "ผลการประเมิน", render: "ผลการประเมิน", width: 150 },
  { key: "ข้อสังเกตเบื้องต้น", render: "ข้อสังเกตเบื้องต้น", width: 300 },
  {
    key: "ข้อเสนอแนะในการปรับปรุงแก้ไข",
    render: "ข้อเสนอแนะในการปรับปรุงแก้ไข",
    width: 300,
  },
  { key: "ผู้รับผิดชอบ", render: "ผู้รับผิดชอบ", width: 200 },
  { key: "กำหนดเวลาแล้วเสร็จ", render: "กำหนดเวลาแล้วเสร็จ", width: 150 },
  { key: "Status", render: "Status", width: 150 },
];

const sortBy = [
  { text: "Latest", value: 0 },
  { text: "Agent", value: 1 },
  { text: "Team", value: 2 },
  { text: "Walk in", value: 3 },
];

const index = (props) => {
  // const { query } = useRouter();
  const [date, setDate] = useState();

  // useEffect(() => {
  //   onChangeCloseDate(state);
  // }, [query]);

  return (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16} center>
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text color={colors.textSky} fontWeight={"med"} fontSize={sizes.xl}>
          Report / CSA Report
        </Text>
      </Div>

      <div>
        <Card top={32}>
          <Div between center>
            <Text
              fontWeight={"bold"}
              fontSize={sizes.xl}
              color={colors.primaryBlack}
            >
              Corrective Action Plan S-RC-IC
            </Text>
            <ButtonBorder
              text="Export"
              textUpper
              textColor={colors.greenExport}
              width={150}
              fontSize={sizes.s}
              borderColor={colors.greenExport}
              icon={"../../static/images/excel@3x.png"}
            />
          </Div>
        </Card>

        <ReportCSAPageAction />
        <ReportCSAPageRusult />
      </div>
    </div>
  );
};

export default withLayout(observer(index));
