import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Grid, Loader, Dimmer } from "semantic-ui-react";
import Link from "next/link";
import {
  ChartPie,
  ChartDonut,
  ProgressBarStatus,
  BottomBar,
  ButtonAll,
  Text,
  DropdownGroup,
  InputDropdown,
  InputAll,
  ButtonBorder,
} from "../../components/element";
import { dataCardonut, dataCardPie } from "../../utils/static";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import styled from "styled-components";
import _ from "lodash";

const TableHeader = styled.div`
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  align-items: center;
  justify-content: center;
`;
const TableBody = TableHeader.extend`
  padding: 0px !important;
  margin: 0px 40px;
  height: 50px !important;
  border-top: ${(props) => props.borderTop && `1px solid ${colors.grayLight}`};
  justify-content: ${(props) => (props.center ? `center` : "space-between")};
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  background-color: ${(props) => props.color || colors.backgroundPrimary};
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.padding || 0}px;
  border-right: ${(props) => props.border && `1px solid #FFF`};
`;
const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;

const CardTabHeader = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: ${(props) => (props.border ? `0px 24px` : " 24px")};
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: row;
`;
const CardTabBody = CardTabHeader.extend`
  border-top: 1px solid #d9d9d6;
  border-radius: 0px 0px 0px 6px !important;
  min-height: 350px;
  padding: 0px 24px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? "center" : "flex-start")};
  padding-left: ${(props) => props.left || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.padding && props.padding}px;
  border-right: ${(props) => props.border && `1px solid #d9d9d6;`}
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const LineGraph = styled.div`
  height: 18px;
  padding: 0px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${(props) => props.width && props.width}%;
  background-color: ${(props) => props.bgcolor || colors.primaryBackground};
`;

const SectionLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${(props) => props.bottom || 8}px;
`;

const Oval = styled.div`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  margin-right: 8px;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
`;

const headers = [
  { key: "Dept", render: "Dept", color: colors.grayLight, width: 10 },
  { key: "Good", render: "Good", color: "#00b140", width: 10 },
  { key: "Fair", render: "Fair", color: "#ffc72c", width: 10 },
  { key: "Poor", render: "Poor", color: "#e4002b", width: 10 },
  { key: "Total", render: "Total", color: colors.primaryBlack, width: 10 },
  { key: "Graph", render: "Graph", color: colors.grayLight, width: 35 },
  {
    key: "EnhancemEnt",
    render: "Enhancem ent",
    color: colors.textPurple,
    width: 15,
  },
];
const dataCSA = [
  {
    id: 1,
    text: "S-RC-IC",
    total: 18,
    good: 16,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 2,
    text: "S-RC-GRC",
    total: 7,
    good: 30,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 3,
    text: "S-RC-RM",
    total: 8,
    good: 16,
    fair: 0,
    poor: 0,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 4,
    text: "S-RC-SG",
    total: 8,
    good: 80,
    fair: 10,
    poor: 2,
    Graph: 0,
    EnhancemEnt: 0,
  },
  {
    id: 4,
    text: "S-RC",
    total: 8,
    good: 16,
    fair: 0,
    poor: 1,
    Graph: 0,
    EnhancemEnt: 0,
  },
];

const sortBy = [
  { text: "Latest", value: 0 },
  { text: "Agent", value: 1 },
  { text: "Team", value: 2 },
  { text: "Walk in", value: 3 },
];
const dataPieList = {
  dataPie: [30, 30, 40],
  color: [colors.green, colors.yellow, colors.red],
};
const index = (props) => {
  const [date, setDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);

  const onChangeCloseDate = (date) => {
    console.log(date);
    setStartDate(moment(date[0].startDate).format("DD/MM/YYYY"));
    setEndDate(moment(date[0].endDate).format("DD/MM/YYYY"));

    // console.log(date);
    // console.log(
    //   moment(date)
    //     .locale('th')
    //     .format('l'),
    // );
    // popperDateHide();
  };
  let datasummary = [];
  let total = 0;
  const handleSum = () => {
    console.log("dataCSA", dataCSA);
    dataCSA.map((item, index) => {
      console.log(item.total, index);
      datasummary = { total: total + item.total[index] };
    });
    let rObj = 0;
    let reformattedArray = dataCSA.map((obj, i) => {
      rObj = rObj + obj.total[i];
      return rObj;
    });
    console.log("rObj", rObj);
    console.log("datasummary", datasummary);
  };
  useEffect(() => {
    onChangeCloseDate(state);
  }, [state]);

  useEffect(() => {
    handleSum();
  }, []);

  return (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16}>
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          Back
        </Text>
      </Div>
      <div>
        <Card top={32}>
          <Text
            fontWeight={"bold"}
            fontSize={sizes.xl}
            color={colors.primaryBlack}
          >
            CSA Report
          </Text>
          <Div top={28}>
            <Div col width={16} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Year
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
            <Div col width={20} right={16}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Department
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
            <Div col width={20}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Divisions
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown minWidth={60} placeholder="เลือกผู้รับผิดชอบ" />
              </Div>
            </Div>
          </Div>
        </Card>

        <Grid style={{ marginTop: 32 }}>
          <CardTabHeader>
            <Div center between>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                Executive Summary
              </Text>
            </Div>
          </CardTabHeader>
          <CardTabBody between>
            <Div center col padding={24}>
              <Text
                fontSize={sizes.s}
                fontWeight="med"
                color={colors.primaryBlack}
                style={{ lineHeight: "normal" }}
              >
                กระบวนการเห็นว่าสามารถดำเนินการเพื่อปรับปรุงความมีประสิทธิผลของการควบคุมให้ดียิ่งขึ้น
                จำนวน 5 Sub-Processes โดยสรุปภาพรวมได้ดังนี้
                การปรับปรุงแนวทางการปฏิบัติงานเพื่อให้บรรลุวัตถุประสงค์ด้านประสิทธิผลและประสิทธิภาพของการดำเนินงาน
              </Text>
              <Div>
                <div class="ui bulleted list">
                  <div class="item">
                    <Text
                      fontSize={sizes.s}
                      fontWeight="med"
                      color={colors.primaryBlack}
                      style={{ lineHeight: "normal" }}
                    >
                      ปรับปรุงกระบวนการจัดเตรียมอุปกรณ์สำหรับการบำรุงรักษา
                      เพื่อให้มั่นเกิดความมั่นใจว่าจะมีอุปกรณ์และเครื่องมือที่เพียงพอและทันเวลา
                    </Text>
                  </div>
                  <div class="item">
                    <Text
                      fontSize={sizes.s}
                      fontWeight="med"
                      color={colors.primaryBlack}
                      style={{ lineHeight: "normal" }}
                    >
                      ปรับปรุงกระบวนการจัดเก็บข้อมูลและระบบสารสนเทศ
                      ให้มีความเป็นระบบ ครบถ้วน
                      และเพียงพอในการเก็บเอกสารที่เกี่ยวข้อง
                    </Text>
                  </div>
                  <div class="item">
                    <Text
                      fontSize={sizes.s}
                      fontWeight="med"
                      color={colors.primaryBlack}
                      style={{ lineHeight: "normal" }}
                    >
                      สรรหา พัฒนาบุคลากรที่มีความรู้และประสบการณ์
                      ในการทำหน้าที่ตรวจสอบคุณสอบคุณภาพของงานในระหว่างการทำ
                      Turnaround เพื่อลดความเสี่ยงที่งานจะผิดพลาด ไม่ได้มาตรฐาน
                      หรือเวลาแล้วเสร็จไม่เป็นไปตามแผนงาน
                    </Text>
                  </div>
                  <div class="item">
                    <Text
                      fontSize={sizes.s}
                      fontWeight="med"
                      color={colors.primaryBlack}
                      style={{ lineHeight: "normal" }}
                    >
                      จัด KPI alignment workshop
                      เพื่อให้สอดคล้องกับหน่วยงานอื่นที่เกี่ยวข้องในสายงานเดียวกัน
                    </Text>
                  </div>
                </div>
              </Div>
              <Text
                fontSize={sizes.s}
                fontWeight="med"
                color={colors.primaryBlack}
                style={{ lineHeight: "normal" }}
              >
                ซึ่งในขั้นต่อไป หน่วยงาน S-RC-IC
                จะประสานงานกับผู้ที่รับผิดชอบกระบวนการเพื่อติดตามความคืบหน้าของแผนการแก้ไขปรับปรุงประเด็น
                รวมถึงให้คำแนะนำเพิ่มเติมอื่นๆ (ถ้ามี)
              </Text>
            </Div>
          </CardTabBody>
        </Grid>
        <Grid style={{ marginTop: 32 }}>
          <CardTabHeader border>
            <Div center between width={30} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                CSA Result
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
            <Div center between width={70} padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                Rating by sub-process
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
          </CardTabHeader>
          <CardTabBody between>
            {/* Card Left  */}
            <Div center col width={30} border padding={24}>
              <Text
                className="upper"
                style={{ marginBottom: 24 }}
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                No. of sub-process
              </Text>
              <Div center col>
                <Div>
                  <ChartPie
                    data={dataPieList.dataPie}
                    backgroundColor={dataPieList.color}
                  />
                </Div>
                <Div between>
                  <Link href={`/report/reportCSADetail?type=${"Good"}`}>
                    <SectionLine className="click">
                      <Oval bgcolor={colors.green} />
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={"bold"}
                        color={colors.primaryBlack}
                      >
                        GOOD
                      </Text>
                    </SectionLine>
                  </Link>
                  <SectionLine>
                    <Oval bgcolor={colors.yellow} />
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight={"bold"}
                      color={colors.primaryBlack}
                    >
                      FAIR
                    </Text>
                  </SectionLine>
                  <SectionLine>
                    <Oval bgcolor={colors.red} />
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight={"bold"}
                      color={colors.primaryBlack}
                    >
                      POOR
                    </Text>
                  </SectionLine>
                </Div>
              </Div>
            </Div>

            {/* Card Right  */}
            <Div center col padding={24}>
              <TableHeader span={12}>
                {headers.map(({ render, key, color, width }) => (
                  <Cell center border key={key} color={color} width={width}>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={
                        render === "Dept" || render === "Graph"
                          ? colors.primaryBlack
                          : colors.primaryBackground
                      }
                    >
                      {render}
                    </Text>
                  </Cell>
                ))}
              </TableHeader>
              {dataCSA &&
                dataCSA.map((item, index) => (
                  <TableBody span={12} key={index} borderTop>
                    <Cell className="click">
                      <Link
                        href={`/report/reportCSADetail?type=${"Rating"}&text=${
                          item.text
                        } `}
                      >
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight={item.text === "S-RC" && "bold"}
                          color={colors.primaryBlack}
                        >
                          {item.text}
                        </Text>
                      </Link>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.good > 0 ? item.good : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.fair > 0 ? item.fair : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.poor > 0 ? item.poor : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.total > 0 ? item.total : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop width={35} padding={4}>
                      {item.text !== "S-RC" && (
                        <Div>
                          {item.good > 0 && (
                            <LineGraph width={item.good} bgcolor={colors.green}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.good}
                              </Text>
                            </LineGraph>
                          )}
                          {item.fair > 0 && (
                            <LineGraph
                              width={item.fair}
                              bgcolor={colors.yellow}
                            >
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.fair}
                              </Text>
                            </LineGraph>
                          )}
                          {item.poor > 0 && (
                            <LineGraph width={item.poor} bgcolor={colors.red}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.poor}
                              </Text>
                            </LineGraph>
                          )}
                        </Div>
                      )}
                    </Cell>
                    <Cell borderTop width={15}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.EnhancemEnt > 0 ? item.EnhancemEnt : "-"}
                      </Text>
                    </Cell>
                  </TableBody>
                ))}
            </Div>
          </CardTabBody>
        </Grid>
        <Grid style={{ marginTop: 32 }}>
          <CardTabHeader border>
            <Div center between width={30} padding={24} border>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                CSA Result
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
            <Div center between width={70} padding={24}>
              <Text
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.textPurple}
              >
                Rating by sub-process
              </Text>
              <Text
                fontSize={sizes.l}
                fontWeight="med"
                color={colors.primaryBlack}
              >
                S-RC
              </Text>
            </Div>
          </CardTabHeader>
          <CardTabBody between>
            {/* Card Left  */}
            <Div center col width={30} border padding={24}>
              <Text
                className="upper"
                style={{ marginBottom: 24 }}
                fontSize={sizes.l}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                No. of Control
              </Text>
              <Div center col>
                <Div>
                  <ChartPie
                    data={dataPieList.dataPie}
                    backgroundColor={dataPieList.color}
                  />
                </Div>
                <Div between>
                  <SectionLine>
                    <Oval bgcolor={colors.green} />
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight={"bold"}
                      color={colors.primaryBlack}
                    >
                      GOOD
                    </Text>
                  </SectionLine>
                  <SectionLine>
                    <Oval bgcolor={colors.yellow} />
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight={"bold"}
                      color={colors.primaryBlack}
                    >
                      FAIR
                    </Text>
                  </SectionLine>
                  <SectionLine>
                    <Oval bgcolor={colors.red} />
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight={"bold"}
                      color={colors.primaryBlack}
                    >
                      POOR
                    </Text>
                  </SectionLine>
                </Div>
              </Div>
            </Div>

            {/* Card Right  */}
            <Div center col padding={24}>
              <TableHeader span={12}>
                {headers.map(({ render, key, color, width }) => (
                  <Cell center border key={key} color={color} width={width}>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={
                        render === "Dept" || render === "Graph"
                          ? colors.primaryBlack
                          : colors.primaryBackground
                      }
                    >
                      {render}
                    </Text>
                  </Cell>
                ))}
              </TableHeader>
              {dataCSA &&
                dataCSA.map((item, index) => (
                  <TableBody span={12} key={index} borderTop>
                    <Cell>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.text}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.good > 0 ? item.good : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.fair > 0 ? item.fair : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.poor > 0 ? item.poor : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.total > 0 ? item.total : "-"}
                      </Text>
                    </Cell>
                    <Cell borderTop width={35} padding={4}>
                      {item.text !== "S-RC" && (
                        <Div>
                          {item.good > 0 && (
                            <LineGraph width={item.good} bgcolor={colors.green}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.good}
                              </Text>
                            </LineGraph>
                          )}
                          {item.fair > 0 && (
                            <LineGraph
                              width={item.fair}
                              bgcolor={colors.yellow}
                            >
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.fair}
                              </Text>
                            </LineGraph>
                          )}
                          {item.poor > 0 && (
                            <LineGraph width={item.poor} bgcolor={colors.red}>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBackground}
                              >
                                {item.poor}
                              </Text>
                            </LineGraph>
                          )}
                        </Div>
                      )}
                    </Cell>
                    <Cell borderTop width={15}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight={item.text === "S-RC" && "bold"}
                        color={colors.primaryBlack}
                      >
                        {item.EnhancemEnt > 0 ? item.EnhancemEnt : "-"}
                      </Text>
                    </Cell>
                  </TableBody>
                ))}
            </Div>
          </CardTabBody>
        </Grid>
      </div>
    </div>
  );
};

export default withLayout(observer(index));
