import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Grid, Loader, Dimmer } from "semantic-ui-react";
import Link from "next/link";
import { ReportCSAPageAction } from "../../components/reportPage";
import {
  Text,
  InputDropdown,
  InputAll,
  ButtonBorder,
} from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListReport } from "../../utils/static";
import styled from "styled-components";
import _ from "lodash";
import { DateRange, Calendar } from "react-date-range";

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  padding: 10px 24px;
  align-items: center;
  justify-content: space-between;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : "8.3")}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 1px 0px;
  border-top: 1px solid #d9d9d6;
`;

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.group ? `space-around` : `start`)};
`;

const CardTab = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  display: inline-block;
  margin: 0px 14px;
  padding: 0px !important;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: flex;
  flex-direction: column;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? "center" : "flex-start")};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Line = styled.div`
  border: 1px solid #000;
  width: 8px;
  margin-top: 20px;
`;

const headers = [
  { key: "No.", render: "No.", width: 5 },
  { key: "Process Name", render: "Process Name", width: 30 },
  { key: "Division", render: "Division", width: 10 },
  { key: "Status", render: "Status", width: 10 },
  { key: "Progress", render: "Progress", width: 20 },
  { key: "Problem", render: "Problem", width: 20 },
];

const sortBy = [
  { text: "Latest", value: 0 },
  { text: "Agent", value: 1 },
  { text: "Team", value: 2 },
  { text: "Walk in", value: 3 },
];

const index = (props) => {
  const [date, setDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);

  const onChangeCloseDate = (date) => {
    console.log(date);
    setStartDate(moment(date[0].startDate).format("DD/MM/YYYY"));
    setEndDate(moment(date[0].endDate).format("DD/MM/YYYY"));

    // console.log(date);
    // console.log(
    //   moment(date)
    //     .locale('th')
    //     .format('l'),
    // );
    // popperDateHide();
  };
  useEffect(() => {
    onChangeCloseDate(state);
  }, [state]);

  return (
    <div style={{ paddingBottom: 96 }}>
      <Dimmer>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <Div top={16}>
        <Image
          src="../../static/images/black@3x.png"
          style={{ marginRight: 12, width: 18, height: 18 }}
        />
        <Text
          color={colors.primaryBlack}
          fontWeight={"bold"}
          fontSize={sizes.xl}
        >
          Back
        </Text>
      </Div>
      <div>
        <Card top={32}>
          <Div between>
            <Text
              fontWeight={"bold"}
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              {"Follow Up Action Report"}
            </Text>
            <ButtonBorder
              text="Export"
              textColor={"#2e7d32"}
              fontSize={sizes.s}
              borderColor={"#2e7d32"}
              icon={"../../static/images/excel@3x.png"}
            />
          </Div>
          <Grid
            columns={3}
            padded="horizontally"
            style={{ margin: 0, margin: "32px -16px 0px" }}
          >
            <Div col width={33} style={{ paddingLeft: 0, marginRight: 8 }}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Close Date
              </Text>
              <Div items={"flex-Start"} between>
                <InputAll
                  width={170}
                  minWidth={60}
                  value={startDate}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
                <Line />
                <InputAll
                  width={170}
                  minWidth={60}
                  value={endDate}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
              </Div>
            </Div>
            <Div col width={33} style={{ paddingLeft: 0, marginRight: 8 }}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Recorded Date
              </Text>
              <Div items={"flex-Start"} between>
                <InputDropdown
                  width={170}
                  minWidth={60}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
                <Line />
                <InputDropdown
                  width={170}
                  minWidth={60}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
              </Div>
            </Div>
            <Div col width={16} style={{ paddingLeft: 0 }}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Status
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown
                  width={170}
                  minWidth={60}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
              </Div>
            </Div>
            <Div col width={16} style={{ paddingLeft: 9, paddingRight: 0 }}>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Division
              </Text>
              <Div items={"flex-Start"}>
                <InputDropdown
                  width={170}
                  minWidth={60}
                  placeholder="เลือกผู้รับผิดชอบ"
                />
              </Div>
            </Div>
          </Grid>
          <div className="react-calendar">
            <DateRange
              showDateDisplay={false}
              editableDateInputs={true}
              onChange={(item) => setState([item.selection])}
              moveRangeOnFirstSelection={false}
              ranges={state}
              showMonthAndYearPickers={false}
            />
          </div>
        </Card>
        <ReportCSAPageAction />
      </div>
    </div>
  );
};

export default withLayout(observer(index));
