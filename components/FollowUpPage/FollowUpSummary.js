import React, { useState } from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { Grid, Pagination, GridRow, Icon } from 'semantic-ui-react';
import {
  Text,
  CheckBoxAll,
  BottomBarFollowUpSummary,
} from '../../components/element';
import { colors, config, sizes } from '../../utils';
import RowFollowUpSummaryList from './RowFollowUpSummaryList';
import { initAuthStore } from '../../contexts';

const CardContainer = styled.div`
  margin-bottom: 98px;
  margin-top: 16px;
`;

const CardTabel = styled.div`
  display: inline-block;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-bottom: ${(props) => props.bottom || 0}px;
  padding: 10px 16px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin-top: 0px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: center;
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 8}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
  padding-left: ${(props) => props.left || 0}px;
`;

const headers = [
  { key: 'No.', render: 'Co-Responsible Division', width: 8 },
  { key: 'Assessment Name', render: 'Assessment Name', width: 45 },
  { key: 'Division', render: 'Division/Shift (Owner)', width: 8, center: 'center' },
  { key: 'Due Date', render: 'Due Date', width: 8, center: 'center' },
  { key: 'Last Update', render: 'Last Update', width: 8, center: 'center' },
  {
    key: 'Rating by Control',
    render: 'Rating by Control',
    width: 8,
    center: 'center',
  },
  { key: 'status', render: 'Status', width: 8, center: 'center' },
];

const FollowUpSummary = ({ data, followUpSummaryModel, followUpModel }) => {
  const authContext = initAuthStore();

  const handleExportFollowSummary = async () => {
    try {
      const result = await followUpSummaryModel.exportFollowUpSummary(
        authContext.accessToken,
        followUpModel.filterModel.searchText,
        followUpModel.filterModel.selectedYear,
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
      );

      window.open(result.config.url, '_blank');
    } catch (error) {
      console.log(error);
    }
  };

  return useObserver(() => (
    <CardContainer>
      <CardTabel bottom={8}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, center, left }) => (
            <Cell key={key} width={width} center={center} left={left}>
              {render === 'Accept' || render === 'Reject' ? (
                <Div col>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={render === 'Accept' ? '#0057b8' : '#e4002b'}
                  >
                    {render}
                  </Text>
                  <CheckBoxAll status={render} />
                </Div>
              ) : (
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.textGray}
                  style={{ textAlign: center ?? '' }}
                >
                  {render}
                </Text>
              )}
            </Cell>
          ))}
        </TableHeader>
      </CardTabel>
      {followUpSummaryModel.list?.map((l) => (
        <RowFollowUpSummaryList list={l} />
      ))}

      <BottomBarFollowUpSummary handleExport={handleExportFollowSummary} />

      {followUpSummaryModel.totalPage > 1 && (
        <Grid>
          <GridRow style={{ justifyContent: 'flex-end', right: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={followUpSummaryModel.page}
              activePage={followUpSummaryModel.page}
              onPageChange={(e, { activePage }) =>
                followUpSummaryModel.getFollowUpSummaryList(
                  authContext.accessToken,
                  activePage,
                  10,
                  followUpModel.filterModel.searchText,
                  followUpModel.filterModel.selectedYear,
                  followUpModel.filterModel.selectedLvl3,
                  followUpModel.filterModel.selectedLvl4,
                  followUpModel.filterModel.selectedBu,
                  followUpModel.filterModel.selectedDepartment.no,
                  followUpModel.filterModel.selectedDivision,
                  followUpModel.filterModel.selectedShift,
                  followUpModel.filterModel.isSetStartDueDate,
                  followUpModel.filterModel.isSetEndDueDate,
                  followUpModel.filterModel.selectedStatus,
                  followUpModel.filterModel.selectedProcessAnd,
                  followUpModel.filterModel.selectedProcessOr,
                  followUpModel.filterModel.selectedObjectives,
                )
              }
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={followUpSummaryModel.totalPage}
            />
          </GridRow>
        </Grid>
      )}
    </CardContainer>
  ));
};

export default FollowUpSummary;
