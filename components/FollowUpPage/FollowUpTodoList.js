import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import {
  Image,
  Grid,
  Pagination,
  GridRow,
  Icon,
  Popup,
} from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import { Text, OverAllRatingStatus, FollowUpStatus } from '../element';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';

const CardContainer = styled.div`
  margin-bottom: 98px;
  margin-top: 16px;
`;

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  min-height: 50px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  margin: 7px 13px 8px 13px;
  padding: 10px 0px;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 3px 13px 3px 13px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
  padding-left: ${(props) => props.left || 0}px;
`;

const headers = [
  { key: 'No.', render: 'Co-Responsible Division', width: 8 },
  { key: 'Assessment Name', render: 'Assessment Name', width: 20 },
  { key: 'Control', render: 'Control', width: 20, center: 'center' },
  { key: 'Division', render: 'Division/Shift (Owner)', width: 10, center: 'center' },
  { key: 'Due Date', render: 'Due Date', width: 8, center: 'center' },
  { key: 'Last Update', render: 'Last Update', width: 8, center: 'center' },
  {
    key: 'Rating by Control',
    render: 'Rating by Control',
    width: 8,
    center: 'center',
  },
  { key: 'status', render: 'Status', width: 8, center: 'center' },
  { key: 'Action Plan', render: 'Action Plan', width: 8, center: 'center' },
];

const FollowUpTodoList = ({ data, followUpModel }) => {
  const authContext = initAuthStore();

  return useObserver(() => (
    <CardContainer>
      <Grid style={{ marginTop: 12 }}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, center }) => (
            <Cell key={key} width={width} center={center}>
              <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.textGray}
                style={{ textAlign: center ?? '' }}
              >
                {render}
              </Text>
            </Cell>
          ))}
        </TableHeader>
        {followUpModel.followUpList.length > 0 &&
          followUpModel.followUpList.map((list, index) => (
            <TableBody span={12} key={index}>
              <Cell width={8}>
                {list.partner && (
                  <Popup
                    content={list.partnerName}
                    trigger={
                      <Image
                        src="../../static/images/icons-followup-group-1-24-px@3x.png"
                        style={{ marginRight: 12, width: 24, height: 24 }}
                      />
                    }
                  />
                )}
              </Cell>
              <Cell width={20}>
                <Popup
                  content={list.assessmentName}
                  trigger={
                    <a href={`/followUp/followUpDetail?id=${list.id}`}>
                      <Text
                        fontSize={sizes.xs}
                        color={colors.primaryBlack}
                        className="click"
                        lineClamp={2}
                      >
                        {list.assessmentName}
                      </Text>
                    </a>
                  }
                />
              </Cell>
              <Cell width={20}>
                <Popup
                  content={list.controlName}
                  trigger={
                    <Text
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                      lineClamp={2}
                    >
                      {list.controlName}
                    </Text>
                  }
                />
              </Cell>
              <Cell width={10} center>
                <Popup
                  trigger={
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {list.division}
                    </Text>
                  }
                  content={`${list.divEn || '-'} ${list.shiftEn || ''}`}
                  size="small"
                />
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {list.dueDate}
                </Text>
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {list.lastUpdate}
                </Text>
              </Cell>
              <Cell width={8} center>
                <OverAllRatingStatus overAllstatus={list.rateId} />
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {/* {list.status} */}
                  <FollowUpStatus status={list.status} />
                </Text>
              </Cell>
              <Cell width={8} center>
                <Popup
                  content={list.actionPlan || '-'}
                  header="Action Plan"
                  position="top center"
                  trigger={
                    <Image
                      src="../../static/images/icons-plan-24-px-sky-blue@3x.png"
                      style={{ width: '24px', height: '24px' }}
                    />
                  }
                />
              </Cell>
            </TableBody>
          ))}
      </Grid>

      {followUpModel.totalPage > 1 && (
        <Grid>
          <GridRow style={{ justifyContent: 'flex-end', right: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={followUpModel.page}
              defaultActivePage={followUpModel.page}
              onPageChange={(e, { activePage }) =>
                followUpModel.getFollowUpTodoList(
                  authContext.accessToken,
                  activePage,
                  10,
                  followUpModel.filterModel.searchText,
                  '',
                  followUpModel.filterModel.selectedLvl3,
                  followUpModel.filterModel.selectedLvl4,
                  followUpModel.filterModel.selectedBu,
                  followUpModel.filterModel.selectedDepartment.no,
                  followUpModel.filterModel.selectedDivision,
                  followUpModel.filterModel.selectedShift,
                  followUpModel.filterModel.isSetStartDueDate,
                  followUpModel.filterModel.isSetEndDueDate,
                  followUpModel.filterModel.selectedStatus,
                  followUpModel.filterModel.selectedProcessAnd,
                  followUpModel.filterModel.selectedProcessOr,
                  followUpModel.filterModel.selectedObjectives,
                )
              }
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={followUpModel.totalPage}
            />
          </GridRow>
        </Grid>
      )}
    </CardContainer>
  ));
};

export default FollowUpTodoList;
