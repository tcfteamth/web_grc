import React, { useState, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { Grid, Image } from 'semantic-ui-react';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import moment from 'moment';
import { colors, sizes } from '../../utils';
import {
  ButtonAll,
  ButtonBorder,
  DivisionDropdown,
  ShiftDropdown,
  FollowUpStatusListDropdown,
  FollowUpYearListDropdown,
  ManageBuDropdown,
  ManageDepartmentDropdown,
  ManageDivisionDropdown,
  ManageShiftDropdown,
  NewDates,
  ProcessLvl3Dropdown,
  ProcessLvl4Dropdown,
  Text,
  DropdownSelect
} from '../element';
import { initAuthStore } from '../../contexts';
import { FollowUpFilterListModel } from '../../model/FollowUpModel';
import { taglistDropdownStore } from '../../model';

const CardFillter = styled.div`
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px;
  margin-top: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const ContainerFilter = ({
  followUpModel,
  followUpSummaryModel,
  activeTab,
  onClose,
}) => {
  const [tagsList, setTagList] = useState();
  const tagListDropdown = useLocalStore(() => taglistDropdownStore);

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };
  const authContext = initAuthStore();
  const followUpFilterListModel = useLocalStore(
    () => new FollowUpFilterListModel(),
  );

  const handleProcessTagSelection = (e,data,field) => {
    let result = followUpModel.filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    followUpModel.filterModel.setField(
      field,
      result,
    );
  }

  const submitFilterList = () => {
    if (activeTab === 'TODO') {
      followUpModel.getFollowUpTodoList(
        authContext.accessToken,
        1,
        10,
        followUpModel.filterModel.searchText,
        '',
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
        followUpModel.filterModel.selectedProcessAnd,
        followUpModel.filterModel.selectedProcessOr,
        followUpModel.filterModel.selectedObjectives,
      );
    } else {
      followUpSummaryModel.getFollowUpSummaryList(
        authContext.accessToken,
        1,
        10,
        followUpModel.filterModel.searchText,
        followUpModel.filterModel.selectedYear,
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
        followUpModel.filterModel.selectedProcessAnd,
        followUpModel.filterModel.selectedProcessOr,
        followUpModel.filterModel.selectedObjectives,
      );
      followUpModel.getFollowUpTotalStatusCount(
        authContext.accessToken,
        followUpModel.filterModel.searchText,
        followUpModel.filterModel.selectedYear,
        followUpModel.filterModel.selectedLvl3,
        followUpModel.filterModel.selectedLvl4,
        followUpModel.filterModel.selectedBu,
        followUpModel.filterModel.selectedDepartment.no,
        followUpModel.filterModel.selectedDivision,
        followUpModel.filterModel.selectedShift,
        followUpModel.filterModel.isSetStartDueDate,
        followUpModel.filterModel.isSetEndDueDate,
        followUpModel.filterModel.selectedStatus,
        followUpModel.filterModel.selectedProcessAnd,
        followUpModel.filterModel.selectedProcessOr,
        followUpModel.filterModel.selectedObjectives,
      );
    }
  };

  useEffect(() => {
    (async () => {
      try {
        await followUpFilterListModel.getFilterList(authContext.accessToken);
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    // clear filter status
    // เพราะ status ใน todo กับ summary ต่างกัน
    followUpModel.filterModel.setField('selectedStatus', '');
  }, [activeTab]);

  return useObserver(() => (
    <CardFillter>
      <Div between>
        <Text fontWeight={'bold'} fontSize={sizes.s} color={'#00aeef'}>
          Advanced Filter
        </Text>
        <div className="click" onClick={onClose}>
          <Image
            src="../../static/images/x-close@3x.png"
            style={{ marginRight: 12, width: 18, height: 18 }}
          />
        </div>
      </Div>
      <Grid>
        <Grid.Row>
          {followUpFilterListModel.filterList?.map((filter) => (
            <>
              {filter.filterName === 'Process (Level 3)' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Process Lv.3
                  </Text>
                  <ProcessLvl3Dropdown
                    handleOnChange={(process) => {
                      followUpModel.filterModel.setField(
                        'selectedLvl3',
                        process,
                      );
                    }}
                    value={followUpModel.filterModel.selectedLvl3}
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Sub-Process (Level 4)' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Process Lv.4
                  </Text>
                  <ProcessLvl4Dropdown
                    handleOnChange={(process) => {
                      followUpModel.filterModel.setField(
                        'selectedLvl4',
                        process,
                      );
                    }}
                    value={followUpModel.filterModel.selectedLvl4}
                    selectedLVl3={followUpModel.filterModel.selectedLvl3}
                    defaultValue="Select Process (Level 4)"
                    placeholder="Select Process (Level 4)"
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'BU' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    BU
                  </Text>
                  <ManageBuDropdown
                    placeholder="Select BU"
                    handleOnChange={(bu) => {
                      followUpModel.filterModel.setField(
                        'selectedBu',
                        bu.value,
                      );

                      followUpModel.filterModel.setField(
                        'selectedDepartment',
                        '',
                      );
                      followUpModel.filterModel.setField(
                        'selectedDivision',
                        '',
                      ); 
                      followUpModel.filterModel.setField(
                        'selectedShift',
                        '',
                      );
                    }}
                    value={followUpModel.filterModel.selectedBu}
                    isDefaultValue={filter.isDefaultValue}
                    filterModel={followUpModel.filterModel}
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Department' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Department
                  </Text>
                  <ManageDepartmentDropdown
                    placeholder="Select Department"
                    handleOnChange={(department) => {
                      followUpModel.filterModel.setField(
                        'selectedDepartment',
                        department,
                      );

                      followUpModel.filterModel.setField(
                        'selectedDivision',
                        '',
                      );
                      followUpModel.filterModel.setField(
                        'selectedShift',
                        '',
                      );
                    }}
                    value={followUpModel.filterModel.selectedDepartment.value}
                    selectedBu={followUpModel.filterModel.selectedBu}
                    isDefaultValue={filter.isDefaultValue}
                    filterModel={followUpModel.filterModel}
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Division' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Division
                  </Text>
                  <DivisionDropdown
                    defaultValue="Select Division"
                    placeholder="Select Division"
                    handleOnChange={(divisionNo) => {
                      followUpModel.filterModel.setField(
                        'selectedDivision',
                        divisionNo,
                      );
                      followUpModel.filterModel.setField(
                        'selectedShift',
                        '',
                      );
                      if (authContext.roles.isAdmin) {
                        followUpModel.filterModel.resetDivisionMistake();
                      }
                    }}
                    value={followUpModel.filterModel.selectedDivision}
                    selectedDepartmentId={
                      followUpModel.filterModel.selectedDepartment.value
                    }
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Shift' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Shift
                  </Text>
                  <ShiftDropdown
                    defaultValue="Select Shift"
                    placeholder="Select Shift"
                    handleOnChange={(shiftNo) => {
                      followUpModel.filterModel.setField(
                        'selectedShift',
                        shiftNo,
                      );
                      followUpModel.filterModel.setField(
                        'selectedDivision',
                        '',
                      );

                      if (authContext.roles.isAdmin) {
                        followUpModel.filterModel.resetShiftMistake();
                      }
                    }}
                    value={followUpModel.filterModel.selectedShift}
                    selectedDepartmentId={
                      followUpModel.filterModel.selectedDepartment.value
                    }
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}
              {filter.filterName === 'year' && activeTab === 'SUMMARY' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Year
                  </Text>
                  <FollowUpYearListDropdown
                    handleOnChange={(year) => {
                      followUpModel.filterModel.setField('selectedYear', year);
                    }}
                    value={followUpModel.filterModel.selectedYear}
                    defaultValue="Select Year"
                    placeholder="Select Year"
                    disabled={filter.isDisable}
                    model={followUpModel.filterModel}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Status' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Status
                  </Text>
                  <FollowUpStatusListDropdown
                    handleOnChange={(status) => {
                      followUpModel.filterModel.setField(
                        'selectedStatus',
                        status,
                      );
                    }}
                    value={followUpModel.filterModel.selectedStatus}
                    defaultValue="Select Status"
                    placeholder="Select Status"
                    tab={activeTab}
                    disabled={filter.isDisable}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Start Due Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Start DueDate
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    onChange={(date) => {
                      followUpModel.filterModel.setField(
                        'selectedStartDueDate',
                        date[0].valueOf(),
                      );
                    }}
                    value={followUpModel.filterModel.selectedStartDueDate}
                    placeholder="Select Start Due Date"
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'End Due Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    End DueDate
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    onChange={(date) => {
                      followUpModel.filterModel.setField(
                        'selectedEndDueDate',
                        date[0].valueOf(),
                      );
                    }}
                    value={followUpModel.filterModel.selectedEndDueDate}
                    placeholder="Select End Due Date"
                  />
                </Grid.Column>
              )}
            </>
          ))}
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Process Tag (AND)
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Tag"
                options={tagsList}
                value={followUpModel.filterModel.selectedProcessAnd}
                isSearchable
                isMulti
                handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
              />
            </Div>
          </Grid.Column>
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Process Tag (OR)
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Tag"
                options={tagsList}
                value={followUpModel.filterModel.selectedProcessOr}
                isSearchable
                isMulti
                handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
              />
            </Div>
          </Grid.Column>
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Objective
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Objective"
                options={objectiveList}
                value={followUpModel.filterModel.selectedObjectives}
                isSearchable
                handleOnChange={ (data) => followUpModel.filterModel.setField(
                  'selectedObjectives',
                  data,
                )
                }
              />
            </Div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Div top={8}>
        <ButtonAll color="#1b1464" text="Filter" onClick={submitFilterList} />
        <ButtonBorder
          text="Clear"
          borderColor="#1b1464"
          handelOnClick={() => {
            followUpModel.filterModel.clearFilterFollowUpList();
            followUpSummaryModel.getFollowUpSummaryList(
              authContext.accessToken,
              1,
              10,
            );
            followUpModel.getFollowUpTotalStatusCount(authContext.accessToken);
          }}
        />
      </Div>
    </CardFillter>
  ));
};

export default ContainerFilter;
