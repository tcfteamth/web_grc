import React, { useState } from 'react';
import { Image, Grid, GridRow, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import Router from 'next/router';
import { colors, sizes } from '../../utils';
import {
  Text,
  ButtonAll,
  InputAll,
  DropdownAll,
  TextArea,
  InputGroup,
  ModalGlobal,
  ButtonBorder,
} from '../element';
import {
  FollowUpControlDetail,
  FollowUpControlMoreDetail,
  FollowUpImprovementDetail,
} from '.';
import { initAuthStore } from '../../contexts';
// import { CardControlAssessment, AddCardControlAssessment } from '..';
// import { MappingModal, LibraryRiskModal, LibraryControlModal } from '../modal';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  risk,
  assessmentDetailModel,
  objects,
  followUpDetailModel,
  followUpId,
}) => {
  const authContext = initAuthStore();
  const [activeSubmitModal, setActiveSubmitModal] = useState(false);

  const handleUpdateFollowUpDetail = async () => {
    try {
      setActiveSubmitModal(false);

      // if (authContext.roles.isVP) {
      //   const result = await followUpDetailModel.submitFollowUp(
      //     authContext.accessToken,
      //     Router.query.id,
      //   );

      //   Router.push('/followUp/followUpList');

      //   return result;
      // }

      const result = await followUpDetailModel.updateFollowUpDetail(
        authContext.accessToken,
        followUpId,
      );

      Router.push('/followUp/followUpList');

      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleSaveFollowUpDetail = async () => {
    try {
      await followUpDetailModel.saveFollowUpDetail(
        authContext.accessToken,
        Router.query.id,
      );

      Router.reload();
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div>
      <Grid columns="equal" style={{ margin: 0, padding: 24 }}>
        <Grid.Column computer={2} style={{ padding: 0 }}>
          <div style={{ display: 'flex' }}>
            <Text fontSize={sizes.m} color={colors.primary}>
              Risk
              <span
                style={{
                  paddingLeft: 4,
                  fontWeight: 'bold',
                  marginRight: '16px',
                }}
              >
                {/* {`R${risk.no}`}{' '} */}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </span>
            </Text>
          </div>
        </Grid.Column>
        <Grid.Column
          computer={13}
          style={{
            padding: 0,
            width: '90%',
            wordWrap: 'break-word',
          }}
        >
          <Text fontSize={sizes.m} color={colors.primary}>
            {followUpDetailModel.followUpDetail &&
              followUpDetailModel.followUpDetail.lv6Name}
          </Text>
        </Grid.Column>

        <Divider />

        {followUpDetailModel && (
          <FollowUpControlDetail followUpDetailModel={followUpDetailModel} />
        )}

        {followUpDetailModel.followUpDetail && (
          <FollowUpControlMoreDetail
            detailModel={followUpDetailModel.followUpDetail}
            followUpDetailModel={followUpDetailModel}
          />
        )}

        <FollowUpImprovementDetail
          followUpDetailModel={followUpDetailModel}
          followUpId={followUpId}
        />

        <Divider />

        <Grid.Row centered>
          {/* <ButtonAll
            text="Submit"
            onClick={() => setActiveSubmitModal(true)}
            disabled={followUpDetailModel.isSubmitButtonDisabled}
          /> */}

          {/* <ButtonAll text="Save" onClick={handleSaveFollowUpDetail} /> */}
        </Grid.Row>

        <ModalGlobal
          onSubmit={handleUpdateFollowUpDetail}
          onClose={() => setActiveSubmitModal(false)}
          open={activeSubmitModal}
          submitText="Submit"
          cancelText="Cancel"
          title="Submit FollowUp"
        />
      </Grid>
      <DividerLine />
    </div>
  ));
};

export default index;
