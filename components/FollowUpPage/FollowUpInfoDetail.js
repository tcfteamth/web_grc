import React from 'react';
import { Segment, Grid, Image } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import { Text } from '../element';
import { colors, sizes } from '../../utils';

const CardItem = styled(Segment)`
  width: 100% !important;
  min-width: 100% !important;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 24px !important;
  flex-direction: column;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 272px;
  }

  @media only screen and (min-width: 1024px) {
    min-height: 318px;
  }
  @media only screen and (min-width: 1300px) {
    min-height: 299px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 280px;
  }
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const FollowUpInfoDetail = ({ followUpDetailModel }) => {
  //

  return useObserver(() => (
    <CardItem>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Text
              fontWeight="bold"
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              Assessor
            </Text>

            <Div top={8}>
              <Image
                src="../../static/images/user@3x.png"
                style={{
                  width: 32,
                  height: 32,
                  marginRight: 8,
                  marginTop: 2,
                }}
              />
              <div>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.worker.name}
                </Text>
                <Text
                  fontSize={14}
                  color={colors.textDarkBlack}
                  style={{ marginTop: -4 }}
                >
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.worker.position}
                </Text>
                <Div center top={8}>
                  <Image
                    src="../../static/images/iconDate@3x.png"
                    style={{
                      marginRight: 8,
                      width: 18,
                      height: 18,
                    }}
                  />
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {followUpDetailModel.info &&
                    followUpDetailModel.info.worker.date
                      ? moment(followUpDetailModel.info.worker.date)
                          .locale('th')
                          .format('L')
                      : '-'}
                  </Text>
                </Div>
              </div>
            </Div>
          </Grid.Column>
          <Grid.Column>
            <Text
              fontWeight="bold"
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              Reviewer
            </Text>
            <Div top={8}>
              <Image
                src="../../static/images/user@3x.png"
                style={{
                  width: 32,
                  height: 32,
                  marginRight: 8,
                  marginTop: 2,
                }}
              />
              <div>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.reviewer.name}
                </Text>
                <Text
                  fontSize={14}
                  color={colors.textDarkBlack}
                  style={{ marginTop: -4 }}
                >
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.reviewer.position}
                </Text>
                <Div center top={8}>
                  <Image
                    src="../../static/images/iconDate@3x.png"
                    style={{
                      marginRight: 8,
                      width: 18,
                      height: 18,
                    }}
                  />
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {followUpDetailModel.info &&
                    followUpDetailModel.info.reviewer.date
                      ? moment(followUpDetailModel.info.reviewer.date)
                          .locale('th')
                          .format('L')
                      : '-'}
                  </Text>
                </Div>
              </div>
            </Div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Text
              fontWeight="bold"
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              Approver
            </Text>

            <Div top={8}>
              <Image
                src="../../static/images/user@3x.png"
                style={{
                  width: 32,
                  height: 32,
                  marginRight: 8,
                  marginTop: 2,
                }}
              />
              <div>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.approver.name}
                </Text>
                <Text
                  fontSize={14}
                  color={colors.textDarkBlack}
                  style={{ marginTop: -4 }}
                >
                  {followUpDetailModel.info &&
                    followUpDetailModel.info.approver.position}
                </Text>
                <Div center top={8}>
                  <Image
                    src="../../static/images/iconDate@3x.png"
                    style={{
                      marginRight: 8,
                      width: 18,
                      height: 18,
                    }}
                  />
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {followUpDetailModel.info &&
                    followUpDetailModel.info.approver.date
                      ? moment(followUpDetailModel.info.approver.date)
                          .locale('th')
                          .format('L')
                      : '-'}
                  </Text>
                </Div>
              </div>
            </Div>
          </Grid.Column>
          <Grid.Column>
            <Text
              fontWeight="bold"
              fontSize={sizes.s}
              color={colors.primaryBlack}
            >
              Division
            </Text>
            <Div top={8} center>
              <Image
                src="../../static/images/iconWork@3x.png"
                style={{
                  width: 32,
                  height: 32,
                  marginRight: 8,
                  marginTop: 2,
                }}
              />

              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {followUpDetailModel.info && followUpDetailModel.info.indicator}
              </Text>
            </Div>
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {`${followUpDetailModel.divEn || '-'} ${followUpDetailModel.shiftEn || ''}`}
            </Text>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </CardItem>
  ));
};

export default FollowUpInfoDetail;
