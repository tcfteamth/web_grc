import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { Grid } from 'semantic-ui-react';
import { useObserver, useLocalStore } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import {
  ButtonAll,
  ButtonBorder,
  DivisionDropdown,
  FollowUpReportStatusListDropdown,
  FollowUpYearListDropdown,
  ManageBuDropdown,
  ManageDepartmentDropdown,
  ManageDivisionDropdown,
  NewDates,
  Text,
  DropdownSelect
} from '../element';
import { initAuthStore } from '../../contexts';
import { taglistDropdownStore } from '../../model';

const CardFillter = styled.div`
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 24px 16px 24px 24px;
  margin-top: 16px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const objectiveList = [
  { label: 'Operations', text: 'Operations', value: 'Operation' },
  { label: 'Reporting', text: 'Reporting', value: 'Reporting' },
  { label: 'Compliance', text: 'Compliance', value: 'Compliance' },
];

const FollowUpDashboardContainerFilter = ({
  filterModel,
  followUpFilterListModel,
  activeTab,
  onSubmit,
  onClear,
  name,
}) => {
  const [tagsList, setTagList] = useState();
  const tagListDropdown = useLocalStore(() => taglistDropdownStore);

  const getTagList = async () => {
    try {
      const optionsResult = await tagListDropdown.getTagList(
        authContext.accessToken,
      );
      let list = []
      optionsResult.map((t) =>
        list.push({
          value: t.value,
          text: t.label,
          label: t.label,
      }))
      setTagList(list);
    } catch (e) {
      console.log(e);
    }
  };
  const authContext = initAuthStore();

  const handleProcessTagSelection = (e,data,field) => {
    let result = filterModel[field];
    if(data.action == 'select-option'){
      result.push(data.option)
    }
    else if(data.action == 'remove-value' || data.action == 'pop-value'){
      result = result.filter(x => x.value != data.removedValue.value)
    }
    else if(data.action == 'clear'){
      result = []
    }
    filterModel.setField(
      field,
      result,
    );
  }

  const handleSubmit = () => {
    onSubmit(
      authContext.accessToken,
      filterModel.selectedYear,
      filterModel.selectedBu,
      filterModel.selectedDepartment.no,
      filterModel.selectedDivision,
      filterModel.selectedProcessAnd,
      filterModel.selectedProcessOr,
      filterModel.selectedObjectives
    );
  };

  useEffect(() => {
    (async () => {
      try {
        await getTagList();
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    // clear filter status
    // เพราะ status ใน todo กับ summary ต่างกัน
    filterModel.setField('selectedStatus', '');
  }, [activeTab]);

  return useObserver(() => (
    <CardFillter className="no-print">
      <Div between>
        <Text fontWeight={'bold'} fontSize={sizes.s} color={'#00aeef'}>
          {name}
        </Text>
      </Div>
      <Grid>
        <Grid.Row>
          {followUpFilterListModel.filterList?.map((filter) => (
            <>
              {filter.filterName === 'BU' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    BU
                  </Text>
                  <ManageBuDropdown
                    placeholder="Select BU"
                    handleOnChange={(bu) => {
                      filterModel.setField('selectedBu', bu.value);

                      filterModel.setField('selectedDepartment', '');
                      filterModel.setField('selectedDivision', '');
                    }}
                    value={filterModel.selectedBu}
                    isDefaultValue={filter.isDefaultValue}
                    filterModel={filterModel}
                    disabled={filter.isDisable || filterModel.isBuDisabled}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Department' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Department
                  </Text>
                  <ManageDepartmentDropdown
                    placeholder="Select Department"
                    handleOnChange={(department) => {
                      filterModel.setField('selectedDepartment', department);

                      filterModel.setField('selectedDivision', '');
                    }}
                    value={filterModel.selectedDepartment.value}
                    selectedBu={filterModel.selectedBu}
                    isDefaultValue={filter.isDefaultValue}
                    filterModel={filterModel}
                    disabled={
                      !filterModel.selectedBu ||
                      filter.isDisable ||
                      filterModel.isDepartmentDisabled
                    }
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Division' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  {' '}
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Division
                  </Text>
                  <ManageDivisionDropdown
                    defaultValue="Select Division"
                    placeholder="Select Division"
                    handleOnChange={(divisionNo) => {
                      filterModel.setField('selectedDivision', divisionNo);

                      // if (authContext.roles.isAdmin) {
                      //   filterModel.resetDivisionMistake();
                      // }
                    }}
                    value={filterModel.selectedDivision}
                    selectedDepartmentId={filterModel.selectedDepartment.value}
                    disabled={!filterModel.selectedBu || filter.isDisable}
                    selectedBu={filterModel.selectedBu}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'year' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Year
                  </Text>
                  <FollowUpYearListDropdown
                    handleOnChange={(year) => {
                      filterModel.setField('selectedYear', year);
                    }}
                    value={filterModel.selectedYear}
                    defaultValue="Select Year"
                    placeholder="Select Year"
                    disabled={filter.isDisable}
                    model={filterModel}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Start Due Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Start Due Date
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    placeholder="Select Start Due Date"
                    value={filterModel.selectedStartDueDate}
                    onChange={(date) => {
                      filterModel.setField(
                        'selectedStartDueDate',
                        date[0].valueOf(),
                      );
                    }}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'End Due Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    End Due Date
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    placeholder="Select End Due Date"
                    value={filterModel.selectedEndDueDate}
                    onChange={(date) => {
                      filterModel.setField(
                        'selectedEndDueDate',
                        date[0].valueOf(),
                      );
                    }}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Start Close Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Start New Due Date/Close Date
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    placeholder="Select Start New Due Date/Close Date"
                    value={filterModel.selectedStartCloseDate}
                    onChange={(date) => {
                      filterModel.setField(
                        'selectedStartCloseDate',
                        date[0].valueOf(),
                      );
                    }}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'End Close Date' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    End New Due Date/Close Date
                  </Text>
                  <NewDates
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                    }}
                    placeholder="Select End New Due Date/Close Date"
                    value={filterModel.selectedEndCloseDate}
                    onChange={(date) => {
                      filterModel.setField(
                        'selectedEndCloseDate',
                        date[0].valueOf(),
                      );
                    }}
                  />
                </Grid.Column>
              )}

              {filter.filterName === 'Status' && (
                <Grid.Column
                  tablet={8}
                  computer={3}
                  mobile={16}
                  style={{ paddingLeft: 0 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Status
                  </Text>
                  <FollowUpReportStatusListDropdown
                    placeholder="Select Status"
                    handleOnChange={(status) => {
                      filterModel.setField('selectedStatus', status);
                    }}
                    value={filterModel.selectedStatus}
                  />
                </Grid.Column>
              )}
            </>
          ))}
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Process Tag (AND)
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Tag"
                options={tagsList}
                value={filterModel.selectedProcessAnd}
                isSearchable
                isMulti
                handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessAnd')}
              />
            </Div>
          </Grid.Column>
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Process Tag (OR)
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Tag"
                options={tagsList}
                value={filterModel.selectedProcessOr}
                isSearchable
                isMulti
                handleOnChange={ (e,data) => handleProcessTagSelection(e,data,'selectedProcessOr')}
              />
            </Div>
          </Grid.Column>
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            style={{ paddingLeft: 0 }}
          >
            {' '}
            <Text
              fontSize={sizes.xxs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Objective
            </Text>
            <Div items={'flex-Start'}>
              <DropdownSelect
                placeholder="Select Objective"
                options={objectiveList}
                value={filterModel.selectedObjectives}
                isSearchable
                handleOnChange={ (data) => filterModel.setField(
                  'selectedObjectives',
                  data,
                )
                }
              />
            </Div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column
            tablet={8}
            computer={3}
            mobile={16}
            verticalAlign="bottom"
            style={{ paddingLeft: 0 }}
          >
            <div style={{ display: 'flex' }}>
              <ButtonAll color="#1b1464" text="Filter" onClick={handleSubmit} />
              <ButtonBorder
                text="Clear"
                borderColor="#1b1464"
                handelOnClick={onClear}
              />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </CardFillter>
  ));
};

export default FollowUpDashboardContainerFilter;
