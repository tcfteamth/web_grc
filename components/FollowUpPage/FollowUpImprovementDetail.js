import React, { useState } from 'react';
import { Grid, TextArea } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import {
  Text,
  InputAll,
  ButtonBorder,
  NewDates,
  FollowUpDetailStatusDropdown,
  ButtonAdd,
  AttachFilesFollowUpModal,
} from '../element';
import styled from 'styled-components';
import { colors, convertFormatDate, sizes } from '../../utils';
import { followUpStatus } from '../../utils/followup/followup-status';

const CardTextArea = styled(TextArea)`
  alignitems: center;
  min-height: ${(props) => props.height || 80}px;
  width: ${(props) => props.width || 100}%;
  width: 100% !important;
  // border-color: #d9d9d6 !important;
  border-color: ${(props) =>
    props.error
      ? colors.red
      : props.type === 'comment'
      ? colors.textBlack
      : colors.textGrayLight}!important;
  border-width: 1px;
  border-radius: 6px 6px 0px 6px !important;
  padding: 8px 16px;
  font-size: ${sizes.xs}px !important;
  font-family: 'Prompt', 'Roboto', sans-serif !important;
`;

const index = ({ followUpId, followUpDetailModel }) => {
  const [activeAttachFileModal, setActiveAttachFileModal] = useState(false);

  console.log(followUpDetailModel)

  return useObserver(() => (
    <Grid style={{ margin: 0, width: '100%' }} columns="equal">
      <Grid.Row>
        <Text fontSize={sizes.s} color={colors.textPurple} fontWeight="bold">
          Progress / Reason
        </Text>
      </Grid.Row>
      <Grid.Row>
        <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
          Progress / Reason
          <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
        </Text>
        {followUpDetailModel.canUpdate ? (
          <CardTextArea
            placeholder="Key Progress / Reason"
            value={followUpDetailModel.followUpDetail?.correctiveAction}
            onChange={(e) =>
              followUpDetailModel.followUpDetail.setField(
                'correctiveAction',
                e.target.value,
              )
            }
            disabled={!followUpDetailModel.canUpdate}
          />
        ) : (
          <div style={{ width: '100%' }}>
            <Text fontSize={sizes.xs} color={colors.primaryBlack}>
              {followUpDetailModel.followUpDetail?.correctiveAction ?? '-'}
            </Text>
          </div>
        )}
      </Grid.Row>
      <Grid.Row>
        <Grid.Column style={{ paddingLeft: 0 }}>
          <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
            Status
            <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
          </Text>
          {followUpDetailModel.canUpdate ? (
            <FollowUpDetailStatusDropdown
              followUpId={followUpId}
              handleOnChange={(e) => {
                followUpDetailModel.followUpDetail.setField('status', e.value);
                if(followUpDetailModel.followUpDetail.isPostpone){
                  followUpDetailModel.followUpDetail.setField('dueDate', null);
                  followUpDetailModel.followUpDetail.setField('postponeDate', followUpDetailModel.followUpDetail.savedPostpone);
                }
              }}
              value={followUpDetailModel.followUpDetail?.status}
              disabled={!followUpDetailModel.canUpdate}
            />
          ) : (
            <div style={{ width: '100%' }}>
              <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                {followUpDetailModel.followUpDetail?.status}
              </Text>
            </div>
          )}
        </Grid.Column>
        <Grid.Column style={{ paddingLeft: 0 }}>
          {followUpDetailModel.canUpdate &&
          followUpDetailModel.followUpDetail?.isCalendarDisabled ? (
            <>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Closed Date / New Due Date
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>
              <NewDates
                options={{
                  altFormat: 'd/m/Y',
                  altInput: true,
                  minDate: followUpDetailModel.followUpDetail.minDate,
                  maxDate: followUpDetailModel.followUpDetail.maxDate,
                }}
                onChange={(date) => {
                  if(followUpDetailModel.followUpDetail.isPostpone){
                    followUpDetailModel.followUpDetail.setField(
                      'postponeDate',
                      date[0] ? date[0].valueOf() : null,
                    );
                  }
                  else{
                    followUpDetailModel.followUpDetail.setField(
                      'dueDate',
                      date[0] ? date[0].valueOf() : null,
                    );
                  }
                }}
                value={ ( followUpDetailModel.followUpDetail?.isPostpone ? 
                  followUpDetailModel.followUpDetail?.postponeDate || followUpDetailModel.followUpDetail?.savedPostpone 
                  : followUpDetailModel.followUpDetail?.dueDate)}
                placeholder="Select Close/Due Date"
              />
            </>
          ) : (
            <div style={{ width: '100%' }}>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Closed Date / New Due Date
                {followUpDetailModel.followUpDetail?.status !==
                  followUpStatus.CANCELLED && (
                  <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span>
                )}
              </Text>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '38px',
                }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {followUpDetailModel.followUpDetail?.isPostpone ? 
                  (followUpDetailModel.followUpDetail?.postponeDate
                    ? convertFormatDate(
                        followUpDetailModel.followUpDetail.postponeDate,
                      )
                    : '-')
                  :
                  (followUpDetailModel.followUpDetail?.dueDate
                    ? convertFormatDate(
                        followUpDetailModel.followUpDetail.dueDate,
                      )
                    : '-')}
                </Text>
              </div>
            </div>
          )}
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column style={{ padding: '0px' }}>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              flexWrap: 'wrap',
              marginRight: '16px',
            }}
          >
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Text
                style={{ paddingRight: 12 }}
                fontSize={sizes.xs}
                color={colors.primaryBlack}
              >
                Document / Evidence
              </Text>
            </div>
            {followUpDetailModel.displayDocumentFiles?.map((doc) => (
              <ButtonAdd
                text={doc.title}
                textSize={sizes.xs}
                href={doc.filePath}
                disabled={!followUpDetailModel.canUpdate || doc.canUpdate}
                onClickRemove={() =>
                  followUpDetailModel.deleteDocumentFiles(doc.id, doc.type)
                }
              />
            ))}
          </div>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ padding: 0 }}>
        <Text fontSize={sizes.xxs} color={colors.red}>
          (Status Closed : Please attached supporting Document /Evidence)
        </Text>
      </Grid.Row>

      {followUpDetailModel.canUpdate && (
        <Grid.Row centered>
          <ButtonBorder
            text="Attach File"
            handelOnClick={() => setActiveAttachFileModal(true)}
            borderColor={colors.primary}
          />
        </Grid.Row>
      )}

      <AttachFilesFollowUpModal
        active={activeAttachFileModal}
        onClose={() => setActiveAttachFileModal(false)}
        onAttatchFiles={(title, files) => {
          console.log(files);
          followUpDetailModel.setFilesUpload(title, files);
        }}
      />
    </Grid>
  ));
};

export default index;
