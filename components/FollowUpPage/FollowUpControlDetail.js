import React, { useState } from 'react';
import { Image, Grid, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import {
  Text,
  ButtonAll,
  CheckBoxAll,
  InputAll,
  ModalGlobal,
  TextArea,
  Box,
} from '../element';
import { FollowUpControlMoreDetail } from '.';
// import {
//   EditDetailControlAssessment,
//   DetailControlAssessment,
//   CreateDetailControlAssessment,
// } from '..';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  item,
  mapping,
  isEdit,
  control,
  risk,
  assessmentDetailModel,
  followUpDetailModel,
}) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isRating, setIsRating] = useState();
  const [isRatingDefalue, setIsRatingDefalue] = useState();
  const [handelDelete, setHandelDelete] = useState(false);

  const handelRating = (value) => {
    if (value === 2 || value === 1) {
      followUpDetailModel.followUpDetail.setField('lv7IsEnhance', false);
    }
    followUpDetailModel.followUpDetail.setField('rateId', value);
  };

  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Column computer={2} style={{ padding: 0 }}>
          <div style={{ display: 'flex' }}>
            {mapping === true ? (
              <Popup
                wide="very"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  padding: 16,
                  marginLeft: -10,
                }}
                trigger={
                  <Image
                    height={18}
                    width={18}
                    style={{ marginRight: '8px' }}
                    src="/static/images/zing@3x.png"
                  />
                }
              >
                <Text
                  fontSize={sizes.xl}
                  color={colors.textSky}
                  style={{ lineHeight: '1.2' }}
                >
                  GC's standard
                </Text>
                <Text
                  fontSize={sizes.s}
                  color={colors.pink}
                  fontWeight="bold"
                  style={{ paddingBottom: 4 }}
                >
                  Control C1 (ID : IC-CSA-1)
                </Text>
                <Text color={colors.primaryBlack}>
                  Process Owner สามารถพิจารณา General Rick & Control
                  ของแต่ละกระบวนการ จาก Rick & Control Library
                  เป็นแนวทางในการประเมินความเสี่ยงเบื้องต้น
                </Text>
              </Popup>
            ) : (
              <Image
                height={18}
                width={18}
                style={{ marginRight: '8px' }}
                src="/static/images/zinggray@3x.png"
              />
            )}
            <div>
              <Text color={colors.pink} fontSize={sizes.s}>
                Control
              </Text>
              <Text fontSize={sizes.s} color={colors.pink} fontWeight="bold">
                {/* C{control.no}{' '} */}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>
            </div>
          </div>
        </Grid.Column>
        <Grid.Column computer={8} style={{ padding: 0 }}>
          <div style={{ width: '100%', wordWrap: 'break-word' }}>
            <div>
              <Text
                color={colors.primaryBlack}
                fontSize={sizes.s}
                fontWeight="med"
              >
                {followUpDetailModel.followUpDetail &&
                  followUpDetailModel.followUpDetail.lv7Name}
              </Text>
            </div>
          </div>
        </Grid.Column>
        <Grid.Column computer={6} style={{ padding: 0 }}>
          <Grid.Row>
            <Div between>
              <div
                style={{
                  backgroundColor: colors.primaryBackground,
                  width: '1px',
                  minHeight: '80px',
                  margin: '0px 16px',
                }}
              />
              <Div col>
                <Div center>
                  <Text
                    fontSize={sizes.xs}
                    color={colors.textDarkBlack}
                    fontWeight="med"
                  >
                    Control Rating
                  </Text>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginRight: -10,
                    }}
                    position="top right"
                    trigger={
                      <Image
                        width={18}
                        style={{ marginLeft: 8 }}
                        src="../../static/images/iconRemark@3x.png"
                      />
                    }
                  >
                    <Grid
                      columns="equal"
                      style={{ margin: 0, paddingRight: 16 }}
                    >
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Good
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - The control is appropriately designed and being
                            implemented as specified
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Fair
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Box>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - The control is designed but may not be adequate
                              and appropriate
                            </Text>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - The controls have been designed adequately but
                              not consistently or only partially effective
                            </Text>
                          </Box>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Poor
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - There is no controls assignedd for the risks
                            involved.
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Enahancement
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - Improve the control that already appropriately
                            designed and being implemented as specified to
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              be more effective{' '}
                            </span>
                            or
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              more efficiency{' '}
                            </span>
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Popup>
                </Div>
                {followUpDetailModel.followUpDetail && (
                  <>
                    <Div style={{ flexWrap: 'wrap' }}>
                      <ButtonAll
                        style={{ marginTop: 4 }}
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight="bold"
                        color={
                          followUpDetailModel.followUpDetail.rateId === 3
                            ? colors.green
                            : colors.btGray
                        }
                        text="Good"
                        onClick={() => handelRating(3)}
                        disabled
                      />
                      <ButtonAll
                        style={{ marginTop: 4 }}
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight="bold"
                        color={
                          followUpDetailModel.followUpDetail.rateId === 2
                            ? colors.yellow
                            : colors.btGray
                        }
                        text="Fair"
                        onClick={() => handelRating(2)}
                        disabled
                      />
                      <ButtonAll
                        style={{ marginTop: 4 }}
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight="bold"
                        color={
                          followUpDetailModel.followUpDetail.rateId === 1
                            ? colors.red
                            : colors.btGray
                        }
                        text="Poor"
                        onClick={() => handelRating(1)}
                        disabled
                      />
                    </Div>
                    <Div top={8}>
                      <Div>
                        <CheckBoxAll
                          checked={
                            followUpDetailModel.followUpDetail.lv7IsEnhance
                          }
                          onChange={(e) =>
                            followUpDetailModel.followUpDetail.setField(
                              'lv7IsEnhance',
                              !followUpDetailModel.followUpDetail.lv7IsEnhance,
                            )
                          }
                          disabled
                        />
                        <Text
                          fontSize={sizes.xs}
                          color={
                            isRatingDefalue === null
                              ? colors.btGray
                              : colors.textBlack
                          }
                          fontWeight="med"
                          style={{ paddingLeft: 16 }}
                        >
                          Enhancement
                        </Text>
                      </Div>
                    </Div>
                  </>
                )}
              </Div>
            </Div>
          </Grid.Row>
        </Grid.Column>

        {/* {isOpen && (
          <Grid.Row style={{ padding: '24px 0px 0px' }}>
            {followUpDetailModel && (
              <FollowUpControlMoreDetail
                followUpDetailModel={followUpDetailModel}
              />
            )}
          </Grid.Row>
        )} */}

        {/* <Grid.Row style={{ padding: '24px 0px 0px' }}>
          {followUpDetailModel.followUpDetail && (
            <FollowUpControlMoreDetail
              detailModel={followUpDetailModel.followUpDetail}
            />
          )}
        </Grid.Row> */}
      </Grid>

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title={'Delete'}
        content={'Do you want to delete control?'}
        onSubmit={() => {
          risk.deleteControl(control.fullPath);
          setHandelDelete(false);
        }}
        submitText={'YES'}
        cancelText={'NO'}
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default index;
