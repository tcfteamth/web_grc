import React, { useState } from 'react';
import { Image, Form } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import { Text, ModalGlobal, CardComment, CommentBox } from '../element';

import { initAuthStore } from '../../contexts';
import { FollowUpControlDetail, FollowUpRiskDetail } from '.';

const CardContener = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: 100%;
`;

const CardTop = styled.div`
  ${(props) => props.error && 'border: 2px solid red !important'};
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 110px;
  padding: 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  objects,
  assessmentDetailModel,
  followUpDetailModel,
  followUpId,
}) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isNewRisk, setIsNewRisk] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);

  const authContext = initAuthStore();

  return useObserver(() => (
    <div style={{ width: '100%', marginBottom: '24px' }}>
      <CardContener>
        <CardTop>
          <Div col style={{ paddingRight: 16 }}>
            <Div>
              <Image
                src="../../static/images/iconObjective-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective{' '}
                <span
                  style={{ fontSize: sizes.m, color: colors.primaryBackground }}
                >
                  *
                </span>
                :
              </Text>
              <div>
                <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                  {followUpDetailModel.followUpDetail &&
                    followUpDetailModel.followUpDetail.lv5Name}
                </Text>
              </div>
            </Div>
            <Div top={10} center>
              <Image
                src="../../static/images/iconList-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective Type
                <span
                  style={{ fontSize: sizes.m, color: colors.primaryBackground }}
                >
                  *
                </span>
                :
              </Text>

              <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                {followUpDetailModel.followUpDetail &&
                  followUpDetailModel.followUpDetail.lv5Type}
              </Text>
            </Div>
          </Div>
        </CardTop>
        <CardBody>
          {followUpDetailModel && (
            <FollowUpRiskDetail
              followUpDetailModel={followUpDetailModel}
              followUpId={followUpId}
            />
          )}
        </CardBody>
      </CardContener>

      <ModalGlobal
        open={handelDelete}
        title={'Delete'}
        content={'Do you want to delete objective and its risk /control?'}
        onSubmit={() => {
          assessmentDetailModel.deleteObject(objects.no);
          setHandelDelete(false);
        }}
        submitText={'YES'}
        cancelText={'NO'}
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default index;
