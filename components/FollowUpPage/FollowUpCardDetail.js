import React from 'react';
import { Image, Segment } from 'semantic-ui-react';
import styled from 'styled-components';
import moment from 'moment';
import { useObserver } from 'mobx-react-lite';
import { RoadmapNextYear, StatusAll, Text } from '../element';
import { colors, sizes } from '../../utils';

const CardItem = styled(Segment)`
  width: 100% !important;
  min-width: 100% !important;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 24px !important;
  flex-direction: column;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  @media only screen and (min-width: 768px) {
    min-height: 272px;
  }

  @media only screen and (min-width: 1024px) {
    min-height: 318px;
  }
  @media only screen and (min-width: 1300px) {
    min-height: 299px;
  }

  @media only screen and (min-width: 1440px) {
    min-height: 280px;
  }
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const FollowUpCardDetail = ({ followUpDetailModel }) => {
  const partnetText = followUpDetailModel.madeByPartner
    ? `- ${followUpDetailModel.madeByPartner}`
    : '';

  return useObserver(() => (
    <CardItem>
      <Text fontSize={sizes.xs} color={colors.textlightGray}>
        PTT Global Chemical Public Company Limited
      </Text>
      <Text fontSize={sizes.xs} color={colors.textlightGray}>
        Control Self-Assessment
      </Text>
      <Div between top={24}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {followUpDetailModel.roadMapImage ? (
            <Image
              src={followUpDetailModel.roadMapImage}
              style={{ width: 18, height: 18, marginRight: 8 }}
            />
          ) : (
            <RoadmapNextYear year={followUpDetailModel.roadMapType} />
          )}

          <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
            {followUpDetailModel.roadMapText}
          </Text>
          <div
            style={{
              margin: '0px 8px',
              height: 20,
              width: 1,
              backgroundColor: colors.btGray,
            }}
          />
          <Text color={colors.textlightGray} fontSize={sizes.xs}>
            Last Update :{' '}
            {moment(followUpDetailModel.createDate).locale('th').format('L')}
          </Text>
        </div>
      </Div>

      <Text
        style={{ lineHeight: 1, paddingTop: 24 }}
        fontWeight="bold"
        fontSize={sizes.xl}
        color={colors.primaryBlack}
      >
        {`${followUpDetailModel.no} ${followUpDetailModel.name} ${partnetText}`}
      </Text>
    </CardItem>
  ));
};

export default FollowUpCardDetail;
