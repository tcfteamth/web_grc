import React, { useState, useEffect } from 'react';
import { Image, Grid, Table, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import Files from 'react-files';
import { colors, sizes } from '../../utils';
import {
  Text,
  ButtonBorder,
  ButtonFile,
  ButtonAdd,
  InputAll,
  InputDropdown,
  DropdownAll,
  NewDate,
  NewDates,
  TextArea,
  DropdownSelect,
} from '../element';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;
const ButtonFileName = styled.div`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  padding: ${(props) => props.padding || '0px 8px 0px 16px'} !important;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-color: ${(props) => props.borderColor || '#eaeaea'} !important;
  border-style: solid !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-radius: 6px !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 32}px !important;
  opacity: ${(props) => (props.disabled ? 0.5 : '')};
`;

const WrapperControlDetail = styled.div`
  border: ${(props) => (props.error ? '1px red solid' : 'none')} !important;
  height: ${(props) => (props.error ? '36px' : 'auto')} !important;
  width: 100% !important;
  text-align: center !important;
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({
  control,
  isEdit,
  assessmentDetailModel,
  detailModel,
  followUpDetailModel,
}) => {
  const [stateDate, setStateDate] = useState();
  const [stateOnFocus, setStateOnFocus] = useState();
  const [showDate, setShowDate] = useState();
  const [benefit, setBenefit] = useState();
  const [screenWidth, setScreenWidth] = useState();
  const [errorMessage, setErrorMessage] = useState();

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, []);

  const renderControlClassification = () => {
    if (detailModel.lv7Format === 'Manual Control') {
      return (
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          Manual controls are performed by individuals. Outside of a system
          (e.g., approval, review, segregration of duties).
        </Text>
      );
    }

    if (detailModel.lv7Format === 'IT Dependent Control') {
      return (
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          Automated control are performed entirely by the computer system (e.g.,
          access right, credit limit in in processing system).
        </Text>
      );
    }

    if (detailModel.lv7Format === 'Automate Control') {
      return (
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          IT Dependent Control are performed by individuals and requires some
          level of system involvement.
        </Text>
      );
    }
  };

  const renderControlType = () => {
    if (detailModel.lv7Type === 'Preventive control') {
      return (
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          Preventive control - attempt to defer or stop an unwanted outcome
          before it happens (e.g., use of passwords, approval, policies,
          procedures).
        </Text>
      );
    }

    if (detailModel.lv7Type === 'Detective control') {
      return (
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          Detective control - attempt to detect errors or irregularities that
          may have already occurred (e.g., reconciliations, monitoring of actual
          expenses vs budget, prior periods, forecasts).
        </Text>
      );
    }
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid style={{ margin: 0 }} columns="equal">
        <Grid.Row style={{ padding: 0 }}>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      {renderControlType()}
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3} style={{ padding: 8 }}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      {renderControlClassification()}
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={4}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Information technology system{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginRight: -10,
                      }}
                      position="top right"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        e.g. SAP ARIBA SALEFORCE
                      </Text>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Executor{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row verticalAlign="top">
                <Table.Cell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {detailModel.lv7Type || '-'}
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {detailModel.lv7Format || '-'}
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {detailModel.lv7Frequency || '-'}
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell width={4}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {detailModel.lv7TechControl || '-'}
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {detailModel.lv7EmployeeController || '-'}
                    </Text>
                  </Div>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>
        <Grid.Column computer={screenWidth < 1367 ? 3 : 2}>
          <Text
            style={{ paddingRight: 12 }}
            fontSize={sizes.xs}
            color={colors.primaryBlack}
          >
            Document / Evidence
          </Text>
        </Grid.Column>
        <Grid.Column computer={screenWidth < 1367 ? 13 : 14}>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              wordWrap: 'break-word',
            }}
          >
            <div
              style={{
                width: '100%',
                minHeight: '28px',
              }}
            >
              <Text
                fontSize={sizes.s}
                color={colors.textPurple}
                fontWeight="bold"
              >
                {`${followUpDetailModel?.assessmentDocuments.title || '-'}`}
              </Text>
            </div>
          </div>

          <diV
            style={{
              marginTop: '8px',
              display: 'flex',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}
            className="click"
          >
            {followUpDetailModel.assessmentDocuments.files?.map((file) => (
              <div style={{ marginTop: '-2px' }}>
                <ButtonAdd
                  text={`${file.title || file.name}`}
                  textSize={sizes.xs}
                  href={file.filePath || false}
                  disabled
                />
              </div>
            ))}
          </diV>
        </Grid.Column>

        <Grid.Row style={{ paddingTop: 24 }}>
          <Text fontSize={sizes.s} color={colors.textPurple} fontWeight="bold">
            Enhancement / Corrective Action
          </Text>
        </Grid.Row>
        <Grid.Row>
          <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
            Observation and Finding{' '}
          </Text>
          <div style={{ width: '100%', display: 'flex' }}>
            <Text fontSize={sizes.xs} color={colors.primaryBlack}>
              {detailModel.initialRemark || '-'}
            </Text>
          </div>
        </Grid.Row>
        <Grid.Row>
          <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
            Enhancement/Corrective Action{' '}
          </Text>
          <div style={{ width: '100%', display: 'flex' }}>
            <Text fontSize={sizes.xs} color={colors.primaryBlack}>
              {detailModel.suggestion || '-'}
            </Text>
          </div>
        </Grid.Row>
        <Grid.Row columns={3}>
          <Grid.Column style={{ paddingLeft: 0 }}>
            <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
              Due Date{' '}
            </Text>
            <div style={{ width: '100%', display: 'flex' }}>
              <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                {detailModel.endDate
                  ? moment(detailModel.endDate).format('DD/MM/YYYY')
                  : '-'}
              </Text>
            </div>
          </Grid.Column>
          <Grid.Column style={{ paddingLeft: 0 }}>
            <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
              Co-Responsible Division
            </Text>
            <div style={{ width: '100%', display: 'flex' }}>
              <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                {followUpDetailModel.partnerName || '-'}
              </Text>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  ));
};

export default index;
