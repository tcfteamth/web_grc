import React, { useState } from 'react';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import { Text, OverAllRatingStatus, FollowUpStatus } from '../element';
import { colors, sizes } from '../../utils';

const CardTabel = styled.div`
  display: inline-block;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-bottom: ${(props) => props.bottom || 0}px;
  padding: 10px 16px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin-top: 0px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
  padding-left: ${(props) => props.left || 0}px;
`;

const RowFollowUpSummaryList = ({ list }) => {
  const [openTab, setOpenTab] = useState(true);

  return (
    <CardTabel bottom={4}>
      <TableBody span={12}>
        <Cell width={9} />
        <Cell width={60}>
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            color={colors.primaryBlack}
          >
            {list.assessmentName}
          </Text>
        </Cell>
        <Cell width={8}>
          <Popup
            trigger={
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {list.division}
              </Text>
            }
            content={`${list.divEn || '-'} ${list.shiftEn || ''}`}
            size="small"
          />
        </Cell>
        <Cell width={20} />
        <Cell width={4} right>
          <div
            className="click"
            style={{ paddingLeft: 8 }}
            onClick={() => setOpenTab(!openTab)}
          >
            {openTab ? (
              <Image width={18} src="/static/images/iconUp-blue.png" />
            ) : (
              <Image width={18} src="/static/images/iconDown.png" />
            )}
          </div>
        </Cell>
      </TableBody>
      {openTab && (
        <div>
          {list.summaryDTOS?.map((summary) => (
            <TableBody span={12}>
              <Cell width={8} center>
                {summary.partner && (
                  <Popup
                    trigger={
                      <Image
                        src="../../static/images/icons-followup-group-1-24-px@3x.png"
                        style={{ marginRight: 12, width: 24, height: 24 }}
                      />
                    }
                    content={summary.partnerName}
                  />
                )}
              </Cell>
              <Cell width={45}>
                <a href={`/followUp/followUpDetail?id=${summary.id}`}>
                  <Text
                    fontSize={sizes.xs}
                    color={colors.primaryBlack}
                    lineClamp={2}
                  >
                    {summary.controlName}
                  </Text>
                </a>
              </Cell>
              <Cell width={8} center>
                <Popup
                  trigger={
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {summary.division}
                    </Text>
                  }
                  content={`${summary.divEn || '-'} ${summary.shiftEn || ''}`}
                  size="small"
                />
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                  {summary.dueDate}
                </Text>
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                  {summary.lastUpdate}
                </Text>
              </Cell>
              <Cell width={8} center>
                <OverAllRatingStatus overAllstatus={summary.rateId} />
              </Cell>
              <Cell width={8} center>
                <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                  <FollowUpStatus status={summary.status} />
                </Text>
              </Cell>
            </TableBody>
          ))}
        </div>
      )}
    </CardTabel>
  );
};

export default RowFollowUpSummaryList;
