import React from 'react';
import styled from 'styled-components';
import { Grid, Image } from 'semantic-ui-react';
import { Text } from '../element';
import { colors, sizes } from '../../utils';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Card = styled.div`
    display: inline-block;
    min-height: 50px;
    width: ${(props) => props.widthCard || 32}%;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    padding: 20px 16px !important;
    flex-direction: row;
    display: flex;
    align-items: center;
    justify-content: ${(props) =>
      props.center
        ? 'center'
        : props.right
        ? 'flex-end'
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 1440px){
      width: ${(props) => (props.width / 12) * 100 || (2.3 / 12) * 100}%;
`;
const statusList = [
  { img: '../../static/images/overdue@3x.png' },
  { img: '../../static/images/iconAddFile@3x.png' },
  { img: '../../static/images/iconTime@3x.png' },
  { img: '../../static/images/iconClose@3x.png ' },
  { img: '../../static/images/complate@3x.png' },
  { img: '../../static/images/iconClose@3x.png ' },
];

const index = ({ followUpModel }) => {
  return (
    <Grid
      columns={5}
      padded="horizontally"
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'space-between',
      }}
    >
      {followUpModel.totalStatusCount.map((status, statusIndex) => (
        <Card>
          <Div between>
            <Div>
              <Image
                src={statusList[statusIndex].img}
                style={{ marginRight: 12, width: 24, height: 24 }}
              />
              <Text
                fontWeight="bold"
                fontSize={sizes.l}
                color={colors.textPurple}
              >
                {status.status}
              </Text>
            </Div>
            <Text fontWeight="bold" fontSize={26} color={colors.primaryBlack}>
              {status.count}
            </Text>
          </Div>
        </Card>
      ))}
    </Grid>
  );
};

export default index;
