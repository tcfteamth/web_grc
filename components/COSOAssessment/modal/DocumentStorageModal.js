import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal, Image } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import Router from 'next/router';
import Files from 'react-files';
import {
  Text,
  DropdownAll,
  RadioBox,
  InputAll,
  ButtonAll,
  ButtonBorder,
  NewDates,
  BottomBarCosoDocument,
  Cell
} from '../../element';
import { colors, sizes } from '../../../utils';
import request from '../../../services';
import { COSODocumentEvidenceList } from '../../COSOAssessment';
import { AttachFilesModal } from '../../COSOAssessment/modal';
import { initAuthStore } from '../../../contexts';
import { headersCOSODocument } from '../../../utils/static';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Section = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({ active, onClose, onAttatchFiles, assessmentData, onSave }) => {
  const authContext = initAuthStore();
  const [title, setTitle] = React.useState('');
  const [files, setFiles] = React.useState(null);
  const [errorMessage, setErrorMessage] = React.useState();
  const [activeAttachFileModal, setActiveAttachFileModal] = useState(false);
  const [screenWidth, setScreenWidth] = useState(null);

  useEffect(() => {
    if (screenWidth === null) {
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth),
      );
      setScreenWidth(window.screen.width);
    }
  }, [screenWidth]);

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
    onClearState();
  };

  const onModalAttatchFiles = () => {
    onAttatchFiles(title, files);
    onModalClose();
  };

  const onClearState = () => {
    setTitle('');
    setFiles(null);
  };

  const handleDelete = async (fileId) => {
    await assessmentData.deleteFilesFromStorage(authContext.accessToken,fileId);
  }

  const handleDownloadAll = async () => {
    await assessmentData.downloadAllFiles(authContext.accessToken);
  }

  return useObserver(() => (
    <ModalBox
      size="medium"
      open={active}
      onClose={onModalClose}
      closeOnDimmerClick
      onOpen={onModalOpen}
      style={{height:'80%'}}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Document Storage
        </Text>
      </div>
      <Section>
          <ButtonBorder
              textWeight="med"
              textSize={sizes.xs}
              width={50}
              text="Add"
              color={colors.backgroundPrimary}
              icon={'../../static/images/iconPlus@3x.png'}
              textColor={'#00aeef'}
              borderColor="#00aeef"
              handelOnClick={() => setActiveAttachFileModal(true)}
          />
      </Section>
      <Section style={{ marginTop: '24px' }}>
        <TableHeader>
          {headersCOSODocument.map(
            ({ render, key, width, widthMobile, mid }) => (
              <Cell
                key={key}
                width={screenWidth < 1024 ? widthMobile : width}
                center={mid}
              >
                <Div center mid={mid}>
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                  </Text>
                  {/* )} */}
                </Div>
              </Cell>
            ),
          )}
        </TableHeader>
      </Section>
      <Modal.Content scrolling style={{ margin:'0px',padding:'0px', maxHeight:'calc(100% - 220px)', marginBottom: '0px' }}>
        <COSODocumentEvidenceList assessmentData={assessmentData} handleDelete={handleDelete}
        />
      </Modal.Content>
      <BottomBarCosoDocument
        assessmentData={assessmentData}
        onModalClose={onClose}
        onModalSave={onSave}
        onDownloadAll={handleDownloadAll}
      />

      <AttachFilesModal
            active={activeAttachFileModal}
            onClose={() => setActiveAttachFileModal(false)}
            onAttatchFiles={onAttatchFiles}
        /> 
    </ModalBox>
  ));
};

export default index;
