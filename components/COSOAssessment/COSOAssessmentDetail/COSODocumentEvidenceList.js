import React, { useEffect, useState } from 'react';
import { useObserver } from 'mobx-react-lite';
import { Pagination, Grid, Icon, Image, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import {
  Text,
  ProgressBarStatusTran,
  Cell,
  TabScrollBar,
  BottomBarCosoDocument,
  ButtonBorder,
  CheckBoxAll,
} from '../../element';
import moment from 'moment';
import { colors, sizes } from '../../../utils';
import { headersCOSODocument } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const NewImage = styled(Image)`
  cursor: ${(props) => (props.disabled ? 'no-drop' : 'pointer')} !important;
`;

const COSODocumentEvidenceList = ({
  assessmentData,
  tab,
  resultSummary,
  onModalClose,
  handleDelete,
  handleSave,
  handleDownloadAll,
}) => {
  const authContext = initAuthStore();
  const [screenWidth, setScreenWidth] = useState(null);

  useEffect(() => {
    if (screenWidth === null) {
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth),
      );
      setScreenWidth(window.screen.width);
    }
  }, [screenWidth]);

  return useObserver(() => (
    <div style={{ width: '100%', overflow: 'hidden' }}>
      <Grid>
        {assessmentData.documentStorageFiles.map((item) => (
          <TableBody>
            <Cell width={screenWidth < 1024 ? 80 : 8} center className="click">
              <CheckBoxAll
                header
                status={false}
                onChange={() =>
                  assessmentData.selectedFileIds.indexOf(item.fileId) === -1
                    ? assessmentData.addTempFile(item)
                    : assessmentData.removeTempFile(item.fileId)
                }
                checked={
                  assessmentData.selectedFileIds.indexOf(item.fileId) !== -1
                }
              />
            </Cell>
            <Cell width={screenWidth < 1024 ? 350 : 35} left className="click">
              <a href={item.filePath} target="_blank">
                <Text
                  className="click"
                  fontSize={sizes.s}
                  color={colors.primaryBlack}
                >
                  {item.title}
                </Text>
              </a>
            </Cell>
            <Cell width={screenWidth < 1024 ? 100 : 10} center>
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {moment(item.createdDate).format('DD/MM/YYYY')}
              </Text>
            </Cell>

            <Cell width={screenWidth < 1024 ? 150 : 15} center>
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {item.createdBy}
              </Text>
            </Cell>
            {/* <Cell width={screenWidth < 1024 ? 70 : 7} center>
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {moment(item.lastUpdate).locale('th').format('L')}
              </Text>
            </Cell> */}
            <Cell width={screenWidth < 1024 ? 70 : 7} center>
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {item.humanFileSize || '-'}
              </Text>
            </Cell>
            <Cell width={screenWidth < 1024 ? 100 : 10} center>
              <div
                onClick={() => !item.canDelete
                    ? null
                    : handleDelete(item.fileId)
                }
              >
                <NewImage
                  src={'../../static/images/delete@3x.png'}
                  style={{ width: 18, height: 18 }}
                  disabled={ !item.canDelete }
                />
              </div>
            </Cell>
          </TableBody>
        ))}
      </Grid>
    </div>
  ));
};

export default COSODocumentEvidenceList;
