import React, { useState } from 'react';
import { Image, Grid, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonAll,
  CheckBoxAll,
  InputAll,
  ModalGlobal,
  TextArea,
} from '../../element';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ item }) => {
  return useObserver(() => <div />);
};

export default index;
