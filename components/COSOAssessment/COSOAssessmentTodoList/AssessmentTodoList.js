import React from 'react';
import { Image, Pagination, Grid, Icon, Popup } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import Link from 'next/link';
import moment from 'moment';
import _ from 'lodash';
import Router from 'next/router';
import {
  CheckBoxAll,
  Text,
  CardEmpty,
  InputAll,
  StatusAll,
  BottomBarCosoAssessment,
  Cell,
  TabScrollBar,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { headersCOSORoadmapStaff } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px !important;
  align-items: flex-start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentApproveList = ({ assessmentData }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();
  const submitToDm = async () => {
    try {
      const result = await assessmentData.sendCosoAssessmentToDmOrVp(
        authContext.accessToken,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        {assessmentData.assessments.length > 0 ? (
          <Div col>
            <TableHeader span={12}>
              {headersCOSORoadmapStaff.map(
                ({ render, key, width, widthMobile, mid }) => (
                  <Cell
                    key={key}
                    width={screenWidth < 1024 ? widthMobile : width}
                    center={mid}
                  >
                    {render === 'Accept' ? (
                      <Div col center>
                        <CheckBoxAll
                          header
                          status={render}
                          onChange={() => assessmentData.setCheckAll()}
                          checked={assessmentData.isChckedAll}
                          disabled={assessmentData.isCheckReadyDisabled}
                        />
                      </Div>
                    ) : (
                      <Div center mid={mid}>
                        <Text
                          center
                          fontSize={sizes.xs}
                          style={{ paddingLeft: render === 'Question' && 16 }}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                        </Text>
                      </Div>
                    )}
                  </Cell>
                ),
              )}
            </TableHeader>
            {assessmentData.assessments.map((item) => (
              <TableBody span={12}>
                <Div col>
                  <Div>
                    <Cell center width={screenWidth < 1024 ? 40 : 4}>
                      <CheckBoxAll
                        status="Accept"
                        onChange={() => {
                          item.submitAssessment.setRoadmapIds(item.id);
                        }}
                        checked={
                          item.submitAssessment.isSelected && item.readyTosubmit
                        }
                        disabled={!item.readyTosubmit}
                      />
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 460 : 46}>
                      <a
                        href={`/COSO_Assessment/COSOAssessmentDetailPage?id=${item.id}&assessmentNo=${item.no}&tab=${assessmentData.tab}`}
                      >
                        <Text
                          className="click"
                          fontSize={sizes.s}
                          color={colors.primaryBlack}
                        >
                          {`${item.no} ${item.name}`}
                        </Text>
                      </a>
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 100 : 10} center>
                      {item.roadmap === 'RE_ASSESSMENT' ? (
                        <Popup
                          wide="very"
                          style={{
                            borderRadius: '6px 6px 0px 6px',
                          }}
                          position="top center"
                          trigger={
                            <Image
                              src="../../static/images/re@3x.png"
                              style={{ width: 18, height: 18 }}
                            />
                          }
                          content={
                            <Text
                              fontWeight="bold"
                              fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                              color={colors.orange}
                            >
                              RE ASSESSMENT
                            </Text>
                          }
                        />
                      ) : item.roadmap === 'NEW_ASSESSMENT' ? (
                        <Popup
                          wide="very"
                          style={{
                            borderRadius: '6px 6px 0px 6px',
                          }}
                          position="top center"
                          trigger={
                            <Image
                              src="../../static/images/new.png"
                              style={{ width: 18, height: 18 }}
                            />
                          }
                          content={
                            <Text
                              fontWeight="bold"
                              fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                              color={colors.orange}
                            >
                              NEW ASSESSMENT
                            </Text>
                          }
                        />
                      ) : (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          -
                        </Text>
                      )}
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 100 : 10} center>
                      {item.readyTosubmit ? (
                        <Image
                          src="../../static/images/tick-active@3x.png"
                          style={{ width: 18, height: 18 }}
                        />
                      ) : (
                        <Image
                          src="../../static/images/tick@3x.png"
                          style={{ width: 18, height: 18 }}
                        />
                      )}
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 100 : 10} center>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {item.owner}
                      </Text>
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 100 : 10} center>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {moment(item.lastUpdate).locale('th').format('L')}
                      </Text>
                    </Cell>
                    <Cell width={screenWidth < 1024 ? 100 : 10} center>
                      <StatusAll child={item} />
                    </Cell>
                  </Div>
                  <Div col>
                    {item.children.map((i) => (
                      <Div style={{ paddingTop: 4 }}>
                        <Cell width={6} />
                        <Cell width={4}>
                          <Text fontSize={sizes.s} color={colors.primaryBlack}>
                            {i.no}
                          </Text>
                        </Cell>
                        <Cell width={90} left className="click">
                          <Text
                            fontSize={sizes.s}
                            color={colors.primaryBlack}
                            className="click"
                          >
                            {i.name}
                          </Text>
                        </Cell>
                      </Div>
                    ))}
                  </Div>
                </Div>
              </TableBody>
            ))}
          </Div>
        ) : (
          <CardEmpty style={{ marginTop: 16 }} textTitle="No Assessment" />
        )}
      </TabScrollBar>

      {assessmentData.totalPage > 1 && (
        <Grid style={{ paddingTop: 14 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getAssessmentCOSOTodoList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl2,
                assessmentData.filterModel.selectedRoadmapType,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift,
                assessmentData.filterModel.selectedStatus,
                assessmentData.filterModel.selectedArea,
                assessmentData.filterModel.selectedIsYear,
                assessmentData.filterModel.selectedOwner,
              )
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      <BottomBarCosoAssessment
        assessmentData={assessmentData}
        isSubmitToDmDisabled={assessmentData.isSubmitToDmDisabled}
        submitToDm={() => submitToDm(authContext.accessToken)}
        page="AssessmentTodoListStaff"
      />
    </div>
  ));
};

export default AssessmentApproveList;
