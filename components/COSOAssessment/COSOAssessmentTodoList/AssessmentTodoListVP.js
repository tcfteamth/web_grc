import React from 'react';
import { Pagination, Grid, Icon, Popup, Image } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import Router from 'next/router';
import { ACCEPT_ASSESSMENT_STATUS } from '../../../model/COSO/Assessment/AcceptAssessmentCOSOModel';
import {
  CheckBoxAll,
  Text,
  StatusAll,
  BottomBarCosoAssessment,
  RadioBox,
  InputAll,
  CardEmpty,
  Cell,
  TabScrollBar,
} from '../../element';

import { colors, sizes } from '../../../utils';
import { headersCOSOAssessmentVP } from '../../../utils/static';
import useWindowDimensions from '../../../hook/useWindowDimensions';
import { initAuthStore } from '../../../contexts';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px !important;
  align-items: flex-start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentApproveList = ({ assessmentData }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  const handleAcceptRoadmap = async () => {
    try {
      const result = await assessmentData.submitAcceptCosoRoadmaps(
        authContext.accessToken,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      {assessmentData.assessments.length > 0 ? (
        <div style={{ width: '100%' }}>
          <TabScrollBar pad={10}>
            <Div col>
              <TableHeader span={12}>
                {headersCOSOAssessmentVP.map(
                  ({ render, key, width, widthMobile, mid }) => (
                    <Cell
                      key={key}
                      width={screenWidth < 1024 ? widthMobile : width}
                      center={mid}
                    >
                      {render === 'Reject' || render === 'Accept' ? (
                        <Div col center>
                          <Text
                            fontSize={sizes.xxxs}
                            fontWeight="med"
                            color={
                              render === 'Reject' ? colors.red : colors.primary
                            }
                          >
                            {render}
                          </Text>
                          {render === 'Accept' && (
                            <CheckBoxAll
                              header
                              status="Accept"
                              onChange={() =>
                                assessmentData.setAllAcceptChecked()
                              }
                              checked={assessmentData.isAllAcceptChecked}
                            />
                          )}
                          {render === 'Reject' && (
                            <CheckBoxAll
                              header
                              status={render}
                              onChange={() =>
                                assessmentData.setAllRejectChecked()
                              }
                              checked={assessmentData.isAllRejectChecked}
                            />
                          )}
                        </Div>
                      ) : (
                        <Div mid={mid}>
                          <Text
                            center
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.textlightGray}
                            style={{ paddingLeft: render === 'Question' && 16 }}
                          >
                            {render}
                          </Text>
                        </Div>
                      )}
                    </Cell>
                  ),
                )}
              </TableHeader>
              {assessmentData.assessments.map((item) => (
                <TableBody span={12}>
                  <Div col>
                    <Div>
                      <Cell width={screenWidth < 1024 ? 40 : 4} center>
                        <RadioBox
                          checked={item.acceptAssessment.isSelectedAccept()}
                          onChange={(e) =>
                            item.acceptAssessment.setNo(
                              ACCEPT_ASSESSMENT_STATUS.accept,
                              item.id,
                            )
                          }
                        />
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 40 : 4} center>
                        <RadioBox
                          status="Reject"
                          checked={item.acceptAssessment.isSelectedReject()}
                          onChange={(e) =>
                            item.acceptAssessment.setNo(
                              ACCEPT_ASSESSMENT_STATUS.reject,
                              item.id,
                            )
                          }
                        />
                      </Cell>
                      <Cell
                        width={screenWidth < 1024 ? 380 : 38}
                        left
                        className="click"
                        style={{ paddingLeft: 16 }}
                      >
                        <a
                          href={`/COSO_Assessment/COSOAssessmentDetailPage?id=${item.id}&assessmentNo=${item.no}&tab=TODO`}
                        >
                          <Text
                            className="click"
                            fontSize={sizes.s}
                            color={colors.primaryBlack}
                          >
                            <span style={{ fontWeight: 'bold' }}>
                              {`${item.no} ${item.name}`}
                            </span>
                          </Text>
                        </a>
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        {item.roadmap === 'RE_ASSESSMENT' ? (
                          <Popup
                            wide="very"
                            style={{
                              borderRadius: '6px 6px 0px 6px',
                            }}
                            position="top center"
                            trigger={
                              <Image
                                src="../../static/images/re@3x.png"
                                style={{ width: 18, height: 18 }}
                              />
                            }
                            content={
                              <Text
                                fontWeight="bold"
                                fontSize={
                                  screenWidth <= 1366 ? sizes.xs : sizes.s
                                }
                                color={colors.orange}
                              >
                                RE ASSESSMENT
                              </Text>
                            }
                          />
                        ) : item.roadmap === 'NEW_ASSESSMENT' ? (
                          <Popup
                            wide="very"
                            style={{
                              borderRadius: '6px 6px 0px 6px',
                            }}
                            position="top center"
                            trigger={
                              <Image
                                src="../../static/images/new.png"
                                style={{ width: 18, height: 18 }}
                              />
                            }
                            content={
                              <Text
                                fontWeight="bold"
                                fontSize={
                                  screenWidth <= 1366 ? sizes.xs : sizes.s
                                }
                                color={colors.textpurplepink}
                              >
                                NEW ASSESSMENT
                              </Text>
                            }
                          />
                        ) : (
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            -
                          </Text>
                        )}
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {item.owner}
                        </Text>
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        <Popup
                          trigger={
                            <Text fontSize={sizes.s} color={colors.primaryBlack}>
                              {item.depDiv}
                            </Text>
                          }
                          content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                          size="small"
                        />
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {item.lastUpdate
                            ? moment(item.lastUpdate).format('DD/MM/YYYY')
                            : '-'}
                        </Text>
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 140 : 14} center>
                        <StatusAll child={item} />
                      </Cell>
                    </Div>
                    <Div col>
                      {item.children.map((i) => (
                        <Div style={{ paddingTop: 4 }}>
                          <Cell width={8} />
                          <Cell width={6} style={{ paddingLeft: 16 }}>
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                            >
                              {i.no}
                            </Text>
                          </Cell>
                          <Cell width={86} left className="click">
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                              className="click"
                            >
                              {i.name}
                            </Text>
                          </Cell>
                        </Div>
                      ))}
                    </Div>
                    {item.acceptAssessment.isSelectedReject() && (
                      <Div
                        style={{
                          paddingLeft: '9%',
                          margin: '8px 0px 0px',
                        }}
                      >
                        <InputAll
                          placeholder="กรุณาระบุเหตุผล"
                          handleOnChange={(e) =>
                            item.acceptAssessment.setRejectComment(
                              e.target.value,
                            )
                          }
                          value={item.acceptAssessment.rejectComment}
                          error={!item.acceptAssessment.rejectComment}
                        />
                      </Div>
                    )}
                  </Div>
                </TableBody>
              ))}
            </Div>
          </TabScrollBar>
        </div>
      ) : (
        <CardEmpty textTitle="No Assessment" />
      )}

      {assessmentData.totalPage > 1 && (
        <Grid style={{ paddingTop: 14 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getAssessmentCOSOTodoList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl2,
                assessmentData.filterModel.selectedRoadmapType,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift,
                assessmentData.filterModel.selectedStatus,
                assessmentData.filterModel.selectedArea,
                assessmentData.filterModel.selectedIsYear,
                assessmentData.filterModel.selectedOwner,
              )
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      <BottomBarCosoAssessment
        assessmentData={assessmentData}
        acceptOrRejectRoadmap={handleAcceptRoadmap}
        isAcceptDisabled={assessmentData.isAcceptOrRejectDisabled}
        page="COSOAssessmentVP"
      />
    </div>
  ));
};

export default AssessmentApproveList;
