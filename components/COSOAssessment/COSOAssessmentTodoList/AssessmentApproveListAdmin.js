import React, { useState } from 'react';
import { Image, Pagination, Grid, Icon, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import Router from 'next/router';
import { useObserver } from 'mobx-react-lite';
import {
  CheckBoxAll,
  Text,
  StatusAll,
  BottomBarCosoAssessment,
  CardEmpty,
  Cell,
  TabScrollBar,
} from '../../element';
import moment from 'moment';
import { colors, sizes } from '../../../utils';
import { headersCOSOAssessmentAdmin } from '../../../utils/static';
import useWindowDimensions from '../../../hook/useWindowDimensions';
import { initAuthStore } from '../../../contexts';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px !important;
  align-items: flex-start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentApproveList = ({ assessmentData }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();
  const submitToDm = async () => {
    try {
      const result = await assessmentData.sendCosoAssessmentToDmOrVp(
        authContext.accessToken,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      {assessmentData.assessments.length > 0 ? (
        <div style={{ width: '100%' }}>
          <TabScrollBar>
            <Div col>
              <TableHeader span={12}>
                {headersCOSOAssessmentAdmin.map(
                  ({ render, key, width, widthMobile, mid }) => (
                    <Cell
                      key={key}
                      width={screenWidth < 1024 ? widthMobile : width}
                      center={mid}
                    >
                      {render === 'Accept' ? (
                        <Div col center>
                          <Text
                            center
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primary}
                          >
                            submit
                          </Text>
                          <CheckBoxAll
                            header
                            status={render}
                            onChange={() => assessmentData.setCheckAll()}
                            checked={assessmentData.isChckedAll}
                            disabled={assessmentData.isCheckReadyDisabled}
                          />
                        </Div>
                      ) : (
                        <Div center mid={mid}>
                          <Text
                            center
                            fontSize={sizes.xs}
                            style={{
                              paddingLeft: render === 'Question' && 16,
                            }}
                            fontWeight="med"
                            color={colors.textlightGray}
                          >
                            {render}
                          </Text>
                        </Div>
                      )}
                    </Cell>
                  ),
                )}
              </TableHeader>
              {assessmentData.assessments.map((item) => (
                <TableBody span={12}>
                  <Div col>
                    <Div>
                      <Cell width={screenWidth < 1024 ? 40 : 4} center>
                        <CheckBoxAll
                          status="Accept"
                          onChange={() => {
                            item.submitAssessment.setRoadmapIds(item.id);
                          }}
                          checked={
                            item.submitAssessment.isSelected &&
                            item.readyTosubmit
                          }
                          disabled={!item.readyTosubmit}
                        />
                      </Cell>
                      <Cell
                        width={screenWidth < 1024 ? 430 : 43}
                        left
                        className="click"
                      >
                        <a
                          href={`/COSO_Assessment/COSOAssessmentDetailPage?id=${item.id}&assessmentNo=${item.no}&tab=TODO`}
                          rel="noopener noreferrer"
                        >
                          <Text
                            fontSize={sizes.s}
                            color={colors.primaryBlack}
                            className="click"
                          >
                            {`${item.no} ${item.name}`}
                          </Text>
                        </a>
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 80 : 8} center>
                        {item.readyTosubmit ? (
                          <Image
                            src="../../static/images/tick-active@3x.png"
                            style={{ width: 18, height: 18 }}
                          />
                        ) : (
                          <Image
                            src="../../static/images/tick@3x.png"
                            style={{ width: 18, height: 18 }}
                          />
                        )}
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        {item.roadmap === 'RE_ASSESSMENT' ? (
                          <Popup
                            wide="very"
                            style={{
                              borderRadius: '6px 6px 0px 6px',
                            }}
                            position="top center"
                            trigger={
                              <Image
                                src="../../static/images/re@3x.png"
                                style={{ width: 18, height: 18 }}
                              />
                            }
                            content={
                              <Text
                                fontWeight="bold"
                                fontSize={
                                  screenWidth <= 1366 ? sizes.xs : sizes.s
                                }
                                color={colors.orange}
                              >
                                RE ASSESSMENT
                              </Text>
                            }
                          />
                        ) : item.roadmap === 'NEW_ASSESSMENT' ? (
                          <Popup
                            wide="very"
                            style={{
                              borderRadius: '6px 6px 0px 6px',
                            }}
                            position="top center"
                            trigger={
                              <Image
                                src="../../static/images/new.png"
                                style={{ width: 18, height: 18 }}
                              />
                            }
                            content={
                              <Text
                                fontWeight="bold"
                                fontSize={
                                  screenWidth <= 1366 ? sizes.xs : sizes.s
                                }
                                color={colors.orange}
                              >
                                NEW ASSESSMENT
                              </Text>
                            }
                          />
                        ) : (
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            -
                          </Text>
                        )}
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        <Popup
                          trigger={
                            <Text fontSize={sizes.s} color={colors.primaryBlack}>
                              {item.depDiv}
                            </Text>
                          }
                          content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                          size="small"
                        />
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 100 : 10} center>
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {item.lastUpdate
                            ? moment(item.lastUpdate).format('DD/MM/YYYY')
                            : '-'}
                        </Text>
                      </Cell>
                      <Cell width={screenWidth < 1024 ? 150 : 15} center>
                        <StatusAll child={item} />
                      </Cell>
                    </Div>
                    <Div col>
                      {item.children.map((i, index) => (
                        <Div style={{ paddingTop: 4 }}>
                          <Cell width={7} />
                          <Cell width={4}>
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                            >
                              {i.no}
                            </Text>
                          </Cell>
                          <Cell width={89} left className="click">
                            <Text
                              fontSize={sizes.s}
                              color={colors.primaryBlack}
                              className="click"
                            >
                              {i.name}
                            </Text>
                          </Cell>
                        </Div>
                      ))}
                    </Div>
                  </Div>
                </TableBody>
              ))}
            </Div>
          </TabScrollBar>
        </div>
      ) : (
        <Grid.Row style={{ padding: 0 }}>
          <CardEmpty
            Icon="../../static/images/iconAssessment@3x.png"
            textTitle="No Assessment"
          />
        </Grid.Row>
      )}

      {assessmentData.totalPage > 1 && (
        <Grid style={{ paddingTop: 14 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getAssessmentCOSOTodoList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl2,
                assessmentData.filterModel.selectedRoadmapType,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift,
                assessmentData.filterModel.selectedStatus,
                assessmentData.filterModel.selectedArea,
                assessmentData.filterModel.selectedIsYear,
                assessmentData.filterModel.selectedOwner,
              )
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      <BottomBarCosoAssessment
        assessmentData={assessmentData}
        page="AssessmentTodoListAdmin"
        isSubmitToDmDisabled={assessmentData.isSubmitToDmDisabled}
        submitToDm={() => submitToDm(authContext.accessToken)}
      />
    </div>
  ));
};

export default AssessmentApproveList;
