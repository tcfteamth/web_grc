import React, { useEffect, useState } from 'react';
import { useObserver } from 'mobx-react-lite';
import { Pagination, Grid, Icon, Image, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import {
  Text,
  ProgressBarStatusTran,
  Cell,
  TabScrollBar,
  BottomBarCosoAssessment,  Remark
} from '../../element';
import moment from 'moment';
import { colors, sizes } from '../../../utils';
import { headersCOSOAssessmentSummary } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentApproveList = ({ assessmentData, tab, resultSummary }) => {
  const authContext = initAuthStore();
  const [screenWidth, setScreenWidth] = useState();

  useEffect(() => {
    window.addEventListener('resize', () => setScreenWidth(window.innerWidth));
    setScreenWidth(window.screen.width);
  }, [screenWidth]);

  const handelExport = async () => {
    try {
      // authContext.currentUser
      const bu = assessmentData.filterModel.selectedBu != null 
                  && assessmentData.filterModel.selectedBu != "" ? assessmentData.filterModel.selectedBu : authContext.roles.isAdmin ? "" :authContext.currentUser.bu;
      const dep = assessmentData.filterModel.selectedDepartment.no != null 
                  && assessmentData.filterModel.selectedDepartment.no != "" ? assessmentData.filterModel.selectedDepartment.no : authContext.roles.isAdmin ? "" :authContext.currentUser.depNo;
                  
      const result = await assessmentData.submitCOSOExportSummary(
        authContext.accessToken,
        assessmentData.searchAssessmentText,
        assessmentData.filterModel.selectedIsYear,
        assessmentData.filterModel.selectedLvl2,
        assessmentData.filterModel.selectedArea,
        assessmentData.filterModel.selectedRoadmapType,
        bu,
        dep,
        assessmentData.filterModel.selectedDivision,
        assessmentData.filterModel.selectedStatus,
        assessmentData.filterModel.selectedOwner,
      );
      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersCOSOAssessmentSummary.map(
              ({ render, key, width, widthMobile, mid }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={mid}
                >
                  <Div center mid={mid}>
                    {key === 'Remark' ? (
                      <Text
                        center
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                      >
                        {render}
                        <br />
                      </Text>
                    ) : (
                      <Text
                        center
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                      >
                        {render}
                      </Text>
                    )}
                  </Div>
                </Cell>
              ),
            )}
          </TableHeader>
          {assessmentData.assessmentsSummary.map((item) => (
            <TableBody>
              <Cell
                width={screenWidth < 1024 ? 380 : 38}
                left
                className="click"
              >
                <a
                  href={`/COSO_Assessment/COSOAssessmentDetailPage?id=${item.id}&assessmentNo=${item.no}&tab=${tab}`}
                >
                  <Text
                    className="click"
                    fontSize={sizes.s}
                    color={colors.primaryBlack}
                    style={{
                      paddingLeft: 16,
                    }}
                  >
                    {item.no} {item.name}
                  </Text>
                </a>
              </Cell>
              <Cell width={screenWidth < 1024 ? 100 : 10} center>
                {item.roadmap === 'RE_ASSESSMENT' ? (
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                    }}
                    position="right center" 
                    trigger={
                      <Image
                        src="../../static/images/re@3x.png"
                        style={{ width: 18, height: 18 }}
                      />
                    }
                    content={
                      <Text
                        fontWeight="bold"
                        fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                        color={colors.orange}
                      >
                        RE ASSESSMENT
                      </Text>
                    }
                  />
                ) : item.roadmap === 'NEW_ASSESSMENT' ? (
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                    }}
                    position="right center" 
                    trigger={
                      <Image
                        src="../../static/images/new.png"
                        style={{ width: 18, height: 18 }}
                      />
                    }
                    content={
                      <Text
                        fontWeight="bold"
                        fontSize={screenWidth <= 1366 ? sizes.xs : sizes.s}
                        color={colors.orange}
                      >
                        NEW ASSESSMENT
                      </Text>
                    }
                  />
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </Cell>
              <Cell
                width={screenWidth < 1024 ? 150 : 15}
                style={{ padding: '8px 32px 0px 16px' }}
              >
                <ProgressBarStatusTran data={item.summaryStatus} />
              </Cell>
              <Cell width={screenWidth < 1024 ? 100 : 10} center>
                <Popup
                  trigger={
                    <Text fontSize={sizes.s} color={colors.primaryBlack}>
                      {item.depDiv}
                    </Text>
                  }
                  content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                  size="small"
                />
              </Cell>

              <Cell width={screenWidth < 1024 ? 130 : 13} center>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {item.owner ? `${item.owner}` : '-'}
                </Text>
              </Cell>
              <Cell width={screenWidth < 1024 ? 70 : 7} center>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {moment(item.lastUpdate).locale('th').format('L')}
                </Text>
              </Cell>
              <Cell width={screenWidth < 1024 ? 70 : 7} center>
              <Remark remark={item.remark} />
              </Cell>
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {assessmentData.assessmentsSummary.length > 0 &&
        assessmentData.totalPageSummary > 1 && (
          <Grid style={{ paddingTop: 14 }}>
            <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
              <Pagination
                style={{
                  fontSize: sizes.xs,
                  fontWeight: 'bold',
                  borderColor: colors.backgroundSecondary,
                }}
                defaultActivePage={assessmentData.pageSummary}
                firstItem={null}
                lastItem={null}
                onPageChange={(e, { activePage }) => {
                  resultSummary(activePage);
                }}
                nextItem={{
                  content: <Icon name="angle right" />,
                  icon: true,
                }}
                prevItem={{
                  content: <Icon name="angle left" />,
                  icon: true,
                }}
                totalPages={assessmentData.totalPageSummary}
              />
            </Grid.Row>
          </Grid>
        )}
      <BottomBarCosoAssessment
        assessmentData={assessmentData}
        page="AssessmentSummaryAdmin"
        exportData={handelExport}
      />
    </div>
  ));
};

export default AssessmentApproveList;
