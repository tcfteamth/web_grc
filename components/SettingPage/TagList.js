import React, { useEffect } from 'react';
import { useObserver } from 'mobx-react-lite';
import { Icon, Grid, Pagination } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, Cell, CardEmpty } from '../element';
import { colors, sizes } from '../../utils';
import { headersDataTag } from '../../utils/static';
import { initAuthStore } from '../../contexts';
import { RowTagList } from '.';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const RoadMapAssessmentAdmin = ({ settingModel }) => {
  const authContext = initAuthStore();

  useEffect(() => {
    settingModel.getTagList(authContext.accessToken);
  }, []);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      {settingModel.tagList.length > 0 ? (
        <>
          <TableHeader>
            {headersDataTag.map(({ render, key, width, mid }) => (
              <Cell key={key} width={width} center={mid}>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="med"
                  color={colors.textlightGray}
                >
                  {render}
                </Text>
              </Cell>
            ))}
          </TableHeader>

          {settingModel.tagList.map((tag, index) => (
            <TableBody>
              <RowTagList tag={tag} index={index + 1} />
            </TableBody>
          ))}
        </>
      ) : (
        <CardEmpty textTitle="No Data" />
      )}

      {settingModel.totalPage > 0 && (
        <Grid>
          <Grid.Row
            style={{
              justifyContent: 'flex-end',
              marginRight: 13,
              marginTop: 14,
            }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={settingModel.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
                settingModel.getTagList(authContext.accessToken, activePage)
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={settingModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default RoadMapAssessmentAdmin;
