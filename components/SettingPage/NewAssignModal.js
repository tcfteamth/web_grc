import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import Router from 'next/router';
import {
  Text,
  InputAll,
  ButtonAll,
  ButtonBorder,
  UserDropdown,
  DropdownAll,
  ModalGlobal
} from '../element';
import { colors, sizes } from '../../utils';
import { ManualTableModel } from '../../model/SettingDatabase';
import request from '../../services';

import { initAuthStore } from '../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose }) => {

  const authContext = initAuthStore();
  const [title, setTitle] = React.useState('');
  const manualTableModel = useLocalStore(() => new ManualTableModel());
  const [buList, setBuList] = React.useState();
  const [departmentList, setDepartmentList] = React.useState();
  const [divisionList, setDivisionList] = useState();
  const [shiftList, setShiftList] = useState();
  const [isSelectDepartment, setSelectDepartment] = useState(false);
  const [isSelectDivision, setSelectDivision] = useState(false);
  const [isSelectShift, setSelectShift] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [createError, setCreateError] = useState();

  const onClearState = () => {
    setTitle('');
  };

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
  };
  const getBuList = async () => {
    
    await request.roadmapServices
      .getBUList(authContext.accessToken)
      .then((response) => {
        const buOptions = response.data.map((bu) => ({
          value: bu.bu,
          label: bu.buFull,
        }));

        setBuList(buOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getDepartmentList = async () => {
    await request.roadmapServices
      .getDepartmentList()
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
        }));

        setDepartmentList(deparmentOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getDepartmentById = async (id) => {
    await request.roadmapServices
      .getDepartmentByBu(id)
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
        }));

        setDepartmentList(deparmentOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getDivisionById = async (id) => {
    await request.roadmapServices
      .getDivisionById(id)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
        }));

        setDivisionList(divisionOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getShiftByDepartmentId = async (id) => {
    await request.roadmapServices
      .getShiftByDepartmentId(id)
      .then((response) => {
        const shiftOptions = response.data.map((shift) => ({
          value: shift.id,
          label: shift.nameEn,
        }));

        setShiftList(shiftOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  useEffect(() => {
    getBuList();
    getDepartmentList();
  }, []);
  const submitCreateNewAssign = async () => {
    const formData = new FormData();
    formData.append('userId', manualTableModel.selectedUser.id);
    if(manualTableModel.selectedBu){
    formData.append('bu', manualTableModel.selectedBu.value);
    }
    if(manualTableModel.selectedDepartment){
    formData.append('departmentId', manualTableModel.selectedDepartment.value);
    }
    if(manualTableModel.selectedDivision){
      formData.append('divisionId', manualTableModel.selectedDivision.value);
    }
    if(manualTableModel.selectedShift){
      formData.append('shiftId', manualTableModel.selectedShift.value);
    }
    formData.append('positionName', manualTableModel.positionName);
    formData.append('lvl', manualTableModel.lvl);

    await request.settingTaskServices
      .submitCreateManualAssign(authContext.accessToken,formData)
      .then((response) => {
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log('error', e);
        setCreateError(e.response.data.message);
        setIsShowModal(true);
        onClose(false);
      });
  };

  return useObserver(() => (
    <div>
    <ModalBox
      size="medium"
      open={active}
      onClose={onModalClose}
      closeOnDimmerClick
      onOpen={onModalOpen}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Create New Assign
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Grid columns="equal">
          <Grid.Row>
            <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Bu
                </Text>
                <DropdownAll
                  placeholder="Select"
                  options={buList && buList}
                  handleOnChange={(e) => {
                    manualTableModel.setField('selectedBu', e);
                    if (e.value) {
                      manualTableModel.setField('lvl',20);
                      manualTableModel.setField('positionName','Acting Senior Vice President');
                      setSelectDepartment(false);
                      setSelectDivision(false);
                      setSelectShift(false);
                      manualTableModel.setField('selectedDepartment',null);
                      manualTableModel.setField('selectedShift',null);
                      manualTableModel.setField('selectedDivision',null);
                      
                      getDepartmentById(e.value);
                    }
                  }}
                  value={manualTableModel.selectedBu}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
            <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Department
                </Text>
                <DropdownAll
                  placeholder="Select"
                  options={departmentList && departmentList}
                  handleOnChange={(e) => {
                    manualTableModel.setField('selectedDepartment', e);
                    if (e.value) {
                      manualTableModel.setField('lvl',30);
                      manualTableModel.setField('positionName','Acting Vice President');
                      setSelectDivision(false);
                      setSelectShift(false);
                      manualTableModel.setField('selectedShift',null);
                      manualTableModel.setField('selectedDivision',null);
                
                      getDivisionById(e.value);
                      getShiftByDepartmentId(e.value);
                    }
                  }}
                  value={manualTableModel.selectedDepartment}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
            <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Division
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                    manualTableModel.setField('selectedDivision', e);
                    if (e.value) {
                      manualTableModel.setField('lvl',40);
                      manualTableModel.setField('positionName','Acting Division Manager');
                      setSelectDivision(true);
                      setSelectShift(false);
                      manualTableModel.setField('selectedShift',null);
                    }
                    }}
                    options={divisionList && divisionList}
                    value={manualTableModel.selectedDivision}
                    isDisabled={isSelectShift}
                  />
                </Grid.Column>
                
            </Grid.Row>
            <Grid.Row>
            <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Shift
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                      manualTableModel.setField('selectedShift', e);
                    if (e.value) {
                      manualTableModel.setField('lvl',50);
                      manualTableModel.setField('positionName','Acting Division Manager');
                      setSelectDivision(false);
                      setSelectShift(true);
                      manualTableModel.setField('selectedDivision',null);
                    }
                    }}
                    options={shiftList && shiftList}
                    value={manualTableModel.selectedShift}
                    isDisabled={isSelectDivision}
                  />
                </Grid.Column>
            </Grid.Row>
          <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Employee Name
                </Text>
                <UserDropdown
                  handleOnChange={(user) => {
                    manualTableModel.setField('selectedUser', user);
                  }}
                  value={manualTableModel.selectedUser}
                  defaultValue="Select Employee "
                  placeholder="Select Employee "
                  page="manualAssign"
                  isMulti
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Position Name
                </Text>

                <Form>
                  <InputAll
                    key="position name"
                    placeholder="Enter position name"
                    value={manualTableModel.positionName}
                    maxLength={40}
                    disabled={true}
                  />
                </Form>
              </Grid.Column>
            </Grid.Row>

            
          </Grid>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onModalClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Create New Assign"
          onClick={() => submitCreateNewAssign()}
        />
      </div>
    </ModalBox>
    <ModalGlobal
      open={isShowModal}
      title="Create New Assign"
      content={createError}
      onClose={() => setIsShowModal(false)}
    />
    </div>
  ));
};

export default index;