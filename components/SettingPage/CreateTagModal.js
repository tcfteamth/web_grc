import React from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import Router from 'next/router';
import {
  Text,
  InputAll,
  ButtonAll,
  ButtonBorder,
  ProcessTagDropdown,
} from '../element';
import { colors, sizes } from '../../utils';
import { SettingTagModel } from '../../model/SettingDatabase';
import request from '../../services';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose }) => {
  const [title, setTitle] = React.useState('');
  const settingModel = useLocalStore(() => new SettingTagModel());

  const onClearState = () => {
    setTitle('');
  };

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
  };

  const submitCreateTag = async () => {
    const formData = new FormData();
    formData.append('name', settingModel.name);
    formData.append('pdmIds', settingModel.selectedProcessId);
    await request.settingTaskServices
      .submitCreateTag(formData)
      .then((response) => {
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log('error', e);
        onClose(false);
        Router.reload();
      });
  };

  return useObserver(() => (
    <ModalBox
      size="medium"
      open={active}
      onClose={onModalClose}
      closeOnDimmerClick
      onOpen={onModalOpen}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Create Tag
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Tag Name
                </Text>

                <Form>
                  <InputAll
                    key="file name"
                    placeholder="Enter file name"
                    value={settingModel.name}
                    maxLength={40}
                    handleOnChange={(e) =>
                      settingModel.setField('name', e.target.value)
                    }
                  />
                </Form>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Process Database
                </Text>
                <ProcessTagDropdown
                  handleOnChange={(process) => {
                    settingModel.onAddProcess(process);
                    settingModel.setField('selectedProcess', process);
                  }}
                  value={settingModel.selectedProcess}
                  defaultValue="Select Process "
                  placeholder="Select Process "
                  page="tag"
                  isMulti
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onModalClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Create Tag"
          disabled={!settingModel.name}
          onClick={() => submitCreateTag()}
        />
      </div>
    </ModalBox>
  ));
};

export default index;
