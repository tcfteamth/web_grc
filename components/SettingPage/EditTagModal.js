import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Router from 'next/router';
import { Grid, Form, Modal } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import {
  Text,
  InputAll,
  ButtonAll,
  ButtonBorder,
  ProcessTagDropdown,
  Cell,
} from '../element';
import { toJS } from 'mobx';
import { initAuthStore } from '../../contexts';
import { colors, sizes } from '../../utils';
import { headersDataTagList } from '../../utils/static';
import { SettingTagModel } from '../../model/SettingDatabase';
import loadingStore from '../../contexts/LoadingStore';
import { RowTagListEdit } from '.';
import request from '../../services';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableHeader = styled.div`
  background-color: ${colors.primary};
  float: left;
  width: 100% !important;
  min-height: 30px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  background-color: ${colors.backgroundPrimary};
  border: 1px solid ${colors.textBlack};
  min-height: 30px;
  margin: 12px 0px 0px;
  padding: 8px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const TableScroll = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: 100%;
  overflow-x: hidden;
  overflow-y: scroll;
  padding: 6px;
  margin: -6px;
`;

const index = ({ active, onClose, tag }) => {
  const settingModel = useLocalStore(() => new SettingTagModel());
  const authContext = initAuthStore();
  const [title, setTitle] = useState(tag.name);

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
    onClearState();
  };

  const submitUpdateTag = async (list) => {
    console.log('', authContext.accessToken, tag.id, title, list);
    await request.settingTaskServices
      .submitUpdateTag(authContext.accessToken, tag.id, title, list)
      .then((response) => {
        console.log(response);
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
        onClose(false);
        Router.reload();
      });
  };

  const checkUpdateTag = async () => {
    const processList = [];
    const submitList = settingModel.listProcessLv.concat(
      settingModel.selectedProcess,
    );
    await submitList.map((it) => {
      processList.push(it.id);
    });
    const list = [...new Set(processList)];
    await submitUpdateTag(list);
  };

  const onClearState = () => {
    setTitle('');
  };

  const initDataEdit = async () => {
    loadingStore.setIsLoading(true);
    await settingModel.getEditTagById(tag.id);
    await loadingStore.setIsLoading(false);
  };

  useEffect(() => {
    initDataEdit();
  }, []);

  useEffect(() => {
    settingModel.getTagList(authContext.accessToken);
  }, []);

  return useObserver(() => (
    <ModalBox
      size="medium"
      open={active}
      onClose={onModalClose}
      closeOnDimmerClick
      onOpen={onModalOpen}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Edit Tag
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Tag Name
                </Text>
                <Form>
                  <InputAll
                    key="file name"
                    placeholder="Enter file name"
                    value={title}
                    maxLength={40}
                    handleOnChange={(e) => {
                      setTitle(e.target.value);
                    }}
                  />
                </Form>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Process Database
                </Text>
                <ProcessTagDropdown
                  handleOnChange={(lvl3) => {
                    settingModel.setField('selectedProcess', lvl3);
                  }}
                  value={settingModel.selectedProcess}
                  defaultValue="Select Process "
                  placeholder="Select Process "
                  page="tag"
                  isMulti
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Process Database List
                </Text>

                <Div col>
                  {/* {settingModel.listProcessLv.map((tag, index) => ()} */}
                  <TableHeader>
                    {headersDataTagList.map(({ render, key, width, mid }) => (
                      <Cell key={key} width={width} center={mid}>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="med"
                          color={colors.primaryBackground}
                        >
                          {render}
                        </Text>
                      </Cell>
                    ))}
                  </TableHeader>
                  <TableScroll style={{ height: 200 }}>
                    <Div col>
                      {settingModel.listProcessLv.map((tag, index) => (
                        <TableBody>
                          <RowTagListEdit
                            tag={tag}
                            setting={settingModel}
                            index={index + 1}
                          />
                        </TableBody>
                      ))}
                    </Div>
                  </TableScroll>
                </Div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onModalClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="SAVE EDIT"
          // disabled={!(files && title)}
          onClick={() => checkUpdateTag()}
        />
      </div>
    </ModalBox>
  ));
};

export default index;
