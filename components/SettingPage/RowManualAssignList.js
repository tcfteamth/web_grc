import React, { useState } from 'react';
import { useObserver, useLocalStore } from 'mobx-react-lite';
import Router from 'next/router';
import { Image } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, Cell, ModalGlobal } from '../element';
import { colors, sizes } from '../../utils';
import { SettingManualTableModel } from '../../model/ManualTable';
import { EditTagModal } from '.';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RoadMapAssessmentAdmin = ({ manualTable, index }) => {
  const [active, setActive] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const settingModel = useLocalStore(() => new SettingManualTableModel());

  const handleDeleteRow = async () => {
    await settingModel.submitDeleteManualAssign(manualTable.id);
    Router.reload();
    setConfirm(false);
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Div col>
        <Div style={{ paddingTop: 4 }}>
          <Cell width={8} center>
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {index}
            </Text>
          </Cell>
          <Cell width={40}>
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {manualTable.employeeId} - {manualTable.employeeName}
            </Text>
          </Cell>
          <Cell width={40} center>
          <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {manualTable.positionName}
            </Text>
          </Cell>
          <Cell width={8} center>
          <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {manualTable.level}
            </Text>
          </Cell>
          <Cell width={40} center>
          <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {manualTable.orgId} - {manualTable.orgName}
            </Text>
          </Cell>
          {/* <Cell width={8} center>
          <Div mid onClick={() => setActive(true)}>
              <Image
                className="click"
                src="../../static/images/edit@3x.png"
                style={{ width: 18, height: 18 }}
              />
            </Div>
          </Cell> */}
          <Cell width={8} center>
            <Div mid onClick={() => setConfirm(true)}>
              <Image
                className="click"
                src="../../static/images/delete@3x.png"
                style={{ width: 18, height: 18 }}
              />
            </Div>
          </Cell>
        </Div>
      </Div>

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to Delete ?"
        content="Please make sure to specify that you want to delete the information."
        onSubmit={handleDeleteRow}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
      {active && (
        <EditTagModal
          active={active}
          onClose={() => setActive(false)}
          tag={tag}
        />
      )}
    </div>
  ));
};

export default RoadMapAssessmentAdmin;
