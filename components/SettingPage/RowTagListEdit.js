import React, { useState } from 'react';
import { useObserver } from 'mobx-react-lite';
import { Image } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, Cell, ModalGlobal } from '../element';
import { colors, sizes } from '../../utils';
import { EditTagModal } from '.';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RoadMapAssessmentAdmin = ({ tag, setting }) => {
  const [active, setActive] = useState(false);
  const [confirm, setConfirm] = useState(false);

  const handleDeleteRow = async () => {
    const array = [];
    setting.listProcessLv.map((t) => {
      array.push(t.id);
    });
    const index = array.indexOf(tag.id);
    if (index > -1) {
      setting.listProcessLv.splice(index, 1);
    }
    setConfirm(false);
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Div col>
        <Div style={{ paddingTop: 4 }}>
          <Cell width={96}>
            <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
              {tag.fullPath} {tag.nameEn}
            </Text>
          </Cell>
          <Cell width={4} center>
            <Div mid onClick={() => setConfirm(true)}>
              <Image
                className="click"
                src="../../static/images/delete@3x.png"
                style={{ width: 16, height: 16 }}
              />
            </Div>
          </Cell>
        </Div>
      </Div>

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to Delete ?"
        content="Please make sure to specify that you want to delete the information."
        onSubmit={handleDeleteRow}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
      {active && (
        <EditTagModal
          active={active}
          onClose={() => setActive(false)}
          tag={tag}
        />
      )}
    </div>
  ));
};

export default RoadMapAssessmentAdmin;
