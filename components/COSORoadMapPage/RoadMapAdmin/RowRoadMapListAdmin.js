import React, { useState, useEffect } from 'react';
import { useObserver } from 'mobx-react-lite';
import Router from 'next/router';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import { CheckBoxAll, Text, ModalGlobal, Cell } from '../../element';
import { colors, sizes } from '../../../utils';
import { EditRoadmapModal } from '../modal';
import useWindowDimensions from '../../../hook/useWindowDimensions';
import { initAuthStore } from '../../../contexts';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowRoadMapListAdmin = ({ roadmap }) => {
  // const roadmapModel = new RoadmapCOSOModel();
  const authContext = initAuthStore();
  const [active, setActive] = useState(false);
  const [confirm, setConfirm] = useState(false);

  const { width: screenWidth } = useWindowDimensions();

  const handleCloseCreateRoadmapModal = (status) => {
    setActive(status);
  };

  const handleDeleteRow = () => {
    roadmap.submitDeleteRoadmap(roadmap.id);
    Router.reload();
    setConfirm(false);
  };

  const handleEditRow = async () => {
    setActive(true);
  };

  return useObserver(() => (
    <Div>
      <Div col>
        <Div>
          <Cell width={screenWidth < 1024 ? 40 : 4}>
            <CheckBoxAll
              status="Accept"
              onChange={(e) => {
                roadmap.submitRoadmaps.setRoadmapIds(roadmap.id);
              }}
              checked={
                roadmap.submitRoadmaps.isSelected &&
                roadmap.status !== 'WAIT_DM_ACCEPT'
              }
              disabled={roadmap.status === 'WAIT_DM_ACCEPT' ? true : false}
            />
          </Cell>
          <Cell width={screenWidth < 1024 ? 40 : 4} left>
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {roadmap.no}
            </Text>
          </Cell>
          <Cell
            width={screenWidth < 1024 ? 300 : 30}
            left
            style={{ paddingRight: 16 }}
          >
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {roadmap.name}
            </Text>
          </Cell>

          <Cell width={screenWidth < 1024 ? 100 : 10} center>
            <Popup
              trigger={
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {roadmap.depDiv}
                </Text>
              }
              content={`${roadmap.divEn || '-'} ${roadmap.shiftEn || ''}`}
              size="small"
            />
          </Cell>
          <Cell width={screenWidth < 1024 ? 140 : 14} center>
            <Text fontSize={sizes.s} color={colors.primaryBlack}>
              {roadmap.status}
            </Text>
          </Cell>
          <Cell width={10} center>
            <a
              href={roadmap.lastYearPath || false}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image
                src={
                  roadmap.lastYearPath
                    ? '../../static/images/file-purple@3x.png'
                    : '../../static/images/file@3x.png'
                }
                style={{ width: 18, height: 18 }}
                className={roadmap.lastYearPath ? 'click' : ''}
              />
            </a>
          </Cell>
          <Cell center width={screenWidth < 1024 ? 100 : 10}>
            <Popup
              style={{
                borderRadius: '6px 6px 0px 6px',
                padding: 16,
                marginRight: -10,
              }}
              position="top right"
              trigger={
                <div>
                  {roadmap.roadmap ? (
                    <Image
                      src="../../static/images/new.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : (
                    <Image
                      src="../../static/images/re.png"
                      style={{ width: 18, height: 18 }}
                    />
                  )}
                </div>
              }
            >
              <div>
                {roadmap.roadmap ? (
                  <Text fontSize={sizes.s} color={colors.primaryBlack}>
                    New Assessment
                  </Text>
                ) : (
                  <Text fontSize={sizes.s} color={colors.primaryBlack}>
                    Re Assessment
                  </Text>
                )}
              </div>
            </Popup>
          </Cell>

          <Cell width={screenWidth < 1024 ? 100 : 10} center>
            {roadmap.remark ? (
              <Popup
                wide={screenWidth >= 1024 && 'very'}
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  padding: 16,
                  marginRight: -10,
                }}
                position="right center" 
                trigger={
                  <Image
                    className="click"
                    width={18}
                    src="../../static/images/markpurple@3x.png"
                  />
                }
              >
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {roadmap.remark}
                </Text>
              </Popup>
            ) : (
              <Image width={18} src="../../static/images/markgray@3x.png" />
            )}
          </Cell>
          <Cell width={screenWidth < 1024 ? 40 : 4} center>
            {roadmap.status !== 'WAIT_DM_ACCEPT' ? (
              <div onClick={() => handleEditRow(roadmap.id)}>
                <Image
                  className="click"
                  src="../../static/images/edit@3x.png"
                  style={{ width: 18, height: 18 }}
                />
              </div>
            ) : (
              <Image
                disabled
                src="../../static/images/edit-gray@3x.png"
                style={{ width: 18, height: 18 }}
              />
            )}
          </Cell>
          <Cell width={screenWidth < 1024 ? 40 : 4} center>
            {roadmap.status !== 'WAIT_DM_ACCEPT' ? (
              <div onClick={() => setConfirm(true)}>
                <Image
                  className="click"
                  src="../../static/images/delete@3x.png"
                  style={{ width: 18, height: 18 }}
                />
              </div>
            ) : (
              <Image
                disabled
                src="../../static/images/delete@3x.png"
                style={{ width: 18, height: 18 }}
              />
            )}
          </Cell>
        </Div>
        <Div col>
          {roadmap.children.map((item, index) => (
            <Div style={{ paddingTop: 4 }}>
              <Cell width={screenWidth < 1024 ? 40 : 8} />
              <Cell width={screenWidth < 1024 ? 40 : 4} left>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {item.no}
                </Text>
              </Cell>
              <Cell width={screenWidth < 1024 ? 500 : 88} left>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {item.name}
                </Text>
              </Cell>
              {/* <Cell width={screenWidth < 1024 ? 100 : 10} center>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {roadmap.depDiv}
                </Text>
              </Cell>
              <Cell width={screenWidth < 1024 ? 140 : 14} center>
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  {roadmap.status}
                </Text>
              </Cell>
              <Cell width={screenWidth < 1024 ? 100 : 10} center>
                {roadmap.remark ? (
                  <Popup
                    wide={screenWidth >= 1024 && 'very'}
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginRight: -10,
                    }}
                    position="top right"
                    trigger={
                      <Image
                        className="click"
                        width={18}
                        src="../../static/images/markpurple@3x.png"
                      />
                    }
                  >
                    <Text fontSize={sizes.s} color={colors.primaryBlack}>
                      {roadmap.remark}
                    </Text>
                  </Popup>
                ) : (
                  <Image width={18} src="../../static/images/markgray@3x.png" />
                )}
              </Cell> */}
              {/* <Cell width={screenWidth < 1024 ? 40 : 4} center>
                {roadmap.status !== 'WAIT_DM_ACCEPT' ? (
                  <div onClick={() => handleEditRow(roadmap.id)}>
                    <Image
                      className="click"
                      src="../../static/images/edit@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  </div>
                ) : (
                  <Image
                    disabled
                    src="../../static/images/edit-gray@3x.png"
                    style={{ width: 18, height: 18 }}
                  />
                )}
              </Cell>
              <Cell width={screenWidth < 1024 ? 40 : 4} center>
                {roadmap.status !== 'WAIT_DM_ACCEPT' ? (
                  <div onClick={() => setConfirm(true)}>
                    <Image
                      className="click"
                      src="../../static/images/delete@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  </div>
                ) : (
                  <Image
                    disabled
                    src="../../static/images/delete@3x.png"
                    style={{ width: 18, height: 18 }}
                  />
                )}
              </Cell> */}
            </Div>
          ))}
        </Div>
        {roadmap.status === 'DM_REJECT' && (
          <Div
            style={{
              margin: '8px 0px 0px',
            }}
          >
            <Cell width={screenWidth < 1024 ? 80 : 8} />
            <Cell width={screenWidth < 1024 ? 100 : 10}>
              <Text fontSize={sizes.s} color={colors.red} fontWeight={'bold'}>
                Reject :
              </Text>
            </Cell>
            <Cell width={screenWidth < 1024 ? 820 : 82}>
              <Text
                fontSize={sizes.s}
                color={colors.red}
                style={{ paddingLeft: 8 }}
              >
                {roadmap.rejectComment}
              </Text>
            </Cell>
          </Div>
        )}
      </Div>
      {active && (
        <EditRoadmapModal
          active={active}
          onClose={() => handleCloseCreateRoadmapModal(false)}
          roadmap={roadmap}
          token={authContext.accessToken}
        />
      )}

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to Delete ?"
        content="Please make sure to specify that you want to delete the information."
        onSubmit={handleDeleteRow}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
    </Div>
  ));
};

export default RowRoadMapListAdmin;
