import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import Router from 'next/router';
import { Icon, Grid, Pagination } from 'semantic-ui-react';
import {
  CheckBoxAll,
  Text,
  BottomBarCOSORoadmap,
  Cell,
  TabScrollBar,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { headersCOSORoadmapAdmin } from '../../../utils/static';
import { initDataContext, initAuthStore } from '../../../contexts';
import { RowRoadMapListAdmin } from '..';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 8px !important;
`;

const CardList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: ${(props) =>
    props.line ? colors.primaryBackground : colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  padding: 16px !important;
  margin: 4px 0px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RoadMapAssessmentAdmin = ({ roadmapData }) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();

  const { width: screenWidth } = useWindowDimensions();

  const submitToDm = async (token) => {
    try {
      const result = await roadmapData.submitRoadmapsCosoToDM(
        authContext.accessToken,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handelExport = async () => {
    try {
      const result = await roadmapData.exportCOSOPageRoadmap(
        authContext.accessToken,
        1,
        roadmapData.searchRoadmapText,
        roadmapData.filterModel.selectedLvl2,
        roadmapData.filterModel.selectedBu,
        roadmapData.filterModel.selectedDepartment.no,
        roadmapData.filterModel.selectedDivision,
        roadmapData.filterModel.selectedShift,
        roadmapData.filterModel.selectedStatus,
        roadmapData.filterModel.selectedArea,
        roadmapData.filterModel.selectedIsYear,
      );
      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(
    () =>
      function cleanup() {
        dataContext.page = '';
      },
    [],
  );
  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar style={{ width: '100% ' }}>
        <Div col>
          <TableHeader>
            {headersCOSORoadmapAdmin.map(
              ({ render, key, width, widthMobile, mid }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={mid}
                >
                  {render === 'Accept' ? (
                    <CheckBoxAll
                      header
                      status={render}
                      onChange={() => roadmapData.checkedAll()}
                      checked={roadmapData.isChckedAll}
                    />
                  ) : (
                    <Div center mid={mid}>
                      {key === 'View' ? (
                        <Text
                          center
                          fontSize={sizes.xs}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                          <br />
                          <span>(Last Year)</span>
                        </Text>
                      ) : (
                        <Text
                          fontSize={sizes.xs}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                        </Text>
                      )}
                    </Div>
                  )}
                </Cell>
              ),
            )}
          </TableHeader>
          <TableBody>
            <Div col>
              {roadmapData.list.map((roadmap, index) => (
                <CardList line={index % 2 === 0}>
                  <RowRoadMapListAdmin roadmap={roadmap} />
                </CardList>
              ))}
            </Div>
          </TableBody>
        </Div>
      </TabScrollBar>

      {roadmapData.totalPage > 1 && (
        <Grid>
          <Grid.Row
            style={{
              justifyContent: 'flex-end',
              marginRight: 13,
              marginTop: 14,
            }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={roadmapData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
                roadmapData.getRoadmapCOSOList(
                  authContext.accessToken,
                  activePage,
                )
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={roadmapData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      <BottomBarCOSORoadmap
        page="roadmapAdminAess"
        submitToDm={() => submitToDm(authContext.accessToken)}
        isSubmitToDmDisabled={roadmapData.isSubmitToDmDisabled}
        roadmapData={roadmapData}
        handelExport={handelExport}
      />
    </div>
  ));
};

export default RoadMapAssessmentAdmin;
