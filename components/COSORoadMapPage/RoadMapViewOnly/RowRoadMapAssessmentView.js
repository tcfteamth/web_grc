import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import Router from 'next/router';
import { Image } from 'semantic-ui-react';
import { toJS } from 'mobx';
import {
  CheckBoxAll,
  Text,
  BottomBarRoadmap,
  AsessorDropdown,
  Remark,
  RadioBox,
  InputAll,
} from '../../element';
import { colors, sizes } from '../../../utils';

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
  padding-left: ${(props) => props.left || 0}px;
  align-items: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowRoadMapAssessmentDM = ({ roadmap }) => {
  const [screenWidth, setScreenWidth] = useState();
  const [isOpen, setIsOpen] = useState(true);

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, []);

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell width={98}>
          <Text fontWeight="med" fontSize={sizes.s} color={colors.primaryBlack}>
            {`${roadmap.no} ${roadmap.name}`}
          </Text>
        </Cell>
        <Cell center width={2}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>
      {roadmap.children.map((lvlFour) => (
        <Div>
          {isOpen && (
            <Div col>
              <Div style={{ padding: '16px 0px 0px' }}>
                <Cell width={50}>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {`${lvlFour.no} ${lvlFour.name}`}
                  </Text>
                </Cell>
                <Cell width={15} center mid>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {lvlFour.owner}
                  </Text>
                </Cell>
                <Cell width={7} center>
                  {lvlFour.hasLastYear ? (
                    <Image
                      src="../../static/images/markgray.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : lvlFour.hasLastYear === '2020' ? (
                    <Image
                      src="../../static/images/file-purple.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : (
                    <Image
                      src="../../static/images/file.png"
                      style={{ width: 18, height: 18 }}
                    />
                  )}
                </Cell>
                <Cell width={9} center>
                  <Remark remark={lvlFour.remark} />
                </Cell>
                <Cell width={9} center>
                  {lvlFour.roadMap === 'RE_ASSESSMENT' ? (
                    <Image
                      src="../../static/images/re.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : lvlFour.roadMap === 'NEW_ASSESSMENT' ? (
                    <Image
                      src="../../static/images/new.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : (
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {lvlFour.year}
                    </Text>
                  )}
                </Cell>
                <Cell center>
                  {lvlFour.isICAgentPlus && (
                    <Text fontSize={sizes.xs} color={colors.green}>
                      IC Agen+
                    </Text>
                  )}
                </Cell>
              </Div>
            </Div>
          )}
        </Div>
      ))}
    </Div>
  ));
};

export default RowRoadMapAssessmentDM;
