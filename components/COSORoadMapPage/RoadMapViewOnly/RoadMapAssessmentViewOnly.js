import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Grid } from 'semantic-ui-react';
import { Text, BottomBarCOSORoadmap, Cell } from '../../element';
import { colors, sizes } from '../../../utils';
import { RowRoadMapAssessmentView } from '..';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  min-height: 50px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  margin: 8px 13px;
  padding: 10px 24px !important;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const TableBody = TableHeader.extend`
  margin: 2px 13px 2px 13px;
  min-height: 62px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const headers = [
  { key: 'Assessment', render: 'Assessment Name', width: 50 },
  { key: 'Assessor', render: 'Assessor', width: 15 },
  { key: 'View', render: 'View CSA', width: 7 },
  { key: 'Remark', render: 'Scope of work', width: 9 },
  { key: 'Roadmap', render: 'Roadmap', width: 9 },
  { key: 'role', render: '', width: 10 },
];

const RoadMapAssessmentViewOnly = ({ roadmapData }) => {
  return (
    <div style={{ width: '100%' }}>
      <Grid style={{ marginTop: 12 }}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, center }) => (
            <Cell key={key} width={width} center={center}>
              <Div center mid={key !== 'Assessment'}>
                {key === 'View' ? (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                    <br />
                    <span>(Last Year)</span>
                  </Text>
                ) : (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                  </Text>
                )}
              </Div>
            </Cell>
          ))}
        </TableHeader>
        {roadmapData &&
          roadmapData.list.map((roadmap, index) => (
            <TableBody>
              <RowRoadMapAssessmentView roadmap={roadmap} />
            </TableBody>
          ))}
      </Grid>

      <BottomBarCOSORoadmap page="roadmapView" />
    </div>
  );
};

export default RoadMapAssessmentViewOnly;
