import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal, Dimmer, Loader } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import Router from 'next/router';
import {
  Text,
  DropdownAll,
  InputAll,
  ButtonBorder,
  ButtonAll,
  TextArea
} from '../../element';
import loadingStore from '../../../contexts/LoadingStore';
import { colors, sizes } from '../../../utils';
import request from '../../../services';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose, roadmap, token }) => {
  const [remarkDetail, setRemarkDetail] = useState(roadmap.remark);
  const [selectedYear, setSelectedYear] = useState();
  const [yearList, setYearList] = useState();
  const [selectedDepartment, setSelectedDepartMent] = useState();
  const [departmentList, setDepartmentList] = useState();
  const [divisionList, setDivisionList] = useState();
  const [selectedDivision, setSelectedDivision] = useState();
  const [lvlTwoList, setLvlTwoList] = useState();
  const [selectedLvlTwo, setSelectedLvlTwo] = useState();

  const initYear = () => {
    const getYear = new Date().getFullYear();
    const years = Array.from(new Array(10), (val, index) => getYear + index);
    const yearList = years.map((item) => ({
      value: new Date(`${item}`),
      label: item,
    }));
    setYearList(yearList);
    setSelectedYear(yearList.find((e) => e.label === roadmap.year));
  };

  const handleSelectedYear = (e) => {
    setSelectedYear(e);
  };

  const getDepDiv = async () => {
    await request.roadmapServices
      .getDepartmentList()
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
          departmentNo: department.departmentNo,
        }));

        setSelectedDepartMent(
          deparmentOptions.find((e) => e.departmentNo === roadmap.depDepNo),
        );
        setDepartmentList(deparmentOptions);
      })
      .catch((e) => {
        console.log(e);
      });

    await request.roadmapServices
      .getDivisionById(roadmap.depId)
      .then((response) => {
        const divisionOptions = response.data.map((div) => ({
          value: div.id,
          label: div.nameEn,
          divisionNo: div.no,
        }));

        setSelectedDivision(
          divisionOptions.find((e) => e.divisionNo == roadmap.divisionNo),
        );

        setDivisionList(divisionOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleSelectedDepartment = (e) => {
    setSelectedDepartMent(e);
    if (e.value) {
      getDivisionById(e.value);
    }
  };

  const getDivisionById = async (id) => {
    await request.roadmapServices
      .getDivisionById(id)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
          no: division.no,
        }));

        setDivisionList(divisionOptions);
        setSelectedDivision(
          divisionOptions.find((e) => e.no === roadmap.list.divisionNo),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleSelectedDivision = (e) => {
    setSelectedDivision(e);
    if (e.value) {
      getSubProcessList(e.no);
    }
  };

  const [subProcessList, setSubProcessList] = useState();

  const getSubProcessList = async () => {
    await request.roadmapCosoServices
      .getSubProcessList(roadmap.divisionNo)
      .then((response) => {
        const subProcessOptions = response.data.map((list) => ({
          value: list.csaPdcId,
          label: list.name,
        }));

        setSubProcessList(subProcessOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [selectedSub, setSelectedSub] = useState();

  const handleSelectedSub = (e) => {
    setSelectedSub(e);
  };

  const getLvlTwoList = async () => {
    await request.roadmapCosoServices
      .getLvlTwoList()
      .then((response) => {
        const lvlTwoOptions = response.data.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.no} ${lvlThree.name}`,
        }));
        setLvlTwoList(lvlTwoOptions);
        setSelectedLvlTwo(lvlTwoOptions.find((e) => e.value === roadmap.no));
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleSelectedLvlTwo = (e) => {
    setSelectedLvlTwo(e);
  };

  const submitUpdateRoadmap = async () => {
    let csaPdcIds = '';

    if (selectedSub != null) {
      await selectedSub.forEach((element) => {
        csaPdcIds = csaPdcIds + ',' + element.value;
      });
    }

    await request.roadmapCosoServices
      .updateRoadmapCoso(
        token,
        roadmap.id,
        selectedYear.label,
        selectedDivision.value,
        selectedLvlTwo.value,
        remarkDetail,
        csaPdcIds != '' ? csaPdcIds.slice(1) : null,
      )
      .then((response) => {
        console.log('Success');
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
        onClose(false);
        Router.reload();
      });
  };

  const isSubmitReady = () => {
    if (selectedYear && selectedDivision) {
      return false;
    }
    return true;
  };

  const handleClose = () => {
    onClose(false);
  };

  const initDataEdit = async () => {
    await roadmap.editRoadmapById(roadmap.id);
    setSelectedSub(roadmap.csaPdcs);
  };

  useEffect(() => {
    (async () => {
      try {
        loadingStore.setIsLoading(true);
        await initDataEdit();
        await getLvlTwoList();
        await getDepDiv();
        await initYear();
        await getSubProcessList();
        loadingStore.setIsLoading(false);
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  return useObserver(() => (
    <>
      {loadingStore.isLoading ? (
        <Dimmer active>
          <Loader />
        </Dimmer>
      ) : (
        <ModalBox
          size="medium"
          open={active}
          onClose={handleClose}
          style={{ zIndex: 1 }}
        >
          <Text fontSize={sizes.xxl} color={colors.textSky}>
            Edit RoadMap
          </Text>
          <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Year
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    options={yearList}
                    handleOnChange={(e) => {
                      handleSelectedYear(e);
                    }}
                    value={selectedYear}
                  />
                </Grid.Column>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Department
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    options={departmentList && departmentList}
                    handleOnChange={(e) => {
                      handleSelectedDepartment(e);
                    }}
                    value={selectedDepartment}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Division
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                      handleSelectedDivision(e);
                    }}
                    options={divisionList && divisionList}
                    value={selectedDivision}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Question
                  </Text>
                  <DropdownAll
                    placeholder="Select Question"
                    options={lvlTwoList}
                    value={selectedLvlTwo}
                    handleOnChange={(e) => handleSelectedLvlTwo(e)}
                  />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Remark
                  </Text>
                  <Form>
                    <TextArea
                      key="remark"
                      placeholder="Remark"
                      value={remarkDetail}
                      maxLength={4000}
                      onChange={(e) => {
                        setRemarkDetail(e.target.value);
                      }}
                    />
                  </Form>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column
                  computer={16}
                  tablet={16}
                  mobile={16}
                  style={{ paddingBottom: 8 }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Sub Process
                  </Text>
                  <DropdownAll
                    placeholder="Select Sub Process"
                    handleOnChange={(e) => handleSelectedSub(e)}
                    options={subProcessList && subProcessList}
                    value={selectedSub}
                    isMulti
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Modal.Content>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
            }}
          >
            <ButtonBorder
              width={140}
              style={{ marginRight: 20 }}
              handelOnClick={handleClose}
              borderColor={colors.textlightGray}
              textColor={colors.primaryBlack}
              textUpper
              textWeight="med"
              text="cancel"
            />
            <ButtonAll
              width={140}
              onClick={submitUpdateRoadmap}
              color={
                isSubmitReady() === 'disabled' ? colors.btGray : colors.primary
              }
              textColor={colors.backgroundPrimary}
              textUpper
              textWeight="med"
              text="Edit Now"
              disabled={isSubmitReady()}
            />
          </div>
        </ModalBox>
      )}
    </>
  ));
};

export default index;
