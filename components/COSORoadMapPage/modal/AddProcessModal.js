import React from 'react';
import styled from 'styled-components';
import { Modal, Text, TextArea } from '../../element';
import { colors, sizes } from '../../../utils';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const index = ({ active, onClose }) => (
  <Modal
    open={active}
    title="Request New Sub-Process"
    submitTextRight="SUBMIT"
    // isSubmitDisabled={isSubmitReady()}
    // onSubmit={submitCreateRoadmap}
    onClose={onClose}
    ContentComponent={() => (
      <div>
        <Text fontSize={sizes.s} color={colors.textDarkBlack}>
          Request New Sub-Process
        </Text>
        <TextArea height={45} placeholder="Fill scope of work" maxLength={30} />
      </div>
    )}
  />
);

export default index;
