import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import Router from 'next/router';
import {
  Text,
  DropdownAll,
  InputAll,
  ButtonAll,
  ButtonBorder,
  TextArea
} from '../../element';
import { initAuthStore } from '../../../contexts';
import { RoadmapCOSOModel } from '../../../model/COSO';
import { Modal } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import request from '../../../services';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose }) => {
  const roadmapModel = new RoadmapCOSOModel();
  const [remarkDetail, setRemarkDetail] = useState('');
  const [selectedYear, setSelectedYear] = useState();
  const [yearList, setYearList] = useState();
  const authContext = initAuthStore();
  const handleSelectedYear = (e) => {
    setSelectedYear(e);
  };

  const [departmentList, setDepartmentList] = useState();
  const [divisionList, setDivisionList] = useState();
  const [selectedDivision, setSelectedDivision] = useState();
  const [isSelectDivision, setSelectDivision] = useState(false);
  const [isSelectShift, setSelectShift] = useState(false);
  const [shiftList, setShiftList] = useState();
  const [selectedShift, setSelectedShift] = useState();
  
  const handleSelectedShift = (e) => {
    setSelectedShift(e);
    console.log('selectedShift', selectedShift);
    if(e.no){
      getShiftSubProcessList(e.no);
    }
    setSelectedDivision(null);
    setSelectShift(true);
    setSelectDivision(false);
  };
  const initYear = () => {
    const getYear = new Date().getFullYear();
    const years = Array.from(new Array(10), (val, index) => getYear + index);
    const yearList = years.map((item) => ({
      value: new Date(`${item}`),
      label: item,
    }));
    setYearList(yearList);
    setSelectedYear(yearList.find((e) => e.label === new Date().getFullYear()));
  };

  const getDepartmentList = async () => {
    await request.roadmapServices
      .getDepartmentList()
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
        }));

        setDepartmentList(deparmentOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getDepartmentList();
    initYear();
  }, []);

  const [selectedDepartment, setSelectedDepartMent] = useState();

  const handleSelectedDepartment = (e) => {
    setSelectedDepartMent(e);
    if (e.value) {
      getDivisionById(e.value);
      getShiftByDepartmentId(e.value);
    }
    setSelectDivision(false);
    setSelectShift(false);
    setSelectedShift(null);
    setSelectedDivision(null);
  };

  const getShiftByDepartmentId = async (id) => {
    await request.roadmapServices
      .getShiftByDepartmentId(id)
      .then((response) => {
        const shiftOptions = response.data.map((shift) => ({
          value: shift.id,
          no: shift.no,
          label: shift.nameEn,
        }));

        setShiftList(shiftOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getDivisionById = async (id) => {
    await request.roadmapServices
      .getDivisionById(id)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
          no: division.no,
        }));

        setDivisionList(divisionOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };


  const handleSelectedDivision = (e) => {
    setSelectedDivision(e);
    if (e.value) {
      getSubProcessList(e.no);
    }
    setSelectedShift(null);
    setSelectDivision(true);
    setSelectShift(false);
  };

  const [subProcessList, setSubProcessList] = useState();
  const getShiftSubProcessList = async (shift) => {
    await request.roadmapCosoServices
      .getShiftSubProcessList(shift, authContext.accessToken)
      .then((response) => {
        const subProcessOptions = response.data.map((list) => ({
          value: list.csaPdcId,
          label: list.name,
        }));

        setSubProcessList(subProcessOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getSubProcessList = async (division) => {
    await request.roadmapCosoServices
      .getSubProcessList(division, authContext.accessToken)
      .then((response) => {
        const subProcessOptions = response.data.map((list) => ({
          value: list.csaPdcId,
          label: list.name,
        }));

        setSubProcessList(subProcessOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [selectedSub, setSelectedSub] = useState();

  const handleSelectedSub = (e) => {
    setSelectedSub(e);
  };

  const [lvlTwoList, setLvlTwoList] = useState();

  const getLvlTwoList = async () => {
    await request.roadmapCosoServices
      .getLvlTwoList()
      .then((response) => {
        const lvlTwoOptions = response.data.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.no} ${lvlThree.name}`,
        }));

        setLvlTwoList(lvlTwoOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    // get two list
    getLvlTwoList();
  }, []);

  const [selectedLvlTwo, setSelectedLvlTwo] = useState();

  const handleSelectedLvlTwo = (e) => {
    setSelectedLvlTwo(e);
  };

  const submitCreateRoadmap = async () => {
    let str = '';

    if (selectedSub != null) {
      await selectedSub.forEach((element) => {
        str = str + ',' + element.value;
      });
    }
    
    const formData = new FormData();
    formData.append('year', selectedYear.label);
    if(selectedDivision){
      formData.append('divisionId', selectedDivision.value);
    }
    if(selectedShift){
      formData.append('shiftId', selectedShift.value);
    }
    formData.append('noLvl2', selectedLvlTwo.value);
    formData.append('remark', remarkDetail);
    if (str != '') {
      formData.append('csaPdcIds', str.slice(1));
    }

    await request.roadmapCosoServices
      .submitCreateRoadmap(formData)
      .then((response) => {
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log('error', e);
        onClose(false);
        Router.reload();
      });
  };

  const isSubmitReady = () => {
    if (selectedYear && (selectedDivision || selectedShift)) {
      return false;
    }

    return true;
  };

  const handleClose = () => {
    onClose(false);
  };

  return useObserver(() => (
    <ModalBox size="medium" open={active} onClose={handleClose}>
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Create RoadMap
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column
                computer={8}
                tablet={8}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Year
                </Text>
                <DropdownAll
                  placeholder="Select"
                  options={yearList}
                  handleOnChange={(e) => {
                    handleSelectedYear(e);
                  }}
                  value={selectedYear}
                />
              </Grid.Column>
              <Grid.Column
                computer={8}
                tablet={8}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Department
                </Text>
                <DropdownAll
                  placeholder="Select"
                  options={departmentList && departmentList}
                  handleOnChange={(e) => {
                    handleSelectedDepartment(e);
                  }}
                  value={selectedDepartment}
                />
              </Grid.Column>
              <Grid.Column
                computer={16}
                tablet={16}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Division
                </Text>
                <DropdownAll
                  placeholder="Select"
                  handleOnChange={(e) => {
                    handleSelectedDivision(e);
                  }}
                  options={divisionList && divisionList}
                  value={selectedDivision}
                  isDisabled={isSelectShift}
                />
              </Grid.Column>
              <Grid.Column   computer={16}
                tablet={16}
                mobile={16}
                style={{ paddingBottom: 8 }} >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Shift
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                      handleSelectedShift(e);
                    }}
                    options={shiftList && shiftList}
                    value={selectedShift}
                    isDisabled={isSelectDivision}
                  />
                </Grid.Column>
              <Grid.Column
                computer={16}
                tablet={16}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Question
                </Text>
                <DropdownAll
                  placeholder="Select Question"
                  options={lvlTwoList}
                  value={selectedLvlTwo}
                  handleOnChange={(e) => handleSelectedLvlTwo(e)}
                />
              </Grid.Column>

              <Grid.Column
                computer={16}
                tablet={16}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Remark
                </Text>
                <Form>
                  <TextArea
                    key="remark"
                    placeholder="Remark"
                    value={remarkDetail}
                    maxLength={4000}
                    onChange={(e) => {
                      setRemarkDetail(e.target.value);
                    }}
                  />
                </Form>
              </Grid.Column>
              <Grid.Column
                computer={16}
                tablet={16}
                mobile={16}
                style={{ paddingBottom: 8 }}
              >
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Sub Process
                </Text>
                <DropdownAll
                  placeholder="Select Sub Process"
                  handleOnChange={(e) => handleSelectedSub(e)}
                  options={subProcessList && subProcessList}
                  value={selectedSub}
                  isMulti
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Modal.Content>
      <Grid>
        <Grid.Row>
          <Grid.Column computer={3} tablet={4} mobile={8}>
            <ButtonBorder
              width={140}
              style={{ marginRight: 20 }}
              handelOnClick={handleClose}
              borderColor={colors.textlightGray}
              textColor={colors.primaryBlack}
              textUpper
              textWeight="med"
              text="cancel"
            />
          </Grid.Column>
          <Grid.Column computer={4} tablet={5} mobile={8}>
            <ButtonAll
              width={140}
              onClick={submitCreateRoadmap}
              color={
                isSubmitReady() === 'disabled' ? colors.btGray : colors.primary
              }
              textColor={colors.backgroundPrimary}
              textUpper
              textWeight="med"
              text="Create Now"
              disabled={isSubmitReady()}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </ModalBox>
  ));
};

export default index;
