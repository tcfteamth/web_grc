import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import { initAuthStore } from '../../../contexts';
import { assesorDropdownStore } from '../../../model';
import { ACCEPT_ROADMAP_STATUS } from '../../../model/AcceptRoadmapModel';
import { Text, AsessorDropdown, RadioBox, InputAll, Cell } from '../../element';
import { colors, sizes } from '../../../utils';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowRoadMapAssessmentDM = ({ roadmap }) => {
  const authContext = initAuthStore();
  const assesorDropdown = useLocalStore(() => assesorDropdownStore);
  const { width: screenWidth } = useWindowDimensions();
  const [listAssesor, setListAssesor] = useState([]);

  useEffect(() => {
    assesorDropdown.getAssesor(authContext.accessToken);
    const result = assesorDropdown.options.map((item) => ({
      value: item.value,
      label: item.text,
    }));
    setListAssesor(result);
  }, []);

  return useObserver(() => (
    <Div col>
      <Div>
        <Cell center width={screenWidth < 1024 ? 40 : 4}>
          <RadioBox
            checked={
              roadmap.acceptRoadmap.isSelectedAccept() &&
              roadmap.acceptRoadmap.userId !== 0
            }
            onChange={(e) =>
              roadmap.acceptRoadmap.setNo(
                ACCEPT_ROADMAP_STATUS.accept,
                roadmap.id,
              )
            }
            disabled={roadmap.acceptRoadmap.userId === 0}
          />
        </Cell>
        <Cell center width={screenWidth < 1024 ? 40 : 4}>
          <RadioBox
            status="Reject"
            checked={roadmap.acceptRoadmap.isSelectedReject()}
            onChange={(e) =>
              roadmap.acceptRoadmap.setNo(
                ACCEPT_ROADMAP_STATUS.reject,
                roadmap.id,
              )
            }
          />
        </Cell>
        <Cell width={screenWidth < 1024 ? 320 : 32}>
          <Text
            style={{ padding: '0px 16px' }}
            fontWeight="med"
            fontSize={sizes.s}
            color={colors.primaryBlack}
          >
            {` ${roadmap.no} ${roadmap.name}`}
          </Text>
        </Cell>
        <Cell width={screenWidth < 1024 ? 100 : 10}>
          <Popup
            trigger={
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {roadmap.depDiv}
              </Text>
            }
            content={`${roadmap.divEn || '-'} ${roadmap.shiftEn || ''}`}
            size="small"
          />
        </Cell>
        <Cell
          width={screenWidth < 1024 ? 250 : 25}
          style={{ paddingRight: 16 }}
        >
          <AsessorDropdown
            handleOnChange={(assesorId) => {
              roadmap.acceptRoadmap.setAssessor(assesorId);
            }}
            value={roadmap.acceptRoadmap.userId}
            disabled={roadmap.acceptRoadmap.isSelectedReject()}
            placeholder={
              screenWidth < 1024
                ? '- Select Person -'
                : '- Select Responsible person -'
            }
          />
        </Cell>
        <Cell width={10} center>
          <a
            href={roadmap.lastYearPath || false}
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image
              src={
                roadmap.lastYearPath
                  ? '../../static/images/file-purple@3x.png'
                  : '../../static/images/file@3x.png'
              }
              style={{ width: 18, height: 18 }}
              className={roadmap.lastYearPath ? 'click' : ''}
            />
          </a>
        </Cell>
        <Cell center width={screenWidth < 1024 ? 100 : 10}>
          <Popup
            style={{
              borderRadius: '6px 6px 0px 6px',
              padding: 16,
              marginRight: -10,
            }}
            position="top right"
            trigger={
              <div>
                {roadmap.roadmap ? (
                  <Image
                    src="../../static/images/new.png"
                    style={{ width: 18, height: 18 }}
                  />
                ) : (
                  <Image
                    src="../../static/images/re.png"
                    style={{ width: 18, height: 18 }}
                  />
                )}
              </div>
            }
          >
            <div>
              {roadmap.roadmap ? (
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  New Assessment
                </Text>
              ) : (
                <Text fontSize={sizes.s} color={colors.primaryBlack}>
                  Re Assessment
                </Text>
              )}
            </div>
          </Popup>
        </Cell>
        <Cell center width={screenWidth < 1024 ? 50 : 5}>
          {roadmap.remark ? (
            <Popup
              style={{
                borderRadius: '6px 6px 0px 6px',
                padding: 16,
                marginRight: -10,
              }}
              position="top right"
              trigger={
                <Image
                  className="click"
                  width={18}
                  src="../../static/images/markpurple@3x.png"
                />
              }
            >
              <Text fontSize={sizes.s} color={colors.primaryBlack}>
                {roadmap.remark}
              </Text>
            </Popup>
          ) : (
            <Image width={18} src="../../static/images/markgray@3x.png" />
          )}
        </Cell>
      </Div>
      <Div col>
        {roadmap.children.map((item) => (
          <Div style={{ paddingTop: 4 }}>
            <Cell center width={screenWidth < 1024 ? 40 : 4} />
            <Cell center width={screenWidth < 1024 ? 40 : 4} />
            <Cell
              width={screenWidth < 1024 ? 520 : 82}
              style={{ paddingLeft: 32 }}
            >
              <Text
                style={{ padding: '0px 16px' }}
                fontSize={sizes.s}
                color={colors.primaryBlack}
              >
                {` ${item.no} ${item.name}`}
              </Text>
            </Cell>
          </Div>
        ))}
      </Div>

      {roadmap.acceptRoadmap.isSelectedReject() && (
        <Div
          style={{
            paddingLeft: '9%',
            margin: '8px 0px 0px',
          }}
        >
          <InputAll
            placeholder="กรุณาระบุเหตุผล"
            handleOnChange={(e) =>
              roadmap.acceptRoadmap.setRejectComment(e.target.value)
            }
            value={roadmap.acceptRoadmap.rejectComment}
            error={!roadmap.acceptRoadmap.rejectComment}
          />
        </Div>
      )}
    </Div>
  ));
};

export default RowRoadMapAssessmentDM;
