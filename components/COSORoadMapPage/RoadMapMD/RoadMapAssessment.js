import React from 'react';
import styled from 'styled-components';
import { Icon, Grid, Pagination } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import Router from 'next/router';
import {
  CheckBoxAll,
  Text,
  BottomBarCOSORoadmap,
  Cell,
  TabScrollBar,
  AsessorDropdown,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { headersCOSORoadmapDM } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';
import { RowRoadMapAssessmentDM } from '..';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px !important;
  align-items: flex-start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const RoadMapAssessment = ({ roadmapData }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  const handleAcceptRoadmap = async () => {
    try {
      const result = await roadmapData.submitAcceptCosoRoadmaps(
        authContext.accessToken,
      );
      Router.reload();
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handelExport = async () => {
    try {
      const bu = authContext.currentUser.bu;
      const dep = authContext.currentUser.depNo;

      const result = await roadmapData.exportCOSOPageRoadmap(
        authContext.accessToken,
        1,
        roadmapData.searchRoadmapText,
        roadmapData.filterModel.selectedLvl2,
        bu,
        dep,
        roadmapData.filterModel.selectedDivision,
        roadmapData.filterModel.selectedShift,
        'WAIT_DM_ACCEPT',
        roadmapData.filterModel.selectedArea,
        roadmapData.filterModel.selectedIsYear,
      );
      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const renderHeader = () => (
    <TableHeader span={12}>
      {headersCOSORoadmapDM.map(
        ({ render, key, width, widthMobile, center }) => (
          <Cell key={key} width={screenWidth < 1024 ? widthMobile : width}>
            {render === 'Reject' || render === 'Accept' ? (
              <Div col center>
                <Text
                  fontSize={sizes.xxxs}
                  fontWeight="med"
                  color={render === 'Reject' ? colors.red : colors.primary}
                >
                  {render}
                </Text>
                {render === 'Accept' && (
                  <CheckBoxAll
                    header
                    status="Accept"
                    onChange={() => roadmapData.setAllAcceptChecked()}
                    checked={roadmapData.isAllAcceptChecked}
                    disabled={roadmapData.isCheckAssessorDisabled}
                  />
                )}
                {render === 'Reject' && (
                  <CheckBoxAll
                    header
                    status={render}
                    onChange={() => roadmapData.setAllRejectChecked()}
                    checked={roadmapData.isAllRejectChecked}
                  />
                )}
              </Div>
            ) : (
              <Div center mid={center}>
                {key === 'View' ? (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                    <br />
                    <span>(Last Year)</span>
                  </Text>
                ) : (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                    style={{ paddingLeft: render === 'Question' && 16 }}
                  >
                    {render}
                  </Text>
                )}
              </Div>
            )}
          </Cell>
        ),
      )}
    </TableHeader>
  );

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar style={{ zIndex: 10 }}>
        <Div col>
          {renderHeader()}
          {roadmapData.list.map((roadmap, index) => (
            <TableBody>
              <RowRoadMapAssessmentDM roadmap={roadmap} />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {roadmapData.totalPage > 1 && (
        <Grid>
          <Grid.Row
            style={{
              justifyContent: 'flex-end',
              marginRight: 13,
              marginTop: 14,
            }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={roadmapData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
                roadmapData.getRoadmapList(authContext.accessToken, activePage)
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={roadmapData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
      <BottomBarCOSORoadmap
        page="listAssessment"
        acceptOrRejectRoadmap={handleAcceptRoadmap}
        isAcceptDisabled={roadmapData.isAcceptOrRejectDisabled}
        handelExport={handelExport}
      />
    </div>
  ));
};

export default RoadMapAssessment;
