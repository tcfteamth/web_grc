import React, { useEffect } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { Text } from '../../element';
import { colors, sizes } from '../../../utils';
import { Pagination, Icon, Grid, Image } from 'semantic-ui-react';
import { initAuthStore, initDataContext } from '../../../contexts';
import { useObserver } from 'mobx-react-lite';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  min-height: 50px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  margin: 14px 13px 8px;
  padding: 10px 24px !important;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const TableBody = TableHeader.extend`
  margin: 2px 13px;
  min-height: 62px;
  padding: 4px 8px !important;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.center ? `center` : `flex-start`)};
  padding-left: ${(props) => props.left || 0}px;
  align-items: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const headers = [
  { key: 'No', render: 'No', width: 5, center: true },
  { key: 'Assessment Name', render: 'Scope of work', width: 85 },
  { key: 'RequestDate', render: 'Request Date', width: 10 },
];

const RoadMapRequeste = ({ requestModel }) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();

  useEffect(() => {
    return function cleanup() {
      dataContext.page = '';
    };
  }, []);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid style={{ marginTop: 12 }}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, center }) => (
            <Cell key={key} width={width} center={center} mid>
              <Text
                fontSize={sizes.xs}
                fontWeight="med"
                color={colors.textlightGray}
              >
                {render}
              </Text>
            </Cell>
          ))}
        </TableHeader>
        {requestModel &&
          requestModel.requests.map((request, index) => (
            <TableBody>
              <Cell center width={5}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {requestModel.page > 1
                    ? `${requestModel.page - 1}${index + 1}`
                    : index + 1}
                </Text>
              </Cell>
              <Cell width={85} style={{ paddingRight: 24 }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {request.detail}
                </Text>
              </Cell>
              <Cell width={10}>
                <Text
                  fontWeight="bold"
                  fontSize={sizes.xs}
                  color={colors.primaryBlack}
                >
                  {moment(request.createdDate).locale('th').format('L')}
                </Text>
              </Cell>
            </TableBody>
          ))}
      </Grid>

      {requestModel.totalPage > 1 && (
        <Grid>
          <Grid.Row
            style={{ justifyContent: 'flex-end', right: 13, marginTop: 12 }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={requestModel.page}
              onPageChange={(e, { activePage }) =>
                requestModel.getRequestList(authContext.accessToken, activePage)
              }
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={requestModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default RoadMapRequeste;
