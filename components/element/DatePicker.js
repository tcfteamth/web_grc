import React from 'react';
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const DatePicker = ({
  focused,
  date,
  onDateChange,
  handleFocusChange,
  displayFormat,
  placeholder,
}) => (
  <SingleDatePicker
    date={date} // momentPropTypes.momentObj or null
    onDateChange={onDateChange} // PropTypes.func.isRequired
    focused={focused} // PropTypes.bool
    onFocusChange={handleFocusChange} // PropTypes.func.isRequired
    displayFormat="DD/MM/YYYY"
    placeholder={placeholder}
    // showDefaultInputIcon
    isOutsideRange={() => false}
    showClearDate={true}
    // readOnly={readOnly}
    numberOfMonths={1}
  />
);

export default DatePicker;
