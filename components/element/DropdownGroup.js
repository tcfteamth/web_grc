import React, { useState, useEffect, useReducer } from 'react';
import { Dropdown, Menu, Image, Icon, Select } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const DropdownGroup = styled(Dropdown)`
  display: flex !important;
  flex-direction: row !important;
  align-items: center !important;
  border-radius: 6px 6px 0px 6px !important;
  width: ${(props) => (props.width ? `${props.width}px` : `98%`)}!important;
  height: ${(props) => (props.height ? props.height : 38)}px !important;
  font-size: ${sizes.xs}px !important;
  padding: 8px 16px !important;
  font-family: Roboto, sans-serif !important;
  text-align: ${(props) => (props.center ? 'center' : 'flex-start')} !important;
  align-self: center !important;
  font-weight: ${(props) =>
    props.fontWeight ? `${props.fontWeight}` : '500'} !important;
  color: ${(props) => props.color || colors.primaryBlack}!important;
  background-color: ${colors.backgroundPrimary} !important;
  box-shadow: ${(props) =>
    props.border ? 'none' : `0 0px 10px 0 rgba(48, 48, 48, 0.1)`} !important;
  border-style: solid !important;
  border-width: ${(props) => (props.border ? 2 : 0)}px !important;
  border-color: ${(props) => (props.border ? '#eaeaea' : 'none')} !important;
`;

const InputDropdown = ({
  color,
  options,
  textDefault,
  handleOnChange,
  border,
  width,
  height,
  value,
  style,
  fontWeight,
  disabled,
}) => (
  <DropdownGroup
    control={Select}
    button
    style={style}
    width={width}
    height={height}
    border={border || false}
    options={options}
    // text={textDefault}
    value={value}
    fontWeight={fontWeight}
    onChange={handleOnChange}
    placeholder={textDefault}
    color={color}
    disabled={disabled}
    icon={
      <span
        style={{
          right: 8,
          position: 'absolute',
        }}
      >
        <Image
          width={18}
          height={18}
          src="../../static/images/iconDown@3x.png"
        />
      </span>
    }
  />
);

export default InputDropdown;
