import React from "react";
import { Bubble } from "react-chartjs-2";
import { colors, sizes } from "../../utils";

const ChartBubble = ({ data }) => {
  const options = {
    maintainAspectRatio: true,
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }],
    },
    legend: {
      display: false,
    },
  };

  const dataBubble = {
    labels: ["January", "January"],
    datasets: [
      {
        label: "COMPLETE",
        barThickness: 30,
        data: ["1", "6", "", ""],
        backgroundColor: colors.textPurple,
      },
      {
        label: "CLOSED",
        barThickness: 24,
        data: ["2", "2", "", ""],
        backgroundColor: colors.greenMint,
      },
      {
        label: "OVERDUE",
        barThickness: 24,
        data: ["", "", "2", ""],
        backgroundColor: colors.red,
      },
      {
        label: "POSTPONE",
        barThickness: 24,
        pointHitRadius: 20,
        fill: false,
        data: [
          { x: 2, y: 10, r: 10 },
          { x: 5, y: 12, r: 10 },
        ],
        backgroundColor: colors.orange,
      },
      {
        label: "OPEN",
        barThickness: 24,
        data: [
          { x: 5, y: 10, r: 10 },
          { x: 5, y: 15, r: 10 },
        ],
        backgroundColor: colors.pink,
      },
    ],
  };

  return <Bubble data={dataBubble} options={options} />;
};

export default ChartBubble;
