import React, { useState, useEffect } from 'react';
import { useObserver } from 'mobx-react';
import { DropdownAll } from '.';

import request from '../../services';

const index = ({ value, handleOnChange, placeholder }) => {
  const [userList, setUserList] = useState();

  const getUserList = async () => {
    await request.roadmapServices
      .getUserList()
      .then((response) => {
        console.log('response', response);
        const options = response.data.map((user) => ({
          value: user.name,
          label: user.name,
          id: user.empId,
          empId: user.empId,
          name: user.name,
        }));
        setUserList(options);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getUserList();
  }, []);

  const getValue = () => {
    return userList.find((user) => user.value === value) || null;
  };

  return useObserver(() => (
    <>
      <DropdownAll
        placeholder={placeholder}
        defaultValue={userList}
        options={userList}
        handleOnChange={(e) => handleOnChange(e)}
        isMulti={false}
      />
    </>
  ));
};

export default index;
