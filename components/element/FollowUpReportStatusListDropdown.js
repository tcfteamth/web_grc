import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { followUpReportStatusListStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
  tab,
}) => {
  const followUpReportStatusList = useLocalStore(
    () => followUpReportStatusListStore,
  );
  const authContext = initAuthStore();
  const [statusList, setStatusList] = useState();

  const getStatusList = async () => {
    try {
      const optionsResult = await followUpReportStatusList.getFollowUpReportStatusList();

      setStatusList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getStatusList(tab);
  }, [tab]);

  const getValue = () => {
    return statusList.find((status) => status.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={defaultValue}
        options={statusList}
        value={statusList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
