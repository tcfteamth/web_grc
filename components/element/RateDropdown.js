import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { rateListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const rateListDropdown = useLocalStore(() => rateListDropdownStore);
  const authContext = initAuthStore();
  const [rateList, setRateList] = useState();

  const getRateList = async () => {
    try {
      const optionsResult = await rateListDropdown.getRateList(
        authContext.accessToken,
      );

      setRateList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRateList();
  }, []);

  const getValue = () => {
    return rateList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={rateList}
        options={rateList}
        value={rateList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
