import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text } from '.';
import { colors, sizes } from '../../utils';
import LoadingStore from '../../contexts/LoadingStore';

const index = () => {
  const loadingStore = useLocalStore(() => LoadingStore);

  return useObserver(() => (
    <Dimmer active={loadingStore.isLoading}>
      <Loader>
        <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
          Loading
        </Text>
      </Loader>
    </Dimmer>
  ));
};

export default index;
