import React from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import { Text } from '.';

const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-top: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px;
  width: 100%;
`;

const index = ({ commentDetail }) => {
  return useObserver(() => (
    <CardComment>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <div>
          <Text
            color={colors.primaryBlack}
            fontSize={sizes.xs}
            fontWeight="med"
          >
            {commentDetail.commentFrom}
          </Text>
        </div>
        <div
          style={{
            display: 'flex',
            fontStyle: 'italic',
          }}
        >
          <Text
            style={{ fontStyle: 'italic' }}
            color={colors.textlightGray}
            fontSize={sizes.xs}
          >
            By{' '}
          </Text>
          <Text
            color={colors.textlightGray}
            style={{
              paddingLeft: 8,
              fontStyle: 'italic',
            }}
            fontSize={sizes.xxs}
          >
            {commentDetail.commentBy}
          </Text>
        </div>
      </div>
      <Text color={colors.primaryBlack} fontSize={sizes.xs}>
        {commentDetail.comment}
      </Text>
    </CardComment>
  ));
};

export default index;
