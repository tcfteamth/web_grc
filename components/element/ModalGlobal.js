import React from 'react';
import { Modal, Button, Image } from 'semantic-ui-react';
import { Text } from '../../components/element';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';
import { ButtonBorder, ButtonAll, TextArea } from '../../components/element';

const CardAlert = styled.div`
  alignitems: center;
  min-height: 250px;
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  background-color: ${colors.backgroundPrimary};
  flex-direction: column !important;
  display: flex !important;
  justify-content: space-between !important;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const ModalGlobal = ({
  size,
  title,
  content,
  onClose,
  open,
  keyContent,
  onSubmit,
  submitText,
  cancelText,
  ContentComponent,
}) => {
  return (
    <Modal size="small" open={open} onClose={onClose}>
      <CardAlert>
        <Div col>
          <Text fontSize={sizes.xl} color={'#00aeef'}>
            {title || `หัวข้อ`}
          </Text>
          {content && (
            <Text
              style={{ paddingTop: 24 }}
              fontSize={sizes.s}
              color={'#666666'}
            >
              {content || `เนื้อหา`}
            </Text>
          )}
          {keyContent && <TextArea placeholder="Tell us more" width={80} />}
        </Div>
        <Div style={{ alignItems: 'flex-end', alignContent: 'flex-end' }}>
          {cancelText && (
            <ButtonBorder
              width={120}
              handelOnClick={onClose}
              borderColor="#b1b3aa"
              textColor={colors.primaryBlack}
              text={cancelText || 'Cancel'}
              textWeight="med"
              textUpper
            />
          )}
          {submitText && (
            <ButtonAll
              width={120}
              style={{ marginLeft: cancelText && 16 }}
              borderColor="#b1b3aa"
              textUpper
              textWeight="med"
              text={submitText || 'Confirm'}
              onClick={onSubmit}
            />
          )}
        </Div>
      </CardAlert>
    </Modal>
  );
};

export default ModalGlobal;
