import React from 'react';
import styled from 'styled-components';
import { Button, Image } from 'semantic-ui-react';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const AddButton = styled(Button)`
  display: flex !important;
  cursor: pointer !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  padding: 0px 24px !important;
  border-radius: ${(props) =>
    props.radius ? '6px 6px 6px 6px' : '6px 6px 0px 6px'} !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 40}px !important;
  border: ${(props) => props.error && '1px solid red !important'};
`;

const ButtonAll = ({
  text,
  color,
  width,
  height,
  textColor,
  textSize,
  textWeight,
  icon,
  iconSize,
  textUpper,
  radius,
  style,
  onClick,
  disabled,
  error,
}) => (
  <div>
    {icon ? (
      <AddButton
        style={style}
        color={color}
        height={height}
        width={width}
        radius={radius}
        onClick={onClick}
        disabled={disabled}
      >
        <Image
          src={icon}
          width={iconSize || 18}
          height={iconSize || 18}
          style={{ marginRight: 12 }}
        />
        <Text
          style={{ textTransform: textUpper ? 'uppercase' : 'none' }}
          color={textColor || colors.backgroundPrimary}
          fontSize={textSize}
          fontWeight={textWeight}
        >
          {text}
        </Text>
      </AddButton>
    ) : (
      <AddButton
        style={style}
        color={color}
        height={height}
        width={width}
        radius={radius}
        onClick={onClick}
        disabled={disabled}
        error={error}
      >
        <Text
          style={{
            textTransform: textUpper ? 'uppercase' : 'none',
            cursor: 'pointer',
          }}
          color={textColor || colors.backgroundPrimary}
          fontSize={textSize}
          fontWeight={textWeight}
        >
          {text}
        </Text>
      </AddButton>
    )}
  </div>
);

export default ButtonAll;
