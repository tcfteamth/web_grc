import React, { useEffect, useState } from 'react';
import { Checkbox, Image } from 'semantic-ui-react';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import {
  Text,
  ButtonBorder,
  ButtonAll,
  CardControl,
  InputAll,
} from '../../components/element';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: 100%;
`;
const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.blueBgLight};
  min-height: 110px;
  padding: 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const CardBody = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  border-radius: 0px 0px 0px 6px !important;
  display: inline-block;
  padding: ${(props) => props.padding || 0}px !important;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
`;

const CardDetail = styled.div`
  border-radius: 6px !important;
  border: solid 1px #d9d9d6;
  display: inline-block;
  padding: 0px !important;
  min-height: 160px;
  width: 100%;
  margin: 10px 0px;
  background-color: ${colors.backgroundPrimary};
  overflow: hidden;
`;

const Divider = styled.div`
  height: 2px;
  width: 100%;
  background-color: ${colors.primary};
  margin-top: 24px;
  margin-bottom: 8px;
`;
const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;
const DividerControl = styled.div`
  height: 1px;
  width: 100%;
  background-color: #d9d9d6;
  margin-top: 16px;
  margin-bottom: 8px;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  margin-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  min-height: ${(props) => props.height || 10}px;
  width: ${(props) => props.width || 100}%;
  align-items: center;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const index = ({ name, control }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isEdit, setIsEdit] = useState(false);
  const [isNewRisk, setIsNewRisk] = useState(false);
  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handleAddNewRisk = (value) => {
    setIsNewRisk(value);
  };
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  return (
    <CardAll>
      <CardTop>
        <Div col>
          <Div center>
            <Image
              src="../../static/images/iconObjective@3x.png"
              style={{
                width: 24,
                height: 24,
                marginRight: 8,
              }}
            />
            <Text
              color={colors.primaryBlack}
              fontWeight={'bold'}
              fontSize={sizes.s}
              style={{ marginRight: 8 }}
            >
              Objective:
            </Text>
            {isEdit ? (
              <InputAll
                placeholder={
                  'สร้างความเชื่อมั่นอย่างสมเหตุสมผลว่าบริษัทมีการควบคุมภายในที่เพียงพอเหมาะสม และปฏิบัติอย่างสม่ำเสมอ'
                }
              />
            ) : (
              <Text color={colors.primaryBlack} fontSize={sizes.s}>
                สร้างความเชื่อมั่นอย่างสมเหตุสมผลว่า
                บริษัทมีการควบคุมภายในที่เพียงพอเหมาะสม และปฏิบัติอย่างสม่ำเสมอ
              </Text>
            )}
          </Div>
          <Div top={10} center>
            <Image
              src="../../static/images/iconList@3x.png"
              style={{
                width: 24,
                height: 24,
                marginRight: 8,
              }}
            />
            <Text
              color={colors.primaryBlack}
              fontWeight={'bold'}
              fontSize={sizes.s}
              style={{ marginRight: 8 }}
            >
              Objective Type:
            </Text>
            {isEdit ? (
              <div style={{ width: '300px' }}>
                <InputDropdown placeholder="ด้านการดำเนินการ (Operations)" />
              </div>
            ) : (
              <Text color={colors.primaryBlack} fontSize={sizes.s}>
                ด้านการดำเนินการ (Operations)
              </Text>
            )}
          </Div>
        </Div>
        <div className="click" style={{ display: 'flex' }}>
          <Image
            src="/static/images/delete-gray@3x.png"
            height={24}
            width={24}
            style={{
              marginRight: 16,
            }}
          />
          <div onClick={() => handelEdit(!isEdit)}>
            {isEdit ? (
              <Image height={24} width={24} src="/static/images/edit@3x.png" />
            ) : (
              <Image
                height={24}
                width={24}
                src="/static/images/edit-gray@3x.png"
              />
            )}
          </div>
        </div>
      </CardTop>
      {/* <CardHeader>
        <Div>
          <Image
            src="../../static/images/iconObjective@3x.png"
            style={{
              width: 18,
              height: 18,
              marginRight: 8,
            }}
          />
          <Text color={colors.primaryBlack} fontWeight={"med"} fontSize={sizes.s}>
            <span style={{ fontWeight: "bold" }}>วัตถุประสงค์: </span>
            สร้างความเชื่อมั่นอย่างสมเหตุสมผลว่า
            บริษัทมีการควบคุมภายในที่เพียงพอเหมาะสม และปฏิบัติอย่างสม่ำเสมอ
          </Text>
        </Div>
        <Div top={6}>
          <Image
            src="../../static/images/iconList@3x.png"
            style={{ marginRight: 8, width: 18, height: 18 }}
          />
          <Text color={colors.primaryBlack} fontWeight={"med"} fontSize={sizes.s}>
            <span style={{ fontWeight: "bold" }}>ประเภทของวัตถุประสงค์: </span>
            ด้านการดำเนินการ (Operations)
          </Text>
        </Div>
      </CardHeader> */}

      <CardBody>
        <div style={{ padding: 24 }}>
          <Div between height={40}>
            <Div width={95}>
              <Div width={15}>
                <Text fontSize={sizes.s} color={colors.primary}>
                  ความเสี่ยงเลขที่
                  <span style={{ paddingLeft: 4, fontWeight: 'bold' }}>R1</span>
                </Text>
              </Div>
              <InputAll placeholder={'text'} />
              {/* <Text fontSize={sizes.s} color={colors.primary}>
                บริษัทไม่มีระบบการประเมินความเสี่ยงเพื่อป้องกันหรือลดข้อผิดพลาดที่อาจเกิดขึ้น
              </Text> */}
            </Div>

            <Div width={5} between style={{ paddingLeft: 8 }}>
              <div>
                <Image
                  height={18}
                  width={18}
                  style={{ marginRight: 8 }}
                  src="/static/images/delete-gray@3x.png"
                />
              </div>
              <div>
                <Image
                  height={18}
                  width={18}
                  src="/static/images/edit-gray@3x.png"
                />
              </div>
            </Div>
          </Div>

          <Div>
            <Image
              height={18}
              width={18}
              style={{ marginRight: 8 }}
              src="/static/images/zing@3x.png"
            />
            <Text fontSize={sizes.s} color={colors.textSky}>
              <span>GC’s standard </span>
              <span style={{ paddingLeft: 4, fontWeight: 'bold' }}>
                R1 (ID : IC-CSA-1)
              </span>
            </Text>
          </Div>
          <Divider />
          <div>
            <CardControl />

            <DividerControl />

            <CardControl />

            <Div mid top={16}>
              <ButtonAll
                height={32}
                textSize={sizes.xs}
                textColor={colors.primaryBlack}
                color={'#FFFFFF'}
                text={'REMOVE CONTROL'}
                icon={'/static/images/delete@2x.png'}
              />
              <div style={{ paddingLeft: 16 }}>
                <ButtonAll
                  height={32}
                  textSize={sizes.xs}
                  textColor={'#00aeef'}
                  color={'#FFFFFF'}
                  text={'ADD CONTROL'}
                  icon={'/static/images/iconPlus@3x.png'}
                />
              </div>
            </Div>
          </div>
        </div>
        <DividerLine />

        <div style={{ padding: 24 }}>
          <Div mid>
            <ButtonBorder
              width={230}
              height={40}
              fontSize={sizes.s}
              textColor={colors.textSky}
              borderColor={colors.textSky}
              textUpper
              textWeight={'med'}
              text={'Add Risk'}
              icon={'../../static/images/iconPlus@3x.png'}
            />
          </Div>
        </div>
      </CardBody>
    </CardAll>
  );
};

export default index;
