import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { roadmapTypeListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const roadmapTypeDropdown = useLocalStore(() => roadmapTypeListDropdownStore);
  const authContext = initAuthStore();
  const [roadmapTypeList, setRoadmapTypeList] = useState();

  const getRoadmapType = async () => {
    try {
      const result = await roadmapTypeDropdown.getRoadmapTypeList(
        authContext.accessToken,
      );

      setRoadmapTypeList(result);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRoadmapType();
  }, []);

  const getValue = () => {
    return roadmapTypeList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={roadmapTypeList}
        options={roadmapTypeList}
        value={roadmapTypeList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
