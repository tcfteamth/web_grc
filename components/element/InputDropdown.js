import React from 'react';
import { Dropdown, Menu, Image, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const DropdownStyle = styled(Dropdown)`
  display: flex !important;
  flex-wrap: wrap;
  flex-direction: row !important;
  align-items: center !important;
  width: 100% !important;
  font-family: 'Roboto', sans-serif !important;
  font-style: normal;
  min-height: ${(props) => props.height || 38}px !important;
  font-size: ${sizes.xs}px !important;
  font-weight: 500 !important;
  padding: 3px 16px !important;
  border-radius: 6px 6px 0px 6px !important;
  // border-color: 1px #eaeaea !important;
  border-width: 1px !important;
  border-color: ${(props) =>
    props.onError ? colors.red : colors.textGrayLight}!important;
  background-color: ${colors.backgroundPrimary} !important;
  color: ${colors.primaryBlack} !important;
`;

const InputDropdown = ({
  isfull,
  options,
  placeholder,
  selection,
  search,
  multiple,
  clearable,
  textDefault,
  handleOnChange,
  value,
  onError,
  width,
}) => (
  <DropdownStyle
    style={{
      fontSize: sizes.xs,
      minHeight: 38,
      padding: search ? 10 : 0,
      width: '100%',
    }}
    clearable={clearable || false}
    fluid={!isfull ? false : true}
    multiple={multiple || false}
    search={search || false}
    selection={selection || true}
    options={options}
    value={value}
    text={textDefault}
    placeholder={placeholder || true}
    onChange={handleOnChange}
    onError={onError}
    icon={
      <span
        style={{
          alignItems: 'center',
          right: 10,
          width: 20,
          height: 20,
          position: 'absolute',
        }}
      >
        <Image width={20} height={20} src="/static/images/iconDown.png" />
      </span>
    }
  />
);

export default InputDropdown;
