import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { yearListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
  filterModel,
}) => {
  const yearListDropdown = useLocalStore(() => yearListDropdownStore);
  const authContext = initAuthStore();
  const [yearList, setYearList] = useState();

  const getYearList = async (token) => {
    try {
      const optionsResult = await yearListDropdown.getYearList(token);

      setYearList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getYearList(authContext.accessToken);
  }, []);

  const getValue = () => {
    return yearList.find((year) => year.value === value) || yearList[0];
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={defaultValue}
        options={yearList}
        value={yearList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
