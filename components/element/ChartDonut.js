import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import Chart from 'chart.js';

const CardDonut = ({ data, backgroundColor, label }) => {
  // Chart.pluginService.register({
  //   beforeDraw: function (chart) {
  //     var width = chart.chart.width,
  //       height = chart.chart.height,
  //       ctx = chart.chart.ctx;

  //     ctx.restore();
  //     var fontSize = (height / 60).toFixed(2);
  //     ctx.font = fontSize + "em sans-serif";
  //     ctx.textBaseline = "middle";

  //     var text = "95%",
  //       textX = Math.round((width - ctx.measureText(text).width) / 2),
  //       textY = height / 2;

  //     ctx.fillText(text, textX, textY);
  //     ctx.save();
  //   },
  // });

  const labelList = label.map((l) => l.name);
  const percenterList = data.map((percent) => percent.toFixed(0));
  const dataDonut = {
    labels: labelList,
    datasets: [
      {
        data: percenterList,
        backgroundColor: backgroundColor,
      },
    ],
  };

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    cutoutPercentage: 85,
    animation: {
      animationRotate: true,
      duration: 500,
      circumference: 300,
      animateRotate: true,
    },
    tooltips: {
      enabled: true,
      bodyFontSize: 16,
      callbacks: {
        label: function (tooltipItem, data) {
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var meta = dataset._meta[Object.keys(dataset._meta)[0]];
          var total = meta.total;
          var currentValue = dataset.data[tooltipItem.index];
          var percentage = parseFloat(
            ((currentValue / total) * 100).toFixed(1),
          );
          return percentage + '%';
        },
        title: function (tooltipItem, data) {
          return data.labels[tooltipItem[0].index];
        },
      },
    },
    legend: {
      position: 'bottom',
      display: false,
      labels: {
        boxWidth: 10,
        usePointStyle: true,
      },
    },
    text: ' ',
  };

  return <Doughnut data={dataDonut} options={options} />;
};

export default CardDonut;
