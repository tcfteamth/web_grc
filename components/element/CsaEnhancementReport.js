import React, { memo } from 'react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import moment from 'moment';
import { Text, OverAllRatingStatus } from '.';
import { colors, sizes } from '../../utils';
import useWindowDimensions from '../../hook/useWindowDimensions';
import { Grid, Image, Popup } from 'semantic-ui-react';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: ${(props) => props.width - 65}px;
  // width: 100%;

  @media print {
    width: 1360;
  }
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 10px 0px;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  width: auto;
  height: 100%;
  padding: 6px 0px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width}px;
  flex-wrap: wrap;
  padding: 0px 12px;
  justify-content: ${(props) => (props.center ? 'center' : 'start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

// rotateX
const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: visible;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

// const headers = [
//   { key: 'Responsible Division', render: 'Responsible Division', width: 100 },
//   { key: 'Level4', render: 'Level 4', width: 80 },
//   { key: 'Sub-Process', render: 'Sub Process', width: 100 },
//   { key: 'Objective', render: 'Objective', width: 100 },
//   { key: 'Risk', render: 'Risk', width: 100 },
//   { key: 'Control', render: 'Control', width: 100 },
//   { key: 'Control Rating', render: 'Control Rating', width: 80 },
//   {
//     key: 'Observation and Finding',
//     render: 'Observation and Finding',
//     width: 130,
//   },
//   {
//     key: 'Enhance/Corrective',
//     render: 'Enhance/Corrective',
//     width: 130,
//   },
//   { key: 'Benefit/Detail', render: 'Benefit/Detail', width: 100 },
//   { key: 'Due Date', render: 'Due Date', width: 100 },
// ];

const headers = [
  { key: 'Responsible Division', render: 'Responsible Division', width: -80 },
  { key: 'Level4', render: 'Level 4', width: -80 },
  { key: 'Sub-Process', render: 'Sub Process', width: 0 },
  { key: 'Objective Type', render: 'Objective Type', width: -80 },
  { key: 'Risk', render: 'Risk', width: 80 },
  { key: 'Control', render: 'Control', width: 80 },
  { key: 'Control Rating', render: 'Control Rating', width: -80 },
  {
    key: 'Observation and Finding',
    render: 'Observation and Finding',
    width: 80,
  },
  {
    key: 'Enhance/Corrective',
    render: 'Enhance/Corrective',
    width: 160,
  },
  { key: 'Benefit/Detail', render: 'Benefit/Detail', width: 0 },
  { key: 'Due Date', render: 'Due Date', width: -80 },
];

const index = ({ departmentListModel }) => {
  const { width: screenWidth } = useWindowDimensions();
  const realWidth = screenWidth - 64;
  const newWidth = realWidth / headers.length;

  const renderCardHeaderTable = () => (
    <Div>
      {headers.map(({ render, key, width }) => (
        <TableHeader key={key}>
          <Cell center width={screenWidth > 1440 ? newWidth + width : newWidth}>
            <Text
              fontSize={sizes.xs}
              fontWeight="med"
              color={colors.textlightGray}
            >
              {render}
            </Text>
          </Cell>
        </TableHeader>
      ))}
    </Div>
  );

  return useObserver(() =>
    departmentListModel.tables.enhancement.map((enhance, enhanceIndex) =>
      enhance.newEnhancement.map((enhanceDetail) => (
        <Grid style={{ margin: '16px 0px' }} className="print-pagebreak pt">
          <Grid.Row style={{ padding: 0 }}>
            <div className="w-table">
              <CardAll width={screenWidth}>
                <CardTop>
                  <Text
                    fontSize={sizes.s}
                    fontWeight="bold"
                    color={colors.primaryBackground}
                  >
                    {`Enhancement_${enhance.indicator} - Part ${
                      enhanceIndex + 1
                    }`}
                  </Text>
                </CardTop>

                <TableScroll>
                  <TabScroll>
                    <Div col>
                      {renderCardHeaderTable()}
                      <DividerLine bottom style={{ marginBottom: 16 }} />
                      <TableBody key={enhance.indicator}>
                        <Div>
                          <Div col>
                            {enhanceDetail.map((detail) => (
                              <Div bottom={8}>
                                {/* <Cell width={80} center> */}
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 80
                                      : newWidth
                                  }
                                  center
                                >
                                  {detail.partner &&
                                  detail.partner !== detail.indicator ? (
                                    <div
                                      style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                      }}
                                    >
                                      <Popup
                                        trigger={
                                          <Text
                                            fontSize={sizes.xs}
                                            fontWeight="bold"
                                            color={colors.primaryBlack}
                                          >
                                            {detail.indicator}
                                          </Text>
                                        }
                                        content={`${detail.divEn || '-'} ${detail.shiftEn || ''}`}
                                        size="small"
                                      />
                                      <Image
                                        width={48}
                                        height={48}
                                        src="../../static/images/icons-followup-group-1-24-px@3x.png"
                                      />
                                      <Popup
                                        trigger={
                                          <Text
                                            fontSize={sizes.xs}
                                            fontWeight="bold"
                                            color={colors.primaryBlack}
                                          >
                                            {detail.partner}
                                          </Text>
                                        }
                                        content={`${detail.partnerDivEn || '-'} ${detail.partnerShiftEn || ''}`}
                                        size="small"
                                      />
                                    </div>
                                  ) : (
                                    <Popup
                                      trigger={
                                        <Text
                                          fontSize={sizes.xs}
                                          fontWeight="bold"
                                          color={colors.primaryBlack}
                                        >
                                          {detail.indicator}
                                        </Text>
                                      }
                                      content={`${detail.divEn || '-'} ${detail.shiftEn || ''}`}
                                      size="small"
                                    />
                                  )}
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.lv4No}
                                  </Text>
                                </Cell>
                                <Cell width={newWidth} center>
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.lv4Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.lv5Type}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.lv6Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.lv7Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    <OverAllRatingStatus
                                      overAllstatus={detail.ratingId}
                                    />
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.initialRemark}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 160
                                      : newWidth
                                  }
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.suggestion}
                                  </Text>
                                </Cell>
                                <Cell width={newWidth} center>
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.benefit}
                                  </Text>
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {detail.benefitDetail}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 80
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {moment(detail.endDate).format(
                                      'DD/MM/YYYY',
                                    )}
                                  </Text>
                                </Cell>
                              </Div>
                            ))}
                          </Div>
                        </Div>
                      </TableBody>
                    </Div>
                  </TabScroll>
                </TableScroll>
              </CardAll>
            </div>
          </Grid.Row>
        </Grid>
      )),
    ),
  );
};

export default memo(index);

// {enhanceIndex ===
//   departmentListModel.tables.enhancement.length && (
//   <DividerLine bottom />
// )}
