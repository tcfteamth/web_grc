import React, { useEffect, useState } from 'react';
import { Grid, Modal, Header, Table, Rating } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text, ButtonBorder, ButtonAll, Box } from '.';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose, allWaitDmAcceptModel, year }) => {
  return useObserver(() => (
    <ModalBox
      size="large"
      open={active}
      onClose={() => {
        onClose();
      }}
      closeOnDimmerClick={() => {
        onClose();
      }}
      closeIcon
    >
      <Modal.Content style={{ padding: 0 }}>
        <Grid centered>
          <Grid.Row>
            <Text fontSize={sizes.xl} color={colors.primary} fontWeight="bold">
              {`Wait DM Accept - ${year} | Total - ${allWaitDmAcceptModel.list?.length}`}
            </Text>
          </Grid.Row>
        </Grid>

        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>No.</Table.HeaderCell>
              <Table.HeaderCell>Level 4</Table.HeaderCell>
              <Table.HeaderCell>Sub - Process</Table.HeaderCell>
              <Table.HeaderCell>Division</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {allWaitDmAcceptModel.list?.map((allwait, allwaitIndex) => (
              <Table.Row>
                <Table.Cell>{allwaitIndex + 1}</Table.Cell>

                <Table.Cell>{allwait.fullPath}</Table.Cell>

                <Table.Cell>{allwait.nameEn}</Table.Cell>

                <Table.Cell>{allwait.divisionNo}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <Grid centered padded="vertically">
          <ButtonAll text="Close" onClick={() => onClose()} />
        </Grid>
      </Modal.Content>
    </ModalBox>
  ));
};

export default index;
