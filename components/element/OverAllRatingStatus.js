import React from 'react';
import styled from 'styled-components';
import { Text } from '.';
import { sizes, colors } from '../../utils';

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  max-width: 100px;
  min-width: 60px;
  width: 100%;
  height: 24px !important;
  text-align: center;
  margin-left: ${(props) => props.left || 0}px;
  background-color: ${(props) =>
    props.Good
      ? colors.green
      : props.Fair
      ? colors.yellow
      : props.Poor
      ? colors.red
      : '#FFFFFF'};
`;

const index = ({ overAllstatus }) => {
  return (
    <div>
      {overAllstatus === 3 && (
        <StatusTag Good>
          <Text
            color={colors.backgroundPrimary}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            Good
          </Text>
        </StatusTag>
      )}
      {overAllstatus === 2 && (
        <StatusTag Fair>
          <Text
            color={colors.backgroundPrimary}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            Fair
          </Text>
        </StatusTag>
      )}
      {overAllstatus === 1 && (
        <StatusTag Poor>
          <Text
            color={colors.backgroundPrimary}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            Poor
          </Text>
        </StatusTag>
      )}
      {!overAllstatus && (
        <StatusTag Poor>
          <Text
            color={colors.backgroundPrimary}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            -
          </Text>
        </StatusTag>
      )}
    </div>
  );
};

export default index;
