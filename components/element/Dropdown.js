import React, { useState, useEffect, useReducer } from "react";
import { Dropdown, Menu, Image, Icon, Select } from "semantic-ui-react";
import styled from "styled-components";
import { colors, sizes } from "../../utils";

const DropdownGroup = styled(Dropdown)`
  border-radius: 6px 6px 0px 6px !important;
  width: 100% !important;
  height: 38px !important;
  font-size: ${sizes.xs}px !important;
  padding: 8px 30px 8px 16px !important;
  text-align: ${(props) => (props.center ? "center" : "flex-start")} !important;
  font-weight: 500 !important;
  color: ${colors.primaryBlack};
  background-color: ${colors.backgroundPrimary} !important;
  border-style: solid !important;
  border-width: 1px !important;
  border-color: #eaeaea !important;
`;

const DropdownAll = ({
  options,
  textDefault,
  handleOnChange,
  border,
  width,
  height,
  value,
}) => (
  <DropdownGroup
    control={Select}
    width={width}
    height={height}
    border={border || false}
    options={options}
    text={textDefault}
    value={value}
    onChange={handleOnChange}
    icon={
      <span
        style={{
          paddingTop: 2,
          right: 8,
          position: "absolute",
        }}
      >
        <Image
          width={18}
          height={18}
          src="../../static/images/iconDown@3x.png"
        />
      </span>
    }
  />
);

export default DropdownAll;
