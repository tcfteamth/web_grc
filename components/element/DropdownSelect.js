import React from 'react';
import Select from 'react-select';
import { colors } from '../../utils';

const colourStyles = {
  control: (styles, { isDisabled, isFocused, isMulti, error }) => {
    return {
      ...styles,
      backgroundColor: isDisabled ? '#f3f4f5' : '#FFFFFF',
      width: '100%',
      fontSize: 14,
      fontColor: '#acacac',
      borderRadius: 6,
      border: `1px solid ${error ? colors.red : colors.textGrayLight}`,
      boxShadow: 'none',
      paddingLeft: isMulti ? 0 : 8,
      left: 0,
      ':hover': {
        color: '#000000',
        border: error && `1px solid ${colors.red}`,
      },
      ':focus': {
        color: '#000000',
        border: '1px solid red',
      },
    };
  },
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    const color = data.color;
    return {
      ...styles,
      fontSize: 14,
      cursor: isDisabled ? 'not-allowed' : 'default',
      ':active': {
        ...styles[':active'],
        backgroundColor: '#f3f4f5',
      },
    };
  },
  menuPortal: (base) => ({ ...base, zIndex: 9999 }),
  input: (styles, { data }) => {
    return {
      ...styles,
    };
  },
  placeholder: (styles, { data }) => {
    return {
      ...styles,
      color: '#b1b8be',
    };
  },
  multiValue: (styles, { data }) => {
    return {
      ...styles,
      backgroundColor: '#ffffff',
      boxShadow: `0px 1px 2px 0 rgba(34, 36, 38, 0.15)`,
      border: '1px solid #dfdfdf',
      borderRadius: 6,
    };
  },
  multiValueLabel: (styles, { data }) => ({
    ...styles,
  }),
  multiValueRemove: (styles, { data }) => ({
    ...styles,
    // backgroundImage: `url(../../../../static/images/closeMulti.png)`,
    backgroundSize: 'cover',
    color: '#acacac',
    ':hover': {
      color: '#000000',
      //   backgroundImage: `url(../../../../static/images/closeMulti.png)`,
      backgroundSize: 'cover',
    },
  }),
};

const DropdownInput = ({
  defaultValue,
  options,
  placeholder,
  value,
  handleOnChange,
  isDisabled,
  state,
  isMulti,
  isSearchable,
  onInputChange,
  error,
}) => (
  <Select
    menuPortalTarget={document.body}
    closeMenuOnSelect
    isMulti={isMulti}
    isDisabled={isDisabled}
    styles={colourStyles}
    state={state}
    defaultValue={defaultValue}
    options={options}
    value={value}
    placeholder={placeholder}
    onChange={handleOnChange}
    isSearchable={isSearchable}
    onInputChange={onInputChange}
    error={error}
  />
);
export default DropdownInput;
