import React from 'react';
import { Pie } from 'react-chartjs-2';

const ChartPie = ({ data, backgroundColor, labels, dataCount }) => {
  const dataPie = {
    // labels: ["GOOD+", "GOOD", "FAIR", "POOR"],
    datasets: [
      {
        data: data,
        backgroundColor: backgroundColor,
        labels: labels || ['GOOD', 'FAIR', 'POOR'],
        dataCount: dataCount || null,
        // hoverBackgroundColor: ["#00b5e1", "#00b140", "#ffc72c", "#e4002b"],
      },
    ],
    animation: {
      animationRotate: false,
      duration: 0,
    },
  };

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    cutoutPercentage: 0,
    animation: {
      animationRotate: false,
      duration: 500,
      circumference: 300,
      animateRotate: false,
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var total = dataset.data.reduce(function (
            previousValue,
            currentValue,
            currentIndex,
            array,
          ) {
            return previousValue + currentValue;
          });
          var currentValue = dataset.data[tooltipItem.index];
          var currentText = dataset.labels[tooltipItem.index];
          var currentCount =
            dataset.dataCount && dataset.dataCount[tooltipItem.index];
          var percentage = Math.floor((currentValue / total) * 100 + 0.5);
          return `${currentText} ${percentage}% ${
            !currentCount ? '' : `Total: ${currentCount}`
          }`;
        },
      },
    },
    legend: {
      position: 'bottom',
      labels: {
        boxWidth: 10,
        usePointStyle: true,
      },
    },
    text: ' ',
  };

  return <Pie data={dataPie} options={options} />;
};

export default ChartPie;
