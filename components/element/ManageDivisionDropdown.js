import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { divisionListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  selectedBu,
  selectedDepartmentId,
  placeholder,
}) => {
  const divisionListDropdown = useLocalStore(() => divisionListDropdownStore);
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();

  const getDivisionList = async (bu, departmentId) => {
    try {
      if (departmentId || bu) {
        const optionsResult = await divisionListDropdown.getManageFilterDivisionList(
          bu,
          departmentId,
        );

        setDepartmentList(optionsResult);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getDivisionList(selectedBu, selectedDepartmentId);
  }, [selectedDepartmentId, selectedBu]);

  const getValue = () => {
    return departmentList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList && departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
