import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { departmentListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  selectedYear,
  selectedBu,
  placeholder,
}) => {
  const departmentListDropdown = useLocalStore(
    () => departmentListDropdownStore,
  );
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();

  const getDepartmentList = async (bu,year) => {
    try {
      const currentYear = new Date().getFullYear();
      if(year === null || year === undefined || year === '' || year === currentYear){
        const optionsResult = await departmentListDropdown.getDepartmentList(bu);
        setDepartmentList(optionsResult);
      }else{
        const optionsResult = await departmentListDropdown.getDepartmentOldList(bu,year);
        setDepartmentList(optionsResult);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getDepartmentList(selectedBu,selectedYear);
  }, [selectedBu,selectedYear]);

  const getValue = () => {
    return departmentList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
