import React from 'react';
import styled from 'styled-components';
import { sizes } from '../../utils';

const CirCle = styled.div`
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  background: #888b8d;
  border-radius: 50%;
  font-size: ${sizes.xxxxs}px;
`;

const index = ({ year, ...rest }) => {
  return (
    <CirCle className="click" {...rest}>
      {year}
    </CirCle>
  );
};

export default index;
