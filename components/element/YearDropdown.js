import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { yearListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({ value, handleOnChange, disabled, placeholder, model }) => {
  const yearListDropdown = useLocalStore(() => yearListDropdownStore);

  const authContext = initAuthStore();
  const [yearList, setYearList] = useState();

  const getYearList = async () => {
    try {
      const optionsResult = await yearListDropdown.getYearListFillter(
        authContext.accessToken,
      );

      // set default year when user open filter
      // dropdown ตัวนี้ต่างจากตัวอื่นเพราะตัวนี้ set filter เข้าไปใน model หลักเช่น Assessmentlist/detail model
      // dropdown ตัวอื่น set เข้าไปใน filter model
      if (model.length > 0) {
        model.forEach((m) =>
          m.filterModel.setField('selectedYear', optionsResult[1].value),
        );
      }

      setYearList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getYearList();
  }, []);

  const getValue = () => {
    return (
      yearList.find((assessor) => assessor.value === value) ||
      yearList[0] ||
      null
    );
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={yearList}
        options={yearList}
        value={yearList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
