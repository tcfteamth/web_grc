import React from 'react';
import { Form } from 'semantic-ui-react';
import { Text } from '.';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  display: flex;
  min-width: 80px;
  max-width: 120px;
  width: 100%;
  padding: 0px 8px;
  min-height: 24px;
  text-align: center;
  align-items: center !important;
  justify-content: center;
  background-color: ${(props) =>
    props.Awaiting2ndLineApprove
      ? colors.brightbule
      : props.AwaitingVPApprove
      ? colors.pralblue
      : props.Preparing
      ? colors.purplepink
      : props.AwaitingDMApprove
      ? colors.pralpurple
      : '#FFFFFF'};
`;

const StatusAll = ({ child }) => (
  <>
    {child.status === 'Awaiting DM Approve' ? (
      <StatusTag AwaitingDMApprove>
        <Text
          fontSize={sizes.xxxs}
          fontWeight="bold"
          color={colors.textpralpurple}
        >
          {child.status}
        </Text>
      </StatusTag>
    ) : child.status === 'Preparing' ? (
      <StatusTag Preparing>
        <Text
          fontSize={sizes.xxxs}
          fontWeight="bold"
          color={colors.textpurplepink}
        >
          {child.status}
        </Text>
      </StatusTag>
    ) : child.status === 'Awaiting 2nd Line Approve' ? (
      <StatusTag Awaiting2ndLineApprove>
        <Text fontSize={sizes.xxxs} fontWeight="bold" color={colors.textSky}>
          {child.status}
        </Text>
      </StatusTag>
    ) : child.status === 'Awaiting VP Approve' ? (
      <StatusTag AwaitingVPApprove>
        <Text
          fontSize={sizes.xxxs}
          fontWeight="bold"
          color={colors.textpralblue}
        >
          {child.status}
        </Text>
      </StatusTag>
    ) : (
      <StatusTag AwaitingVPApprove>
        <Text
          fontSize={sizes.xxxs}
          fontWeight="bold"
          color={colors.textpralblue}
        >
          {child.status}
        </Text>
      </StatusTag>
    )}
  </>
);

export default StatusAll;
