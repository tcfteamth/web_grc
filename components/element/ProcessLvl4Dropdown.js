import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { lvl4ListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
  selectedLVl3,
  getIdLvl4,
}) => {
  const lvl4ListDropdown = useLocalStore(() => lvl4ListDropdownStore);
  const authContext = initAuthStore();
  const [lvl4List, setLvl4List] = useState();

  const getRoadmapType = async (lvl3No) => {
    try {
      if (lvl3No) {
        const optionsResult = await lvl4ListDropdown.getRoadmapLvl4List(lvl3No);

        setLvl4List(optionsResult);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRoadmapType(selectedLVl3);
  }, [selectedLVl3]);

  const getValue = () => {
    return lvl4List.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    console.log('getIdLvl4', getIdLvl4);
    if (getIdLvl4 === 'true') {
      handleOnChange(e);
    } else {
      handleOnChange(e.value);
    }
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={lvl4List}
        options={lvl4List}
        value={lvl4List && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
