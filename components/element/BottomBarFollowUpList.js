import React, { useEffect, useState } from 'react';
import { Menu, Popup, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
  ModalGlobal,
} from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../RoadMapPage/modal';
// import { RoadmapListModel } from '../../model';
import useWindowDimensions from '../../hook/useWindowDimensions';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;
const BottombarLogo = styled(Menu)`
  height: 55px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  background-color: transparent !important;
  border: transparent !important;
  border-top: transparent !important;
  box-shadow: none !important;
`;

const BottomBar = observer(
  ({
    detailReportModel,
    manageRoleModel,
    filterModel,
    isFilterReport,
    followUpModel,
  }) => {
    const authContext = initAuthStore();
    const [activeSubmitModal, setActiveSubmitModal] = useState(false);
    const { width: screenWidth } = useWindowDimensions();
    const authStore = initAuthStore();

    const submitFollowUp = async () => {
      try {
        setActiveSubmitModal(false);
        const result = await followUpModel.submitFollowUp(
          authStore.accessToken,
        );

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    return useObserver(() => (
      <div>
        {/* Logo for print  */}
        <BottombarLogo className="show-in-print" fluid fixed="bottom">
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
              paddingTop: 16,
            }}
          >
            <Image
              src="../../static/images/gc-logo-4-x@3x.png"
              style={{ width: 100, height: 'auto' }}
            />
          </div>
        </BottombarLogo>
        <Bottombar
          className="pl-Bottombar no-print"
          fluid
          fixed="bottom"
          size="large"
        >
          <Menu.Menu position="right">
            <Section>
              <ButtonAll
                text= {authContext.roles.isVP ? "Approve" : "Submit"}
                textColor={colors.backgroundPrimary}
                fontSize={sizes.s}
                textUpper
                textWeight="med"
                color={colors.primary}
                onClick={() => setActiveSubmitModal(true)}
                disabled={!followUpModel.isSelectFollowUpList}
              />
            </Section>
          </Menu.Menu>
        </Bottombar>

        <ModalGlobal
          onSubmit={submitFollowUp}
          onClose={() => setActiveSubmitModal(false)}
          open={activeSubmitModal}
          submitText="Submit"
          cancelText="Cancel"
          title="Submit FollowUp"
        />
      </div>
    ));
  },
);

export default BottomBar;
