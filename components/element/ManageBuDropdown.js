import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { manageBUDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  isDefaultValue,
  selectedBu,
  placeholder,
  filterModel,
}) => {
  const manageBUDropdown = useLocalStore(() => manageBUDropdownStore);
  const authContext = initAuthStore();
  const [buList, setBUList] = useState();

  const getManageDepartmentList = async () => {
    try {
      const optionsResult = await manageBUDropdown.getManageFilterBUList(
        authContext.accessToken,
      );

      setBUList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getManageDepartmentList();
  }, []);

  const getValue = () => {
    if (isDefaultValue) {
      filterModel.setField('selectedBu', buList[0].value);

      return buList[0];
    }

    return buList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={buList}
        options={buList}
        value={buList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
