import React, { useState } from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';
import { Text, TextArea } from '.';

const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-top: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px;
  width: 100%;
`;

const index = ({ onChange, value }) => {
  const handleChangeComment = (e) => {
    onChange(e.target.value);
  };

  return useObserver(() => (
    <CardComment>
      <Text
        color={colors.primaryBlack}
        fontSize={sizes.xs}
        fontWeight="med"
        className="upper"
      >
        Comment
      </Text>
      <TextArea
        height={160}
        type="comment"
        placeholder="Key Comment"
        onChange={handleChangeComment}
        value={value}
      />
    </CardComment>
  ));
};

export default index;
