import React from 'react';
import styled from 'styled-components';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  min-width: 60px;
  max-width: 100px;
  width: 100%;
  height: 24px !important;
  text-align: center;
  margin-left: ${(props) => props.left || 0}px;
  background-color: ${(props) =>
    props.rateId === 3
      ? colors.green
      : props.rateId === 2
      ? colors.yellow
      : props.rateId === 1
      ? colors.red
      : '#FFFFFF'};
`;

const index = ({ rateId }) => {
  const renderStatus = () => {
    if (rateId === 3) {
      return 'Good';
    }

    if (rateId === 2) {
      return 'Fair';
    }

    if (rateId === 1) {
      return 'Poor';
    }

    return '-';
  };

  return (
    <StatusTag rateId={rateId}>
      <Text
        color={colors.backgroundPrimary}
        fontWeight="bold"
        fontSize={sizes.xxs}
      >
        {renderStatus()}
      </Text>
    </StatusTag>
  );
};

export default index;
