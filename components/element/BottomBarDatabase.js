import React, { useState } from 'react';
import { Menu, Button, Grid, Select } from 'semantic-ui-react';
import { observer, useLocalStore } from 'mobx-react-lite';
import Router from 'next/router';
import styled from 'styled-components';
import { dataTag } from '../../utils/static';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
} from '.';
import { SaveModal, CreateNewObjectRiskModal } from '../AssessmentPage/modal';
import { SelectCreateProcessModal } from '../DatabasePage/modal';
import { IAFindingDetailModel } from '../../model/AssessmentModel';
import { ComplianceModel } from '../../model/ProcessDBModel';
import { AttachFilesModal } from '../COSODatabase';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const BottomBar = observer(({ page, RCModel }) => {
  const authContext = initAuthStore();
  const [active, setActive] = useState(false);
  const [activeObject, setActiveObject] = useState(false);
  const [mode, setMode] = useState('');
  const [titleText, setTitleText] = useState('');
  const [btnRight, setBtnRight] = useState();
  const [tagList, setTagList] = useState([]);

  const [selectModal, setSelectModal] = useState(false);
  const [saveModal, setSaveModal] = useState(false);
  const handleSelection = (updater) => (e, { value }) => updater(value);
  const [title, setTitle] = useState({ preTitle: '', postTitle: '' });
  const iafindingDetailModel = useLocalStore(() => new IAFindingDetailModel());
  const complianceModel = useLocalStore(() => new ComplianceModel());

  const [activeImportIAFileModal, setActiveImportIAFileModal] = useState(false);
  const [activeCompliance, setActiveCompliance] = useState(false);

  const setAlertModal = (title, preTitle, postTitle, mode, btnRight) => {
    setTitleText(title);
    setTitle({ preTitle, postTitle });
    setMode(mode);
    setActive(true);
    setBtnRight(btnRight);
  };

  const handleCloseCreateRoadmapModal = (status) => {
    setActiveObject(status);
  };

  const handelPage = (value) => {
    window.location.href = value;
  };

  const handleImportIA = async (files) => {
    const formData = new FormData();
    formData.append('excel', files);

    await iafindingDetailModel
      .importExcelIa(formData)
      .then((response) => {
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleExportIA = async () => {
    try {
      const result = await iafindingDetailModel.exportIafindingDetail(
        authContext.accessToken,
      );

      window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleImportCompliance = async (files) => {
    const formData = new FormData();
    formData.append('excel', files);

    await complianceModel
      .importExcelCompliance(formData)
      .then((response) => {
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleExportCompliance = async () => {
    try {
      const result = await complianceModel.exportExcelCompliance(
        authContext.accessToken,
      );
      window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleCreateNewObject = () => {
    try {
      const result = RCModel.addObject(RCModel.lvl4);
      setActiveObject(false);
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleCreateRiskAndControl = async () => {
    try {
      setSaveModal(false);
      const result = await RCModel.createRiskAndControl(
        authContext.accessToken,
      );
      window.location.href = '/databasePage/riskAndControlManagment';
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleExportRiskAndControl = async () => {
    try {
      const result = await RCModel.exportExcelRiskAndControl(
        authContext.accessToken,
      );
      window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  const handleExportProcessDatabase = async () => {
    try {
      const result = await RCModel.exportExcelProcessDatabase(
        authContext.accessToken,
      );
      window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div>
      {page && page ? (
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
            {authContext.roles.isAdmin && page === 'Compliance' && (
              <Section>
                <ButtonBorder
                  text="import"
                  textWeight="med"
                  textColor={colors.primaryBlack}
                  width={160}
                  fontSize={sizes.s}
                  textUpper
                  borderColor={colors.btGray}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={() => setActiveCompliance(true)}
                />
              </Section>
            )}

            {page === 'processAdmin' && authContext.roleName === 'SUPER-ADMIN' && (
              <Section>
                <ButtonBorder
                  borderColor={colors.textSky}
                  textColor={colors.textSky}
                  textUpper
                  fontSize={sizes.s}
                  textWeight="med"
                  text="Create process Database"
                  icon="../../static/images/iconPlus@3x.png"
                  handelOnClick={() => setSelectModal(true)}
                />
              </Section>
            )}

            {page === 'riskAndControl' &&
              authContext.roleName === 'SUPER-ADMIN' && (
                <Section>
                  <ButtonBorder
                    borderColor={colors.textSky}
                    textColor={colors.textSky}
                    textUpper
                    fontSize={sizes.s}
                    textWeight="med"
                    text="Create Risk and Control"
                    icon="../../static/images/iconPlus@3x.png"
                    handelOnClick={() => {
                      handelPage('/databasePage/createRiskAndControlManagment');
                    }}
                  />
                </Section>
              )}

            {authContext.roles.isAdmin && page === 'iaFinding' && (
              <Section>
                <ButtonBorder
                  text="IMPORT"
                  width={150}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  handelOnClick={() => setActiveImportIAFileModal(true)}
                  icon="../../static/images/excel@3x.png"
                />
              </Section>
            )}
            {authContext.roles.isAdmin && (
              <>
                {page === 'createRiskAndControl' ||
                page === 'editRiskAndControl' ? (
                  <ButtonBorder
                    textUpper
                    textWeight="med"
                    textSize={sizes.s}
                    handelOnClick={() => handleCloseCreateRoadmapModal(true)}
                    borderColor="#00aeef"
                    textColor="#00aeef"
                    text="Add Objective"
                    icon="../../static/images/iconPlus@3x.png"
                    disabled={!RCModel.lvl4}
                  />
                ) : (
                  ''
                )}
              </>
            )}
          </Menu.Menu>
          <Menu.Menu position="right">
            {page === 'riskAndControl' || page === 'processAdmin' ? (
              <Section>
                <ButtonBorder
                  text="Export"
                  width={150}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={() => {
                    if (page === 'processAdmin') handleExportProcessDatabase();
                    else handleExportRiskAndControl();
                  }}
                />
              </Section>
            ) : (
              ''
            )}
            {page === 'createRiskAndControl' ||
            page === 'editRiskAndControl' ? (
              <Section>
                <ButtonAll
                  text="SAVE"
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.primary}
                  disabled={!RCModel.lvl4}
                  onClick={() => {
                    if (RCModel.isSetAllObjectData === true) {
                      console.log('in if');
                      setSaveModal(true);
                    } else {
                      console.log('else if');
                      RCModel.setValidateAssessmentForm();
                    }
                  }}
                />
              </Section>
            ) : (
              ''
            )}
          </Menu.Menu>

          {page === 'createRiskAndControl' || page === 'editRiskAndControl' ? (
            <SaveModal
              active={saveModal}
              onClose={() => setSaveModal(false)}
              onSubmit={handleCreateRiskAndControl}
            />
          ) : (
            ''
          )}

          {/* Modal สำหรับ create roadmap */}
          {page === 'createRiskAndControl' || page === 'editRiskAndControl' ? (
            <CreateNewObjectRiskModal
              active={activeObject}
              onClose={() => handleCloseCreateRoadmapModal(false)}
              onSubmit={handleCreateNewObject}
              assessmentRCModel={RCModel}
            />
          ) : (
            ''
          )}

          {/* Modal สำหรับ create roadmap */}
          {page === 'processAdmin' && (
            <SelectCreateProcessModal
              active={selectModal}
              setSelectModal={() => setSelectModal(false)}
              onClose={() => setSelectModal(false)}
            />
          )}

          {authContext.roles.isAdmin && page === 'iaFinding' && (
            <Section>
              <ButtonBorder
                text="EXPORT"
                width={150}
                textColor={colors.greenExport}
                fontSize={sizes.s}
                textUpper
                textWeight="med"
                borderColor={colors.greenExport}
                handelOnClick={handleExportIA}
                icon="../../static/images/excel@3x.png"
              />
            </Section>
          )}
          {page === 'Compliance' && (
            <Section>
              <ButtonBorder
                text="EXPORT"
                width={150}
                textColor={colors.greenExport}
                fontSize={sizes.s}
                textUpper
                textWeight="med"
                borderColor={colors.greenExport}
                handelOnClick={handleExportCompliance}
                icon="../../static/images/excel@3x.png"
              />
            </Section>
          )}

          <Modal
            open={active}
            title={titleText}
            mode={mode}
            submitTextRight={btnRight}
            ContentComponent={() => (
              <div>
                {mode === 'processAdmin' ? (
                  <Grid columns="equal">
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Key Process (Level 2)
                        </Text>
                        <InputDropdown search placeholder="Key Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process (Level 3)
                        </Text>
                        <InputAll placeholder="Select Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Sub-Process (Level 4)
                        </Text>
                        <InputAll placeholder="Select Sub-Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process Definition
                        </Text>
                        <TextArea placeholder="Remark" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Value
                        </Text>
                        <InputAll placeholder="Value" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Source Mapping - Process Name
                        </Text>
                        <InputAll placeholder="Source Mapping - Process Name" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Source Mapping - Process Definition
                        </Text>
                        <InputAll placeholder="Source Mapping - Process Definition" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Exactly Matched
                        </Text>
                        <div>
                          <Grid columns="equal">
                            <Grid.Row>
                              <Grid.Column>
                                <Div between center top={4}>
                                  <RadioBox label="YES" />
                                  <RadioBox label="No" />
                                </Div>
                              </Grid.Column>
                              <Grid.Column>
                                <InputAll
                                  disabled
                                  minWidth={60}
                                  placeholder="Reason"
                                />
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </div>
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Remark
                        </Text>
                        <InputAll minWidth={60} placeholder="2019" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Tag
                        </Text>
                        <InputDropdown
                          control={Select}
                          placeholder="Select Tag"
                          search
                          width={50}
                          options={dataTag}
                          value={tagList}
                          multiple
                          handleOnChange={handleSelection(setTagList)}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                ) : mode === 'roadmapList' ? (
                  <div>
                    <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                      {title.preTitle}
                    </Text>
                    <TextArea height={45} placeholder="Fill scope of work" />
                  </div>
                ) : mode === 'roadmapAdmin' ? (
                  <Grid columns="equal">
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Year
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Owner
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Department
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Division
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process (Level 3)
                        </Text>
                        <DropdownAll placeholder="Select Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Sub-Process (Level 4)
                        </Text>
                        <DropdownAll placeholder="Select Sub-Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Remark
                        </Text>
                        <InputAll placeholder="Remark" />
                      </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          IC Agent+
                        </Text>
                        <div>
                          <Grid columns="equal">
                            <Grid.Row>
                              <Grid.Column>
                                <Div center top={4}>
                                  <RadioBox label="YES" />
                                  <div style={{ paddingLeft: 24 }}>
                                    <RadioBox label="No" />
                                  </div>
                                </Div>
                              </Grid.Column>
                              <Grid.Column />
                            </Grid.Row>
                          </Grid>
                        </div>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                ) : (
                  <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                      {title.preTitle}
                    </Text>
                    <Text fontSize={sizes.xxs} color={colors.textDarkBlack}>
                      {title.postTitle}
                    </Text>
                  </div>
                )}
              </div>
            )}
            onClose={() => setActive(false)}
          />
          <AttachFilesModal
            active={activeImportIAFileModal}
            onClose={() => setActiveImportIAFileModal(false)}
            onAttatchFiles={(files) => {
              handleImportIA(files);
            }}
          />
          <AttachFilesModal
            active={activeCompliance}
            onClose={() => setActiveCompliance(false)}
            onAttatchFiles={(files) => {
              handleImportCompliance(files);
            }}
          />
        </Bottombar>
      ) : (
        ''
      )}
    </div>
  );
});

export default BottomBar;
