import React from 'react';
import styled from 'styled-components';
import { Button, Image } from 'semantic-ui-react';
import { Text } from '../../components/element';
import { colors, sizes } from '../../utils';

const AddButton = styled(Button)`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-color: ${(props) => props.borderColor || '#b1b3aa'} !important;
  border-style: solid !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-radius: ${(props) =>
    props.radius ? `6px 6px 6px 6px` : `6px 6px 0px 6px`} !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 40}px !important;
  opacity: ${(props) => (props.disabled ? 0.5 : '')};

  @media only screen and (min-width: 320px) {
    padding: ${(props) => props.padding || '0px 8px'} !important;
  }
  @media only screen and (min-width: 768px) {
    padding: ${(props) => props.padding || '0px 24px'} !important;
  }
`;

const ButtonBorder = ({
  padding,
  disabled,
  text,
  color,
  width,
  height,
  textColor,
  textSize,
  textWeight,
  icon,
  border,
  borderColor,
  handelOnClick,
  iconSize,
  borderSize,
  textUpper,
  style,
  radius,
}) => (
  <div>
    {icon ? (
      <AddButton
        style={style}
        color={color}
        height={height}
        width={width}
        borderColor={borderColor}
        onClick={disabled ? null : handelOnClick}
        borderSize={borderSize}
        border={border}
        disabled={disabled}
        padding={padding}
        radius={radius}
      >
        <Image
          src={icon}
          width={iconSize || 18}
          height={iconSize || 18}
          style={{
            marginRight: text ? 8 : 0,
          }}
        />
        <Text
          style={{ textTransform: textUpper ? 'uppercase' : 'none' }}
          color={textColor || colors.primaryBlack}
          fontSize={textSize}
          fontWeight={textWeight || 'normal'}
        >
          {text}
        </Text>
      </AddButton>
    ) : (
      <AddButton
        style={style}
        color={color}
        height={height}
        width={width}
        onClick={disabled ? null : handelOnClick}
        borderColor={borderColor}
        borderSize={borderSize}
        border={border}
        disabled={disabled}
        radius={radius}
      >
        <Text
          style={{ textTransform: textUpper ? 'uppercase' : 'none' }}
          color={textColor || colors.primaryBlack}
          fontSize={textSize}
          fontWeight={textWeight || 'normal'}
        >
          {text}
        </Text>
      </AddButton>
    )}
  </div>
);

export default ButtonBorder;
