import React from "react";
import { ProgressBar, Step } from "react-step-progress-bar";

const ProgressBarStatus = ({ percent, data }) => {
  return (
    <ProgressBar percent={percent}>
      <Step transition="scale">
        {({ accomplished, index }) => (
          <div
            style={{ fontWeight: "bold" }}
            className={`indexedStep ${accomplished ? "one" : null}`}
          >
            {data}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished, index }) => (
          <div
            style={{ fontWeight: "bold" }}
            className={`indexedStep ${accomplished ? "two" : null}`}
          >
            {data}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished, index }) => (
          <div
            style={{ fontWeight: "bold" }}
            className={`indexedStep ${accomplished ? "three" : null}`}
          >
            {data}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished, index }) => (
          <div
            style={{ fontWeight: "bold" }}
            className={`indexedStep ${accomplished ? "four" : null}`}
          >
            {data}
          </div>
        )}
      </Step>
      <Step transition="scale">
        {({ accomplished, index }) => (
          <div
            style={{ fontWeight: "bold" }}
            className={`indexedStep ${accomplished ? "five" : null}`}
          >
            {data}
          </div>
        )}
      </Step>
    </ProgressBar>
  );
};

export default ProgressBarStatus;
