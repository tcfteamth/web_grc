import React from 'react';
import styled from 'styled-components';
import { Image } from 'semantic-ui-react';
import Files from 'react-files';
import { Text } from '.';

const ButtonClick = styled.div`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  padding: 0px 0px !important;
  border: dashed 2px ${(props) => props.borderColor || '#1b1464'} !important;
  border-radius: ${(props) =>
    props.border ? '6px' : '6px 6px 0px 6px'} !important;
  width: ${(props) => props.width || 40}px !important;
  height: ${(props) => props.height || 40}px !important;
`;
const Div = styled.div`
  flex-direction: row;
  justify-content: flex-start;
  display: flex;
  align-items: center;
  width: ${(props) => props.width || 'auto'}px;
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
`;

const ButtonFile = ({
  text,
  color,
  width,
  height,
  textColor,
  textSize,
  textWeight,
  border,
  borderColor,
  icon,
  iconSize,
  handelOnChange,
}) => (
  <Div>
    <Div width={width} height={height}>
      <label for="file-upload" class="custom-file-upload click">
        <Files
          className="files-dropzone click"
          onChange={handelOnChange}
          accepts={['image/png', '.jpg', '.pdf', '.xlsx', '.docx', '.pptx']}
          // maxFiles={1}
          multiple
          maxFileSize={5242880}
          minFileSize={0}
          clickable
        >
          <ButtonClick
            border={border}
            color={color}
            height={height}
            width={width}
            borderColor={borderColor}
          >
            {icon && (
              <Image
                src={icon}
                width={iconSize || 16}
                height={iconSize || 16}
              />
            )}
            {text && (
              <Text
                style={{ textTransform: 'uppercase' }}
                color={textColor || '#1b1464'}
                fontSize={textSize || 16}
                fontWeight={textWeight || 'normal'}
              >
                {text || 'Attach File'}
              </Text>
            )}
          </ButtonClick>
        </Files>
      </label>
      {/* <input
        id="file-upload"
        type="file"
        onChange={handelOnChange}
        accept="application/pdf, application/vnd.ms-excel, image/x-png,image/gif,image/jpeg "
      /> */}
    </Div>
    {/* <Text
      style={{ paddingLeft: 16 }}
      color={"#b1b3aa"}
      fontSize={sizes.xxs}
      fontWeight={"normal"}
    >
      {"No file selected."}
    </Text> */}
  </Div>
);

export default ButtonFile;
