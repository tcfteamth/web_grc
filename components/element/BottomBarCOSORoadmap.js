import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import { useObserver } from 'mobx-react';
import styled from 'styled-components';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { ButtonBorder, ButtonAll } from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../COSORoadMapPage/modal';
import useWindowDimensions from '../../hook/useWindowDimensions';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    submitToDm,
    isSubmitToDmDisabled,
    acceptOrRejectRoadmap,
    isAcceptDisabled,
    roadmapData,
    handelExport,
  }) => {
    const authContext = initAuthStore();
    // const roadmapListModel = useLocalStore(() => RoadmapListModel);
    const [active, setActive] = useState(false);
    const [activeAddProcessModal, setActiveAddProcessModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [mode, setMode] = useState('');
    const [titleText, setTitleText] = useState('');
    const [btnRight, setBtnRight] = useState();

    const [title, setTitle] = useState({ preTitle: '', postTitle: '' });
    const { width: screenWidth } = useWindowDimensions();
    const setAlertModal = (title, preTitle, postTitle, mode, btnRight) => {
      setTitleText(title);
      setTitle({ preTitle, postTitle });
      setMode(mode);
      setActive(true);
      setBtnRight(btnRight);
    };

    const handleCloseCreateRoadmapModal = (status) => {
      setActive(status);
    };

    const handleCloseNextModal = (status) => {
      setActiveNextModal(status);
    };

    // const handelExport = async () => {
    //   try {
    //     const result = await roadmapData.exportCOSOPageRoadmap(
    //       authContext.accessToken,
    //     );
    //     await window.open(result.config.url, '_blank');
    //     return result;
    //   } catch (e) {
    //     console.log(e);
    //   }
    // };

    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
            {page === 'roadmapAdminAess' &&
              authContext.roleName === 'SUPER-ADMIN' && (
                <Section>
                  <ButtonBorder
                    borderColor={colors.textSky}
                    textColor={colors.textSky}
                    textUpper
                    fontSize={sizes.s}
                    textWeight="med"
                    text={screenWidth >= 768 && 'Create RoadMap'}
                    icon="../../static/images/iconPlus@3x.png"
                    handelOnClick={() =>
                      setAlertModal(
                        'Create RoadMap',
                        '',
                        '',
                        'roadmapAdmin',
                        'Create Now',
                      )
                    }
                  />
                </Section>
              )}
          </Menu.Menu>

          <Menu.Menu position="right">
            {page === 'listAssessment' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setActiveNextModal(true);
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  disabled={isAcceptDisabled}
                  text="next"
                />
              </Section>
            )}

            {page === 'listRequests' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setAlertModal('Confirm COSO roadmap', '', '', 'Confirm');
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  text="next"
                  disabled
                />
              </Section>
            )}

            {page === 'roadmapAdminAess' ||
            page === 'roadmapAdminReq' ||
            page === 'roadmapView' ||
            page === 'listAssessment' ? (
              <Section>
                <ButtonBorder
                  text={screenWidth >= 768 && 'Export'}
                  // width={150}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={handelExport}
                />
              </Section>
            ) : (
              ''
            )}

            {page === 'roadmapAdminAess' && (
              <Section>
                <ButtonAll
                  text={
                    screenWidth < 768 ? 'Submit' : 'Submit to process owner'
                  }
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  disabled={isSubmitToDmDisabled}
                  onClick={submitToDm}
                />
              </Section>
            )}
            {page === 'roadmapAdminReq' && (
              <Section>
                <ButtonAll
                  text={
                    screenWidth < 768 ? 'Submit' : 'Submit to process owner'
                  }
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color="#A4A2BF"
                />
              </Section>
            )}
          </Menu.Menu>

          {/* Modal สำหรับ create roadmap */}
          {page === 'roadmapAdminAess' && (
            <CreateRoadmapModal
              active={active}
              onClose={() => handleCloseCreateRoadmapModal(false)}
              // onCheck={() => setIsCheck(true)}
            />
          )}

          {/* Modal สำหรับ เพิ่มกระบวนการ */}
          {page === 'listAssessment' && (
            <AddProcessModal
              active={activeAddProcessModal}
              onClose={() => setActiveAddProcessModal(false)}
            />
          )}

          {/* Modal สำหรับ next button  */}
          {page === 'listAssessment' && (
            <NextModal
              active={activeNextModal}
              onClose={() => handleCloseNextModal(false)}
              onSubmit={acceptOrRejectRoadmap}
            />
          )}
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
