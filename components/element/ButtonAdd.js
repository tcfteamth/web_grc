import React from 'react';
import styled from 'styled-components';
import { Button, Image } from 'semantic-ui-react';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const ButtonFileName = styled.div`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  padding: ${(props) => props.padding || '0px 8px 0px 16px'} !important;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-color: ${(props) => props.borderColor || '#eaeaea'} !important;
  border-style: solid !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-radius: 6px !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 32}px !important;
  opacity: ${(props) => (props.disabled ? 0.5 : '')};
`;

const ButtonAdd = ({
  disabled,
  text,
  textSize,
  textUpper,
  style,
  onClickRemove,
  href,
}) => (
  <ButtonFileName style={{ margin: 2 }} className="click">
    <a href={href} target="_blank" rel="noopener noreferrer">
      <Text color={colors.textSky} fontSize={textSize} fontWeight={'normal'}>
        {text}
      </Text>
    </a>
    <div style={{ paddingLeft: 16 }} onClick={onClickRemove} className="click">
      {!disabled && (
        <Image
          src={`../../static/images/close-x@3x.png`}
          width={'auto'}
          height={6}
        />
      )}
    </div>
  </ButtonFileName>
);

export default ButtonAdd;
