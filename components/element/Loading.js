import React from 'react'
import { Loader } from 'semantic-ui-react'

const Loading = props => <Loader active inline='centered' style={props.style} />

export default Loading