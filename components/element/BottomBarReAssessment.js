import React, { useEffect, useState } from 'react';
import { Menu, Popup, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
} from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../RoadMapPage/modal';
// import { RoadmapListModel } from '../../model';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    submitToDm,
    isCreateRoadmapDisabled,
    acceptOrRejectRoadmap,
    isAcceptDisabled,
    requestModel,
    reassessmentModel,
  }) => {
    const dataContext = initDataContext();
    const authContext = initAuthStore();
    // const roadmapListModel = useLocalStore(() => RoadmapListModel);
    const [active, setActive] = useState(false);
    const [activeAddProcessModal, setActiveAddProcessModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [mode, setMode] = useState('');
    const [titleText, setTitleText] = useState('');
    const [btnRight, setBtnRight] = useState();

    const [title, setTitle] = useState({ preTitle: '', postTitle: '' });
    const [screenWidth, setScreenWidth] = useState();

    const setAlertModal = (title, preTitle, postTitle, mode, btnRight) => {
      setTitleText(title);
      setTitle({ preTitle, postTitle });
      setMode(mode);
      setActive(true);
      setBtnRight(btnRight);
    };

    const handleCloseCreateRoadmapModal = (status) => {
      setActive(status);
    };

    const handleCloseNextModal = (status) => {
      setActiveNextModal(status);
    };

    

    const createReAssessment = async (token,acceptReAssessmenList) => {
      try {
        const result = await reassessmentModel.createReAssessment(
          authContext.accessToken,acceptReAssessmenList
        );
        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

  
    useEffect(() => {
      setScreenWidth(window.screen.width);
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth),
      );
    }, [screenWidth]);

    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
         
          </Menu.Menu>

          <Menu.Menu position="right">
       
            
              <Section>
                <ButtonAll
                  text={
                     'Re-Assessment' 
                  }
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  disabled={isCreateRoadmapDisabled}
                  onClick={createReAssessment}
                />
              </Section>
            
          
          </Menu.Menu>
       
            <CreateRoadmapModal
              active={active}
              onClose={() => handleCloseCreateRoadmapModal(false)}
            />
          
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
