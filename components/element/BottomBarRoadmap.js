import React, { useEffect, useState } from 'react';
import { Menu, Popup, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
} from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../RoadMapPage/modal';
// import { RoadmapListModel } from '../../model';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    submitToDm,
    isSubmitToDmDisabled,
    acceptOrRejectRoadmap,
    isAcceptDisabled,
    requestModel,
    roadmapModel,
  }) => {
    const dataContext = initDataContext();
    const authContext = initAuthStore();
    // const roadmapListModel = useLocalStore(() => RoadmapListModel);
    const [active, setActive] = useState(false);
    const [activeAddProcessModal, setActiveAddProcessModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [mode, setMode] = useState('');
    const [titleText, setTitleText] = useState('');
    const [btnRight, setBtnRight] = useState();

    const [title, setTitle] = useState({ preTitle: '', postTitle: '' });
    const [screenWidth, setScreenWidth] = useState();

    const setAlertModal = (title, preTitle, postTitle, mode, btnRight) => {
      setTitleText(title);
      setTitle({ preTitle, postTitle });
      setMode(mode);
      setActive(true);
      setBtnRight(btnRight);
    };

    const handleCloseCreateRoadmapModal = (status) => {
      setActive(status);
    };

    const handleCloseNextModal = (status) => {
      setActiveNextModal(status);
    };

    const handleAcceptRoadmap = async () => {
      try {
        const result = await roadmapModel.submitAcceptRoadmaps(
          authContext.accessToken,
        );
        Router.reload();
        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleSubmitToDm = async (token) => {
      try {
        const result = await roadmapModel.submitRoadmapsToDM(
          authContext.accessToken,
        );
        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleSubmitNewRequest = async (token, newRequest) => {
      try {
        const result = await requestModel.submitNewRequest(token, newRequest);

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleExport = async () => {

      try {
        const result = await roadmapModel.exportRoadmap(
          authContext.accessToken,
          1,
          roadmapModel.searchRoadmapText,
          roadmapModel.filterModel.selectedLvl3,
          roadmapModel.filterModel.selectedLvl4,
          roadmapModel.filterModel.selectedBu,
          roadmapModel.filterModel.selectedRoadmapType,
          roadmapModel.filterModel.selectedDepartment.no,
          roadmapModel.filterModel.selectedDivision,
          roadmapModel.filterModel.selectedShift,
          roadmapModel.filterModel.selectedStatus,
          roadmapModel.filterModel.selectedYear,
        );
        await window.open(result.config.url, '_blank');
        return result;
      } catch (e) {
        console.log(e);
      }
    };

    useEffect(() => {
      setScreenWidth(window.screen.width);
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth),
      );
    }, [screenWidth]);

    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
            {page === 'processAdmin' ||
            page === 'riskAndControl' ||
            page === 'Compliance' ? (
              <Section>
                <ButtonBorder
                  text="import"
                  textWeight="med"
                  textColor={colors.primaryBlack}
                  width={160}
                  fontSize={sizes.s}
                  textUpper
                  borderColor={colors.btGray}
                  icon="../../static/images/excel@3x.png"
                />
              </Section>
            ) : (
              ''
            )}

            {page === 'listAssessmenrt' && (
              <Section left>
                <ButtonBorder
                  handelOnClick={() => setActiveAddProcessModal(true)}
                  borderColor="#00aeef"
                  textColor="#00aeef"
                  textWeight="med"
                  textUpper
                  fontSize={sizes.s}
                  text={
                    screenWidth < 768 ? 'Request' : 'Request New Sub-Process'
                  }
                  icon="../../static/images/iconPlus@3x.png"
                />
              </Section>
            )}

            {/* {page === "roadmapAdminAess" || page === "roadmapAdminReq" ? (
              <Section>
                <ButtonBorder
                  borderColor={colors.textSky}
                  textColor={colors.textSky}
                  textUpper
                  textWeight="med"
                  text="Create RoadMap"
                  icon="../../static/images/iconPlus@3x.png"
                  handelOnClick={() =>
                    setAlertModal(
                      "Create RoadMap",
                      "",
                      "",
                      "roadmapAdmin",
                      "Create Now",
                    )
                  }
                />
              </Section>
            ) : (
              ""
            )} */}
            {page === 'roadmapAdminAess' &&
              authContext.roleName === 'SUPER-ADMIN' && (
                <Section>
                  <ButtonBorder
                    borderColor={colors.textSky}
                    textColor={colors.textSky}
                    textUpper
                    fontSize={sizes.s}
                    textWeight="med"
                    text={screenWidth >= 768 && 'Create RoadMap'}
                    icon="../../static/images/iconPlus@3x.png"
                    handelOnClick={() =>
                      setAlertModal(
                        'Create RoadMap',
                        '',
                        '',
                        'roadmapAdmin',
                        'Create Now',
                      )
                    }
                  />
                </Section>
              )}
          </Menu.Menu>

          <Menu.Menu position="right">
            {page === 'listAssessmenrt' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setActiveNextModal(true);
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  disabled={isAcceptDisabled}
                  text="next"
                />
              </Section>
            )}
            {page === 'listRequests' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setAlertModal('Confirm CSA roadmap', '', '', 'Confirm');
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  text="next"
                  disabled
                />
              </Section>
            )}

            {page === 'roadmapAdminAess' ||
            page === 'roadmapAdminReq' ||
            page === 'roadmapView' ? (
              <Section>
                <ButtonBorder
                  text={screenWidth >= 768 && 'Export'}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={() =>
                    handleExport()
                  }
                />
              </Section>
            ) : (
              ''
            )}

            {page === 'roadmapAdminAess' && (
              <Section>
                <ButtonAll
                  text={
                    screenWidth < 768 ? 'Submit' : 'Submit to process owner'
                  }
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  disabled={isSubmitToDmDisabled}
                  onClick={handleSubmitToDm}
                />
              </Section>
            )}
            {page === 'roadmapAdminReq' && (
              <Section>
                <ButtonAll
                  text={
                    screenWidth < 768 ? 'Submit' : 'Submit to process owner'
                  }
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color="#A4A2BF"
                />
              </Section>
            )}
          </Menu.Menu>

          {/* Modal สำหรับ create roadmap */}
          {page === 'roadmapAdminAess' && (
            <CreateRoadmapModal
              active={active}
              onClose={() => handleCloseCreateRoadmapModal(false)}
            />
          )}

          {/* Modal สำหรับ เพิ่มกระบวนการ */}
          {page === 'listAssessmenrt' && (
            <AddProcessModal
              active={activeAddProcessModal}
              onClose={() => setActiveAddProcessModal(false)}
              onSubmitRequest={(newRequest) =>
                // requestModel.submitNewRequest(
                //   authContext.accessToken,
                //   newRequest,
                // )
                handleSubmitNewRequest(authContext.accessToken, newRequest)
              }
            />
          )}

          {/* Modal สำหรับ next button  */}
          {page === 'listAssessmenrt' && (
            <NextModal
              active={activeNextModal}
              onClose={() => handleCloseNextModal(false)}
              onSubmit={handleAcceptRoadmap}
            />
          )}
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
