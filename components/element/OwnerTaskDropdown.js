import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { ownerTaskListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  selectedDepartmentId,
  placeholder,
  tab,
  type,
}) => {
  const ownerTaskListDropdown = useLocalStore(() => ownerTaskListDropdownStore);
  const authContext = initAuthStore();
  const [ownerTaskList, setOwnerTaskList] = useState();

  const getOwnerTask = async (activeTab, activeType) => {
    try {
      const optionsResult = await ownerTaskListDropdown.getOwnerTaskList(
        authContext.accessToken,
      );

      setOwnerTaskList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getOwnerTask();
  }, []);

  const getValue = () => {
    return ownerTaskList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={ownerTaskList}
        options={ownerTaskList}
        value={ownerTaskList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
