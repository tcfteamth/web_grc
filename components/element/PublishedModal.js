import React, { useEffect, useState } from 'react';
import { Button, Grid, Modal, Radio } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text, ButtonBorder, ButtonAll, Box } from './';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose, onSubmit, page, publishedModel }) => {
  const authContext = initAuthStore();
  const [activeTab, setActiveTab] = useState('Published');

  const renderPublishedList = () => {
    return (
      publishedModel.statusList &&
      publishedModel.statusList.map((publishedStatus) => (
        <Box horizontal="row" alignCenter>
          <div style={{ marginRight: '8px' }}>
            <Radio toggle checked={publishedStatus.status} disabled />
          </div>
          <Text fontSize={sizes.s} color={colors.textDarkBlack}>
            {publishedStatus.bu}
          </Text>
        </Box>
      ))
    );
  };

  const renderSignedList = () => {
    return (
      publishedModel.signList &&
      publishedModel.signList.map((signStatus) => (
        <Box horizontal="row" alignCenter>
          <div style={{ marginRight: '8px' }}>
            <Radio toggle checked={signStatus.status} disabled />
          </div>
          <Text fontSize={sizes.s} color={colors.textDarkBlack}>
            {signStatus.bu}
          </Text>
        </Box>
      ))
    );
  };

  return (
    <ModalBox
      size="tiny"
      open={active}
      onClose={() => {
        onClose();
        setActiveTab('Published');
      }}
      closeOnDimmerClick={() => {
        onClose();
        setActiveTab('Published');
      }}
    >
      <Modal.Content style={{ padding: 0 }}>
        <Text center fontSize={sizes.xl} color={colors.primaryBlack}>
          {`${activeTab} ${publishedModel.year}`}
        </Text>

        <Grid centered>
          <Grid.Column
            tablet={8}
            computer={8}
            mobile={16}
            style={{
              paddingBottom: '16px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Button.Group>
              <Button
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: '10px 20px',
                  backgroundColor: `${
                    activeTab === 'Published'
                      ? '#0057b8'
                      : colors.backgroundPrimary
                  }`,
                }}
                onClick={() => setActiveTab('Published')}
              >
                <Text
                  fontWeight="med"
                  fontSize={sizes.s}
                  color={
                    activeTab === 'Published'
                      ? colors.backgroundPrimary
                      : '#0057b8'
                  }
                >
                  Published
                </Text>
              </Button>
              <Button
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: '10px 20px',
                  backgroundColor: `${
                    activeTab === 'Signed'
                      ? '#0057b8'
                      : colors.backgroundPrimary
                  }`,
                }}
                onClick={() => setActiveTab('Signed')}
              >
                <Text
                  fontWeight="med"
                  fontSize={sizes.s}
                  style={{ display: 'flex', alignContent: 'center' }}
                  color={
                    activeTab === 'Signed'
                      ? colors.backgroundPrimary
                      : '#0057b8'
                  }
                >
                  Signed
                </Text>
              </Button>
            </Button.Group>
          </Grid.Column>
        </Grid>

        <div>
          {activeTab === 'Published'
            ? renderPublishedList()
            : renderSignedList()}
        </div>
      </Modal.Content>
    </ModalBox>
  );
};

export default index;
