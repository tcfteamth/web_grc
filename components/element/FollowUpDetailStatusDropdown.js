import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { followUpDetailStatusStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  placeholder,
  followUpId,
}) => {
  const followUpDetailStatus = useLocalStore(() => followUpDetailStatusStore);
  const authContext = initAuthStore();
  const [statusList, setStatusList] = useState();

  const getFollowUpDetailStatus = async () => {
    try {
      const optionsResult = await followUpDetailStatus.getFollowUpDetailStatus(
        authContext.accessToken,
        followUpId,
      );

      setStatusList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getFollowUpDetailStatus();
  }, [followUpId]);

  const getValue = () => {
    return statusList.find((s) => s.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={statusList}
        options={statusList}
        value={statusList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
