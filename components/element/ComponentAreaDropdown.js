import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { areaListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';
import { ComponentArea } from '../../utils/static';

const index = ({ value, handleOnChange, disabled, placeholder }) => {
  const componentAreaDropdown = useLocalStore(() => areaListDropdownStore);
  const authContext = initAuthStore();
  const [ComponentAreaList, setComponentArea] = useState();

  const getComponentArea = async () => {
    try {
      const optionsResult = await componentAreaDropdown.getAreaListDropdownList(
        authContext.accessToken,
      );
      setComponentArea(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getComponentArea();
  }, []);

  const getValue = () => {
    return (
      ComponentAreaList.find((assessor) => assessor.value === value) || null
    );
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={ComponentAreaList}
        options={ComponentAreaList}
        value={ComponentAreaList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
