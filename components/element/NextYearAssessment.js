import React from 'react';
import styled from 'styled-components';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const NextYearAssessment = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background: ${colors.textBrightGray};
  display: flex;
  justify-content: center;
  align-items: center;
`;

const index = ({ year }) => {
  return (
    <NextYearAssessment>
      <Text
        color={colors.backgroundPrimary}
        fontSize={sizes.xs}
        fontWeight="med"
      >
        {year}
      </Text>
    </NextYearAssessment>
  );
};

export default index;
