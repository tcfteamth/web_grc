import React, { useState, useEffect, useReducer } from 'react';
import { Dropdown, Menu, Image, Icon, Input } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';
import { Text } from '.';

const InputStyle = styled(Input)`
  display: flex !important;
  flex-direction: row !important;
  align-items: center !important;
  width: ${(props) => props.width || 100}% !important;
  height: ${(props) => props.height || 38}px !important;
  font-size: ${sizes.xs}px !important;
  font-weight: normal;
  border-radius: 6px 6px 0px 6px !important;
  border: 1px solid #eaeaea !important;
  background-color: #ffffff !important;
  color: ${colors.primaryBlack};
  &:placeholder {
    color: ${(props) => (props.disabled ? colors.textSky : colors.red)};
  }
`;

const InputAll = ({
  placeholder,
  handleOnChange,
  width,
  disabled,
  defaultValue,
  onClick,
}) => (
  <InputStyle
    style={{
      fontSize: sizes.xs,
      minHeight: 34,
    }}
    action={
      <div className="click" style={{ display: 'flex' }} onClick={onClick}>
        <div
          style={{
            backgroundColor: colors.btGray,
            width: '1px',
            minHeight: '10px',
          }}
        />
        <Text style={{ padding: '0px 16px' }} color={colors.textSky}>
          Library
        </Text>
      </div>
    }
    width={width}
    placeholder={placeholder || true}
    onChange={handleOnChange}
    disabled={disabled}
    defaultValue={defaultValue}
  />
);

export default InputAll;
