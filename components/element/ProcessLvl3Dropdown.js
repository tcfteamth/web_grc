import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { lvl3ListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const lvl3ListDropdown = useLocalStore(() => lvl3ListDropdownStore);
  const authContext = initAuthStore();
  const [lvl3List, setLvl3List] = useState();

  const getRoadmapType = async () => {
    try {
      const optionsResult = await lvl3ListDropdown.getRoadmapLvl3List(
        authContext.accessToken,
      );

      setLvl3List(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRoadmapType();
  }, []);

  const getValue = () => {
    return lvl3List.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={lvl3List}
        options={lvl3List}
        value={lvl3List && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
