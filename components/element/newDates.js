import React, { useState } from 'react';
import Flatpickr from 'react-flatpickr';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { colors, sizes } from '../../utils';

const Box = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  border: ${(props) => (props.onError ? '1px solid red' : '')};
`;

const Styled = styled(Flatpickr)`
  display: flex !important;
  flex-direction: row !important;
  align-items: center !important;
  width: ${(props) => props.width || 100}% !important;
  height: ${(props) => props.height || 38}px !important;
  font-size: ${sizes.xs}px !important;
  font-weight: 500;
  border: ${(props) => (props.onError ? '2px solid red' : '')} !important;
  background-color: #ffffff !important;
  border-radius: 6px 6px 0px 6px !important;
  padding-left: 16px;
  color: ${colors.primaryBlack};
  &:placeholder {
    color: ${(props) => (props.disabled ? colors.textSky : colors.red)};
  }
`;

const index = ({
  onChange,
  value,
  options,
  onError,
  disabled,
  placeholder,
}) => {
  return useObserver(() => (
    <Box onError={onError}>
      <Styled
        value={value}
        onChange={onChange}
        dateFormat="d-m-Y"
        options={options}
        disabled={disabled}
        placeholder={placeholder}
      />
    </Box>
  ));
};

export default index;
