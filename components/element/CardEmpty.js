import React from 'react';
import { Button, Image } from 'semantic-ui-react';
import Link from 'next/link';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';
import { Text, ButtonBorder } from '.';

const Card = styled.div`
  width: 100%;
  height: 268px;
  border-radius: 6px;
  box-shadow: 0 2px 10px 0 rgba(48, 48, 48, 0.04);
  background-color: #fefeff;
  display: flex;
  align-self: center !important;
  flex-direction: column;
  align-content: center !important;
  align-items: center !important;
  justify-content: ${(props) => (props.center ? 'center' : 'space-between')};

  @media only screen and (min-width: 280px) {
    padding: 20px;
  }

  @media only screen and (min-width: 768px) {
    padding: 20px;
  }

  @media only screen and (min-width: 1024px) {
    padding: 40px 150px;
  }
`;

const index = ({ textTitle, Icon, textButton, link, handelOnClick }) => {
  return (
    <Card center={!textButton}>
      {Icon && (
        <Image
          src={Icon}
          style={{ marginRight: 12, width: 'auto', height: 80 }}
        />
      )}
      <Text
        style={{ paddingTop: 16, paddingBottom: 24 }}
        color={colors.textlightGray}
        fontWeight={'med'}
        fontSize={sizes.xl}
      >
        {textTitle || 'text'}
      </Text>
      {textButton && (
        <div style={{ cursor: 'pointer' }}>
          {link ? (
            <Link href={link}>
              <ButtonBorder
                fontSize={sizes.s}
                width={230}
                height={40}
                text={textButton}
                icon={'../../static/images/iconPlus@3x.png'}
                textUpper
                textWeight={'med'}
                textColor={colors.textSky}
                borderColor={colors.textSky}
                handelOnClick={handelOnClick}
              />
            </Link>
          ) : (
            <ButtonBorder
              fontSize={sizes.s}
              width={230}
              height={40}
              text={textButton}
              icon={'../../static/images/iconPlus@3x.png'}
              textUpper
              textWeight={'med'}
              textColor={colors.textSky}
              borderColor={colors.textSky}
              handelOnClick={handelOnClick}
            />
          )}
        </div>
      )}
    </Card>
  );
};

export default index;
