import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import { DropdownSelect } from '.';
import { followUpYearListStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
  model,
}) => {
  const followUpYearList = useLocalStore(() => followUpYearListStore);
  const authContext = initAuthStore();
  const [yearList, setYearList] = useState();

  const getYearList = async () => {
    try {
      const optionsResult = await followUpYearList.getFollowUpYearList();

      model.setField('selectedYear', followUpYearList.getFirstOption.value);
      console.log('optionsResult', optionsResult);
      setYearList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getYearList();
  }, []);

  const getValue = () => {
    return yearList.find((year) => year.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={defaultValue}
        options={yearList}
        value={yearList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
