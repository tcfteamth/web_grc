import React from 'react';
import styled from 'styled-components';

const Cell = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
  padding-left: ${(props) => props.left || 0}px;
  justify-content: ${(props) => (props.center ? 'center' : 'flex-start')};

  @media only screen and (min-width: 320px) {
    width: ${(props) => props.width}px;
  }
  @media only screen and (min-width: 1024px) {
    width: ${(props) => props.width}%;
  }
`;

const TabScrollBar = ({
  left,
  around,
  right,
  center,
  between,
  mid,
  width,
  style,
  key,
  children,
}) => {
  return (
    <Cell
      style={style}
      width={width}
      key={key}
      mid={mid}
      left={left}
      right={right}
      center={center}
      between={between}
      around={around}
    >
      {children}
    </Cell>
  );
};

export default TabScrollBar;
