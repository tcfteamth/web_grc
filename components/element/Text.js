import React from 'react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const Text = styled.div`
  margin-top: 2px;
  margin-bottom: 2px;
  font-family: 'Roboto', sans-serif;
  font-style: normal;
  ${(props) => props.center && 'text-align: center;'}
  color: ${(props) => props.color || colors.textlightGray};
  font-size: ${(props) => props.fontSize || `${sizes.s}`}px;
  font-weight: ${(props) =>
    props.fontWeight === 'med'
      ? 600
      : props.fontWeight === 'bold'
      ? 700
      : props.fontWeight === 'light'
      ? 200
      : 500};
  text-decoration: ${(props) =>
    props.overline
      ? 'overline'
      : props.linethrough
      ? 'line-through'
      : props.underline
      ? 'underline'
      : 'initial'};
  overflow: ${(props) => (props.lineClamp ? 'hidden' : 'initial')};
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: ${(props) => props.lineClamp || 'initial'};
  -webkit-box-orient: vertical;
  ${({ breakWord }) => breakWord && 'word-break: break-word;'}
`;
export default (props) => <Text {...props} />;
