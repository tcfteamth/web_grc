import React, { useEffect, useState } from 'react';
import { Menu, Popup, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
} from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../RoadMapPage/modal';
// import { RoadmapListModel } from '../../model';
import useWindowDimensions from '../../hook/useWindowDimensions';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;
const BottombarLogo = styled(Menu)`
  height: 55px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  background-color: transparent !important;
  border: transparent !important;
  border-top: transparent !important;
  box-shadow: none !important;
`;

const BottomBar = observer(
  ({
    detailReportModel,
    manageRoleModel,
    filterModel,
    isFilterReport,
    reportEx,
    onSaveReport,
  }) => {
    const authContext = initAuthStore();
    // const roadmapListModel = useLocalStore(() => RoadmapListModel);
    const [active, setActive] = useState(false);
    const [activeAddProcessModal, setActiveAddProcessModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const { width: screenWidth } = useWindowDimensions();

    const handleCloseCreateRoadmapModal = (status) => {
      setActive(status);
    };

    const handleCloseNextModal = (status) => {
      setActiveNextModal(status);
    };

    return useObserver(() => (
      <div>
        {/* Logo for print  */}
        <BottombarLogo className="show-in-print" fluid fixed="bottom">
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
              paddingTop: 16,
            }}
          >
            <Image
              src="../../static/images/gc-logo-4-x@3x.png"
              style={{ width: 100, height: 'auto' }}
            />
          </div>
        </BottombarLogo>
        <Bottombar
          className="pl-Bottombar no-print"
          fluid
          fixed="bottom"
          size="large"
        >
          <Menu.Menu position="left">
            {isFilterReport && (
              <Section>
                <ButtonBorder
                  text={screenWidth >= 768 && 'Export'}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={reportEx}
                />
              </Section>
            )}

            {isFilterReport && (
              <Section>
                <ButtonBorder
                  text={screenWidth >= 768 && 'Export'}
                  width={150}
                  textWeight="med"
                  textUpper
                  textColor={colors.redExport}
                  fontSize={sizes.s}
                  borderColor={colors.redExport}
                  icon="../../static/images/pdf@3x.png"
                  handelOnClick={() => {
                    window.print();
                  }}
                />
              </Section>
            )}
          </Menu.Menu>

          <Menu.Menu position="right">
            {filterModel.selectedBu && isFilterReport && (
              <Menu.Menu position="right">
                <Section>
                  <ButtonAll
                    text="SAVE"
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.primary}
                    onClick={onSaveReport}
                    disabled={!manageRoleModel.enableSave}
                  />
                </Section>
              </Menu.Menu>
            )}
          </Menu.Menu>
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
