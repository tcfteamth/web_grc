import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import Router from 'next/router';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import { ButtonBorder, Text, ButtonAll, RadioBox } from '.';
import {
  SendToModalCOSO,
  SaveModal,
  NextModal,
  SendToStaffModal,
  ValidateModal,
} from '../AssessmentPage/modal';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    assessmentData,
    saveButtonData,
    submitToDm,
    isSubmitToDmDisabled,
    acceptOrRejectRoadmap,
    isAcceptDisabled,
    exportData,
  }) => {
    const dataContext = initDataContext();
    const authContext = initAuthStore();
    const [saveModal, setSaveModal] = useState(false);
    const [sendToModal, setSendToModal] = useState(false);
    const [activeAdminModal, setActiveAdminModal] = useState(false);
    const [activeValidateModal, setActiveValidateModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [activeSendToStaffModal, setActiveSendToStaffModal] = useState(false);

    const handleCloseNextModal = (status) => {
      setActiveNextModal(status);
    };

    const handleRejectToStaff = async () => {
      try {
        const result = await assessmentData.cosoRejectToStaff(
          authContext.accessToken,
        );

        Router.replace('/COSO_Assessment/COSOAssessmentList');

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handelExport = async () => {
      try {
        const result = await assessmentData.submitExportAssessment(
          authContext.accessToken,
          Router.query.id,
        );

        await window.open(result.config.url, '_blank');
        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const renderButtonLeft = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {assessmentData &&
          assessmentData.buttonBar.map((item) => (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {item.buttonName === 'EXPORT EXCEL' && item.isShow && (
                <Section left>
                  <ButtonBorder
                    text="Export"
                    textUpper
                    textWeight="med"
                    textColor={colors.greenExport}
                    textSize={sizes.s}
                    width={160}
                    borderColor={colors.greenExport}
                    icon="../../static/images/excel@3x.png"
                    disabled={!item.canClick}
                    handelOnClick={handelExport}
                  />
                </Section>
              )}
            </div>
          ))}
      </div>
    );

    const renderButtonRight = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {assessmentData &&
          assessmentData.buttonBar.map((item) => (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {item.buttonName === 'SENT TO STAFF' && item.isShow && (
                <Section>
                  <ButtonAll
                    text={item.buttonName}
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.textSky}
                    onClick={() => setActiveSendToStaffModal(true)}
                    disabled={!item.canClick}
                  />
                </Section>
              )}

              {item.buttonName === 'SAVE' && item.isShow && (
                <Section>
                  <ButtonAll
                    text={`${item.buttonName}`}
                    width={140}
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.textPurple}
                    onClick={saveButtonData}
                    disabled={!item.canClick}
                  />
                </Section>
              )}
              {item.buttonName === 'READY TO SUBMIT' && item.isShow && (
                <div style={{ display: 'flex', paddingLeft: 16 }}>
                  <RadioBox
                    onChange={() => {
                      assessmentData.setValidateAssessmentForm();
                      if (assessmentData.canCheckReadyToSubmit) {
                        assessmentData.approveStatus.setIsReadyToSubmit(
                          !assessmentData.isReadyToSubmit,
                        );
                        assessmentData.setField(
                          'isReadyToSubmit',
                          !assessmentData.isReadyToSubmit,
                        );
                      } else {
                        setActiveValidateModal(true);
                      }
                    }}
                    checked={assessmentData.isReadyToSubmit}
                  />
                  <Text
                    className="upper"
                    fontSize={sizes.s}
                    fontWeight="med"
                    color={colors.textBlack}
                    style={{ paddingLeft: 8 }}
                  >
                    Ready to submit
                  </Text>
                </div>
              )}
            </div>
          ))}
      </div>
    );

    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
            {page === 'COSOAssessmentDetail' && renderButtonLeft()}
          </Menu.Menu>

          <Menu.Menu position="right">
            {page === 'COSOAssessmentDetail' && renderButtonRight()}
            {!authContext.roles.isDM &&
              !authContext.roles.isVP &&
              !authContext.roles.isAdmin &&
              page === 'AssessmentTodoList' && (
                <Section>
                  <ButtonAll
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    disabled={!assessmentData.canSubmitAssessment}
                    color={colors.textPurple}
                    onClick={() => setSendToModal(true)}
                  />
                </Section>
              )}
            {page === 'COSOAssessmentTodoListDM' && authContext.roles.isDM && (
              <Section>
                <ButtonAll
                  text="submit"
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  disabled={isSubmitToDmDisabled}
                  onClick={() => setSendToModal(true)}
                />
              </Section>
            )}
            {page === 'AssessmentTodoListStaff' && (
              <Section>
                <ButtonAll
                  text="submit"
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  disabled={isSubmitToDmDisabled}
                  onClick={() => setSendToModal(true)}
                />
              </Section>
            )}
            {page === 'COSOAssessmentVP' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setActiveNextModal(true);
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  disabled={isAcceptDisabled}
                  text="next"
                />
              </Section>
            )}

            {page === 'AssessmentTodoListAdmin' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => {
                    setActiveAdminModal(true);
                  }}
                  width={160}
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  disabled={isSubmitToDmDisabled}
                  text="next"
                />
              </Section>
            )}
            {page === 'AssessmentSummaryAdmin' && (
              <Section>
                <ButtonBorder
                  text="EXPORT"
                  textUpper
                  textWeight="med"
                  textColor={colors.greenExport}
                  textSize={sizes.s}
                  width={160}
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                  handelOnClick={exportData}
                />
              </Section>
            )}
          </Menu.Menu>

          {/* Modal สำหรับ save */}
          {/* COSO SAVE Detail */}
          {/* {page === 'COSOAssessmentDetail' && (
            <SaveModal
              active={saveModal}
              onClose={() => setSaveModal(false)}
              // closeOnDimmerClick={saveModal}
              onSubmit={saveButtonData}
            />
          )} */}

          {/* staff send to DM  and DM send to VP */}
          <SendToModalCOSO
            active={sendToModal}
            onClose={() => setSendToModal(false)}
            onSubmit={submitToDm}
          />

          {page === 'AssessmentTodoListAdmin' && (
            <NextModal
              active={activeAdminModal}
              onClose={() => setActiveAdminModal(false)}
              onSubmit={submitToDm}
            />
          )}

          {/* Modal สำหรับ next button  */}
          {page === 'COSOAssessmentVP' && (
            <NextModal
              active={activeNextModal}
              onClose={() => handleCloseNextModal(false)}
              onSubmit={acceptOrRejectRoadmap}
            />
          )}

          <SendToStaffModal
            active={activeSendToStaffModal}
            onClose={() => setActiveSendToStaffModal(false)}
            onSubmit={handleRejectToStaff}
          />

          <ValidateModal
            active={activeValidateModal}
            onClose={() => setActiveValidateModal(false)}
          />
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
