import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { statusListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  selectedDepartmentId,
  placeholder,
  tab,
  type,
}) => {
  const statusListDropdown = useLocalStore(() => statusListDropdownStore);
  const authContext = initAuthStore();
  const [statusList, setStatusList] = useState();

  const getRoadmapType = async (activeTab, activeType) => {
    try {
      const optionsResult = await statusListDropdown.getAllStatus(
        authContext.accessToken,
        activeTab,
        activeType,
      );

      setStatusList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRoadmapType(tab, type);
  }, [tab]);

  const getValue = () => {
    return statusList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={statusList}
        options={statusList}
        value={statusList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
