import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { bulistDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  selectedYear,
  placeholder,
}) => {
  const buDropdown = useLocalStore(() => bulistDropdownStore);
  const authContext = initAuthStore();
  const [buList, setBuList] = useState(); 

  const getBuList = async (year) => {
    try {
      const currentYear = new Date().getFullYear();
      if(year === null || year === undefined || year === '' || year === currentYear){
        const  optionsResult = await buDropdown.getBUList(authContext.accessToken); 
        setBuList(optionsResult);
      }else{
        const optionsResult = await buDropdown.getBUOldList(authContext.accessToken,year); 
        setBuList(optionsResult);
      }
     
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    getBuList(selectedYear);
  }, [selectedYear]);

  const getValue = () => {
    return buList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDDL = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={buList}
        options={buList}
        value={buList && getValue()}
        handleOnChange={onSelectedDDL}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
