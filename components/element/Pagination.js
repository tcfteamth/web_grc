/* eslint-disable react/prop-types */
/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import { Pagination, Icon } from "semantic-ui-react";
import styled from "styled-components";
import { Text } from "../../components/element";
import { colors, sizes } from "../../utils";

const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: ${colors.bgPrimary};
  padding-top: 16px;
`;

export default ({
  activepage,
  handlepaginationchange,
  totalpages,
  totalelement,
}) => (
  <Container align="right">
    <Pagination
      pointing
      secondary
      activePage={activepage}
      onPageChange={handlepaginationchange}
      totalPages={totalpages}
      firstItem={null}
      lastItem={null}
      ellipsisItem={{
        content: (
          <Icon name="ellipsis horizontal" style={{ color: colors.darkGray }} />
        ),
        icon: true,
      }}
      prevItem={{
        content: <Icon name="caret left" style={{ color: colors.darkGray }} />,
      }}
      nextItem={{
        content: <Icon name="caret right" style={{ color: colors.darkGray }} />,
      }}
      pageItem={{ color: "blue" }}
    />
  </Container>
);
