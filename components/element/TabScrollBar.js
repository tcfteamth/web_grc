import React, { Children } from 'react';
import styled from 'styled-components';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
`;

const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
  @media only screen and (min-width: 320px) {
    width: auto !important;
  }
  @media only screen and (min-width: 1024px) {
    width: 100% !important;
  }
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  border-radius: 6px 0px 6px 6px !important;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const TabScrollBar = ({ children }) => {
  return (
    <CardAll className="w-highlight">
      <TableScroll>
        <TabScroll>{children}</TabScroll>
      </TableScroll>
    </CardAll>
  );
};

export default TabScrollBar;
