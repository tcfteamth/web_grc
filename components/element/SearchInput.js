import React from 'react';
import styled from 'styled-components';
import { Image, Form } from 'semantic-ui-react';
import { colors, sizes } from '../../utils';

const SearchInput = styled.input`
  font-size: ${sizes.xs}px;
  color: #333333 !important;
  :placeholder {
    color: #b1b3aa !important;
  }
  @media only screen and (min-width: 320px) {
    min-width: 200px;
  }
  @media only screen and (min-width: 768px) {
    min-width: 130px;
  }
  @media only screen and (min-width: 1440px) {
    min-width: ${(props) => (props.width ? props.width : 250)}px;
  }
  @media only screen and (min-width: 1920px) {
    min-width: 280px;
  }
`;
const LineSearch = styled.div`
  flex-direction: row;
  justify-content: flex-start;
  display: flex;
  align-items: center;
  border-bottom: 2px solid #b1b3aa;
  @media only screen and (min-width: 320px) {
    min-width: 200px;
  }
  @media only screen and (min-width: 768px) {
    min-width: 130px;
  }
  @media only screen and (min-width: 1440px) {
    min-width: ${(props) => (props.width ? props.width : 200)}px;
  }
`;

const index = ({ onChange, value, onEnterKeydown, placeholder, width }) => (
  <LineSearch width={width}>
    <Image
      src="../../static/images/iconSearch@3x.png"
      style={{ marginRight: 12, width: 18, height: 18 }}
    />
    <div className="ui transparent input">
      <SearchInput
        type="text"
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        onKeyDown={onEnterKeydown}
      />
    </div>
  </LineSearch>
);

export default index;
