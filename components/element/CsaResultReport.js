import React, { memo } from 'react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text, OverAllRatingStatus } from '.';
import { colors, sizes } from '../../utils';
import useWindowDimensions from '../../hook/useWindowDimensions';
import { Popup } from 'semantic-ui-react';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: ${(props) => props.width - 65}px;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 10px 0px;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  width: auto;
  height: 100%;
  padding: 6px 0px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width}px;
  flex-wrap: wrap;
  padding-right: 24px;
  justify-content: ${(props) => (props.center ? 'center' : 'start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

// rotateX
const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const headers = [
  { key: 'Level3', render: 'Level 3', width: 150 },
  { key: 'Process', render: 'Process', width: 200 },
  { key: 'Level4', render: 'Level 4', width: 150 },
  { key: 'Sub-process', render: 'Sub Process', width: 200 },
  { key: 'Division', render: 'Division', width: 150 },
  { key: 'Overall Rating', render: 'Overall Rating', width: 150 },
  { key: 'Enhancement', render: 'Enhancement', width: 150 },
];

const RiskAndControlList = ({ departmentListModel, filterModel }) => {
  const { width: screenWidth } = useWindowDimensions();
  const realWidth = screenWidth - 64;
  const newWidth = realWidth / headers.length;

  const renderCardHeaderTable = () => (
    <div style={{ borderBottom: '1px solid #333333' }}>
      <Div left={10}>
        {headers.map(({ render, key, width }) => (
          <TableHeader key={key}>
            <Cell center width={newWidth}>
              <Text
                fontSize={sizes.xs}
                fontWeight="med"
                color={colors.textlightGray}
              >
                {render}
              </Text>
            </Cell>
          </TableHeader>
        ))}
      </Div>
    </div>
  );

  return useObserver(() => (
    <div className="w-table">
      <CardAll width={screenWidth}>
        <CardTop>
          <Text
            fontSize={sizes.s}
            fontWeight="bold"
            color={colors.primaryBackground}
          >
            {`CSA Result ${filterModel.selectedYear}_${departmentListModel.name}`}
          </Text>
        </CardTop>
        <TableScroll>
          <TabScroll>
            <Div col>
              {renderCardHeaderTable()}
              {departmentListModel.tables.csaResult.map(
                (result, resultIndex) => (
                  <TableBody key={result.no}>
                    <Div left={10}>
                      <Cell width={newWidth} center>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {result.lv3No}
                        </Text>
                      </Cell>
                      <Cell width={newWidth}>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {result.lv3Name}
                        </Text>
                      </Cell>
                      <Div col>
                        {result.subProcesses.map((subProcesses) => (
                          <Div col bottom={8}>
                            <Div>
                              <Cell width={newWidth} center>
                                <Text
                                  fontSize={sizes.xs}
                                  fontWeight="bold"
                                  color={colors.primaryBlack}
                                >
                                  {subProcesses.lv4No}
                                </Text>
                              </Cell>
                              <Cell width={newWidth}>
                                <Text
                                  fontSize={sizes.xs}
                                  color={colors.primaryBlack}
                                >
                                  {subProcesses.lv4Name}
                                </Text>
                              </Cell>
                              <Cell width={newWidth} center>
                                <Popup
                                  trigger={
                                    <Text
                                      fontSize={sizes.xs}
                                      fontWeight="bold"
                                      color={colors.primaryBlack}
                                    >
                                      {subProcesses.indicator}
                                    </Text>
                                  }
                                  content={`${subProcesses.divEn || '-'} ${subProcesses.shiftEn || ''}`}
                                  size="small"
                                />
                              </Cell>
                              <Cell width={newWidth} center>
                                <OverAllRatingStatus
                                  overAllstatus={subProcesses.overAllRatingId}
                                />
                              </Cell>
                              <Cell width={newWidth} center>
                                {subProcesses.enhancement || '-'}
                              </Cell>
                            </Div>
                          </Div>
                        ))}
                      </Div>
                    </Div>

                    {resultIndex + 1 !==
                      departmentListModel.tables.csaResult.length && (
                      <DividerLine bottom />
                    )}
                  </TableBody>
                ),
              )}
            </Div>
          </TabScroll>
        </TableScroll>
      </CardAll>
    </div>
  ));
};

export default memo(RiskAndControlList);
