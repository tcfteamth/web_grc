import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import request from '../../services';
import { NewAssignModal } from '../SettingPage';
import Router from 'next/router';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { ButtonBorder } from '.';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(({ page }) => {
  const [activeNewAssignModal, setActiveNewAssignModal] = useState(false);
  const authContext = initAuthStore();

  const handleExportData = async () => {
    try {
      const result = await cosoDB.exportCOSODatabase(authContext.accessToken);
      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div>
      <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
        <Menu.Menu position="left">
          <Section>
            <ButtonBorder
              borderColor={colors.textSky}
              textColor={colors.textSky}
              textUpper
              fontSize={sizes.s}
              textWeight="med"
              text="Create New Assign"
              icon="../../static/images/iconPlus@3x.png"
              handelOnClick={() => setActiveNewAssignModal(true)}
            />
          </Section>
        </Menu.Menu>
      </Bottombar>

      <NewAssignModal
        active={activeNewAssignModal}
        onClose={() => setActiveNewAssignModal(false)}
      />
    </div>
  ));
});

export default BottomBar;
