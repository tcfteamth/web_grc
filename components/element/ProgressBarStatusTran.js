import React, { useEffect, useState } from 'react';
import { ProgressBar, Step } from 'react-step-progress-bar';
import 'react-step-progress-bar/styles.css';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import { Popup } from 'semantic-ui-react';
import { isNumber } from 'lodash';

const Oval = styled.div`
  width: ${(props) => props.width || 18}px;
  height: ${(props) => props.height || 18}px;
  border-radius: 50%;
  background-color: ${(props) => props.bgcolor || colors.backgroundPrimary};
  color: white;
  font-size: ${(props) => props.fontSize || sizes.m}px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Span = styled.span`
  color: ${(props) => props.textColor || colors.textBlack};
  font-size: ${sizes.xs}px;
  font-weight: bold;
`;
const spanDefault = [
  { key: 1, label: '1', value: 0 },
  { key: 2, label: '2', value: 1 },
  { key: 3, label: '3', value: 2 },
  { key: 4, label: '4', value: 1 },
  { key: 5, label: '5', value: 2 },
];

const ProgressBarStatusTran = ({ data, mode }) => {
  const [isPercent, setIsPercent] = useState();

  const widthDependOnDotMode = () => {
    if (mode === 'card') {
      return 40;
    }

    if (mode === 'dashboard') {
      return 24;
    }
  };

  const fontSizeDependOnDotMode = () => {
    if (mode === 'card') {
      return sizes.m;
    }

    if (mode === 'dashboard') {
      return sizes.xxs;
    }
  };

  const renderDotStatus = (item, index) => {
    return (
      <Popup
        size={5}
        style={{
          borderRadius: '6px 6px 0px 6px',
          marginLeft: -10,
        }}
        trigger={
          <Oval
            bgcolor={
              item.isReach
                ? `${
                    index === 0
                      ? colors.textpurplepink
                      : index === 1
                      ? colors.textpralpurple
                      : index === 2
                      ? colors.textpralblue
                      : index === 3
                      ? colors.textbrightbule
                      : index === 4
                      ? colors.textbluebright
                      : colors.btGray
                  }`
                : colors.btGray
            }
            width={widthDependOnDotMode()}
            height={widthDependOnDotMode()}
            fontSize={fontSizeDependOnDotMode()}
          >
            {(mode === 'card' || mode === 'dashboard') && item.count}
          </Oval>
        }
      >
        <Span
          textColor={
            item.isReach
              ? `${
                  index === 0
                    ? colors.textpurplepink
                    : index === 1
                    ? colors.textpralpurple
                    : index === 2
                    ? colors.textpralblue
                    : index === 3
                    ? colors.textbrightbule
                    : index === 4
                    ? colors.textbluebright
                    : colors.btGray
                }`
              : colors.btGray
          }
        >
          {item.statusName || item.status || item.count}
        </Span>
      </Popup>
    );
  };

  let sum = 0;
  const handelPercent = (data) => {
    data &&
      data.map((item, index) => {
        if (item.isReach === true) {
          sum = sum + 35;
        }
      });
    setIsPercent(sum);
  };

  useEffect(() => {
    handelPercent(data);
  }, [data]);

  return (
    <ProgressBar
      filledBackground="linear-gradient(to right, #626262, #626262)"
      percent={isPercent - 35}
    >
      {data
        ? data.map((item, index) => (
            <Step transition="scale">
              {() => (
                <div>
                  {item.isReach ? (
                    renderDotStatus(item, index)
                  ) : (
                    <Oval
                      bgcolor={
                        item.isReach ? colors.textpurplepink : colors.btGray
                      }
                      width={widthDependOnDotMode()}
                      height={widthDependOnDotMode()}
                    >
                      {(mode === 'card' || mode === 'dashboard') && '-'}
                    </Oval>
                  )}
                </div>
              )}
            </Step>
          ))
        : spanDefault.map(() => (
            <Step transition="scale">
              {() => <Oval bgcolor={colors.btGray} />}
            </Step>
          ))}
    </ProgressBar>
  );
};

export default ProgressBarStatusTran;
