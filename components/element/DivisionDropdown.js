import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { divisionListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  selectedDepartmentId,
  selectedDepartmentNo,
  selectedYear,
  placeholder,
}) => {
  const divisionListDropdown = useLocalStore(() => divisionListDropdownStore);
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();

  const getDivisionList = async (departmentId,departmentNo,year) => {
    try {
      const currentYear = new Date().getFullYear();
      if(year === null || year === undefined || year === '' || year === currentYear){
      if (departmentId) {
        const optionsResult = await divisionListDropdown.getDivisionList(
          departmentId,
        );

        setDepartmentList(optionsResult);
      } else {
        const optionsResult = await divisionListDropdown.getDivisionWithoutId(
          authContext.accessToken,
        );

        setDepartmentList(optionsResult);
      }
      }else{
        const optionsResult = await divisionListDropdown.getDivisionOldList(
          departmentNo,
          year,
        );
        setDepartmentList(optionsResult);   
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getDivisionList(selectedDepartmentId,selectedDepartmentNo,selectedYear);
  }, [selectedDepartmentId,selectedDepartmentNo,selectedYear]);

  const getValue = () => {
    return departmentList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList && departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
