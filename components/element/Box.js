import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { sizes, colors } from '../../utils';

const calJustifyContent = (props) => {
  if (props.spaceBetween) {
    return 'space-between';
  }
  if (props.spaceAround) {
    return 'space-around';
  }
  if (props.end) {
    return 'flex-end';
  }
  if (props.center) {
    return 'center';
  }
  if (props.stretch) {
    return 'stretch';
  }
  return 'flex-start';
};

const calAlignItems = (props) => {
  if (props.alignEnd) {
    return 'flex-end';
  }
  if (props.alignCenter) {
    return 'center';
  }
  if (props.alignStretch) {
    return 'stretch';
  }
  if (props.alignBaseline) {
    return 'baseline';
  }
  if (props.selfStart) {
    return 'flex-start';
  }
  return 'auto';
};

const calAlignSelf = (props) => {
  if (props.selfEnd) {
    return 'flex-end';
  }
  if (props.selfCenter) {
    return 'center';
  }
  if (props.selfStretch) {
    return 'stretch';
  }
  if (props.selfBaseline) {
    return 'baseline';
  }
  return 'auto';
};

const calWrap = (props) => {
  if (props.noWrap) {
    return 'nowrap';
  }
  return 'wrap';
};

const possibleSize = ['Xs', 'Sm', 'Md', 'Lg', 'Xl', 'XXl', 'XXXl'];
const calSide = (props, propsPrefix, cssAttr) =>
  possibleSize
    .map((size) =>
      props[propsPrefix + size]
        ? `${cssAttr}: ${sizes[size.toLowerCase()]};`
        : undefined,
    )
    .filter((p) => p)
    .join('\n');

const calMarginPadding = (props) => {
  const mpStyle = `
        ${calSide(props, 'm', 'margin')}
        ${calSide(props, 'p', 'padding')}
        ${calSide(props, 'mt', 'margin-top')}
        ${calSide(props, 'mr', 'margin-right')}
        ${calSide(props, 'mb', 'margin-bottom')}
        ${calSide(props, 'ml', 'margin-left')}
        ${calSide(props, 'pt', 'padding-top')}
        ${calSide(props, 'pr', 'padding-right')}
        ${calSide(props, 'pb', 'padding-bottom')}
        ${calSide(props, 'pl', 'padding-left')}
    `;
  return mpStyle;
};

const StyledBox = styled.div`
  ${(props) => (props.contentsOverlaps ? 'position: relative;' : '')}
  display: ${(props) => (props.contentsOverlaps ? 'block' : 'flex')};
  flex-direction: ${(props) => (props.horizontal ? 'row' : 'column')};
  flex: ${(props) => props.flex};
  justify-content: ${calJustifyContent};
  align-items: ${calAlignItems};
  align-self: ${calAlignSelf};
  flex-wrap: ${calWrap};
  ${(props) => props.minWidth && `min-width: ${props.minWidth};`}
  ${(props) => props.minHeight && `min-height: ${props.minHeight};`}
    ${(
    props,
  ) => props.width && `width: ${props.width};`}
    ${(props) =>
    props.height && `height: ${props.height};`}
    ${(props) =>
    props.withBackground &&
    `background-color: ${
      typeof props.withBackground === 'boolean'
        ? colors.foreground
        : props.withBackground
    }`}
    ${(props) =>
    props.bordered &&
    `border: 1px solid ${colors.border};`}
    ${calMarginPadding};
`;

export default function Box(props) {
  return <StyledBox {...props} />;
}
