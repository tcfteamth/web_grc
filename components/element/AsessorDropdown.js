import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import styled from 'styled-components';
import { DropdownSelect } from '.';
import { assesorDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';
import { toJS } from 'mobx';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const assesorDropdown = useLocalStore(() => assesorDropdownStore);
  const authContext = initAuthStore();
  const [listAssesor, setListAssesor] = useState();

  useEffect(() => {
    assesorDropdown.getAssesor(authContext.accessToken);
    const result = assesorDropdown.options.map((item) => ({
      value: item.value,
      label: item.text,
    }));

    setListAssesor(result);
  }, []);

  const getValue = () => {
    return listAssesor.find((assessor) => assessor.value === value);
  };

  const onSelectedDDL = (e) => {
    handleOnChange(e.value);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={listAssesor}
        options={listAssesor}
        value={listAssesor && getValue()}
        handleOnChange={onSelectedDDL}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
