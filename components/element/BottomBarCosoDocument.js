import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import Router from 'next/router';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import { ButtonBorder, Text, ButtonAll, RadioBox } from '.';
import {
  SendToModal,
  SaveModal,
  NextModal,
  SendToStaffModal,
  ValidateModal,
} from '../AssessmentPage/modal';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    assessmentData,
    saveButtonData,
    submitToDm,
    isSubmitToDmDisabled,
    acceptOrRejectRoadmap,
    isAcceptDisabled,
    onModalClose,
    onModalSave,
    onDownloadAll
  }) => {
    const dataContext = initDataContext();
    const authContext = initAuthStore();
    const [saveModal, setSaveModal] = useState(false);
    const [sendToModal, setSendToModal] = useState(false);
    const [activeAdminModal, setActiveAdminModal] = useState(false);
    const [activeValidateModal, setActiveValidateModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [activeSendToStaffModal, setActiveSendToStaffModal] = useState(false);

    const renderButtonLeft = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Section left>
            <ButtonAll
              text="Download All"
              textColor={colors.backgroundPrimary}
              fontSize={sizes.s}
              textUpper
              textWeight="med"
              color={colors.textSky}
              icon="../../static/images/save@3x.png"
              onClick={onDownloadAll}
              // disabled={!item.canClick}
            />
          </Section>
        </div>
      </div>
    );

    const renderButtonRight = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Section>
            <ButtonBorder
                width={140}
                style={{ marginRight: 20 }}
                handelOnClick={onModalClose}
                borderColor={colors.textlightGray}
                textColor={colors.primaryBlack}
                textUpper
                textWeight="med"
                text="cancel"
            />
          </Section>
          <Section>
            <ButtonAll
                text={'Save'}
                width={140}
                textColor={colors.backgroundPrimary}
                textUpper
                textWeight="med"
                onClick={onModalSave}
              // disabled={!item.canClick}
            />
          </Section>
        </div>
      </div>
    );


    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large" style={{padding:'10px'}}>
          <Menu.Menu position="left">{renderButtonLeft()}</Menu.Menu>

          <Menu.Menu position="right">{renderButtonRight()}</Menu.Menu>
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
