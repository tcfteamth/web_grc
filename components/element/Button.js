import React from "react";
import styled from "styled-components";
import { Button } from "semantic-ui-react";
import { Text } from ".";
import { colors, sizes } from "../../utils";

const Savebutton = styled(Button)`
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  border-radius: 4px !important;
  width: ${(props) => props.width || 190}px !important;
  height: ${(props) => props.height || 40}px !important;
  margin-top: 12px !important;
  margin-right: 20px !important;
`;

export default (props) => (
  <Savebutton {...props}>
    <Text color={colors.backgroundPrimary}>{props.title} </Text>
  </Savebutton>
);
