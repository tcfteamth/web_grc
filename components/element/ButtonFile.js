import React from 'react';
import styled from 'styled-components';
import { Text } from '../../components/element';

const ButtonClick = styled.div`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  padding: 0px 16px !important;
  border: solid 2px ${(props) => props.borderColor || '#1b1464'} !important;
  border-radius: ${(props) =>
    props.border ? '6px' : '6px 6px 0px 6px'} !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 38}px !important;
`;
const Div = styled.div`
  flex-direction: row;
  justify-content: flex-start;
  display: flex;
  align-items: center;
  width: ${(props) => props.width || 'auto'}px;
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
`;

const ButtonFile = ({
  text,
  color,
  width,
  height,
  textColor,
  textSize,
  textWeight,
  border,
  borderColor,
  onChange,
}) => (
  <Div>
    <Div width={width} height={height}>
      <label for="file-upload" class="custom-file-upload click">
        <ButtonClick
          border={border}
          color={color}
          height={height}
          width={width}
          borderColor={borderColor}
        >
          <Text
            style={{ textTransform: 'uppercase' }}
            color={textColor || '#1b1464'}
            fontSize={textSize}
            fontWeight={textWeight || 'normal'}
          >
            {text || 'Attach File'}
          </Text>
        </ButtonClick>
      </label>
      <input
        id="file-upload"
        type="file"
        accept="application/pdf, image/jpeg, image/png"
        // onChange={(e) => console.log(e.target.files)}
        onChange={onChange}
        // multiple
        // webkitdirectory="true"
      />
    </Div>
  </Div>
);

export default ButtonFile;
