import React, { useEffect, useState } from 'react';
import { Menu, Popup, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { colors, sizes } from '../../utils';
import { initDataContext, initAuthStore } from '../../contexts';
import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
  ModalGlobal,
} from '.';
import {
  CreateRoadmapModal,
  AddProcessModal,
  NextModal,
} from '../RoadMapPage/modal';
// import { RoadmapListModel } from '../../model';
import useWindowDimensions from '../../hook/useWindowDimensions';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;
const BottombarLogo = styled(Menu)`
  height: 55px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  background-color: transparent !important;
  border: transparent !important;
  border-top: transparent !important;
  box-shadow: none !important;
`;

const BottomBar = observer(
  ({
    detailReportModel,
    manageRoleModel,
    filterModel,
    isFilterReport,
    followUpModel,
    handleExport,
  }) => {
    const authContext = initAuthStore();
    const [activeSubmitModal, setActiveSubmitModal] = useState(false);
    const { width: screenWidth } = useWindowDimensions();
    const authStore = initAuthStore();

    return useObserver(() => (
      <div>
        <Bottombar
          className="pl-Bottombar no-print"
          fluid
          fixed="bottom"
          size="large"
        >
          <Menu.Menu position="left">
            <Section>
              <ButtonBorder
                text={screenWidth >= 768 && 'Export'}
                textColor={colors.greenExport}
                fontSize={sizes.s}
                textUpper
                textWeight="med"
                borderColor={colors.greenExport}
                icon="../../static/images/excel@3x.png"
                handelOnClick={handleExport}
              />
            </Section>
          </Menu.Menu>
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
