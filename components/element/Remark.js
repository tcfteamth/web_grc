import React from 'react';
import { Popup, Image } from 'semantic-ui-react';

const index = ({ remark }) => (
  <Popup
    className={remark ? 'click' : ''}
    content={remark}
    disabled={!remark}
    position="right center" 
    trigger={
      <Image
        className={remark ? 'click' : ''}
        src={
          remark
            ? '../../static/images/markpurple@3x.png'
            : '../../static/images/markgray@3x.png'
        }
        style={{ width: 18, height: 18 }}
      />
    }
  />
);

export default index;
