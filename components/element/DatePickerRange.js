import React from 'react'
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import { compose, withState, withHandlers } from 'recompose'
import { Button } from 'semantic-ui-react'

const enhance = compose(
    withState('focusedInput', 'setFocusedInput', false),
    withHandlers({
        handleFocusChange: props => ({ focused }) => props.setFocused(focused)
    })
)

export default enhance((props) => (
    <DateRangePicker
        startDate={props.startDate} // momentPropTypes.momentObj or null,
        endDate={props.endDate} // momentPropTypes.momentObj or null,
        onDatesChange={props.onDatesChange} // PropTypes.func.isRequired,
        focusedInput={props.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
        onFocusChange={focusedInput => (props.setFocusedInput(focusedInput))} // PropTypes.func.isRequired,
        displayFormat="DD/MM/YYYY"
        showDefaultInputIcon
        isOutsideRange={() => false}
        readOnly={true}
        showClearDates={props.showClearDates}
        // reopenPickerOnClearDates={true}
    />
))