import React, { useEffect, useState } from 'react';
import { Menu } from 'semantic-ui-react';
import Link from 'next/link';
import Router, { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initDataContext } from '../../contexts';
import { ButtonBorder, Text, ButtonAll } from '.';
import request from '../../services';
import { toJS } from 'mobx';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(({ page, roleModel }) => {
  const dataContext = initDataContext();
  const [screenWidth, setScreenWidth] = useState();

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, [screenWidth]);

  const handleCreateNewRole = async () => {
    roleModel.getUsetListNew();

    const body = {
      roleName: roleModel.roleName.trim(),
      amount: 0,
      createdDate: '',
      isDelete: false,
      isAdmin: roleModel.isAdmin,
      isIcAgent: roleModel.isIcAgent,
      isCompliance: roleModel.isCompliance,
      isIaFinding: roleModel.isIaFinding,
      users: toJS(roleModel.roleUserNew),
      positionLevel: roleModel.selectPosition == undefined ? null : roleModel.selectPosition.value,
      roleType: roleModel.roleType.value,
      roleSystem: roleModel.selectRoleSystem == undefined ? null : roleModel.selectRoleSystem.value,
      authorities: toJS(roleModel.authoritiesId),
    };

    await request.clientServices
      .createNewRole(body)
      .then(() => {
        Router.push('/roleManagement/RoleList');
      })
      .catch((error) => {
        dataContext.status = 'ชื่อ Role มีในระบบแล้ว';
        console.log(error);
      });
  };

  const handleEditRole = async () => {
    roleModel.getUsetListNew();

    const body = {
      id: roleModel.roleId,
      roleName: roleModel.roleName.trim(),
      amount: 0,
      createdDate: '',
      isDelete: false,
      isAdmin: roleModel.isAdmin,
      isIcAgent: roleModel.isIcAgent,
      isCompliance: roleModel.isCompliance,
      isIaFinding: roleModel.isIaFinding,
      users: toJS(roleModel.roleUserNew),
      positionLevel: roleModel.selectPosition == undefined ? null : roleModel.selectPosition.value,
      roleType: roleModel.roleType.value,
      roleSystem: roleModel.selectRoleSystem == undefined ? null : roleModel.selectRoleSystem.value,
      authorities: toJS(roleModel.authoritiesId),
    };

    console.log('body Edit', body);
    await request.clientServices
      .editRole(body)
      .then((response) => {
        console.log('handleEditRole', response);
        Router.push('/roleManagement/RoleList');
      })
      .catch((error) => {
        dataContext.status = 'ชื่อ Role มีในระบบแล้ว';
        console.log('handleEditRole', error);
      });
  };

  return useObserver(() => (
    <div>
      <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
        <Menu.Menu position="left">
          {page === 'createList' && (
            <Link href="/roleManagement/RoleCreate">
              <Section>
                <ButtonBorder
                  borderColor="#00aeef"
                  textColor="#00aeef"
                  textUpper
                  textWeight="med"
                  width={160}
                  text="Create Role"
                  icon="../../static/images/iconPlus@3x.png"
                />
              </Section>
            </Link>
          )}
        </Menu.Menu>

        <Menu.Menu position="right">
          {page === 'editRole' && (
            <Section>
              <ButtonAll
                width={160}
                borderColor={colors.primary}
                textColor={colors.backgroundPrimary}
                color={colors.primary}
                textUpper
                textWeight="med"
                text="save"
                disabled={
                  !roleModel.roleName 
                  || !roleModel.roleType 
                  || !(
                    (roleModel.roleType.value == 'SPECIAL' && (roleModel.isAdmin || roleModel.isCompliance || roleModel.isIaFinding || roleModel.isIcAgent))
                    || (roleModel.roleType.value == 'GENERAL')
                  )
                }
                onClick={() => handleEditRole()}
              />
            </Section>
          )}
          {page === 'createRole' && (
            <Section>
              <ButtonAll
                width={160}
                borderColor={colors.primary}
                textColor={colors.backgroundPrimary}
                color={colors.primary}
                textUpper
                textWeight="med"
                text="save"
                disabled={
                  !roleModel.roleName 
                  || !roleModel.roleType 
                  || !(
                    (roleModel.roleType.value == 'SPECIAL' && (roleModel.isAdmin || roleModel.isCompliance || roleModel.isIaFinding || roleModel.isIcAgent))
                    || (roleModel.roleType.value == 'GENERAL')
                  )
                }
                onClick={() => handleCreateNewRole()}
              />
            </Section>
          )}
        </Menu.Menu>
      </Bottombar>
    </div>
  ));
});

export default BottomBar;
