import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import Router from 'next/router';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { ButtonAll, ModalGlobal } from '.';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(({ followUpDetailModel }) => {
  const [activeSubmitModal, setActiveSubmitModal] = useState(false);
  const authContext = initAuthStore();

  const handleSubmitFollowUpDetail = async () => {
    try {
      setActiveSubmitModal(false);

      await followUpDetailModel.updateFollowUpDetail(
        authContext.accessToken,
        Router.query.id,
      );

      Router.push('/followUp/followUpList');
    } catch (e) {
      console.log(e);
    }
  };

  const handleSaveFollowUpDetail = async () => {
    try {
      await followUpDetailModel.saveFollowUpDetail(
        authContext.accessToken,
        Router.query.id,
      );

      Router.reload();
    } catch (e) {
      console.log(e);
    }
  };

  const submitWording = () => {
    if (authContext.roles.isAdmin || authContext.roles.isVP) {
      return 'APPROVE';
    }

    return 'SUBMIT';
  };

  return useObserver(() => (
    <div>
      <Bottombar
        className="pl-Bottombar no-print"
        fluid
        fixed="bottom"
        size="large"
      >
        <Menu.Menu position="right">
          <Section>
            <div style={{ marginRight: '24px' }}>
              <ButtonAll
                text="Save"
                textUpper
                textWeight="med"
                onClick={handleSaveFollowUpDetail}
              />
            </div>
            <ButtonAll
              text={submitWording()}
              textColor={colors.backgroundPrimary}
              fontSize={sizes.s}
              textUpper
              textWeight="med"
              color={colors.primary}
              onClick={() => setActiveSubmitModal(true)}
              disabled={
                followUpDetailModel.followUpDetail &&
                followUpDetailModel.isSubmitButtonDisabled
              }
            />
          </Section>
        </Menu.Menu>
      </Bottombar>

      <ModalGlobal
        onSubmit={handleSubmitFollowUpDetail}
        onClose={() => setActiveSubmitModal(false)}
        open={activeSubmitModal}
        submitText="Submit"
        cancelText="Cancel"
        title="Submit FollowUp"
      />
    </div>
  ));
});

export default BottomBar;
