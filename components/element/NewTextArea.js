import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const borderColor = (active, error) => {
  if (error) return colors.red;

  if (active) return colors.primary;

  return 'inherit';
};

const StyledTextArea = styled(TextArea)`
  min-height: ${(props) => props.height || 100}px !important;
  width: 100% !important;
  padding: 8px 16px;
  font-size: ${sizes.s}px !important;
  font-family: 'Prompt', 'Roboto', sans-serif !important;
  border: 1px solid ${(props) => borderColor(props.active, props.error)} !important;
  color: ${(props) => (props.active ? '' : colors.primary)} !important;
  font-weight: bold !important;
`;

const index = ({
  placeholder,
  width,
  height,
  maxLength,
  onChange,
  value,
  error,
  style,
  type,
  disabled,
  active,
}) => (
  <Form widths="100%">
    <StyledTextArea
      placeholder={placeholder}
      onChange={onChange}
      value={value}
      error={error}
      style={style}
      disabled={disabled}
      height={height}
      maxLength={maxLength}
      active={active}
    />
  </Form>
);

export default index;
// <CardTextArea
//   style={style}
//   width={width}
//   height={height}
//   placeholder={placeholder}
//   maxLength={maxLength}
//   onChange={onChange}
//   value={value}
//   error={error}
//   type={type}
//   disabled={disabled}
// />
