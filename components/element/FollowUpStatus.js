import React from 'react';
import styled from 'styled-components';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const handleBackGroundColor = (status) => {
  if (status === 'cancelled' || status === 'cancelled(co)') {
    return colors.secondary;
  }
  if (status === 'open') return colors.pink;
  if (status === 'over') return colors.red;
  if (status === 'reject') return colors.lightDark;
  if (status === 'closed' || status === 'closed(co)') return colors.greenMint;
  if (status === 'completed') return colors.primary;
  if (status === 'postpone' || status === 'postpone(co)') return colors.orange;
};

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  display: flex;
  min-width: 80px;
  max-width: 120px;
  width: 100%;
  padding: 0px 8px;
  min-height: 24px;
  text-align: center;
  align-items: center !important;
  justify-content: center;
  background-color: ${(props) => handleBackGroundColor(props.status)};
`;

const allStatus = [
  'cancelled',
  'open',
  'closed',
  'completed',
  'over',
  'postpone',
  'reject',
  'cancelled(co)',
  'closed(co)',
  'postpone(co)',
];

const StatusAll = ({ status }) => {
  const findStatus = () => {
    const splitStatus = status.toLowerCase().split(' ');
    let filterStatus;

    for (let i = 0; i < splitStatus.length; i++) {
      filterStatus = allStatus.find((s) => s === splitStatus[i]);

      if (filterStatus) {
        return filterStatus;
      }
    }
  };

  return (
    <>
      <StatusTag status={findStatus()}>
        <Text
          color={colors.backgroundPrimary}
          fontWeight="bold"
          fontSize={sizes.xxs}
        >
          {status}
        </Text>
      </StatusTag>
    </>
  );
};

export default StatusAll;
