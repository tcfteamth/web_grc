import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { shiftListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  selectedDepartmentId,
  selectedDepartmentNo,
  selectedYear,
  placeholder,
}) => {
  const shiftListDropdown = useLocalStore(() => shiftListDropdownStore);
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();

  const getShiftList = async (departmentId,departmentNo,year) => {
    try {
      const currentYear = new Date().getFullYear();
      if(year === null || year === undefined || year === '' || year === currentYear){
      if (departmentId) {
        const optionsResult = await shiftListDropdown.getShiftList(
          departmentId,
        );

        setDepartmentList(optionsResult);
      } else {
        const optionsResult = await shiftListDropdown.getShiftWithoutId(
          authContext.accessToken,
        );

        setDepartmentList(optionsResult);
      }
    }else{
      const optionsResult = await shiftListDropdown.getShiftOldList(
        departmentNo,
        year,
      );
      setDepartmentList(optionsResult);     
    } 
  }catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getShiftList(selectedDepartmentId,selectedDepartmentNo,selectedYear);
  }, [selectedDepartmentId,selectedDepartmentNo,selectedYear]);

  const getValue = () => {
    return departmentList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList && departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
