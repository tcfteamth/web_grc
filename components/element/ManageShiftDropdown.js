import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { shiftListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  selectedBu,
  selectedDepartmentId,
  placeholder,
}) => {
  const shiftListDropdown = useLocalStore(() => shiftListDropdownStore);
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();

  const getShiftList = async (bu, departmentId) => {
    try {
      if (departmentId || bu) {
        const optionsResult = await shiftListDropdown.getManageFilterShiftList(
          bu,
          departmentId,
        );

        setDepartmentList(optionsResult);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getShiftList(selectedBu, selectedDepartmentId);
  }, [selectedDepartmentId, selectedBu]);

  const getValue = () => {
    return departmentList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList && departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
