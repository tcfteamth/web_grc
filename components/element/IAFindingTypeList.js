import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { iaFindingTypeList } from '../../model';

const index = ({ value, handleOnChange, disabled, placeholder }) => {
  const rateListDropdown = useLocalStore(() => iaFindingTypeList);
  const [rateList, setRateList] = useState();

  const getRateList = () => {
    try {
      const optionsResult = rateListDropdown.getIAFindingTypelist();

      setRateList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRateList();
  }, []);

  const getValue = () => {
    return rateList.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={rateList}
        options={rateList}
        value={rateList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
