import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import { colors, sizes } from "../../utils";

const ChartHorizontalBar = ({ data }) => {
  const options = {
    maintainAspectRatio: true,
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }],
    },
    legend: {
      display: false,
    },
  };

  const dataBar = {
    labels: ["S-RC-IC", "S-RC-GRC", "S-RC-RM", "S-RC-SG"],
    datasets: [
      {
        label: "COMPLETE",
        barThickness: 24,
        data: ["1", "6", "", ""],
        backgroundColor: colors.textPurple,
      },
      {
        label: "CLOSED",
        barThickness: 24,
        data: ["2", "2", "", ""],
        backgroundColor: colors.greenMint,
      },
      {
        label: "OVERDUE",
        barThickness: 24,
        data: ["", "", "2", ""],
        backgroundColor: colors.red,
      },
      {
        label: "POSTPONE",
        barThickness: 24,
        data: ["2", "", "3", ""],
        backgroundColor: colors.orange,
      },
      {
        label: "OPEN",
        barThickness: 24,
        data: ["", "", "1", ""],
        backgroundColor: colors.pink,
      },
    ],
  };

  return <HorizontalBar data={dataBar} options={options} />;
};

export default ChartHorizontalBar;
