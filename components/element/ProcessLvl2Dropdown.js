import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { lvl2ListDropdownStore } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({ value, handleOnChange, disabled, placeholder }) => {
  const lvl2ListDropdown = useLocalStore(() => lvl2ListDropdownStore);
  const authContext = initAuthStore();
  const [lvl2List, setLvl2List] = useState();

  const getRoadmapType = async () => {
    try {
      const optionsResult = await lvl2ListDropdown.getRoadmapLvl2List(
        authContext.accessToken,
      );
      setLvl2List(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRoadmapType();
  }, []);

  const getValue = () => {
    return lvl2List.find((assessor) => assessor.value === value) || null;
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e.value);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={lvl2List}
        options={lvl2List}
        value={lvl2List && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
