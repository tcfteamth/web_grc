import React from 'react';
import { Modal } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, ButtonBorder, ButtonAll } from '.';
import { colors, sizes } from '../../utils';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

export default ({
  size,
  title,
  content,
  onClose,
  open,
  onSubmit,
  submitText,
  cancelText,
  ContentComponent,
  status,
  mode,
  submitTextRight,
  isSubmitDisabled,
  closeOnDimmerClick,
  hideCancelButton,
}) => (
  <ModalBox
    size={size || 'medium'}
    open={open}
    onClose={onClose}
    closeOnDimmerClick={closeOnDimmerClick}
  >
    <div>
      <Text fontSize={sizes.xxl} color={colors.textSky}>
        {title || 'Request New Sub-Process'}
      </Text>
    </div>
    <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
      {ContentComponent ? (
        <ContentComponent />
      ) : (
        <Text fontSize={sizes.s} color={colors.textGray}>
          {content || 'Do you want to do this action'}
        </Text>
      )}
    </Modal.Content>
    {status && status.page ? (
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textWeight="med"
          textUpper
          text="No"
        />
        {status.page === 'isSubmit' && (
          <ButtonAll
            width={140}
            onClick={onSubmit}
            color={colors.primary}
            textColor={colors.backgroundPrimary}
            textUpper
            textWeight="med"
            text={status.text || 'Create Now'}
          />
        )}
      </div>
    ) : (
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        {!hideCancelButton && (
          <ButtonBorder
            width={140}
            style={{ marginRight: 20 }}
            handelOnClick={onClose}
            borderColor={colors.textlightGray}
            textColor={colors.primaryBlack}
            textUpper
            textWeight="med"
            text={cancelText || 'cancel'}
          />
        )}
        {mode && mode === 'processAdmin' ? (
          <ButtonAll
            width={140}
            onClick={onSubmit}
            color={colors.primary}
            textColor={colors.backgroundPrimary}
            textUpper
            textWeight="med"
            text={submitText || 'Create Now'}
          />
        ) : (
          <ButtonAll
            width={140}
            onClick={onSubmit}
            color={
              isSubmitDisabled === 'disabled' ? colors.btGray : colors.primary
            }
            textColor={colors.backgroundPrimary}
            textUpper
            textWeight="med"
            text={submitTextRight || 'submit'}
            disabled={isSubmitDisabled}
          />
        )}
      </div>
    )}
  </ModalBox>
);
