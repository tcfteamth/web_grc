import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import { DropdownSelect } from '.';
import { rateListDropdownStore, rcMappingList } from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const rateListDropdown = useLocalStore(() => rcMappingList);
  const authContext = initAuthStore();
  const [rateList, setRateList] = useState();

  const getRateList = async (page, size, filterNo, search) => {
    try {
      const optionsResult = await rateListDropdown.getRCMappingList(
        page,
        size,
        filterNo,
        search,
      );

      setRateList(optionsResult);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getRateList(1, '', defaultValue);
  }, []);

  const getValue = () => {
    return (
      rateList.find(
        (assessor) =>
          assessor.value === value || assessor.value === defaultValue,
      ) || null
    );
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={rateList}
        options={rateList}
        value={rateList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
