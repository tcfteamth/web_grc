import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const CardTextArea = styled(TextArea)`
  alignitems: center;
  min-height: ${(props) => props.height || 80}px;
  width: ${(props) => props.width || 100}%;
  width: 100% !important;
  // border-color: #d9d9d6 !important;
  border-color: ${(props) =>
    props.error
      ? colors.red
      : props.type === 'comment'
      ? colors.textBlack
      : colors.textGrayLight}!important;
  border-width: 1px;
  border-radius: 6px 6px 0px 6px !important;
  padding: 8px 16px;
  font-size: ${sizes.xs}px !important;
  resize: none !important;
  font-family: 'Prompt', 'Roboto', sans-serif !important;
`;

const TextAreaExampleTextArea = ({
  placeholder,
  width,
  height,
  maxLength,
  onChange,
  value,
  error,
  style,
  type,
  disabled,
}) => (
  <CardTextArea
    style={style}
    width={width}
    height={height}
    placeholder={placeholder}
    maxLength={maxLength}
    onChange={onChange}
    value={value}
    error={error}
    type={type}
    disabled={disabled}
  />
);

export default TextAreaExampleTextArea;
