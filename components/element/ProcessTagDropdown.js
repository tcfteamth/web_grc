import React, { useState, useEffect } from 'react';
import { useObserver } from 'mobx-react';
import { DropdownAll } from '.';

import request from '../../services';

const index = ({ value, handleOnChange, disabled, placeholder, isMulti }) => {
  const [lvlAllList, setLvlAllList] = useState();

  const getLvlList = async () => {
    await request.roadmapServices
      .getLvlList()
      .then((response) => {
        console.log('response', response);

        const lvlThreeOptions = response.data.listLv3.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.fullPath} ${lvlThree.nameTh}`,
          id: lvlThree.id,
        }));
        const lvlFourOptions = response.data.listLv4.map((lvlFour) => ({
          value: lvlFour.no,
          label: `${lvlFour.fullPath} ${lvlFour.nameTh}`,
          id: lvlFour.id,
        }));

        const submitList = lvlThreeOptions.concat(lvlFourOptions);
        setLvlAllList(submitList);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getLvlList();
  }, []);

  const getValue = () => {
    return lvlAllList.find((assessor) => assessor.value === value) || null;
  };

  return useObserver(() => (
    <>
      <DropdownAll
        placeholder={placeholder}
        defaultValue={lvlAllList}
        options={lvlAllList}
        // value={lvlAllList && getValue()}
        handleOnChange={(e) => handleOnChange(e)}
        isDisabled={disabled}
        isMulti={isMulti}
      />
    </>
  ));
};

export default index;
