import React, { useState, useEffect, useReducer } from 'react';
import { Form, Input } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';
import { useForm } from 'react-hook-form';
const customStyle = {
  styleInput: {
    height: '38px',
    width: '100%',
    fontSize: sizes.s,
  },
  styleInputError: {
    border: `1px solid ${colors.primary}`,
    height: '38px',
    width: '100%',
    fontSize: sizes.s,
  },
};

const InputField = ({
  placeholder,
  name,
  ref,
  error,
  handleOnChange,
  width,
  style,
  type,
}) => (
  <Form.Field
    control={Input}
    error={error}
    onChange={handleOnChange}
    style={{
      display: 'flex',
      flexDirection: 'row',
    }}
  >
    <input
      ref={ref}
      style={style || customStyle.styleInput}
      name={name || 'name'}
      type={type || 'text'}
      placeholder={placeholder || 'placeholder'}
    />
  </Form.Field>
);

export default InputField;
