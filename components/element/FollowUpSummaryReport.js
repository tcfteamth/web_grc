import React, { memo } from 'react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import moment from 'moment';
import { Text, OverAllRatingStatus, FollowUpStatus } from '.';
import { colors, sizes } from '../../utils';
import useWindowDimensions from '../../hook/useWindowDimensions';
import { Grid, Image, Popup } from 'semantic-ui-react';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: ${(props) => props.width - 65}px;
  // width: 100%;

  @media print {
    width: 1360;
  }
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${({ reportType }) =>
    reportType === 'corrective' ? colors.yellow : colors.primary};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const TableHeader = styled.div`
  float: left;
  width: auto;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 10px 0px;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  width: auto;
  height: 100%;
  padding: 6px 0px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width}px;
  flex-wrap: wrap;
  padding: 0px 12px;
  justify-content: ${(props) => (props.center ? 'center' : 'start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

// rotateX
const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: visible;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const headersEnhancement = [
  { key: 'Responsible Division', render: 'Responsible Division', width: -70 },
  { key: 'Level4', render: 'Level 4', width: -70 },
  { key: 'Sub-Process', render: 'Sub Process', width: 0 },
  { key: 'Risk', render: 'Risk', width: 100 },
  { key: 'Control', render: 'Control', width: 100 },
  {
    key: 'Observation and Finding',
    render: 'Observation and Finding',
    width: 40,
  },
  {
    key: 'Enhance /Corrective',
    render: 'Enhance /Corrective',
    width: 40,
  },
  { key: 'Progress', render: 'Progress', width: 40 },
  { key: 'Benefit/Detail', render: 'Benefit/Detail', width: -70 },
  { key: 'Date', render: 'Date', width: -60 },
  { key: 'Status', render: 'Status', width: -60 },
];

const headerCorrectiveAction = [
  { key: 'Responsible Division', render: 'Responsible Division', width: -70 },
  { key: 'Level4', render: 'Level 4', width: -70 },
  { key: 'Sub-Process', render: 'Sub Process', width: 0 },
  { key: 'Risk', render: 'Risk', width: 100 },
  { key: 'Control', render: 'Control', width: 100 },
  {
    key: 'Observation and Finding',
    render: 'Observation and Finding',
    width: 40,
  },
  {
    key: 'Enhance /Corrective',
    render: 'Enhance /Corrective',
    width: 40,
  },
  { key: 'Progress', render: 'Progress', width: 40 },
  { key: 'Date', render: 'Date', width: -60 },
  { key: 'Status', render: 'Status', width: -60 },
];

const index = ({ name, type, reportData }) => {
  const { width: screenWidth } = useWindowDimensions();
  const realWidth = screenWidth - 64;
  const newHeader =
    type === 'corrective' ? headerCorrectiveAction : headersEnhancement;
  const newWidth = realWidth / newHeader.length;

  const typeName = () => {
    if (type === 'corrective') {
      return 'Corrective Action Plan';
    }

    return 'Enhancement';
  };

  const renderCardHeaderTable = () => (
    <Div>
      {newHeader.map(({ render, key, width }) => (
        <TableHeader key={key}>
          <Cell center width={screenWidth > 1440 ? newWidth + width : newWidth}>
            <Text
              fontSize={sizes.xs}
              fontWeight="med"
              color={colors.textlightGray}
            >
              {render}
            </Text>
          </Cell>
        </TableHeader>
      ))}
    </Div>
  );

  return useObserver(() =>
    reportData.map((detail) =>
      detail.newDetailDTO?.map((newDTO, newDTOIdex) => (
        <Grid style={{ margin: '16px 0px' }} className="print-pagebreak pt">
          <Grid.Row style={{ padding: 0 }}>
            <div className="w-table">
              <CardAll width={screenWidth}>
                <CardTop reportType={type}>
                  <Text
                    fontSize={sizes.s}
                    fontWeight="bold"
                    color={colors.primaryBackground}
                  >
                    {typeName()} - {name} - Part {newDTOIdex + 1}
                  </Text>
                </CardTop>

                <TableScroll>
                  <TabScroll>
                    <Div col>
                      <div style={{ padding: '12px' }}>
                        <Text
                          fontSize={sizes.s}
                          fontWeight="bold"
                          color={colors.textBlack}
                        >
                          Objective type: {detail.lv5Type}
                        </Text>
                      </div>
                      <DividerLine bottom style={{ marginBottom: 16 }} />
                      {renderCardHeaderTable()}
                      <DividerLine bottom style={{ marginBottom: 16 }} />
                      <TableBody>
                        <Div>
                          <Div col>
                            {newDTO.map((dto) => (
                              <Div bottom={16}>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 70
                                      : newWidth
                                  }
                                  center
                                >
                                  <div
                                    style={{
                                      display: 'flex',
                                      justifyContent: 'center',
                                      alignItems: 'center',
                                      flexDirection: 'column',
                                    }}
                                  >
                                    <Popup
                                      trigger={
                                        <Text
                                          fontSize={sizes.xs}
                                          fontWeight="bold"
                                          color={colors.primaryBlack}
                                        >
                                          {dto.indicator}
                                        </Text>
                                      }
                                      content={`${dto.divEn || '-'} ${dto.shiftEn || ''}`}
                                      size="small"
                                    />
                                    {dto.partner && (
                                      <>
                                        <Image
                                          width={48}
                                          height={48}
                                          src="../../static/images/icons-followup-group-1-24-px@3x.png"
                                        />
                                        <Popup
                                          trigger={
                                            <Text
                                              fontSize={sizes.xs}
                                              fontWeight="bold"
                                              color={colors.primaryBlack}
                                            >
                                              {dto.partner}
                                            </Text>
                                          }
                                          content={`${dto.partnerDivEn || '-'} ${dto.partnerShiftEn || ''}`}
                                          size="small"
                                        />
                                      </>
                                    )}
                                  </div>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 70
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.lv4No}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440 ? newWidth + 0 : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.lv4Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 100
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.lv6Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 100
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.lv7Name}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 40
                                      : newWidth
                                  }
                                  center
                                  breakWord
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                  >
                                    {dto.initialRemark || '-'}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 40
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.suggestion || '-'}
                                  </Text>
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth + 40
                                      : newWidth
                                  }
                                  center
                                >
                                  <Text
                                    fontSize={sizes.xs}
                                    color={colors.primaryBlack}
                                    breakWord
                                  >
                                    {dto.progress || '-'}
                                  </Text>
                                </Cell>
                                {type !== 'corrective' && (
                                  <Cell
                                    width={
                                      screenWidth > 1440
                                        ? newWidth - 70
                                        : newWidth
                                    }
                                    center
                                  >
                                    <div
                                      style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                      }}
                                    >
                                      {!dto.benefit && !dto.benefitDetail ? (
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          -
                                        </Text>
                                      ) : (
                                        <>
                                          <Text
                                            fontSize={sizes.xs}
                                            color={colors.primaryBlack}
                                            breakWord
                                          >
                                            {dto.benefit || '-'}
                                          </Text>
                                          <Text
                                            fontSize={sizes.xs}
                                            color={colors.primaryBlack}
                                            breakWord
                                          >
                                            {dto.benefitDetail || '-'}
                                          </Text>
                                        </>
                                      )}
                                    </div>
                                  </Cell>
                                )}

                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 60
                                      : newWidth
                                  }
                                  center
                                >
                                  <div
                                    style={{
                                      display: 'flex',
                                      flexDirection: 'column',
                                      alignItems: 'center',
                                      justifyContent: 'center',
                                    }}
                                  >
                                    <Text
                                      fontSize={sizes.xxs}
                                      color={colors.primaryBlack}
                                      fontWeight="bold"
                                    >
                                      Due Date
                                    </Text>
                                    <Text
                                      fontSize={sizes.xs}
                                      color={colors.primaryBlack}
                                      breakWord
                                    >
                                      {dto.dueDate || '-'}
                                    </Text>

                                    <Text
                                      fontSize={sizes.xxs}
                                      color={colors.primaryBlack}
                                      fontWeight="bold"
                                      breakWord
                                    >
                                      New Due Date / Close Date
                                    </Text>
                                    <Text
                                      fontSize={sizes.xs}
                                      color={colors.primaryBlack}
                                      breakWord
                                    >
                                      {dto.endDate || '-'}
                                    </Text>
                                  </div>

                                  {/* <div
                                    style={{
                                      display: 'flex',
                                      flexDirection: 'column',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Text
                                      fontSize={sizes.xs}
                                      color={colors.primaryBlack}
                                      fontWeight="bold"
                                    >
                                      New due Date/Close Date
                                    </Text>
                                    <Text
                                      fontSize={sizes.xs}
                                      color={colors.primaryBlack}
                                      breakWord
                                    >
                                      {dto.endDate || '-'}
                                    </Text>
                                  </div> */}
                                </Cell>
                                <Cell
                                  width={
                                    screenWidth > 1440
                                      ? newWidth - 60
                                      : newWidth
                                  }
                                >
                                  <FollowUpStatus status={dto.status} />
                                </Cell>
                              </Div>
                            ))}
                          </Div>
                        </Div>
                      </TableBody>
                    </Div>
                  </TabScroll>
                </TableScroll>
              </CardAll>
            </div>
          </Grid.Row>
        </Grid>
      )),
    ),
  );
};

export default memo(index);
