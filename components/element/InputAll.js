import React from 'react';
import { Input } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../utils';

const customStyle = {
  styleInput: {
    minHeight: '38px',
    fontSize: sizes.xs,
    border: `1px solid ${colors.textGrayLight}`,
  },
  styleInputError: {
    border: `1px solid ${colors.red}`,
    minHeight: '38px',
    fontSize: sizes.xs,
  },
};

const InputStyle = styled(Input)`
  display: flex !important;
  flex-direction: row !important;
  align-items: center !important;
  width: 100% !important;
  min-width: ${(props) => `${props.width}px` || `100%`} !important;
  height: ${(props) => props.height || 38}px !important;
  font-size: ${sizes.xs}px !important;
  font-weight: 500;
  border-radius: 6px 6px 0px 6px !important;
  background-color: #eaeaea !important;
  color: ${colors.primaryBlack};
  &:placeholder {
    color: ${(props) => (props.disabled ? colors.textSky : colors.red)};
  }
`;

const InputAll = ({
  placeholder,
  handleOnChange,
  width,
  disabled,
  defaultValue,
  maxLength,
  value,
  key,
  ref,
  style,
  error,
  type,
}) => (
  <InputStyle
    type={type}
    ref={ref}
    key={key}
    value={value}
    width={width}
    placeholder={placeholder || true}
    onChange={handleOnChange}
    disabled={disabled}
    defaultValue={defaultValue}
    maxLength={maxLength}
    style={error ? customStyle.styleInputError : customStyle.styleInput}
  />
);

export default InputAll;
