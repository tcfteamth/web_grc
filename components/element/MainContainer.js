import React from "react";
import styled from "styled-components";
import { colors, sizes } from "../../utils";

const MainContainer = styled.div`
  padding: 10px;
  background-color: ${colors.backgroundSecondary};
  box-shadow: 0 0px 0px 0 !important;
`;
const index = ({ children }) => <MainContainer>{children}</MainContainer>;
export default index;
