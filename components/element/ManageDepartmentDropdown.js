import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import Router from 'next/router';
import { DropdownSelect } from '.';
import {
  manageDepartmentDropdownStore,
  departmentFollowUpVp,
} from '../../model';
import { initAuthStore } from '../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  isDefaultValue,
  selectedBu,
  placeholder,
  filterModel,
}) => {
  const manageDepartmentDropdown = useLocalStore(
    () => manageDepartmentDropdownStore,
  );
  const getDepartmentFollowUpVP = useLocalStore(() => departmentFollowUpVp);
  const authContext = initAuthStore();
  const [departmentList, setDepartmentList] = useState();
  const [isFirstTime, setIsFirstTime] = useState(true);

  const getManageDepartmentList = async () => {
    try {
      // Follow up: VP จะเห็นได้แค่ department ของตัวเอง
      // แต่ว่า follow up dashboard/report สามารถดูของ department อื่นได้
      if (
        authContext.roles.isVP &&
        Router.pathname === '/followUp/followUpList'
      ) {
        const optionsResult = await getDepartmentFollowUpVP.getDepartmentFollowUpVP(
          authContext.accessToken,
        );

        setDepartmentList(optionsResult);

        return;
      }

      // if (selectedBu) {
      //   const optionsResult = await manageDepartmentDropdown.getManageFilterDepartmentList(
      //     authContext.accessToken,
      //     selectedBu,
      //   );

      //   setDepartmentList(optionsResult);
      // } else {
        const optionsResult = await manageDepartmentDropdown.getManageFilterDepartmentList(
          authContext.accessToken,
          '',
        );

        setDepartmentList(optionsResult);
      // }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getManageDepartmentList();
  }, [selectedBu]);

  const getValue = () => {
    if (isDefaultValue) {
      filterModel.setField('selectedDepartment', departmentList[0] || '');
    }

    if (
      authContext.roles.isAdmin ||
      authContext.roles.isIaFinding ||
      authContext.roles.isEVP ||
      authContext.roles.isIcAgent ||
      authContext.roles.isCompliance ||
      authContext.roles.isSEVP ||
      authContext.roles.isVP
    ) {
      return (
        departmentList.find((assessor) => assessor.value === value) || null
      );
    }

    return (
      departmentList.find((assessor) => assessor.value === value) ||
      departmentList[0] ||
      ''
    );
  };

  const onSelectedDropdownList = (e) => {
    handleOnChange(e);
    // console.log(e);
  };

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={departmentList}
        options={departmentList}
        value={departmentList && getValue()}
        handleOnChange={onSelectedDropdownList}
        isDisabled={disabled}
        placeholder={placeholder}
      />
    </>
  ));
};

export default index;
