import React, { useState } from 'react';
import { Checkbox, Image, Grid, Popup } from 'semantic-ui-react';
import { colors, sizes } from '../../utils';
import styled from 'styled-components';
import {
  Text,
  ButtonBorder,
  ButtonAll,
  CheckBoxAll,
  ButtonFile,
  InputDropdown,
  InputAll,
  TextArea,
  DatePicker,
  Box,
} from '../../components/element';

const CardDetail = styled.div`
  border-radius: 6px !important;
  border: solid 1px #d9d9d6;
  display: inline-block;
  padding: 0px !important;
  min-height: 90px;
  width: 100%;
  margin: 10px 0px;
  background-color: ${colors.backgroundPrimary};
  overflow: hidden;
`;
const Divider = styled.div`
  height: 80px;
  align-items: center;
  justify-content: center;
  width: 2px;
  border: solid 1px #d9d9d6;
  background-color: ${colors.primary};
`;

const TableRow = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  height: 80%;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  border-radius: 6px;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '100')}%;
  }
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 25}%;
  min-height: 56px;
  flex-wrap: wrap;
  padding: 8px;
  align-items: center !important;
  background-color: ${(props) =>
    props.bg ? colors.grayLight : colors.backgroundPrimary} !important;
  border: 1px solid #d9d9d6 !important;
  border-bottom: ${(props) => props.bottom && 'none'} !important;
  border-top: ${(props) => props.border && 'none'} !important;
  border-left: ${(props) => props.border && 'none'} !important;
  justify-content: ${(props) =>
    props.group ? `space-around` : `center`} !important;
`;

const Div = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  margin-top: ${(props) => props.top || 0}px;
  padding-top: ${(props) => props.ptop || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const index = ({ name }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [stateDate, setStateDate] = useState();
  const [stateOnFocus, setStateOnFocus] = useState();
  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return (
    <div>
      <Div ptop={23} bottom={23}>
        <Div col center width={10}>
          <Text fontSize={sizes.xxs} color={colors.pink} fontWeight={'bold'}>
            การควบคุม
          </Text>
          <Text fontSize={sizes.s} color={colors.pink} fontWeight={'bold'}>
            C2
          </Text>
        </Div>
        <Div width={50}>
          <TextArea placeholder="... กรอก" height={80} />
        </Div>
        <Div mid width={2}>
          <Divider />
        </Div>
        <Div col width={38}>
          <Div between style={{ alignSelf: 'center' }}>
            <Div>
              <Text
                fontSize={sizes.xs}
                color={colors.textDarkBlack}
                fontWeight={'med'}
              >
                Control Rating
              </Text>
              <Popup
                wide="very"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  padding: 16,
                  marginRight: -10,
                }}
                position="top right"
                trigger={
                  <Image
                    width={18}
                    style={{ marginLeft: 8 }}
                    src="../../static/images/iconRemark@3x.png"
                  />
                }
              >
                <Grid columns="equal" style={{ margin: 0, paddingRight: 16 }}>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.primaryBlack}
                      >
                        Good
                      </Text>
                    </Grid.Column>
                    <Grid.Column>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        - The control is appropriately designed and being
                        implemented as specified
                      </Text>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.primaryBlack}
                      >
                        Fair
                      </Text>
                    </Grid.Column>
                    <Grid.Column>
                      <Box>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          - The control is designed but may not be adequate and
                          appropriate
                        </Text>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          - The controls have been designed adequately but not
                          consistently or only partially effective
                        </Text>
                      </Box>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.primaryBlack}
                      >
                        Poor
                      </Text>
                    </Grid.Column>
                    <Grid.Column>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        - There is no controls assignedd for the risks involved.
                      </Text>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.primaryBlack}
                      >
                        Enahancement
                      </Text>
                    </Grid.Column>
                    <Grid.Column>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        - Improve the control that already appropriately
                        designed and being implemented as specified to
                        <span style={{ fontWeight: 'bold' }}>
                          {' '}
                          be more effective{' '}
                        </span>
                        or
                        <span style={{ fontWeight: 'bold' }}>
                          {' '}
                          more efficiency{' '}
                        </span>
                      </Text>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Popup>
            </Div>
            <div
              className="click"
              style={{ paddingLeft: 8 }}
              onClick={() => handelOpen(!isOpen)}
            >
              {isOpen ? (
                <Image width={24} src="../../static/images/iconUp-blue.png" />
              ) : (
                <Image width={24} src="../../static/images/iconDown.png" />
              )}
            </div>
          </Div>
          <Div>
            <div>
              <ButtonAll
                width={100}
                height={32}
                textSize={sizes.xs}
                textWeight={'bold'}
                color={colors.green}
                text={'Good'}
              />
            </div>
            <div style={{ paddingLeft: 8 }}>
              <ButtonAll
                width={100}
                height={32}
                textSize={sizes.xs}
                textWeight={'bold'}
                color={colors.yellow}
                text={'Fair'}
              />
            </div>
            <div style={{ paddingLeft: 8 }}>
              <ButtonAll
                width={100}
                height={32}
                textSize={sizes.xs}
                textWeight={'bold'}
                color={colors.btGray}
                text={'Poor'}
              />
            </div>
          </Div>
          <Div top={8}>
            <CheckBoxAll disabled />
            <Text
              style={{ paddingLeft: 16 }}
              fontSize={sizes.xs}
              color={colors.btGray}
              fontWeight={'med'}
            >
              Enhancement
            </Text>
          </Div>
        </Div>
      </Div>
      {isOpen && (
        <div>
          <CardDetail>
            <TableRow>
              <Cell border bg width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Control Type
                </Text>
                <Image
                  width={18}
                  style={{ marginLeft: 8 }}
                  src="../../static/images/iconRemark@3x.png"
                />
              </Cell>
              <Cell border bg width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Control Classification
                </Text>
                <Image
                  width={18}
                  style={{ marginLeft: 8 }}
                  src="../../static/images/iconRemark@3x.png"
                />
              </Cell>
              <Cell border bg width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Frequency
                </Text>
              </Cell>
              <Cell border bg width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Information technology system
                </Text>
              </Cell>
              <Cell border bg width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Executor
                </Text>
              </Cell>
            </TableRow>

            <TableRow>
              <Cell border width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  การควบคุมภายในแบบป้องกัน (Preventive control)
                </Text>
              </Cell>
              <Cell border width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  การควบคุมด้วยมือ (Manual Control)
                </Text>
              </Cell>
              <Cell border width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  รายปี
                </Text>
              </Cell>
              <Cell border width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  -
                </Text>
              </Cell>
              <Cell border width={20}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Process Owner
                </Text>
              </Cell>
            </TableRow>
          </CardDetail>
          <Div center>
            <Text fontSize={sizes.xs} color={colors.primaryBlack}>
              Document / Evidence
            </Text>
            <div style={{ paddingLeft: 8 }}>
              <ButtonBorder
                border
                height={32}
                textColor={colors.textSky}
                borderColor={colors.textGrayLight}
                text={'CSA - 56-1'}
                textSize={sizes.xs}
              />
            </div>
            <div>
              <ButtonFile
                height={32}
                textSize={sizes.xs}
                border
                text={'Attach file'}
              />
            </div>
          </Div>
          <div style={{ paddingTop: 24 }}>
            <Text fontSize={sizes.s} fontWeight={'bold'} color={'#1b1464'}>
              Enhancement / Corrective Action
            </Text>
            <Div top={16}>
              <div style={{ width: '100%' }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  ความก้าวหน้าของการดำเนินการแก้ไข
                </Text>
                <InputAll placeholder="กรอกความก้าวหน้าของการดำเนินการแก้ไข" />
              </div>
            </Div>
            <Div top={16}>
              <div style={{ width: '100%' }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  ปัญหาหรืออุปสรรค (ถ้ามี)
                </Text>
                <InputAll placeholder="กรอกปัญหาหรืออุปสรรค" />
              </div>
            </Div>
            <Div top={16} between>
              <div style={{ width: '24%' }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Status
                </Text>
                <InputDropdown placeholder="CLOSE" />
              </div>
              <div style={{ width: '24%' }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Due Date
                </Text>
                <DatePicker
                  date={stateDate}
                  onDateChange={(date) => setStateDate(date)}
                  handleFocusChange={({ focused }) => setStateOnFocus(focused)}
                  focused={stateOnFocus}
                  placeholder={'เลือกวันท่ี'}
                />
              </div>
              <div style={{ width: '45%' }}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  Benefit
                </Text>
                <InputDropdown placeholder="Select Benefit" />
              </div>
            </Div>
          </div>
        </div>
      )}
    </div>
  );
};

export default index;
