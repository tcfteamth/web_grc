import React from 'react';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import { Text, Modal } from '.';
import { initTimeoutStore } from '../../contexts';
import { colors, sizes } from '../../utils';

const index = () => {
  const timeoutStore = useLocalStore(() => initTimeoutStore);

  return useObserver(() => (
    <Modal
      open={timeoutStore.activeTimeoutModal}
      title="Inactive Timeout"
      submitTextRight="LOGOUT"
      onSubmit={() => timeoutStore.authStore.logout()}
      closeOnDimmerClick={false}
      hideCancelButton
      ContentComponent={() => (
        <div>
          <Text fontSize={sizes.s} color={colors.textDarkBlack}>
            You have been inactive for 30 minutes. Please login again to
            proceed.
          </Text>
        </div>
      )}
    />
  ));
};

export default index;
