import React from 'react';
import styled from 'styled-components';
import { Checkbox, Image } from 'semantic-ui-react';
import { Text } from '.';
import { colors, sizes } from '../../utils';

const NewRadio = styled(Checkbox)`
  & label {
    position: absolute !important;
    min-width: 20px;
    min-height: 20px;
    cursor: ${(props) => (props.disabled ? 'no-drop' : 'pointer')} !important;
    background-size: contain !important;
    overflow: hidden !important;
    user-select: none !important;
    background-image: url(${(props) =>
      props.header
        ? '../../static/images/redio-default-black@3x.png'
        : props.disabled
        ? '../../static/images/redio-cant-select@3x.png'
        : '../../static/images/redio-default@3x.png'}) !important;
  }
  & label:after,
  label:focus {
    content: '' !important;
    min-width: 20px;
    min-height: 20px;
    cursor: ${(props) => (props.disabled ? 'no-drop' : 'pointer')} !important;
    background-size: contain !important;
    background-image: url('../../static/images/iconChecked@3x.png') !important;
    background-image: url(${(props) =>
      props.status === 'Reject'
        ? '../../static/images/close@3x.png'
        : '../../static/images/iconChecked@3x.png'}) !important;
  }
`;

const CheckBoxAll = ({
  header,
  status,
  disabled,
  label,
  onChange,
  checked,
  radio,
}) => (
  <div style={{ display: 'flex', alignItems: 'center' }}>
    <NewRadio
      radio={radio}
      header={header}
      onChange={onChange}
      status={status}
      disabled={disabled}
      checked={checked}
    />
    {label && (
      <Text
        fontSize={sizes.xxs}
        fontWeight="med"
        color={colors.primaryBlack}
        style={{ paddingLeft: 16 }}
      >
        {label}
      </Text>
    )}
  </div>
);

export default CheckBoxAll;
