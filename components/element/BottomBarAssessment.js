import React, { useState, useEffect } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import Router from 'next/router';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { ButtonBorder, Text, ButtonAll, RadioBox, ModalGlobal } from '.';
import {
  SendToModal,
  CreateNewObjectModal,
  SaveModal,
  NextModal,
  ValidateModal,
  SendToStaffModal,
  RemarkModal,
} from '../AssessmentPage/modal';
import { APPROVE_ASSESSMENT_STATUS } from '../../model/AssessmentModel/ApproveAssessmentModel';
import { VP_ASSESSMENT_STATUS } from '../../model/AssessmentModel/VPAssessmentModel';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(
  ({
    page,
    readyToSubmitData,
    assessmentData,
    assessmentDetailModel,
    iafindingDetailModel,
  }) => {
    const authContext = initAuthStore();
    const [active, setActive] = useState(false);
    const [saveModal, setSaveModal] = useState(false);
    const [activeNextModal, setActiveNextModal] = useState(false);
    const [sendToModal, setSendToModal] = useState(false);
    const [activeAdminModal, setActiveAdminModal] = useState(false);
    const [activeValidateModal, setActiveValidateModal] = useState(false);
    const [activeSendToStaffModal, setActiveSendToStaffModal] = useState(false);
    const [activeWaringTaskFollowUp, setActiveWaringTaskFollowUp] = useState(
      false,
    );

    const handleCloseCreateRoadmapModal = (status) => {
      setActive(status);
    };

    const handleSendToDm = async () => {
      try {
        setSendToModal(false);
        setActiveAdminModal(false);
        const result = await assessmentData.sendAssessmentToDmOrVp(
          authContext.accessToken,
        );

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleSubimtAssessment = async () => {
      try {
        setActiveNextModal(false);
        const result = await assessmentData.submitAssessmentToAdmin(
          authContext.accessToken,
        );

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleCreateNewObject = () => {
      try {
        setActive(false);

        const result = assessmentDetailModel.addObject();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleSaveAssesmentDetail = async () => {
      try {
        setSaveModal(false);
        const result = await assessmentDetailModel.updateAssessmentDetail(
          authContext.accessToken,
        );

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleRejectToStaff = async () => {
      try {
        setActiveSendToStaffModal(false);

        const result = await assessmentDetailModel.rejectToStaff(
          authContext.accessToken,
        );

        Router.replace('/Assessment/assessmentList');

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    const handleSubmitIAFinding = async () => {
      try {
        const result = await iafindingDetailModel.submitIAFinding(
          authContext.accessToken,
        );

        Router.reload();

        return result;
      } catch (e) {
        console.log(e);
      }
    };

    // const handleImportIA = async (files) => {
    //   const formData = new FormData();
    //   formData.append('excel', files);

    //   await iafindingDetailModel
    //     .importExcelIa(formData)
    //     .then((response) => {
    //       Router.reload();
    //     })
    //     .catch((e) => {
    //       console.log(e);
    //     });
    // };

    const renderButtonLeft = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {assessmentDetailModel &&
          assessmentDetailModel.buttonBar.map((item) => (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {item.buttonName === 'CREATE NEW OBJECT' &&
                item.isShow &&
                assessmentDetailModel.canUpdate.add && (
                  <Section left>
                    <ButtonBorder
                      textUpper
                      textWeight="med"
                      textSize={sizes.s}
                      handelOnClick={() => handleCloseCreateRoadmapModal(true)}
                      borderColor="#00aeef"
                      textColor="#00aeef"
                      text="Add Objective"
                      icon="../../static/images/iconPlus@3x.png"
                      disabled={!item.canClick}
                    />
                  </Section>
                )}
              {item.buttonName === 'EXPORT EXCEL' && item.isShow && (
                <Section left>
                  <ButtonBorder
                    text="Export"
                    textUpper
                    textWeight="med"
                    textColor={colors.greenExport}
                    textSize={sizes.s}
                    width={160}
                    borderColor={colors.greenExport}
                    icon="../../static/images/excel@3x.png"
                    disabled={!item.canClick}
                    handelOnClick={submitExportAssessmentDetail}
                  />
                </Section>
              )}
            </div>
          ))}
      </div>
    );

    const renderButtonRight = () => (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {assessmentDetailModel &&
          assessmentDetailModel.buttonBar.map((item) => (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {item.buttonName === 'SENT TO STAFF' && item.isShow && (
                <Section>
                  <ButtonAll
                    text={item.buttonName}
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.textSky}
                    onClick={() => setActiveSendToStaffModal(true)}
                    disabled={!item.canClick}
                  />
                </Section>
              )}

              {item.buttonName === 'SAVE' && item.isShow && (
                <Section>
                  <ButtonAll
                    text={item.buttonName}
                    width={140}
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.textPurple}
                    onClick={() => setSaveModal(true)}
                    disabled={!item.canClick}
                  />
                </Section>
              )}
              {item.buttonName === 'READY TO SUBMIT' && item.isShow && (
                <div style={{ display: 'flex', paddingLeft: 16 }}>
                  <RadioBox
                    onChange={() => {
                      assessmentDetailModel.setValidateAssessmentForm();
                      if (assessmentDetailModel.canCheckReadyToSubmit) {
                        assessmentDetailModel.approveStatus.setIsReadyToSubmit(
                          !assessmentDetailModel.isReadyToSubmit,
                        );

                        assessmentDetailModel.setField(
                          'isReadyToSubmit',
                          !assessmentDetailModel.isReadyToSubmit,
                        );
                      } else {
                        setActiveValidateModal(true);
                      }

                      // Active Modal เตือนกรณีที่มี followup locked อยู่
                      if (
                        authContext.roles.isAdmin &&
                        assessmentDetailModel.isOneOfAssessmentLockedByFollowup
                      ) {
                        setActiveWaringTaskFollowUp(true);
                      }
                    }}
                    checked={assessmentDetailModel.isReadyToSubmit}
                  />
                  <Text
                    className="upper"
                    fontSize={sizes.s}
                    fontWeight="med"
                    color={colors.textBlack}
                    style={{ paddingLeft: 8 }}
                  >
                    Ready to submit
                  </Text>
                </div>
              )}

              {item.buttonName === 'READY TO SUBMIT (IC)' && item.isShow && (
                <div style={{ display: 'flex', paddingLeft: 16 }}>
                  <RadioBox
                    onChange={() => {
                      if (assessmentDetailModel.canCheckReadyToSubmit) {
                        assessmentDetailModel.setField(
                          'isReadyToSubmit',
                          !assessmentDetailModel.isReadyToSubmit,
                        );
                      } else {
                        setActiveValidateModal(true);
                      }
                    }}
                    checked={assessmentDetailModel.isReadyToSubmit}
                  />
                  <Text
                    className="upper"
                    fontSize={sizes.s}
                    fontWeight="med"
                    color={colors.textBlack}
                    style={{ paddingLeft: 8 }}
                  >
                    READY TO SUBMIT (IC)
                  </Text>
                </div>
              )}
            </div>
          ))}
      </div>
    );

    const submitExportAssessmentDetail = async () => {
      const result = await assessmentDetailModel.exportAssessmentDetail();

      window.open(result.config.url, '_blank');

      return result;
    };

    return useObserver(() => (
      <div>
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          <Menu.Menu position="left">
            {page === 'AssessmentDetail' && renderButtonLeft()}

            {/* NOTE: hide สำหรับ uat 1 */}
            {/* {page === 'AssessmentDetail' || page === 'AssessmentDetailVP' && (
              <Section>
                <ButtonBorder
                  text="Export"
                  textUpper
                  textWeight="med"
                  textColor={colors.greenExport}
                  textSize={sizes.s}
                  width={160}
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                />
              </Section>
            )} */}

            {/* {page === "roadmapAdminAess" || page === "roadmapAdminReq" ? (
              <Section>
                <ButtonBorder
                  borderColor={colors.textSky}
                  textColor={colors.textSky}
                  textUpper
                  textWeight="med"
                  text="Create RoadMap"
                  icon="../../static/images/iconPlus@3x.png"
                  handelOnClick={() =>
                    setAlertModal(
                      "Create RoadMap",
                      "",
                      "",
                      "roadmapAdmin",
                      "Create Now",
                    )
                  }
                />
              </Section>
            ) : (
              ""
            )} */}
            {/* {page === "roadmapAdminAess" &&
            authContext.accessMenu("CSA_ROADMAP_ADD") &&
            authContext.roles[0].roleName === "SUPER-ADMIN" && (
              <Section>
                <ButtonBorder
                  borderColor={colors.textSky}
                  textColor={colors.textSky}
                  textUpper
                  textWeight="med"
                  text="Create RoadMap"
                  icon="../../static/images/iconPlus@3x.png"
                  handelOnClick={() =>
                    setAlertModal(
                      "Create RoadMap",
                      "",
                      "",
                      "roadmapAdmin",
                      "Create Now",
                    )
                  }
                />
              </Section>
            )} */}
            {/* {iafindingDetailModel && page === 'IaAssessment' && (
              <Section>
                <ButtonBorder
                  text="IMPORT"
                  textWeight="med"
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  width={160}
                  borderColor={colors.greenExport}
                  icon={'../../static/images/excel@3x.png'}
                  handelOnClick={() => setActiveAttachFileModal(true)}
                />
              </Section>
            )} */}
          </Menu.Menu>

          <Menu.Menu position="right">
            {page === 'AssessmentDetail' && renderButtonRight()}

            {!authContext.roles.isDM &&
              !authContext.roles.isVP &&
              !authContext.roles.isAdmin &&
              page === 'AssessmentTodoList' && (
                <Section>
                  <ButtonAll
                    text="submit"
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    disabled={!assessmentData.canSubmitAssessment}
                    color={colors.textPurple}
                    onClick={() => setSendToModal(true)}
                  />
                </Section>
              )}

            {(page === 'AssessmentTodoList' ||
              page === 'AssessmentTodoListDM') &&
              authContext.roles.isDM && (
                <Section>
                  <ButtonAll
                    text="submit"
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.textPurple}
                    disabled={!assessmentData.canSubmitAssessment}
                    onClick={() => setSendToModal(true)}
                  />
                </Section>
              )}

            {iafindingDetailModel &&
              iafindingDetailModel.iafindingDetailList.length > 0 &&
              page === 'IaAssessment' && (
                <Section>
                  <ButtonAll
                    text="save"
                    textColor={colors.backgroundPrimary}
                    fontSize={sizes.s}
                    textUpper
                    textWeight="med"
                    color={colors.primary}
                    onClick={() => handleSubmitIAFinding()}
                    // disabled={iafindingDetailModel.isOneOfListChecked}
                  />
                </Section>
              )}

            {page === 'AssessmentListVP' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() => setActiveNextModal(true)}
                  // handelOnClick={approvementData}
                  width={160}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  text="next"
                  disabled={assessmentData.isNextButtonDisable}
                />
              </Section>
            )}

            {page === 'AssessmentApproveListAdmin' &&
              authContext.roles.isAdmin && (
                <Section>
                  <ButtonBorder
                    handelOnClick={() => setActiveAdminModal(true)}
                    // handelOnClick={approvementData}
                    width={160}
                    textWeight="med"
                    borderColor={colors.textGrayLight}
                    textColor={colors.primaryBlack}
                    disabled={!assessmentData.isCheckedForNext}
                    textUpper
                    text="next"
                  />
                </Section>
              )}
          </Menu.Menu>

          {/* Modal สำหรับ create roadmap */}
          {page === 'AssessmentDetail' && (
            <CreateNewObjectModal
              active={active}
              onClose={() => handleCloseCreateRoadmapModal(false)}
              onSubmit={handleCreateNewObject}
              assessmentDetailModel={assessmentDetailModel}
            />
          )}

          {/* Modal สำหรับ save */}

          {page === 'AssessmentDetail' && (
            <SaveModal
              active={saveModal}
              onClose={() => setSaveModal(false)}
              onSubmit={handleSaveAssesmentDetail}
            />
          )}

          {/* staff send to DM  and DM send to VP */}
          <SendToModal
            active={sendToModal}
            onClose={() => setSendToModal(false)}
            onSubmit={handleSendToDm}
          />

          {/* VP send to ADMIN */}
          <RemarkModal
            active={activeNextModal}
            onClose={() => setActiveNextModal(false)}
            onSubmit={handleSubimtAssessment}
          />

          {/* Modal สำหรับ เพิ่มกระบวนการ */}
          {/* {page === "listAssessmenrt" && (
          <AddProcessModal
            active={activeAddProcessModal}
            onClose={() => setActiveAddProcessModal(false)}
          />
        )} */}

          {/* Modal สำหรับ next button  */}
          <NextModal
            active={activeAdminModal}
            onClose={() => setActiveAdminModal(false)}
            onSubmit={handleSendToDm}
          />

          <ValidateModal
            active={activeValidateModal}
            onClose={() => setActiveValidateModal(false)}
          />

          {/* <ModalGlobal
            content="Please fill all the required fields."
            open={activeValidateModal}
            onClose={() => setActiveValidateModal(false)}
            title="You fill out incomplete information!"
          /> */}

          <SendToStaffModal
            active={activeSendToStaffModal}
            onClose={() => setActiveSendToStaffModal(false)}
            onSubmit={handleRejectToStaff}
          />

          <ModalGlobal
            content="One of your followup haven't finish yet."
            open={activeWaringTaskFollowUp}
            onClose={() => setActiveWaringTaskFollowUp(false)}
            title="FollowUp Warning!"
          />
        </Bottombar>
      </div>
    ));
  },
);

export default BottomBar;
