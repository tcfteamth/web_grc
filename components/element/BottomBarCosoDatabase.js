import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import request from '../../services';
import { AttachFilesModal } from '../COSODatabase';
import Router from 'next/router';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import { ButtonBorder } from '.';

const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;

const BottomBar = observer(({ page, cosoDB }) => {
  const [activeAttachFileModal, setActiveAttachFileModal] = useState(false);
  const authContext = initAuthStore();

  const submitImportData = async (files) => {
    console.log('files', files);
    const formData = new FormData();
    formData.append('excel', files);
    await request.databaseCosoServices
      .importExcelDatabase(formData)
      .then((response) => {
        console.log('response', response);
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleExportData = async () => {
    try {
      const result = await cosoDB.exportCOSODatabase(authContext.accessToken);
      await window.open(result.config.url, '_blank');
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  return useObserver(() => (
    <div>
      <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
        <Menu.Menu position="left">
          {authContext.roles.isAdmin && (
            <Section>
              <ButtonBorder
                text="IMPORT"
                textUpper
                textWeight="med"
                textColor={colors.greenExport}
                textSize={sizes.s}
                width={160}
                borderColor={colors.textGrayLight}
                icon="../../static/images/excel@3x.png"
                handelOnClick={() => setActiveAttachFileModal(true)}
              />
            </Section>
          )}
        </Menu.Menu>

        <Menu.Menu position="right">
          <Section>
            <ButtonBorder
              text="EXPORT"
              textUpper
              textWeight="med"
              textColor={colors.greenExport}
              textSize={sizes.s}
              width={160}
              borderColor={colors.greenExport}
              icon="../../static/images/excel@3x.png"
              handelOnClick={handleExportData}
            />
          </Section>
        </Menu.Menu>
      </Bottombar>
      <AttachFilesModal
        active={activeAttachFileModal}
        onClose={() => setActiveAttachFileModal(false)}
        onAttatchFiles={(files) => {
          submitImportData(files);
        }}
      />
    </div>
  ));
});

export default BottomBar;
