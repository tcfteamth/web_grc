import React, { useState } from 'react';
import { Image, Pagination, GridRow, Icon } from 'semantic-ui-react';
import { CheckBoxAll, Text } from '../element';
import { colors, sizes } from '../../utils';
import { dataControlASToDo } from '../../utils/static';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';

const CardContainer = styled.div`
  margin-bottom: 98px;
  margin-top: 16px;
`;

const CardTabel = styled.div`
  display: inline-block;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.pad || 0}px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const TableHeader = styled.div`
  font-size: ${(props) => props.FontSize || 0}px;
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;

const headers = [
  { key: 'Submit', render: 'Submit', width: 4 },
  { key: 'Assessment Name', render: 'Assessment Name', width: 53, left: '16' },
  {
    key: 'Ready to Submit',
    render: 'Ready to Submit',
    width: 5,
    mid: 'center',
  },
  { key: 'Type', render: 'Type', width: 5, mid: 'center' },
  { key: 'Last Update', render: 'Last Update', width: 8, mid: 'center' },
  { key: 'Status', render: 'Status', width: 15, mid: 'center' },
];

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.center ? `center` : `start`)};
`;

const Div = styled.div`
  flex-direction: row;
  justify-content: space-between;
  display: flex;
  align-items: center;
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
`;

const TableBody = TableHeader.extend`
  margin: 3px 13px 3px 13px;
`;

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  width: 140px !important;
  height: 24px !important;
  text-align: center;
  margin-left: ${(props) => props.left || 0}px;
  background-color: ${(props) =>
    props.Awaiting2ndLineApprove
      ? colors.brightbule
      : props.AwaitingVPApprove
      ? colors.pralblue
      : props.Preparing
      ? colors.purplepink
      : props.AwaitingDMApprove
      ? colors.pralpurple
      : `#FFFFFF`};
`;

const index = ({}) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isShow, setIsShow] = useState(false);
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenInput = (value) => {
    setIsShow(value);
  };

  return (
    <CardContainer>
      <CardTabel pad={10}>
        <TableHeader span={12} FontSize={16}>
          {headers.map(({ render, key, width, mid, left }) => (
            <Cell key={key} width={width} center={mid} left={left}>
              {render === 'Submit' ? (
                <Div
                  col
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={'#0057b8'}
                  >
                    {render}
                  </Text>
                  <CheckBoxAll status={render} />
                </Div>
              ) : render === 'Ready to Submit' ? (
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.textGray}
                >
                  {render}
                </Text>
              ) : (
                <Div>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.textGray}
                  >
                    {render}
                  </Text>
                  <Image width={18} src="/static/images/iconDown-gray.png" />
                </Div>
              )}
            </Cell>
          ))}
        </TableHeader>
      </CardTabel>
      {dataControlASToDo &&
        dataControlASToDo.map((item, index) => (
          <CardTabel center bottom={4} pad={10}>
            <TableBody span={12} key={index}>
              <Cell width={4}>
                <CheckBoxAll header status={'Submit'} />
              </Cell>
              <Cell width={90} left>
                <Text
                  fontWeight="bold"
                  fontSize={sizes.s}
                  color={colors.primaryBlack}
                >
                  {item.textHeader}
                </Text>
              </Cell>
              <Cell width={3} right>
                <div
                  className="click"
                  style={{ paddingLeft: 8 }}
                  onClick={() => handelOpen(!isOpen)}
                >
                  {isOpen ? (
                    <Image width={18} src="/static/images/iconUp-blue.png" />
                  ) : (
                    <Image width={18} src="/static/images/iconDown.png" />
                  )}
                </div>
              </Cell>
            </TableBody>
            {isOpen && (
              <div>
                {item.subData.map((item, index) => (
                  <div>
                    <Link href="/Assessment/assessmentDetail">
                      <TableBody span={12} key={index}>
                        <Cell width={4}>
                          <CheckBoxAll
                            header
                            color={'#d9d9d6'}
                            status={'Submit'}
                          />
                        </Cell>
                        <Cell width={52} left>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.text}
                          </Text>
                        </Cell>
                        <Cell width={5} center>
                          {item.readyToSubmit === 'Ready to Submit' ? (
                            <Image
                              width={18}
                              src="/static/images/tick-active.png"
                            />
                          ) : (
                            <Image width={18} src="/static/images/tick.png" />
                          )}
                        </Cell>
                        <Cell width={5} center>
                          {item.type === 'Re Assessment' && (
                            <Image width={18} src="/static/images/re@3x.png" />
                          )}
                          {item.type === 'New Assessment' && (
                            <Image width={18} src="/static/images/new@3x.png" />
                          )}
                        </Cell>
                        <Cell width={9} center>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.lastUpdate}
                          </Text>
                        </Cell>
                        <Cell width={16} center>
                          {item.status === 'Awaiting DM Approve' && (
                            <StatusTag AwaitingDMApprove>
                              <Text
                                color={colors.textpralpurple}
                                fontWeight="bold"
                                fontSize={sizes.xxxs}
                              >
                                {item.status}
                              </Text>
                            </StatusTag>
                          )}
                          {item.status === 'Preparing' && (
                            <StatusTag Preparing>
                              <Text
                                color={colors.textpurplepink}
                                fontWeight="bold"
                                fontSize={sizes.xxxs}
                              >
                                {item.status}
                              </Text>
                            </StatusTag>
                          )}
                          {item.status === 'Awaiting VP Approve' && (
                            <StatusTag AwaitingVPApprove>
                              <Text
                                color={colors.textpralblue}
                                fontWeight="bold"
                                fontSize={sizes.xxxs}
                              >
                                {item.status}
                              </Text>
                            </StatusTag>
                          )}
                          {item.status === 'Awaiting 2nd Line Approve' && (
                            <StatusTag Awaiting2ndLineApprove>
                              <Text
                                color={colors.textbrightbule}
                                fontWeight="bold"
                                fontSize={sizes.xxxs}
                              >
                                {item.status}
                              </Text>
                            </StatusTag>
                          )}
                        </Cell>
                      </TableBody>
                    </Link>

                    {/* <div
                      style={{
                        paddingLeft: '9.5%',
                        paddingTop: 8,
                        paddingBottom: 8,
                      }}
                    >
                      {isShow && <InputAll placeholder="กรุณาระบุเหตุผล" />}
                    </div> */}
                  </div>
                ))}
              </div>
            )}
          </CardTabel>
        ))}

      <GridRow style={{ justifyContent: 'flex-end', right: 13 }}>
        <Pagination
          style={{
            fontSize: sizes.xs,
            fontWeight: 'bold',
            borderColor: colors.backgroundSecondary,
          }}
          defaultActivePage={1}
          // activePage={1}
          // onPageChange={handlePageChange}
          firstItem={null}
          lastItem={null}
          nextItem={{
            content: <Icon name="angle right" />,
            icon: true,
          }}
          prevItem={{
            content: <Icon name="angle left" />,
            icon: true,
          }}
          totalPages={10}
        />
      </GridRow>
    </CardContainer>
  );
};

export default index;
