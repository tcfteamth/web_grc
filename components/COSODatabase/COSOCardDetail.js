import React from 'react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text, Box } from '../element';
import { colors, sizes } from '../../utils';
import { toJS } from 'mobx';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  margin-bottom: 24px;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

// rotateX
const TabScroll = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const CardTab = styled.div`
  background-color: ${colors.backgroundPrimary};
`;

const DividerLine = styled.div`
  height: 1px;
  width: 100%;
  margin: 16px 0px;
  background-color: ${colors.btGray};
`;
const DividerLineBlack = styled.div`
  height: 2px;
  width: 100%;
  margin-bottom: 6px;
  background-color: ${colors.primaryBlack};
`;

const CoSoControl = ({ databaseDetail }) => {
  // แสดง exacly match กับ remark เฉพาะ role admin

  return useObserver(() => (
    <CardAll style={{ marginTop: 16, width: '100%' }}>
      <CardTop>
        <Text
          fontSize={sizes.s}
          fontWeight="bold"
          color={colors.backgroundPrimary}
        >
          {`${databaseDetail.no} ${databaseDetail.name}`}
        </Text>
      </CardTop>
      <CardTab>
        {/* <TableScroll>
          <TabScroll> */}
        <Box flex={1}>
          {databaseDetail.children.map((item) => (
            <Box horizontal="row" flex={1}>
              <Box flex={1} style={{ padding: '16px' }}>
                <Text
                  fontSize={sizes.s}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  {`${item.no} ${item.name}`}
                </Text>
                {item.children &&
                  item.children.length > 0 &&
                  item.children.map((i) => (
                    <Box
                      flex={1}
                      horizontal="row"
                      style={{ paddingLeft: 24, paddingTop: 16 }}
                    >
                      {/* <DividerLine /> */}
                      <Text fontSize={sizes.s} color={colors.primaryBlack}>
                        {`${i.no} ${i.name}`}
                      </Text>
                    </Box>
                  ))}
              </Box>
              <DividerLineBlack />
            </Box>
          ))}
        </Box>
        {/* </TabScroll>
        </TableScroll> */}
      </CardTab>
    </CardAll>
  ));
};

export default CoSoControl;
