import React, { useState } from 'react';
import {
  Grid,
  Table,
  Radio,
  Modal,
  Button,
  Segment,
  Header,
  Image,
} from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import { Text, DropdownAll, CheckBoxAll, ButtonAll, Box } from '../element';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';

const ImageItem = styled(Image)`
  margin-bottom: 16px;
  @media only screen and (min-width: 768px) {
    width: auto;
    height: 100px !important;
  }
  @media only screen and (min-width: 1440px) {
    width: auto;
    height: 100px !important;
  }
`;
const CardItem = styled(Segment)`
  width: 200px;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 16px !important;
  flex-direction: column;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  z-index: 10 !important;
  &:hover,
  :active {
    box-shadow: 0 2px 9px 0 rgba(0, 87, 184, 0.5) !important;
  }

  @media only screen and (min-width: 768px) {
    height: 200px;
  }

  @media only screen and (min-width: 1440px) {
    height: 200px;
  }
`;

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onSubmit, selectRolesModel, roles }) => {
  return useObserver(() => (
    <div>
      <ModalBox open={active}>
        <Modal.Content>
          <Modal.Description>
            <Text fontSize={sizes.xxl} color={colors.textSky} fontWeight="med">
              Please Select Your Roles
            </Text>
            <div style={{ paddingTop: 24 }}>
              <Grid columns="equal">
                {roles.map((role) => (
                  <Grid.Column
                    mobile={16}
                    tablet={8}
                    computer={4}
                    style={{ margin: 8 }}
                  >
                    <CardItem
                      className="upper click"
                      onClick={() =>
                        selectRolesModel.submitRole.setRoleField(
                          role.roleId,
                          role.roleSys,
                        )
                      }
                    >
                      <CheckBoxAll
                        checked={
                          selectRolesModel.isSelectedRole === role.roleSys
                        }
                      />
                      <Box alignCenter>
                        <ImageItem
                          src={'../../static/images/roleInactive@3x.png'}
                        />
                        <Text
                          color={colors.textlightGray}
                          fontWeight={'bold'}
                          fontSize={sizes.s}
                          style={{ paddingLeft: 2 }}
                        >
                          {role.roleName}
                        </Text>
                      </Box>
                    </CardItem>
                  </Grid.Column>
                ))}
              </Grid>
            </div>
          </Modal.Description>
        </Modal.Content>
        <div style={{ display: 'flex', marginLeft: 'auto' }}>
          <div style={{ marginLeft: 'auto' }}>
            <ButtonAll
              color={colors.primary}
              textColor={colors.backgroundPrimary}
              textUpper
              text="Submit"
              disabled={!selectRolesModel.submitRole.roleId}
              onClick={onSubmit}
            />
          </div>
        </div>
      </ModalBox>
    </div>
  ));
};

export default index;
