import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Image, Popup, Modal } from 'semantic-ui-react';
import moment from 'moment';
import { useObserver } from 'mobx-react';
import {
  CheckBoxAll,
  Text,
  InputAll,
  Remark,
  StatusAll,
  ButtonAll,
  RoadmapNextYear,
  Cell,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { ACCEPT_ASSESSMENT_STATUS } from '../../../model/AssessmentModel/AcceptAssessmentModel';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TabInput = styled.div`
  margin: 10px 0;
  padding-left: 4%;
  width: 100%;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowAssessmentToDoListDM = ({ assessment }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isOpenRemark, setIsOpenRemark] = useState(false);
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell center width={screenWidth < 1024 ? 30 : 3}>
          <CheckBoxAll
            header
            status="Accept"
            onChange={() => assessment.setCheckAllReadyToSubmit()}
            checked={assessment.isCheckAllReadyToSubmit}
            disabled={!assessment.isOneOfAssessmentCanCheck}
          />
        </Cell>
        <Cell
          width={screenWidth < 1024 ? 940 : 94}
          className="click"
          onClick={() => setIsOpen(!isOpen)}
        >
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            style={{ paddingLeft: 16 }}
            color={colors.primaryBlack}
          >
            {assessment.no} {assessment.name}
          </Text>
        </Cell>
        
        <Cell center width={screenWidth < 1024 ? 30 : 3}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>
      {assessment.children.map((child) => (
        <Div>
          {isOpen && (
            <Div col>
              <Div style={{ padding: '16px 0px 0px' }}>
                <Cell center width={screenWidth < 1024 ? 30 : 3}>
                  <CheckBoxAll
                    disabled={!child.readyToSubmit}
                    onChange={() =>
                      child.submitAssessment.setRoadmapIds(child.id)
                    }
                    checked={child.submitAssessment.isSelected}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 350 : 35} className="click">
                  <a
                    href={`/Assessment/assessmentDetail?id=${child.id}&assessmentId=${child.no}`}
                  >
                    <Text
                      style={{ paddingLeft: 16 }}
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
                      {`${child.no} ${child.name}`}
                    </Text>
                  </a>
                </Cell>
                <Cell center width={screenWidth < 1024 ? 100 : 10}>
                <Text
                      style={{ paddingLeft: 16 }}
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
            {child.latestYear}
               </Text>
               </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  {child.readyToSubmit ? (
                    <Image
                      src="../../static/images/tick-active@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : (
                    <Image
                      src="../../static/images/tick@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  )}
                </Cell>

                <Cell width={screenWidth < 1024 ? 100 : 10} center mid>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    trigger={
                      child.roadMapImage ? (
                        <Image
                          src={child.roadMapImage}
                          style={{ width: 18, height: 18 }}
                        />
                      ) : (
                        <RoadmapNextYear
                          year={child.year || child.roadMapType}
                        />
                      )
                    }
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {child.roadMapType}
                    </Text>
                  </Popup>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center mid>
                  <div onClick={() => setIsOpenRemark(!isOpenRemark)}>
                    <Remark className="click" remark={child.remark} />
                  </div>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(child.lastUpdate).locale('th').format('L')}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 120 : 12} center>
                  <StatusAll child={child} />
                </Cell>
              </Div>
              {child.status === 'VP Reject' && (
                <TabInput>
                  <InputAll
                    placeholder="กรุณาระบุเหตุผล"
                    defaultValue={child.rejectComment}
                    disabled
                  />
                </TabInput>
              )}
            </Div>
          )}
        </Div>
      ))}
      {/* /// MOdal remark */}
      <ModalBox
        size="medium"
        open={isOpenRemark}
        onClose={() => setIsOpenRemark(false)}
        closeOnDimmerClick={() => setIsOpenRemark(false)}
        // onSubmit={onSubmit}
      >
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Edit Remark
        </Text>
        <Modal.Content style={{ padding: '16px 0px' }}>
          <div>
            <Text fontSize={sizes.s} color={colors.textDarkBlack}>
              Remark
            </Text>
            <InputAll placeholder={'Edit Remark'} />
          </div>
        </Modal.Content>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        >
          <ButtonAll
            text="SAVE"
            textColor={colors.backgroundPrimary}
            fontSize={sizes.s}
            textUpper
            textWeight="med"
            color={colors.primary}
          />
        </div>
      </ModalBox>
    </Div>
  ));
};

export default RowAssessmentToDoListDM;
