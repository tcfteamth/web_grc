import React, { useState, useEffect } from 'react';
import { Image, Grid, Table, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import Files from 'react-files';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonBorder,
  ButtonFile,
  ButtonAdd,
  InputAll,
  InputDropdown,
  DropdownAll,
  NewDate,
  NewDates,
  TextArea,
  ModalGlobal,
  NewTextArea,
} from '../../element';
import { AttachFilesModal } from '../modal';
import { PartnerListDropdown } from '../container';
import {
  CONTROL_TYPE_OPTIONS,
  CONTROL_FORMAT_OPTIONS,
  CONTROL_FREQUENCY_OPTIONS,
} from '../../../utils/assessment/controlOptions';
import { BENEFIT_OPTIONS } from '../../../utils/assessment/benefitOptions';
import { partnerlistDropdownStore } from '../../../model';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;
const ButtonFileName = styled.div`
  display: flex !important;
  flex-direction: row !important;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: ${(props) => props.color || colors.primary} !important;
  text-align: center;
  padding: ${(props) => props.padding || '0px 8px 0px 16px'} !important;
  background-color: ${(props) => props.color || '#FFFFFF'} !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-color: ${(props) => props.borderColor || '#eaeaea'} !important;
  border-style: solid !important;
  border-width: ${(props) => props.borderSize || 2}px !important;
  border-radius: 6px !important;
  min-width: ${(props) => props.width || 60}px !important;
  height: ${(props) => props.height || 32}px !important;
  opacity: ${(props) => (props.disabled ? 0.5 : '')};
`;

const WrapperControlDetail = styled.div`
  border: ${(props) => (props.error ? '1px red solid' : 'none')} !important;
  height: ${(props) => (props.error ? '36px' : 'auto')} !important;
  width: 100% !important;
  text-align: center !important;
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({ control, isEdit, assessmentDetailModel }) => {
  const [stateDate, setStateDate] = useState();
  const [stateOnFocus, setStateOnFocus] = useState();
  const [showDate, setShowDate] = useState();
  const [benefit, setBenefit] = useState();
  const [screenWidth, setScreenWidth] = useState();
  const [activeAttachFileModal, setActiveAttachFileModal] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const [coResponsible, setCoResponsible] = useState(false);
  const [checkResponsible, setCheckResponsible] = useState(false);

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, []);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid style={{ margin: 0 }} columns="equal">
        <Grid.Row style={{ padding: 0 }}>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      {control.type === 'Preventive control' ? (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          Preventive control - attempt to defer or stop an
                          unwanted outcome before it happens (e.g., use of
                          passwords, approval, policies, procedures).
                        </Text>
                      ) : (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          Detective control - attempt to detect errors or
                          irregularities that may have already occurred (e.g.,
                          reconciliations, monitoring of actual expenses vs
                          budget, prior periods, forecasts).
                        </Text>
                      )}
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3} style={{ padding: 8 }}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      {control.format === 'Manual Control' ? (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          Manual controls are performed by individuals. Outside
                          of a system (e.g., approval, review, segregration of
                          duties).
                        </Text>
                      ) : control.format === 'Automate Control' ? (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          Automated control are performed entirely by the
                          computer system (e.g., access right, credit limit in
                          in processing system).
                        </Text>
                      ) : (
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          IT Dependent Control are performed by individuals and
                          requires some level of system involvement.
                        </Text>
                      )}
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={4}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Information technology system{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginRight: -10,
                      }}
                      position="top right"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        e.g. SAP ARIBA SALEFORCE
                      </Text>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Executor{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row verticalAlign="top">
                <Table.Cell width={3}>
                  <Div mid center>
                    {isEdit ? (
                      <InputDropdown
                        placeholder="Select Control Type"
                        options={CONTROL_TYPE_OPTIONS}
                        handleOnChange={(e, { value }) =>
                          control.setField('type', value)
                        }
                        textDefault={control.type || null}
                        value={control.type}
                        onError={control.validateFormModel.validateType}
                      />
                    ) : (
                      <WrapperControlDetail
                        error={control.validateFormModel.validateType}
                      >
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {control.type || '-'}
                        </Text>
                      </WrapperControlDetail>
                    )}
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    {isEdit ? (
                      <InputDropdown
                        placeholder="Select Control Classification"
                        options={CONTROL_FORMAT_OPTIONS}
                        handleOnChange={(e, { value }) => {
                          control.setField('format', value);

                          if (value === 'Manual Control') {
                            control.setField('techControl', '-');
                          } else {
                            control.setField('techControl', '');
                          }
                        }}
                        textDefault={control.format || null}
                        value={control.format}
                        onError={control.validateFormModel.validateFormat}
                      />
                    ) : (
                      <WrapperControlDetail
                        error={control.validateFormModel.validateFormat}
                      >
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {control.format || '-'}
                        </Text>
                      </WrapperControlDetail>
                    )}
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    {isEdit ? (
                      <InputDropdown
                        placeholder="Select Frequency"
                        options={CONTROL_FREQUENCY_OPTIONS}
                        handleOnChange={(e, { value }) =>
                          control.setField('frequency', value)
                        }
                        textDefault={control.frequency || null}
                        value={control.frequency}
                        onError={control.validateFormModel.validateFrequency}
                      />
                    ) : (
                      <WrapperControlDetail
                        error={control.validateFormModel.validateFrequency}
                      >
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {control.frequency || '-'}
                        </Text>
                      </WrapperControlDetail>
                    )}
                  </Div>
                </Table.Cell>
                <Table.Cell width={4}>
                  <Div mid center>
                    {isEdit ? (
                      <InputAll
                        placeholder="key Information technology system"
                        handleOnChange={(e) => {
                          control.setField('techControl', e.target.value);
                        }}
                        disabled={!control.isTypeAutomateControl}
                        value={control.techControl}
                        error={control.validateFormModel.validateTechControl}
                      />
                    ) : (
                      <WrapperControlDetail
                        error={control.validateFormModel.validateTechControl}
                      >
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {control.techControl || '-'}
                        </Text>
                      </WrapperControlDetail>
                    )}
                  </Div>
                </Table.Cell>
                <Table.Cell width={3}>
                  <Div mid center>
                    {isEdit ? (
                      <InputAll
                        placeholder="Key Process owner"
                        handleOnChange={(e) =>
                          control.setField('employeeController', e.target.value)
                        }
                        value={control.employeeController}
                        error={
                          control.validateFormModel.validateEmployeeController
                        }
                      />
                    ) : (
                      <WrapperControlDetail
                        error={
                          control.validateFormModel.validateEmployeeController
                        }
                      >
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {control.employeeController || '-'}
                        </Text>
                      </WrapperControlDetail>
                    )}
                  </Div>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>
        <Grid.Column computer={screenWidth < 1367 ? 3 : 2}>
          <Text
            style={{ paddingRight: 12 }}
            fontSize={sizes.xs}
            color={colors.primaryBlack}
          >
            Document / Evidence
            <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
          </Text>
        </Grid.Column>
        <Grid.Column computer={screenWidth < 1367 ? 13 : 14}>
          <NewTextArea
            onChange={(e) => {
              control.setField('titles', e.target.value);
            }}
            value={control.titles}
            disabled={!isEdit}
            error={control.validateFormModel.validateDocument}
            active={isEdit}
          />

          <diV
            style={{
              marginTop: '8px',
              display: 'flex',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}
          >
            {control.displayFiles.map((file, index) => (
              <div style={{ marginTop: '-2px' }}>
                <ButtonAdd
                  text={`${file.title || file.name}`}
                  textSize={sizes.xs}
                  href={file.filePath || false}
                  onClickRemove={() =>
                    control.deleteAssessmentFiles(file.id, file.filesType)
                  }
                  disabled={!isEdit}
                />
              </div>
            ))}

            {isEdit && (
              <>
                <Files
                  style={customStyle.styleAddFile}
                  onChange={(files) => {
                    console.log('add file', files);
                    control.files.push(files[files.length - 1]);
                  }}
                  accepts={[
                    'image/png',
                    '.jpg',
                    '.pdf',
                    '.xlsx',
                    '.docx',
                    '.pptx',
                    '.jpeg',
                  ]}
                  // maxFiles={1}
                  multiple
                  maxFileSize={5242880}
                  minFileSize={0}
                  clickable
                  onError={(error, file) => {
                    if (error) {
                      setErrorMessage('File Size Maximum 5 MB');
                    } else {
                      setErrorMessage();
                    }
                  }}
                >
                  Attach File
                </Files>
                <Text
                  fontSize={sizes.xs}
                  color={colors.primaryBlack}
                  style={{ marginLeft: '8px' }}
                >
                  Optional
                </Text>
              </>
            )}
          </diV>
        </Grid.Column>

        {control.doNeedImprovement && (
          <>
            <Grid.Row style={{ paddingTop: 24 }}>
              <Text
                fontSize={sizes.s}
                color={colors.textPurple}
                fontWeight="bold"
              >
                Enhancement / Corrective Action
              </Text>
            </Grid.Row>
            <Grid.Row>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Observation and Finding{' '}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>
              {isEdit ? (
                <InputAll
                  placeholder="key Observation and Finding"
                  handleOnChange={(e) =>
                    control.setField('initialRemark', e.target.value)
                  }
                  value={control.initialRemark}
                  error={control.validateFormModel.validateInitialRemark}
                />
              ) : (
                <WrapperControlDetail
                  error={control.validateFormModel.validateInitialRemark}
                >
                  <div style={{ width: '100%', display: 'flex' }}>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {control.initialRemark || '-'}
                    </Text>
                  </div>
                </WrapperControlDetail>
              )}
            </Grid.Row>
            <Grid.Row>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Enhancement/Corrective Action{' '}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>
              {isEdit ? (
                <InputAll
                  placeholder="Key Enhancement/Corrective Action"
                  handleOnChange={(e) =>
                    control.setField('suggestion', e.target.value)
                  }
                  value={control.suggestion}
                  error={control.validateFormModel.validateSuggestion}
                />
              ) : (
                <WrapperControlDetail
                  error={control.validateFormModel.validateSuggestion}
                >
                  <div style={{ width: '100%', display: 'flex' }}>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {control.suggestion || '-'}
                    </Text>
                  </div>
                </WrapperControlDetail>
              )}
            </Grid.Row>
            <Grid.Row columns={3}>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Responsible Division{' '}
                </Text>
                {isEdit ? (
                  <InputAll
                    disabled
                    value={assessmentDetailModel.info.indicator}
                  />
                ) : (
                  <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                    {assessmentDetailModel.info.indicator}
                  </Text>
                )}
              </Grid.Column>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Co-Responsible Division
                </Text>
                {isEdit ? (
                  <div>
                    {!checkResponsible ? (
                      <div onClick={() => setCoResponsible(true)}>
                        <PartnerListDropdown
                          handleOnChange={(e) => {
                            console.log(e);
                            const partnerDisplay = {
                              label: e.label,
                              value: e.value,
                            };

                            const submitPartner = {
                              // indicator: e.label,
                              divisionNo: e.value,
                            };

                            control.setField('partner', submitPartner);
                            control.setField('showPartner', partnerDisplay);
                          }}
                          placeholder="Search Responsible Division"
                          value={control.showPartner}
                        />
                      </div>
                    ) : (
                      <PartnerListDropdown
                        handleOnChange={(e) => {
                          console.log(e);
                          const partnerDisplay = {
                            label: e.label,
                            value: e.value,
                          };

                          const submitPartner = {
                            // indicator: e.label,
                            divisionNo: e.value,
                          };

                          control.setField('partner', submitPartner);
                          control.setField('showPartner', partnerDisplay);
                        }}
                        placeholder="Search Responsible Division"
                        value={control.showPartner}
                      />
                    )}
                  </div>
                ) : (
                  <div style={{ width: '100%', display: 'flex' }}>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {control.partner ? control.showPartner.label : '-'}
                    </Text>
                  </div>
                )}
              </Grid.Column>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Due Date{' '}
                  <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span>
                </Text>
                {isEdit ? (
                  <NewDates
                    value={control.convertTime}
                    onChange={(date) => {
                      control.setField('endDate', date[0].valueOf());
                    }}
                    options={{
                      altFormat: 'd/m/Y',
                      altInput: true,
                      minDate: 'today',
                    }}
                    onError={control.validateFormModel.validateEndDate}
                  />
                ) : (
                  <WrapperControlDetail
                    error={control.validateFormModel.validateEndDate}
                  >
                    <div style={{ width: '100%', display: 'flex' }}>
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {control.endDate
                          ? moment(control.endDate).format('DD/MM/YYYY')
                          : '-'}
                      </Text>
                    </div>
                  </WrapperControlDetail>
                )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={3}>
              {control.rateId === 3 && (
                <Grid.Column style={{ paddingLeft: 0 }}>
                  <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                    Benefit{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  {isEdit ? (
                    <InputDropdown
                      options={BENEFIT_OPTIONS}
                      placeholder="Select Benefit"
                      handleOnChange={(e, { value }) =>
                        control.setField('benefit', value)
                      }
                      value={control.benefit}
                      textDefault={control.benefit || null}
                      onError={control.validateFormModel.validateBenefit}
                    />
                  ) : (
                    <WrapperControlDetail
                      error={control.validateFormModel.validateBenefit}
                    >
                      <div style={{ width: '100%', display: 'flex' }}>
                        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                          {control.benefit || '-'}
                        </Text>
                      </div>
                    </WrapperControlDetail>
                  )}
                </Grid.Column>
              )}

              <Grid.Column style={{ paddingLeft: 0 }}>
                {isEdit && control.benefit ? (
                  <div>
                    <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                      {control.benefit}{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <InputAll
                      minWidth={60}
                      placeholder={
                        control.benefit === 'Financial'
                          ? 'e.g. Reduce cost xxx.xx MB'
                          : 'e.g. Increase accuracy of report'
                      }
                      handleOnChange={(e) =>
                        control.setField('benefitText', e.target.value)
                      }
                      value={control.benefitText}
                      error={control.validateFormModel.validateBenefitText}
                    />
                  </div>
                ) : (
                  <div>
                    {control.benefit && (
                      <>
                        <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                          {control.benefit}{' '}
                          <span
                            style={{ fontSize: sizes.m, color: colors.red }}
                          >
                            *
                          </span>
                        </Text>
                        <WrapperControlDetail
                          error={control.validateFormModel.validateBenefitText}
                        >
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            {control.benefitText}
                          </Text>
                        </WrapperControlDetail>
                      </>
                    )}
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
          </>
        )}
      </Grid>
      <AttachFilesModal
        active={activeAttachFileModal}
        onClose={() => setActiveAttachFileModal(false)}
        control={control}
        onAttatchFiles={(title, files) => {
          console.log('onAttatchFiles', title, files);

          control.onAddAttatchFiles(title, files);
        }}
      />

      {/* confrim delete */}
      <ModalGlobal
        open={coResponsible}
        title="Warning !!"
        content="Please contact your Co-responsible Division for ackowledgmment before select."
        cancelText="CLOSE"
        onClose={() => {
          setCheckResponsible(true);
          setCoResponsible(false);
        }}
      />
    </div>
  ));
};

export default index;
