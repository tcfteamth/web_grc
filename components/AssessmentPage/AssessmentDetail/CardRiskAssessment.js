import React, { useState } from 'react';
import {
  Image,
  Grid,
  GridRow,
  Popup,
  TextArea as TextArea2,
  Form,
  Message,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonAll,
  InputAll,
  DropdownAll,
  TextArea,
  InputGroup,
  ModalGlobal,
  NewTextArea,
} from '../../element';
import { CardControlAssessment, AddCardControlAssessment } from '..';
import { MappingModal, LibraryRiskModal, LibraryControlModal } from '../modal';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  risk,
  newRisk,
  assessmentDetailModel,
  objects,
  riskIndex,
}) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [mappingModal, setMappingModal] = useState(false);
  const [libraryRisk, setLibraryRisk] = useState(false);
  const [libraryControl, setLibraryControl] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);

  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handelAdd = (value) => {
    setIsAdd(value);
  };
  return useObserver(() => (
    <div>
      <Grid columns="equal" style={{ margin: 0, padding: 24 }}>
        <Grid.Column computer={2} style={{ padding: 0 }}>
          <div style={{ display: 'flex' }} id={risk.navId}>
            {risk.standardFullPath ? (
              <Popup
                wide="very"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  padding: '16px',
                  marginLeft: -10,
                }}
                trigger={
                  <Image
                    height={18}
                    width={18}
                    style={{ marginRight: 8 }}
                    src="/static/images/zing@3x.png"
                  />
                }
              >
                <Text
                  fontSize={sizes.xl}
                  color={colors.textSky}
                  style={{ lineHeight: '1.2' }}
                >
                  GC's standard
                </Text>
                <Text
                  fontSize={sizes.s}
                  color={colors.primary}
                  fontWeight="bold"
                  style={{ paddingTop: 8 }}
                >
                  Risk:
                </Text>
                <Text color={colors.primaryBlack}>{risk.standardText}</Text>
                <Text
                  fontSize={sizes.s}
                  color={colors.primary}
                  fontWeight="bold"
                  style={{ paddingTop: 12 }}
                >
                  Related Law & Regulations
                </Text>
                <Text style={{ whiteSpace: 'pre-wrap' }} fontSize={sizes.s} color={colors.primaryBlack}>
                  <span style={{ fontWeight: 'bold', display: 'inline'}}>
                    {'External: '}
                  </span>
                  <span style={{ display: 'inline'}}>
                    {risk.complianceCore || '-'}
                  </span>
                </Text>
                <Text style={{ whiteSpace: 'pre-wrap' }} fontSize={sizes.s} color={colors.primaryBlack}>
                  <span style={{ fontWeight: 'bold', display: 'inline'}}>
                    {'Law Summary: '}
                  </span>
                  <span style={{ display: 'inline'}}>
                    {risk.externalLawSummary || '-'}
                  </span>
                </Text>
                <Text style={{ whiteSpace: 'pre-wrap' }} fontSize={sizes.s} color={colors.primaryBlack}>
                  <span style={{ fontWeight: 'bold', display: 'inline'}}>
                    {'Internal: '}
                  </span>
                  <span style={{ display: 'inline'}}>
                    {risk.internalLaws || '-'}
                  </span>
                </Text>
                <Text style={{ whiteSpace: 'pre-wrap' }} fontSize={sizes.s} color={colors.primaryBlack}>
                  <span style={{ fontWeight: 'bold', display: 'inline'}}>
                    {'Law Summary: '}
                  </span>
                  <span style={{ display: 'inline'}}>
                    {risk.internalLawSummary || '-'}
                  </span>
                </Text>
              </Popup>
            ) : (
              <Image
                height={18}
                width={18}
                style={{ marginRight: 8 }}
                src="/static/images/zinggray@3x.png"
              />
            )}

            <Text fontSize={sizes.m} color={colors.primary}>
              Risk
              <span
                style={{
                  paddingLeft: 4,
                  fontWeight: 'bold',
                  marginRight: '16px',
                }}
              >
                {`R${risk.no}`}{' '}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </span>
            </Text>
          </div>
        </Grid.Column>
        <Grid.Column
          computer={13}
          style={{
            padding: 0,
            width: '90%',
            wordWrap: 'break-word',
          }}
        >
          <NewTextArea
            placeholder="Risk Description"
            value={risk.nameTh}
            onChange={(e) => risk.setField('nameTh', e.target.value)}
            error={risk.validateRiskFormModel.validateRiskName}
            disabled={!isEdit}
            active={isEdit}
          />
        </Grid.Column>
        <Grid.Column computer={1} style={{ padding: 0 }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            {assessmentDetailModel.canUpdate.delete && (
              <div className="click" onClick={() => setHandelDelete(true)}>
                <Image
                  height={18}
                  width={18}
                  style={{ marginRight: 16 }}
                  src="/static/images/delete-gray@3x.png"
                />
              </div>
            )}

            {assessmentDetailModel.canUpdate.wording && (
              <div className="click" onClick={() => handelEdit(!isEdit)}>
                <Text
                  fontSize={sizes.m}
                  color={isEdit ? colors.textBlack : colors.textlightGray}
                >
                  Edit
                </Text>
              </div>
            )}
          </div>
        </Grid.Column>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {risk.controls.map((control, index) => (
            <div style={{ width: '100%' }}>
              {index !== 0 && <Divider bgcolor={colors.primaryBackground} />}
              {!control.id ? (
                <div style={{ width: '100%' }}>
                  <AddCardControlAssessment
                    control={control}
                    no={index}
                    risk={risk}
                    assessmentDetailModel={assessmentDetailModel}
                  />
                </div>
              ) : (
                <CardControlAssessment
                  isEdit={isEdit}
                  control={control}
                  risk={risk}
                  assessmentDetailModel={assessmentDetailModel}
                />
              )}
            </div>
          ))}
        </Grid.Row>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {isAdd && (
            <div style={{ width: '100%' }}>
              <AddCardControlAssessment />
              <Divider />
            </div>
          )}
          <Div mid>
            <ButtonAll
              textSize={sizes.xs}
              textColor={colors.pink}
              color="#FFFFFF"
              text="ADD CONTROL"
              icon="/static/images/iconPlus-pink@3x.png"
              // onClick={() => handelAdd(!isAdd)}
              onClick={() => risk.addControl()}
              disabled={!assessmentDetailModel.canUpdate.add}
            />
          </Div>
        </Grid.Row>
      </Grid>
      <DividerLine />

      <LibraryRiskModal
        active={libraryRisk}
        onClose={() => setLibraryRisk(false)}
      />

      <LibraryControlModal
        active={libraryControl}
        onClose={() => setLibraryControl(false)}
      />

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title={'Delete'}
        content={'Do you want to delete risk and its control?'}
        onClose={() => setHandelDelete(false)}
        onSubmit={() => {
          objects.deleteRisk(risk.fullPath);
          setHandelDelete(false);
        }}
        submitText={'YES'}
        cancelText={'NO'}
      />
    </div>
  ));
};

export default index;
