import React, { useState, useEffect } from 'react';
import { Image } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { Text, InputAll, InputDropdown, ButtonBorder } from '../../../element';
import { colors, sizes } from '../../../../utils';
import { CardCreateRiskAssessment } from '../PageCreateAssessment';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  margin-bottom: 24px;
  width: 100%;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 110px;
  padding: 24px;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const RiskAndControlList = (props) => {
  // const riskAndControlContext = useLocalStore(() => initRiskAndControlContext);
  const [isEdit, setIsEdit] = useState(false);
  const [isNewRisk, setIsNewRisk] = useState(false);
  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handleAddNewRisk = (value) => {
    setIsNewRisk(value);
  };

  useEffect(() => {
    console.log('cleanup');
  }, []);

  return useObserver(() => (
    <div style={{ paddingTop: 16 }}>
      <CardAll>
        <CardTop>
          <Div center>
            <Image
              src="../../static/images/iconObjective-white@3x.png"
              style={{
                width: 24,
                height: 24,
                marginRight: 8,
              }}
            />
            <Text
              color={colors.backgroundPrimary}
              fontWeight={'bold'}
              fontSize={sizes.s}
              style={{ marginRight: 8 }}
            >
              Objective
              <span
                style={{ fontSize: sizes.m, color: colors.primaryBackground }}
              >
                *
              </span>
              :
            </Text>
            <InputAll placeholder="Objective Description" />
          </Div>
          <Div top={10} center>
            <Image
              src="../../static/images/iconList-white@3x.png"
              style={{
                width: 24,
                height: 24,
                marginRight: 8,
              }}
            />
            <Text
              color={colors.primaryBackground}
              fontWeight={'bold'}
              fontSize={sizes.s}
              style={{ marginRight: 8 }}
            >
              Objective Type
              <span
                style={{ fontSize: sizes.m, color: colors.primaryBackground }}
              >
                *
              </span>
              :
            </Text>
            <div style={{ width: '350px' }}>
              <InputDropdown placeholder="Select ORC" />
            </div>
          </Div>
        </CardTop>
        <CardBody>
          <CardCreateRiskAssessment newRisk={isNewRisk} />
          <Div
            mid
            style={{ padding: 24 }}
            onClick={() => handleAddNewRisk(!isNewRisk)}
          >
            {isNewRisk ? (
              <ButtonBorder
                width={230}
                height={40}
                fontSize={sizes.s}
                textColor={colors.backgroundPrimary}
                borderColor={colors.textSky}
                color={colors.textSky}
                textUpper
                textWeight={'med'}
                text={'Create Risk Now'}
              />
            ) : (
              <ButtonBorder
                width={230}
                height={40}
                fontSize={sizes.s}
                textColor={colors.textSky}
                borderColor={colors.textSky}
                textUpper
                textWeight={'med'}
                text={'Add Risk'}
                icon={'../../static/images/iconPlus@3x.png'}
              />
            )}
          </Div>
        </CardBody>
      </CardAll>
    </div>
  ));
};

export default RiskAndControlList;
