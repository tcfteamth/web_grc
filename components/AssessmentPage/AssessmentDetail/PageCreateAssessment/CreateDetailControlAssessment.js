import React, { useState, useEffect } from 'react';
import { Image, Grid, Table, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import Files from 'react-files';
import { colors, sizes } from '../../../../utils';
import {
  Text,
  ButtonFile,
  ButtonAdd,
  InputDropdown,
  InputAll,
  DatePicker,
  DropdownAll,
  NewDates,
  ButtonBorder,
  Box,
  TextArea,
  ModalGlobal,
  NewTextArea,
} from '../../../element';
import {
  CONTROL_TYPE_OPTIONS,
  CONTROL_FORMAT_OPTIONS,
  CONTROL_FREQUENCY_OPTIONS,
} from '../../../../utils/assessment/controlOptions';
import { AttachFilesModal } from '../../modal';
import { PartnerListDropdown } from '../../container';
import { BENEFIT_OPTIONS } from '../../../../utils/assessment/benefitOptions';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({ control, assessmentDetailModel }) => {
  const [stateDate, setStateDate] = useState();
  const [stateOnFocus, setStateOnFocus] = useState();
  const [benefit, setBenefit] = useState();
  const [screenWidth, setScreenWidth] = useState();
  const [activeAttachFileModal, setActiveAttachFileModal] = useState(false);
  const [errorMessage, setErrorMessage] = useState();

  const [coResponsible, setCoResponsible] = useState(false);
  const [checkResponsible, setCheckResponsible] = useState(false);

  useEffect(() => {
    setScreenWidth(window.screen.width);
  }, []);

  return useObserver(() => (
    <div>
      <Grid style={{ margin: 0 }}>
        {/* // responsive screen 1024 not support screen 768*/}
        <Grid.Row style={{ padding: 0 }}>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>

                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Grid
                        columns="equal"
                        style={{ margin: 0, paddingRight: 16 }}
                      >
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Preventive control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              Preventive control - attempt to defer or stop an
                              unwanted outcome before it happens (e.g., use of
                              passwords, approval, policies, procedures).
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Detective control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                Detective control - attempt to detect errors or
                                irregularities that may have already occurred
                                (e.g., reconciliations, monitoring of actual
                                expenses vs budget, prior periods, forecasts).
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3} style={{ padding: 8 }}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 8,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Grid
                        columns="equal"
                        style={{ margin: 0, paddingRight: 16 }}
                      >
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Manual Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              Manual controls are performed by individuals.
                              Outside of a system (e.g., approval, review,
                              segregration of duties).
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Automate Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                Automated control are performed entirely by the
                                computer system (e.g., access right, credit
                                limit in in processing system).
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              IT Dependent Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                IT Dependent Control are performed by
                                individuals and requires some level of system
                                involvement.
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={4}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Information technology system{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide="very"
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginRight: -10,
                      }}
                      position="top right"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        e.g. SAP ARIBA SALEFORCE
                      </Text>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Executor{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Control Type"
                    options={CONTROL_TYPE_OPTIONS}
                    handleOnChange={(e, { value }) =>
                      control.setField('type', value)
                    }
                    textDefault={control.type || null}
                    value={control.type}
                    onError={control.validateFormModel.validateType}
                  />
                </Table.Cell>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Control Classification"
                    options={CONTROL_FORMAT_OPTIONS}
                    handleOnChange={(e, { value }) => {
                      control.setField('format', value);

                      if (value === 'Manual Control') {
                        control.setField('techControl', '-');
                      } else {
                        control.setField('techControl', '');
                      }
                    }}
                    textDefault={control.format || null}
                    value={control.format}
                    onError={control.validateFormModel.validateFormat}
                  />
                </Table.Cell>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Frequency"
                    options={CONTROL_FREQUENCY_OPTIONS}
                    handleOnChange={(e, { value }) =>
                      control.setField('frequency', value)
                    }
                    textDefault={control.frequency || null}
                    value={control.frequency}
                    onError={control.validateFormModel.validateFrequency}
                  />
                </Table.Cell>
                <Table.Cell width={4}>
                  <InputAll
                    placeholder="Key Information technology system"
                    handleOnChange={(e) => {
                      control.setField('techControl', e.target.value);
                    }}
                    disabled={!control.isTypeAutomateControl}
                    value={control.techControl}
                    error={control.validateFormModel.validateTechControl}
                  />
                </Table.Cell>
                <Table.Cell width={3}>
                  <InputAll
                    placeholder="Key Process owner"
                    handleOnChange={(e) =>
                      control.setField('employeeController', e.target.value)
                    }
                    value={control.employeeController}
                    error={control.validateFormModel.validateEmployeeController}
                  />
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>

        <Grid.Column computer={screenWidth < 1367 ? 3 : 2}>
          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
            Document / Evidence
            <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
          </Text>
        </Grid.Column>
        <Grid.Column computer={screenWidth < 1367 ? 13 : 14}>
          <NewTextArea
            onChange={(e) => {
              control.setField('titles', e.target.value);
            }}
            value={control.titles}
            maxLength={4000}
            error={control.validateFormModel.validateDocument}
            active
          />

          <diV
            style={{
              marginTop: '8px',
              display: 'flex',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}
          >
            {control.displayFiles.map((file, index) => (
              <div style={{ marginTop: '-2px' }}>
                <ButtonAdd
                  text={`${file.title || file.name}`}
                  textSize={sizes.xs}
                  href={file.filePath || false}
                  onClickRemove={() =>
                    control.deleteAssessmentFiles(file.id, file.filesType)
                  }
                />
              </div>
            ))}

            <Files
              style={customStyle.styleAddFile}
              onChange={(files) => {
                console.log('add file', files);
                control.files.push(files[files.length - 1]);
              }}
              accepts={[
                'image/png',
                '.jpg',
                '.pdf',
                '.xlsx',
                '.docx',
                '.pptx',
                '.jpeg',
              ]}
              // maxFiles={1}
              multiple
              maxFileSize={5242880}
              minFileSize={0}
              clickable
              onError={(error, file) => {
                if (error) {
                  setErrorMessage('File Size Maximum 5 MB');
                } else {
                  setErrorMessage();
                }
              }}
            >
              Attach File
            </Files>
            <Text
              fontSize={sizes.xs}
              color={colors.primaryBlack}
              style={{ marginLeft: '8px' }}
            >
              Optional
            </Text>
          </diV>
        </Grid.Column>

        {control.doNeedImprovement && (
          <>
            <Grid.Row style={{ paddingTop: 24 }}>
              <Text
                fontSize={sizes.s}
                color={colors.textPurple}
                fontWeight="bold"
              >
                Enhancement / Corrective Action
              </Text>
            </Grid.Row>
            <Grid.Row>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Observation and Finding{' '}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>
              <InputAll
                placeholder="key Observation and Finding"
                handleOnChange={(e) =>
                  control.setField('initialRemark', e.target.value)
                }
                value={control.initialRemark}
                error={control.validateFormModel.validateInitialRemark}
              />
            </Grid.Row>
            <Grid.Row>
              <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                Enhancement/Corrective Action{' '}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </Text>

              <InputAll
                placeholder="Key Enhancement/Corrective Action"
                handleOnChange={(e) =>
                  control.setField('suggestion', e.target.value)
                }
                value={control.suggestion}
                error={control.validateFormModel.validateSuggestion}
              />
            </Grid.Row>
            <Grid.Row columns={3}>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Responsible Division{' '}
                </Text>
                <InputAll
                  disabled
                  value={assessmentDetailModel.info.indicator}
                />
              </Grid.Column>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Co-Responsible Division
                  {/* <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span> */}
                </Text>
                <PartnerListDropdown
                  handleOnChange={(e) => {
                    setCoResponsible(true);
                    const partnerDisplay = {
                      label: e.label,
                      value: e.value,
                    };

                    const submitPartner = {
                      // indicator: e.label,
                      divisionNo: e.value,
                    };

                    control.setField('partner', submitPartner);
                    control.setField('showPartner', partnerDisplay);
                  }}
                  placeholder="Search Responsible Division"
                  value={control.showPartner}
                />
              </Grid.Column>
              <Grid.Column style={{ paddingLeft: 0 }}>
                <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                  Due Date{' '}
                  <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span>
                </Text>
                <NewDates
                  value={control.convertTime}
                  onChange={(date) => {
                    control.setField('endDate', date[0].valueOf());
                  }}
                  options={{
                    altFormat: 'd/m/Y',
                    altInput: true,
                  }}
                  onError={control.validateFormModel.validateEndDate}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={3}>
              {control.rateId === 3 && (
                <Grid.Column style={{ paddingLeft: 0 }}>
                  <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                    Benefit{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  <InputDropdown
                    options={BENEFIT_OPTIONS}
                    placeholder="Select Benefit"
                    handleOnChange={(e, { value }) =>
                      control.setField('benefit', value)
                    }
                    value={control.benefit}
                    textDefault={control.benefit || null}
                    onError={control.validateFormModel.validateBenefit}
                  />
                </Grid.Column>
              )}

              <Grid.Column style={{ paddingLeft: 0 }}>
                {control.benefit && (
                  <div>
                    <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
                      {control.benefit}{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <InputAll
                      minWidth={60}
                      placeholder={
                        control.benefit === 'Financial'
                          ? 'e.g. Reduce cost xxx.xx MB'
                          : 'e.g. Increase accuracy of report'
                      }
                      handleOnChange={(e) =>
                        control.setField('benefitText', e.target.value)
                      }
                      value={control.benefitText}
                      error={control.validateFormModel.validateBenefitText}
                    />
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
          </>
        )}
      </Grid>
      <AttachFilesModal
        active={activeAttachFileModal}
        onClose={() => setActiveAttachFileModal(false)}
        control={control}
        onAttatchFiles={(title, files) => {
          console.log('onAttatchFiles', title, files);

          control.onAddAttatchFiles(title, files);
        }}
      />

      <ModalGlobal
        open={coResponsible}
        title="Warning !!"
        content="Please contact your Co-responsible Division for ackowledgmment before select."
        cancelText="CLOSE"
        onClose={() => {
          setCheckResponsible(true);
          setCoResponsible(false);
        }}
      />
    </div>
  ));
};

export default index;
