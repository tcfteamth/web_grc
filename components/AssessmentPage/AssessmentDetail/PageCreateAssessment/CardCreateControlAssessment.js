import React, { useState } from 'react';
import { Image, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../../../utils';
import { Text, InputAll } from '../../../element';
import { CreateDetailControlAssessment } from '.';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ item, mapping, edit }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isRating, setIsRating] = useState();
  const [isRatingDefalue, setIsRatingDefalue] = useState('');
  const handelRating = (value) => {
    setIsRating(value);
  };
  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return (
    <div style={{ width: '100%' }}>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Column tablet={6} computer={3} style={{ paddingTop: 0 }}>
          <Div center>
            <Text
              style={{ width: '40%' }}
              fontSize={sizes.s}
              color={colors.pink}
            >
              Control
            </Text>
            <div style={{ width: '60%' }}>
              <InputAll style={{ marginLeft: 8 }} placeholder="Control..." />
            </div>
          </Div>
        </Grid.Column>
        <Grid.Column tablet={10} computer={13} style={{ padding: 0 }}>
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <InputAll placeholder="Control Description" />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                paddingLeft: 16,
                marginTop: -4,
                justifyContent: 'center',
              }}
            >
              {isOpen ? (
                <Image
                  onClick={() => handelOpen(!isOpen)}
                  width={24}
                  src="/static/images/iconUp-blue@3x.png"
                />
              ) : (
                <Image
                  onClick={() => handelOpen(!isOpen)}
                  width={24}
                  src="/static/images/iconDown-blue@3x.png"
                />
              )}
              <Image
                width={18}
                style={{ marginLeft: 3 }}
                src="/static/images/delete@2x.png"
              />
            </div>
          </div>
        </Grid.Column>
      </Grid>
      <CreateDetailControlAssessment />
    </div>
  );
};

export default index;
