import React, { useState } from 'react';
import { Image, Grid, Segment } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, TextArea, BottomBarAssessment } from '../../element';
import { listRisk } from '../../../utils/static';
import { CardRiskAssessment, CardRiskAssessmentVP } from '..';
import styled from 'styled-components';

const CardContener = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: 100%;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 110px;
  padding: 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const CardFoot = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  display: flex;
`;

const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-top: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = () => {
  const [isEdit, setIsEdit] = useState(false);
  const [isNewRisk, setIsNewRisk] = useState(false);
  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handleAddNewRisk = (value) => {
    setIsNewRisk(value);
  };
  return (
    <div>
      <CardContener>
        <CardTop>
          <Div col style={{ paddingRight: 16 }}>
            <Div center>
              <Image
                src="../../static/images/iconObjective-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight={'bold'}
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective
                <span
                  style={{ fontSize: sizes.m, color: colors.primaryBackground }}
                >
                  *
                </span>
                :
              </Text>
              <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                สร้างความเชื่อมั่นอย่างสมเหตุสมผลว่า
                บริษัทมีการควบคุมภายในที่เพียงพอเหมาะสม และปฏิบัติอย่างสม่ำเสมอ
              </Text>
            </Div>
            <Div top={10} center>
              <Image
                src="../../static/images/iconList-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight={'bold'}
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective Type:
              </Text>
              <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                ด้านการดำเนินการ (Operations)
              </Text>
            </Div>
          </Div>
        </CardTop>
        <CardBody>
          <CardRiskAssessmentVP listRisk={listRisk} />
        </CardBody>
        <CardFoot />
      </CardContener>

      <CardComment>
        <Text
          color={colors.primaryBlack}
          fontSize={sizes.xxs}
          fontWeight={'med'}
          className="upper"
        >
          Comment
        </Text>
        <TextArea height={160} placeholder={'กรุณากรอกรายละเอียด '} />
      </CardComment>
      <BottomBarAssessment page={'AssessmentDetailVP'} />
    </div>
  );
};

export default index;
