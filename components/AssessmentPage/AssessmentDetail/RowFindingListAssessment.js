import React from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import { Text, CheckBoxAll, TextArea } from '../../element';
import { colors, sizes } from '../../../utils';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 0px 14px;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  border-top: 1px solid #d9d9d6;
  display: flex;
  align-items: center;
  flex-direction: row;
  width: auto;
`;

const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: start;
`;

const CardComment = styled.div`
  min-height: 24px;
  padding: 24px !important;
  width: 100%;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const PageAction = ({ ia }) => {
  return useObserver(() => (
    <div>
      <Div>
        <TableBody key={ia.findingNo}>
          <Div>
            <Cell width={50} center>
              <CheckBoxAll
                status="Accept"
                checked={ia.isCheckYes}
                onChange={() => ia.setCheck(true)}
              />
            </Cell>
            <Cell width={50} center>
              <CheckBoxAll
                status="Reject"
                checked={ia.isCheckNo}
                onChange={() => ia.setCheck(false)}
              />
            </Cell>
            <Cell width={150}>
              <div className="break-word" style={{ width: '150px' }}>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  {ia.auditEngagementCode ? ia.auditEngagementCode : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={300}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '300px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.auditEngagementName ? ia.auditEngagementName : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={150}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '150px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.type ? ia.type : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.auditUnitLink ? ia.auditUnitLink : '-'}
                </Text>
              </div>
            </Cell>

            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.subProcess ? ia.subProcess : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.categorizedRecommendation
                    ? ia.categorizedRecommendation
                    : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.focusObjective ? ia.focusObjective : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={600}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '600px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.findingNo && ia.findingNo}
                  {ia.findingName ? ia.findingName : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={600}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '600px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.findingDescription ? ia.findingDescription : '-'}
                </Text>
              </div>
            </Cell>

            <Cell width={100}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '100px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.findingRate ? ia.findingRate : '-'}
                </Text>
              </div>
            </Cell>

            <Cell width={600}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '600px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.rootCause ? ia.rootCause : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={600}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '600px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.impact ? ia.impact : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={600}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '600px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.recommendationDescription
                    ? ia.recommendationDescription
                    : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.issueState ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {ia.issueState}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>

            <Cell width={200}>
              <div className="break-word" style={{ width: '200px' }}>
                {ia.recommendationState ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {ia.recommendationState}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>

            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.targetDate ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(ia.targetDate).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>

            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.revisedTargetDate ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(ia.revisedTargetDate).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>

            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.closed ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(ia.closed).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.closedBy ? ia.closedBy : '-'}
                </Text>
              </div>
            </Cell>

            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.actualStart ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(ia.actualStart).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.actualEnd ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(ia.actualEnd).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>

            <Cell width={300}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '300px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.teamLead ? ia.teamLead : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={300}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '300px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.teamMember ? ia.teamMember : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={300}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '300px' }}
              >
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {ia.auditUnits ? ia.auditUnits : '-'}
                </Text>
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.remark1 ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {ia.remark1}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>
            <Cell width={200}>
              <div
                className="break-word"
                style={{ paddingRight: 10, width: '200px' }}
              >
                {ia.remark2 ? (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {ia.remark2}
                  </Text>
                ) : (
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                )}
              </div>
            </Cell>
          </Div>
        </TableBody>
      </Div>
      <CardComment>
        <Text
          color={colors.primaryBlack}
          fontSize={sizes.xxs}
          fontWeight="med"
          className="upper"
        >
          Comment
        </Text>
        <TextArea
          placeholder="กรุณากรอกรายละเอียด"
          value={ia.submitIAFindingModel.comment || ia.comment}
          onChange={(e) =>
            ia.submitIAFindingModel.setField('comment', e.target.value)
          }
        />
      </CardComment>
    </div>
  ));
};

export default PageAction;
