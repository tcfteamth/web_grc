import React, { useState } from 'react';
import { Image, Grid, Table, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../../utils';
import { Text, ButtonBorder } from '../../element';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ control }) => (
  <div style={{ width: '100%' }}>
    <Grid style={{ margin: 0 }}>
      <Grid.Row style={{ padding: 0 }}>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Control Type{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    position="top left"
                    trigger={
                      <Image
                        width={18}
                        style={{ marginLeft: 8 }}
                        src="../../static/images/iconRemark@3x.png"
                      />
                    }
                  >
                    {control.type === 'Preventive control' ? (
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        Preventive control - attempt to defer or stop an
                        unwanted outcome before it happens (e.g., use of
                        passwords, approval, policies, procedures).
                      </Text>
                    ) : (
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        Detective control - attempt to detect errors or
                        irregularities that may have already occurred (e.g.,
                        reconciliations, monitoring of actual expenses vs
                        budget, prior periods, forecasts).
                      </Text>
                    )}
                  </Popup>
                </Div>
              </Table.HeaderCell>
              <Table.HeaderCell width={3} style={{ padding: 8 }}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Control Classification{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    position="top left"
                    trigger={
                      <Image
                        width={18}
                        style={{ marginLeft: 8 }}
                        src="../../static/images/iconRemark@3x.png"
                      />
                    }
                  >
                    {control.format === 'Manual Control' ? (
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        Manual controls are performed by individuals. Outside of
                        a system (e.g., approval, review, segregration of
                        duties).
                      </Text>
                    ) : control.format === 'Automate Control' ? (
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        Automated control are performed entirely by the computer
                        system (e.g., access right, credit limit in in
                        processing system).
                      </Text>
                    ) : (
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        IT Dependent Control are performed by individuals and
                        requires some level of system involvement.
                      </Text>
                    )}
                  </Popup>
                </Div>
              </Table.HeaderCell>
              <Table.HeaderCell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Frequency{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                </Div>
              </Table.HeaderCell>
              <Table.HeaderCell width={4}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Information technology system{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    position="top left"
                    trigger={
                      <Image
                        width={18}
                        style={{ marginLeft: 8 }}
                        src="../../static/images/iconRemark@3x.png"
                      />
                    }
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      e.g. SAP ARIBA SALEFORCE
                    </Text>
                  </Popup>
                </Div>
              </Table.HeaderCell>
              <Table.HeaderCell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Executor{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                </Div>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {control.type || 'type'}
                  </Text>
                </Div>
              </Table.Cell>
              <Table.Cell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {control.format || 'format'}
                  </Text>
                </Div>
              </Table.Cell>
              <Table.Cell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {control.frequency || 'frequency'}
                  </Text>
                </Div>
              </Table.Cell>
              <Table.Cell width={4}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    -
                  </Text>
                </Div>
              </Table.Cell>
              <Table.Cell width={3}>
                <Div mid center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {control.document || 'document'}
                  </Text>
                </Div>
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </Grid.Row>

      <Grid.Row
        style={{ display: 'flex', alignItems: 'center', paddingBottom: 0 }}
      >
        <Text fontSize={sizes.xs} color={colors.primaryBlack}>
          Document / Evidence
        </Text>
        <div style={{ display: 'flex', paddingLeft: 12 }}>
          <ButtonBorder
            style={{ margin: '0px 4px' }}
            radius
            height={32}
            width={102}
            textColor={colors.textSky}
            borderColor={colors.textGrayLight}
            text="CSA - 56-1"
            textSize={sizes.xs}
          />
          <ButtonBorder
            style={{ margin: '0px 4px' }}
            radius
            height={32}
            width={102}
            textColor={colors.textSky}
            borderColor={colors.textGrayLight}
            text="CSA - 56-1"
            textSize={sizes.xs}
          />
          <ButtonBorder
            style={{ margin: '0px 4px' }}
            radius
            height={32}
            width={102}
            textColor={colors.textSky}
            borderColor={colors.textGrayLight}
            text="CSA - 56-1"
            textSize={sizes.xs}
          />
        </div>
      </Grid.Row>
    </Grid>
  </div>
);
export default index;
