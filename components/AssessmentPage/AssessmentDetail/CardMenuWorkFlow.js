import React, { useState, useEffect } from 'react';
import { Image, Grid } from 'semantic-ui-react';
import { colors } from '../../../utils';
import { Text, ButtonFileAdditions } from '../../element';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';

const CardContener = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  background-color: ${colors.backgroundPrimary};
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: 100%;
  min-height: 338px;
`;
const CardFile = styled.div`
  height: 80px;
  border-radius: 6px;
  border: solid 1px #d9d9d6;
  background-color: #ffffff;
  padding: 20px 24px;
  flex-direction: row;
  display: flex;
`;

const index = ({ assessmentDetailModel }) => {
  const [fileName, setFileName] = useState('');
  const [fileSize, setFileSize] = useState('');

  const handelFile = (files) => {
    // console.log('files', files[files.length - 1]);
    console.log('files', files);

    const file = files[files.length - 1];
    assessmentDetailModel.workflowFiles.push(file);
  };

  useEffect(() => {}, []);

  return useObserver(() => (
    <CardContener>
      <Grid style={{ margin: 0, padding: 16 }}>
        {assessmentDetailModel.workflowList &&
          assessmentDetailModel.workflowList.map((item, index) => (
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={8}
              style={{ padding: 8 }}
            >
              {/* <a href={item.filePath || item.preview.url} target="_blank"> */}
              <CardFile>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%',
                  }}
                >
                  <Image
                    height={40}
                    width={40}
                    style={{ marginRight: 8 }}
                    src="/static/images/pdf-blue@3x.png"
                  />
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      paddingLeft: 24,
                      width: '100%',
                    }}
                  >
                    <a href={item.filePath || item.preview.url} target="_blank">
                      <Text fontSice={20} color={colors.primaryBlack}>
                        {item.name || item.fileName || `File-${index + 1}`}
                      </Text>
                    </a>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}
                    >
                      <a
                        href={item.filePath || item.preview.url}
                        target="_blank"
                      >
                        <Text fontSice={20} color={colors.textlightGray}>
                          File Size : {item.sizeReadable || item.fileSize}
                        </Text>
                      </a>
                      <div
                        onClick={() =>
                          assessmentDetailModel.deleteWorkflowUploadFiles(
                            item.id,
                            item.filesType,
                          )
                        }
                      >
                        <Image
                          height={18}
                          width={18}
                          style={{ marginRight: 8 }}
                          src="/static/images/delete-gray@3x.png"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </CardFile>
              {/* </a> */}
            </Grid.Column>
          ))}
        <Grid.Column mobile={16} tablet={8} computer={8} style={{ padding: 8 }}>
          <CardFile>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <ButtonFileAdditions
                icon={'/static/images/iconPlus-navy@3x.png'}
                width={40}
                height={40}
                border={6}
                borderColor={'#0057b8'}
                handelOnChange={(value) => handelFile(value)}
              />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  paddingLeft: 24,
                }}
              >
                <Text fontSice={20} color={colors.primaryBlack}>
                  {fileName ? fileName : 'Upload File'}
                </Text>
                <Text fontSice={20} color={colors.textlightGray}>
                  File Size Maximum 5 MB
                </Text>
              </div>
            </div>
          </CardFile>
        </Grid.Column>
      </Grid>
    </CardContener>
  ));
};

export default index;
