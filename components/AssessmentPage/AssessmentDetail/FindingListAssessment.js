import React, { useState } from 'react';
import { useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import {
  Text,
  CheckBoxAll,
  BottomBarAssessment,
  CardEmpty,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { RowFindingListAssessment } from '..';

const DividerLineBlack = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? `#333333` : `#f6f6f6`)};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 0px 14px;
`;

const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
`;

const CardTab = styled.div`
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
`;

const headers = [
  { key: 'Yes', render: 'Yes', width: 50 },
  { key: 'No', render: 'No', width: 50 },
  { key: 'Audit Engagement Code', render: 'Audit Engagement Code', width: 150 },
  { key: 'Audit Engagement Name', render: 'Audit Engagement Name', width: 300 },
  { key: 'Type', render: 'Type', width: 150 },
  { key: 'Auditee Units (Link)', render: 'Auditee Units (Link)', width: 200 },
  { key: 'Sub-process', render: 'Sub-process', width: 200 },
  {
    key: 'Categorized Recommendation',
    render: 'Categorized Recommendation',
    width: 200,
  },
  {
    key: 'Focus Object/Risk/Control',
    render: 'Focus Object/Risk/Control',
    width: 200,
  },
  {
    key: 'Finding/Recommendation for CSA assessment',
    render: 'Finding/Recommendation for CSA assessment',
    width: 600,
  },
  {
    key: 'Finding Description',
    render: 'Finding Description',
    width: 600,
  },

  { key: 'Finding Rating', render: 'Finding Rating', width: 100 },

  { key: 'Root Cause', render: 'Root Cause', width: 600 },
  { key: 'Impact', render: 'Impact', width: 600 },
  {
    key: 'Recommendation Description',
    render: 'Recommendation Description',
    width: 600,
  },
  { key: 'Issue State', render: 'Issue State', width: 200 },
  {
    key: 'Recommendation State',
    render: 'Recommendation State',
    width: 200,
  },
  { key: 'Target Date', render: 'Target Date', width: 200 },

  { key: 'Revised Target Date', render: 'Revised Target Date', width: 200 },
  { key: 'Closed', render: 'Closed', width: 200 },
  { key: 'Closed by', render: 'Closed by', width: 200 },
  { key: 'Actual Start', render: 'Actual Start', width: 200 },
  { key: 'Actual End', render: 'Actual End', width: 200 },
  { key: 'Team Lead', render: 'Team Lead', width: 300 },
  { key: 'Team Member', render: 'Team Member', width: 300 },
  { key: 'Auditee Units', render: 'Auditee Units', width: 300 },
  { key: 'Remark1', render: 'Remark1', width: 200 },
  { key: 'Remark2', render: 'Remark2', width: 200 },
];

const PageAction = ({ iafindingDetailModel }) => {
  return useObserver(() => (
    <div width="100%">
      {iafindingDetailModel.iafindingDetailList.length > 0 ? (
        <CardTab className="w-highlight">
          <TableScroll>
            <Div col>
              <TableHeader>
                {headers.map(({ render, key, width, mid }) => (
                  <Cell key={key} width={width} center={mid}>
                    {render === 'Yes' || render === 'No' ? (
                      <Div col center>
                        <Text
                          fontSize={sizes.xs}
                          fontWeight="med"
                          color={render === 'No' ? colors.red : colors.primary}
                        >
                          {render}
                        </Text>
                        {render === 'Yes' && (
                          <CheckBoxAll
                            status={key}
                            onChange={() => {
                              iafindingDetailModel.setCheckYesAll(true);
                            }}
                            checked={iafindingDetailModel.isCheckYesAll}
                          />
                        )}
                        {render === 'No' && (
                          <CheckBoxAll
                            status={key}
                            onChange={() => {
                              iafindingDetailModel.setCheckNoAll(false);
                            }}
                            checked={iafindingDetailModel.isCheckNoAll}
                          />
                        )}
                      </Div>
                    ) : (
                      <Div center mid={mid}>
                        <Text
                          center
                          fontSize={sizes.xs}
                          style={{
                            paddingLeft: render === 'Assessment Name' && 16,
                          }}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                        </Text>
                      </Div>
                    )}
                  </Cell>
                ))}
              </TableHeader>
              {iafindingDetailModel.iafindingDetailList.map((ia) => (
                <RowFindingListAssessment ia={ia} />
              ))}
              <DividerLineBlack bottom style={{ marginBottom: 16 }} />
            </Div>
          </TableScroll>
        </CardTab>
      ) : (
        <CardEmpty textTitle="No IA Finding" />
      )}
      <BottomBarAssessment
        page="IaAssessment"
        iafindingDetailModel={iafindingDetailModel}
      />
    </div>
  ));
};

export default PageAction;
