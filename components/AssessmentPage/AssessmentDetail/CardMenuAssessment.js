import React, { useState } from 'react';
import { Image, Grid, Segment } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonBorder,
  InputAll,
  InputDropdown,
  CardRisk,
  TextArea,
  RadioBox,
  ModalGlobal,
  NewTextArea,
} from '../../element';
import { toJS } from 'mobx';
import {
  CardRiskAssessment,
  CardRiskAssessmentVP,
  CreateRiskAndControl,
} from '..';

import { initAuthStore } from '../../../contexts';
import { APPROVE_ASSESSMENT_STATUS } from '../../../model/AssessmentModel/ApproveAssessmentModel';
import { CardCreateRiskAssessment } from './PageCreateAssessment';
import { OBJECT_TYPES } from '../modal/CreateNewObjectModal';

const CardContener = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  width: 100%;
`;

const CardTop = styled.div`
  ${(props) => props.error && 'border: 2px solid red !important'};
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 110px;
  padding: 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const CardComment = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 24px;
  margin-top: 16px;
  display: flex;
  flex-direction: column;
  padding: 24px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  data,
  objectName,
  typeName,
  objects,
  assessmentDetailModel,
  objectIndex,
}) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isNewRisk, setIsNewRisk] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);

  const [objectNameTitle, setObjectNameTitle] = useState(objectName);
  const authContext = initAuthStore();

  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handleAddNewRisk = (value) => {
    setIsNewRisk(value);
  };

  const handleTextArea = (id) => {
    if (isEdit || !id) {
      return true;
    }

    return false;
  };

  return useObserver(() => (
    <div>
      <CardContener>
        <CardTop
          error={
            objects.validateObjectFormModel.validateNameTh ||
            objects.validateObjectFormModel.validateType
          }
        >
          <Div col style={{ paddingRight: 16 }}>
            <Div id={objects.navId}>
              <Image
                src="../../static/images/iconObjective-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective{' '}
                <span
                  style={{ fontSize: sizes.m, color: colors.primaryBackground }}
                >
                  *
                </span>
                :
              </Text>
              {/* {isEdit || !objects.id ? (
                <div style={{ width: '100%' }}>
                  <NewTextArea
                    placeholder="Key Objective"
                    value={objects.nameTh}
                    onChange={(e) => {
                      objects.setField('nameTh', e.target.value);
                    }}
                    active
                    height={50}
                  />
                </div>
              ) : (
                <div id={objects.navId}>
                  <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                    {objects.nameTh}
                  </Text>
                </div>
              )} */}
              <div style={{ width: '100%' }}>
                <NewTextArea
                  placeholder="Key Objective"
                  value={objects.nameTh}
                  onChange={(e) => {
                    objects.setField('nameTh', e.target.value);
                  }}
                  active={handleTextArea(objects.id)}
                  disabled={!handleTextArea(objects.id)}
                  height={50}
                />
              </div>
            </Div>
            <Div top={10} center>
              <Image
                src="../../static/images/iconList-white@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginRight: 8,
                }}
              />
              <Text
                color={colors.backgroundPrimary}
                fontWeight="bold"
                fontSize={sizes.s}
                style={{ marginRight: 8 }}
              >
                Objective Type
                <span
                  style={{ fontSize: sizes.m, color: colors.primaryBackground }}
                >
                  *
                </span>
                :
              </Text>
              {isEdit || !objects.id ? (
                <div style={{ minWidth: '300px' }}>
                  <InputDropdown
                    placeholder="Select Objective Type"
                    options={OBJECT_TYPES}
                    handleOnChange={(e, { value }) =>
                      objects.setField('type', value)
                    }
                    value={objects.type}
                  />
                </div>
              ) : (
                <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                  {objects.type || null}
                </Text>
              )}
            </Div>
          </Div>

          {!authContext.roles.isVP && (
            <div style={{ display: 'flex' }}>
              {assessmentDetailModel.canUpdate.delete && (
                <div
                  className="click"
                  style={{ display: 'flex' }}
                  onClick={() => setHandelDelete(true)}
                >
                  <Image
                    src="/static/images/delete-white@3x.png"
                    height={24}
                    width={24}
                    style={{
                      marginRight: 16,
                    }}
                  />
                </div>
              )}

              {assessmentDetailModel.canUpdate.add && (
                <div className="click" onClick={() => handelEdit(!isEdit)}>
                  {/* {isEdit ? (
                    <Image
                      height={24}
                      width={24}
                      src="/static/images/edit-white@3x.png"
                    />
                  ) : (
                    <Image
                      height={24}
                      width={24}
                      src="/static/images/edit-white@3x.png"
                    />
                  )} */}
                  <Text color={colors.backgroundPrimary} fontSize={sizes.s}>
                    Edit
                  </Text>
                </div>
              )}
            </div>
          )}
        </CardTop>
        <CardBody>
          {objects.risks.map((risk, riskIndex) => (
            <div style={{ width: '100%' }}>
              {!risk.id ? (
                <CardCreateRiskAssessment
                  risk={risk}
                  objects={objects}
                  assessmentDetailModel={assessmentDetailModel}
                />
              ) : (
                <CardRiskAssessment
                  risk={risk}
                  newRisk={isNewRisk}
                  objects={objects}
                  assessmentDetailModel={assessmentDetailModel}
                />
              )}
            </div>
          ))}

          <Div
            mid
            style={{ padding: 24 }}
            onClick={() => handleAddNewRisk(!isNewRisk)}
          >
            <ButtonBorder
              height={40}
              width={230}
              fontSize={sizes.s}
              textColor={colors.textSky}
              borderColor={colors.textSky}
              textUpper
              textWeight="med"
              text="Add Risk"
              icon="../../static/images/iconPlus@3x.png"
              handelOnClick={() => objects.addRisk()}
              disabled={!assessmentDetailModel.canUpdate.add}
            />
          </Div>
        </CardBody>
      </CardContener>

      {/* confrim delete */}

      <ModalGlobal
        open={handelDelete}
        title={'Delete'}
        content={'Do you want to delete objective and its risk /control?'}
        onSubmit={() => {
          assessmentDetailModel.deleteObject(objects.no);
          setHandelDelete(false);
        }}
        submitText={'YES'}
        cancelText={'NO'}
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default index;
