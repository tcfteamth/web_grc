import React, { useEffect, useState } from 'react';
import {
  Modal,
  Button,
  Grid,
  Pagination,
  Icon,
  Radio,
} from 'semantic-ui-react';
import styled from 'styled-components';
import {
  ButtonBorder,
  Text,
  DropdownAll,
  RadioBox,
  ButtonAll,
} from '../../element';
import { sizes, colors } from '../../../utils';
import { initAuthStore } from '../../../contexts';
import { useLocalStore } from 'mobx-react-lite';
import { useObserver } from 'mobx-react';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ assessmentDetailModel, disabled }) => {
  const authContext = initAuthStore();
  // const cloneAssessmentModel = useLocalStore(() => new )
  const [isOpen, setIsOpen] = useState(false);
  const [selectedYear, setSelectedYear] = useState();
  const [selectedDivion, setSelectedDivision] = useState();

  const getYear = () => {
    try {
      assessmentDetailModel.cloneAssessmentModel.getYearlist(
        authContext.accessToken,
      );
    } catch (e) {
      console.log(e);
    }
  };

  const getDiviosion = () => {
    try {
      assessmentDetailModel.cloneAssessmentModel.getDivisionList();
    } catch (e) {
      console.log(e);
    }
  };

  const handleClearObservable = () => {
    assessmentDetailModel.cloneAssessmentModel.setField('submit', {});
    assessmentDetailModel.cloneAssessmentModel.setField('selectedYear', {});
    assessmentDetailModel.cloneAssessmentModel.setField('selectedDivision', {});
  };

  const initCloneAssessment = () => {
    assessmentDetailModel.cloneAssessmentModel.getCloneAssessmentList(
      authContext.accessToken,
      assessmentDetailModel.departmentNo,
    );
  };

  useEffect(() => {
    assessmentDetailModel.cloneAssessmentModel.getCloneAssessmentList(
      authContext.accessToken,
      assessmentDetailModel.departmentNo,
    );

    getYear();
  }, []);

  useEffect(() => {
    getDiviosion();
  }, [assessmentDetailModel.cloneAssessmentModel.departmentId]);

  return useObserver(() => (
    <ModalBox
      onClose={() => {
        handleClearObservable();
        setIsOpen(false);
      }}
      onOpen={() => {
        initCloneAssessment();
        setIsOpen(true);
      }}
      onMount={() => handleClearObservable()}
      closeOnEscape
      open={isOpen}
      trigger={
        <ButtonBorder
          width={230}
          height={40}
          fontSize={sizes.s}
          textColor={colors.textSky}
          borderColor={colors.textSky}
          textUpper
          textWeight={'med'}
          text="Duplicate Assessment"
          //  icon={'../../static/images/iconPlus@3x.png'}
          handelOnClick={() => setIsOpen(true)}
          disabled={disabled}
        />
      }
    >
      <Modal.Content>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Duplicate Assessment
        </Text>
        <div>
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Year
                </Text>
                {assessmentDetailModel.cloneAssessmentModel.yearList && (
                  <DropdownAll
                    placeholder="Select Year"
                    options={
                      assessmentDetailModel.cloneAssessmentModel.yearOptions
                    }
                    handleOnChange={(option) => {
                      assessmentDetailModel.cloneAssessmentModel.setField(
                        'selectedYear',
                        option,
                      );
                      assessmentDetailModel.cloneAssessmentModel.getCloneAssessmentList(
                        authContext.accessToken,
                        assessmentDetailModel.departmentNo,
                        1,
                        assessmentDetailModel.cloneAssessmentModel.selectedYear
                          .value,
                        assessmentDetailModel.cloneAssessmentModel
                          .selectedDivision.value,
                      );

                      assessmentDetailModel.cloneAssessmentModel.setField(
                        'submit',
                        {},
                      );
                    }}
                    // value={selectedYear}
                  />
                )}
              </Grid.Column>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                  style={{ paddingBottom: 8 }}
                >
                  Division
                </Text>
                {assessmentDetailModel.cloneAssessmentModel.divisionList && (
                  <DropdownAll
                    placeholder="Select Division"
                    options={
                      assessmentDetailModel.cloneAssessmentModel.divisionOptions
                    }
                    handleOnChange={(option) => {
                      assessmentDetailModel.cloneAssessmentModel.setField(
                        'selectedDivision',
                        option,
                      );

                      assessmentDetailModel.cloneAssessmentModel.getCloneAssessmentList(
                        authContext.accessToken,
                        assessmentDetailModel.departmentNo,
                        1,
                        assessmentDetailModel.cloneAssessmentModel.selectedYear
                          .value,
                        assessmentDetailModel.cloneAssessmentModel
                          .selectedDivision.value,
                      );

                      assessmentDetailModel.cloneAssessmentModel.setField(
                        'submit',
                        {},
                      );
                    }}
                  />
                )}
              </Grid.Column>
            </Grid.Row>
            {assessmentDetailModel.cloneAssessmentModel.prototypeCopyList
              .length !== 0 ? (
              assessmentDetailModel.cloneAssessmentModel.prototypeCopyList.map(
                (assessment) => (
                  <Grid.Row>
                    <div style={{ marginRight: '8px' }}>
                      <RadioBox
                        onChange={() => {
                          assessmentDetailModel.cloneAssessmentModel.setSubmit(
                            assessment.id,
                            assessment.no,
                          );
                        }}
                        checked={
                          assessment.no ===
                          assessmentDetailModel.cloneAssessmentModel.submit.no
                        }
                      />
                    </div>
                    <Text color={colors.primaryBlack} fontSize={sizes.xs}>
                      {`${assessment.no} ${assessment.name}`}
                    </Text>
                  </Grid.Row>
                ),
              )
            ) : (
              <Text>No Assessment</Text>
            )}

            {assessmentDetailModel.cloneAssessmentModel.totalPage > 1 && (
              <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
                <Pagination
                  style={{
                    fontSize: sizes.xs,
                    fontWeight: 'bold',
                    borderColor: colors.backgroundSecondary,
                  }}
                  defaultActivePage={1}
                  onPageChange={(e, { activePage }) => {
                    assessmentDetailModel.cloneAssessmentModel.getCloneAssessmentList(
                      authContext.accessToken,
                      assessmentDetailModel.departmentNo,
                      activePage,
                      assessmentDetailModel.cloneAssessmentModel.selectedYear
                        .value,
                      assessmentDetailModel.cloneAssessmentModel
                        .selectedDivision.value,
                    );
                  }}
                  firstItem={null}
                  lastItem={null}
                  nextItem={{
                    content: <Icon name="angle right" />,
                    icon: true,
                  }}
                  prevItem={{
                    content: <Icon name="angle left" />,
                    icon: true,
                  }}
                  totalPages={
                    assessmentDetailModel.cloneAssessmentModel.totalPage
                  }
                />
              </Grid.Row>
            )}
          </Grid>
        </div>
      </Modal.Content>

      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={() => {
            setIsOpen(false);
            handleClearObservable();
          }}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          onClick={() => {
            assessmentDetailModel.getSelectedCloneAssessment(
              authContext.accessToken,
            );

            setIsOpen(false);
          }}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Duplicate Now"
          disabled={!assessmentDetailModel.cloneAssessmentModel.submit.no}
        />
      </div>
    </ModalBox>
  ));
};

export default index;
