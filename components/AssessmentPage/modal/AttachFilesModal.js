import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import Router from 'next/router';
import Files from 'react-files';
import {
  Text,
  DropdownAll,
  RadioBox,
  InputAll,
  ButtonAll,
  ButtonBorder,
  NewDates,
  TextArea,
} from '../../element';
import { colors, sizes } from '../../../utils';
import request from '../../../services';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({ active, onClose, control, onAttatchFiles }) => {
  const [title, setTitle] = React.useState('');
  const [files, setFiles] = React.useState(null);
  const [errorMessage, setErrorMessage] = React.useState();

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
    onClearState();
  };

  const onModalAttatchFiles = () => {
    onAttatchFiles(title, files);
    onModalClose();
  };

  const onClearState = () => {
    setTitle('');
    setFiles(null);
  };

  return useObserver(() => (
    <ModalBox
      size={'medium'}
      open={active}
      onClose={onModalClose}
      closeOnDimmerClick
      onOpen={onModalOpen}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          {'Attach File'}
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Title
                </Text>
                <Form>
                  {/* <InputAll
                    key="file name"
                    placeholder="Enter file name"
                    value={title}
                    maxLength={1000}
                    handleOnChange={(e) => {
                      setTitle(e.target.value);
                    }}
                  /> */}
                  <TextArea
                    value={title}
                    maxLength={1000}
                    onChange={(e) => {
                      setTitle(e.target.value);
                    }}
                  />
                </Form>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Document / Evidence
                  </Text>
                  <div style={{ paddingLeft: 16 }}>
                    {files ? (
                      <Text fontSize={sizes.xs} color={colors.textlightGray}>
                        File name : {files && files.name}
                      </Text>
                    ) : (
                      <Files
                        style={customStyle.styleAddFile}
                        className="files-dropzone click"
                        onChange={(files) => {
                          setFiles(files[files.length - 1]);
                        }}
                        accepts={[
                          'image/png',
                          '.jpg',
                          '.pdf',
                          '.xlsx',
                          '.docx',
                          '.pptx',
                        ]}
                        maxFiles={1}
                        maxFileSize={5242880}
                        minFileSize={0}
                        clickable
                        onError={(error, file) => {
                          if (error) {
                            setErrorMessage('File Size Maximum 5 MB');
                          } else {
                            setErrorMessage();
                          }
                        }}
                      >
                        Browse File
                      </Files>
                    )}
                  </div>
                  <div style={{ marginLeft: sizes.s }}>
                    <Text>{errorMessage}</Text>
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onModalClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text={'cancel'}
        />
        <ButtonAll
          width={140}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Attach Now"
          disabled={!title}
          onClick={() => onModalAttatchFiles()}
        />
      </div>
    </ModalBox>
  ));
};

export default index;
