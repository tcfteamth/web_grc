import React, { useEffect, useState } from 'react';
import { Grid } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import { Modal, Text, DropdownAll } from '../../element';
import { colors, sizes } from '../../../utils';

const index = ({ active, onClose }) => {
  const handleClose = () => {
    onClose(false);
  };
  useEffect(() => {}, []);

  return useObserver(() => (
    <Modal
      open={active}
      title="Library Control"
      submitTextRight="Submit"
      ContentComponent={() => (
        <div style={{ paddingBottom: 56 }}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Library
                </Text>
                <DropdownAll placeholder="Select Library" />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      )}
      onClose={handleClose}
    />
  ));
};

export default index;
