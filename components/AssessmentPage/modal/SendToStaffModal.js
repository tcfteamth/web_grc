import React from 'react';
import styled from 'styled-components';
import { Modal, Text } from '../../element';
import { colors, sizes } from '../../../utils';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const index = ({ active, onClose, onSubmit }) => (
  <Modal
    open={active}
    title="Send this assessment back to staff."
    submitTextRight="Submit"
    onSubmit={onSubmit}
    onClose={onClose}
    // ContentComponent={() => (
    //   <div>
    //     <Text fontSize={sizes.s} color={colors.textDarkBlack}>
    //       {/* Lorem Ipsum is simply dummy text of the printing and typesetting
    //       industry. Lorem Ipsum has been the industry's standard dummy text ever
    //       since the 1500s when an unknown printer took a galley of type and
    //       scrambled it to make a type specimen book. */}
    //     </Text>
    //   </div>
    // )}
  />
);

export default index;
