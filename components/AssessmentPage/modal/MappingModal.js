import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import {
  Grid,
  Table,
  Modal,
  Loader,
  Dimmer,
  Image,
  Container,
} from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import {
  Text,
  CheckBoxAll,
  RCMappingDropdown,
  ButtonAll,
  ButtonBorder,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { RiskAndControlMappingModel } from '../../../model/AssessmentModel';
import loadingStore from '../../../contexts/LoadingStore';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({
  active,
  onClose,
  lvl4No,
  lvl6No,
  lvl6Name,
  OnSubmit,
  defaultStandardId,
  controlName,
  standardText,
}) => {
  const rcMappingModel = useLocalStore(() => new RiskAndControlMappingModel());
  const handleClose = () => {
    onClose(false);
  };

  const handleSetStanddardFullpath = () => {
    OnSubmit(
      rcMappingModel.selectedSubmitId,
      rcMappingModel.selectedSubmitFullPath,
      rcMappingModel.selectedStandardText,
      rcMappingModel.selectedRiskStandardFullpath,
      rcMappingModel.selectedRiskStandardText,
    );
    onClose();
  };

  useEffect(() => {
    rcMappingModel.getRiskAndControlListMapping(
      1,
      '',
      rcMappingModel.selectedFullpath || lvl6No || lvl4No || '',
      true,
    );
  }, [rcMappingModel.selectedFullpath, lvl4No]);

  useEffect(() => {
    rcMappingModel.setMapping(
      defaultStandardId,
      lvl4No,
      standardText,
      lvl6No,
      lvl6Name,
    );
  }, []);

  return useObserver(() => (
    <ModalBox
      open={active}
      title="Risk & Control Mapping"
      submitTextRight="Submit"
      onClose={handleClose}
      closeOnDimmerClick={false}
    >
      <Grid columns="equal">
        <Grid.Row>
          <Grid.Column>
            <Text
              fontSize={sizes.xs}
              fontWeight="bold"
              color={colors.primaryBlack}
            >
              Sub-Process (Level 4)
            </Text>
            <RCMappingDropdown
              handleOnChange={(e) => {
                rcMappingModel.setField('selectedFullpath', e.value);
                // reset ค่าที่ถูกเลือกไว้
                rcMappingModel.setField('selectedSubmitFullPath', '');
                rcMappingModel.setField('selectedSubmitId', '');
                rcMappingModel.setField('selectedStandardText', '');
                rcMappingModel.setField('selectedRiskStandardFullpath', '');
                rcMappingModel.setField('selectedRiskStandardText', '');
              }}
              value={rcMappingModel.selectedFullpath}
              defaultValue={lvl6No || lvl4No}
              disabled={lvl6No || lvl4No}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row centered>
          <Container text>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Text
                fontSize={sizes.xs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                {controlName}
              </Text>
              <Image
                height={18}
                width={18}
                style={{ margin: '16px 0' }}
                src={
                  rcMappingModel.selectedSubmitId
                    ? '/static/images/zing@3x.png'
                    : '/static/images/zinggray@3x.png'
                }
              />
              {rcMappingModel.selectedStandardText && (
                <div style={{ display: 'flex' }}>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    {rcMappingModel.selectedStandardText}
                  </Text>
                  <Image
                    className="click"
                    height={18}
                    width={18}
                    src="/static/images/close@3x.png"
                    onClick={() => rcMappingModel.removeMapping()}
                  />
                </div>
              )}
            </div>
          </Container>
        </Grid.Row>

        <Grid.Row style={{ padding: 16 }}>
          {loadingStore.isLoading ? (
            <Dimmer active>
              <Loader />
            </Dimmer>
          ) : (
            <Table celled structured>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="med"
                      color={colors.textlightGray}
                    >
                      Sub-Process
                    </Text>
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="med"
                      color={colors.textlightGray}
                    >
                      Object
                    </Text>
                  </Table.HeaderCell>

                  <Table.HeaderCell>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="med"
                      color={colors.textlightGray}
                    >
                      Risk
                    </Text>
                  </Table.HeaderCell>

                  <Table.HeaderCell />

                  <Table.HeaderCell>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="med"
                      color={colors.textlightGray}
                    >
                      Control
                    </Text>
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                {rcMappingModel.contents?.length > 0 &&
                  rcMappingModel.contents.map((lv4, lv4Index) =>
                    lv4.children.map((lv5, lv5Index) =>
                      lv5.children.map((lv6, lv6Index) =>
                        lv6.children.map((lv7, lv7Index) => (
                          <Table.Row>
                            {lv7Index === 0 &&
                              lv6Index === 0 &&
                              lv5Index === 0 && (
                                <Table.Cell rowSpan={lv4.rowSpanLv4}>
                                  <Text
                                    fontSize={sizes.xxs}
                                    color={colors.primaryBlack}
                                  >
                                    {lv4.no} {lv4.name}
                                  </Text>
                                </Table.Cell>
                              )}

                            {lv7Index === 0 && lv6Index === 0 && (
                              <Table.Cell rowSpan={lv5.rowSpanLv5}>
                                <Text
                                  fontSize={sizes.xxs}
                                  color={colors.primaryBlack}
                                >
                                  {lv5.no} {lv5.name}
                                </Text>
                              </Table.Cell>
                            )}

                            {lv7Index === 0 && (
                              <Table.Cell rowSpan={lv6.children.length}>
                                <Text
                                  fontSize={sizes.xxs}
                                  color={colors.primaryBlack}
                                >
                                  {lv6.no} {lv6.name}
                                </Text>
                              </Table.Cell>
                            )}

                            <Table.Cell textAlign="center">
                              <CheckBoxAll
                                checked={
                                  rcMappingModel.selectedSubmitId === lv7.id
                                }
                                onChange={() => {
                                  rcMappingModel.setMapping(
                                    lv7.id,
                                    lv4.no,
                                    `${lv7.no} ${lv7.name}`,
                                    lv6.fullPath,
                                    `${lv6.no} ${lv6.name}`,
                                  );
                                }}
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Text
                                fontSize={sizes.xxs}
                                color={colors.primaryBlack}
                              >
                                {lv7.no} {lv7.name}
                              </Text>
                            </Table.Cell>
                          </Table.Row>
                        )),
                      ),
                    ),
                  )}
              </Table.Body>
            </Table>
          )}
        </Grid.Row>
        <Grid.Row centered>
          <ButtonBorder text="Cancel" handelOnClick={handleClose} />
          <ButtonAll
            text="Confirm"
            onClick={handleSetStanddardFullpath}
            // disabled={!rcMappingModel.isSelectedMapping}
          />
        </Grid.Row>
      </Grid>
    </ModalBox>
  ));
};

export default index;
