import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import { Modal, Text, DropdownAll, TextArea, InputAll } from '../../element';
import { initRoadmapContext } from '../../../contexts/roadmap';
import { colors, sizes } from '../../../utils';
import request from '../../../services';

const InputDefault = styled(InputAll)``;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const initYear = () => {
  const getYear = new Date().getFullYear();
  const years = Array.from(new Array(10), (val, index) => getYear + index);
  const yearList = years.map((item) => ({
    value: new Date(`${item}`),
    label: item,
  }));

  return yearList;
};

const index = ({ active, onClose, onSubmit, closeOnDimmerClick }) => {
  const [selectedLvlFour, setSelectedLvlFour] = useState();

  const handleClose = () => {
    onClose(false);
  };
  useEffect(() => {}, []);

  return useObserver(() => (
    <Modal
      open={active}
      title="Save Change"
      submitTextRight="Save"
      // isSubmitDisabled={isSubmitReady()}
      onSubmit={onSubmit}
      closeOnDimmerClick={closeOnDimmerClick}
      ContentComponent={() => (
        <div style={{ paddingBottom: 56 }}>
          <Text fontSize={sizes.s} color={colors.textDarkBlack}>
            Your changes have not been saved. You can save your change, click
          </Text>
        </div>
      )}
      onClose={handleClose}
    />
  ));
};

export default index;
