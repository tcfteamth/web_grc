import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import { Modal, Text, TextArea, InputAll, InputDropdown } from '../../element';
import { colors, sizes } from '../../../utils';

const InputDefault = styled(InputAll)``;

export const OBJECT_TYPES = [
  {
    key: 'Operations',
    text: 'ด้านการดำเนินงาน (Operations)',
    value: 'ด้านการดำเนินงาน (Operations)',
  },
  {
    key: 'Compliance',
    text: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
    value: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
  },
  {
    key: 'Reporting',
    text: 'ด้านการรายงาน (Reporting)',
    value: 'ด้านการรายงาน (Reporting)',
  },
];

const index = ({ active, onClose, onSubmit, assessmentRCModel }) => {
  const handleClose = () => {
    onClose(false);
  };
  useEffect(() => {}, []);

  return useObserver(() => (
    <Modal
      open={active}
      title="Add Objective"
      submitTextRight="Create Now"
      // isSubmitDisabled={isSubmitReady()}
      onSubmit={onSubmit}
      ContentComponent={() => (
        <div style={{ paddingBottom: 56 }}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  Objective{' '}
                  <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span>
                </Text>
                <TextArea
                  height={45}
                  placeholder="กรอกวัตถุประสงค์"
                  onChange={(e) => {
                    assessmentRCModel.setNewNameThObject(e.target.value);
                  }}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.primaryBlack}
                >
                  ประเภทของวัตุประสงค์{' '}
                  <span style={{ fontSize: sizes.m, color: colors.red }}>
                    *
                  </span>
                </Text>
                <InputDropdown
                  placeholder="Select "
                  options={OBJECT_TYPES}
                  handleOnChange={(e, { value }) =>
                    assessmentRCModel.setNewObjectTypes(value)
                  }
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      )}
      onClose={handleClose}
    />
  ));
};

export default index;
