import React from 'react';
import { Modal } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, ButtonBorder, ButtonAll, Box } from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore } from '../../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const Oval = styled.div`
  width: 8px;
  height: 8px;
  background-color: #0057b8;
  left: 0px;
  border-radius: 10px;
  margin: 14px 14px 14px 0;
`;

const index = ({ active, onClose, onSubmit }) => {
  const authContext = initAuthStore();
  return (
    <ModalBox
      size="medium"
      open={active}
      onClose={onClose}
      closeOnDimmerClick={onClose}
      // onSubmit={onSubmit}
    >
      <Modal.Content style={{ padding: 0 }}>
        <div>
          <Box horizontal="row" alignCenter>
            <Oval />
            <Text fontSize={sizes.s} color={colors.textDarkBlack}>
              Please ensure that Risks and Controls are assessed as of the
              present process
            </Text>
          </Box>
          {/* <Box horizontal="row" alignCenter>
            <Oval />
            <Text fontSize={sizes.s} color={colors.textDarkBlack}>
              Please ensure that you have completed all required information and
              initiatives (if any)
            </Text>
          </Box> */}

          <Box horizontal="row" alignCenter>
            <Oval />
            <Text fontSize={sizes.s} color={colors.textDarkBlack}>
              Please ensure that this assessment is incorporated the
              recommendation from internal / external audit (If any)
            </Text>
          </Box>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingTop: 92,
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textWeight="med"
          textUpper
          text="CANCEL"
        />
        <ButtonAll
          width={140}
          color={colors.primary}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Agree and Submit"
          onClick={onSubmit}
        />
        {!authContext.roles.isDM &&
          !authContext.roles.isVP &&
          !authContext.roles.isAdmin && (
            <Box horizontal="row">
              <Text
                style={{ paddingLeft: 4 }}
                fontSize={sizes.xxs}
                color={colors.textlightGray}
              >
                *
              </Text>
              <Text
                style={{ width: 180, paddingLeft: 4 }}
                fontSize={sizes.xxs}
                color={colors.textlightGray}
              >
                The automatic email shall be sent to DM for reviewing
              </Text>
            </Box>
          )}

        {authContext.roles.isDM && (
          <Box horizontal="row">
            <Text
              style={{ paddingLeft: 4 }}
              fontSize={sizes.xxs}
              color={colors.textlightGray}
            >
              *
            </Text>
            <Text
              style={{ width: 180, paddingLeft: 4 }}
              fontSize={sizes.xxs}
              color={colors.textlightGray}
            >
              The automatic email shall be sent to VP for approving
            </Text>
          </Box>
        )}
      </div>
    </ModalBox>
  );
};

export default index;
