import React from 'react';
import { Modal } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, ButtonBorder, ButtonAll, Box } from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore } from '../../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const Oval = styled.div`
  width: 8px;
  height: 8px;
  background-color: #0057b8;
  left: 0px;
  border-radius: 10px;
  margin: 14px 14px 14px 0;
`;

const index = ({ active, onClose, onSubmit, id, status }) => {
  return (
    <ModalBox
      size="medium"
      open={active}
      onClose={onClose}
      closeOnDimmerClick={onClose}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Add Request
        </Text>
      </div>
      <Modal.Content style={{ padding: 0 }}>
        <div>
          <Box horizontal="row" alignCenter>
            <Oval />
            {status ? (
              <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                Please make sure you cancel reading the request.
                {/* โปรดตรวจสอบว่าคุณจะยกเลิกการอ่านคำขอ */}
              </Text>
            ) : (
              <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                Please make sure you have read the request.
                {/* โปรดตรวจสอบว่าคุณได้อ่านคำขอแล้ว */}
              </Text>
            )}
          </Box>
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingTop: 92,
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textWeight="med"
          textUpper
          text="NO"
        />
        <ButtonAll
          width={140}
          color={colors.primary}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="YES"
          onClick={onSubmit}
        />
      </div>
    </ModalBox>
  );
};

export default index;
