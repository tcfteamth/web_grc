import React, { useState } from 'react';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import {
  Text,
  RoadmapNextYear,
  ProgressBarStatusTran,
  Cell,  Remark
} from '../../element';
import { colors, sizes } from '../../../utils';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowAssessmentToDoList = ({ assessment, tab, role }) => {
  const [isOpen, setIsOpen] = useState(true);
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell
          width={screenWidth < 1024 ? 980 : 98}
          className="click"
          onClick={() => setIsOpen(!isOpen)}
        >
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            color={colors.primaryBlack}
          >
            {assessment.no} {assessment.name}
          </Text>
        </Cell>
        <Cell center width={screenWidth < 1024 ? 20 : 2}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>
      {assessment.children.map((i, index) => (
        <Div key={index}>
          {isOpen && (
            <Div col>
              <Div style={{ padding: '16px 0px 0px' }}>
                <Cell width={screenWidth < 1024 ? 450 : 45} className="click">
                  <a
                    href={`/Assessment/assessmentDetail?id=${i.id}&assessmentId=${i.no}&tab=${tab}`}
                    rel="noopener noreferrer"
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {i.no} {i.name}
                    </Text>
                  </a>
                </Cell>
                <Cell width={screenWidth < 1024 ? 70 : 7} center>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    trigger={
                      i.roadMapImage ? (
                        <Image
                          src={i.roadMapImage}
                          style={{ width: 18, height: 18 }}
                        />
                      ) : (
                        <RoadmapNextYear year={i.year || i.roadMapType} />
                      )
                    }
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {i.roadMapType}
                    </Text>
                  </Popup>
                </Cell>
                <Cell width={screenWidth < 1024 ? 130 : 13} center mid>
                  <Popup
                    trigger={
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        {i.depDiv}
                      </Text>
                    }
                    content={`${i.divEn || '-'} ${i.shiftEn || ''}`}
                    size="small"
                  />
                </Cell>
                <Cell
                  width={screenWidth < 1024 ? 150 : 15}
                  style={{ padding: '8px 32px 0px 16px' }}
                >
                  <ProgressBarStatusTran data={i.summaryStatus} />
                </Cell>
                <Cell width={screenWidth < 1024 ? 130 : 13} center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {i.owner}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 70 : 7} center>
                <Remark remark={i.remark} />
                </Cell>
                <Cell center width={screenWidth < 1024 ? 100 : 10}>
                <Text
                   style={{ marginRight: 30}}
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
            {i.latestYear}
               </Text>
               </Cell>
                {/* <Cell width={screenWidth < 1024 ? 70 : 7} center>
                  <a
                    href={`/Assessment/assessmentDetail?id=${i.id}&assessmentId=${i.no}&tab=${tab}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Image
                      src="../../static/images/file-purple@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  </a>
                </Cell> */}
              </Div>
            </Div>
          )}
        </Div>
      ))}
    </Div>
  ));
};

export default RowAssessmentToDoList;
