import React, { useState } from 'react';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { useObserver } from 'mobx-react';
import {
  CheckBoxAll,
  Text,
  StatusAll,
  RoadmapNextYear,
  Cell,
  Remark
} from '../../element';
import { colors, sizes } from '../../../utils';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowAssessmentToDoList = ({ assessment, role }) => {
  const [isOpen, setIsOpen] = useState(true);
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell center width={screenWidth < 1024 ? 30 : 3}>
          <CheckBoxAll
            header
            status="Accept"
            checked={assessment.isSubmitAssessmentCheckedAll(role)}
            onChange={(e, data) =>
              assessment.setCheckAllSubmitAssessment(role, data.checked)
            }
          />
        </Cell>
        <Cell
          width={screenWidth < 1024 ? 940 : 94}
          className="click"
          onClick={() => setIsOpen(!isOpen)}
        >
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            style={{ paddingLeft: 16 }}
            color={colors.primaryBlack}
          >
            {assessment.no} {assessment.name}
          </Text>
        </Cell>
        <Cell center width={screenWidth < 1024 ? 30 : 3}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>
      {assessment.children.map((child) => (
        <Div>
          {isOpen && (
            <Div col>
              <Div style={{ padding: '16px 0px 0px' }}>
                <Cell center width={screenWidth < 1024 ? 30 : 3}>
                  <CheckBoxAll
                    status="Accept"
                    disabled={!child.readyToSubmit}
                    checked={child.submitAssessment.isSelected}
                    onChange={() => child.onSelectedSubmitAssessment()}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 380 : 38} className="click">
                  <a
                    href={`/Assessment/assessmentDetail?id=${child.id}&assessmentId=${child.no}`}
                  >
                    <Text
                      style={{ paddingLeft: 16 }}
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
                      {`${child.no} ${child.name}`}
                    </Text>
                  </a>
                </Cell>
                <Cell center width={screenWidth < 1024 ? 100 : 10}>
                <Text
                      style={{ paddingLeft: 16 }}
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
            {child.latestYear}
               </Text>
               </Cell>
                <Cell width={screenWidth < 1024 ? 70 : 7} center>
                  {child.readyToSubmit ? (
                    <Image
                      src="../../static/images/tick-active@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  ) : (
                    <Image
                      src="../../static/images/tick@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  )}
                </Cell>
                <Cell width={screenWidth < 1024 ? 70 : 7} center>
                  <Remark remark={child.remark} />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center mid>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    trigger={
                      child.roadMapImage ? (
                        <Image
                          src={child.roadMapImage}
                          style={{ width: 18, height: 18 }}
                        />
                      ) : (
                        <RoadmapNextYear
                          year={child.year || child.roadMapType}
                        />
                      )
                    }
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {child.roadMapType}
                    </Text>
                  </Popup>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {moment(child.lastUpdate).locale('th').format('L')}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 150 : 15} center>
                  <StatusAll child={child} />
                </Cell>
              </Div>
            </Div>
          )}
        </Div>
      ))}
    </Div>
  ));
};

export default RowAssessmentToDoList;
