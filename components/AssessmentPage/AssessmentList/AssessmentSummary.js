import React, { useState, useEffect } from 'react';
import { Pagination, Icon, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text, Cell, TabScrollBar } from '../../element';
import { colors, sizes } from '../../../utils';
import { headersAssessmentSummaryDM } from '../../../utils/static';
import useWindowDimensions from '../../../hook/useWindowDimensions';
import { RowAssessmentSummary } from '../';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentSummary = ({ dataSummary, tab, onChangePage }) => {
  const [dataList, setDataList] = useState();
  const [totalPages, setTotalPages] = useState();
  const { width: screenWidth } = useWindowDimensions();

  useEffect(() => {
    setDataList(dataSummary && dataSummary.assessments);
    setTotalPages(dataSummary && dataSummary.totalPage);
    console.log('dataSummary', dataSummary);
  }, [dataSummary]);

  return (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersAssessmentSummaryDM.map(
              ({ render, key, width, widthMobile }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center
                > 
                  <Div
                    center
                    mid={
                      key === 'Type' ||
                      key === 'Division' ||
                      key === 'View' ||
                      key === 'wait'
                    }
                  >
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="med"
                      color={colors.textlightGray}
                    >
                      {render}
                    </Text>  
                  </Div>
                 </Cell> 
              ),
            )}
          </TableHeader>
          {dataList &&
            dataList.map((item, index) => (
              <TableBody>
                <RowAssessmentSummary assessment={item} tab={tab} />
              </TableBody>
            ))} 
        </Div>
      </TabScrollBar>

      {totalPages && totalPages > 1 && (
        <Grid>
          <Grid.Row
            style={{
              justifyContent: 'flex-end',
              marginRight: 13,
              marginTop: 12,
            }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={1}
              onPageChange={(e, { activePage }) => {
                console.log('page', activePage);
                onChangePage(activePage);
              }}
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={totalPages}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  );
};

export default AssessmentSummary;
