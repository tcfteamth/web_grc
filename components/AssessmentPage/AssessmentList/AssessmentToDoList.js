import React from 'react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import {
  CheckBoxAll,
  Text,
  BottomBarAssessment,
  Cell,
  TabScrollBar,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { headersAssessmentStaff } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';
import { RowAssessmentToDoList } from '../';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentToDoList = ({ assessmentData, role }) => {
  const { width: screenWidth } = useWindowDimensions();
  const authContext = initAuthStore();

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader span={12}>
            {headersAssessmentStaff.map(
              ({ render, key, width, widthMobile, center }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={center}
                >
                  {render === 'Submit' ? (
                    <Div col center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="med"
                        color={colors.primary}
                      >
                        {render}
                      </Text>
                      <CheckBoxAll
                        header
                        status={render}
                        checked={assessmentData.isSubmitAssessmentCheckedAll()}
                        onChange={(e, data) =>
                          assessmentData.setCheckAllSubmitAssessment()
                        }
                      />
                    </Div>
                  ) : (
                    <Div center mid={key !== 'Assessment'}>
                      <Text
                        center
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                        style={{ paddingLeft: key === 'Assessment' && 16 }}
                      >
                        {render}
                      </Text>
                    </Div>
                  )}
                </Cell>
              ),
            )}
          </TableHeader>
          {assessmentData.assessmentList.map((assessment, index) => (
            <TableBody>
              <RowAssessmentToDoList assessment={assessment} />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      <BottomBarAssessment
        page="AssessmentTodoList"
        submitToDmData={() =>
          assessmentData.sendAssessmentToDmOrVp(authContext.accessToken)
        }
        assessmentData={assessmentData}
      />
    </div>
  ));
};

export default AssessmentToDoList;
