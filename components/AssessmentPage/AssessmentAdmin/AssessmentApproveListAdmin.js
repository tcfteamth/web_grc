import React from 'react';
import { useObserver } from 'mobx-react-lite';
import { Pagination, Grid, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import {
  CheckBoxAll,
  Text,
  BottomBarAssessment,
  TabScrollBar,
  Cell,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { headersApproveAdmin } from '../../../utils/static';
import { initAuthStore } from '../../../contexts';
import { RowAssessmentApproveListAdmin } from '..';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const AssessmentApproveList = ({ assessmentData }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersApproveAdmin.map(
              ({ render, key, width, mid, widthMobile }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={mid}
                >
                  {render === 'Reject' || render === 'Accept' ? (
                    <Div col center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="med"
                        color={
                          render === 'Reject' ? colors.red : colors.primary
                        }
                      >
                        {render}
                      </Text>

                      {render === 'Accept' && (
                        <CheckBoxAll
                          header
                          status={render}
                          checked={assessmentData.isAllAssessmentChecked}
                          onChange={(e) => {
                            // assessmentData.setCheckAcceptAll();
                            assessmentData.setCheckAllAssessment();
                          }}
                          disabled={assessmentData.isAllReadyToSend}
                        />
                      )}
                    </Div>
                  ) : (
                    <Div center mid={mid}>
                      <Text
                        center
                        fontSize={sizes.xs}
                        style={{
                          paddingLeft: render === 'Assessment Name' && 16,
                        }}
                        fontWeight="med"
                        color={
                          render === 'Enhancement'
                            ? colors.primary
                            : colors.textlightGray
                        }
                      >
                        {render}
                      </Text>
                    </Div>
                  )}
                </Cell>
              ),
            )}
            <Cell width={screenWidth < 1024 ? 180 : 18} center>
              <Div col center>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.textGray}
                  style={{ textAlign: 'center' }}
                >
                  Control by Rating
                </Text>
                <Div between>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.green}
                  >
                    Good
                  </Text>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.yellow}
                  >
                    Fair
                  </Text>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.red}
                  >
                    Poor
                  </Text>
                </Div>
              </Div>
            </Cell>
          </TableHeader>
          {assessmentData.assessmentList.map((assessment, index) => (
            <TableBody bottom={4} pad={10}>
              <RowAssessmentApproveListAdmin assessment={assessment} />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {assessmentData.totalPage > 1 && (
        <Grid>
          <Grid.Row
            style={{
              justifyContent: 'flex-end',
              marginRight: 13,
              marginTop: 14,
            }}
          >
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getAssessmentList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl3,
                assessmentData.filterModel.selectedLvl4,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedRoadmapType,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift,
                assessmentData.filterModel.selectedStatus,
                assessmentData.filterModel.selectedRate,
                assessmentData.filterModel.selectedYear,
                assessmentData.filterModel.selectedProcessAnd,
                assessmentData.filterModel.selectedProcessOr,
                assessmentData.filterModel.selectedObjectives)
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      <BottomBarAssessment
        assessmentData={assessmentData}
        page="AssessmentApproveListAdmin"
      />
    </div>
  ));
};

export default AssessmentApproveList;
