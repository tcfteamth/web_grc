import React, { useState } from 'react';
import { toJS } from 'mobx';
import { useObserver } from 'mobx-react-lite';
import { Image, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';
import { CheckBoxAll, Text, InputAll, StatusAll, Cell } from '../../element';
import { colors, sizes } from '../../../utils';
import { CountRate } from '../container';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  font-size: ${(props) => props.fontSize || 0}px;
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 3px 0px;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  max-width: 100px;
  min-width: 60px;
  width: 100%;
  height: 24px !important;
  text-align: center;
  margin-left: ${(props) => props.left || 0}px;
  background-color: ${(props) =>
    props.Good
      ? colors.green
      : props.Fair
      ? colors.yellow
      : props.Poor
      ? colors.red
      : '#FFFFFF'};
`;

const RowAssessmentApproveListAdmin = ({ assessment }) => {
  const [isOpen, setIsOpen] = useState(true);
  const { width: screenWidth } = useWindowDimensions();

  const handelOpen = (value) => {
    setIsOpen(value);
  };

  const renderStatus = (child) => {
    let status = [];
    if (typeof child === 'string') {
      status = child.split(' ');
    }

    return (
      <div>
        {status && status[2] === 'Good' && (
          <StatusTag Good>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              Good
            </Text>
          </StatusTag>
        )}
        {status[2] === 'Fair' && (
          <StatusTag Fair>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              Fair
            </Text>
          </StatusTag>
        )}
        {status[2] === 'Poor' && (
          <StatusTag Poor>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              Poor
            </Text>
          </StatusTag>
        )}
        {typeof child === 'object' && (
          <Text
            color={colors.textlightGray}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            -
          </Text>
        )}
      </div>
    );
  };

  return useObserver(() => (
    <Div col>
      <Div>
        <Cell width={screenWidth < 1024 ? 40 : 4} center>
          {/* <CheckBoxAll
            header
            status="AcceptHeader"
            // onChange={() => assessment.setAcceptAllLevel4()}
            // checked={assessment.isAllLevel4Accept}
            onChange={() => assessment.setCheckAllAssessment2()}
            checked={assessment.isAllAssessmentChecked}
            disabled={}
          /> */}
          <CheckBoxAll
            header
            status="AcceptHeader"
            onChange={() => assessment.setCheckAllAssessment2()}
            checked={assessment.isCheckAllReadyToSubmit}
            disabled={!assessment.isOneOfAssessmentCanCheck}
          />
        </Cell>
        <Cell
          width={screenWidth < 1024 ? 1140 : 114}
          className="click"
          left
          onClick={() => setIsOpen(!isOpen)}
        >
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            style={{ paddingLeft: 16 }}
            color={colors.primaryBlack}
          >
            {assessment.no} {assessment.name}
          </Text>
        </Cell>
        <Cell width={screenWidth < 1024 ? 20 : 2} center>
          <div className="click" onClick={() => handelOpen(!isOpen)}>
            {isOpen ? (
              <Image width={18} src="/static/images/iconUp-blue@3x.png" />
            ) : (
              <Image width={18} src="/static/images/iconDown-blue@3x.png" />
            )}
          </div>
        </Cell>
      </Div>
      {isOpen && (
        <div style={{ width: '100%' }}>
          {assessment.children.map((child, index) => (
            <div style={{ width: '100%' }}>
              <TableBody span={12} key={index}>
                <Cell width={screenWidth < 1024 ? 40 : 4} center>
                  {/* <CheckBoxAll
                    status="Accept"
                    onChange={() => {
                      child.submitAssessment.setRoadmapIds(child.id);
                    }}
                    checked={child.submitAssessment.isSelected}
                  /> */}
                  <CheckBoxAll
                    status="Accept"
                    disabled={!child.readyToSubmit}
                    onChange={() =>
                      child.submitAssessment.setRoadmapIds(child.id)
                    }
                    checked={
                      child.readyToSubmit && child.submitAssessment.isSelected
                    }
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 280 : 28}>
                  <a
                    href={`/Assessment/assessmentDetail?id=${child.id}&assessmentId=${child.no}`}
                  >
                    <Text
                      className="click"
                      fontSize={sizes.xs}
                      style={{ paddingLeft: 16 }}
                      color={colors.primaryBlack}
                    >
                      {`${child.no} ${child.name}`}
                    </Text>
                  </a>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Image
                    src={
                      child.verifyCompliance
                        ? '../../static/images/tick-active@3x.png'
                        : '../../static/images/tick@3x.png'
                    }
                    style={{ width: 18, height: 18 }}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Popup
                    trigger={
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {child.objectives.map(obj => {
                          const matchResult = obj.match(/\(([^)]+)\)/);
                          return (obj.split(" ").length >= 2 ? matchResult[matchResult.length-1] : ( obj ? obj : null));
                          }).filter(obj => obj !== null).map(obj => obj[0]).join(',') || '-'}
                      </Text>
                    }
                    content={child.objectives.join(', ')}
                    size="small"
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 120 : 12} center>
                  <StatusAll child={child} />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Image
                    src={
                      child.readyToSubmit
                        ? '../../static/images/tick-active@3x.png'
                        : '../../static/images/tick@3x.png'
                    }
                    style={{ width: 18, height: 18 }}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Popup
                    trigger={
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {child.depDiv}
                      </Text>
                    }
                    content={`${child.divEn || '-'} ${child.shiftEn || ''}`}
                    size="small"
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 80 : 8} center>
                  {renderStatus(child.overAllRating)}
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  {child.enhance <= 0 ? (
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      -
                    </Text>
                  ) : (
                    <Text fontSize={sizes.xs} color={colors.primary}>
                      {child.enhance}
                    </Text>
                  )}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {toJS(controlRate.nameEn) === 'Good' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {toJS(controlRate.nameEn) === 'Fair' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {toJS(controlRate.nameEn) === 'Poor' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
              </TableBody>

              {child.acceptAssessment.isSelectedReject() && (
                <TableBody
                  span={12}
                  key={index}
                  style={{ paddingLeft: 100, paddingRight: 16 }}
                >
                  <InputAll
                    placeholder="กรุณาระบุเหตุผล"
                    handleOnChange={(e) => {
                      child.acceptAssessment.setRejectComment(e.target.value);
                    }}
                    value={child.acceptAssessment.rejectComment}
                  />
                </TableBody>
              )}
            </div>
          ))}
        </div>
      )}
    </Div>
  ));
};

export default RowAssessmentApproveListAdmin;
