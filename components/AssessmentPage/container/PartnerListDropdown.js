import React, { useState, useEffect } from 'react';
import { useLocalStore, useObserver } from 'mobx-react';
import _ from 'lodash';
import { DropdownSelect } from '../../element';
import { partnerlistDropdownStore } from '../../../model';
import { initAuthStore } from '../../../contexts';

const index = ({
  value,
  handleOnChange,
  disabled,
  defaultValue,
  placeholder,
}) => {
  const partnerlistDropdown = useLocalStore(() => partnerlistDropdownStore);
  const authContext = initAuthStore();
  const [listAssesor, setListAssesor] = useState();

  useEffect(() => {
    partnerlistDropdown.getPartners(authContext.accessToken);
  }, []);

  const handleInputChange = _.debounce((text) => {
    if (text) {
      partnerlistDropdown.getPartners(authContext.accessToken, text);
    }
  }, 1000);

  return useObserver(() => (
    <>
      <DropdownSelect
        closeMenuOnSelect
        defaultValue={defaultValue}
        options={partnerlistDropdown.options}
        value={value}
        handleOnChange={handleOnChange}
        isDisabled={disabled}
        placeholder={placeholder}
        onInputChange={handleInputChange}
      />
    </>
  ));
};

export default index;
