import React from 'react';
import styled from 'styled-components';
import { Text } from '../../element';
import { colors, sizes } from '../../../utils';

const CountStyled = styled(Text)`
  font-size: ${sizes.xs}px;
  color: ${(props) =>
    props.status === 'Good'
      ? colors.green
      : props.status === 'Fair'
      ? colors.yellow
      : props.status === 'Poor'
      ? colors.red
      : colors.textlightGray};
  font-weight: 600;
`;

const index = ({ count, rates, status }) => (
  <CountStyled status={count && status}>{count || '-'}</CountStyled>
);

export default index;
