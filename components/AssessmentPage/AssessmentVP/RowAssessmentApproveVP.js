import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Image , Popup} from 'semantic-ui-react';
import Link from 'next/link';
import moment from 'moment';
import { useObserver } from 'mobx-react';
import { CheckBoxAll, Text, InputAll, Cell, Remark } from '../../element';
import { CountRate } from '../container';
import { colors, sizes } from '../../../utils';
import { initDataContext, initAuthStore } from '../../../contexts';
import { ACCEPT_ASSESSMENT_STATUS } from '../../../model/AssessmentModel/AcceptAssessmentModel';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  font-size: ${(props) => props.FontSize || 0}px;
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin: 3px 0px;
`;

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px;
  min-width: 100px;
  width: 100%;
  height: 24px !important;
  text-align: center;
  margin-left: ${(props) => props.left || 0}px;
  background-color: ${(props) =>
    props.Good
      ? colors.green
      : props.Fair
      ? colors.yellow
      : props.Poor
      ? colors.red
      : '#FFFFFF'};
`;

const RowAssessmentApproveVP = ({ assessment, role, index }) => {
  const [isOpen, setIsOpen] = useState(true);
  const { width: screenWidth } = useWindowDimensions();

  const renderStatus = (child) => {
    let status = [];
    if (typeof child === 'string') {
      status = child.split(' ');
    }

    return (
      <div>
        {status && status[2] === 'Good' && (
          <StatusTag Good>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              {'Good'}
            </Text>
          </StatusTag>
        )}
        {status[2] === 'Fair' && (
          <StatusTag Fair>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              {'Fair'}
            </Text>
          </StatusTag>
        )}
        {status[2] === 'Poor' && (
          <StatusTag Poor>
            <Text
              color={colors.backgroundPrimary}
              fontWeight="bold"
              fontSize={sizes.xxs}
            >
              {'Poor'}
            </Text>
          </StatusTag>
        )}
        {typeof child === 'object' && (
          <Text
            color={colors.textlightGray}
            fontWeight="bold"
            fontSize={sizes.xxs}
          >
            -
          </Text>
        )}
      </div>
    );
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TableBody span={12} key={index}>
        <Cell width={screenWidth < 1024 ? 40 : 4} center>
          <CheckBoxAll
            header
            status="AcceptHeader"
            checked={assessment.isAcceptAssessmentCheckedAll(
              role,
              ACCEPT_ASSESSMENT_STATUS.accept,
            )}
            onChange={(e, data) =>
              assessment.setCheckAllAcceptAssessment(
                role,
                ACCEPT_ASSESSMENT_STATUS.accept,
                data.checked,
              )
            }
          />
          {/* <CheckBoxAll status="AcceptHeader" disabled /> */}
        </Cell>
        <Cell width={screenWidth < 1024 ? 40 : 4} center>
          <CheckBoxAll
            header
            status="Reject"
            checked={assessment.isAcceptAssessmentCheckedAll(
              role,
              ACCEPT_ASSESSMENT_STATUS.reject,
            )}
            onChange={(e, data) =>
              assessment.setCheckAllAcceptAssessment(
                role,
                ACCEPT_ASSESSMENT_STATUS.reject,
                data.checked,
              )
            }
          />
          {/* <CheckBoxAll status="Reject" disabled /> */}
        </Cell>
        <Cell
          width={screenWidth < 1024 ? 940 : 94}
          left
          className="click"
          onClick={() => setIsOpen(!isOpen)}
        >
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            style={{ paddingLeft: 16 }}
            color={colors.primaryBlack}
          >
            {assessment.no} {assessment.name}
          </Text>
        </Cell>
        <Cell width={screenWidth < 1024 ? 30 : 3} center>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image width={18} src="/static/images/iconUp-blue@3x.png" />
            ) : (
              <Image width={18} src="/static/images/iconDown-blue@3x.png" />
            )}
          </div>
        </Cell>
      </TableBody>
      {isOpen && (
        <div style={{ width: '100%' }}>
          {assessment.children.map((child, index) => (
            <div>
              <TableBody span={12} key={index}>
                <Cell center width={screenWidth < 1024 ? 40 : 4}>
                  <CheckBoxAll
                    status="Accept"
                    onChange={() =>
                      child.acceptAssessment.setNo(
                        ACCEPT_ASSESSMENT_STATUS.accept,
                        child.id,
                      )
                    }
                    checked={child.acceptAssessment.isSelectedAccept()}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 40 : 4} center>
                  <CheckBoxAll
                    status="Reject"
                    onChange={() =>
                      child.acceptAssessment.setNo(
                        ACCEPT_ASSESSMENT_STATUS.reject,
                        child.id,
                      )
                    }
                    checked={child.acceptAssessment.isSelectedReject()}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 370 : 37}>
                  <a
                    href={`/Assessment/assessmentDetail?id=${child.id}&assessmentId=${child.no}`}
                  >
                    <Text
                      className="click"
                      fontSize={sizes.xs}
                      style={{ paddingLeft: 16 }}
                      color={colors.primaryBlack}
                    >
                      {`${child.no} ${child.name}`}
                    </Text>
                  </a>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Popup
                    trigger={
                      <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                        {child.depDiv}
                      </Text>
                    }
                    content={`${child.divEn || '-'} ${child.shiftEn || ''}`}
                    size="small"
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Remark remark={child.remark} />
                </Cell>
                <Cell width={screenWidth < 1024 ? 80 : 8} center>
                  {renderStatus(child.overAllRating)}
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  {child.enhance <= 0 ? (
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      -
                    </Text>
                  ) : (
                    <Text fontSize={sizes.xs} color={colors.primary}>
                      {child.enhance}
                    </Text>
                  )}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {controlRate.nameEn === 'Good' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {controlRate.nameEn === 'Fair' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
                <Cell width={screenWidth < 1024 ? 60 : 6} center>
                  {child.controlByRating.map((controlRate) => (
                    <div>
                      {controlRate.nameEn === 'Poor' && (
                        <CountRate
                          status={controlRate.nameEn}
                          count={controlRate.count}
                        />
                      )}
                    </div>
                  ))}
                </Cell>
              </TableBody>
              {child.acceptAssessment.isSelectedReject() && (
                <TableBody
                  span={12}
                  key={index}
                  style={{ paddingLeft: 100, paddingRight: 16 }}
                >
                  <InputAll
                    placeholder="กรุณาระบุเหตุผล"
                    handleOnChange={(e) => {
                      child.acceptAssessment.setRejectComment(e.target.value);
                    }}
                    value={child.acceptAssessment.rejectComment}
                    error={!child.acceptAssessment.rejectComment}
                  />
                </TableBody>
              )}
            </div>
          ))}
        </div>
      )}
    </div>
  ));
};

export default RowAssessmentApproveVP;
