import React, { useState } from 'react';
import { Image, Grid, Table } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonBorder } from '../../element';
import styled from 'styled-components';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = () => {
  return (
    <div style={{ paddingTop: 24 }}>
      <Grid>
        <Grid.Row>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Information technology system
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Executor
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      การควบคุมภายในแบบป้องกัน (Preventive control)
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      การควบคุมด้วยมือ (Manual Control)
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      รายปี
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      -
                    </Text>
                  </Div>
                </Table.Cell>
                <Table.Cell>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Process Owner
                    </Text>
                  </Div>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>

        <Grid.Row
          style={{ display: 'flex', alignItems: 'center', paddingBottom: 0 }}
        >
          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
            Document / Evidence
          </Text>
          <div style={{ display: 'flex', paddingLeft: 12 }}>
            <ButtonBorder
              style={{ margin: '0px 4px' }}
              radius
              height={32}
              width={102}
              textColor={colors.textSky}
              borderColor={colors.textGrayLight}
              text={'CSA - 56-1'}
              textSize={sizes.xs}
            />
            <ButtonBorder
              style={{ margin: '0px 4px' }}
              radius
              height={32}
              width={102}
              textColor={colors.textSky}
              borderColor={colors.textGrayLight}
              text={'CSA - 56-1'}
              textSize={sizes.xs}
            />
            <ButtonBorder
              style={{ margin: '0px 4px' }}
              radius
              height={32}
              width={102}
              textColor={colors.textSky}
              borderColor={colors.textGrayLight}
              text={'CSA - 56-1'}
              textSize={sizes.xs}
            />
          </div>
        </Grid.Row>

        <Grid.Row>
          <Text fontSize={sizes.s} color={colors.textPurple} fontWeight="bold">
            Enhancement / Corrective Action
          </Text>
        </Grid.Row>
        <Grid.Row>
          <Div col>
            <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
              Enhancement / Corrective Action
            </Text>
            <Text color={colors.primaryBlack}>{''}</Text>
          </Div>
        </Grid.Row>
        <Grid.Row>
          <Div col>
            <Text fontSize={sizes.xs} color={colors.textDarkBlack}>
              Enhancement/Corrective Action
            </Text>
            <Text color={colors.primaryBlack}>{''}</Text>
          </Div>
        </Grid.Row>
        <Grid.Row columns={4}>
          <Grid.Column style={{ paddingLeft: 0 }}>
            <Text>ผู้รับผิดชอบ</Text>
            <Text color={colors.primaryBlack}>Kunlanee</Text>
          </Grid.Column>
          <Grid.Column style={{ paddingLeft: 0 }}>
            <Text>Due Date</Text>
            <Text color={colors.primaryBlack}>20/05/2563</Text>
          </Grid.Column>
          <Grid.Column style={{ paddingLeft: 0 }}>
            <Text>Benefit</Text>
            <Text color={colors.primaryBlack}>{''}</Text>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};
export default index;
