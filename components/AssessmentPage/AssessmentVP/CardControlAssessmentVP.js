import React, { useState } from 'react';
import { Image, Grid, Popup } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonAll, CheckBoxAll, Box } from '../../element';
import { DetailControlAssessmentVP } from '../../AssessmentPage';
import styled from 'styled-components';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ edit }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isRating, setIsRating] = useState();
  const handelRating = (value) => {
    setIsRating(value);
  };
  const handelOpen = (value) => {
    setIsOpen(value);
  };
  return (
    <div>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Row style={{ padding: 0 }}>
          <Grid.Column
            width={2}
            style={{
              display: 'flex',
              alignItems: 'top',
              padding: '0px',
            }}
          >
            <Popup
              wide="very"
              style={{
                borderRadius: '6px 6px 0px 6px',
                padding: 16,
                marginLeft: -10,
              }}
              trigger={
                <Image
                  height={18}
                  width={18}
                  style={{ marginRight: '8px' }}
                  src="/static/images/zing@3x.png"
                />
              }
            >
              <Text
                fontSize={sizes.xl}
                color={colors.textSky}
                style={{ lineHeight: '1.2' }}
              >
                GC's standard
              </Text>
              <Text
                fontSize={sizes.s}
                color={colors.pink}
                fontWeight={'bold'}
                style={{ paddingBottom: 4 }}
              >
                Control C1 (ID : IC-CSA-1)
              </Text>
              <Text color={colors.primaryBlack}>
                Process Owner สามารถพิจารณา General Rick & Control
                ของแต่ละกระบวนการ จาก Rick & Control Library
                เป็นแนวทางในการประเมินความเสี่ยงเบื้องต้น
              </Text>
            </Popup>
            <div>
              <Text color={colors.pink} fontWeight={'bold'}>
                Control
              </Text>
              <Text fontSize={sizes.s} color={colors.pink} fontWeight={'bold'}>
                C1
              </Text>
            </div>
          </Grid.Column>
          <Grid.Column width={8} style={{ padding: 0, display: 'flex' }}>
            <Text color={colors.primaryBlack} fontWeight={'med'}>
              บริษัทมีระบบการประเมิน 2 ระดับ อันได้แก่ 1. Process Level: Process
              Owner ทุกส่วนงานต้องประเมินความเพียงพอการควบคุมภายใน
              ในกระบวนการตนเอง เป็นประจำทุกปี (Control Self-Assessment: CSA) 2.
              Corporate Level:
              หน่วยงานที่เกี่ยวข้องทำการประเมินการควบคุมภายในโดยใช้แบบประเมินของกลต.
              เป็นประจำทุกปี
            </Text>
            <div
              style={{
                backgroundColor: colors.btGray,
                width: '1px',
                minHeight: '80px',
                margin: '0px 16px',
              }}
            />
          </Grid.Column>
          <Grid.Column width={6} style={{ padding: 0 }}>
            <Grid.Row>
              <Div between>
                <Div col>
                  <Div center>
                    <Text
                      fontSize={sizes.xs}
                      color={colors.textDarkBlack}
                      fontWeight={'med'}
                    >
                      Control Rating{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide="very"
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginRight: -10,
                      }}
                      position="right center" 
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Grid
                        columns="equal"
                        style={{ margin: 0, paddingRight: 16 }}
                      >
                        <Grid.Row>
                          <Grid.Column width={4}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Good
                            </Text>
                          </Grid.Column>
                          <Grid.Column>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - The control is appropriately designed and being
                              implemented as specified
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column width={4}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Fair
                            </Text>
                          </Grid.Column>
                          <Grid.Column>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                - The control is designed but may not be
                                adequate and appropriate
                              </Text>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                - The controls have been designed adequately but
                                not consistently or only partially effective
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column width={4}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Poor
                            </Text>
                          </Grid.Column>
                          <Grid.Column>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - There is no controls assignedd for the risks
                              involved.
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column width={4}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Enahancement
                            </Text>
                          </Grid.Column>
                          <Grid.Column>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - Improve the control that already appropriately
                              designed and being implemented as specified to
                              <span style={{ fontWeight: 'bold' }}>
                                {' '}
                                be more effective{' '}
                              </span>
                              or
                              <span style={{ fontWeight: 'bold' }}>
                                {' '}
                                more efficiency{' '}
                              </span>
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Popup>
                  </Div>
                  {edit ? (
                    <Div style={{ display: 'flex', flexWrap: 'wrap' }}>
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={
                          isRating === 'Good' ? colors.green : colors.btGray
                        }
                        text={'Good'}
                        onClick={() => handelRating('Good')}
                      />
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={
                          isRating === 'Fair' ? colors.yellow : colors.btGray
                        }
                        text={'Fair'}
                        onClick={() => handelRating('Fair')}
                      />
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={isRating === 'Poor' ? colors.red : colors.btGray}
                        text={'Poor'}
                        onClick={() => handelRating('Poor')}
                      />
                    </Div>
                  ) : (
                    <Div>
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={colors.green}
                        text={'Good'}
                      />
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={colors.yellow}
                        text={'Fair'}
                      />
                      <ButtonAll
                        width={88}
                        height={24}
                        textSize={sizes.xs}
                        textWeight={'bold'}
                        color={colors.btGray}
                        text={'Poor'}
                      />
                    </Div>
                  )}
                  <Div top={8}>
                    {edit ? (
                      <Div>
                        <CheckBoxAll />
                        <Text
                          fontSize={sizes.xxs}
                          color={colors.textBlack}
                          fontWeight={'med'}
                          style={{ paddingLeft: 16 }}
                        >
                          Enhancement
                        </Text>
                        <div>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.textSky}
                            style={{ paddingLeft: 90 }}
                          >
                            Last Year
                          </Text>
                        </div>
                      </Div>
                    ) : (
                      <Div>
                        <CheckBoxAll disabled />
                        <Text
                          fontSize={sizes.xxs}
                          color={colors.btGray}
                          fontWeight={'med'}
                          style={{ paddingLeft: 16 }}
                        >
                          Enhancement
                        </Text>
                        <div>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.textSky}
                            style={{ paddingLeft: 90 }}
                          >
                            Last Year
                          </Text>
                        </div>
                      </Div>
                    )}
                  </Div>
                </Div>
                <div
                  className="click"
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}
                >
                  <div onClick={() => handelOpen(!isOpen)}>
                    {isOpen ? (
                      <Image
                        width={24}
                        src="../../static/images/iconUp-blue@3x.png"
                      />
                    ) : (
                      <Image
                        width={24}
                        src="../../static/images/iconDown-blue@3x.png"
                      />
                    )}
                  </div>
                </div>
              </Div>
            </Grid.Row>
          </Grid.Column>
        </Grid.Row>
        <DetailControlAssessmentVP />
      </Grid>
    </div>
  );
};

export default index;
