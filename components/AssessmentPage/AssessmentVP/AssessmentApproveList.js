import React, { useState } from 'react';
import { Image, Pagination, GridRow, Grid, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import { useObserver } from 'mobx-react-lite';
import {
  CheckBoxAll,
  Text,
  InputAll,
  BottomBarAssessment,
  TabScrollBar,
  Cell,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore } from '../../../contexts';
import { ACCEPT_ROADMAP_STATUS } from '../../../model/AcceptRoadmapModel';
import { ACCEPT_ASSESSMENT_STATUS } from '../../../model/AssessmentModel/AcceptAssessmentModel';
import { ROLES } from '../../../contexts/AuthContext';
import { RowAssessmentApproveVP } from '..';
import { toJS } from 'mobx';
import { headersCSAAssessmentVP } from '../../../utils/static';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const CardContainer = styled.div`
  width: 100%;
`;

const CardTabel = styled.div`
  display: inline-block;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-bottom: ${(props) => props.bottom || 0}px;
  padding: ${(props) => props.pad || 0}px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

// const TableHeader = styled.div`
//   font-size: ${(props) => props.FontSize || 0}px;
//   float: left;
//   width: 100%;
//   min-height: 40px;
//   display: flex;
//   flexdirection: row;
//   align-items: center;
//   justify-content: space-between;
//   @media only screen and (min-width: 768px) {
//     width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
//   }
// `;

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const AssessmentApproveList = ({ assessmentData, role }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader span={12}>
            {headersCSAAssessmentVP.map(
              ({ render, key, width, widthMobile, mid }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={mid}
                >
                  {render === 'Reject' || render === 'Accept' ? (
                    <Div col center>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="med"
                        color={
                          render === 'Reject' ? colors.red : colors.primary
                        }
                      >
                        {render}
                      </Text>
                      {render === 'Accept' && (
                        <CheckBoxAll
                          header
                          status={render}
                          checked={assessmentData.isAcceptAssessmentCheckedAll(
                            role,
                            ACCEPT_ASSESSMENT_STATUS.accept,
                          )}
                          onChange={(e, data) =>
                            assessmentData.setCheckAllAcceptAssessment(
                              role,
                              ACCEPT_ASSESSMENT_STATUS.accept,
                              data.checked,
                            )
                          }
                        />
                      )}
                      {render === 'Reject' && (
                        <CheckBoxAll
                          header
                          status={render}
                          checked={assessmentData.isAcceptAssessmentCheckedAll(
                            role,
                            ACCEPT_ASSESSMENT_STATUS.reject,
                          )}
                          onChange={(e, data) =>
                            assessmentData.setCheckAllAcceptAssessment(
                              role,
                              ACCEPT_ASSESSMENT_STATUS.reject,
                              data.checked,
                            )
                          }
                        />
                      )}
                    </Div>
                  ) : (
                    <Div center mid={mid}>
                      <Text
                        center
                        fontSize={sizes.xs}
                        style={{
                          paddingLeft: render === 'Assessment Name' && 16,
                        }}
                        fontWeight="med"
                        color={
                          render === 'Enhancement'
                            ? colors.primary
                            : colors.textlightGray
                        }
                      >
                        {render}
                      </Text>
                    </Div>
                  )}
                </Cell>
              ),
            )}
            <Cell width={screenWidth < 1024 ? 180 : 18} center>
              <Div col center>
                <Text
                  fontSize={sizes.xs}
                  fontWeight="bold"
                  color={colors.textGray}
                >
                  Control by Rating
                </Text>
                <Div between>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.green}
                  >
                    Good
                  </Text>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.yellow}
                  >
                    Fair
                  </Text>
                  <Text
                    center
                    style={{ width: '33%' }}
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.red}
                  >
                    Poor
                  </Text>
                </Div>
              </Div>
            </Cell>
          </TableHeader>
          {assessmentData.assessmentList.map((assessment, index) => (
            <TableBody>
              <RowAssessmentApproveVP
                assessment={assessment}
                role={role}
                index={index}
              />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {assessmentData.totalPage > 1 && (
        <Grid>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getAssessmentList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl3,
                assessmentData.filterModel.selectedLvl4,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedRoadmapType,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift,
                assessmentData.filterModel.selectedStatus,
                assessmentData.filterModel.selectedRate,
                assessmentData.filterModel.selectedYear,
                assessmentData.filterModel.selectedProcessAnd,
                assessmentData.filterModel.selectedProcessOr,
                assessmentData.filterModel.selectedObjectives)
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
      <BottomBarAssessment
        page="AssessmentListVP"
        approvementData={() =>
          assessmentData.submitAssessmentToAdmin(authContext.accessToken)
        }
        assessmentData={assessmentData}
      />
    </div>
  ));
};

export default AssessmentApproveList;
