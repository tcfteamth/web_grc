import React, { useState } from 'react';
import { Image, Grid, GridRow, Popup } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonAll, InputAll, InputGroup } from '../../element';
import {
  CardControlAssessmentVP,
  AddCardControlAssessment,
} from '../../AssessmentPage';
import styled from 'styled-components';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;
const DividerLine = styled.div`
  height: 4px;
  margin-top: 40px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ listRisk }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handelAdd = (value) => {
    setIsAdd(value);
  };
  return (
    <div>
      {listRisk &&
        listRisk.map((item) => (
          <div>
            <Grid columns="equal" style={{ margin: 0, padding: 24 }}>
              <Grid.Row style={{ padding: 0 }}>
                <Div between>
                  <Div>
                    <Text fontSize={sizes.s} color={colors.primary}>
                      Risk
                      <span
                        style={{
                          paddingLeft: 4,
                          fontWeight: 'bold',
                          marginRight: '16px',
                        }}
                      >
                        R1
                      </span>
                    </Text>
                    <Text fontSize={sizes.s} color={colors.primary}>
                      บริษัทไม่มีระบบการประเมินความเสี่ยงเพื่อป้องกันหรือลดข้อผิดพลาดที่อาจเกิดขึ้น
                    </Text>
                  </Div>
                </Div>
              </Grid.Row>
              <Divider />
              {item.control.map((item, index) => (
                <Grid.Row style={{ padding: 0 }}>
                  {index !== 0 && <Divider bgcolor={'#d9d9d6'} />}
                  <CardControlAssessmentVP />
                </Grid.Row>
              ))}
            </Grid>
            <DividerLine />
          </div>
        ))}
    </div>
  );
};

export default index;
