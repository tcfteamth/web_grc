import React, { useEffect, useState } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import { colors, sizes } from '../../../utils';
import { Text, ProgressBarStatusTran } from '../../element';
import { CardResultVPModel } from '../../../model/AssessmentModel';
import styled from 'styled-components';

import { initAuthStore } from '../../../contexts';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const CardBody = styled.div`
  display: flex;
  display: inline-block;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  padding: 40px 20px !important;
  margin-bottom: 16px;
`;

const TabRow = styled.div`
  display: flex;
  width: 100%;
  height: 40px;
  align-items: center;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  padding: 4px 8px !important;
  border: 1px solid #eaeaea;
`;

const StatusTag = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  padding: 1px 16px;
  height: ${(props) => (props.Bodys ? '40px' : '44px')};
  text-align: center;
  align-items: center;
  display: flex;
  width: 100%;
  justify-content: space-around;
  background-color: ${(props) =>
    props.Awaiting2ndLineApprove
      ? colors.brightbule
      : props.AwaitingVPApprove
      ? colors.pralblue
      : props.Preparing
      ? colors.purplepink
      : props.AwaitingDMApprove
      ? colors.pralpurple
      : props.Division
      ? colors.purpledark
      : props.Closed
      ? colors.bluebright
      : `#FFFFFF`};
`;

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  margin: 14px 13px 8px 13px;
  align-items: center;
  padding: 0px !important;
  justify-content: space-between;
`;
const TableBody = TableHeader.extend`
  margin: 2px 13px 2px 13px;
  min-height: 62px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
  padding: 8px;
  align-items: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const headers = [
  { key: 'Division', render: 'Division', width: 20 },
  { key: 'Preparing', render: 'Preparing', width: 20 },
  { key: 'Approving', render: 'Approving', width: 20 },
  { key: 'Verifying', render: 'Verifying', width: 20 },
  {
    key: 'Completed',
    render: 'Completed',
    width: 20,
  },
];

const index = ({ deparmentId }) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isEdit, setIsEdit] = useState(false);
  const cardResultVPModel = useLocalStore(() => new CardResultVPModel());
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelEdit = (value) => {
    setIsEdit(value);
  };

  useEffect(() => {
    cardResultVPModel.getResultVP(authContext.accessToken, deparmentId);
  }, [deparmentId]);

  return useObserver(() => (
    <div>
      <CardBody style={{ margin: 0 }}>
        <Grid style={{ padding: 0 }}>
          <TableHeader span={12}>
            {screenWidth >= 768 &&
              headers.map(({ render, key, width, center }) => (
                <Cell key={key} width={width} center={center}>
                  {key === 'Division' ? (
                    <StatusTag
                      Division
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '0px 8px',
                      }}
                    >
                      <Text
                        color={colors.textpurpledark}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        {screenWidth < 1024 ? 'Div' : 'Division'}
                      </Text>
                      <Text
                        color={colors.primaryBlack}
                        fontWeight="med"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        Total
                      </Text>
                    </StatusTag>
                  ) : key === 'Preparing' ? (
                    <StatusTag Preparing>
                      <Text
                        color={colors.textpurplepink}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        {render}
                      </Text>
                    </StatusTag>
                  ) : key === 'Approving' ? (
                    <StatusTag AwaitingDMApprove>
                      <Text
                        color={colors.textpralpurple}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        {render}
                      </Text>
                    </StatusTag>
                  ) : key === 'Verifying' ? (
                    <StatusTag AwaitingVPApprove>
                      <Text
                        color={colors.textpralblue}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        {render}
                      </Text>
                    </StatusTag>
                  ) : key === 'Completed' ? (
                    <StatusTag Awaiting2ndLineApprove>
                      <Text
                        color={colors.textbrightbule}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxxs : sizes.xs}
                      >
                        {render}
                      </Text>
                    </StatusTag>
                  ) : key === 'Closed' ? (
                    <StatusTag Closed>
                      <Text
                        color={colors.textbluebright}
                        fontWeight="bold"
                        fontSize={sizes.xs}
                      >
                        {render}
                      </Text>
                    </StatusTag>
                  ) : (
                    ''
                  )}
                </Cell>
              ))}
          </TableHeader>
          {cardResultVPModel.list &&
            cardResultVPModel.list.map((result, index) => (
              <TableBody>
                <Cell width={screenWidth < 768 ? 100 : 20} center>
                  <TabRow>
                    <Div
                      between
                      style={{ alignItems: 'center', alignSelf: 'center' }}
                    >
                      <Text
                        color={colors.textPurple}
                        fontWeight="bold"
                        fontSize={screenWidth < 1024 ? sizes.xxs : sizes.m}
                      >
                        {result.name}
                      </Text>
                      <Text
                        color={colors.primaryBlack}
                        fontWeight="med"
                        fontSize={screenWidth < 1024 ? sizes.xxs : sizes.m}
                      >
                        {screenWidth < 768
                          ? `Total - ${result.total}`
                          : result.total}
                      </Text>
                    </Div>
                  </TabRow>
                </Cell>
                <Cell
                  center
                  width={screenWidth < 768 ? 100 : 80}
                  style={{
                    paddingLeft: screenWidth < 1024 ? '50px' : '100px',
                    paddingRight: screenWidth < 1024 ? '50px' : '100px',
                  }}
                >
                  <Div className="card-status-vp">
                    <ProgressBarStatusTran
                      data={result.status}
                      mode={screenWidth < 768 ? 'dashboard' : 'card'}
                    />
                  </Div>
                </Cell>
              </TableBody>
            ))}
        </Grid>
      </CardBody>
    </div>
  ));
};

export default index;
