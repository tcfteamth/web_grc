import React, { useState, useEffect } from "react";
import { withLayout } from "../../hoc";
import { Image, Loader, Dimmer } from "semantic-ui-react";
import Link from "next/link";
// import Router, { useRouter } from "next/router";
import { ButtonAll, Text, ButtonBorder } from "../../components/element";
import { observer } from "mobx-react-lite";
import { colors, sizes } from "../../utils";
import { ListCSAReport } from "../../utils/static";
import styled from "styled-components";
import _ from "lodash";

const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  overflow-x: auto;
  overflow-y: hidden;
`;

const CardTab = styled.div`
  border-radius: 6px 6px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableHeader = styled.div`
  height: 50%;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  border-top: ${(props) => (props.top ? "" : `1px solid #d9d9d6`)};
  width: auto;
`;

const CardRow = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 50px;
  width: ${(props) => props.width || 100}%;
  border-top: ${(props) => (props.top ? "" : `1px solid #d9d9d6`)};
`;

const Card = styled.div`
    display: flex;
    flex-direction: column;
    display: inline-block;
    background-color: ${colors.backgroundPrimary};
    border-radius: 6px 6px 0px 6px !important;      
    min-height: 50px;
    width: 100%;
    padding: ${(props) => props.padding || 24}px;
    box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
    margin-top: ${(props) => props.top || 10}px;
    text-align: ${(props) =>
      props.center
        ? "center"
        : props.right
        ? "flex-end"
        : props.mid
        ? `center`
        : props.between
        ? `space-between`
        : `flex-start`};

    @media only screen and (min-width: 800px){
      width: ${(props) => (props.width / 12) * 100 || (12 / 12) * 100}%;
`;
const Cell = styled.div`
  width: ${(props) => props.width}%;
  padding: 10px;
  display: flex;
  align-items: start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? "center" : "flex-start")};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? "space-around"
      : props.right
      ? "flex-end"
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;

const Line = styled.div`
  border: 1px solid #000;
  width: 8px;
  margin-top: 20px;
`;

const headers = [
  { key: "Sub-Process", render: "Sub-Process", width: 35 },
  { key: "ความเสี่ยง", render: "ความเสี่ยง", width: 35 },
  { key: "การควบคุม", render: "การควบคุม", width: 15 },
  { key: "ผลการประเมิน", render: "ผลการประเมิน", width: 15 },
];

const sortBy = [
  { text: "Latest", value: 0 },
  { text: "Agent", value: 1 },
  { text: "Team", value: 2 },
  { text: "Walk in", value: 3 },
];

const PageAction = (props) => {
  // const { query } = useRouter();
  const [date, setDate] = useState();

  // useEffect(() => {
  //   onChangeCloseDate(state);
  // }, [query]);

  return (
    <div>
      <CardTab style={{ marginTop: 16 }}>
        <Div col>
          <TableHeader left={14}>
            {headers.map(({ render, key, width }) => (
              <Cell width={width}>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.textGray}
                >
                  {render}
                </Text>
                {render === "Sub-Process" || render === "Status" ? (
                  <Image
                    src="../../static/images/iconDown-gray.png"
                    style={{ marginLeft: 8, width: 18, height: 18 }}
                  />
                ) : (
                  ""
                )}
              </Cell>
            ))}
          </TableHeader>

          {ListCSAReport &&
            ListCSAReport.map((item, index) => (
              <TableBody>
                <Cell width={35}>
                  <Div col>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="med"
                      color={colors.primaryBlack}
                    >
                      {item.text}
                    </Text>
                  </Div>
                </Cell>

                <div>
                  {item.data.map((i, index) => (
                    <CardRow top={index === 0}>
                      <Cell width={35}>
                        <Div col>
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            {i.risk}
                          </Text>
                        </Div>
                      </Cell>
                      <Cell width={15}>
                        <Div col>
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            {i.control}
                          </Text>
                        </Div>
                      </Cell>
                      <Cell width={15}>
                        <Div col>
                          <ButtonAll
                            text={i.assessment}
                            color={colors.yellow}
                            width={80}
                            height={24}
                            textSize={sizes.xxxs}
                            textWeight={"bold"}
                          />
                        </Div>
                      </Cell>
                    </CardRow>
                  ))}
                </div>
              </TableBody>
            ))}
        </Div>
      </CardTab>
    </div>
  );
};

export default PageAction;
