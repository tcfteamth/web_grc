import React, { useState } from 'react';
import styled from 'styled-components';
import { CheckBoxAll, Text, InputAll } from '../../components/element';
import { colors, sizes } from '../../utils';
import {
  Image,
  Icon,
  Grid,
  Pagination,
  GridRow,
  Loader,
  Dimmer,
} from 'semantic-ui-react';
import { dataList, dataListRoadMap } from '../../utils/static';

const CardContainer = styled.div`
  margin-bottom: 98px;
  margin-top: 16px;
`;

const CardTabel = styled.div`
  display: inline-block;
  min-height: 50px;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  margin-bottom: ${(props) => props.bottom || 0}px;
  padding: 10px 16px;
  text-align: ${(props) =>
    props.center
      ? 'center'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;
const TableHeader = styled.div`
  float: left;
  width: 100%;
  min-height: 40px;
  display: flex;
  flexdirection: row;
  align-items: center;
  justify-content: space-between;
  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  margin-top: 0px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: center;
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 8}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
`;
const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? `center`
      : props.between
      ? `space-between`
      : `flex-start`};
  padding-left: ${(props) => props.left || 0}px;
`;

const headers = [
  { key: 'Accept', render: 'Accept', width: 4 },
  { key: 'Reject', render: 'Reject', width: 4 },
  { key: 'Assessment Name', render: 'Assessment Name', width: 53, left: '16' },
  { key: 'Assessor', render: 'Assessor', width: 15 },
  {
    key: 'View CSA (Last Year)',
    render: 'View CSA (Last Year)',
    width: 8,
    mid: 'center',
  },
  { key: 'Remark', render: 'Remark', width: 8, mid: 'center' },
  { key: 'Roadmap', render: 'Roadmap', width: 8, mid: 'center' },
];

const RoadMapAssessment = ({}) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isShow, setIsShow] = useState(false);

  const handelOpen = (value) => {
    setIsOpen(value);
  };
  const handelOpenInput = (value) => {
    console.log('value', value);
    setIsShow(value);
  };

  return (
    <CardContainer>
      <CardTabel style={{ marginBottom: 8 }}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, mid, left }) => (
            <Cell key={key} width={width} center={mid} left={left}>
              {render === 'Accept' || render === 'Reject' ? (
                <Div col>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={render === 'Accept' ? '#0057b8' : '#e4002b'}
                  >
                    {render}
                  </Text>
                  <CheckBoxAll status={render} />
                </Div>
              ) : (
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="bold"
                  color={colors.textGray}
                >
                  {render}
                </Text>
              )}
            </Cell>
          ))}
        </TableHeader>
      </CardTabel>
      {dataListRoadMap &&
        dataListRoadMap.map((item, index) => (
          <CardTabel center bottom={4}>
            <TableBody span={12} key={index}>
              <Cell width={4} center>
                <CheckBoxAll header status={'Accept'} />
              </Cell>
              <Cell width={4} center>
                <CheckBoxAll header status={'Reject'} />
              </Cell>
              <Cell width={89} left={16}>
                <Text
                  fontWeight="bold"
                  fontSize={sizes.s}
                  color={colors.primaryBlack}
                >
                  {item.textHeader}
                </Text>
              </Cell>
              <Cell width={4} right>
                <div
                  className="click"
                  style={{ paddingLeft: 8 }}
                  onClick={() => handelOpen(!isOpen)}
                >
                  {isOpen ? (
                    <Image width={18} src="/static/images/iconUp-blue.png" />
                  ) : (
                    <Image width={18} src="/static/images/iconDown.png" />
                  )}
                </div>
              </Cell>
            </TableBody>
            {isOpen && (
              <div>
                {item.subData.map((item, index) => (
                  <div>
                    <TableBody span={12} key={index}>
                      <Cell width={4} center>
                        <CheckBoxAll status={'Accept'} />
                      </Cell>
                      <Cell width={4} center>
                        <CheckBoxAll status={'Reject'} />
                      </Cell>
                      <Cell width={53} left={16}>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {item.text}
                        </Text>
                      </Cell>
                      <Cell width={15}>
                        {item.assessor === 'Assign +' ? (
                          <div
                            className="click"
                            onClick={() => handelOpenInput(!isShow)}
                          >
                            <Text fontSize={sizes.xxs} color={'#00aeef'}>
                              {item.assessor}
                            </Text>
                          </div>
                        ) : (
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.assessor}
                          </Text>
                        )}
                      </Cell>
                      <Cell width={8} center>
                        <Image
                          src="../../static/images/file-purple.png"
                          style={{ width: 18, height: 18 }}
                        />
                      </Cell>
                      <Cell width={8} center>
                        <Image
                          src="../../static/images/edit-purple@3x.png"
                          style={{ width: 18, height: 18 }}
                        />
                      </Cell>
                      <Cell width={8} center>
                        {item.status === 'Re Assessment' ? (
                          <Image
                            src="../../static/images/re@3x.png"
                            style={{ width: 18, height: 18 }}
                          />
                        ) : item.status === 'New Assessment' ? (
                          <Image
                            src="../../static/images/new@3x.png"
                            style={{ width: 18, height: 18 }}
                          />
                        ) : (
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.year}
                          </Text>
                        )}
                      </Cell>
                    </TableBody>
                    <div style={{ paddingLeft: '6%' }}>
                      {isShow && <InputAll placeholder="กรุณาระบุเหตุผล" />}
                    </div>
                  </div>
                ))}
              </div>
            )}
          </CardTabel>
        ))}

      <GridRow style={{ justifyContent: 'flex-end', marginRight: 13 }}>
        <Pagination
          style={{
            fontSize: sizes.xs,
            fontWeight: 'bold',
            borderColor: colors.backgroundSecondary,
          }}
          defaultActivePage={1}
          // activePage={1}
          // onPageChange={handlePageChange}
          firstItem={null}
          lastItem={null}
          nextItem={{
            content: <Icon name="angle right" />,
            icon: true,
          }}
          prevItem={{
            content: <Icon name="angle left" />,
            icon: true,
          }}
          totalPages={10}
        />
      </GridRow>
      {/* <BottomBar /> */}
    </CardContainer>
  );
};

export default RoadMapAssessment;
