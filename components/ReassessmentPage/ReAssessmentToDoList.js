import React from 'react';
import styled from 'styled-components';
import { Icon, Grid, Pagination } from 'semantic-ui-react';
import { useObserver } from 'mobx-react';
import {
  CheckBoxAll,
  Text,
  BottomBarAssessment,
  Cell,
  TabScrollBar,
} from '../element';
import { colors, sizes } from '../../utils';
import { headersReAssessment } from '../../utils/static';
import useWindowDimensions from '../../hook/useWindowDimensions';
import { initAuthStore } from '../../contexts';
import { RowReAssessmentToDoList } from '.';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const ReAssessmentToDoList = ({ assessmentData, role }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersReAssessment.map(
              ({ render, key, width, widthMobile, mid }) => (
                <Cell
                  center={mid}
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                >
                  {render === 'Submit' ? (
                    <Div col center>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.primary}
                      >
                        {render}
                      </Text>
                      <CheckBoxAll
                        header
                        status={render}
                        checked={assessmentData.isSubmitAssessmentCheckedAll()}
                        onChange={(e, data) =>
                          assessmentData.setCheckAllSubmitAssessment()
                        }
                      />
                    </Div>
                  ) : (
                    <Div mid={key !== 'Assessment'}>
                      <Text
                        center
                        fontSize={sizes.xs}
                        fontWeight="med"
                        color={colors.textlightGray}
                        style={{ paddingLeft: key === 'Assessment' && 16 }}
                      >
                        {render}
                      </Text>
                    </Div>
                  )}
                </Cell>
              ),
            )}
          </TableHeader>
          {assessmentData.assessmentList.map((assessment, index) => (
            <TableBody>
              <RowReAssessmentToDoList assessment={assessment} />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {assessmentData.totalPage > 1 && (
        <Grid>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={assessmentData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) =>
              assessmentData.getReAssessmentList(
                authContext.accessToken,
                activePage,
                assessmentData.searchAssessmentText,
                assessmentData.filterModel.selectedLvl3,
                assessmentData.filterModel.selectedLvl4,
                assessmentData.filterModel.selectedBu,
                assessmentData.filterModel.selectedDepartment.no,
                assessmentData.filterModel.selectedDivision,
                assessmentData.filterModel.selectedShift)
              }
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={assessmentData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default ReAssessmentToDoList;
