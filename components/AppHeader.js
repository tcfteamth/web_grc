/* eslint-disable react/jsx-fragments */
import React, { useState, useEffect } from 'react';
import { Segment, Header, Button, Popup, Grid, Image } from 'semantic-ui-react';
import styled from 'styled-components';
import idx from 'idx';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { colors, sizes } from '../utils';
import { Text } from './element';
import { initAuthStore, initDataContext } from '../contexts';

const Segments = styled(Segment)`
  flex-direction: row;
  display: flex;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  width: 100% !important;
  height: 90px !important;
  background-color: rgba(255, 255, 255, 1) !important;
  align-self: top !important;
  align-items: center !important;
  justify-content: space-between;
  position: fixed !important;
  margin-top: 0 !important;
  padding: 0px !important;
  z-index: 10;
`;

const LineHeader = styled.div`
  border-left: 1px solid rgba(48, 48, 48, 0.04) !important;
  height: 87px !important;
  margin-right: 24px;
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const Text1Line = styled(Text)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;
`;

const AppHeader = (props) => {
  const authContext = initAuthStore();
  const [isLanguage, setIsLanguage] = useState('Thai');
  const [userProfile, setUserProfile] = useState(authContext.currentUser);
  const [screenWidth, setScreenWidth] = useState();
  const onHandleLanguage = (data) => {
    setIsLanguage();
  };

  useEffect(() => {
    // setUserProfile();
    setScreenWidth(window.screen.width);
  }, []);

  return (
    <Segments attached="top" style={{ alignItems: 'center' }}>
      <Header floated="left" style={{ padding: 0, alignItems: 'center' }}>
        <Link href="/">
          <div
            className="img-header menu-slide-desktop"
            style={{
              flexDirection: 'row',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Image
              src="../static/images/logo-csa@3x.png"
              style={{ width: 'auto', height: 80, marginTop: -20 }}
            />
          </div>
        </Link>
      </Header>
      <Header floated="right">
        <Div between center>
          <Div center className="menu-profile">
            <Div center>
              {/* <LineHeader /> */}
              {/* {userProfile && (
                <Image
                  circular
                  src={
                    userProfile.profileImage ||
                    'https://react.semantic-ui.com/images/wireframe/square-image.png'
                  }
                  style={{ width: 48, height: 48 }}
                />
              )} */}
              <Div col style={{ paddingLeft: 16 }}>
                {userProfile && (
                  <React.Fragment>
                    <Popup
                      trigger={
                        <Text1Line
                          fontSize={sizes.s}
                          fontweight="med"
                          color={colors.primaryBlack}
                        >
                          {userProfile.firstNameTh} {userProfile.lastNameTh}
                        </Text1Line>
                      }
                      content={`${userProfile.firstNameTh} ${userProfile.lastNameTh}`}
                      size="huge"
                    />
                     {userProfile.role !== 'IC-AGENT' && (<Popup
                      trigger={
                        <Text1Line
                          fontSize={sizes.s}
                          fontweight="med"
                          color={colors.textlightGray}
                          style={{ marginTop: -4 }}
                        >
                          {userProfile.positionEn} - {userProfile.divEn}
                        </Text1Line>
                      }
                      content={`${userProfile.positionEn} ${userProfile.divEn}`}
                      size="huge"
                    />)}
                    {userProfile.role === 'IC-AGENT' && (<Popup
                      trigger={
                        <Text1Line
                          fontSize={sizes.s}
                          fontweight="med"
                          color={colors.textlightGray}
                          style={{ marginTop: -4 }}
                        >
                          {userProfile.role} - {userProfile.divEn}
                        </Text1Line>
                      }
                      content={`${userProfile.role} ${userProfile.divEn}`}
                      size="huge"
                    />)}
                  </React.Fragment>
                )}
              </Div>
            </Div>
          </Div>
          <Div>
            <div
              className="button click menu-profile-text"
              style={{
                backgroundColor: '#f6f6f6',
                marginRight: 16,
                borderRadius: 4,
                padding: 7,
              }}
            >
              <Popup
                position="top right"
                style={{
                  borderRadius: '6px 6px 0px 6px',
                  marginRight: -16,
                  marginTop: 16,
                }}
                trigger={
                  <Image
                    src="../static/images/user@3x.png"
                    height={22}
                    width={23}
                  />
                }
                content={
                  <Div col>
                    <Text
                      color={colors.primaryBlack}
                      fontWeight="med"
                      fontSize={sizes.s}
                    >
                      {userProfile.firstNameTh} {userProfile.lastNameTh}
                    </Text>
                    <Text fontSize={sizes.s}>
                      {userProfile.positionEn} - {userProfile.divEn}
                    </Text>
                  </Div>
                }
                size="huge"
              />
            </div>
            <div
              className="button click"
              style={{
                backgroundColor: '#f6f6f6',
                marginRight: 16,
                borderRadius: 4,
                padding: 7,
              }}
              onClick={authContext.logout}
            >
              <Image src="../static/images/power.png" height={22} width={23} />
            </div>
          </Div>
        </Div>
      </Header>
    </Segments>
  );
};

AppHeader.propTypes = {
  title: PropTypes.string,
};

export default AppHeader;
