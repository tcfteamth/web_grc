import React, { useEffect, useState, useContext } from 'react';
import {
  Menu,
  Image,
  Button,
  Dimmer,
  Grid,
  Loader,
  Radio,
} from 'semantic-ui-react';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import { toJS } from 'mobx';
import _ from 'lodash';
import { colors } from '../utils';
import { handleRoleAccessMenu } from '../utils/roleAccessment';
import { initDataContext, initAuthStore } from '../contexts';

import {
  ButtonBorder,
  Text,
  ButtonAll,
  Modal,
  DropdownAll,
  InputDropdown,
  TextArea,
  InputAll,
  RadioBox,
} from './element';
import request from '../services';

const ButtonControl = styled(Button)`
  background-color: ${(props) => props.bgcolor || colors.lightBlack} !important;
  border-radius: 6px !important;
  border-width: 2px !important;
  height: ${(props) => props.height || 40}px !important;
`;
const Section = styled.div`
  display: flex;
  align-items: center;
  ${(props) => (props.left ? 'margin-right: 10px' : 'margin-left: 10px;')}
`;
const Bottombar = styled(Menu)`
  height: 96px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fefefe !important;
  border-top: 4px solid ${colors.primary} !important;
`;
const Divider = styled.div`
  height: 39px;
  width: 1px;
  background-color: ${colors.backgroundPrimary};
`;
const Badge = styled.div`
  background-color: ${colors.lightgray};
  border-radius: 4px;
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const BottomBar = observer((props) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();
  const [roles, setRoles] = useState(authContext.roles);
  // const router = useRouter();
  const [select, setSelect] = useState([]);
  const [active, setActive] = useState(false);
  const [mode, setMode] = useState('');
  const [titleText, setTitleText] = useState('');
  const [btnRight, setBtnRight] = useState();

  const [title, setTitle] = useState({ preTitle: '', postTitle: '' });

  const setAlertModal = (title, preTitle, postTitle, mode, btnRight) => {
    setTitleText(title);
    setTitle({ preTitle, postTitle });
    setMode(mode);
    setActive(true);
    setBtnRight(btnRight);
  };

  const handleSave = () => {
    console.log('save', props);
  };

  const handleCreateNewRole = async () => {
    const body = {
      roleName: dataContext.name.trim(),
      isDelete: false,
      authorities: dataContext.roleId,
    };

    const { name, isDelete, roleId } = dataContext;

    if (name && !isDelete && roleId.length > 0) {
      await request.clientServices
        .createNewRole(body)
        .then((response) => {
          console.log(response);
          Router.push('/roleManagement/RoleList');
        })
        .catch((error) => {
          dataContext.status = 'ชื่อ Role มีในระบบแล้ว';
          console.log(error);
        });
    }
  };

  const handleEditRole = async () => {
    const body = {
      id: dataContext.id,
      roleName: dataContext.name.trim(),
      isDelete: false,
      authorities: dataContext.roleId,
    };

    const { name, isDelete, roleId } = dataContext;
    // console.log(name);
    if (name && !isDelete && roleId.length > 0) {
      await request.clientServices
        .editRole(body)
        .then((response) => {
          console.log('handleEditRole', response);
          Router.push('/roleManagement/RoleList');
        })
        .catch((error) => {
          dataContext.status = 'ชื่อ Role มีในระบบแล้ว';
          console.log('handleEditRole', error);
        });
    }
  };

  useEffect(
    () =>
      // setAaa(props.dataContext.page); // A magical happen here

      function cleanup() {
        dataContext.name = '';
        dataContext.status = '';
        dataContext.roleId = [];
      },
    [],
  );

  useEffect(() => {
    console.log('dataContext.name', dataContext.name);
  }, [dataContext.name]);

  return (
    <div>
      {dataContext.page && (
        <Bottombar className="pl-Bottombar" fluid fixed="bottom" size="large">
          {console.log('dataContext', dataContext.roleId)}
          <Menu.Menu position="left">
            {dataContext.page === 'processAdmin' &&
              handleRoleAccessMenu('CSA_DATABASE_ADD') && (
                <Section>
                  <ButtonBorder
                    borderColor={colors.textSky}
                    textColor={colors.textSky}
                    textUpper
                    textWeight="med"
                    text="Create New Process"
                    icon="../../static/images/iconPlus@3x.png"
                    handelOnClick={() =>
                      setAlertModal('Create Process', '', '', 'processAdmin')
                    }
                  />
                </Section>
              )}
            {dataContext.page === 'roleList' &&
              handleRoleAccessMenu('CSA_ROADMAP_ADD') && (
                <Section left>
                  <ButtonBorder
                    handelOnClick={() =>
                      setAlertModal({
                        header: 'Request New Sub-Process',
                        content: 'Scope of work',
                        btnCancel: 'Cancel',
                        btnSubmit: 'Summit',
                        keyContent: true,
                      })
                    }
                    borderColor="#00aeef"
                    textColor="#00aeef"
                    text="Request New Sub-Process"
                    icon="../../static/images/iconPlus@3x.png"
                  />
                </Section>
              )}

            {dataContext.page === 'roadmapList' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() =>
                    setAlertModal(
                      'Request New Sub-Process',
                      'Scope of work',
                      '',
                      'roadmapList',
                    )
                  }
                  fontSize={sizes.s}
                  textWeight="med"
                  borderColor="#00aeef"
                  textColor="#00aeef"
                  text="Request New Sub-Process"
                  icon="../../static/images/iconPlus@3x.png"
                />
              </Section>
            )}

            {dataContext.page === 'roadmapAdminAess' ||
            dataContext.page === 'roadmapAdminReq' ? (
              <Section>
                <ButtonBorder
                  borderColor={colors.textSky}
                  textColor={colors.textSky}
                  textUpper
                  textWeight="med"
                  text="Create RoadMap"
                  icon="../../static/images/iconPlus@3x.png"
                  handelOnClick={() =>
                    setAlertModal('Create RoadMap', '', '', 'roadmapAdmin')
                  }
                />
              </Section>
            ) : (
              ''
            )}

            {/* <Section left>
            <ButtonBorder
              handelOnClick={() =>
                setAlertModal("คุณยืนยันที่จะลบ", "ใช่หรือไม่", "delete")
              }
              borderColor={colors.textSky}
              textColor={colors.textSky}
              text="Create Sub Process"
              textUpper
              icon={"../../static/images/iconPlus@3x.png"}
            />
          </Section>
          <Section left>
            <ButtonBorder
              text="Export"
              textColor={colors.greenExport}
              fontSize={sizes.s}
              width={160}
              borderColor={colors.greenExport}
              icon={"../../static/images/excel@3x.png"}
            />
          </Section>
          <Section left>
            <ButtonBorder
              text="import"
              textColor={colors.primaryBlack}
              width={160}
              fontSize={sizes.s}
              textUpper
              borderColor={colors.btGray}
              icon={"../../static/images/excel@3x.png"}
            />
          </Section>
          <Section left>
            <ButtonAll
              borderColor={colors.primary}
              textColor={colors.backgroundPrimary}
              textUpper
              width={160}
              text="save"
            />
          </Section> */}
          </Menu.Menu>

          <Menu.Menu position="right">
            {/* <Section left>
            <ButtonBorder
              handelOnClick={() =>
                setAlertModal("คุณยืนยันที่จะลบ", "ใช่หรือไม่", "delete")
              }
              borderColor="#00aeef"
              textColor="#00aeef"
              text="Create Sub Process"
              icon={"../../static/images/iconPlus@3x.png"}
            />
          </Section> */}
            {dataContext.page === 'role' &&
              handleRoleAccessMenu('ROLE_MANAGEMENT_ADD') && (
                <Link href="/roleManagement/RoleCreate">
                  <Section>
                    <ButtonBorder
                      borderColor="#00aeef"
                      textColor="#00aeef"
                      textUpper
                      textWeight="med"
                      width={160}
                      text="Create Role"
                      icon="../../static/images/iconPlus@3x.png"
                    />
                  </Section>
                </Link>
              )}
            {(dataContext.page === 'roleCreate' ||
              dataContext.page === 'roleEdit') &&
            handleRoleAccessMenu('ROLE_MANAGEMENT_ADD') ? (
              // <Link href="/roleManagement/RoleList">
              <Section>
                <ButtonAll
                  width={160}
                  borderColor={colors.primary}
                  textColor={colors.backgroundPrimary}
                  color={
                    dataContext.roleId.length === 0 || !dataContext.name
                      ? colors.textGray
                      : colors.primary
                  }
                  textUpper
                  textWeight="med"
                  text="save"
                  disabled={
                    dataContext.roleId.length === 0 || !dataContext.name
                  }
                  onClick={
                    dataContext.page === 'roleCreate'
                      ? handleCreateNewRole
                      : handleEditRole
                  }
                />
              </Section>
            ) : (
              // </Link>
              ''
            )}
            {dataContext.page === 'process' ||
            dataContext.page === 'roadmapView' ||
            dataContext.page === 'roadmapAdminAess' ||
            dataContext.page === 'roadmapAdminReq' ||
            dataContext.page === 'processAdmin' ? (
              <Section>
                <ButtonBorder
                  text="Export"
                  width={150}
                  textColor={colors.greenExport}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  borderColor={colors.greenExport}
                  icon="../../static/images/excel@3x.png"
                />
              </Section>
            ) : (
              ''
            )}

            {dataContext.page === 'roadmapList' && (
              <Section>
                <ButtonBorder
                  handelOnClick={() =>
                    setAlertModal(
                      'Confirm Accept and Reject',
                      '',
                      '',
                      'Confirm',
                    )
                  }
                  width={160}
                  textWeight="med"
                  borderColor={colors.textGrayLight}
                  textColor={colors.primaryBlack}
                  textUpper
                  text="next"
                />
              </Section>
            )}
            {dataContext.page === 'roadmapAdminAess' && (
              <Section>
                <ButtonAll
                  text="send to dm"
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color={colors.textPurple}
                  // color={"#A4A2BF"}
                />
              </Section>
            )}
            {dataContext.page === 'roadmapAdminReq' && (
              <Section>
                <ButtonAll
                  text="send to dm"
                  textColor={colors.backgroundPrimary}
                  fontSize={sizes.s}
                  textUpper
                  textWeight="med"
                  color="#A4A2BF"
                />
              </Section>
            )}

            {/* <Link href="/roleManagement/RoleCreate">
            <Section left>
              <ButtonBorder
                borderColor={colors.textGrayLight}
                textColor={colors.primaryBlack}
                textUpper
                text="next"
              />
            </Section>
          </Link>

          <Section left>
            <ButtonBorder
              text="Export"
              textColor={colors.greenExport}
              fontSize={sizes.s}
              borderColor={colors.greenExport}
              icon={"../../static/images/excel@3x.png"}
            />
          </Section>


          <Link href="/roleManagement/RoleList">
            <Section left>
              <ButtonAll
                color={colors.textPurple}
                textColor={colors.backgroundPrimary}
                textUpper
                text="Send to VP"
              />
            </Section>
          </Link>

          <Section left>
            <ButtonBorder
              borderColor={colors.textGrayLight}
              textColor={colors.primaryBlack}
              textUpper
              text="reject"
            />
          </Section>
          <Section left>
            <ButtonAll
              borderColor={colors.primary}
              textColor={colors.backgroundPrimary}
              textUpper
              text="approve"
            />
          </Section>

          <Section left>
            <Grid.Row
              style={{ display: "flex", alignItems: "center", marginTop: 8 }}
            >
              <div style={{ margin: "0px 15px" }}>
                <CheckBoxAll />
              </div>
              <Text
                className="upper"
                fontSize={sizes.xs}
                color={colors.primaryBlack}
                fontWeight={"med"}
              >
                ready to submit
              </Text>
            </Grid.Row>
          </Section> */}
          </Menu.Menu>

          <Modal
            open={active}
            title={titleText}
            mode={mode}
            submitTextRight={btnRight}
            ContentComponent={() => (
              <div>
                {mode === 'processAdmin' ? (
                  <Grid columns="equal">
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Key Process (Level 2)
                        </Text>
                        <InputDropdown placeholder="Key Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process (Level 3)
                        </Text>
                        <InputAll placeholder="Select Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Sub-Process (Level 4)
                        </Text>
                        <InputAll placeholder="Select Sub-Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process Definition
                        </Text>
                        <TextArea placeholder="Remark" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Value
                        </Text>
                        <InputAll placeholder="Value" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Source Mapping - Process Name
                        </Text>
                        <InputAll placeholder="Source Mapping - Process Name" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Source Mapping - Process Definition
                        </Text>
                        <InputAll placeholder="Source Mapping - Process Definition" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Exactly Matched
                        </Text>
                        <div>
                          <Grid columns="equal">
                            <Grid.Row>
                              <Grid.Column>
                                <Div between center top={4}>
                                  <RadioBox label="YES" />
                                  <RadioBox label="No" />
                                </Div>
                              </Grid.Column>
                              <Grid.Column>
                                <InputAll
                                  disabled
                                  minWidth={60}
                                  placeholder="Reason"
                                />
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </div>
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Remark
                        </Text>
                        <InputAll minWidth={60} placeholder="2019" />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                ) : mode === 'roadmapList' ? (
                  <div>
                    <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                      {title.preTitle}
                    </Text>
                    <TextArea height={45} placeholder="กรอกลักษณะการทำงาน" />
                  </div>
                ) : mode === 'roadmapAdmin' ? (
                  <Grid columns="equal">
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Year
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Owner
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Department
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                          style={{ paddingBottom: 8 }}
                        >
                          Division
                        </Text>
                        <DropdownAll placeholder="Select" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Process (Level 3)
                        </Text>
                        <DropdownAll placeholder="Select Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Sub-Process (Level 4)
                        </Text>
                        <DropdownAll placeholder="Select Sub-Process" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          Remark
                        </Text>
                        <InputAll placeholder="Remark" />
                      </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                      <Grid.Column>
                        <Text
                          fontSize={sizes.xxs}
                          fontWeight="bold"
                          color={colors.primaryBlack}
                        >
                          IC Agent+
                        </Text>
                        <div>
                          <Grid columns="equal">
                            <Grid.Row>
                              <Grid.Column>
                                <Div center top={4}>
                                  <RadioBox label="YES" />
                                  <div style={{ paddingLeft: 24 }}>
                                    <RadioBox label="No" />
                                  </div>
                                </Div>
                              </Grid.Column>
                              <Grid.Column />
                            </Grid.Row>
                          </Grid>
                        </div>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                ) : (
                  <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Text fontSize={sizes.s} color={colors.textDarkBlack}>
                      {title.preTitle}
                    </Text>
                    <Text fontSize={sizes.xxs} color={colors.textDarkBlack}>
                      {title.postTitle}
                    </Text>
                  </div>
                )}
              </div>
            )}
            onClose={() => setActive(false)}
          />
        </Bottombar>
      )}
    </div>
  );
});

export default BottomBar;
