import React, { useState, useEffect } from 'react';
import {
  Image,
  Loader,
  Dimmer,
  Grid,
  Popup,
  Button,
  Header,
} from 'semantic-ui-react';
import Link from 'next/link';
// import Router, { useRouter } from "next/router";
import { observer } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import { ButtonAll, Text, ButtonBorder } from '../element';
import { colors, sizes } from '../../utils';
import { listProcessDatabase } from '../../utils/static';
import { withLayout } from '../../hoc';
import { initDataContext } from '../../contexts';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.blueBgLight};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;
const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 100px;
  display: inline-block;
`;

const DividerLine = styled.div`
  height: 2px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const ContentLine = styled.div`
  height: 1px;
  width: 100%;
  margin-left: 48px;
  background-color: #d9d9d6;
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  min-height: 50px;
  display: flex;
  flex-wrap: wrap;
  flexdirection: row;
  border-radius: 0px 0px 0px 0px !important;
  align-items: start;
  justify-content: space-between;
  // box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  border-bottom: 1px solid #d9d9d6;

  @media only screen and (min-width: 768px) {
    width: ${(props) => (props.span ? (props.span / 12) * 100 : '8.3')}%;
  }
`;
const TableBody = TableHeader.extend`
  padding: 10px 0px;
  border-radius: 0px !important;
  // border-top: 2px solid ${colors.primaryBlack};
  border-bottom: none;
`;

const TableContent = TableBody.extend`
  margin-top: 0px;
`;

const Cell = styled.div`
  width: ${(props) => props.width}%;
  padding: 10px;
  display: flex;
  min-height: 50px;
  align-items: ${(props) => (props.center ? 'center' : 'start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const headers = [
  { key: 'Process', render: 'Process', width: 25 },
  { key: 'Definition', render: 'Process Definition', width: 25 },
  { key: 'Value', render: 'Value', width: 25 },
  { key: 'Mapping', render: 'Source Mapping', width: 25 },
];

const PageAction = ({ titleLevelTwo }) => {
  // const { query } = useRouter();
  const [date, setDate] = useState();
  const dataContext = initDataContext();

  // useEffect(() => {
  //   onChangeCloseDate(state);
  // }, [query]);
  useEffect(() => {
    console.log('process list');

    return function cleanup() {
      dataContext.name = '';
      dataContext.roleId = [];
      dataContext.page = '';
    };
  }, []);
  //

  return (
    <CardAll style={{ marginTop: 16 }}>
      <CardTop>
        <Text fontSize={sizes.s} fontWeight="bold" color={colors.primary}>
          1.1 Develop and embed vision and strategy
        </Text>
      </CardTop>
      <CardBody>
        <TableHeader span={12} style={{ height: 50 }}>
          {headers.map(({ render, key, width }, index) => (
            <Cell
              key={key}
              width={width}
              center
              style={{ paddingLeft: index === 0 && 27 }}
            >
              <Div center>
                <Text
                  fontSize={sizes.xxs}
                  fontWeight="med"
                  color={colors.textlightGray}
                >
                  {render}
                </Text>
                {/* {index === 0 && (
                  <Image
                    src="../../static/images/iconDown-gray.png"
                    style={{ marginLeft: 8, width: 18, height: 18 }}
                  />
                )} */}
              </Div>
            </Cell>
          ))}
        </TableHeader>

        {listProcessDatabase &&
          listProcessDatabase.map((item, index) => (
            <div>
              <TableBody span={12} key={index} index={index}>
                {index !== 0 && <DividerLine />}
                <Cell width={25} style={{ paddingLeft: 27 }}>
                  <Div col>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                    >
                      {item.text}
                    </Text>
                    <Div>
                      <ButtonAll
                        radius
                        style={{ margin: '4px 4px 4px 0px' }}
                        textSize={sizes.xxs}
                        textWeight="med"
                        height={24}
                        text={`Process Type ${item.processType}`}
                        textColor={colors.textPurple}
                        color={colors.purpledark}
                      />
                      <ButtonAll
                        radius
                        style={{ margin: '4px 4px 4px 0px' }}
                        textSize={sizes.xxs}
                        textWeight="med"
                        height={24}
                        text={`Ref No. ${item.refNo}`}
                        textColor={colors.textPurple}
                        color={colors.purpledark}
                      />
                    </Div>
                  </Div>
                </Cell>
                <Cell width={25}>
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {item.definition}
                  </Text>
                </Cell>
                <Cell width={25}>
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {item.value}
                  </Text>
                </Cell>
                <Cell width={25}>
                  <Div col>
                    <Div col>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Process Name
                      </Text>
                      <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                        {item.processDefinition}
                      </Text>
                    </Div>
                    <Div col top={16}>
                      <Text
                        fontSize={sizes.xxs}
                        fontWeight="bold"
                        color={colors.primaryBlack}
                      >
                        Process Definition
                      </Text>
                      <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                        {item.processDefinition}
                      </Text>
                    </Div>
                  </Div>
                </Cell>
              </TableBody>
              {item.content.map((i) => (
                <div>
                  <TableContent span={12} key={index}>
                    <ContentLine />
                    <Cell width={25} style={{ paddingLeft: 48 }}>
                      <Div col>
                        <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                          {i.text}
                        </Text>
                        <Link
                          href={`/databasePage/riskAndControlManagment?id=${i.id}`}
                        >
                          <Div top={4} bottom={4} className="click">
                            <Image
                              src="../../static/images/zing@3x.png"
                              style={{
                                width: 18,
                                height: 18,
                                marginRight: 8,
                              }}
                            />
                            <Text fontSize={sizes.xxs} color={colors.textSky}>
                              Risk & Control
                            </Text>
                          </Div>
                        </Link>
                        <Div>
                          <ButtonAll
                            style={{ margin: '4px 4px 4px 0px' }}
                            radius
                            textSize={sizes.xxs}
                            textWeight="med"
                            height={24}
                            text={`Process Type ${i.processType}`}
                            textColor={colors.textPurple}
                            color={colors.purpledark}
                          />

                          <ButtonAll
                            radius
                            style={{ margin: '4px 4px 4px 0px' }}
                            textSize={sizes.xxs}
                            textWeight="med"
                            height={24}
                            text={`Grouping ${i.grouping}`}
                            textColor={colors.textPurple}
                            color={colors.purpledark}
                          />
                          <ButtonAll
                            radius
                            style={{ margin: '4px 4px 4px 0px' }}
                            textSize={sizes.xxs}
                            textWeight="med"
                            height={24}
                            text={`COSO ${i.cosoGroup}`}
                            textColor={colors.textPurple}
                            color={colors.purpledark}
                          />
                          <ButtonAll
                            radius
                            style={{ margin: '4px 4px 4px 0px' }}
                            textSize={sizes.xxs}
                            textWeight="med"
                            height={24}
                            text={`COSO NO. ${i.cosoNumber}`}
                            textColor={colors.textPurple}
                            color={colors.purpledark}
                          />
                        </Div>
                      </Div>
                    </Cell>
                    <Cell width={25}>
                      <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                        {i.definition}
                      </Text>
                    </Cell>
                    <Cell width={25}>
                      <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                        {item.value}
                      </Text>
                    </Cell>
                    <Cell width={25}>
                      <Div col>
                        <Div col>
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                          >
                            Process Name
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.processDefinition}
                          </Text>
                        </Div>
                        <Div col top={16}>
                          <Text
                            fontSize={sizes.xxs}
                            fontWeight="bold"
                            color={colors.primaryBlack}
                          >
                            Process Definition
                          </Text>
                          <Text
                            fontSize={sizes.xxs}
                            color={colors.primaryBlack}
                          >
                            {item.processDefinition}
                          </Text>
                        </Div>
                      </Div>
                    </Cell>
                  </TableContent>
                  {/* <DividerLine /> */}
                </div>
              ))}
            </div>
          ))}
      </CardBody>
    </CardAll>
  );
};

export default PageAction;
