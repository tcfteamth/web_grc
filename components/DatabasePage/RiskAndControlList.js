import React, { useState, useEffect } from 'react';
import { Grid, Pagination, Icon, Image } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import Link from 'next/link';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { initAuthStore } from '../../contexts';
import { Text, ModalGlobal, CardEmpty } from '../element';
import { colors, sizes } from '../../utils';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  margin-bottom: 24px;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  padding: 10px 0px;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  width: auto;
  height: 100%;
  padding: 10px 14px;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width}px;
  flex-wrap: wrap;
  padding-right: 24px;
  justify-content: ${(props) => (props.center ? 'center' : 'start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  background-color: ${colors.backgroundPrimary};
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const headers = [
  { key: 'Objective', render: 'Objective', width: 400 },
  { key: 'RiskNo', render: 'Risk No.', width: 150 },
  { key: 'RiskDetail', render: 'Risk Description', width: 350 },
  { key: 'ControlNo', render: 'Control No.', width: 150 },
  { key: 'Control', render: 'Control Description', width: 350 },
  { key: 'ExternalLaws', render: 'External Laws / Rules / Regulations', width: 250 },
  { key: 'ComplianceCore', render: 'Compliance Core Regulatory', width: 150 },
  { key: 'ElegalLibrary', render: 'E-legal Library No.', width: 150 },
  { key: 'ExternalSummary', render: 'External Law Summary', width: 350 },
  { key: 'InternalLaws', render: 'Internal Rules / Regulations', width: 250 },
  { key: 'InternalSummary', render: 'Internal Law Summary', width: 350 },
  { key: 'ControlType', render: 'Control Type', width: 250 },
  { key: 'Format', render: 'Control Classification', width: 250 },
  { key: 'Frequency', render: 'Frequency', width: 250 },
];

const OBJECT_TYPES = [
  {
    key: 'Operation',
    text: 'ด้านการดำเนินงาน (Operations)',
    value: 'ด้านการดำเนินงาน (Operations)',
  },
  {
    key: 'Compliance',
    text: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
    value: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
  },
  {
    key: 'Reporting',
    text: 'ด้านการรายงาน (Reporting)',
    value: 'ด้านการรายงาน (Reporting)',
  },
];

const RiskAndControlList = ({ token, riskAndControlModel }) => {
  const authContext = initAuthStore();
  const [handelDelete, setHandelDelete] = useState(false);
  const [deleteError, setDeleteError] = useState(false);
  const router = useRouter();

  const handleOnDelete = async () => {
    try {
      setHandelDelete(false);
      loadingStore.setIsLoading(true);
      await request.processDBServices.onDeleteRiskAndControlList(
        token,
        riskAndControlModel.deleteNo,
      );
      await riskAndControlModel.getRiskAndControlList(
        authContext.accessToken,
        riskAndControlModel.page,
      );

      riskAndControlModel.setField('searchText', '');
      riskAndControlModel.setField('lvl4', '');
      riskAndControlModel.setField('fullPath', '');
      riskAndControlModel.setField('objectType', '');

      loadingStore.setIsLoading(false);
      router.reload();
    } catch (e) {
      console.log(e);
      setDeleteError(true);
      loadingStore.setIsLoading(false);
    }
  };

  const renderType = (type) => {
    // const typeObj = OBJECT_TYPES.find((e) => e.key === type);
    return (
      <div style={{ paddingTop: 8 }}>
        <span
          style={{
            fontSize: sizes.xs,
            color: colors.textlightGray,
            fontWeight: 600,
          }}
        >
          ประเภท :
        </span>
        <span
          style={{
            fontSize: sizes.xs,
            paddingLeft: 4,
            color: colors.textDarkBlack,
            fontWeight: 600,
          }}
        >
          {type && type}
        </span>
      </div>
    );
  };

  const renderCardHeaderTable = () => (
    <Div left={24}>
      {headers.map(({ render, key, width }) => (
        <TableHeader key={key}>
          <Cell
            center={render === 'Risk No.' || render === 'Control No.'}
            width={width}
          >
            <Text
              fontSize={sizes.xs}
              fontWeight="med"
              color={colors.textlightGray}
            >
              {render}
            </Text>
          </Cell>
        </TableHeader>
      ))}
    </Div>
  );

  return useObserver(() => (
    <div>
      {riskAndControlModel.mode === 'list' &&
      riskAndControlModel.list.length > 0 ? (
        <>
          {riskAndControlModel.list.map((item, index) => (
            <CardAll className="w-highlight">
              <CardTop>
                <div>
                  <Text
                    fontSize={sizes.s}
                    fontWeight="bold"
                    color={colors.primaryBackground}
                  >
                    {`${item.no} ${item.name}`}
                  </Text>
                </div>

                {authContext.roles.isAdmin && (
                  <Div>
                    <div style={{ marginRight: 16 }}>
                      <Link
                        href={{
                          pathname: '/databasePage/editRiskAndControlManagment',
                          query: {
                            id: item.id,
                          },
                        }}
                      >
                        <a
                          className="click"
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            paddingLeft: 16,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <Image
                            width={22}
                            style={{ marginLeft: 3 }}
                            src="/static/images/edit-white@3x.png"
                          />
                          <Text
                            style={{ marginLeft: 8 }}
                            fontSize={sizes.s}
                            fontWeight="bold"
                            color={colors.primaryBackground}
                          >
                            Edit
                          </Text>
                        </a>
                      </Link>
                    </div>
                    {item.isDelete && (
                      <div
                        onClick={() => {
                          riskAndControlModel.setField('deleteNo', item.id);
                          setHandelDelete(true);
                        }}
                        className="click"
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          paddingLeft: 16,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <Image
                          width={22}
                          style={{ marginLeft: 3 }}
                          src="/static/images/delete-white@3x.png"
                        />
                        <Text
                          style={{ marginLeft: 8 }}
                          fontSize={sizes.s}
                          fontWeight="bold"
                          color={colors.primaryBackground}
                        >
                          Delete
                        </Text>
                      </div>
                    )}
                  </Div>
                )}
              </CardTop>
              <TableScroll>
                <TabScroll>
                  <Div col>
                    {renderCardHeaderTable()}
                    <DividerLine />
                    {item.children.map((lvlFive, index) => (
                      <TableBody key={index}>
                        <Div left={10}>
                          <Cell width={400}>
                            <Div col>
                              <div>
                                <Text
                                  fontSize={sizes.xs}
                                  color={colors.primaryBlack}
                                >
                                  {lvlFive.name}
                                </Text>
                              </div>
                              {renderType(lvlFive.type)}
                            </Div>
                          </Cell>
                          <Div col>
                            {lvlFive.children.map((lvlSix, lvlSixIndex) => (
                              <Div col>
                                <Div>
                                  <Cell center width={150}>
                                    <Text
                                      fontSize={sizes.xs}
                                      fontWeight="bold"
                                      color={colors.primaryBlack}
                                    >
                                      {`R${lvlSixIndex + 1}`}
                                    </Text>
                                  </Cell>
                                  <Cell width={350}>
                                    <Text
                                      fontSize={sizes.xs}
                                      color={colors.primaryBlack}
                                    >
                                      {lvlSix.name}
                                    </Text>
                                  </Cell>
                                  <Div col>
                                    {lvlSix.children.map(
                                      (lvlSeven, lvlSevenIndex) => (
                                        <Div
                                          style={{ padding: '0px 0px 24px ' }}
                                        >
                                          <Cell center width={150}>
                                            <Div col mid center>
                                              <Text
                                                fontSize={sizes.xs}
                                                fontWeight="bold"
                                                color={colors.primaryBlack}
                                              >
                                                {`C${lvlSevenIndex + 1}`}
                                              </Text>
                                              <Text
                                                fontSize={sizes.xs}
                                                color={colors.textlightGray}
                                              >
                                                {lvlSeven.isStandard
                                                  ? 'Standard'
                                                  : 'Optional'}
                                              </Text>
                                            </Div>
                                          </Cell>
                                          <Cell width={350}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {lvlSeven.name}
                                            </Text>
                                          </Cell>
                                          <Cell width={250}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {(lvlSix.externalLaws || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={150}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {(lvlSix.complianceCore || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={150}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {(lvlSix.elegalLibrary || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={350}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                              style={{ whiteSpace: 'pre-wrap' }}
                                            >
                                              {(lvlSix.externalLawSummary || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={250}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {(lvlSix.internalLaws || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={350}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                              style={{ whiteSpace: 'pre-wrap' }}
                                            >
                                              {(lvlSix.internalLawSummary || '-')}
                                            </Text>
                                          </Cell>
                                          <Cell width={250}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {lvlSeven.controlType
                                                ? lvlSeven.controlType
                                                : '-'}
                                            </Text>
                                          </Cell>
                                          <Cell width={250}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {lvlSeven.controlFormat
                                                ? lvlSeven.controlFormat
                                                : '-'}
                                            </Text>
                                          </Cell>
                                          <Cell width={250}>
                                            <Text
                                              fontSize={sizes.xs}
                                              color={colors.primaryBlack}
                                            >
                                              {lvlSeven.controlFrequency
                                                ? lvlSeven.controlFrequency
                                                : '-'}
                                            </Text>
                                          </Cell>
                                        </Div>
                                      ),
                                    )}
                                  </Div>
                                </Div>
                              </Div>
                            ))}
                          </Div>
                          <Cell />
                        </Div>
                      </TableBody>
                    ))}
                    <DividerLine bottom style={{ marginBottom: 16 }} />
                  </Div>
                </TabScroll>
              </TableScroll>
            </CardAll>
          ))}
        </>
      ) : (
        <>
          {riskAndControlModel.mode === 'list' && (
            <CardEmpty textTitle="No Risk and Control Library" />
          )}
        </>
      )}

      {riskAndControlModel.mode === 'noLvl' && riskAndControlModel && (
        <CardAll className="w-highlight">
          <CardTop>
            <Text
              fontSize={sizes.s}
              fontWeight="bold"
              color={colors.backgroundPrimary}
            >
              {riskAndControlModel.no} {riskAndControlModel.name}
            </Text>
          </CardTop>
          <TableScroll>
            <TabScroll>
              <Div col>
                {renderCardHeaderTable()}
                <DividerLine />
                {riskAndControlModel.list.map((lvlFive, index) => (
                  <TableBody key={index}>
                    <Div left={10}>
                      <Cell width={400}>
                        <Div col>
                          <div>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {lvlFive.name}
                            </Text>
                          </div>
                          {renderType(lvlFive.type)}
                        </Div>
                      </Cell>
                      <Div col>
                        {lvlFive.children.map((lvlSix, lvlSixIndex) => (
                          <Div col>
                            <Div>
                              <Cell center width={150}>
                                <Text
                                  fontSize={sizes.xs}
                                  fontWeight="bold"
                                  color={colors.primaryBlack}
                                >
                                  {`R${lvlSixIndex + 1}`}
                                </Text>
                              </Cell>
                              <Cell width={350}>
                                <Text
                                  fontSize={sizes.xs}
                                  color={colors.primaryBlack}
                                >
                                  {lvlSix.name}
                                </Text>
                              </Cell>
                              <Div col>
                                {lvlSix.children.map(
                                  (lvlSeven, lvlSevenIndex) => (
                                    <Div style={{ padding: '0px 0px 24px ' }}>
                                      <Cell center width={150}>
                                        <Div col mid center>
                                          <Text
                                            fontSize={sizes.xs}
                                            fontWeight="bold"
                                            color={colors.primaryBlack}
                                          >
                                            {`C${lvlSevenIndex + 1}`}
                                          </Text>
                                          <Text
                                                fontSize={sizes.xs}
                                                color={colors.textlightGray}
                                              >
                                                {lvlSeven.isStandard
                                                  ? 'Standard'
                                                  : 'Optional'}
                                              </Text>
                                          <Text
                                            fontSize={sizes.xs}
                                            color={colors.textlightGray}
                                          >
                                            {lvlSeven.controlType}
                                          </Text>
                                        </Div>
                                      </Cell>
                                      <Cell width={350}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {lvlSeven.name}
                                        </Text>
                                      </Cell>
                                      <Cell width={250}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {(lvlSix.externalLaws || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={150}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {(lvlSix.complianceCore || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={150}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {(lvlSix.elegalLibrary || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={350}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                          style={{ whiteSpace: 'pre-wrap' }}
                                        >
                                          {(lvlSix.externalLawSummary || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={250}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {(lvlSix.internalLaws || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={350}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                          style={{ whiteSpace: 'pre-wrap' }}
                                        >
                                          {(lvlSix.internalLawSummary || '-')}
                                        </Text>
                                      </Cell>
                                      <Cell width={250}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {lvlSeven.controlType
                                            ? lvlSeven.controlType
                                            : '-'}
                                        </Text>
                                      </Cell>
                                      <Cell width={250}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {lvlSeven.controlFormat
                                            ? lvlSeven.controlFormat
                                            : '-'}
                                        </Text>
                                      </Cell>
                                      <Cell width={250}>
                                        <Text
                                          fontSize={sizes.xs}
                                          color={colors.primaryBlack}
                                        >
                                          {lvlSeven.controlFrequency
                                            ? lvlSeven.controlFrequency
                                            : '-'}
                                        </Text>
                                      </Cell>
                                    </Div>
                                  ),
                                )}
                              </Div>
                            </Div>
                          </Div>
                        ))}
                      </Div>
                      <Cell />
                    </Div>
                  </TableBody>
                ))}

                <DividerLine bottom style={{ marginBottom: 16 }} />
              </Div>
            </TabScroll>
          </TableScroll>
        </CardAll>
      )}

      {riskAndControlModel.totalPage > 0 && (
        <Grid style={{ marginTop: 16 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={riskAndControlModel.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) => {
                riskAndControlModel.setField('page', activePage);
                riskAndControlModel.getRiskAndControlList(
                  authContext.accessToken,
                  activePage,
                );
              }}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={riskAndControlModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete risk and its control ?"
        onClose={() => setHandelDelete(false)}
        onSubmit={() => {
          handleOnDelete();
        }}
        submitText="YES"
        cancelText="NO"
      />
      {/* confrim delete */}
      <ModalGlobal
        open={deleteError}
        title="Error"
        content="คุณไม่สามารถลบ Process นี้ได้"
        onClose={() => setDeleteError(false)}
        onSubmit={() => {
          setDeleteError(false);
        }}
        submitText="YES"
        cancelText="NO"
      />
    </div>
  ));
};

export default RiskAndControlList;
