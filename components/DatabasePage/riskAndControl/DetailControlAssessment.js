import React, { useState } from 'react';
import { Image, Grid, Table } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonFile, InputDropdown, InputAll } from '../../element';
import styled from 'styled-components';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = () => {
  return (
    <div style={{ width: '100%' }}>
      <Grid style={{ margin: 0 }}>
        <Grid.Row style={{ padding: 0 }}>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Standard / Optional
                    </Text>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      เอกสารอ้างอิง
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <InputDropdown placeholder="Select" />
                </Table.Cell>
                <Table.Cell>
                  <InputDropdown placeholder="Select" />
                </Table.Cell>
                <Table.Cell>
                  <InputDropdown placeholder="Select" />
                </Table.Cell>
                <Table.Cell>
                  <InputDropdown placeholder="Select" />
                </Table.Cell>
                <Table.Cell>
                  <InputAll placeholder="d" />
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>

        <Grid.Row
          style={{ display: 'flex', alignItems: 'center', paddingBottom: 0 }}
        >
          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
            Document / Evidence
          </Text>
          <div style={{ paddingLeft: 16 }}>
            <ButtonFile
              height={32}
              textSize={sizes.xs}
              border
              text={'Attach file'}
            />
          </div>
        </Grid.Row>
      </Grid>
    </div>
  );
};
export default index;
