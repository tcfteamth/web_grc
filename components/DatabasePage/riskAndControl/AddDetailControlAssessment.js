import React, { useState } from 'react';
import { Image, Grid, Table } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonBorder, ButtonFile, TextArea } from '../../element';

const index = () => {
  return (
    <div style={{ width: '100%' }}>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Row style={{ padding: 0 }}>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </div>
                </Table.HeaderCell>
                <Table.HeaderCell>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification
                    </Text>
                    <Image
                      width={18}
                      style={{ marginLeft: 8 }}
                      src="../../static/images/iconRemark@3x.png"
                    />
                  </div>
                </Table.HeaderCell>
                <Table.HeaderCell>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Frequency
                  </Text>
                </Table.HeaderCell>
                <Table.HeaderCell>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Information technology system
                  </Text>
                </Table.HeaderCell>
                <Table.HeaderCell>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Executor
                  </Text>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <TextArea
                    height={45}
                    placeholder={'การควบคุมภายในแบบป้องกัน(Preventive control)'}
                  />
                </Table.Cell>
                <Table.Cell>
                  <TextArea
                    height={45}
                    placeholder={'การควบคุมด้วยมือ (Manual Control)'}
                  />
                </Table.Cell>
                <Table.Cell>
                  <TextArea height={45} placeholder={'รายปี'} />
                </Table.Cell>
                <Table.Cell>
                  <TextArea height={45} placeholder={'-'} />
                </Table.Cell>
                <Table.Cell>
                  <TextArea height={45} placeholder={'Process Owner'} />
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>
        <Grid.Row
          style={{ display: 'flex', alignItems: 'center', paddingBottom: 0 }}
        >
          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
            Document / Evidence
          </Text>
          <div style={{ display: 'flex', paddingLeft: 12 }}>
            <ButtonBorder
              style={{ margin: '0px 4px' }}
              radius
              height={32}
              width={102}
              textColor={colors.textSky}
              borderColor={colors.textGrayLight}
              text={'CSA - 56-1'}
            />
            <ButtonFile
              height={32}
              textSize={sizes.xs}
              border
              text={'Attach file'}
            />
          </div>
        </Grid.Row>
      </Grid>
    </div>
  );
};
export default index;
