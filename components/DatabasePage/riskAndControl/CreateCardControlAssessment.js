import React, { useState } from 'react';
import { useObserver } from 'mobx-react';
import { Image, Grid, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../../utils';
import {
  Text,
  InputAll,
  ButtonAll,
  CheckBoxAll,
  TextArea,
  ModalGlobal,
  Box,
} from '../../element';
// import { CreateDetailControlAssessment } from '..';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({
  item,
  mapping,
  edit,
  risk,
  control,
  assessmentDetailModel,
}) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isRating, setIsRating] = useState();
  const [isRatingDefalue, setIsRatingDefalue] = useState('');
  const [handelDelete, setHandelDelete] = useState(false);

  const handelRating = (value) => {
    control.setRate(value);
    console.log(value);
  };

  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Column computer={2} style={{ paddingTop: 0 }}>
          <Div col>
            <Text color={colors.pink} fontSize={sizes.s}>
              Control
            </Text>
            <Text fontSize={sizes.s} color={colors.pink} fontWeight="bold">
              {`C${control.no}`}{' '}
              <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
            </Text>
          </Div>
        </Grid.Column>
        <Grid.Column computer={8} style={{ padding: 0 }}>
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <TextArea
              placeholder="Control Description"
              onChange={(e) => control.setField('nameTh', e.target.value)}
              error={control.validateFormModel.validateNameTh}
              value={control.nameTh}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                paddingLeft: 16,
                marginTop: -4,
                justifyContent: 'center',
              }}
            />
          </div>
        </Grid.Column>
        <Grid.Column computer={6} style={{ padding: 0 }}>
          <Grid.Row>
            <Div between>
              <div
                style={{
                  backgroundColor: colors.primaryBackground,
                  width: '1px',
                  minHeight: '80px',
                  margin: '0px 16px',
                }}
              />
              <Div col>
                <Div center>
                  <Text
                    fontSize={sizes.xs}
                    color={colors.textDarkBlack}
                    fontWeight="med"
                  >
                    Control Rating{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </Text>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginRight: -10,
                    }}
                    position="top right"
                    trigger={
                      <Image
                        width={18}
                        style={{ marginLeft: 8 }}
                        src="../../static/images/iconRemark@3x.png"
                      />
                    }
                  >
                    <Grid
                      columns="equal"
                      style={{ margin: 0, paddingRight: 16 }}
                    >
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Good
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - The control is appropriately designed and being
                            implemented as specified
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Fair
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Box>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - The control is designed but may not be adequate
                              and appropriate
                            </Text>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              - The controls have been designed adequately but
                              not consistently or only partially effective
                            </Text>
                          </Box>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Poor
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - There is no controls assignedd for the risks
                            involved.
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4}>
                          <Text
                            fontSize={sizes.xs}
                            fontWeight="med"
                            color={colors.primaryBlack}
                          >
                            Enahancement
                          </Text>
                        </Grid.Column>
                        <Grid.Column>
                          <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                            - Improve the control that already appropriately
                            designed and being implemented as specified to
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              be more effective{' '}
                            </span>
                            or
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              more efficiency{' '}
                            </span>
                          </Text>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Popup>
                </Div>
                <Div style={{ display: 'flex', flexWrap: 'wrap' }}>
                  <ButtonAll
                    style={{ margin: 2 }}
                    width={88}
                    height={24}
                    textSize={sizes.xs}
                    textWeight="bold"
                    color={control.rateId === 3 ? colors.green : colors.btGray}
                    text="Good"
                    onClick={() => control.setRates(3)}
                    error={control.validateFormModel.validateRateId}
                  />
                  <ButtonAll
                    style={{ margin: 2 }}
                    width={88}
                    height={24}
                    textSize={sizes.xs}
                    textWeight="bold"
                    color={control.rateId === 2 ? colors.yellow : colors.btGray}
                    text="Fair"
                    onClick={() => control.setRates(2)}
                    error={control.validateFormModel.validateRateId}
                  />
                  <ButtonAll
                    style={{ margin: 2 }}
                    width={88}
                    height={24}
                    textSize={sizes.xs}
                    textWeight="bold"
                    color={control.rateId === 1 ? colors.red : colors.btGray}
                    text="Poor"
                    onClick={() => control.setRates(1)}
                    error={control.validateFormModel.validateRateId}
                  />
                </Div>
                <Div top={8}>
                  <Div>
                    <CheckBoxAll
                      disabled={control.canCheckImprovement}
                      onChange={() =>
                        control.setField('isEnhance', !control.isEnhance)
                      }
                      checked={control.isEnhance}
                    />
                    <Text
                      fontSize={sizes.xs}
                      color={colors.textBlack}
                      fontWeight="med"
                      style={{ paddingLeft: 16 }}
                    >
                      Enhancement
                    </Text>
                    <div>
                      <Text
                        fontSize={sizes.xs}
                        color={colors.textSky}
                        style={{ paddingLeft: 90 }}
                      >
                        Last Year
                      </Text>
                    </div>
                  </Div>
                </Div>
              </Div>
              <div
                className="click"
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                }}
              >
                <div onClick={() => handelOpen(!isOpen)}>
                  {isOpen ? (
                    <Image width={24} src="/static/images/iconUp-blue@3x.png" />
                  ) : (
                    <Image
                      width={24}
                      src="/static/images/iconDown-blue@3x.png"
                    />
                  )}
                </div>
                {!control.isStandard && (
                  <div onClick={() => setHandelDelete(true)}>
                    <Image
                      height={18}
                      width={18}
                      style={{ marginLeft: 2 }}
                      src="/static/images/delete@2x.png"
                    />
                  </div>
                )}
              </div>
            </Div>
          </Grid.Row>
        </Grid.Column>
      </Grid>
      <Grid.Row style={{ padding: '24px 0px 0px' }}>
        {/* <CreateDetailControlAssessment
          control={control}
          assessmentDetailModel={assessmentDetailModel}
        /> */}
      </Grid.Row>

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete control?"
        onSubmit={() => {
          risk.deleteControl(control.fullPath);
          setHandelDelete(false);
        }}
        submitText="YES"
        cancelText="NO"
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default index;
