import React, { useState } from 'react';
import { Image, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonAll,
  InputAll,
  InputGroup,
  TextArea,
  ModalGlobal,
} from '../../element';
import { CreateCardControlAssessment, CreateNewRiskAssessment } from '..';
// import {
//   MappingModal,
//   LibraryRiskModal,
//   LibraryControlModal,
// } from "../../AssessmentPage/modal";
import { initAuthStore } from '../../../contexts';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  padding-left: ${(props) => props.left || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ riskAndControlModel, newRisk, risk, objects }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);

  const authContext = initAuthStore();

  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handelAdd = (value) => {
    setIsAdd(value);
  };
  return useObserver(() => (
    <div>
      <Grid columns="equal" style={{ margin: 0, padding: '24px' }}>
        <Grid.Column computer={2} style={{ paddingTop: 0 }}>
          <Div center>
            <Text fontSize={sizes.m} color={colors.primary}>
              <span>Risk</span>{' '}
              <span
                style={{
                  paddingLeft: 4,
                  fontWeight: 'bold',
                  marginRight: '16px',
                }}
              >
                {/* {`R${risk.no}`}{' '} */}
                <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
              </span>
            </Text>
          </Div>
        </Grid.Column>
        <Grid.Column computer={14} style={{ padding: 0 }}>
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <TextArea
              placeholder="Risk Description"
              // onChange={(e) => risk.setField('nameTh', e.target.value)}
              // value={risk.nameTh}
              // error={risk.validateRiskFormModel.validateRiskName}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                paddingLeft: 16,
                justifyContent: 'flex-start',
              }}
              onClick={() => {
                setHandelDelete(true);
              }}
              className="click"
            >
              <Image
                width={18}
                style={{ marginLeft: 3 }}
                src="/static/images/delete@2x.png"
              />
            </div>
          </div>
        </Grid.Column>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {console.log('riskAndControlModel', riskAndControlModel)}
          {/* {risk.controls.map((control) => (
            <div style={{ width: '100%' }}>
              <CreateCardControlAssessment
                // risk={risk}
                control={control}
                // assessmentDetailModel={assessmentDetailModel}
              />
              <Divider />
            </div>
          ))} */}
          <Div mid>
            <ButtonAll
              textSize={sizes.xs}
              textColor={colors.pink}
              color="#FFFFFF"
              text="ADD CONTROL"
              icon="/static/images/iconPlus-pink@3x.png"
              // onClick={() => handelAdd(!isAdd)}
              onClick={() => risk.addControl()}
            />
          </Div>
        </Grid.Row>
      </Grid>
      <DividerLine />

      {newRisk && <CreateNewRiskAssessment />}

      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete risk and its control?"
        onSubmit={() => {
          objects.deleteRisk(risk.fullPath);
          setHandelDelete(false);
        }}
        submitText="YES"
        cancelText="NO"
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default index;
