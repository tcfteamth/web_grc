import React, { useState } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react';
import { colors, sizes } from '../../../utils';
import { Text, ButtonAll, TextArea, ModalGlobal, InputAll } from '../../element';
import { CardControlAssessment, AddCardControlAssessment } from '..';
// import { MappingModal, LibraryRiskModal, LibraryControlModal } from '../modal';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ objects, risk, RCModel }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);

  return useObserver(() => (
    <div>
      <Grid columns="equal" style={{ margin: 0, padding: 24 }}>
        <Grid.Column tablet={6} computer={2} style={{ paddingTop: 0 }}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column tablet={6} computer={6}>
                <Text fontSize={sizes.m} color={colors.primary}>
                  Risk
                </Text>
              </Grid.Column>
              <Grid.Column tablet={6} computer={10}>
                <Text fontSize={sizes.m} color={colors.primary}>
                  <span
                    style={{
                      paddingLeft: 8,
                      fontWeight: 'bold',
                    }}
                  >
                    {`R${risk.no}`}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </span>
                </Text>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>

        <Grid.Column
          computer={14}
          style={{
            padding: 0,
            width: '90%',
            wordWrap: 'break-word',
          }}
        >
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <div
              style={{ display: 'flex', flexDirection: 'row', width: '98%' }}
            >
              <TextArea
                placeholder="Risk Description"
                value={risk.nameTh}
                onChange={(e) => {
                  risk.setField('nameTh', e.target.value);
                  risk.setField('nameEn', e.target.value);
                }}
                error={risk.validateRiskFormModel.validateRiskName}
              />
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
              className="click"
              onClick={() => setHandelDelete(true)}
            >
              <Image
                src="../../static/images/delete-gray@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginLeft: 16,
                }}
              />
            </div>
          </div>
        </Grid.Column>
        <Grid.Row style={{ padding: 0 }}>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={2}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    External Laws / Rules / Regulations
                  </Text>
                </Grid.Column>
                  <InputAll
                    placeholder="External Laws / Rules / Regulations"
                    handleOnChange={(e) => {
                      risk.setField('externalLaws', e.target.value);
                    }}
                    value={risk.externalLaws}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={2}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Compliance Core Regulatory
                  </Text>
                </Grid.Column>
                  <InputAll
                    placeholder="Compliance Core Regulatory"
                    handleOnChange={(e) => {
                      risk.setField('complianceCore', e.target.value);
                    }}
                    value={risk.complianceCore}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row style={{ padding: 0 }}>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={3}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    E-legal Library No.
                  </Text>
                </Grid.Column>
                  <InputAll
                    placeholder="E-legal Library No."
                    handleOnChange={(e) => {
                      risk.setField('elegalLibrary', e.target.value);
                    }}
                    value={risk.elegalLibrary}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={3}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    External Law Summary
                  </Text>
                </Grid.Column>
                  <TextArea
                    placeholder="External Law Summary"
                    maxLength={4000}
                    onChange={(e) => {
                      risk.setField('externalLawSummary', e.target.value);
                    }}
                    value={risk.externalLawSummary}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row style={{ padding: 0 }}>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={3}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Internal Rules / Regulations
                  </Text>
                </Grid.Column>
                  <InputAll
                    placeholder="Internal Rules / Regulations"
                    handleOnChange={(e) => {
                      risk.setField('internalLaws', e.target.value);
                    }}
                    value={risk.internalLaws}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
          <Grid.Column
            width={8}
            style={{ padding: 10 }}
          >
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <Grid.Column
                  width={3}
                  style={{ paddingRight: 5 , width: '30%' }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    Internal Law Summary
                  </Text>
                </Grid.Column>
                  <TextArea
                    placeholder="Internal Law Summary"
                    maxLength={4000}
                    onChange={(e) => {
                      risk.setField('internalLawSummary', e.target.value);
                    }}
                    value={risk.internalLawSummary}
                    // error={control.validateFormModel.validateNameTh}
                  />
              </div>
          </Grid.Column>
        </Grid.Row>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {risk.controls.map((control, index) => (
            <div style={{ width: '100%' }}>
              {index !== 0 && <Divider bgcolor={colors.primaryBackground} />}
              {!control.id ? (
                <div style={{ width: '100%' }}>
                  <AddCardControlAssessment
                    control={control}
                    RCModel={RCModel}
                    risk={risk}
                    no={index}
                  />
                </div>
              ) : (
                <CardControlAssessment
                  control={control}
                  RCModel={RCModel}
                  risk={risk}
                  isEdit={isEdit}
                />
              )}
            </div>
          ))}
        </Grid.Row>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          <Div mid>
            <ButtonAll
              textSize={sizes.xs}
              textColor={colors.pink}
              color="#FFFFFF"
              text="ADD CONTROL"
              icon="/static/images/iconPlus-pink@3x.png"
              onClick={() => risk.addControl()}
            />
          </Div>
        </Grid.Row>
      </Grid>
      <DividerLine />

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete risk and its control?"
        onClose={() => setHandelDelete(false)}
        onSubmit={() => {
          objects.deleteRisk(risk.fullPath);
          setHandelDelete(false);
        }}
        submitText="YES"
        cancelText="NO"
      />
    </div>
  ));
};

export default index;
