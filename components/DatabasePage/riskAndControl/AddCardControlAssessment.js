import React, { useState } from 'react';
import { useObserver } from 'mobx-react';
import styled from 'styled-components';
import { Image, Grid, Popup } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import { Text, InputAll, TextArea, ModalGlobal } from '../../element';
import { CreateDetailControlAssessment } from '..';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? `space-between` : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ control, RCModel, risk, no }) => {
  const [handelDelete, setHandelDelete] = useState(false);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <Grid columns="equal" style={{ margin: 0 }}>
        <Grid.Column tablet={6} computer={2} style={{ paddingTop: 0 }}>
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column tablet={6} computer={6}>
                <Div center>
                  <Text
                    style={{ width: '40%' }}
                    fontSize={sizes.m}
                    color={colors.pink}
                  >
                    Control
                  </Text>
                </Div>
              </Grid.Column>
              <Grid.Column tablet={6} computer={10}>
                <Text fontSize={sizes.m} color={colors.pink}>
                  <span
                    style={{
                      paddingLeft: 8,
                      fontWeight: 'bold',
                    }}
                  >
                    {`C${control.no}`}{' '}
                    <span style={{ fontSize: sizes.m, color: colors.red }}>
                      *
                    </span>
                  </span>
                </Text>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
        <Grid.Column
          mobile={16}
          tablet={8}
          computer={14}
          style={{ padding: 0 }}
        >
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <div
              style={{ display: 'flex', flexDirection: 'row', width: '98%' }}
            >
              <TextArea
                placeholder="Control Description"
                onChange={(e) => {
                  control.setField('nameTh', e.target.value);
                  control.setField('nameEn', e.target.value);
                }}
                value={control.nameTh}
                error={control.validateFormModel.validateNameTh}
              />
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
              className="click"
              onClick={() => setHandelDelete(true)}
            >
              <Image
                src="../../static/images/delete-gray@3x.png"
                style={{
                  width: 24,
                  height: 24,
                  marginLeft: 16,
                }}
              />
            </div>
          </div>
        </Grid.Column>
      </Grid>
      <Grid.Row style={{ padding: '24px 0px 0px' }}>
        <CreateDetailControlAssessment control={control} RCModel={RCModel} />
      </Grid.Row>

      {/* confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete control?"
        onClose={() => setHandelDelete(false)}
        onSubmit={() => {
          risk.deleteControl(control.fullPath);
          setHandelDelete(false);
        }}
        submitText="YES"
        cancelText="NO"
      />
    </div>
  ));
};

export default index;
