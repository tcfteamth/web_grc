import React, { useState } from 'react';
import { Image, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { colors, sizes } from '../../../utils';
import { Text, ButtonAll, InputAll } from '../../element';
// import {
//   CardCreateControlAssessment,
//   CreateCardControlAssessment,
// } from '../PageCreateAssessment';
// import {
//   MappingModal,
//   LibraryRiskModal,
//   LibraryControlModal,
// } from "../../AssessmentPage/modal";
import { initAuthStore } from '../../../contexts';

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const DividerLine = styled.div`
  height: 4px;
  width: 100%;
  background-color: ${colors.primaryBlack};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  padding-left: ${(props) => props.left || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const index = ({ detail, newRisk }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [mappingModal, setMappingModal] = useState(false);
  const [libraryRisk, setLibraryRisk] = useState(false);
  const [libraryControl, setLibraryControl] = useState(false);
  const authContext = initAuthStore();

  const handelEdit = (value) => {
    setIsEdit(value);
  };
  const handelAdd = (value) => {
    setIsAdd(value);
  };
  return (
    <div>
      <Grid columns="equal" style={{ margin: 0, padding: '24px 40px' }}>
        <Grid.Column tablet={6} computer={3} style={{ paddingTop: 0 }}>
          <Div center>
            <Text
              style={{ width: '40%' }}
              fontSize={sizes.s}
              color={colors.primary}
            >
              Risk
            </Text>
            <div style={{ width: '60%' }}>
              <InputAll style={{ marginLeft: 8 }} placeholder="Risk..." />
            </div>
          </Div>
        </Grid.Column>
        <Grid.Column tablet={10} computer={13} style={{ padding: 0 }}>
          <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
            <InputAll placeholder="Control Description" />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                paddingLeft: 16,
                marginTop: -4,
                justifyContent: 'center',
              }}
            >
              <Image
                width={18}
                style={{ marginLeft: 3 }}
                src="/static/images/delete@2x.png"
              />
            </div>
          </div>
        </Grid.Column>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {/* <CardCreateControlAssessment edit={isEdit} /> */}
        </Grid.Row>
        <Divider />
        <Grid.Row style={{ padding: 0 }}>
          {isAdd && (
            <div style={{ width: '100%' }}>
              {/* <CreateCardControlAssessment /> */}
              <Divider />
            </div>
          )}
          <Div mid>
            <ButtonAll
              textSize={sizes.xs}
              textColor={colors.pink}
              color="#FFFFFF"
              text="ADD CONTROL"
              icon="/static/images/iconPlus-pink@3x.png"
              onClick={() => handelAdd(!isAdd)}
            />
          </Div>
        </Grid.Row>
      </Grid>
      <DividerLine />
      {/* <MappingModal
        active={mappingModal}
        onClose={() => setMappingModal(false)}
      />
      <LibraryRiskModal
        active={libraryRisk}
        onClose={() => setLibraryRisk(false)}
      />

      <LibraryControlModal
        active={libraryControl}
        onClose={() => setLibraryControl(false)}
      /> */}
    </div>
  );
};

export default index;
