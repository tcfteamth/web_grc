import React, { useState, useEffect } from 'react';
import { Image, Grid, Table, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import Files from 'react-files';
import { colors, sizes } from '../../../utils';
import {
  Text,
  ButtonAdd,
  InputDropdown,
  InputAll,
  NewDates,
  Box,
  TextArea,
  RadioBox,
} from '../../element';
import {
  CONTROL_TYPE_OPTIONS,
  CONTROL_FORMAT_OPTIONS,
  CONTROL_FREQUENCY_OPTIONS,
} from '../../../utils/assessment/controlOptions';
import { AttachFilesModal } from '../../AssessmentPage/modal';
import { BENEFIT_OPTIONS } from '../../../utils/assessment/benefitOptions';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  padding-top: ${(props) => props.top || 0}px;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) =>
    props.mid ? 'center' : props.between ? 'space-between' : 'flex-start'};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${(props) => props.bgcolor || colors.primary};
  margin: 24px 0px;
`;

const customStyle = {
  styleAddFile: {
    height: 30,
    minWidth: 102,
    color: colors.textPurple,
    borderRadius: `6px 6px 0px 6px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: sizes.xs,
    border: `2px solid ${colors.textPurple}`,
  },
};

const index = ({ control, RCModel }) => {
  const [screenWidth, setScreenWidth] = useState();

  useEffect(() => {
    console.log('RCModel >>', RCModel);
    console.log('control >>', control);
    setScreenWidth(window.screen.width);
  }, []);

  return useObserver(() => (
    <div>
      <Grid style={{ margin: 0 }}>
        <Grid.Row>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Type{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>

                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 16,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Grid
                        columns="equal"
                        style={{ margin: 0, paddingRight: 16 }}
                      >
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Preventive control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              Preventive control - attempt to defer or stop an
                              unwanted outcome before it happens (e.g., use of
                              passwords, approval, policies, procedures).
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Detective control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                Detective control - attempt to detect errors or
                                irregularities that may have already occurred
                                (e.g., reconciliations, monitoring of actual
                                expenses vs budget, prior periods, forecasts).
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3} style={{ padding: 8 }}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Control Classification
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                    <Popup
                      wide={screenWidth >= 1024 && 'very'}
                      style={{
                        borderRadius: '6px 6px 0px 6px',
                        padding: 8,
                        marginLeft: -10,
                      }}
                      position="top left"
                      trigger={
                        <Image
                          width={18}
                          style={{ marginLeft: 8 }}
                          src="../../static/images/iconRemark@3x.png"
                        />
                      }
                    >
                      <Grid
                        columns="equal"
                        style={{ margin: 0, paddingRight: 16 }}
                      >
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Manual Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              Manual controls are performed by individuals.
                              Outside of a system (e.g., approval, review,
                              segregration of duties).
                            </Text>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              Automate Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                Automated control are performed entirely by the
                                computer system (e.g., access right, credit
                                limit in in processing system).
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column computer={3} tablet={16}>
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="med"
                              color={colors.primaryBlack}
                            >
                              IT Dependent Control
                            </Text>
                          </Grid.Column>
                          <Grid.Column computer={13} tablet={16}>
                            <Box>
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                IT Dependent Control are performed by
                                individuals and requires some level of system
                                involvement.
                              </Text>
                            </Box>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Popup>
                  </Div>
                </Table.HeaderCell>
                <Table.HeaderCell width={3}>
                  <Div mid center>
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      Frequency{' '}
                      <span style={{ fontSize: sizes.m, color: colors.red }}>
                        *
                      </span>
                    </Text>
                  </Div>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Control Type"
                    options={CONTROL_TYPE_OPTIONS}
                    handleOnChange={(e, { value }) => {
                      control.setField('type', value);
                    }}
                    textDefault={control.type || null}
                    value={control.type}
                    onError={control.validateFormModel.validateType}
                  />
                </Table.Cell>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Control Classification"
                    options={CONTROL_FORMAT_OPTIONS}
                    handleOnChange={(e, { value }) => {
                      control.setField('format', value);

                      if (value === 'Manual Control') {
                        control.setField('techControl', '-');
                      } else {
                        control.setField('techControl', '');
                      }
                    }}
                    textDefault={control.format || null}
                    value={control.format}
                    onError={control.validateFormModel.validateFormat}
                  />
                </Table.Cell>
                <Table.Cell width={3}>
                  <InputDropdown
                    placeholder="Select Frequency"
                    options={CONTROL_FREQUENCY_OPTIONS}
                    handleOnChange={(e, { value }) => {
                      control.setField('frequency', value);
                    }}
                    textDefault={control.frequency || null}
                    value={control.frequency}
                    onError={control.validateFormModel.validateFrequency}
                  />
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Row>

        {/* <Divider /> */}

        <Grid.Row style={{ padding: 0, marginTop: 24 }}>
          <Grid.Column computer={2} style={{ padding: 0 }}>
            <Text fontSize={sizes.xs} color={colors.primaryBlack}>
              Standard
              <span style={{ fontSize: sizes.m, color: colors.red }}>*</span>
            </Text>
          </Grid.Column>
          <Grid.Column
            computer={14}
            style={{ padding: 0, display: 'flex', alignItems: 'center' }}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <RadioBox
                onChange={() => control.setField('isStandard', true)}
                label="Yes"
                checked={control.isStandard === true}
              />
            </div>
            <div style={{ marginLeft: 16 }}>
              <RadioBox
                onChange={() => control.setField('isStandard', false)}
                label="No"
                checked={control.isStandard === false}
              />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  ));
};

export default index;
