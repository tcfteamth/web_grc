import React, { useState, useEffect } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import Link from 'next/link';
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _ from 'lodash';
import {
  Text,
  InputAll,
  InputDropdown,
  ButtonBorder,
  ModalGlobal,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { CardRiskAssessment } from '..';

const CardAll = styled.div`
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  display: inline-block;
  border-radius: 6px 6px 0px 6px !important;
  margin-bottom: 24px;
  width: 100%;
`;

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  min-height: 110px;
  padding: 24px;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const CardBody = styled.div`
  border-radius: 0px 0px 0px 6px !important;
  background-color: ${colors.backgroundPrimary};
  min-height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const DividerLine = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? '#333333' : '#f6f6f6')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

export const OBJECT_TYPES = [
  {
    key: 'Operation',
    text: 'ด้านการดำเนินงาน (Operations)',
    value: 'ด้านการดำเนินงาน (Operations)',
  },
  {
    key: 'Compliance',
    text: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
    value: 'ด้านการปฏิบัติตามกฎ ระเบียบและข้อบังคับที่เกี่ยวข้อง (Compliance)',
  },
  {
    key: 'Reporting',
    text: 'ด้านการรายงาน (Reporting)',
    value: 'ด้านการรายงาน (Reporting)',
  },
];

const RiskAndControlList = ({ objects, objectIndex, RCModel }) => {
  const [isNewRisk, setIsNewRisk] = useState(false);
  const [handelDelete, setHandelDelete] = useState(false);
  const handleAddNewRisk = (value) => {
    setIsNewRisk(value);
  };

  const getTypeList = async () => {
    const typeObj = OBJECT_TYPES.find(
      (e) => e.value === objects.type || e.key === objects.type,
    );
    if (typeObj) {
      objects.setField('type', typeObj.value);
    }
  };

  useEffect(() => {
    if (RCModel.canUpdate) {
      getTypeList();
    }
  }, [RCModel, objects]);

  return useObserver(() => (
    <div style={{ paddingTop: 16 }}>
      <CardAll>
        <CardTop>
          <Grid>
            <Grid.Row>
              <Grid.Column computer={2}>
                <Div center>
                  <Image
                    src="../../static/images/iconObjective-white@3x.png"
                    style={{
                      width: 24,
                      height: 24,
                      marginRight: 8,
                    }}
                  />
                  <Text
                    color={colors.backgroundPrimary}
                    fontWeight="bold"
                    fontSize={sizes.s}
                    style={{ marginRight: 8 }}
                  >
                    Objective:
                  </Text>
                </Div>
              </Grid.Column>
              <Grid.Column computer={14}>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    width: '100%',
                  }}
                >
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      width: '98%',
                    }}
                  >
                    <InputAll
                      placeholder="Objective Description"
                      handleOnChange={(e) => {
                        objects.setField('nameTh', e.target.value);
                        objects.setField('nameEn', e.target.value);
                      }}
                      value={objects.nameTh}
                      error={objects.validateObjectFormModel.validateNameTh}
                    />
                  </div>

                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                    }}
                    className="click"
                    onClick={() => setHandelDelete(true)}
                  >
                    <Image
                      src="../../static/images/delete-white@3x.png"
                      style={{
                        width: 24,
                        height: 24,
                        marginLeft: 16,
                      }}
                    />
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{ paddingTop: 0 }}>
              <Grid.Column computer={2}>
                <Div top={10} center>
                  <Image
                    src="../../static/images/iconList-white@3x.png"
                    style={{
                      width: 24,
                      height: 24,
                      marginRight: 8,
                    }}
                  />
                  <Text
                    color={colors.primaryBackground}
                    fontWeight="bold"
                    fontSize={sizes.s}
                    style={{ marginRight: 8 }}
                  >
                    Objective Type:
                  </Text>
                </Div>
              </Grid.Column>
              <Grid.Column computer={14}>
                <div style={{ width: '50%' }}>
                  <InputDropdown
                    placeholder="Select Objective Type"
                    options={OBJECT_TYPES}
                    handleOnChange={(e, { value }) => {
                      objects.setField('type', value);
                    }}
                    value={objects.type}
                    onError={objects.validateObjectFormModel.validateType}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </CardTop>
        <CardBody>
          {objects &&
            objects.risks.map((risk) => (
              <div style={{ width: '100%' }}>
                <CardRiskAssessment
                  objects={objects}
                  risk={risk}
                  RCModel={RCModel}
                />
              </div>
            ))}
          <Div
            mid
            style={{ padding: 24 }}
            onClick={() => handleAddNewRisk(!isNewRisk)}
          >
            <ButtonBorder
              height={40}
              width={230}
              fontSize={sizes.s}
              textColor={colors.textSky}
              borderColor={colors.textSky}
              textUpper
              textWeight="med"
              text="Add Risk"
              icon="../../static/images/iconPlus@3x.png"
              handelOnClick={() => objects.addRisk()}
            />
          </Div>
        </CardBody>
      </CardAll>
      {/*
       confrim delete */}
      <ModalGlobal
        open={handelDelete}
        title="Delete"
        content="Do you want to delete object?"
        onSubmit={() => {
          RCModel.deleteObject(objects.no);
          setHandelDelete(false);
        }}
        submitText="YES"
        cancelText="NO"
        onClose={() => setHandelDelete(false)}
      />
    </div>
  ));
};

export default RiskAndControlList;
