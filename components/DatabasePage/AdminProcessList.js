import React, { useState, useEffect } from 'react';
import {
  Image,
  Loader,
  Dimmer,
  Grid,
  Select,
  Popup,
  Button,
  Header,
} from 'semantic-ui-react';
import Link from 'next/link';
// import Router, { useRouter } from "next/router";
import { observer, useLocalStore, useObserver } from 'mobx-react-lite';
import styled from 'styled-components';
import _, { head } from 'lodash';
import { Text, CardEmpty } from '../element';
import { colors, sizes } from '../../utils';
import { initProcessDBContext, initAuthStore } from '../../contexts';
import Level2 from './processDBList/Level2';

const CardTop = styled.div`
  border-radius: 6px 6px 0px 0px !important;
  background-color: ${colors.primary};
  height: 62px;
  padding: 8px 24px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;
const TableHeader = styled.div`
  float: left;
  width: 100%;
  min-height: 50px;
  flex-wrap: wrap;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

// rotateX
const TabScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;
// rotateX Scroll Bar
const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
  transform: rotateX(180deg);
  -moz-transform: rotateX(180deg); /* Mozilla */
  -webkit-transform: rotateX(180deg); /* Safari and Chrome */
  -ms-transform: rotateX(180deg); /* IE 9+ */
  -o-transform: rotateX(180deg); /* Opera */
`;

const CardTab = styled.div`
  background-color: ${colors.backgroundPrimary};
`;

const DividerLine = styled.div`
  height: 1px;
  width: 100%;
  margin-bottom: 6px;
  background-color: ${colors.btGray};
`;

const headers = [
  { key: 'Process', render: 'Process  Name', width: 350 },
  { key: 'Definition', render: 'Process Definition', width: 400 },
  { key: 'Value', render: 'Process Value', width: 300 },
  { key: 'Mapping', render: 'Process Reference', width: 250 },
  { key: 'Matched', render: 'Exactly Matched', width: 250 },
  { key: 'Remark', render: 'Remark', width: 250, center: 'center' },
  { key: 'Status', render: 'Status', width: 250, center: 'center' },
];

const PageAction = ({ tab }) => {
  const processDBContext = useLocalStore(() => initProcessDBContext);
  const authContext = initAuthStore();
  const [radioY, setRadioY] = useState(false);
  const [radioN, setRadioN] = useState(false);
  const [active, setActive] = useState(false);
  const [mode, setMode] = useState(false);
  const [tagList, setTagList] = useState([]);

  const [title, setTitle] = useState({
    title: '',
    preTitle: '',
    postTitle: '',
  });

  const hadelRadio = (value) => {
    if (value.type === 'yes') {
      setRadioY(true);
      setRadioN(false);
    } else {
      setRadioY(false);
      setRadioN(true);
    }
  };

  const setAlertModal = (titleText, preTitle, postTitle, mode) => {
    setTitle({ titleText, preTitle, postTitle });
    setMode(mode);
    setActive(true);
  };

  const handleSelection = (updater) => (e, { value }) => updater(value);

  // แสดง exacly match กับ remark เฉพาะ role admin
  const renderHeader = () => {
    const headerUser = headers.filter(
      (header) => header.key !== 'Matched' && header.key !== 'Remark',
    );

    console.log('headerUser', headerUser);

    if (authContext.roleName !== 'SUPER-ADMIN') {
      return headerUser;
    }

    return headers;
  };

  const renderProcessDB = () => {
    return (
      <div style={{ width: '100%' }}>
        {processDBContext.managementProcess ? (
          processDBContext.managementProcess &&
          processDBContext.managementProcess.children.map((lvlTwo) => (
            <div style={{ width: '100%', marginBottom: 24 }}>
              <CardTop>
                <Text
                  fontSize={sizes.s}
                  fontWeight="bold"
                  color={colors.backgroundPrimary}
                >
                  {lvlTwo.no} {lvlTwo.name}
                </Text>
              </CardTop>
              <CardTab className="w-highlight">
                <TableScroll>
                  <TabScroll>
                    <Div col>
                      <Div left={14}>
                        {renderHeader().map(
                          ({ render, key, width, center }) => (
                            <TableHeader key={key}>
                              <Cell width={width} mid={center}>
                                <Text
                                  fontSize={sizes.xs}
                                  fontWeight="bold"
                                  color={colors.textlightGray}
                                >
                                  {render}
                                </Text>
                                {render === 'Sub-Process' ||
                                render === 'Status' ? (
                                  <Image
                                    src="../../static/images/iconDown-gray.png"
                                    style={{
                                      marginLeft: 8,
                                      width: 18,
                                      height: 18,
                                    }}
                                  />
                                ) : (
                                  ''
                                )}
                              </Cell>
                            </TableHeader>
                          ),
                        )}
                      </Div>
                      <DividerLine />
                      <Level2 lvlTwo={lvlTwo} />
                    </Div>
                  </TabScroll>
                </TableScroll>
              </CardTab>
            </div>
          ))
        ) : (
          <CardEmpty textTitle="NO DATA MANAGEMENT PROCESS" />
        )}
      </div>
    );
  };

  const renderCoreProcess = () => {
    return (
      <div style={{ width: '100%' }}>
        {processDBContext.coreProcess ? (
          processDBContext.coreProcess &&
          processDBContext.coreProcess.children.map((lvlTwo) => (
            <div>
              <CardTop>
                <Text
                  fontSize={sizes.s}
                  fontWeight="bold"
                  color={colors.backgroundPrimary}
                >
                  {lvlTwo.no} {lvlTwo.name}
                </Text>
              </CardTop>
              <CardTab className="w-highlight">
                <TableScroll>
                  <TabScroll>
                    <Div col>
                      <Div left={14}>
                        {renderHeader().map(({ render, key, width }) => (
                          <TableHeader key={key}>
                            <Cell width={width}>
                              <Text
                                fontSize={sizes.xs}
                                fontWeight="bold"
                                color={colors.textGray}
                              >
                                {render}
                              </Text>
                              {render === 'Sub-Process' ||
                              render === 'Status' ? (
                                <Image
                                  src="../../static/images/iconDown-gray.png"
                                  style={{
                                    marginLeft: 8,
                                    width: 18,
                                    height: 18,
                                  }}
                                />
                              ) : (
                                ''
                              )}
                            </Cell>
                          </TableHeader>
                        ))}
                      </Div>
                      <DividerLine />
                      <Level2 lvlTwo={lvlTwo} />
                    </Div>
                  </TabScroll>
                </TableScroll>
              </CardTab>
            </div>
          ))
        ) : (
          <CardEmpty textTitle="NO DATA CORE PROCESS" />
        )}
      </div>
    );
  };

  const renderSupport = () => {
    return (
      <div style={{ width: '100%' }}>
        {processDBContext.support ? (
          processDBContext.support &&
          processDBContext.support.children.map((lvlTwo) => (
            <div>
              <CardTop>
                <Text
                  fontSize={sizes.s}
                  fontWeight="bold"
                  color={colors.backgroundPrimary}
                >
                  {`${lvlTwo.no} ${lvlTwo.name}`}
                </Text>
              </CardTop>
              <CardTab className="w-highlight">
                <TableScroll>
                  <TabScroll>
                    <Div col>
                      <Div left={14}>
                        {renderHeader().map(({ render, key, width }) => (
                          <TableHeader key={key}>
                            <Cell width={width}>
                              <Text
                                fontSize={sizes.xs}
                                fontWeight="bold"
                                color={colors.textGray}
                              >
                                {render}
                              </Text>
                              {render === 'Sub-Process' ||
                              render === 'Status' ? (
                                <Image
                                  src="../../static/images/iconDown-gray.png"
                                  style={{
                                    marginLeft: 8,
                                    width: 18,
                                    height: 18,
                                  }}
                                />
                              ) : (
                                ''
                              )}
                            </Cell>
                          </TableHeader>
                        ))}
                      </Div>
                      <DividerLine />
                      <Level2 lvlTwo={lvlTwo} />
                    </Div>
                  </TabScroll>
                </TableScroll>
              </CardTab>
            </div>
          ))
        ) : (
          <CardEmpty textTitle="NO DATA SUPPORT PROCESS" />
        )}
      </div>
    );
  };

  useEffect(() => {
    processDBContext.getProcessList();
    processDBContext.getTagList();

    console.log(processDBContext.processList);
  }, []);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      {processDBContext.lvlOneType === 1 && renderProcessDB()}
      {processDBContext.lvlOneType === 2 && renderCoreProcess()}
      {processDBContext.lvlOneType === 3 && renderSupport()}
    </div>
  ));
};

export default PageAction;
