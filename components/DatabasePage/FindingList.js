import React, { useState } from 'react';
import { observer, useObserver } from 'mobx-react-lite';
import { Icon, Grid, Pagination, Radio } from 'semantic-ui-react';
import moment from 'moment';
import styled from 'styled-components';
import { CheckBoxAll, Text } from '../../components/element';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';

const DividerLineBlack = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? `#333333` : `#f6f6f6`)};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  border-top: 1px solid #d9d9d6;
  display: flex;
  align-items: start;
  flex-direction: row;
  width: auto;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}px;
  padding: 10px;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.group ? `space-around` : `start`)};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: flex-star;
`;

const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
`;

const CardTab = styled.div`
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
`;

const headers = [
  { key: 'Yes', render: 'Yes', width: 50 },
  { key: 'No', render: 'No', width: 50 },
  { key: 'Audit Engagement Code', render: 'Audit Engagement Code', width: 150 },
  { key: 'Audit Engagement Name', render: 'Audit Engagement Name', width: 300 },
  { key: 'Type', render: 'Type', width: 150 },
  { key: 'Auditee Units (Link)', render: 'Auditee Units (Link)', width: 200 },
  { key: 'Sub-process', render: 'Sub-process', width: 200 },
  {
    key: 'Categorized Recommendation',
    render: 'Categorized Recommendation',
    width: 200,
  },
  {
    key: 'Focus Object/Risk/Control',
    render: 'Focus Object/Risk/Control',
    width: 200,
  },
  {
    key: 'Finding/Recommendation for CSA assessment',
    render: 'Finding/Recommendation for CSA assessment',
    width: 600,
  },
  {
    key: 'Finding Description',
    render: 'Finding Description',
    width: 600,
  },

  { key: 'Finding Rating', render: 'Finding Rating', width: 100 },

  { key: 'Root Cause', render: 'Root Cause', width: 600 },
  { key: 'Impact', render: 'Impact', width: 600 },
  {
    key: 'Recommendation Description',
    render: 'Recommendation Description',
    width: 600,
  },
  { key: 'Issue State', render: 'Issue State', width: 200 },
  {
    key: 'Recommendation State',
    render: 'Recommendation State',
    width: 200,
  },
  { key: 'Target Date', render: 'Target Date', width: 200 },

  { key: 'Revised Target Date', render: 'Revised Target Date', width: 200 },
  { key: 'Closed', render: 'Closed', width: 200 },
  { key: 'Closed by', render: 'Closed by', width: 200 },
  { key: 'Actual Start', render: 'Actual Start', width: 200 },
  { key: 'Actual End', render: 'Actual End', width: 200 },
  { key: 'Team Lead', render: 'Team Lead', width: 300 },
  { key: 'Team Member', render: 'Team Member', width: 300 },
  { key: 'Auditee Units', render: 'Auditee Units', width: 300 },
  { key: 'Remark1', render: 'Remark1', width: 200 },
  { key: 'Remark2', render: 'Remark2', width: 200 },
];

const PageAction = ({ iaFindingListModel }) => {
  const authContext = initAuthStore();

  return useObserver(() => (
    <div>
      <CardTab className="w-highlight">
        <TableScroll>
          <Div col>
            <Div left={14}>
              {headers.map(({ render, key, width }) => (
                <TableHeader key={key}>
                  <Cell width={width}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.textlightGray}
                    >
                      {render}
                    </Text>
                  </Cell>
                </TableHeader>
              ))}
            </Div>

            {iaFindingListModel.list &&
              iaFindingListModel.list.map((ia, index) => (
                <div>
                  <Div>
                    <TableBody key={index}>
                      <Div>
                        <Cell width={50}>
                          <CheckBoxAll
                            status="Accept"
                            toggle
                            checked={ia.isCheck}
                            disabled
                          />
                        </Cell>
                        <Cell width={50}>
                          <CheckBoxAll
                            status="Reject"
                            toggle
                            checked={ia.isCheck === false}
                            disabled
                          />
                        </Cell>
                        <Cell width={150}>
                          <div
                            className="break-word"
                            style={{ width: '150px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="bold"
                              color={colors.primaryBlack}
                            >
                              {ia.auditEngagementCode
                                ? ia.auditEngagementCode
                                : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={300}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '300px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.auditEngagementName
                                ? ia.auditEngagementName
                                : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={150}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '150px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.type ? ia.type : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.auditUnitLink ? ia.auditUnitLink : '-'}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.subProcess ? ia.subProcess : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.categorizedRecommendation
                                ? ia.categorizedRecommendation
                                : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.focusObjective ? ia.focusObjective : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={600}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '600px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.findingNo && ia.findingNo}
                              {ia.findingName ? ia.findingName : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={600}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '600px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.findingDescription
                                ? ia.findingDescription
                                : '-'}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={100}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '100px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.findingRate ? ia.findingRate : '-'}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={600}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '600px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.rootCause ? ia.rootCause : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={600}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '600px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.impact ? ia.impact : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={600}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '600px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.recommendationDescription
                                ? ia.recommendationDescription
                                : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.issueState ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {ia.issueState}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ width: '200px' }}
                          >
                            {ia.recommendationState ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {ia.recommendationState}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.targetDate ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {moment(ia.targetDate).format('DD/MM/YYYY')}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.revisedTargetDate ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {moment(ia.revisedTargetDate).format(
                                  'DD/MM/YYYY',
                                )}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.closed ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {moment(ia.closed).format('DD/MM/YYYY')}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.closedBy ? ia.closedBy : '-'}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.actualStart ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {moment(ia.actualStart).format('DD/MM/YYYY')}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.actualEnd ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {moment(ia.actualEnd).format('DD/MM/YYYY')}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>

                        <Cell width={300}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '300px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.teamLead ? ia.teamLead : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={300}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '300px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.teamMember ? ia.teamMember : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={300}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '300px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.auditUnits ? ia.auditUnits : '-'}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.remark1 ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {ia.remark1}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            {ia.remark2 ? (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                {ia.remark2}
                              </Text>
                            ) : (
                              <Text
                                fontSize={sizes.xs}
                                color={colors.primaryBlack}
                              >
                                -
                              </Text>
                            )}
                          </div>
                        </Cell>
                      </Div>
                    </TableBody>
                  </Div>
                  {ia.comment && (
                    <Div style={{ padding: 16 }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.red}
                      >
                        Comment :
                      </Text>
                      <Text fontSize={sizes.xs} color={colors.red}>
                        {ia.comment}
                      </Text>
                    </Div>
                  )}
                </div>
              ))}

            <DividerLineBlack bottom style={{ marginBottom: 16 }} />
          </Div>
        </TableScroll>
      </CardTab>

      {iaFindingListModel.totalPage > 1 && (
        <Grid style={{ marginTop: 16 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={iaFindingListModel.page}
              activePage={iaFindingListModel.page}
              onPageChange={(e, { activePage }) => {
                iaFindingListModel.setField('page', activePage);

                iaFindingListModel.getIAFindingList(
                  authContext.accessToken,
                  iaFindingListModel.page,
                  iaFindingListModel.searchText,
                  iaFindingListModel.filterModel.selectedBu,
                  iaFindingListModel.filterModel.selectedDepartment.no,
                  iaFindingListModel.filterModel.selectedDivision,
                  iaFindingListModel.filterModel.selectedIAFindingType,
                );
              }}
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={iaFindingListModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default PageAction;
