import React, { useState, useEffect } from 'react';
import { Image, Grid, Loader, Dimmer, Segment, Modal } from 'semantic-ui-react';
import styled from 'styled-components';
import Link from 'next/link';
import { Text, ButtonBorder, ButtonAll } from '../../element';
import { colors, sizes } from '../../../utils';
import {
  NextModal,
  CreateProcessThreeModal,
  CreateProcessTwoModal,
  CreateProcessFourModal,
} from '../modal';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const ImageItem = styled(Image)`
  @media only screen and (min-width: 768px) {
    width: auto;
    height: 100px !important;
  }
  @media only screen and (min-width: 1440px) {
    width: auto;
    height: 250px !important;
  }
`;
const CardItem = styled(Segment)`
  width: 334px;
  background-color: #ffffff;
  border-radius: 6px 6px 0px 6px !important;
  display: flex;
  padding: 32px 32px 40px 32px !important;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;

  &:hover,
  :active {
    box-shadow: 0 2px 9px 0 rgba(0, 87, 184, 0.5) !important;
  }

  @media only screen and (min-width: 768px) {
    height: 220px;
  }

  @media only screen and (min-width: 1440px) {
    height: 334px;
  }
`;

const Div = styled.div`
  flex-direction: row;
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: flex-start;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const index = ({ active, onClose, setSelectModal }) => {
  const [levelTwo, setLevelTwo] = useState(false);
  const [levelThree, setLevelThree] = useState(false);
  const [levelFour, setLevelFour] = useState(false);
  const [windowSize, setWindowSize] = useState();

  const handelOpenModal = (value, Lv) => {
    console.log(value, Lv);
    if (Lv === 'lv2') {
      setLevelTwo(value);
    } else if (Lv === 'lv3') {
      setLevelThree(value);
    } else if (Lv === 'lv4') {
      setLevelFour(value);
    }
    setSelectModal(false);
  };
  useEffect(() => {
    setWindowSize(window.innerWidth);
  }, []);

  return (
    <div>
      <ModalBox
        size={windowSize <= 1024 ? 'small' : 'large'}
        open={active}
        onClose={onClose}
        closeOnDimmerClick={onClose}
      >
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Select Process Level
        </Text>
        <Modal.Content style={{ padding: 0 }}>
          <div style={{ paddingTop: 32 }}>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <Div between onClick={() => handelOpenModal(true, 'lv2')}>
                    <CardItem className="upper click">
                      <ImageItem src="../../static/images/process-database-lv@3x.png" />
                      <Text
                        color={colors.textSky}
                        fontWeight={'bold'}
                        fontSize={sizes.l}
                        style={{ paddingTop: 16 }}
                      >
                        Key Process (Level 2)
                      </Text>
                    </CardItem>
                  </Div>
                </Grid.Column>
                <Grid.Column>
                  <Div between onClick={() => handelOpenModal(true, 'lv3')}>
                    <CardItem className="upper click">
                      <ImageItem src="../../static/images/process-database-lv@3x.png" />
                      <Text
                        color={colors.textSky}
                        fontWeight={'bold'}
                        fontSize={sizes.l}
                        style={{ paddingTop: 16 }}
                      >
                        Process (Level 3)
                      </Text>
                    </CardItem>
                  </Div>
                </Grid.Column>
                <Grid.Column>
                  <Div between onClick={() => handelOpenModal(true, 'lv4')}>
                    <CardItem className="upper click">
                      <ImageItem src="../../static/images/process-database-lv@3x.png" />
                      <Text
                        color={colors.textSky}
                        fontWeight={'bold'}
                        fontSize={sizes.l}
                        style={{ paddingTop: 16 }}
                      >
                        Sub-Process (Level 4)
                      </Text>
                    </CardItem>
                  </Div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Modal.Content>
      </ModalBox>

      <CreateProcessTwoModal
        active={levelTwo}
        onClose={() => setLevelTwo(false)}
      />
      <CreateProcessThreeModal
        active={levelThree}
        onClose={() => setLevelThree(false)}
      />
      <CreateProcessFourModal
        active={levelFour}
        onClose={() => setLevelFour(false)}
      />
    </div>
  );
};

export default index;
