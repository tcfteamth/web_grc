import React, { useState, useEffect } from 'react';
import { Grid, Modal, Dimmer, Loader } from 'semantic-ui-react';
import Router from 'next/router';
import styled from 'styled-components';
import request from '../../../services';
import {
  Text,
  ButtonBorder,
  ButtonAll,
  InputAll,
  DropdownAll,
  ModalGlobal,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore } from '../../../contexts';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const lvl1s = [
  {
    key: '1 - Management Process',
    label: 'Level 1 - Management Process',
    value: 1,
  },
  { key: '2 - Core Process', label: 'Level 2 - Core Process', value: 2 },
  { key: '3 - Support', label: 'Level 3 - Support', value: 3 },
];

const index = ({ active, onClose, setSelectModal }) => {
  const authContext = initAuthStore();

  const [windowSize, setWindowSize] = useState();
  const [process, setProcess] = useState();
  const [processName, setProcessName] = useState();
  const [selectedProcess, setSelectedProcess] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [txtModal, setTxtModal] = useState();

  const createProcessMaster = async () => {
    setIsLoading(true);

    const data = {
      no: process,
      nameEn: processName,
      nameTh: processName,
      lvl: 2,
      parent: selectedProcess.value,
    };

    try {
      await request.processDBServices.createProcessMaster(
        authContext.accessToken,
        data,
      );
      setTxtModal('Success');
      Router.reload();
    } catch (e) {
      setTxtModal('มี Process Database นี้อยู่แล้ว');
      console.log(e);
    }
    setShowModal(true);
    setIsLoading(false);
  };

  useEffect(() => {
    setWindowSize(window.innerWidth);
  }, []);

  return (
    <div>
      <Dimmer active={isLoading}>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <ModalBox
        size={windowSize <= 1024 ? 'small' : 'large'}
        open={active}
        onClose={onClose}
        closeOnDimmerClick={onClose}
      >
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Create Process Database
        </Text>
        <Modal.Content style={{ padding: 0 }}>
          <div style={{ paddingTop: 32 }}>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Enterprise Process (Level 1)
                  </Text>
                  <DropdownAll
                    placeholder="Key Process"
                    options={lvl1s}
                    handleOnChange={(e) => {
                      setSelectedProcess(e);
                    }}
                    value={selectedProcess}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Key Process (Level 2){' '}
                    <span style={{ color: colors.textGray }}>
                      {' '}
                      Ex. 2 ( 2 is number Level 2 )
                    </span>
                  </Text>
                  <InputAll
                    type="number"
                    placeholder="Key Process Number"
                    handleOnChange={(text) => setProcess(text.target.value)}
                    value={process}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Name (Level 2)
                  </Text>
                  <InputAll
                    placeholder="Process Name (Level 2)"
                    handleOnChange={(text) => setProcessName(text.target.value)}
                    value={processName}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Modal.Content>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 92,
          }}
        >
          <ButtonBorder
            width={140}
            style={{ marginRight: 20 }}
            handelOnClick={onClose}
            borderColor={colors.textlightGray}
            textColor={colors.primaryBlack}
            textWeight="med"
            textUpper
            text="CANCEL"
          />
          <ButtonAll
            width={140}
            color={
              process && processName && selectedProcess
                ? colors.primary
                : colors.btGray
            }
            textColor={colors.backgroundPrimary}
            textUpper
            onClick={() => createProcessMaster()}
            disabled={process && processName && selectedProcess ? false : true}
            textWeight="med"
            text={'Create Now'}
          />
        </div>
      </ModalBox>

      <ModalGlobal
        open={showModal}
        title="Process Master"
        content={txtModal}
        onClose={() => {
          setShowModal(false);
        }}
      />
    </div>
  );
};

export default index;
