import React, { useState, useEffect } from 'react';
import { Grid, Loader, Dimmer, Modal, Select, TextArea, Form } from 'semantic-ui-react';
import Router from 'next/router';
import styled from 'styled-components';
import request from '../../../services';
import {
  Text,
  ButtonBorder,
  ButtonAll,
  InputAll,
  DropdownAll,
  RadioBox,
  InputDropdown,
  ModalGlobal,
} from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore, initProcessDBContext } from '../../../contexts';
import { useLocalStore, useObserver } from 'mobx-react-lite';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  flex-wrap: wrap;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const index = ({ active, onClose, setSelectModal }) => {
  const authContext = initAuthStore();
  const processDBContext = useLocalStore(() => initProcessDBContext);

  const [windowSize, setWindowSize] = useState();
  const [process, setProcess] = useState();
  const [processName, setProcessName] = useState();
  const [selectedProcess, setSelectedProcess] = useState();
  const [processValue, setProcessValue] = useState();
  const [processDefinition, setProcessDefinition] = useState();
  const [sourceMapName, setSourceMapName] = useState();
  const [sourceMapDefinition, setSourceMapDefinition] = useState();
  const [tagIds, setTagIds] = useState([]);
  const [remark, setRemark] = useState();
  const [isExactlyMatched, setIsExactlyMatched] = useState(true);
  const [reasonExactlyMatched, setReasonExactlyMatched] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [txtModal, setTxtModal] = useState();
  const handleSelection = (updater) => (e, { value }) => {
    setTagIds(value);
  };
  const tagList = [];
  const lvl2s = [];

  processDBContext.processList &&
    processDBContext.processList.map((p) =>
      p.children.map((child) =>
        lvl2s.push({
          key: child.no + ' ' + child.name,
          label: child.no + ' ' + child.name,
          value: child.no,
        }),
      ),
    );

  processDBContext.tagList &&
    processDBContext.tagList.map((t) =>
      tagList.push({
        value: t.id,
        text: t.name,
      }),
    );

  const createProcessMaster = async () => {
    setIsLoading(true);

    const data = {
      no: process,
      nameEn: processName,
      nameTh: processName,
      lvl: 3,
      parent: selectedProcess.value,
      value: processValue,
      processDefinition,
      sourceMappingName: sourceMapName,
      sourceMappingDefinition: sourceMapDefinition,
      tagIds:
        tagIds.length == 0 ? null : tagIds.map((t) => '|' + t + '|').join(','),
      remark: remark,
      exactlyMatched: isExactlyMatched,
      reasonExactlyMatched: !isExactlyMatched ? reasonExactlyMatched : null,
    };

    try {
      await request.processDBServices.createProcessMaster(
        authContext.accessToken,
        data,
      );
      setTxtModal('Success');
      Router.reload();
    } catch (e) {
      setTxtModal('มี Process Database นี้อยู่แล้ว');
      console.log(e);
    }
    setShowModal(true);
    setIsLoading(false);
  };

  useEffect(() => {
    setWindowSize(window.innerWidth);
  }, []);

  return useObserver(() => (
    <div>
      <Dimmer active={isLoading}>
        <Loader>
          <Text fontSize={sizes.s} color={colors.backgroundPrimary}>
            Loading
          </Text>
        </Loader>
      </Dimmer>
      <ModalBox
        size={windowSize <= 1024 ? 'small' : 'large'}
        open={active}
        onClose={onClose}
        closeOnDimmerClick={onClose}
      >
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Create Process Database
        </Text>
        <Modal.Content style={{ padding: 0 }}>
          <div style={{ paddingTop: 32 }}>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Key Process (Level 2)
                  </Text>
                  <DropdownAll
                    placeholder="Key Process"
                    options={lvl2s}
                    handleOnChange={(e) => {
                      setSelectedProcess(e);
                    }}
                    value={selectedProcess}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process (Level 3){' '}
                    <span style={{ color: colors.textGray }}>
                      {' '}
                      Ex. 2 ( 2 is number Level 3 )
                    </span>
                  </Text>
                  <InputAll
                    type="number"
                    placeholder="Key Process Number"
                    handleOnChange={(text) => setProcess(text.target.value)}
                    value={process}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Name (Level 3)
                  </Text>
                  <InputAll
                    placeholder="Process Name (Level 3)"
                    handleOnChange={(text) => setProcessName(text.target.value)}
                    value={processName}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Definition
                  </Text>
                  <Form>
                    <TextArea
                      placeholder="Process Definition"
                      onChange={(e) => setProcessDefinition(e.target.value)}
                      value={processDefinition}
                    />
                  </Form>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Value
                  </Text>
                  <InputAll
                    placeholder="Process Value"
                    handleOnChange={(e) => setProcessValue(e.target.value)}
                    value={processValue}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Reference - Process Name
                  </Text>
                  <InputAll
                    placeholder="Source Mapping - Process Name"
                    handleOnChange={(text) =>
                      setSourceMapName(text.target.value)
                    }
                    value={sourceMapName}
                  />
                </Grid.Column>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Process Reference - Process Definition
                  </Text>
                  <InputAll
                    placeholder="Source Mapping - Process Definition"
                    handleOnChange={(text) =>
                      setSourceMapDefinition(text.target.value)
                    }
                    value={sourceMapDefinition}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Exactly Matched
                  </Text>
                  <div>
                    <Grid columns="equal">
                      <Grid.Row>
                        <Grid.Column>
                          <Div between center top={4}>
                            <RadioBox
                              label="YES"
                              checked={isExactlyMatched}
                              onChange={() => {
                                setIsExactlyMatched(true);
                              }}
                            />
                            <RadioBox
                              label="NO"
                              checked={!isExactlyMatched}
                              onChange={() => {
                                setIsExactlyMatched(false);
                              }}
                            />
                          </Div>
                        </Grid.Column>
                        <Grid.Column>
                          <InputAll
                            disabled={isExactlyMatched}
                            minWidth={60}
                            placeholder="Reason"
                            handleOnChange={(text) =>
                              setReasonExactlyMatched(text.target.value)
                            }
                            value={reasonExactlyMatched}
                          />
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                </Grid.Column>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Remark
                  </Text>
                  <InputAll
                    minWidth={60}
                    placeholder="Remark"
                    handleOnChange={(text) => setRemark(text.target.value)}
                    value={remark}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text fontSize={sizes.xs} color={colors.textDarkGray}>
                    Tag
                  </Text>
                  <InputDropdown
                    control={Select}
                    placeholder="Select Tag"
                    search
                    width={50}
                    options={tagList}
                    value={tagIds}
                    multiple
                    handleOnChange={handleSelection(setTagIds)}
                    handleSelection
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Modal.Content>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 92,
          }}
        >
          <ButtonBorder
            width={140}
            style={{ marginRight: 20 }}
            handelOnClick={onClose}
            borderColor={colors.textlightGray}
            textColor={colors.primaryBlack}
            textWeight="med"
            textUpper
            text="CANCEL"
          />
          <ButtonAll
            width={140}
            color={
              process && processName && selectedProcess
                ? colors.primary
                : colors.btGray
            }
            textColor={colors.backgroundPrimary}
            textUpper
            onClick={() => createProcessMaster()}
            disabled={process && processName && selectedProcess ? false : true}
            textWeight="med"
            text={'Create Now'}
          />
        </div>
      </ModalBox>

      <ModalGlobal
        open={showModal}
        title="Process Master"
        content={txtModal}
        onClose={() => {
          setShowModal(false);
        }}
      />
    </div>
  ));
};

export default index;
