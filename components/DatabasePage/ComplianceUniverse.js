import React from 'react';
import { useObserver } from 'mobx-react-lite';
import { Grid, Icon, Pagination } from 'semantic-ui-react';
import styled from 'styled-components';
import { Text } from '../../components/element';
import { colors, sizes } from '../../utils';
import { initAuthStore } from '../../contexts';
import moment from 'moment';

const DividerLineBlack = styled.div`
  height: 1px !important;
  width: 100%;
  background-color: ${(props) => (props.bottom ? `#333333` : `#f6f6f6`)};
`;

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  border-top: 1px solid #d9d9d6;
  display: flex;
  align-items: start;
  flex-direction: row;
  width: auto;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}px;
  padding: 10px;
  flex-wrap: wrap;
  justify-content: ${(props) => (props.group ? `space-around` : `start`)};
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: flex-star;
`;

const TableScroll = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: auto;
  overflow-x: auto;
  overflow-y: hidden;
`;

const CardTab = styled.div`
  background-color: ${colors.backgroundPrimary};
  border-radius: 6px 6px 0px 6px !important;
`;

const headers = [
  { key: ' No.', render: ' No.', width: 100 },
  {
    key: 'process',
    render: 'กระบวนการทำงาน (ที่หน่วยงานภายในต้องดำเนินการเพื่อให้ comply)',
    width: 400,
  },
  {
    key: 'ชื่อกฎหมาย',
    render: 'ชื่อกฎหมาย / กฎระเบียบ',
    width: 400,
  },
  { key: 'ประเภท', render: 'ประเภท', width: 150 },
  { key: 'มาตราที่เกี่ยวข้อง', render: 'มาตราที่เกี่ยวข้อง', width: 150 },
  { key: 'Division', render: 'Division', width: 100 },
  { key: 'Objective', render: 'Objective', width: 300 },
  { key: 'compliance risk', render: 'compliance risk', width: 250 },
  {
    key: 'Compliance Impact',
    render: 'Compliance Impact',
    width: 250,
  },
  {
    key: 'Likelihood',
    render: 'Likelihood of non-compliance (Rating)',
    width: 250,
  },
  {
    key: 'Impact',
    render: 'Impact from non-compliance (Rating)',
    width: 250,
  },
  {
    key: 'Risk',
    render: 'Risk level (Rating likelihood x impact)',
    width: 250,
  },

  {
    key: 'Existing Control',
    render: 'Existing Control',
    width: 250,
  },
  {
    key: 'Control type',
    render: 'Control type',
    width: 250,
  },
  {
    key: 'Control classification',
    render: 'Control classification',
    width: 250,
  },

  { key: 'Frequency', render: 'Frequency', width: 250 },
  {
    key: 'Information Technology system',
    render: 'Information Technology system',
    width: 250,
  },
  {
    key: 'Executor',
    render: 'Executor',
    width: 250,
  },
  {
    key: 'Document / Evidence',
    render: 'Document / Evidence',
    width: 250,
  },
  {
    key: 'Control Rating',
    render: 'Control Rating',
    width: 250,
  },
  {
    key: 'Observation & Finding',
    render: 'Observation & Finding',
    width: 250,
  },
  {
    key: 'Enhancement / Corrective Action',
    render: 'Enhancement / Corrective Action',
    width: 250,
  },
  {
    key: 'Benefit',
    render: 'Benefit (only for enhancement)',
    width: 250,
  },
  {
    key: 'Due date',
    render: 'Due date',
    width: 100,
  },
  {
    key: 'Co-responsible division (if any)',
    render: 'Co-responsible division (if any)',
    width: 250,
  },
  {
    key: 'E-legal library no.',
    render: 'E-legal library no.',
    width: 200,
  },
  {
    key: 'Standard / optional',
    render: 'Standard / optional',
    width: 200,
  },
  {
    key: 'GRC no.',
    render: 'GRC no.',
    width: 150,
  },
  {
    key: 'Remark1',
    render: 'Remark1',
    width: 250,
  },
  {
    key: 'Remark2',
    render: 'Remark2',
    width: 250,
  },
];

const complianceModelList = ({ complianceModel }) => {
  const authContext = initAuthStore();

  return useObserver(() => (
    <div>
      <CardTab className="w-highlight">
        <TableScroll>
          <Div col>
            <Div left={14}>
              {headers.map(({ render, key, width }) => (
                <TableHeader key={key}>
                  <Cell width={width}>
                    <Text
                      fontSize={sizes.xs}
                      fontWeight="bold"
                      color={colors.textlightGray}
                    >
                      {render}
                    </Text>
                  </Cell>
                </TableHeader>
              ))}
            </Div>

            {complianceModel.list &&
              complianceModel.list.map((ia, index) => (
                <div>
                  <Div>
                    <TableBody key={index}>
                      <Div>
                        <Cell width={100}>
                          <div
                            className="break-word"
                            style={{ width: '100px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              fontWeight="bold"
                              color={colors.primaryBlack}
                            >
                              {ia.no}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={400}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '400px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.process}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={400}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '400px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.regulation}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={150}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '150px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.type}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={150}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '150px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.measure}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={100}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '100px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.division}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={300}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '300px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.objective}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.complianceRisk}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.complianceImpact}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.likelihoodRating}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.impactRating}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.riskLvRating}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.existingControl}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.controlType}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.controlClassification}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.frequency}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.information}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.executor}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.documentEvidence}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.controlRating}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.observationFinding}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.enhancementCorrective}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.benefit}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={100}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '100px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {moment(ia.dueDate).locale('th').format('L')}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.coResponsibleDivision}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.eLegalLibraryNo}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={200}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '200px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.standardOptional}
                            </Text>
                          </div>
                        </Cell>

                        <Cell width={150}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '150px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.grcNo}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.remark1}
                            </Text>
                          </div>
                        </Cell>
                        <Cell width={250}>
                          <div
                            className="break-word"
                            style={{ paddingRight: 10, width: '250px' }}
                          >
                            <Text
                              fontSize={sizes.xs}
                              color={colors.primaryBlack}
                            >
                              {ia.remark2}
                            </Text>
                          </div>
                        </Cell>
                      </Div>
                    </TableBody>
                  </Div>
                  {ia.comment && (
                    <Div style={{ padding: 16 }}>
                      <Text
                        fontSize={sizes.xs}
                        fontWeight="bold"
                        color={colors.red}
                      >
                        Comment :
                      </Text>
                      <Text fontSize={sizes.xs} color={colors.red}>
                        {ia.comment}
                      </Text>
                    </Div>
                  )}
                </div>
              ))}

            <DividerLineBlack bottom style={{ marginBottom: 16 }} />
          </Div>
        </TableScroll>
      </CardTab>

      {complianceModel.totalPage > 1 && (
        <Grid style={{ marginTop: 16 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={complianceModel.page}
              activePage={complianceModel.page}
              onPageChange={(e, { activePage }) => {
                complianceModel.setField('page', activePage);

                complianceModel.getComplianceList(
                  authContext.accessToken,
                  complianceModel.page,
                  complianceModel.searchText,
                  complianceModel.filterModel.selectedBu,
                  complianceModel.filterModel.selectedDepartment.no,
                  complianceModel.filterModel.selectedDivision,
                );
              }}
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={complianceModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default complianceModelList;
