import React, { useState, memo } from 'react';
import styled from 'styled-components';
import { Image, Popup, Radio } from 'semantic-ui-react';
import { useLocalStore } from 'mobx-react-lite';
import Link from 'next/link';
import { Text, ButtonAll } from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore, initProcessDBContext } from '../../../contexts';
import { EditProcessFourModal } from '../modal';
import Router from 'next/router';

const ContentLine = styled.div`
  height: 1px;
  width: 100%;
  margin-left: 36px;
  background-color: #d9d9d6;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: start;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const CardRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
`;

const Level3 = ({ lvlThree }) => {
  const processDBContext = useLocalStore(() => initProcessDBContext);
  const authContext = initAuthStore();
  const [active, setActive] = useState(false);
  const [mode, setMode] = useState(false);
  const [data, setData] = useState(false);
  const [isToggle, setIsToggle] = useState(false);

  const [title, setTitle] = useState({
    title: '',
    preTitle: '',
    postTitle: '',
  });

  const setDataModal = (value) => {
    setActive(true);
    setData(value);
  };

  const onToggleHide = async (value) => {
    await processDBContext.hideSubprocess(authContext.accessToken, value);
    processDBContext.getProcessList();
    processDBContext.getTagList();
  };

  const setAlertModal = ({ titleText, preTitle, postTitle, mode }) => {
    setTitle({ titleText, preTitle, postTitle });
    setMode(mode);
    setActive(true);
  };

  return (
    <>
      {lvlThree &&
        lvlThree.children.map((lvlFour) => (
          <div>
            <ContentLine />
            <Div>
              <Cell width={350} style={{ paddingLeft: 36 }}>
                <Div col>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    {`${lvlFour.no} ${lvlFour.name}`}
                  </Text>

                  {lvlFour.hasRiskControl ? (
                    <Link
                      href={`/databasePage/riskAndControlManagment?noLvl=${lvlFour.no}`}
                    >
                      <Div top={4} bottom={4} className="click">
                        <Image
                          src="../../static/images/zing@3x.png"
                          style={{
                            width: 18,
                            height: 18,
                            marginRight: 8,
                          }}
                        />
                        <Text fontSize={sizes.xs} color={colors.textSky}>
                          Risk & Control
                        </Text>
                      </Div>
                    </Link>
                  ) : (
                    <Div top={4} bottom={4} className="click">
                      <Image
                        src="../../static/images/zinggray@3x.png"
                        style={{
                          width: 18,
                          height: 18,
                          marginRight: 8,
                        }}
                      />
                      <Text fontSize={sizes.xs} color={colors.textGray}>
                        Risk & Control
                      </Text>
                    </Div>
                  )}

                  {/* Role Admin */}
                  {authContext.roles.isAdmin && (
                    <div
                      className="click"
                      onClick={() => setDataModal(lvlFour)}
                    >
                      <Image
                        src="../../static/images/edit-gray@3x.png"
                        style={{
                          width: 18,
                          height: 18,
                          marginTop: 8,
                        }}
                      />
                    </div>
                  )}
                  {lvlFour.tags && lvlFour.tags.length > 0 && (
                    <CardRow>
                      {lvlFour &&
                        lvlFour.tags.map((tag) => (
                          <ButtonAll
                            style={{ margin: '4px 4px 4px 0px' }}
                            radius
                            textSize={sizes.xxs}
                            textWeight="med"
                            height={24}
                            text={tag.name}
                            textColor={colors.textPurple}
                            color={colors.purpledark}
                          />
                        ))}
                    </CardRow>
                  )}
                </Div>
              </Cell>
              <Cell width={400}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {lvlFour.processDefinition}
                </Text>
              </Cell>
              <Cell width={300}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {lvlFour.value ? lvlFour.value : '-'}
                </Text>
              </Cell>
              <Cell width={250}>
                <Div col>
                  <Div col>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                    >
                      Process Name
                    </Text>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {lvlFour.sourceMappingName
                        ? lvlFour.sourceMappingName
                        : '-'}
                    </Text>
                  </Div>
                  <Div col top={16}>
                    <Text
                      fontSize={sizes.xxs}
                      fontWeight="bold"
                      color={colors.primaryBlack}
                    >
                      Process Definition
                    </Text>
                    <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                      {lvlFour.sourceMappingDefinition
                        ? lvlFour.sourceMappingDefinition
                        : '-'}
                    </Text>
                  </Div>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {lvlFour.sourceMapping}
                  </Text>
                </Div>
              </Cell>
              {/* NOTE: ลบออกตอนที่ service ส่ง status ของ level 4 */}
              {authContext.roles.isAdmin && (
                <Cell width={250}>
                <Div>
                  {lvlFour.exactlyMatch ? (
                    <Image
                      src="../../static/images/iconChecked@3x.png"
                      style={{ width: 20, height: 20 }}
                    />
                  ) : (
                    <Div>
                      <Image
                        src="../../static/images/close@3x.png"
                        style={{ width: 20, height: 20 }}
                      />
                      <Text
                        style={{ paddingLeft: 8 }}
                        fontSize={sizes.xs}
                        color={colors.primaryBlack}
                      >
                        Devised
                      </Text>
                    </Div>
                  )}
                </Div>
              </Cell>
              )}

              {authContext.roles.isAdmin && (
                <Cell width={250} center mid>
                  <>
                    {lvlFour.remark ? (
                      <Popup
                        wide="very"
                        style={{
                          borderRadius: '6px 6px 0px 6px',
                          padding: 16,
                          marginRight: -10,
                        }}
                        position="top right"
                        trigger={
                          <Image
                            className="click"
                            width={18}
                            src="../../static/images/markpurple@3x.png"
                          />
                        }
                      >
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {lvlFour.remark}
                        </Text>
                      </Popup>
                    ) : (
                      <Image
                        width={18}
                        src="../../static/images/markgray@3x.png"
                      />
                    )}
                  </>
                </Cell>
              )}

              {/* {authContext.roles.isAdmin && ( */}
              <Cell width={250} mid>
                <Radio
                  toggle
                  onChange={async () => {
                    onToggleHide(lvlFour.id);
                  }}
                  checked={lvlFour.isDisabled}
                  disabled={!authContext.roles.isAdmin}
                />
              </Cell>
              {/* )} */}
            </Div>
          </div>
        ))}

      {active && (
        <EditProcessFourModal
          active={active}
          onClose={() => setActive(false)}
          data={data}
        />
      )}
    </>
  );
};

export default memo(Level3);
