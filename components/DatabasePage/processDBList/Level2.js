import React, { useState, memo } from 'react';
import { Image, Grid, Select, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { ButtonAll, Text } from '../../element';
import { colors, sizes } from '../../../utils';
import { initAuthStore } from '../../../contexts';
import Level3 from './Level3';
import { EditProcessThreeModal } from '../modal';

const TableHeader = styled.div`
  float: left;
  width: 100%;
  height: 50%;
  flex-wrap: wrap;
  display: inline-block !important;
  flex-direction: row;
  align-items: center;
  justify-content: start;
`;

const TableBody = TableHeader.extend`
  margin: 1px 0px;
  padding: 5px 14px;
  width: auto;
`;

const DividerLine = styled.div`
  height: 2px;
  width: 100%;
  margin-bottom: 6px;
  background-color: ${colors.primaryBlack};
`;

const Cell = styled.div`
  width: ${(props) => props.width}px;
  padding: 10px;
  display: flex;
  align-items: start;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;

const CardRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const Level2 = ({ lvlTwo }) => {
  const authContext = initAuthStore();
  const [active, setActive] = useState(false);
  const [radioY, setRadioY] = useState(false);
  const [radioN, setRadioN] = useState(false);
  const [mode, setMode] = useState(false);
  const [tagList, setTagList] = useState([]);
  const [data, setData] = useState(false);
  const [title, setTitle] = useState({
    title: '',
    preTitle: '',
    postTitle: '',
  });

  const setDataModal = (value) => {
    console.log('object', value);
    setActive(true);
    setData(value);
  };

  const setAlertModal = ({ titleText, preTitle, postTitle, mode }) => {
    setTitle({ titleText, preTitle, postTitle });
    setMode(mode);
    setActive(true);
  };

  const hadelRadio = (value) => {
    console.log('value', value);
    if (value.type === 'yes') {
      setRadioY(true);
      setRadioN(false);
    } else {
      setRadioY(false);
      setRadioN(true);
    }
  };

  const handleSelection = (updater) => (e, { value }) => updater(value);

  return (
    <>
      {lvlTwo.children.map((lvlThree, index) => (
        <div style={{ padding: 0, width: '100%' }}>
          {index !== 0 && <DividerLine />}
          <TableBody key={index}>
            <Div>
              <Cell width={350}>
                <Div col>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    {lvlThree.no} {lvlThree.name}
                  </Text>
                  {authContext.roles.isAdmin && (
                    <Div
                      className="click"
                      onClick={() => setDataModal(lvlThree)}
                    >
                      <Image
                        src="../../static/images/edit-gray@3x.png"
                        style={{ width: 18, height: 18 }}
                      />
                    </Div>
                  )}

                  <CardRow>
                    {lvlThree.tags?.map((tag) => (
                      <ButtonAll
                        style={{ margin: '4px 4px 4px 0px' }}
                        radius
                        textSize={sizes.xxs}
                        textWeight="med"
                        height={24}
                        text={tag.name}
                        textColor={colors.textPurple}
                        color={colors.purpledark}
                      />
                    ))}
                  </CardRow>
                </Div>
              </Cell>
              <Cell width={400}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {lvlThree.processDefinition}
                </Text>
              </Cell>
              <Cell width={300}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {lvlThree.value}
                </Text>
              </Cell>
              <Cell width={250}>
                {/* NOTE: แสดงตอนที่มี data จริงมาแล้ว */}
                <Div col>
                  <Div col>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Process Name
                  </Text>
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                    {lvlThree.sourceMappingName
                        ? lvlThree.sourceMappingName
                        : '-'}
                  </Text>
                </Div>
                <Div col top={16}>
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Process Definition
                  </Text>
                  <Text fontSize={sizes.xxs} color={colors.primaryBlack}>
                  {lvlThree.sourceMappingDefinition
                        ? lvlThree.sourceMappingDefinition
                        : '-'}
                  </Text>
                </Div>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {lvlThree.sourceMapping}
                  </Text>
                </Div>
              </Cell>
              {authContext.roles.isAdmin && (
                <Cell width={250}>
                  <Div>
                    {lvlThree.exactlyMatch ? (
                      <Image
                        src="../../static/images/iconChecked@3x.png"
                        style={{ width: 20, height: 20 }}
                      />
                    ) : (
                      <Div>
                        <Image
                          src="../../static/images/close@3x.png"
                          style={{ width: 20, height: 20 }}
                        />
                        <Text
                          style={{ paddingLeft: 8 }}
                          fontSize={sizes.xs}
                          color={colors.primaryBlack}
                        >
                          Devised
                        </Text>
                      </Div>
                    )}
                  </Div>
                </Cell>
              )}
              {authContext.roles.isAdmin && (
                <Cell width={250} center mid>
                  <>
                    {lvlThree.remark ? (
                      <Popup
                        wide="very"
                        style={{
                          borderRadius: '6px 6px 0px 6px',
                          padding: 16,
                          marginRight: -10,
                        }}
                        position="top right"
                        trigger={
                          <Image
                            className="click"
                            width={18}
                            src="../../static/images/markpurple@3x.png"
                          />
                        }
                      >
                        <Text fontSize={sizes.s} color={colors.primaryBlack}>
                          {lvlThree.remark}
                        </Text>
                      </Popup>
                    ) : (
                      <Image
                        width={18}
                        src="../../static/images/markgray@3x.png"
                      />
                    )}
                  </>
                </Cell>
              )}
            </Div>
            {/* {renderThree(lvlThree)} */}
            <Level3 lvlThree={lvlThree} />
          </TableBody>
        </div>
      ))}
      {active && (
        <EditProcessThreeModal
          active={active}
          onClose={() => setActive(false)}
          lvlThree={data}
        />
      )}
    </>
  );
};

export default memo(Level2);
