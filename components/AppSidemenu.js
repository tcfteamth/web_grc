import React, { useEffect, useState } from 'react';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import { Menu, Image, Segment, Label } from 'semantic-ui-react';
import Router, { useRouter } from 'next/router';
import styled from 'styled-components';
import Link from 'next/link';
import BurgerMenu from 'react-burger-menu';
import { Text } from '../components/element';
import { colors, sizes } from '../utils';
import { initAuthStore } from '../contexts';
import useWindowDimensions from '../hook/useWindowDimensions';
import AlertModel from '../model/AlertModel';

const SideMenu = styled(Menu)`
  overflow: scroll;
  border: 0px !important;
  border-radius: 0 !important;
  width: 226px !important;
  padding-top 87px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
  
`;

const BackgroundStatusMenu = styled.div`
  background-color: rgba(0, 87, 184, 0.05) !important;
  height: 56px !important;
  margin-left: 6px !important;
  flex-direction: row !important;
  display: flex !important;
  align-items: center !important;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
  justify-content: space-between;

  @media only screen and (min-width: 768px) {
    width: 287px !important;
  }

  @media only screen and (min-width: 1024px) {
    width: 287px !important;
  }

  @media only screen and (min-width: 1440px) {
    width: 98% !important;
  }
`;

const BackgroundNoStatusMenu = BackgroundStatusMenu.extend`
  background-color: ${colors.backgroundPrimary} !important;
  border-top-left-radius: 0px;
  border-bottom-left-radius: 0px;
`;

const FrameOverflowHidden = styled.div`
  overflow: hidden;
  padding-bottom: 120px;
`;

const FrameMargin = styled.div`
  margin: 8px 0px;
  margin-left: 8px;
  line-height: 2.14;
`;

const Segments = styled(Segment)`
  padding: 0px !important;
  box-shadow: none !important;
  border: none !important;
`;

const ImageIcon = styled(Image)`
  margin-left: 8px;
  width: 32px;
  height: auto;
`;

const AlertIcon = styled(Label)`
  position: absolute;
  right: 1.5em;
`;

const AppSidemenu = (props) => {
  const authContext = initAuthStore();
  const Menu = BurgerMenu['slide'];
  const [sideMenuItems, setSideMenuItems] = useState();
  const [showBurger, setShowBurger] = useState(false);
  const { width: screenWidth } = useWindowDimensions();
  const router = useRouter();
  const alertModel = useLocalStore(() => new AlertModel());

  useEffect(() => {
    (async () => {
      await alertModel.getAlert(authContext.accessToken);
      setSideMenuItems([
        {
          isShow: true,
          type: 'csa',
          menuItems: [
            {
              isShow: true,
              isPath: false,
              key: 'Database',
              label: 'Database',
              pathnameList: [
                '/databasePage/dashboard',
                '/databasePage/processMenagment',
                '/databasePage/riskAndControlManagment',
                '/databasePage/complianceMenagment',
                '/databasePage/iaFindingMenagment',
                '/databasePage/createRiskAndControlManagment',
                '/databasePage/editRiskAndControlManagment',
              ],
              iconActive: '../static/images/iconDatabase-active@3x.png',
              iconInactive: '../static/images/iconDatabase@3x.png',
              path: '/databasePage/dashboard',
              paddingLeft: 8,
            },
            {
              isShow:
                authContext.roles.isDM === false &&
                authContext.roles.isVP === false &&
                authContext.roles.isAdmin === false
                  ? false
                  : authContext.roles.isVP === true
                  ? false
                  : true,
              key: 'Roadmap',
              isPath: false,
              label: 'Roadmap',
              pathnameList: [
                '/RoadMap/roadMapList',
                '/RoadMap/roadMapAdmin',
                '/RoadMap/roadMapView',
              ],
              iconActive: '../static/images/iconRoadmap-active@3x.png',
              iconInactive: '../static/images/iconRoadmap@3x.png',
              path: '/RoadMap/roadMapList',
              paddingLeft: 8,
              alertKey: 'CSA_RoadMap'
            },
            {
              isShow: true,
              isPath: false,
              key: 'Assessment',
              label: 'Assessment',
              pathnameList: [
                '/Assessment/assessmentList',
                '/Assessment/assessmentDetail',
                '/Assessment/assessmentListVP',
                '/Assessment/assessmentDetailVP',
                '/Assessment/assessmentListIAFinding',
                '/Assessment/assessmentListDM',
              ],
              path: '/Assessment/assessmentList',
              iconActive: '../static/images/iconList-active@3x.png',
              iconInactive: '../static/images/iconAssessment@3x.png',
              paddingLeft: 8,
              alertKey: 'CSA_Assessment'
            },
            {
              isShow: true,
              isPath: false,
              key: 'Dashboard',
              label: 'Dashboard',
              pathnameList: ['/report/reportDashboard'],
              path: '/report/reportDashboard',
              iconActive: '../static/images/dashboard-sky@3x.png',
              iconInactive: '../static/images/iconDashboard@3x.png',
              paddingLeft: 8,
            },
            {
              isShow: true,
              isPath: true,
              key: 'Report',
              label: 'Report',
              pathnameList: ['/reports/csaReport'],
              path: '/reports/csaReport',
              iconActive: '../static/images/iconReport-active@3x.png',
              iconInactive: '../static/images/iconReport@3x.png',
              paddingLeft: 8,
              alertKey: 'CSA_Report'
            },
            {
              isShow: !authContext.roles.isIaFinding,
              isPath: false,
              key: 'followUp',
              label: 'Follow Up',
              pathnameList: [
                '/followUp/followUpList',
                '/followUp/followUpDetail',
              ],
              path: '/followUp/followUpList',
              iconActive: '../static/images/iconFollowUp-active@3x.png',
              iconInactive: '../static/images/iconFollowUp@3x.png',
              paddingLeft: 8,
              alertKey: 'CSA_FollowUp'
            },
            {
              isShow: !authContext.roles.isIaFinding,
              isPath: false,
              key: 'Follow Up Dashboard',
              label: 'Follow Up Dashboard',
              pathnameList: ['/followUpDashboard/followUpDashboard'],
              path: '/followUpDashboard/followUpDashboard',
              iconActive: '../static/images/dashboard-sky@3x.png',
              iconInactive: '../static/images/iconDashboard@3x.png',
              paddingLeft: 8,
            },
            {
              isShow: !authContext.roles.isIaFinding,
              isPath: true,
              key: 'Follow Up Report',
              label: 'Follow Up Report',
              pathnameList: ['/followUpReport/followUpReport'],
              path: '/followUpReport/followUpReport',
              iconActive: '../static/images/iconReport-active@3x.png',
              iconInactive: '../static/images/iconReport@3x.png',
              paddingLeft: 8,
            },
          ],
        },
        {
          isShow: !authContext.roles.isIaFinding,
          type: 'coso',
          menuItems: [
            {
              isShow: true,
              key: 'Database',
              label: 'Database',
              pathnameList: ['/COSO_Database/dashboard'],
              iconActive: '../static/images/iconDatabase-active@3x.png',
              iconInactive: '../static/images/iconDatabase@3x.png',
              path: '/COSO_Database/dashboard',
              paddingLeft: 8,
            },
            {
              isShow:
                authContext.roles.isDM === false &&
                authContext.roles.isVP === false &&
                authContext.roles.isAdmin === false
                  ? false
                  : authContext.roles.isVP === true
                  ? false
                  : true,
              key: 'Roadmap',
              label: 'Roadmap',
              pathnameList: [
                '/COSORoadMap/roadMapList',
                '/COSORoadMap/roadMapAdmin',
                '/COSORoadMap/roadMapView',
              ],
              iconActive: '../static/images/iconRoadmap-active@3x.png',
              iconInactive: '../static/images/iconRoadmap@3x.png',
              path: '/COSORoadMap/roadMapList',
              paddingLeft: 8,
              alertKey: 'COSO_RoadMap'
            },
            {
              isShow: true,
              key: 'Assessment',
              label: 'Assessment',
              pathnameList: [
                '/COSO_Assessment/COSOAssessmentList',
                '/COSO_Assessment/COSOAssessmentDetailView',
                '/COSO_Assessment/COSOAssessmentDetailPage',
              ],
              path: '/COSO_Assessment/COSOAssessmentList',
              iconActive: '../static/images/iconList-active@3x.png',
              iconInactive: '../static/images/iconAssessment@3x.png',
              paddingLeft: 8,
              alertKey: 'COSO_Assessment'
            },

            {
              isShow: authContext.roles.isAdmin,
              type: 'link',
              key: 'report',
              label: 'Report',
              pathnameList: [
                'https://pttgcgroup.sharepoint.com/:f:/s/dept_S-RC/EoZ8i8CwHT5Ou3b_yOI8Pd8BQM_0Z1rmSrewpBu-Bs9Z9w?e=Yzw8rP',
              ],
              path:
                'https://pttgcgroup.sharepoint.com/:f:/s/dept_S-RC/EoZ8i8CwHT5Ou3b_yOI8Pd8BQM_0Z1rmSrewpBu-Bs9Z9w?e=Yzw8rP',
              iconActive: '../static/images/iconReport-active@3x.png',
              iconInactive: '../static/images/iconReport@3x.png',
              paddingLeft: 8,
            },
          ],
        },
        {
          isShow: true,
          type: 'Manual',
          menuItems: [
            {
              isShow: true,
              type: 'Manual',
              key: 'CSA Manual',
              label: 'CSA Manual',
              pathnameList: ['/manualPage/dashboard'],
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              path: '/manualPage/dashboard',
              paddingLeft: 8,
            },
          ],
        },
        {
          isShow: authContext.roles.isAdmin,
          type: 'setting',
          menuItems: [
            {
              isShow:
                authContext.roles.isDM === false &&
                authContext.roles.isVP === false &&
                authContext.roles.isAdmin === false
                  ? false
                  : authContext.roles.isAdmin === true
                  ? true
                  : false,
              key: 'RoleManagement',
              label: 'Role Management',
              pathnameList: [
                '/roleManagement/RoleList',
                '/roleManagement/RoleCreate',
                '/roleManagement/RoleEdit',
              ],
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              path: '/roleManagement/RoleList',
              paddingLeft: 8,
            },
            {
              isShow: true,
              key: 'Setting Task',
              label: 'Setting Task',
              pathnameList: ['/setting/settingTask'],
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              path: '/setting/settingTask',
              paddingLeft: 8,
            },
            {
              isShow: true,
              key: 'Tag Management',
              label: 'Tag Management',
              pathnameList: ['/setting/settingDatabase'],
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              path: '/setting/settingDatabase',
              paddingLeft: 8,
            },
            {
              isShow: true,
              key: 'Manual Assign',
              label: 'Manual Assign',
              pathnameList: ['/setting/manualAssign'],
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              path: '/setting/manualAssign',
              paddingLeft: 8,
            },
            {
              isShow: true,
              isPath: true,
              key: 'ReAssessment',
              label: 'Re Assessment',
              pathnameList: [
                '/Re_Assessment/ReassessmentList'],
              path: '/Re_Assessment/ReassessmentList',
              iconActive: '../static/images/roleactive@3x.png',
              iconInactive: '../static/images/roleInactive@3x.png',
              paddingLeft: 8,
            },
          ],
        },
      ]);
    })();
  }, []);

  useEffect(() => {
    if (sideMenuItems) {
      sideMenuItems.every((child) =>
        child.menuItems.find((e) =>
          e.pathnameList.find((v) => {
            if (v === router.asPath) {
              if (e.isPath) {
                setShowBurger(true);
                var element = document.getElementById('change-pageWrapper');
                if (element) {
                  element.classList.remove('PageWrapper');
                  element.classList.add('PageWrapper-page');
                }
              }
            }
          }),
        ),
      );
    }
  }, [router, sideMenuItems]);

  const renderList = ({
    key,
    pathnameList = [],
    path,
    label,
    paddingLeft,
    iconActive,
    iconInactive,
    type,
    alertKey,
  }) => {
    let focused = pathnameList.includes(props.url.pathname);

    return (
      <FrameMargin key={key}>
        {type === 'link' ? (
          <a href={path} target="_blank">
            {focused ? (
              <BackgroundStatusMenu>
                <div style={{ alignItems: 'center', display: 'flex', position: 'relative', width: 'inherit' }}>
                  <ImageIcon src={iconActive} />
                  <Text
                    fontSize={sizes.s}
                    color={colors.primaryBlack}
                    fontWeight={'med'}
                    style={{
                      paddingLeft,
                      fontStretch: 'normal',
                      fontStyle: 'normal',
                    }}
                  >
                    {label}
                  </Text>
                  { (alertKey && alertModel[alertKey]) ? (
                    <AlertIcon circular color={'red'} key={key+'alert'}>
                      {alertModel[alertKey]}
                    </AlertIcon>
                  ) : ''}
                </div>
                <div
                  style={{
                    backgroundColor: colors.primary,
                    width: 4,
                    height: '100%',
                  }}
                />
              </BackgroundStatusMenu>
            ) : (
              <BackgroundNoStatusMenu>
                <div style={{ alignItems: 'center', display: 'flex', position: 'relative', width: 'inherit' }}>
                  <ImageIcon src={iconInactive} />
                  <Text
                    fontSize={sizes.s}
                    fontWeight={'med'}
                    color={colors.textlightGray}
                    style={{
                      paddingLeft,
                      fontStretch: 'normal',
                      fontStyle: 'normal',
                    }}
                  >
                    {label}
                  </Text>
                  { (alertKey && alertModel[alertKey]) ? (
                    <AlertIcon circular color={'red'} key={key+'alert'}>
                      {alertModel[alertKey]}
                    </AlertIcon>
                  ) : ''}
                </div>
                <div />
              </BackgroundNoStatusMenu>
            )}
          </a>
        ) : (
          <Link href={path}>
            <a>
              {focused ? (
                <BackgroundStatusMenu>
                  <div style={{ alignItems: 'center', display: 'flex', position: 'relative', width: 'inherit' }}>
                    <ImageIcon src={iconActive} />
                    <Text
                      fontSize={sizes.s}
                      color={colors.primaryBlack}
                      fontWeight={'med'}
                      style={{
                        paddingLeft,
                        fontStretch: 'normal',
                        fontStyle: 'normal',
                      }}
                    >
                      {label}
                    </Text>
                    { (alertKey && alertModel[alertKey]) ? (
                      <AlertIcon circular color={'red'} key={key+'alert'}>
                        {alertModel[alertKey]}
                      </AlertIcon>
                    ) : ''}
                  </div>
                  <div
                    style={{
                      backgroundColor: colors.primary,
                      width: 4,
                      height: '100%',
                    }}
                  />
                </BackgroundStatusMenu>
              ) : (
                <BackgroundNoStatusMenu>
                  <div style={{ alignItems: 'center', display: 'flex', position: 'relative', width: 'inherit' }}>
                    <ImageIcon src={iconInactive} />
                    <Text
                      fontSize={sizes.s}
                      fontWeight={'med'}
                      color={colors.textlightGray}
                      style={{
                        paddingLeft,
                        fontStretch: 'normal',
                        fontStyle: 'normal',
                      }}
                    >
                      {label}
                    </Text>
                    { (alertKey && alertModel[alertKey]) ? (
                      <AlertIcon circular color={'red'} key={key+'alert'}>
                        {alertModel[alertKey]}
                      </AlertIcon>
                    ) : ''}
                  </div>
                  <div />
                </BackgroundNoStatusMenu>
              )}
            </a>
          </Link>
        )}
      </FrameMargin>
    );
  };

  return (
    <div>
      {!showBurger || screenWidth <= 1280 ? (
        <SideMenu className="menu-slide-desktop" vertical fixed="left">
          <Segments style={{ overflow: 'overlay', maxHeight: '100%' }}>
            <FrameOverflowHidden>
              {sideMenuItems &&
                sideMenuItems.map((item,i) => (
                  <div key={i}>
                    {item.type ? (
                      <div>
                        {item.isShow && (
                          <Text
                            fontSize={sizes.s}
                            color={colors.primaryBlack}
                            fontWeight={'bold'}
                            style={{
                              fontStretch: 'normal',
                              paddingTop: item.type === 'csa' ? 30 : 24,
                              paddingLeft: 28,
                              textTransform: 'uppercase',
                            }}
                          >
                            {item.type}
                          </Text>
                        )}

                        {item.isShow
                          ? item.menuItems &&
                            item.menuItems.map((i) =>
                              i.isShow ? renderList(i) : null,
                            )
                          : ''}
                      </div>
                    ) : null}
                  </div>
                ))}
            </FrameOverflowHidden>
          </Segments>
        </SideMenu>
      ) : (
        ''
      )}

      {showBurger || screenWidth <= 1280 ? (
        <div id="outer-container">
          <Menu
            id={'slide'}
            pageWrapId={'page-wrap'}
            outerContainerId={'outer-container'}
          >
            <div style={{ padding: 16 }}>
              <Image
                src={'../static/images/gc-logo-4-x@3x.png'}
                style={{ width: 'auto', height: 52 }}
              />
            </div>
            {sideMenuItems &&
              sideMenuItems.map((item) => (
                <div>
                  {item.type && (
                    <div>
                      {item.isShow && (
                        <>
                          <Text
                            fontSize={sizes.s}
                            color={colors.primaryBlack}
                            fontWeight={'bold'}
                            style={{
                              fontStretch: 'normal',
                              paddingTop: 24,
                              paddingLeft: 28,
                              textTransform: 'uppercase',
                            }}
                          >
                            {item.type}
                          </Text>
                          {item.isShow
                            ? item.menuItems &&
                              item.menuItems.map((i) =>
                                i.isShow ? renderList(i) : null,
                              )
                            : ''}
                        </>
                      )}
                    </div>
                  )}
                </div>
              ))}
          </Menu>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default AppSidemenu;
