import React, { useState, useEffect } from 'react';
import { useObserver } from 'mobx-react-lite';
import {
  Pagination,
  Icon,
  Grid,
  Image,
  Loader,
  Dimmer,
  Popup
} from 'semantic-ui-react';
import Router from 'next/router';
import styled from 'styled-components';
import moment from 'moment';
import { Text, TabScrollBar, Cell } from '../../element';
import { ReadyModal } from '../../AssessmentPage/modal';
import { colors, sizes } from '../../../utils';
import { headersRequestAdmin } from '../../../utils/static';
import { initAuthStore, initDataContext } from '../../../contexts';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};

  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RoadMapRequeste = ({ requestModel }) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();
  const [isOpenRead, setOpenRead] = useState(false);
  const [isOpenReadID, setOpenReadID] = useState();
  const [isOpenReadStatus, setOpenReadStatus] = useState();

  const handelSubmitRead = async () => {
    await requestModel.submitReadRequest(authContext.accessToken, isOpenReadID);
    await requestModel.getRequestList(
      authContext.accessToken,
      requestModel.page,
    );
    setOpenRead(false);
  };

  const handelOpenRead = (value, id, status) => {
    setOpenRead(value);
    setOpenReadID(id);
    setOpenReadStatus(status);
  };

  useEffect(() => {
    return function cleanup() {
      dataContext.page = '';
    };
  }, []);

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersRequestAdmin.map(
              ({ render, key, width, widthMobile, center }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={center}
                  mid
                >
                  <Text
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                  </Text>
                </Cell>
              ),
            )}
          </TableHeader>
          {requestModel &&
            requestModel.requests.map((request, index) => (
              <TableBody>
                <Cell center width={screenWidth < 1024 ? 30 : 3}>
                  <div
                    className="click"
                    onClick={() =>
                      handelOpenRead(
                        !isOpenRead,
                        request.id,
                        request.isComplete,
                      )
                    }
                  >
                    <Image
                      src={
                        !request.isComplete
                          ? '../../static/images/tick@3x.png'
                          : '../../static/images/tick-active@3x.png'
                      }
                      style={{ width: 18, height: 18 }}
                    />
                  </div>
                </Cell>
                <Cell center width={screenWidth < 1024 ? 50 : 5}>
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {requestModel.page > 1
                      ? `${requestModel.page - 1}${index + 1}`
                      : index + 1}
                  </Text>
                </Cell>
                <Cell
                  width={screenWidth < 1024 ? 620 : 62}
                  style={{ paddingRight: 24 }}
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {request.detail}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10}>
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={colors.primaryBlack}
                  >
                    {moment(request.createdDate).locale('th').format('L')}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10}>
                  <Text
                    fontWeight="bold"
                    fontSize={sizes.xs}
                    color={colors.primaryBlack}
                  >
                    {request.firstName}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10}>
                  <Popup
                    trigger={
                    <Text
                      fontWeight="bold"
                      fontSize={sizes.xs}
                      color={colors.primaryBlack}
                    >
                      {request.deptDiv}
                    </Text>
                    }
                    content={`${request.divEn || '-'} ${request.shiftEn || ''}`}
                    size="small"
                  />
                </Cell>
              </TableBody>
            ))}
        </Div>
      </TabScrollBar>

      {requestModel.totalPage > 1 && (
        <Grid style={{ marginTop: 14 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', right: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              defaultActivePage={requestModel.page}
              onPageChange={(e, { activePage }) => {
                requestModel.getRequestList(
                  authContext.accessToken,
                  activePage,
                );
                requestModel.setField('page', activePage);
              }}
              firstItem={null}
              lastItem={null}
              nextItem={{
                content: <Icon name="angle right" />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={requestModel.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
      <ReadyModal
        active={isOpenRead}
        id={isOpenReadID}
        status={isOpenReadStatus}
        onClose={() => setOpenRead(false)}
        onSubmit={handelSubmitRead}
      />

      {/* <BottomBarRoadmap page={'listRequests'} requestModel={requestModel} /> */}
    </div>
  ));
};

export default RoadMapRequeste;
