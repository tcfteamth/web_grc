import React, { useEffect } from 'react';
import { useObserver } from 'mobx-react-lite';
import { Icon, Grid, Pagination } from 'semantic-ui-react';
import styled from 'styled-components';
import { CheckBoxAll, Text, TabScrollBar, Cell } from '../../element';
import { colors, sizes } from '../../../utils';
import { headersAssessmentAdmin } from '../../../utils/static';
import { initDataContext, initAuthStore } from '../../../contexts';
import { RowRoadMapListAdmin } from '..';
import useWindowDimensions from '../../../hook/useWindowDimensions';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100% !important;
  min-height: 50px;
  display: flex;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  padding: 0px !important;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px !important;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const TableBody = TableHeader.extend`
  min-height: 62px;
  margin: 8px 0px 0px;
  padding: 16px 24px !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RoadMapAssessmentAdmin = ({ roadmapData }) => {
  const dataContext = initDataContext();
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();

  useEffect(
    () =>
      function cleanup() {
        dataContext.page = '';
      },
    [],
  );

  return useObserver(() => (
    <div style={{ width: '100%' }}>
      <TabScrollBar>
        <Div col>
          <TableHeader>
            {headersAssessmentAdmin.map(
              ({ render, key, width, widthMobile, center }) => (
                <Cell
                  key={key}
                  width={screenWidth < 1024 ? widthMobile : width}
                  center={center}
                >
                  {render === 'Accept' ? (
                    <CheckBoxAll
                      header
                      status={render}
                      onChange={() => roadmapData.checkedAll()}
                      checked={roadmapData.isChckedAll}
                    />
                  ) : (
                    <Div center mid={key !== 'Assessment'}>
                      {key === 'View' ? (
                        <Text
                          center
                          fontSize={sizes.xs}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                          <br />
                          <span>(Last Year)</span>
                        </Text>
                      ) : (
                        <Text
                          center
                          fontSize={sizes.xs}
                          fontWeight="med"
                          color={colors.textlightGray}
                        >
                          {render}
                        </Text>
                      )}
                    </Div>
                  )}
                </Cell>
              ),
            )}
          </TableHeader>
          {roadmapData.list.map((roadmap, index) => (
            <TableBody key={index}>
              <RowRoadMapListAdmin
                roadmap={roadmap}
                roadmapData={roadmapData}
              />
            </TableBody>
          ))}
        </Div>
      </TabScrollBar>

      {roadmapData.totalPage > 1 && (
        <Grid style={{ paddingTop: 14 }}>
          <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
            <Pagination
              style={{
                fontSize: sizes.xs,
                fontWeight: 'bold',
                borderColor: colors.backgroundSecondary,
              }}
              activePage={roadmapData.page}
              firstItem={null}
              lastItem={null}
              onPageChange={(e, { activePage }) => {
                roadmapData.getRoadmapList(
                  authContext.accessToken,
                  activePage,
                  roadmapData.searchRoadmapText,
                  roadmapData.filterModel.selectedLvl3,
                  roadmapData.filterModel.selectedLvl4,
                  roadmapData.filterModel.selectedBu,
                  roadmapData.filterModel.selectedRoadmapType,
                  roadmapData.filterModel.selectedDepartment.no,
                  roadmapData.filterModel.selectedDivision,
                  roadmapData.filterModel.selectedShift,
                  roadmapData.filterModel.selectedStatus,
                  roadmapData.filterModel.selectedYear,
                );
              }}
              nextItem={{
                content: <Icon name="angle right"  />,
                icon: true,
              }}
              prevItem={{
                content: <Icon name="angle left" />,
                icon: true,
              }}
              totalPages={roadmapData.totalPage}
            />
          </Grid.Row>
        </Grid>
      )}
    </div>
  ));
};

export default RoadMapAssessmentAdmin;
