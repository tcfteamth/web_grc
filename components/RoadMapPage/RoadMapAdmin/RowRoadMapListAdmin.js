import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useObserver, useLocalStore } from 'mobx-react-lite';
import Router from 'next/router';
import Link from 'next/link';
import { Image, Popup } from 'semantic-ui-react';
import { toJS } from 'mobx';
import { initAuthStore } from '../../../contexts';
import {
  CheckBoxAll,
  Text,
  Remark,
  RoadmapNextYear,
  Cell,
  ModalGlobal,
} from '../../element';
import request from '../../../services';
import loadingStore from '../../../contexts/LoadingStore';
import { colors, sizes } from '../../../utils';
import { EditRoadmapModal } from '../modal';
import { RoadmapListModel } from '../../../model';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RejectComment = styled.div`
  display: flex;
  border-radius: 6px 6px 0px 6px;
  border: 2px solid #eaeaea;
  height: 38px;
  color: red;
  width: 100%;
  padding: 4px 16px;
  align-items: center;
  font-size: ${sizes.xs}px;
`;

const RowRoadMapListAdmin = ({ roadmap, roadmapData }) => {
  const roadmapModel = useLocalStore(() => new RoadmapListModel());
  const authContext = initAuthStore();
  const [screenWidth, setScreenWidth] = useState();
  const [isOpen, setIsOpen] = useState(true);
  const [active, setActive] = useState(false);
  const [confirm, setConfirm] = useState(false);

  const handleReF = async () => {
    roadmapData.getRoadmapList(
      authContext.accessToken,
      roadmapData.page,
      roadmapData.searchRoadmapText,
      roadmapData.filterModel.selectedLvl3,
      roadmapData.filterModel.selectedLvl4,
      roadmapData.filterModel.selectedBu,
      roadmapData.filterModel.selectedRoadmapType,
      roadmapData.filterModel.selectedDepartment.no,
      roadmapData.filterModel.selectedDivision,
      roadmapData.filterModel.selectedShift,
      roadmapData.filterModel.selectedStatus,
      roadmapData.filterModel.selectedYear,
    );
  };

  const handleDeleteRow = async () => {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.roadmapServices.submitDeleteRoadmap(
        roadmapData.ids,
      );
      setConfirm(false);
      loadingStore.setIsLoading(false);
      handleReF();
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
    setConfirm(false);
  };

  const handleUpdateRow = async (id) => {
    await roadmapData.setField('ids', id);
    await setActive(true);
  };

  useEffect(() => {
    setScreenWidth(window.screen.width);
    window.addEventListener('resize', () => setScreenWidth(window.innerWidth));
  }, [screenWidth]);

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell width={4}>
          <CheckBoxAll
            header
            status="Accept"
            checked={roadmap.isAllSubmitRoadmapIdsCheked}
            onChange={() => roadmap.setAllSubmitRoadmapIds()}
          />
        </Cell>
        <Cell width={93}>
          <Text
            fontWeight="bold"
            fontSize={sizes.s}
            color={colors.primaryBlack}
          >
            {`${roadmap.no} ${roadmap.name}`}
          </Text>
        </Cell>
        <Cell center width={3}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>

      {roadmap.children.map((item, index) => (
        <Div col>
          {isOpen && (
            <Div style={{ padding: '16px 0px 0px' }}>
              <Cell width={4}>
                <CheckBoxAll
                  status="Accept"
                  onChange={(e) => {
                    item.submitRoadmaps.setRoadmapIds(item.id);
                  }}
                  checked={item.submitRoadmaps.isSelected}
                />
              </Cell>
              <Cell width={40}>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {`${item.no} ${item.name}`}
                </Text>
              </Cell>
              <Cell width={16} center mid>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {item.latestYear}
                </Text>
              </Cell>
              <Cell width={10} center mid>
                <Popup
                  trigger={
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {item.depDiv}
                    </Text>
                  }
                  content={`${item.divEn || '-'} ${item.shiftEn || ''}`}
                  size="small"
                />
              </Cell>
              <Cell width={16} center mid>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {item.status}
                </Text>
              </Cell>
              <Cell width={8} center mid>
                <a
                  href={item.lastYearPath || false}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Image
                    src={
                      item.lastYearPath
                        ? '../../static/images/file-purple@3x.png'
                        : '../../static/images/file@3x.png'
                    }
                    style={{ width: 18, height: 18 }}
                    className="click"
                  />
                </a>
              </Cell>
              <Cell width={8} center>
                <Remark remark={item.remark} />
              </Cell>
              <Cell width={8} center>
                <Popup
                  wide="very"
                  style={{
                    borderRadius: '6px 6px 0px 6px',
                    padding: 16,
                    marginLeft: -10,
                  }}
                  trigger={
                    item.roadMapImage ? (
                      <Image
                        src={item.roadMapImage}
                        style={{ width: 18, height: 18 }}
                      />
                    ) : (
                      <RoadmapNextYear year={item.year} />
                    )
                  }
                >
                  <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                    {item.roadMapType}
                  </Text>
                </Popup>
              </Cell>
              <Cell width={4} center>
                {item.status !== 'WAIT_DM_ACCEPT' ? (
                  <div onClick={() => handleUpdateRow(item.id)}>
                    <Image
                      className="click"
                      src="../../static/images/edit@3x.png"
                      style={{ width: 18, height: 18 }}
                    />
                  </div>
                ) : (
                  <Image
                    disabled
                    src="../../static/images/edit-gray@3x.png"
                    style={{ width: 18, height: 18 }}
                  />
                )}
              </Cell>
              <Cell width={screenWidth < 1024 ? 40 : 4} center>
                {item.status !== 'WAIT_DM_ACCEPT' ? (
                  <div
                    onClick={() => {
                      roadmapData.setField('ids', item.id);
                      setConfirm(true);
                    }}
                  >
                    <Image
                      className="click"
                      src="../../static/images/delete@2x.png"
                      style={{ width: 20, height: 20 }}
                    />
                  </div>
                ) : (
                  <Image
                    disabled
                    src="../../static/images/delete-gray@3x.png"
                    style={{ width: 20, height: 20 }}
                  />
                )}
              </Cell>
            </Div>
          )}

          {item.status === 'DM_REJECT' && (
            <Div
              style={{
                paddingLeft: screenWidth <= 1366 ? '42px' : '64px',
                margin: '8px 0px 0px',
              }}
            >
              <RejectComment>{item.rejectComment}</RejectComment>
            </Div>
          )}
        </Div>
      ))}

      {roadmapData.ids && (
        <EditRoadmapModal
          active={active}
          roadmap={roadmapData}
          onClose={() => setActive(false)}
          token={authContext.accessToken}
          onSubmit={() => setActive(false)}
        />
      )}

      {/* call Back */}
      <ModalGlobal
        open={confirm}
        title="Do you want to Delete ?"
        content="Please make sure to specify that you want to delete the information."
        onSubmit={handleDeleteRow}
        submitText="YES"
        cancelText="NO"
        onClose={() => setConfirm(false)}
      />
    </Div>
  ));
};

export default RowRoadMapListAdmin;
