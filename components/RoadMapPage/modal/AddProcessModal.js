import React, { useState } from 'react';
import styled from 'styled-components';
import { Modal } from 'semantic-ui-react';
import { ButtonAll, ButtonBorder, Text, TextArea } from '../../element';
import { colors, sizes } from '../../../utils';
import { useObserver } from 'mobx-react-lite';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose, onSubmitRequest }) => {
  const [newRequest, setNewRequest] = useState('');

  const onClearState = () => {
    setNewRequest('');
  };

  const onModalOpen = () => {
    onClearState();
  };

  const onModalClose = () => {
    if (onClose) {
      onClose();
    }
    onClearState();
  };

  const onSubmit = () => {
    onSubmitRequest(newRequest);
  };

  return (
    <ModalBox
      open={active}
      onClose={onModalClose}
      size="medium"
      closeOnDimmerClick
      onOpen={onModalOpen}
    >
      <div>
        <Text fontSize={sizes.xxl} color={colors.textSky}>
          Request New Process
        </Text>
      </div>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <div>
          <Text fontSize={sizes.s} color={colors.textDarkBlack}>
            Request New Sub-Process
          </Text>
          <TextArea
            height={100}
            placeholder="Fill scope of work"
            maxLength={4000}
            onChange={(e) => {
              setNewRequest(e.target.value);
            }}
            value={newRequest}
          />
        </div>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={onModalClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="submit"
          disabled={!newRequest}
          onClick={() => onSubmit()}
        />
      </div>
    </ModalBox>
  );
};

export default index;
