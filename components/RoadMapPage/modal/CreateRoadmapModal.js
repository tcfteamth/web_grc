import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form } from 'semantic-ui-react';
import { useObserver } from 'mobx-react-lite';
import Router from 'next/router';
import {
  Text,
  DropdownAll,
  InputAll,
  ButtonAll,
  ButtonBorder,
  ModalGlobal,
  TextArea
} from '../../element';
import { Modal } from 'semantic-ui-react';
import { colors, sizes } from '../../../utils';
import request from '../../../services';

const InputDefault = styled(InputAll)``;

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
  padding-left: ${(props) => props.left || 0}px;
  padding-top: ${(props) => props.top || 0}px;
  padding-right: ${(props) => props.right || 0}px;
  padding-bottom: ${(props) => props.bottom || 0}px;
  margin-top: ${(props) => props.marginTop || 0}px;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.mid
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
`;

const initYear = () => {
  const getYear = new Date().getFullYear();
  const years = Array.from(new Array(10), (val, index) => getYear + index);
  const yearList = years.map((item) => ({
    value: new Date(`${item}`),
    label: item,
  }));

  return yearList;
};

const index = ({ active, onClose }) => {
  const [remarkDetail, setRemarkDetail] = useState('');
  const [isIcChecked, setIsIcChecked] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);

  const [selectedYear, setSelectedYear] = useState();

  const handleSelectedYear = (e) => {
    setSelectedYear(e);
    console.log(e);
  };

  const [departmentList, setDepartmentList] = useState();

  const getDepartmentList = async () => {
    await request.roadmapServices
      .getDepartmentList()
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
        }));

        setDepartmentList(deparmentOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getDepartmentList();
  }, []);

  const [selectedDepartment, setSelectedDepartMent] = useState();

  const handleSelectedDepartment = (e) => {
    setSelectedDepartMent(e);

    if (e.value) {
      setSelectDivision(false);
      setSelectShift(false);
      setSelectedShift(null);
      setSelectedDivision(null);

      getDivisionById(e.value);
      getShiftByDepartmentId(e.value);
    }
  };

  const [divisionList, setDivisionList] = useState();

  const getShiftByDepartmentId = async (id) => {
    await request.roadmapServices
      .getShiftByDepartmentId(id)
      .then((response) => {
        const shiftOptions = response.data.map((shift) => ({
          value: shift.id,
          label: shift.nameEn,
        }));

        setShiftList(shiftOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [selectedDivision, setSelectedDivision] = useState();
  const [isSelectDivision, setSelectDivision] = useState(false);
  const [isSelectShift, setSelectShift] = useState(false);


  const handleSelectedDivision = (e) => {
    setSelectedDivision(e);
    setSelectedShift(null);
    setSelectDivision(true);
    setSelectShift(false);
    console.log('selectedDivision', selectedDivision);
  };

  const [shiftList, setShiftList] = useState();

  const [selectedShift, setSelectedShift] = useState();
  const handleSelectedShift = (e) => {
    setSelectedShift(e);
    setSelectedDivision(null);
    setSelectShift(true);
    setSelectDivision(false);
    console.log('selectedShift', selectedShift);
  };
  const getDivisionById = async (id) => {
    await request.roadmapServices
      .getDivisionById(id)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
        }));

        setDivisionList(divisionOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const [lvlThreeList, setLvlThreeList] = useState();

  const getLvlThreeList = async () => {
    await request.roadmapServices
      .getLvlThreeList()
      .then((response) => {
        const lvlThreeOptions = response.data.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.no} ${lvlThree.name}`,
        }));

        setLvlThreeList(lvlThreeOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getLvlThreeList();
  }, []);

  const [selectedLvlThree, setSelectedLvlThree] = useState();

  const handleSelectedLvlThree = (e) => {
    setSelectedLvlThree(e);
    setSelectedLvlFour(null);
    // console.log(e.value);
    getLvlFourList(e.value);
  };

  const [lvlFourList, setLvlFourList] = useState();

  const getLvlFourList = async (lvlThreeNo) => {
    await request.roadmapServices
      .getLvlFourList(lvlThreeNo)
      .then((response) => {
        const lvlFourOptions = response.data.map((lvlFour) => ({
          value: lvlFour.no,
          label: `${lvlFour.no} ${lvlFour.name}`,
        }));

        console.log('getLvlFourList', response.data);
        setLvlFourList(lvlFourOptions);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [selectedLvlFour, setSelectedLvlFour] = useState();

  const handleSelectedLvlFour = (e) => {
    setSelectedLvlFour(e);
  };

  const submitCreateRoadmap = async () => {
    const formData = new FormData();
    formData.append('year', selectedYear.label);
    if(selectedDivision){
      formData.append('divisionId', selectedDivision.value);
    }
    if(selectedShift){
      formData.append('shiftId', selectedShift.value);
    }
    formData.append('noLvl4', selectedLvlFour.value);
    formData.append('remark', remarkDetail);
    formData.append('isICAgentPlus', isIcChecked);

    await request.roadmapServices
      .submitCreateRoadmap(formData)
      .then((response) => {
        console.log(response);
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        setIsShowModal(true);
        console.log(e);
        onClose(false);
      });
  };

  const isSubmitReady = () => {
    
    if (selectedYear && (selectedDivision || selectedShift) && selectedLvlFour) {
      return false;
    }

    return true;
  };

  const handleClose = () => {
    onClose(false);
  };

  return useObserver(() => (
    <div>
      <ModalBox
        size={'medium'}
        open={active}
        onClose={handleClose}
        // closeOnDimmerClick={closeOnDimmerClick}
      >
        <div>
          <Text fontSize={sizes.xxl} color={colors.textSky}>
            {'Create RoadMap'}
          </Text>
        </div>
        <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
          <div>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column computer={4} tablet={4} mobile={16}>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Year
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    options={initYear()}
                    handleOnChange={(e) => {
                      handleSelectedYear(e);
                    }}
                    value={selectedYear}
                  />
                </Grid.Column>
                <Grid.Column computer={4} tablet={8} mobile={16}>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Department
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    options={departmentList && departmentList}
                    handleOnChange={(e) => {
                      console.log(e);
                      handleSelectedDepartment(e);
                    }}
                    value={selectedDepartment}
                  />
                </Grid.Column>
                <Grid.Column computer={4} tablet={8} mobile={16}>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Division
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                      handleSelectedDivision(e);
                    }}
                    options={divisionList && divisionList}
                    value={selectedDivision}
                    isDisabled={isSelectShift}
                  />
                </Grid.Column>
                <Grid.Column computer={4} tablet={8} mobile={16} >
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                    style={{ paddingBottom: 8 }}
                  >
                    Shift
                  </Text>
                  <DropdownAll
                    placeholder="Select"
                    handleOnChange={(e) => {
                      handleSelectedShift(e);
                    }}
                    options={shiftList && shiftList}
                    value={selectedShift}
                    isDisabled={isSelectDivision}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Process (Level 3)
                  </Text>
                  <DropdownAll
                    placeholder="Select Process"
                    options={lvlThreeList}
                    value={selectedLvlThree}
                    handleOnChange={(e) => handleSelectedLvlThree(e)}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Sub-Process (Level 4)
                  </Text>
                  <DropdownAll
                    placeholder="Select Sub-Process"
                    options={lvlFourList && lvlFourList}
                    value={selectedLvlFour}
                    handleOnChange={(e) => {
                      handleSelectedLvlFour(e);
                    }}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    Remark
                  </Text>
                  <Form>
                    <TextArea
                      key="remark"
                      placeholder="Remark"
                      value={remarkDetail}
                      maxLength={4000}
                      onChange={(e) => {
                        setRemarkDetail(e.target.value);
                      }}
                    />
                  </Form>
                </Grid.Column>
              </Grid.Row>

              {/* <Grid.Row>
                <Grid.Column>
                  <Text
                    fontSize={sizes.xxs}
                    fontWeight="bold"
                    color={colors.primaryBlack}
                  >
                    IC Agent+
                  </Text>
                  <div>
                    <Grid columns="equal">
                      <Grid.Row>
                        <Grid.Column>
                          <Div center top={4}>
                            <RadioBox
                              label="YES"
                              checked={isIcChecked}
                              onChange={handleSetIcChecked}
                            />
                            <div style={{ paddingLeft: 24 }}>
                              <RadioBox
                                label="No"
                                checked={!isIcChecked}
                                onChange={handleSetIcChecked}
                              />
                            </div>
                          </Div>
                        </Grid.Column>
                        <Grid.Column />
                      </Grid.Row>
                    </Grid>
                  </div>
                </Grid.Column>
              </Grid.Row> */}
            </Grid>
          </div>
        </Modal.Content>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
          }}
        >
          <ButtonBorder
            width={140}
            style={{ marginRight: 20 }}
            handelOnClick={handleClose}
            borderColor={colors.textlightGray}
            textColor={colors.primaryBlack}
            textUpper
            textWeight="med"
            text={'cancel'}
          />
          <ButtonAll
            width={140}
            onClick={submitCreateRoadmap}
            color={
              isSubmitReady() === 'disabled' ? colors.btGray : colors.primary
            }
            textColor={colors.backgroundPrimary}
            textUpper
            textWeight="med"
            text={'Create Now'}
            disabled={isSubmitReady()}
          />
        </div>
      </ModalBox>

      <ModalGlobal
          open={isShowModal}
          title="Create Roadmap"
          content={'roadmap นี้เคยถูกสร้างไว้แล้ว'}
          onClose={() => setIsShowModal(false)}
        />
    </div>
  ));
};

export default index;
