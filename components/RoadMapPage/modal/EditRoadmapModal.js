import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Grid, Form, Modal } from 'semantic-ui-react';
import { useObserver, useLocalStore } from 'mobx-react';
import Router from 'next/router';

import {
  Text,
  DropdownAll,
  InputAll,
  ButtonBorder,
  ButtonAll,
  TextArea,
} from '../../element';
import loadingStore from '../../../contexts/LoadingStore';
import { colors, sizes } from '../../../utils';
import request from '../../../services';
import { RoadmapListModel } from '../../../model';

const ModalBox = styled(Modal)`
  border-radius: 6px 6px 0px 6px !important;
  padding: 32px;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;

const index = ({ active, onClose, roadmap, token, onSubmit }) => {
  const roadmapModel = useLocalStore(() => new RoadmapListModel());
  const [yearList, setYearList] = useState();
  const [departmentList, setDepartmentList] = useState();
  const [divisionList, setDivisionList] = useState();
  const [shiftList, setShiftList] = useState();

  const [isSelectDivision, setSelectDivision] = useState(false);
  const [isSelectShift, setSelectShift] = useState(false);
  const initYear = () => {
    const getYear = new Date().getFullYear();
    const years = Array.from(new Array(10), (val, index) => getYear + index);
    const yearList = years.map((item) => ({
      value: new Date(`${item}`),
      label: item,
    }));
    setYearList(yearList);
    roadmapModel.setField(
      'year',
      yearList.find((e) => e.label === roadmap.year),
    );
  };

  const getDepDiv = async () => {
    await request.roadmapServices
      .getDepartmentList()
      .then((response) => {
        const deparmentOptions = response.data.map((department) => ({
          value: department.id,
          label: department.departmentNameTh,
          departmentNo: department.departmentNo,
        }));
        roadmapModel.setField(
          'departmentNo',
          deparmentOptions.find((e) => e.departmentNo === roadmap.departmentNo),
        );
        setDepartmentList(deparmentOptions);
        if(roadmap.departmentId || roadmap.departmentId != '0'){
          getDivisionById(roadmap.departmentId);
          getShiftByDepartmentId(roadmap.departmentId);
        }else if(roadmap.divisionNo){
          getDivisionByDivisionNo(roadmap.divisionNo);
        }else if(roadmap.shiftNo){
          getShiftByShiftNo(roadmap.shiftNo);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getDivisionById = async (id) => {
    await request.roadmapServices
      .getDivisionById(id)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
          no: division.no,
        }));
        setDivisionList(divisionOptions);
        roadmapModel.setField(
          'divisionNo',
          divisionOptions.find((e) => e.no === roadmap.divisionNo),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getShiftByDepartmentId = async (id) => {
    await request.roadmapServices
      .getShiftByDepartmentId(id)
      .then((response) => {
        const shiftOptions = response.data.map((shift) => ({
          value: shift.id,
          label: shift.nameEn,
          no: shift.no,
        }));

        setShiftList(shiftOptions);
        roadmapModel.setField(
          'shiftNo',
          shiftOptions.find((e) => e.no === roadmap.shiftNo),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getDivisionByDivisionNo = async (no) => {
    await request.roadmapServices
      .getDivisionByDivisionNo(no)
      .then((response) => {
        const divisionOptions = response.data.map((division) => ({
          value: division.id,
          label: division.nameEn,
          no: division.no,
        }));
        console.log(divisionOptions);
        setDivisionList(divisionOptions);
        roadmapModel.setField(
          'divisionNo',
          divisionOptions.find((e) => e.no === roadmap.divisionNo),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };


  const getShiftByShiftNo = async (no) => {
    await request.roadmapServices
      .getShiftByShiftNo(no)
      .then((response) => {
        const shiftOptions = response.data.map((shift) => ({
          value: shift.id,
          label: shift.nameEn,
          no: shift.no,
        }));

        setShiftList(shiftOptions);
        roadmapModel.setField(
          'shiftNo',
          shiftOptions.find((e) => e.no === roadmap.shiftNo),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [lvlThreeList, setLvlThreeList] = useState();

  const getLvlThreeList = async () => {
    await request.roadmapServices
      .getLvlThreeList()
      .then((response) => {
        const lvlThreeOptions = response.data.map((lvlThree) => ({
          value: lvlThree.no,
          label: `${lvlThree.no} ${lvlThree.name}`,
        }));
        setLvlThreeList(lvlThreeOptions);
        roadmapModel.setField(
          'lvl3',
          lvlThreeOptions.find((e) => e.value === roadmap.lvl3),
        );
        getLvlFourList(roadmap.lvl3);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [lvlFourList, setLvlFourList] = useState();

  const getLvlFourList = async (lvlThreeNo) => {
    await request.roadmapServices
      .getLvlFourList(lvlThreeNo)
      .then((response) => {
        const lvlFourOptions = response.data.map((lvlFour) => ({
          value: lvlFour.no,
          label: `${lvlFour.no} ${lvlFour.name}`,
        }));
        setLvlFourList(lvlFourOptions);
        roadmapModel.setField(
          'lvl4',
          lvlFourOptions.find((e) => e.value === roadmap.lvl4),
        );
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const isSubmitReady = () => {
    if (
      roadmapModel.year &&
      (roadmapModel.divisionNo || roadmapModel.shiftNo)
    ) {
      return false;
    }
    return true;
  };

  const handleClose = () => {
    onClose(false);
  };

  const submitUpdateRoadmap = async () => {
    const formData = new FormData();
    formData.append('year', roadmapModel.year.label);
    if (roadmapModel.divisionNo) {
      formData.append('divisionId', roadmapModel.divisionNo.value);
    }
    if (roadmapModel.shiftNo) {
      formData.append('shiftId', roadmapModel.shiftNo.value);
    }
    formData.append('noLvl4', roadmapModel.lvl4.value);
    formData.append('roadmapId', roadmapModel.ids);
    formData.append('remark', roadmapModel.remark);
    console.log(formData);
    await request.roadmapServices
      .updateRoadmap(token, formData)
      .then((res) => {
        console.log('Success');
        onClose(false);
        Router.reload();
      })
      .catch((e) => {
        console.log(e);
        onClose(false);
        Router.reload();
      });
    onSubmit();
  };

  const initDataEdit = async () => {
    loadingStore.setIsLoading(true);
    await roadmap.editRoadmapById(roadmap.ids);
    await roadmapModel.setField('remark', roadmap.remark);
    await roadmapModel.setField('ids', roadmap.ids);
    await loadingStore.setIsLoading(false);
  };

  useEffect(() => {
    (async () => {
      loadingStore.setIsLoading(true);
      await initDataEdit();
      await getLvlThreeList();
      await getDepDiv();
      await initYear();
      loadingStore.setIsLoading(false);
    })();
  }, [roadmap.ids]);

  return useObserver(() => (
    <ModalBox size="medium" open={active} onClose={handleClose}>
      <Text fontSize={sizes.xxl} color={colors.textSky}>
        Edit RoadMap
      </Text>
      <Modal.Content style={{ margin: '24px 0px', padding: 0 }}>
        <Grid columns="equal">
          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Year
              </Text>
              <DropdownAll
                placeholder="Select"
                options={yearList}
                handleOnChange={(e) => roadmapModel.setField('year', e)}
                value={roadmapModel.year}
              />
            </Grid.Column>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Department
              </Text>
              <DropdownAll
                placeholder="Select"
                options={departmentList && departmentList}
                handleOnChange={(e) => {
                  roadmapModel.setField('departmentNo', e);
                  roadmapModel.setField('shiftNo', null);
                  roadmapModel.setField('divisionNo', null);
                  setSelectShift(false);
                  setSelectDivision(false);
                }}
                value={roadmapModel.departmentNo}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Division
              </Text>
              <DropdownAll
                placeholder="Select"
                options={divisionList && divisionList}
                handleOnChange={(e) => {
                  roadmapModel.setField('divisionNo', e);
                  roadmapModel.setField('shiftNo', null);
                  setSelectDivision(true);
                }}
                value={roadmapModel.divisionNo}
                isDisabled={isSelectShift}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
                style={{ paddingBottom: 8 }}
              >
                Shift
              </Text>
              <DropdownAll
                placeholder="Select"
                options={shiftList && shiftList}
                handleOnChange={(e) => {
                  roadmapModel.setField('shiftNo', e);
                  roadmapModel.setField('divisionNo', null);
                  setSelectShift(true);
                }}
                value={roadmapModel.shiftNo}
                isDisabled={isSelectDivision}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Process (Level 3)
              </Text>
              <DropdownAll
                placeholder="Select Process"
                options={lvlThreeList}
                value={roadmapModel.lvl3}
                handleOnChange={(e) => {
                  roadmapModel.setField('lvl3', e);
                  getLvlFourList(roadmapModel.lvl3.value);
                }}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Sub-Process (Level 4)
              </Text>
              <DropdownAll
                placeholder="Select Sub-Process"
                options={lvlFourList && lvlFourList}
                value={roadmapModel.lvl4}
                handleOnChange={(e) => {
                  roadmapModel.setField('lvl4', e);
                }}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column>
              <Text
                fontSize={sizes.xxs}
                fontWeight="bold"
                color={colors.primaryBlack}
              >
                Remark
              </Text>
              <Form>
                <TextArea
                  key="remark"
                  placeholder="Remark"
                  maxLength={4000}
                  value={roadmapModel.remark}
                  onChange={(e) => {
                    roadmapModel.setField('remark', e.target.value);
                  }}
                />
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
      >
        <ButtonBorder
          width={140}
          style={{ marginRight: 20 }}
          handelOnClick={handleClose}
          borderColor={colors.textlightGray}
          textColor={colors.primaryBlack}
          textUpper
          textWeight="med"
          text="cancel"
        />
        <ButtonAll
          width={140}
          onClick={submitUpdateRoadmap}
          color={
            isSubmitReady() === 'disabled' ? colors.btGray : colors.primary
          }
          textColor={colors.backgroundPrimary}
          textUpper
          textWeight="med"
          text="Edit Now"
          disabled={isSubmitReady()}
        />
      </div>
    </ModalBox>
  ));
};

export default index;
