import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Image, Icon, Grid, Pagination, Popup } from 'semantic-ui-react';
import { Text, InputAll, BottomBarRoadmap, Remark } from '../../element';
import { colors, sizes } from '../../../utils';
import { RowRoadMapAssessmentView } from '..';

const TableHeader = styled.div`
  background-color: ${colors.backgroundPrimary};
  float: left;
  width: 100%;
  min-height: 50px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  border-radius: 6px 6px 0px 6px !important;
  margin: 8px 13px;
  padding: 10px 24px !important;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0 0px 10px 0 rgba(48, 48, 48, 0.1) !important;
`;
const TableBody = TableHeader.extend`
  margin: 2px 13px 2px 13px;
  min-height: 62px;
  padding: 16px 24px !important;
`;

const Cell = styled.div`
  display: flex;
  width: ${(props) => props.width || 10}%;
  flex-wrap: wrap;
  justify-content: ${(props) =>
    props.around
      ? 'space-around'
      : props.right
      ? 'flex-end'
      : props.center
      ? 'center'
      : props.between
      ? 'space-between'
      : 'flex-start'};
  padding-left: ${(props) => props.left || 0}px;
  align-items: ${(props) => (props.mid ? 'center' : 'flex-start')};
`;
const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const headers = [
  { key: 'Assessment', render: 'Assessment Name', width: 50 },
  { key: 'Assessor', render: 'Assessor', width: 15 },
  { key: 'View', render: 'View CSA', width: 7 },
  { key: 'Remark', render: 'Scope of work', width: 9 },
  { key: 'Roadmap', render: 'Roadmap', width: 9 },
  { key: 'role', render: '', width: 10 },
];

const RoadMapAssessmentViewOnly = ({ roadmapData }) => {
  const [isOpen, setIsOpen] = useState(true);
  const handelOpen = (value) => {
    setIsOpen(value);
  };

  return (
    <div style={{ width: '100%' }}>
      <Grid style={{ marginTop: 12 }}>
        <TableHeader span={12}>
          {headers.map(({ render, key, width, center }) => (
            <Cell key={key} width={width} center={center}>
              <Div center mid={key !== 'Assessment'}>
                {key === 'View' ? (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                    <br />
                    <span>(Last Year)</span>
                  </Text>
                ) : (
                  <Text
                    center
                    fontSize={sizes.xs}
                    fontWeight="med"
                    color={colors.textlightGray}
                  >
                    {render}
                  </Text>
                )}
                {/* {key === 'Assessment' ||
                key === 'Assessor' ||
                key === 'Roadmap' ? (
                  <Image
                    src="../../static/images/iconDown-gray@3x.png"
                    style={{ width: 18, height: 18, marginLeft: 8 }}
                  />
                ) : (
                  ''
                )} */}
              </Div>
            </Cell>
          ))}
        </TableHeader>
        {roadmapData &&
          roadmapData.list.map((roadmap, index) => (
            <TableBody>
              <RowRoadMapAssessmentView roadmap={roadmap} />
            </TableBody>
          ))}
      </Grid>

      {/* <Grid>
        <Grid.Row style={{ justifyContent: 'flex-end', marginRight: 13 }}>
          <Pagination
            style={{
              fontSize: sizes.xs,
              fontWeight: 'bold',
              borderColor: colors.backgroundSecondary,
            }}
            defaultActivePage={1}
            firstItem={null}
            lastItem={null}
            nextItem={{
              content: <Icon name="angle right" />,
              icon: true,
            }}
            prevItem={{
              content: <Icon name="angle left" />,
              icon: true,
            }}
            totalPages={10}
          />
        </Grid.Row>
      </Grid> */}

      <BottomBarRoadmap page="roadmapView" />
    </div>
  );
};

export default RoadMapAssessmentViewOnly;
