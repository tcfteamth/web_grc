import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Image, Popup } from 'semantic-ui-react';
import { useLocalStore, useObserver } from 'mobx-react';
import { initAuthStore } from '../../../contexts';
import { assesorDropdownStore } from '../../../model';
import { ACCEPT_ROADMAP_STATUS } from '../../../model/AcceptRoadmapModel';
import {
  CheckBoxAll,
  Text,
  AsessorDropdown,
  Remark,
  RadioBox,
  InputAll,
  RoadmapNextYear,
  Cell,
} from '../../element';
import useWindowDimensions from '../../../hook/useWindowDimensions';
import { colors, sizes } from '../../../utils';

const Div = styled.div`
  flex-direction: ${(props) => (props.col ? 'column' : 'row')};
  display: flex;
  width: ${(props) => props.width || 100}%;
  justify-content: ${(props) => (props.mid ? 'center' : 'flex-start')};
  align-items: ${(props) => (props.center ? 'center' : 'flex-start')};
`;

const RowRoadMapAssessmentDM = ({ roadmap }) => {
  const authContext = initAuthStore();
  const { width: screenWidth } = useWindowDimensions();
  const [isOpen, setIsOpen] = useState(true);
  const assesorDropdown = useLocalStore(() => assesorDropdownStore);

  const [listAssesor, setListAssesor] = useState([]);

  useEffect(() => {
    assesorDropdown.getAssesor(authContext.accessToken);
    const result = assesorDropdown.options.map((item) => ({
      value: item.value,
      label: item.text,
    }));
    setListAssesor(result);
  }, []);

  return useObserver(() => (
    <Div col>
      <Div center>
        <Cell center width={screenWidth < 1024 ? 400 : 3.3}>
          <CheckBoxAll
            header
            status="Accept"
            onChange={() => roadmap.setAllAcceptChecked()}
            checked={
              roadmap.isAllCheckedUserId === false && roadmap.isAllAcceptChecked
            }
            disabled={
              roadmap.isAllCheckedLvl3Disabled === false &&
              roadmap.isAllCheckedUserId === true
                ? true
                : false
            }
          />
        </Cell>
        <Cell center width={screenWidth < 1024 ? 40 : 3.3}>
          <CheckBoxAll
            header
            status="Reject"
            onChange={() => roadmap.setAllRejectChecked()}
            checked={roadmap.isAllRejectChecked}
            disabled={roadmap.isAllCheckedLvl3Disabled}
          />
        </Cell>
        <Cell width={screenWidth < 1024 ? 890 : 89}>
          <Text
            style={{ paddingLeft: 16 }}
            fontWeight="med"
            fontSize={sizes.s}
            color={colors.primaryBlack}
          >
            {`${roadmap.no} ${roadmap.name}`}
          </Text>
        </Cell>
        <Cell center width={screenWidth < 1024 ? 30 : 3}>
          <div className="click" onClick={() => setIsOpen(!isOpen)}>
            {isOpen ? (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconUp-blue@3x.png"
              />
            ) : (
              <Image
                style={{ width: 24, height: 24 }}
                src="/static/images/iconDown-blue@3x.png"
              />
            )}
          </div>
        </Cell>
      </Div>
      {roadmap.children.map((lvlFour, index) => (
        <Div>
          {isOpen && (
            <Div col>
              <Div style={{ padding: '16px 0px 0px' }}>
                <Cell center width={screenWidth < 1024 ? 40 : 4}>
                  {/* <RadioBox
                    checked={lvlFour.acceptRoadmap.isSelectedAccept()}
                    onChange={(e) =>
                      lvlFour.acceptRoadmap.setNo(
                        ACCEPT_ROADMAP_STATUS.accept,
                        lvlFour.id,
                      )
                    }
                    disabled={
                      lvlFour.isThisYear && !lvlFour.acceptRoadmap.userId
                    }
                  /> */}
                  <RadioBox
                    checked={
                      lvlFour.acceptRoadmap.isSelectedAccept() &&
                      lvlFour.acceptRoadmap.userId !== undefined
                    }
                    onChange={(e) => {
                      lvlFour.acceptRoadmap.setNo(
                        ACCEPT_ROADMAP_STATUS.accept,
                        lvlFour.id,
                      );
                    }}
                    disabled={lvlFour.acceptRoadmap.userId === undefined}
                  />

                  {/* <CheckBoxAll status="Accept" /> */}
                </Cell>
                <Cell center width={screenWidth < 1024 ? 40 : 4}>
                  <RadioBox
                    status="Reject"
                    checked={lvlFour.acceptRoadmap.isSelectedReject()}
                    onChange={(e) =>
                      lvlFour.acceptRoadmap.setNo(
                        ACCEPT_ROADMAP_STATUS.reject,
                        lvlFour.id,
                      )
                    }
                    disabled={lvlFour.isThisYear}
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 420 : 42}>
                  <Text
                    style={{ paddingLeft: 16 }}
                    fontSize={sizes.xs}
                    color={colors.primaryBlack}
                  >
                    {`${lvlFour.no} ${lvlFour.name}`}
                  </Text>
                </Cell>
                <Cell width={screenWidth < 1024 ? 200 : 20} center mid>
                <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                  {lvlFour.latestYear}
                </Text>
              </Cell>
                <Cell width={screenWidth < 1024 ? 200 : 20} center mid>
                  <AsessorDropdown
                    handleOnChange={(assesorId) => {
                      lvlFour.acceptRoadmap.setAssessor(assesorId);
                    }}
                    value={lvlFour.acceptRoadmap.userId}
                    disabled={
                      lvlFour.isThisYear ||
                      lvlFour.acceptRoadmap.isSelectedReject()
                    }
                    defaultValue="Select Assessor"
                    placeholder="Select Assessor"
                  />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <a
                    href={lvlFour.lastYearPath || false}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Image
                      src={
                        lvlFour.lastYearPath
                          ? '../../static/images/file-purple@3x.png'
                          : '../../static/images/file@3x.png'
                      }
                      style={{ width: 18, height: 18 }}
                      className="click"
                    />
                  </a>
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Remark remark={lvlFour.remark} />
                </Cell>
                <Cell width={screenWidth < 1024 ? 100 : 10} center>
                  <Popup
                    wide="very"
                    style={{
                      borderRadius: '6px 6px 0px 6px',
                      padding: 16,
                      marginLeft: -10,
                    }}
                    trigger={
                      lvlFour.roadMapImage ? (
                        <Image
                          src={lvlFour.roadMapImage}
                          style={{ width: 18, height: 18 }}
                        />
                      ) : (
                        <RoadmapNextYear year={lvlFour.year} />
                      )
                    }
                  >
                    <Text fontSize={sizes.xs} color={colors.primaryBlack}>
                      {lvlFour.roadMapType}
                    </Text>
                  </Popup>
                </Cell>
              </Div>
              {lvlFour.acceptRoadmap.isSelectedReject() && (
                <Div
                  style={{
                    paddingLeft: screenWidth <= 1366 ? '42px' : '64px',
                    margin: '8px 0px 0px',
                  }}
                >
                  <InputAll
                    placeholder="กรุณาระบุเหตุผล"
                    handleOnChange={(e) =>
                      lvlFour.acceptRoadmap.setRejectComment(e.target.value)
                    }
                    value={lvlFour.acceptRoadmap.rejectComment}
                    error={!lvlFour.acceptRoadmap.rejectComment}
                  />
                </Div>
              )}
            </Div>
          )}
        </Div>
      ))}
    </Div>
  ));
};

export default RowRoadMapAssessmentDM;
