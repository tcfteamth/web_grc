import colors from './colors';

// Page Roadmap >  Request Admin
export const headersRequestAdmin = [
  {
    key: 'Add',
    render: 'Add',
    center: true,
    width: 3,
    widthMobile: 30,
  },
  { key: 'No', render: 'No', width: 5, center: true, widthMobile: 50 },
  {
    key: 'Assessment Name',
    render: 'Scope of work',
    width: 62,
    widthMobile: 620,
  },
  { key: 'RequestDate', render: 'Request Date', width: 10, widthMobile: 100 },
  { key: 'Owner', render: 'Owner', width: 10, widthMobile: 100 },
  { key: 'Dept/Div', render: 'Dept/Div', width: 10, widthMobile: 100 },
];

// Page Roadmap >  headers Assessment Admin
export const headersAssessmentAdmin = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Assessment', render: 'Assessment Name', width: 40, widthMobile: 300 },
  { key: 'LatestYear', render: 'LatestYear', width: 10, widthMobile: 100 },
  { key: 'Division', render: 'Division / Shift', width: 10, widthMobile: 100 },
  { key: 'Status', render: 'Status', width: 16, widthMobile: 160 },
  { key: 'View', render: 'View CSA', width: 8, widthMobile: 80 },
  { key: 'Remark', render: 'Scope of work', width: 8, widthMobile: 80 },
  { key: 'Roadmap', render: 'Roadmap', width: 8, widthMobile: 80 },
  // { key: 'IC Agent+', render: 'IC Agent+', width: 8, widthMobile: 80 },
  { key: 'edit', render: 'Edit', width: 4, widthMobile: 40 },
  { key: 'delete', render: 'Delete', width: 4, widthMobile: 40 },
];

// Page Roadmap >  headers Assessment Admin
export const headersRoadmapDM = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Reject', render: 'Reject', width: 4, widthMobile: 40 },
  { key: 'Assessment', render: 'Assessment Name', width: 50, widthMobile: 420 },
  { key: 'LatestYear', render: 'LatestYear', width: 5, widthMobile: 40 },
  { key: 'Assessor', render: 'Assessor', width: 30, widthMobile: 200 },
  { key: 'View', render: 'View CSA', width: 10, widthMobile: 100 },
  { key: 'Remark', render: 'Scope of work', width: 10, widthMobile: 100 },
  { key: 'Roadmap', render: 'Roadmap', width: 10, widthMobile: 100 },
];

// Page CSA Assessment > Admin
export const headersApproveAdmin = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  {
    key: 'Assessment Name',
    render: 'Assessment Name',
    width: 28,
    widthMobile: 280,
  },
  {
    key: 'Compliance Objective',
    render: 'Compliance Objective',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Objectives',
    render: 'Objectives',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Status',
    render: 'Status',
    width: 12,
    mid: 'center',
    widthMobile: 120,
  },
  {
    key: 'Ready to Submit',
    render: 'Ready to Submit',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Overall Rating',
    render: 'Overall Rating',
    width: 8,
    mid: 'center',
    widthMobile: 80,
  },
  {
    key: 'Enhancement',
    render: 'Enhancement',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
];

// Page Roadmap >  Request Admin
export const headersRequestDM = [
  { key: 'No', render: 'No', width: 5, center: true, widthMobile: 50 },
  {
    key: 'Assessment Name',
    render: 'Scope of work',
    width: 85,
    widthMobile: 850,
  },
  { key: 'RequestDate', render: 'Request Date', width: 10, widthMobile: 100 },
];

// Page CSA Assessment >  DM
export const headersAssessmentDM = [
  { key: 'Submit', render: 'Submit', width: 3, widthMobile: 30 },
  {
    key: 'Assessment',
    render: 'Assessment Name',
    width: 35,
    widthMobile: 350,
  },
  { key: 'LatestYear', render: 'Latest Year', width: 10, widthMobile: 100 },
  { key: 'Ready', render: 'Ready to Submit', width: 10, widthMobile: 100 },
  { key: 'Type', render: 'Roadmap', width: 10, widthMobile: 100 },
  {
    key: 'Remark',
    render: 'Scope of work',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  { key: 'Last', render: 'Last Update', width: 10, widthMobile: 100 },
  { key: 'Status', render: 'Status', width: 12, widthMobile: 120 },
];

// Page CSA AssessmentSummary >  DM
export const headersAssessmentSummaryDM = [
  { key: 'Assessment', render: 'Assessment Name', width: 45, widthMobile: 450 },
  { key: 'Type', render: 'Roadmap', width: 7, widthMobile: 70 },
  { key: 'Division', render: 'Division', width: 13, widthMobile: 130 },
  { key: 'Status', render: `Status`, width: 15, widthMobile: 150 },
  { key: 'wait', render: `Task Owner`, width: 13, widthMobile: 130 },
  { key: 'Remark', render: `Scope of work`, width: 7, widthMobile: 70 },
  { key: 'LatestYear', render: 'Latest Year', width: 10, widthMobile: 100 }
];

// Page CSA  AssessmentStaff >  Staff
export const headersAssessmentStaff = [
  { key: 'Submit', render: 'Submit', width: 3, widthMobile: 30 },
  { key: 'Assessment', render: 'Assessment Name', width: 38, widthMobile: 380 },
  { key: 'LatestYear', render: 'Latest Year', width: 10, widthMobile: 100 },
  { key: 'Ready', render: 'Ready to Submit ', width: 7, widthMobile: 70 },
  { key: 'Scope', render: 'Scope of Work', width: 7, widthMobile: 70 },
  { key: 'Type', render: 'Roadmap', width: 10, widthMobile: 100 },
  { key: 'Last', render: 'Last Update', width: 10, widthMobile: 100 },
  { key: 'Status', render: 'Status', width: 15, widthMobile: 150 },
];

// Page COSO  Roadmap >  Admin
export const headersCOSORoadmapAdmin = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'No', render: 'No', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question', width: 30, widthMobile: 300 },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Status',
    render: 'Status',
    width: 14,
    mid: 'center',
    widthMobile: 140,
  },
  {
    key: 'View',
    render: 'View COSO',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Remark',
    render: 'Scope of work',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  { key: 'edit', render: 'Edit', width: 4, mid: 'center', widthMobile: 40 },
  { key: 'delete', render: 'Delete', width: 4, mid: 'center', widthMobile: 40 },
];

// Page COSO  Roadmap >  DM
export const headersCOSORoadmapDM = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Reject', render: 'Reject', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question', width: 32, widthMobile: 320 },
  { key: 'Division', render: 'Division', width: 10, widthMobile: 100 },
  {
    key: 'Responsible person',
    render: 'Responsible person',
    width: 25,
    widthMobile: 250,
  },
  {
    key: 'View',
    render: 'View COSO',
    width: 10,
    widthMobile: 100,
    center: 'center',
  },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    widthMobile: 100,
    center: 'center',
  },
  { key: 'Remark', render: 'Scope of work', width: 5, widthMobile: 50 },
];

// Page COSO  Assessment >  Summary
export const headersCOSOAssessmentSummary = [
  { key: 'Question', render: 'Question', width: 38, widthMobile: 380 },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Status',
    render: 'Status',
    width: 15,
    widthMobile: 150,
  },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },

  {
    key: 'Task Owner',
    render: 'Task Owner',
    width: 13,
    mid: 'center',
    widthMobile: 130,
  },
  {
    key: 'Last Update',
    render: 'Last Update',
    width: 7,
    mid: 'center',
    widthMobile: 70,
  },
  {
    key: 'Remark',
    render: 'Scope of Work',
    width: 7,
    widthMobile: 70,
    center: 'center',
  },
];

// Page COSO  Assessment >  Admin
export const headersCOSOAssessmentAdmin = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  // { key: 'No', render: 'No', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question', width: 43, widthMobile: 430 },
  {
    key: 'Ready to Submit',
    render: 'Ready to Submit',
    width: 8,
    widthMobile: 80,
    mid: 'center',
  },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Last Update',
    render: 'Last Update',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Status',
    render: 'Status',
    width: 15,
    widthMobile: 150,
    mid: 'center',
  },
];

// Page COSO  Assessment >  staff
export const headersCOSORoadmapStaff = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question Staff', width: 46, widthMobile: 460 },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Ready to Submit',
    render: 'Ready to Submit',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Task Owner',
    render: 'Task Owner',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Last Update',
    render: 'Last Update',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Status',
    render: 'Status',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
];

// Page COSO  Assessment >  DM
export const headersCOSOAssessmentDM = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  // { key: 'No', render: 'No', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question', width: 48, widthMobile: 480 },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Ready to Submit',
    render: 'Ready to Submit',
    width: 8,
    widthMobile: 80,
    mid: 'center',
  },
  {
    key: 'Task Owner',
    render: 'Task Owner',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Last Update',
    render: 'Last Update',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Status',
    render: 'Status',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
];

// Page COSO  Assessment >  VP
export const headersCOSOAssessmentVP = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Reject', render: 'Reject', width: 4, widthMobile: 40 },
  { key: 'Question', render: 'Question', width: 38, widthMobile: 380 },
  {
    key: 'Roadmap',
    render: 'Roadmap',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Task Owner',
    render: 'Task Owner',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Last Update',
    render: 'Last Update',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
  {
    key: 'Status',
    render: 'Status',
    width: 14,
    widthMobile: 140,
    mid: 'center',
  },
];
// Page CSA Assessment >  DM
export const headersReAssessment = [
  { key: 'Submit', render: 'Submit', width: 3, widthMobile: 30 },
  {
    key: 'Assessment',
    render: 'Assessment Name',
    width: 45,
    widthMobile: 450,
  },
  { key: 'Indicator', render: 'Indicator', width: 12, widthMobile: 100 },
  { key: 'LatestYear', render: 'Latest Year', width: 12, widthMobile: 100 },
  { key: 'Type', render: 'Roadmap', width: 12, widthMobile: 100 },
  {
    key: 'Remark',
    render: 'Scope of work',
    width: 12,
    widthMobile: 100,
    mid: 'center',
  },
];
export const headersCSAAssessmentVP = [
  { key: 'Accept', render: 'Accept', width: 4, widthMobile: 40 },
  { key: 'Reject', render: 'Reject', width: 4, widthMobile: 40 },
  {
    key: 'Assessment Name',
    render: 'Assessment Name',
    width: 37,
    widthMobile: 370,
  },
  {
    key: 'Division',
    render: 'Division',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Scope of Work',
    render: 'Scope of Work',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
  {
    key: 'Overall Rating',
    render: 'Overall Rating',
    width: 8,
    mid: 'center',
    widthMobile: 80,
  },
  {
    key: 'Enhancement',
    render: 'Enhancement',
    width: 10,
    mid: 'center',
    widthMobile: 100,
  },
];

// Page COSO  Document
export const headersCOSODocument = [
  {
    key: 'Selected',
    render: 'Selected Document',
    width: 8,
    mid: 'center',
    widthMobile: 80,
  },
  { key: 'Document', render: 'Document', width: 35, widthMobile: 350 },
  {
    key: 'Upload Date',
    render: 'Upload Date',
    width: 10,
    widthMobile: 100,
    mid: 'center'
  },
  {
    key: 'Upload By',
    render: 'Upload By',
    width: 15,
    widthMobile: 150,
  },
  {
    key: 'Size',
    render: 'Size',
    width: 7,
    mid: 'center',
    widthMobile: 70,
  },
  {
    key: 'Delete',
    render: 'Delete',
    width: 10,
    widthMobile: 100,
    mid: 'center',
  },
];

// Page Setting Database >> tag
export const headersDataTag = [
  { key: 'No', render: 'No', width: 4, mid: 'center' },
  { key: 'Tag', render: 'Tag Name', width: 80 },
  { key: 'Edit', render: 'Edit', width: 8, mid: 'center' },
  { key: 'Delete', render: 'Delete', width: 8, mid: 'center' },
];

// Page Setting Database >> Edit tag
export const headersDataTagList = [
  { key: 'Process No', render: 'Process No', width: 92 },
  { key: 'Delete', render: 'Delete', width: 8, mid: 'center' },
];
// Page Setting Database >> tag
export const headersManualTable = [
  { key: 'No', render: 'No', width: 8, mid: 'center' },
  { key: 'EmpId', render: 'Emp Id', width: 40 },
  { key: 'Position', render: 'Position', width: 40 ,mid: 'center'},
  { key: 'Level', render: 'Level', width: 8 ,mid: 'center'},
  { key: 'OrgId', render: 'Department/Division/Shift No.', width: 40 , mid: 'center'},
  // { key: 'Edit', render: 'Edit', width: 8, mid: 'center' },
  { key: 'Delete', render: 'Delete', width: 8, mid: 'center' },
];
export const sortTypes = [
  {
    id: 1,
    text: 'à¸ªà¸¡à¸¨à¸£à¸µ à¸¡à¸µà¹‚à¸Šà¸„',
    value: 1,
    label: 'à¸ªà¸¡à¸¨à¸£à¸µ à¸¡à¸µà¹‚à¸Šà¸„',
  },
  {
    id: 2,
    text: 'à¸¡à¸²à¸£à¸§à¸¢ à¸¡à¸µà¸Šà¸µà¸¢',
    value: 2,
    label: 'à¸¡à¸²à¸£à¸§à¸¢ à¸¡à¸µà¸Šà¸µà¸¢',
  },
  {
    id: 3,
    text: 'à¸ªà¹‰à¸¡ à¸«à¸¢à¸¸à¸”',
    value: 3,
    label: 'à¸ªà¹‰à¸¡ à¸«à¸¢à¸¸à¸”',
  },
];

export const listTag = [
  { key: 1, label: 'All Type', value: 1 },
  { key: 2, label: 'M', value: 2 },
  { key: 3, label: 'C', value: 3 },
];

export const listObjectiveType = [
  { key: 1, text: 'Operations', value: 1 },
  { key: 2, text: 'Reporting', value: 2 },
  { key: 3, text: 'Compliance', value: 3 },
];

export const listType = [
  { key: 1, text: 'All Type', value: 1 },
  { key: 2, text: 'M', value: 2 },
  { key: 3, text: 'C', value: 3 },
];
export const listSubProcess = [
  { id: 1, text: 'm', value: 1, label: 'm' },
  { id: 2, text: 'c', value: 2, label: 'c' },
];

export const listCompli = [
  {
    id: 1,
    Legal:
      'à¸žà¸£à¸š.à¹‚à¸£à¸‡à¸‡à¸²à¸™ (à¸‰à¸šà¸±à¸šà¸—à¸µà¹ˆ 2) à¸ž.à¸¨. 2562',
    Ministerial: '',
    Department: '',
    Related: 'à¸¡à¸²à¸•à¸£à¸² 10',
    involved: 'SHE,ARO,REF,OLE,U-P1,POL,TP-PQ/TP-PP,EX-EX,M-CM',
    WorkProcess:
      'à¸Šà¸³à¸£à¸°à¸„à¹ˆà¸²à¸˜à¸£à¸£à¸¡à¹€à¸™à¸µà¸¢à¸¡à¸£à¸²à¸¢à¸›à¸µ',
    compliance: '',
    Effect: '',
    GRC: '',
  },
  {
    id: 2,
    Legal:
      'à¸žà¸£à¸š.à¹‚à¸£à¸‡à¸‡à¸²à¸™ (à¸‰à¸šà¸±à¸šà¸—à¸µà¹ˆ 2) à¸ž.à¸¨. 2562',
    Ministerial: '',
    Department: '',
    Related: 'à¸¡à¸²à¸•à¸£à¸² 10',
    involved: 'SHE,ARO,REF,OLE,U-P1,POL,TP-PQ/TP-PP,EX-EX,M-CM',
    WorkProcess:
      'à¸Šà¸³à¸£à¸°à¸„à¹ˆà¸²à¸˜à¸£à¸£à¸¡à¹€à¸™à¸µà¸¢à¸¡à¸£à¸²à¸¢à¸›à¸µ',
    compliance: '',
    Effect: '',
    GRC: '',
  },
  {
    id: 3,
    Legal:
      'à¸žà¸£à¸š.à¹‚à¸£à¸‡à¸‡à¸²à¸™ (à¸‰à¸šà¸±à¸šà¸—à¸µà¹ˆ 2) à¸ž.à¸¨. 2562',
    Ministerial: '',
    Department: '',
    Related: 'à¸¡à¸²à¸•à¸£à¸² 10',
    involved: 'SHE,ARO,REF,OLE,U-P1,POL,TP-PQ/TP-PP,EX-EX,M-CM',
    WorkProcess:
      'à¸Šà¸³à¸£à¸°à¸„à¹ˆà¸²à¸˜à¸£à¸£à¸¡à¹€à¸™à¸µà¸¢à¸¡à¸£à¸²à¸¢à¸›à¸µ',
    compliance: '',
    Effect: '',
    GRC: '',
  },
];

export const listRisk = [
  {
    id: 1,
    text: '1.1.1.1 Assess the external environment',
    objective:
      'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
    type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
    riskNo: 'R1',
    riskDetail:
      'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
    control: [
      {
        id: 1,
        controlNoType: 'Optional',
        controlNo: 'C1',
        controlDetail: `à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡`,
        controlType:
          'à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹à¸šà¸šà¸›à¹‰à¸­à¸‡à¸à¸±à¸™ (Preventive control)',
        controlFrequency: 'à¸£à¸²à¸¢à¸›à¸µ',
        controlPattern: '',
        controlDocument: 'New supplier approval form ',
      },
      {
        id: 2,
        controlNoType: 'Optional',
        controlNo: 'C2',
        controlDetail: `Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™`,
        controlType: 'IC-CSA-2',
        controlFrequency: null,
        controlPattern: null,
        controlDocument: 'New supplier approval form ',
      },
    ],
    subProcess: '1.1.1.1 Assess the external environment',
  },
  {
    id: 2,
    text: '1.1.1.2 Survey market and determine customer needs and wants',
    objective:
      'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
    type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
    riskNo: 'R2',
    riskDetail:
      'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
    control: [
      {
        id: 1,
        controlNoType: 'Standard',
        controlNo: 'C3',
        controlDetail: `à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ`,
        controlType:
          'à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹à¸šà¸šà¸›à¹‰à¸­à¸‡à¸à¸±à¸™ (Preventive control)',
        controlFrequency: 'à¸£à¸²à¸¢à¸›à¸µ',
        controlPattern: `à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸”à¹‰à¸§à¸¢à¸¡à¸·à¸­ (Manual Control)`,
        controlDocument: 'New supplier approval form ',
      },
      {
        id: 2,
        controlNoType: 'Standard',
        controlNo: 'C4',
        controlDetail: `Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™`,
        controlType:
          'à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹à¸šà¸šà¸›à¹‰à¸­à¸‡à¸à¸±à¸™ (Preventive control)',
        controlFrequency: '',
        controlPattern: '',
        controlDocument: 'New supplier approval form ',
      },
    ],
    subProcess: '1.1.1.1 Assess the external environment',
  },
  {
    id: 3,
    text: '1.1.2.1 Develop overall mission statement ',
    objective:
      'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
    type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
    riskNo: 'R3',
    riskDetail:
      'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
    control: [
      {
        id: 1,
        controlNoType: 'Optional',
        controlNo: 'C5',
        controlDetail: `Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™`,
        controlType:
          'à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹à¸šà¸šà¸›à¹‰à¸­à¸‡à¸à¸±à¸™ (Preventive control)',
        controlFrequency: 'à¸£à¸²à¸¢à¸›à¸µ',
        controlPattern: `à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸”à¹‰à¸§à¸¢à¸¡à¸·à¸­ (Manual Control)`,
        controlDocument: 'New supplier approval form ',
      },
    ],
    subProcess: '1.1.1.1 Assess the external environment',
  },
];
export const findingDatabase = [
  {
    id: 1,
    no: '191205',
    name:
      'à¹„à¸¡à¹ˆà¸¡à¸µà¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡à¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£',
    FindingDescription:
      'à¸ˆà¸²à¸à¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡ (GC Way of Conduct) à¸žà¸šà¸§à¹ˆà¸² à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Subsidiary Governance & Policy (S-RC-SG) à¸¡à¸µà¸à¸²à¸£à¸ˆà¸±à¸”à¸—à¸³à¸ªà¸£à¸¸à¸›à¸¢à¹ˆà¸­Flow chart à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™ à¹€à¸žà¸·à¹ˆà¸­à¹ƒà¸Šà¹‰à¸ à¸²à¸¢à¹ƒà¸™à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ à¸­à¸¢à¹ˆà¸²à¸‡à¹„à¸£à¸à¹‡à¸•à¸²à¸¡ à¹„à¸¡à¹ˆà¸¡à¸µà¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£ à¹à¸¥à¸°à¸ªà¸·à¹ˆà¸­à¸ªà¸²à¸£à¹ƒà¸«à¹‰à¸œà¸¹à¹‰à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸£à¸±à¸šà¸—à¸£à¸²à¸š',
    AuditEngagementName: `à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ªà¸±à¸”à¸ªà¹ˆà¸§à¸™à¸à¸²à¸£à¸¥à¸‡à¸—à¸¸à¸™ à¹à¸¥à¸°à¹à¸™à¸§à¸—à¸²à¸‡à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¸šà¸£à¸´à¸©à¸±à¸—à¹ƒà¸™à¸à¸¥à¸¸à¹ˆà¸¡ (Portfolio Management and Way of Conduct)`,
    AuditeeUnits: [
      {
        id: 1,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Investment Management and M&A (S-IM)`,
      },
      {
        id: 2,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Strategy and Planning (S-SP)`,
      },
      {
        id: 3,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Corporate Risk Management and Internal Control System (S-RC)`,
      },
    ],
    RootCause: 'No procedure identified',
    FindingRating: 'L',
    Impact: [
      {
        id: 1,
        text:
          '1.à¸œà¸¹à¹‰à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸”à¸³à¹€à¸™à¸´à¸™à¸‡à¸²à¸™à¹„à¸¡à¹ˆà¸ªà¸­à¸”à¸„à¸¥à¹‰à¸­à¸‡à¸à¸±à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸²à¸¡à¸²à¸£à¸–à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¹„à¸”à¹‰à¸•à¸²à¸¡à¸—à¸µà¹ˆà¸•à¹‰à¸­à¸‡à¸à¸²à¸£',
      },
      {
        id: 2,
        text:
          '2.à¸œà¸¹à¹‰à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸­à¸²à¸ˆà¸¥à¸°à¹€à¸¥à¸¢à¸«à¸™à¹‰à¸²à¸—à¸µà¹ˆà¸‚à¸­à¸‡à¸•à¸™à¹€à¸­à¸‡ à¹€à¸™à¸·à¹ˆà¸­à¸‡à¸ˆà¸²à¸à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸£à¸°à¸šà¸¸à¸«à¸™à¹‰à¸²à¸—à¸µà¹ˆà¹„à¸§à¹‰à¸­à¸¢à¹ˆà¸²à¸‡à¸Šà¸±à¸”à¹€à¸ˆà¸™',
      },
    ],
    RecommendationDescription: [
      {
        id: 1,
        text:
          '1.à¸ˆà¸±à¸”à¸—à¸³à¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡à¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£ à¸‹à¸¶à¹ˆà¸‡à¸„à¸£à¸­à¸šà¸„à¸¥à¸¸à¸¡à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡',
      },
      {
        id: 2,
        text:
          '2.à¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸„à¸§à¸£à¸¡à¸µà¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­ (à¹€à¸Šà¹ˆà¸™ à¸­à¸¢à¹ˆà¸²à¸‡à¸™à¹‰à¸­à¸¢à¸›à¸µà¸¥à¸°à¸„à¸£à¸±à¹‰à¸‡ à¸«à¸£à¸·à¸­à¹€à¸¡à¸·à¹ˆà¸­à¸¡à¸µà¸à¸²à¸£à¹€à¸›à¸¥à¸µà¹ˆà¸¢à¸™à¹à¸›à¸¥à¸‡)',
      },
    ],
    RecommendationState: 'Closed-Verified',
    TeamLead: [
      { id: 1, text: 'Kunlayanee Wongyoo ' },
      { id: 2, text: 'Meesit Maedee ' },
      { id: 3, text: 'Suvida Nuangrit' },
      { id: 4, text: 'Tanyalak Passarawooth' },
    ],
    TeamMember: 'Khun Lawan S.',
  },
  {
    id: 2,
    no: '191205',
    name:
      'à¹„à¸¡à¹ˆà¸¡à¸µà¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡à¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£',
    FindingDescription:
      'à¸ˆà¸²à¸à¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡ (GC Way of Conduct) à¸žà¸šà¸§à¹ˆà¸² à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Subsidiary Governance & Policy (S-RC-SG) à¸¡à¸µà¸à¸²à¸£à¸ˆà¸±à¸”à¸—à¸³à¸ªà¸£à¸¸à¸›à¸¢à¹ˆà¸­Flow chart à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™ à¹€à¸žà¸·à¹ˆà¸­à¹ƒà¸Šà¹‰à¸ à¸²à¸¢à¹ƒà¸™à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ à¸­à¸¢à¹ˆà¸²à¸‡à¹„à¸£à¸à¹‡à¸•à¸²à¸¡ à¹„à¸¡à¹ˆà¸¡à¸µà¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£ à¹à¸¥à¸°à¸ªà¸·à¹ˆà¸­à¸ªà¸²à¸£à¹ƒà¸«à¹‰à¸œà¸¹à¹‰à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸£à¸±à¸šà¸—à¸£à¸²à¸š',
    AuditEngagementName: `à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ªà¸±à¸”à¸ªà¹ˆà¸§à¸™à¸à¸²à¸£à¸¥à¸‡à¸—à¸¸à¸™ à¹à¸¥à¸°à¹à¸™à¸§à¸—à¸²à¸‡à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¸šà¸£à¸´à¸©à¸±à¸—à¹ƒà¸™à¸à¸¥à¸¸à¹ˆà¸¡ (Portfolio Management and Way of Conduct)`,
    AuditeeUnits: [
      {
        id: 1,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Investment Management and M&A (S-IM)`,
      },
      {
        id: 2,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Strategy and Planning (S-SP)`,
      },
      {
        id: 3,
        text: `à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™ Corporate Risk Management and Internal Control System (S-RC)`,
      },
    ],
    RootCause: 'No procedure identified',
    FindingRating: 'L',
    Impact: [
      {
        id: 1,
        text:
          '1.à¸œà¸¹à¹‰à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸”à¸³à¹€à¸™à¸´à¸™à¸‡à¸²à¸™à¹„à¸¡à¹ˆà¸ªà¸­à¸”à¸„à¸¥à¹‰à¸­à¸‡à¸à¸±à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸²à¸¡à¸²à¸£à¸–à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¹„à¸”à¹‰à¸•à¸²à¸¡à¸—à¸µà¹ˆà¸•à¹‰à¸­à¸‡à¸à¸²à¸£',
      },
      {
        id: 2,
        text:
          '2.à¸œà¸¹à¹‰à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸­à¸²à¸ˆà¸¥à¸°à¹€à¸¥à¸¢à¸«à¸™à¹‰à¸²à¸—à¸µà¹ˆà¸‚à¸­à¸‡à¸•à¸™à¹€à¸­à¸‡ à¹€à¸™à¸·à¹ˆà¸­à¸‡à¸ˆà¸²à¸à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸£à¸°à¸šà¸¸à¸«à¸™à¹‰à¸²à¸—à¸µà¹ˆà¹„à¸§à¹‰à¸­à¸¢à¹ˆà¸²à¸‡à¸Šà¸±à¸”à¹€à¸ˆà¸™',
      },
    ],
    RecommendationDescription: [
      {
        id: 1,
        text:
          '1.à¸ˆà¸±à¸”à¸—à¸³à¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡à¹€à¸›à¹‡à¸™à¸¥à¸²à¸¢à¸¥à¸±à¸à¸©à¸“à¹Œà¸­à¸±à¸à¸©à¸£ à¸‹à¸¶à¹ˆà¸‡à¸„à¸£à¸­à¸šà¸„à¸¥à¸¸à¸¡à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸à¸²à¸£à¸šà¸£à¸´à¸«à¸²à¸£à¸ˆà¸±à¸”à¸à¸²à¸£à¹à¸šà¸šà¸à¸¥à¸¸à¹ˆà¸¡',
      },
      {
        id: 2,
        text:
          '2.à¸‚à¸±à¹‰à¸™à¸•à¸­à¸™à¸à¸²à¸£à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸‡à¸²à¸™à¸„à¸§à¸£à¸¡à¸µà¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­ (à¹€à¸Šà¹ˆà¸™ à¸­à¸¢à¹ˆà¸²à¸‡à¸™à¹‰à¸­à¸¢à¸›à¸µà¸¥à¸°à¸„à¸£à¸±à¹‰à¸‡ à¸«à¸£à¸·à¸­à¹€à¸¡à¸·à¹ˆà¸­à¸¡à¸µà¸à¸²à¸£à¹€à¸›à¸¥à¸µà¹ˆà¸¢à¸™à¹à¸›à¸¥à¸‡)',
      },
    ],
    RecommendationState: 'Closed-Verified',
    TeamLead: [
      { id: 1, text: 'Kunlayanee Wongyoo ' },
      { id: 2, text: 'Meesit Maedee ' },
      { id: 3, text: 'Suvida Nuangrit' },
      { id: 4, text: 'Tanyalak Passarawooth' },
    ],
    TeamMember: 'Khun Lawan S.',
  },
];

export const listProcessDatabase = [
  {
    id: 1,
    text: `1.1.1 Define the business concept and long-term vision`,
    definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
    value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
    processName: `APQC - Petroleum Downstream V 6.1.0`,
    processDefinition: `APQC - Cross Industry Process V 7.0.4`,
    processType: 'M',
    refNo: '1.1',
    disable: true,
    content: [
      {
        id: 1,
        text: `1.1.1.1 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: true,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
      {
        id: 2,
        text: `1.1.1.1 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
    ],
  },
  {
    id: 2,
    text: `1.2.1 Define the business concept and long-term vision`,
    definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
    value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
    processName: `APQC - Petroleum Downstream V 6.1.0`,
    processDefinition: `APQC - Cross Industry Process V 7.0.4`,
    processType: 'M',
    refNo: '1.1',
    disable: true,
    content: [
      {
        id: 1,
        text: `1.2.1.1 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
      {
        id: 2,
        text: `1.2.1.2 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
    ],
  },
  {
    id: 3,
    text: `1.3.1 Define the business concept and long-term vision`,
    definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
    value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
    processName: `APQC - Petroleum Downstream V 6.1.0`,
    processDefinition: `APQC - Cross Industry Process V 7.0.4`,
    processType: 'M',
    refNo: '1.1',
    disable: true,
    content: [
      {
        id: 1,
        text: `1.3.1.1 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
      {
        id: 2,
        text: `1.3.1.2 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
      {
        id: 3,
        text: `1.3.1.3 Assess the external environment`,
        definition: `The process of creating a conceptual framework of the organization's business activity and strategic vision with long-term applicability. Scout the organization's internal capabilities, as well as the customer's needs and desires, to identify a fit that can be used to advance a conceptual structure of the organization's business activity. Conduct analysis in light of relevant externalities and large-scale shifts in the market landscape.`,
        value: `Long-term strategic vision that leverages the core competencies of the organization and its employees`,
        processName: `APQC - Petroleum Downstream V 6.1.0`,
        processDefinition: `APQC - Cross Industry Process V 7.0.4`,
        processType: 'M',
        grouping: 'C',
        cosoName: 'C',
        cosoGroup: 'C',
        cosoNumber: '2',
        disable: false,
        riskAndControl: [
          {
            idNo: 1,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 2,
            risk: 'IC-CSA-1',
          },
          {
            idNo: 3,
            risk: 'IC-CSA-1',
          },
        ],
      },
    ],
  },
];

export const ListCSAReport = [
  {
    id: 1,
    text: `3.1.1.1 Develop stock target level by product/location"`,
    objective: 'Operations',
    data: [
      {
        id: 1,
        risk:
          ' Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        control:
          'control 1 is simply dummy text of the printing and typesetting industry.',
        assessment: '2-fair',
        observation:
          'Spare Part à¸¢à¸±à¸‡à¹„à¸¡à¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸•à¹ˆà¸­à¸à¸²à¸£à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        suggestion:
          'à¸—à¸³à¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™ Spare Part à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ à¹‚à¸”à¸¢à¸à¸³à¸«à¸™à¸”à¹€à¸›à¹‡à¸™à¸à¸¥à¸¢à¸¸à¸—à¸˜à¸‚à¸­à¸‡à¸à¹ˆà¸²à¸¢à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        responsible: 'A-MN / T-TA-IM',
        status: 'Open',
        date: '31/12/2019',
      },
      {
        id: 2,
        risk:
          ' Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        control:
          ' control 2 simply dummy text of the printing and typesetting industry.',
        assessment: '2-fair',
        observation:
          'Spare Part à¸¢à¸±à¸‡à¹„à¸¡à¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸•à¹ˆà¸­à¸à¸²à¸£à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        suggestion:
          'à¸—à¸³à¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™ Spare Part à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ à¹‚à¸”à¸¢à¸à¸³à¸«à¸™à¸”à¹€à¸›à¹‡à¸™à¸à¸¥à¸¢à¸¸à¸—à¸˜à¸‚à¸­à¸‡à¸à¹ˆà¸²à¸¢à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        responsible: 'A-MN / T-TA-IM',
        status: 'Postpone',
        date: '31/12/2019',
      },
      {
        id: 3,
        risk:
          ' Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        assessment: '2-fair',
        observation:
          'Spare Part à¸¢à¸±à¸‡à¹„à¸¡à¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸•à¹ˆà¸­à¸à¸²à¸£à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        control: 'control 3  vdvdsv ',
        suggestion:
          'à¸—à¸³à¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™ Spare Part à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ à¹‚à¸”à¸¢à¸à¸³à¸«à¸™à¸”à¹€à¸›à¹‡à¸™à¸à¸¥à¸¢à¸¸à¸—à¸˜à¸‚à¸­à¸‡à¸à¹ˆà¸²à¸¢à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        responsible: 'A-MN / T-TA-IM',
        status: 'Postpone',
        date: '31/12/2019',
      },
    ],
  },
  {
    id: 1,
    text: `3.1.1.1 Develop stock target level by product/location"`,
    objective: 'Operations',
    data: [
      {
        id: 1,
        risk: 'risk',
        control: ' control',
        assessment: '2-fair',
        observation:
          'Spare Part à¸¢à¸±à¸‡à¹„à¸¡à¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸•à¹ˆà¸­à¸à¸²à¸£à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        responsible: 'A-MN / T-TA-IM',
        suggestion:
          'à¸—à¸³à¸à¸²à¸£à¸—à¸šà¸—à¸§à¸™ Spare Part à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ à¹‚à¸”à¸¢à¸à¸³à¸«à¸™à¸”à¹€à¸›à¹‡à¸™à¸à¸¥à¸¢à¸¸à¸—à¸˜à¸‚à¸­à¸‡à¸à¹ˆà¸²à¸¢à¸šà¸³à¸£à¸¸à¸‡à¸£à¸±à¸à¸©à¸²',
        status: 'Overdue',
        date: '31/12/2019',
      },
    ],
  },
];

export const menuRole = [
  {
    id: 1,
    menu: 'CSA Database',
    updateDate: '01/01/2020',
  },
  {
    id: 2,
    menu: 'CSA Roadmap',
    updateDate: '01/01/2020',
  },
  {
    id: 3,
    menu: 'CSA Assessment',
    updateDate: '01/01/2020',
  },
  {
    id: 4,
    menu: 'CSA Follow Up',
    updateDate: '01/01/2020',
  },
  {
    id: 5,
    menu: 'CSA Report',
    updateDate: '01/01/2020',
  },
  {
    id: 6,
    menu: 'COSO Database',
    updateDate: '01/01/2020',
  },
  {
    id: 7,
    menu: 'COSO Assessment',
    updateDate: '01/01/2020',
  },
  {
    id: 8,
    menu: 'COSO Report',
    updateDate: '01/01/2020',
  },
  {
    id: 9,
    menu: 'Role Management',
    updateDate: '01/01/2020',
  },
];

export const ListStaff = [
  {
    id: 1,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 2,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 3,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 4,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 5,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 6,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 7,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 8,
    firstName: 'à¸Šà¸¡à¸žà¸¹à¹ˆ',
    lastName: 'à¸ªà¸¡à¸ªà¸§à¸¢',
    countMember: 2,
    updateDate: '01/01/2020',
  },
];

export const dataTag = [
  { text: 'Process Type M', value: 0 },
  { text: 'Process Type C', value: 1 },
  { text: 'Process Type A', value: 2 },
  { text: 'Process Type D', value: 3 },
  { text: 'Grouping C', value: 4 },
  { text: 'COSO C', value: 5 },
  { text: 'COSO B', value: 6 },
  { text: 'COSO NO. 2', value: 7 },
  { text: 'COSO NO. 1', value: 8 },
  { text: 'COSO NO. 3', value: 9 },
];
export const ListRole = [
  {
    id: 1,
    label: 'S-RC-IC',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 2,
    label: 'DM-GRC',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 3,
    label: 'CC-CG-RC',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 4,
    label: 'IC Agent',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 5,
    label: 'IC Agent+',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 6,
    label: 'IA',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 7,
    label: 'Staff',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 8,
    label: 'DM',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 9,
    label: 'VP',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 10,
    label: 'EVP',
    countMember: 2,
    updateDate: '01/01/2020',
  },
  {
    id: 11,
    label: 'Mgmt',
    countMember: 2,
    updateDate: '01/01/2020',
  },
];
export const ListReport = [
  {
    id: 1,
    text: `3.1.1.1 Develop stock target level by product/location"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
  {
    id: 2,
    text: `3.1.3.1 Develop asset maintenance and shutdown strategy"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
  {
    id: 3,
    text: `3.1.3.3 Manage equipment reliability"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
  {
    id: 4,
    text: `3.1.3.4 Execute and control plant maintenance - preventive maintenance"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
  {
    id: 5,
    text: `3.1.3.4 Execute and control plant maintenance - preventive maintenance"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
  {
    id: 6,
    text: `3.1.3.4 Execute and control plant maintenance - preventive maintenance"`,
    division: 'A-MN',
    progress:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    problem:
      'when an unknown printer took a galley of type and scrambled it to make a type specimen book',
    status: 'OPEN',
  },
];

export const ListReportResult = [
  {
    id: 1,
    text: `3.1.1 Develop stock target level by product/location"`,

    subProcess: [
      {
        id: 1,
        text: '3.1.1.1 took a galley of type and scrambled it to make a ',
        division: 'A-MN',
        status: 'OPEN',
      },
      {
        id: 2,
        text: '3.1.1.2 took a galley of type and scrambled it to make a ',
        division: 'A-MN',
        status: 'OPEN',
      },
      {
        id: 3,
        text: '3.1.1.3 took a galley of type and scrambled it to make a ',
        division: 'A-MN',
        status: 'OPEN',
      },
    ],
  },
  {
    id: 2,
    text: `3.3.1 Develop stock target level by product/location"`,
    subProcess: [
      {
        id: 1,
        text: '3.3.1.1 took a galley of type and scrambled it to make a ',
        division: 'A-MN',
        status: 'OPEN',
      },
      {
        id: 2,
        text: '3.3.1.2 took a galley of type and scrambled it to make a ',
        division: 'A-MN',
        status: 'OPEN',
      },
    ],
  },
];

export const ListSummary = [
  {
    id: 1,
    text: `3.1.1.1 Develop stock target level by product/location`,
    controlNo: 2,
    controlList: [
      {
        controlId: 1,
        text:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        dueDate: '30/06/2563',
        lastDate: '30/06/2563',
        rating: 'Fair',
        status: 'Open',
      },
    ],
  },
  {
    id: 2,
    text: `3.1.3.4 Execute and control plant maintenance`,
    controlNo: 1,
    controlList: [
      {
        controlId: 1,
        text:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        dueDate: '30/06/2563',
        lastDate: '30/06/2563',
        rating: 'Fair',
        status: 'Open',
      },
      {
        controlId: 2,
        text:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        dueDate: '30/06/2563',
        lastDate: '30/06/2563',
        rating: 'Fair',
        status: 'Open',
      },
    ],
  },
];

export const ListAssess = [
  {
    id: 1,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Fair',
    status: 'Open',
  },
  {
    id: 2,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Fair',
    status: 'Open',
  },
  {
    id: 3,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Good',
    status: 'Open',
  },
  {
    id: 1,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Fair',
    status: 'Postpone',
  },
  {
    id: 4,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Fair',
    status: 'Open',
  },
  {
    id: 5,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Poor',
    status: 'Close',
  },
  {
    id: 6,
    text: `1.4.4.1 Establish internal controls policies, and procedures"`,
    dueDate: '30/06/2563',
    lastDate: '30/06/2563',
    rating: 'Fair',
    status: 'Close',
  },
];

export const dataListRoadMapRe = [
  {
    id: 1,
    text: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    requestDate: '03/01/2020',
    owner: 'Busarakham',
    deptDiv: 'S-RC-IC',
  },
  {
    id: 2,
    text: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    requestDate: '03/01/2020',
    owner: 'Busarakham',
    deptDiv: 'S-RC-IC',
  },
  {
    id: 3,
    text: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    requestDate: '03/01/2020',
    owner: 'Busarakham',
    deptDiv: 'S-RC-IC',
  },
  {
    id: 4,
    text: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    requestDate: '03/01/2020',
    owner: 'Busarakham',
    deptDiv: 'S-RC-IC',
  },
  {
    id: 5,
    text: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    requestDate: '03/01/2020',
    owner: 'Busarakham',
    deptDiv: 'S-RC-IC',
  },
];
export const dataListRoadMap = [
  {
    id: 1,
    textHeader: `1.4.4 Manage Internal Control`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `1.4.4.1 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: '2020',
        status: 'New Assessment',
        ICAgent: true,
      },
      {
        id: 2,
        text: `1.4.4.2 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'none',
        status: 'Re Assessment',
        ICAgent: false,
      },
      {
        id: 3,
        text: `1.4.4.3 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: '2020',
        status: 'New Assessment',
        ICAgent: true,
      },
    ],
  },
  {
    id: 2,
    textHeader: `1.3.1 Manage Internal Control`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `1.3.1.1 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
    ],
  },
  {
    id: 3,
    textHeader: `3.1.2 Maintenance Management and Shutdowns`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `3.1.2.1 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
      {
        id: 2,
        text: `3.1.2.2 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
      {
        id: 3,
        text: `3.1.2.3 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
    ],
  },
];

export const dataListCOSORoadMap = [
  {
    id: 1,
    textHeader: `1. Manage Internal Control`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `1.4 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: '2020',
        status: 'New Assessment',
        ICAgent: true,
      },
      {
        id: 2,
        text: `1.2 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'none',
        status: 'Re Assessment',
        ICAgent: false,
      },
      {
        id: 3,
        text: `1.3 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: '2020',
        status: 'New Assessment',
        ICAgent: true,
      },
    ],
  },
  {
    id: 2,
    textHeader: `3. Manage Internal Control`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `3.1 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
    ],
  },
  {
    id: 3,
    textHeader: `4. Maintenance Management and Shutdowns`,
    disable: true,
    subData: [
      {
        id: 1,
        text: `4.1 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
      {
        id: 2,
        text: `4.2 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
      {
        id: 3,
        text: `4.3 Manage Internal Control`,
        assessor: 'Busarakham',
        assessment: 'remark',
        status: 'Re Assessment',
        ICAgent: true,
      },
    ],
  },
];

export const dataListFollow = [
  {
    id: 1,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
  {
    id: 2,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
  {
    id: 3,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
  {
    id: 4,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
  {
    id: 5,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
  {
    id: 6,
    text: `1.4.4 Manage Internal Control`,
    year: 'à¸›à¸£à¸°à¸ˆà¸³à¸›à¸µ 2563 - à¸„à¸§à¸­à¸£à¹Œà¹€à¸•à¸­à¸£à¹Œ 1',
    close: '10',
    open: '12',
    postPone: '6',
  },
];

export const dataCardonut = [
  { id: 1, text: 'Preparing', color: '#1b1464' },
  { id: 2, text: 'Awaiting DM Approve', color: '#002247' },
  { id: 3, text: 'Awaiting VP Approve', color: '#013a7a' },
  { id: 4, text: 'Awaiting 2nd Line Approve', color: '#0057b8' },
  { id: 5, text: 'Closed', color: '#00aeef' },
];

export const dataCardStatus = [
  { id: 1, text: 'Complete', color: colors.textPurple },
  { id: 2, text: 'Closed', color: colors.greenMint },
  { id: 3, text: 'Overdue', color: colors.red },
  { id: 4, text: 'Postpone', color: colors.orange },
  { id: 5, text: 'Open', color: colors.pink },
  { id: 1, text: 'ASSESSMENT', color: '#666666' },
  { id: 2, text: 'APPROVE BY VP', color: '#0057b8' },
  { id: 3, text: 'REVIEW BY S-RC-IC', color: '#ff8111' },
  { id: 4, text: 'COMPLETE', color: '#00b140' },
];

export const dataCardPie = [
  { id: 1, text: 'GOOD+', color: '#00b5e1' },
  { id: 2, text: 'GOOD', color: '#00b140' },
  { id: 3, text: 'FAIR', color: '#ffc72c' },
  { id: 4, text: 'POOR', color: '#e4002b' },
];

// assessment

export const dataControlASToDo = [
  {
    id: 1,
    submit: 'Submit',
    textHeader: `1.4.4 Manage Internal Control`,
    subData: [
      {
        id: 1,
        submit: 'Submit',
        text: `1.4.4.1 Establish internal control policies, and procedures`,
        readyToSubmit: 'Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting DM Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 2,
        submit: 'Submit',
        text: `1.4.4.2 Develop and moniter compliance with internal control policies and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'New Assessment',
        lastUpdate: '20/04/2020',
        status: 'Preparing',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 3,
        submit: 'Not Submit',
        text: `1.4.4.3 Establish internal control policies, and procedures`,
        readyToSubmit: 'Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting VP Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 4,
        submit: 'Not Submit',
        text: `1.4.4.4 Develop and moniter compliance with internal control policies and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'New Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting 2nd Line Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 5,
        submit: 'Not Submit',
        text: `1.4.4.5 Establish internal control policies, and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Preparing',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
    ],
  },
  {
    id: 2,
    submit: 'Submit',
    textHeader: `3.1.1 Manage inventory and warehouse`,
    subData: [
      {
        id: 1,
        submit: 'Submit',
        text: `3.1.1.1 Establish internal control policies, and procedures`,
        readyToSubmit: 'Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting DM Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 2,
        submit: 'Submit',
        text: `3.1.1.2 Develop and moniter compliance with internal control policies and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'New Assessment',
        lastUpdate: '20/04/2020',
        status: 'Preparing',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 3,
        submit: 'Not Submit',
        text: `3.1.1.3 Establish internal control policies, and procedures`,
        readyToSubmit: 'Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting VP Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 4,
        submit: 'Not Submit',
        text: `3.1.1.4 Develop and moniter compliance with internal control policies and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'New Assessment',
        lastUpdate: '20/04/2020',
        status: 'Awaiting 2nd Line Approve',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
      {
        id: 5,
        submit: 'Not Submit',
        text: `3.1.1.5 Establish internal control policies, and procedures`,
        readyToSubmit: 'Not Ready to Submit',
        type: 'Re Assessment',
        lastUpdate: '20/04/2020',
        status: 'Preparing',
        objective:
          'à¸ªà¸£à¹‰à¸²à¸‡à¸„à¸§à¸²à¸¡à¹€à¸Šà¸·à¹ˆà¸­à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥à¸§à¹ˆà¸² à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¸—à¸µà¹ˆà¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹€à¸«à¸¡à¸²à¸°à¸ªà¸¡ à¹à¸¥à¸°à¸›à¸à¸´à¸šà¸±à¸•à¸´à¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸¡à¹ˆà¸³à¹€à¸ªà¸¡à¸­',
        type: 'à¸”à¹‰à¸²à¸™à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£ (Operations)',
        riskAndControl: [
          {
            idNo: 1,
            risk:
              'à¸šà¸£à¸´à¸©à¸±à¸—à¹„à¸¡à¹ˆà¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸žà¸·à¹ˆà¸­à¸›à¹‰à¸­à¸‡à¸à¸±à¸™à¸«à¸£à¸·à¸­à¸¥à¸”à¸‚à¹‰à¸­à¸œà¸´à¸”à¸žà¸¥à¸²à¸”à¸—à¸µà¹ˆà¸­à¸²à¸ˆà¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™',
            control: [
              {
                id: 1,
                detail:
                  'à¸šà¸£à¸´à¸©à¸±à¸—à¸¡à¸µà¸£à¸°à¸šà¸šà¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™ 2 à¸£à¸°à¸”à¸±à¸š à¸­à¸±à¸™à¹„à¸”à¹‰à¹à¸à¹ˆ 1. Process Level: Process Owner à¸—à¸¸à¸à¸ªà¹ˆà¸§à¸™à¸‡à¸²à¸™à¸•à¹‰à¸­à¸‡à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™ à¹ƒà¸™à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£à¸•à¸™à¹€à¸­à¸‡ à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ (Control Self-Assessment: CSA) 2. Corporate Level: à¸«à¸™à¹ˆà¸§à¸¢à¸‡à¸²à¸™à¸—à¸µà¹ˆà¹€à¸à¸µà¹ˆà¸¢à¸§à¸‚à¹‰à¸­à¸‡à¸—à¸³à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸à¸²à¸£à¸„à¸§à¸šà¸„à¸¸à¸¡à¸ à¸²à¸¢à¹ƒà¸™à¹‚à¸”à¸¢à¹ƒà¸Šà¹‰à¹à¸šà¸šà¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸‚à¸­à¸‡à¸à¸¥à¸•. à¹€à¸›à¹‡à¸™à¸›à¸£à¸°à¸ˆà¸³à¸—à¸¸à¸à¸›à¸µ',
                evaluation: null,
              },
            ],
            mapping: true,
          },
          {
            idNo: 2,
            risk:
              'Process Owner à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹„à¸¡à¹ˆà¸„à¸£à¸šà¸–à¹‰à¸§à¸™ à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆà¸ªà¸¡à¹€à¸«à¸•à¸¸à¸ªà¸¡à¸œà¸¥',
            control: [
              {
                id: 2,
                detail:
                  'à¸à¸³à¸«à¸™à¸”à¹ƒà¸«à¹‰à¸¡à¸µà¸à¸²à¸£à¸ªà¸­à¸šà¸—à¸²à¸™ CSA à¹à¸¥à¸°à¸­à¸™à¸¸à¸¡à¸±à¸•à¸´à¹‚à¸”à¸¢ DM à¹à¸¥à¸° VP à¸•à¸²à¸¡à¸¥à¸³à¸”à¸±à¸š',
                evaluation: 'Good',
              },
              {
                id: 3,
                detail:
                  'Process Owner à¸ªà¸²à¸¡à¸²à¸£à¸–à¸žà¸´à¸ˆà¸²à¸£à¸“à¸² General Risk & Control à¸‚à¸­à¸‡à¹à¸•à¹ˆà¸¥à¸°à¸à¸£à¸°à¸šà¸§à¸™à¸à¸²à¸£ à¸ˆà¸²à¸ Risk & Control Library à¹€à¸›à¹‡à¸™à¹à¸™à¸§à¸—à¸²à¸‡à¹ƒà¸™à¸à¸²à¸£à¸›à¸£à¸°à¹€à¸¡à¸´à¸™à¸„à¸§à¸²à¸¡à¹€à¸ªà¸µà¹ˆà¸¢à¸‡à¹€à¸šà¸·à¹‰à¸­à¸‡à¸•à¹‰à¸™',
                evaluation: 'Fair',
              },
            ],
            mapping: false,
          },
        ],
      },
    ],
  },
];
