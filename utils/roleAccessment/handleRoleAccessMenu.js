import { initAuthStore } from "../../contexts";

const authContext = initAuthStore();

const handleAccessMenu = (accessAuth) => {
  if (
    !authContext.roles.isAdmin &&
    !authContext.roles.isDM &&
    !authContext.roles.isVP
  ) {
    return true;
  }
  return false;

  // const isAccess = authContext.roles.map((role) =>
  //   role.authorities.find((auth) => auth.authority === accessAuth),
  // );
  // const isAccess = authContext.roles.isAdmin;
  // console.log("authContext.roles", isAccess);
  // if (isAccess[0] !== undefined) {
  //   return true;
  // }
  // return false;
};

export default handleAccessMenu;
