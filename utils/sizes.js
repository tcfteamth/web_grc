const sizes = {
  xxxxs: 8,
  xxxs: 10,
  xxs: 12,
  xs: 14,
  s: 16,
  m: 18,
  l: 20,
  xl: 24,
  xxl: 28,
  xxxl: 32,
  xxxxl: 36,
};

export default sizes;
