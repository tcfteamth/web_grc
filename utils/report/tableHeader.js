export const casResault = [
  { key: 'Level 3', render: 'Level 3', width: 200 },
  { key: 'Process', render: 'Process', width: 450 },
  { key: 'Level 4', render: 'Level 4', width: 200 },
  { key: 'Sub Process', render: 'Sub Process', width: 450 },
  { key: 'Division', render: 'Division', width: 200 },
  { key: 'Overall Rating', render: 'Overall Rating', width: 200 },
];

export const csaResultDetail = {
  no: '1.1.1.1',
  lvl: 4,
  children: [
    {
      no: '1.1.1.1.1',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.1.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Keep documented standards of practice and behavior as set forth by Environmental rules and regulations',
              controlDocument: null,
              order: 1001001001001001001,
            },
            {
              no: '1.1.1.1.1.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Request for environmental audits of the business in order to get a clean bill of health or improve on ineffective ecological concerns',
              controlDocument: null,
              order: 1001001001001001001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  ecological concerns',
          order: 1001001001001001,
        },
        {
          no: '1.1.1.1.1.2',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'The Company should monitor and track compliance with its corporate social responsibility program',
              controlDocument: null,
              order: 1001001001001002001,
            },
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify social and cultural changes in the environment where business is conducted',
              controlDocument: null,
              order: 1001001001001002001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  social and cultural changes and trends ',
          order: 1001001001001002,
        },
        {
          no: '1.1.1.1.1.3',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name: 'Analyze the demographics in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Align business demographic composition to that in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง demographics',
          order: 1001001001001003,
        },
        {
          no: '1.1.1.1.1.4',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Lobby against adverse regulatory and legislative agendas by contributing articles or attending discussion sessions',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Proactive identify, manage and react to changing federal and state policy or negative political factors within the local and global market',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify changing legislation which adds compliance requirements that are difficult to manage and very costly in implementation',
              controlDocument: null,
              order: 1001001001001004001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง political and regulatory issues ',
          order: 1001001001001004,
        },
        {
          no: '1.1.1.1.1.5',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.5.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Follow domestic and global economic trends e.g., recessions and downturns in consumer spending',
              controlDocument: null,
              order: 1001001001001005001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง economic trends ',
          order: 1001001001001005,
        },
        {
          no: '1.1.1.1.1.6',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.6.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Act on competitive intelligence to gain a competitive advantage and enter new market segments ahead of competitors',
              controlDocument: null,
              order: 1001001001001006001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง competition',
          order: 1001001001001006,
        },
      ],
      name:
        'ผู้บริหารได้รับข้อมูลสภาพแวดล้อมภายนอก (External environment) อย่างเพียงพอ และเป็นปัจจุบัน เพื่อสนับสนุนการประเมินสมมติฐานสำหรับกำหนดวิสัยทัศน์และกลยุทธองค์กร',
      order: 1001001001001,
      lvl3: 'lvl3',
    },
    {
      no: '1.1.1.1.2',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.2.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ลงทุนและจัดตั้งทีมงานเพื่อศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) เช่น การรับข่าวสารผ่าน industry literature การเข้าร่วมสัมมนาต่างๆ เป็นต้น',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'กำหนดแนวทางในการสื่อสารข้อมูลเกี่ยวกับเทคโนโลยีใหม่ให้ผู้เกี่ยวข้อง ผู้บริหารระดับสูง และคณะกรรมการบริษัทรับทราบ',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ทีมงานศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) รายงานความก้าวหน้าการดำเนินงานอย่างสม่ำเสมอ เช่น ความก้าวหน้าของโครงการเทคโนโลยีที่จัดทำ ผู้รับผิดชอบและผู้สนับสนุนโครงการ แนวทางการพัฒนาใช้ในองค์กร',
              controlDocument: null,
              order: 1001001001002001001,
            },
          ],
          name:
            'กลยุทธ์และการดำเนินงานของบริษัทไม่สอดคล้องกับเทคโนโลยีที่เปลี่ยนแปลงไปตามสภาพแวดล้อมในปัจจุบัน ส่งผลให้บริษัทไม่สามารถสู้กับคู่แข่งได้',
          order: 1001001001002001,
        },
      ],
      name: 'ผู้บริหารเข้าใจเกี่ยวกับเทคโนโลยีในปัจจุบัน',
      order: 1001001001002,
    },
  ],
  name: 'CSA Result',
  order: 1001001001,
};

export const csaCorrectiveDetail = {
  no: '1.1.1.1',
  lvl: 4,
  children: [
    {
      no: '1.1.1.1.1',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.1.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Keep documented standards of practice and behavior as set forth by Environmental rules and regulations',
              controlDocument: null,
              order: 1001001001001001001,
            },
            {
              no: '1.1.1.1.22222',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Request for environmental audits of the business in order to get a clean bill of health or improve on ineffective ecological concerns',
              controlDocument: null,
              order: 1001001001001001001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  ecological concerns',
          order: 1001001001001001,
        },
        {
          no: '1.1.1.1.1.2',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'The Company should monitor and track compliance with its corporate social responsibility program',
              controlDocument: null,
              order: 1001001001001002001,
            },
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify social and cultural changes in the environment where business is conducted',
              controlDocument: null,
              order: 1001001001001002001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  social and cultural changes and trends ',
          order: 1001001001001002,
        },
        {
          no: '1.1.1.1.1.3',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name: 'Analyze the demographics in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Align business demographic composition to that in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง demographics',
          order: 1001001001001003,
        },
        {
          no: '1.1.1.1.1.4',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Lobby against adverse regulatory and legislative agendas by contributing articles or attending discussion sessions',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Proactive identify, manage and react to changing federal and state policy or negative political factors within the local and global market',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify changing legislation which adds compliance requirements that are difficult to manage and very costly in implementation',
              controlDocument: null,
              order: 1001001001001004001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง political and regulatory issues ',
          order: 1001001001001004,
        },
        {
          no: '1.1.1.1.1.5',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.5.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Follow domestic and global economic trends e.g., recessions and downturns in consumer spending',
              controlDocument: null,
              order: 1001001001001005001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง economic trends ',
          order: 1001001001001005,
        },
        {
          no: '1.1.1.1.1.6',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.6.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Act on competitive intelligence to gain a competitive advantage and enter new market segments ahead of competitors',
              controlDocument: null,
              order: 1001001001001006001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง competition',
          order: 1001001001001006,
        },
      ],
      name: 'sub process',
      order: 1001001001001,
      lvl3: 'Division',
    },
    {
      no: '1.1.1.1.2',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.2.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ลงทุนและจัดตั้งทีมงานเพื่อศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) เช่น การรับข่าวสารผ่าน industry literature การเข้าร่วมสัมมนาต่างๆ เป็นต้น',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'กำหนดแนวทางในการสื่อสารข้อมูลเกี่ยวกับเทคโนโลยีใหม่ให้ผู้เกี่ยวข้อง ผู้บริหารระดับสูง และคณะกรรมการบริษัทรับทราบ',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ทีมงานศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) รายงานความก้าวหน้าการดำเนินงานอย่างสม่ำเสมอ เช่น ความก้าวหน้าของโครงการเทคโนโลยีที่จัดทำ ผู้รับผิดชอบและผู้สนับสนุนโครงการ แนวทางการพัฒนาใช้ในองค์กร',
              controlDocument: null,
              order: 1001001001002001001,
            },
          ],
          name:
            'กลยุทธ์และการดำเนินงานของบริษัทไม่สอดคล้องกับเทคโนโลยีที่เปลี่ยนแปลงไปตามสภาพแวดล้อมในปัจจุบัน ส่งผลให้บริษัทไม่สามารถสู้กับคู่แข่งได้',
          order: 1001001001002001,
        },
      ],
      name: 'ผู้บริหารเข้าใจเกี่ยวกับเทคโนโลยีในปัจจุบัน',
      order: 1001001001002,
    },
  ],
  name: 'CSA Corrective',
  order: 1001001001,
};

export const csaEnhancementDetail = {
  no: '1.1.1.1',
  lvl: 4,
  children: [
    {
      no: '1.1.1.1.1',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.1.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Keep documented standards of practice and behavior as set forth by Environmental rules and regulations',
              controlDocument: null,
              order: 1001001001001001001,
            },
            {
              no: '1.1.1.1.1.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Request for environmental audits of the business in order to get a clean bill of health or improve on ineffective ecological concerns',
              controlDocument: null,
              order: 1001001001001001001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  ecological concerns',
          order: 1001001001001001,
        },
        {
          no: '1.1.1.1.1.2',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'The Company should monitor and track compliance with its corporate social responsibility program',
              controlDocument: null,
              order: 1001001001001002001,
            },
            {
              no: '1.1.1.1.1.2.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify social and cultural changes in the environment where business is conducted',
              controlDocument: null,
              order: 1001001001001002001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง  social and cultural changes and trends ',
          order: 1001001001001002,
        },
        {
          no: '1.1.1.1.1.3',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name: 'Analyze the demographics in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
            {
              no: '1.1.1.1.1.3.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Align business demographic composition to that in the market place',
              controlDocument: null,
              order: 1001001001001003001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง demographics',
          order: 1001001001001003,
        },
        {
          no: '1.1.1.1.1.4',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Lobby against adverse regulatory and legislative agendas by contributing articles or attending discussion sessions',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Proactive identify, manage and react to changing federal and state policy or negative political factors within the local and global market',
              controlDocument: null,
              order: 1001001001001004001,
            },
            {
              no: '1.1.1.1.1.4.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Identify changing legislation which adds compliance requirements that are difficult to manage and very costly in implementation',
              controlDocument: null,
              order: 1001001001001004001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง political and regulatory issues ',
          order: 1001001001001004,
        },
        {
          no: '1.1.1.1.1.5',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.5.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Follow domestic and global economic trends e.g., recessions and downturns in consumer spending',
              controlDocument: null,
              order: 1001001001001005001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง economic trends ',
          order: 1001001001001005,
        },
        {
          no: '1.1.1.1.1.6',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.1.6.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'Act on competitive intelligence to gain a competitive advantage and enter new market segments ahead of competitors',
              controlDocument: null,
              order: 1001001001001006001,
            },
          ],
          name:
            'ข้อมูลสภาพแวดล้อมภายนอกอาจไม่ได้รับ ไม่เพียงพอ ไม่เป็นปัจจุบัน หรือขาดบางข้อมูลในเรื่อง competition',
          order: 1001001001001006,
        },
      ],
      name:
        'ผู้บริหารได้รับข้อมูลสภาพแวดล้อมภายนอก (External environment) อย่างเพียงพอ และเป็นปัจจุบัน เพื่อสนับสนุนการประเมินสมมติฐานสำหรับกำหนดวิสัยทัศน์และกลยุทธองค์กร',
      order: 1001001001001,
      lvl3: 'lvl3',
    },
    {
      no: '1.1.1.1.2',
      lvl: 5,
      children: [
        {
          no: '1.1.1.1.2.1',
          lvl: 6,
          children: [
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ลงทุนและจัดตั้งทีมงานเพื่อศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) เช่น การรับข่าวสารผ่าน industry literature การเข้าร่วมสัมมนาต่างๆ เป็นต้น',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'กำหนดแนวทางในการสื่อสารข้อมูลเกี่ยวกับเทคโนโลยีใหม่ให้ผู้เกี่ยวข้อง ผู้บริหารระดับสูง และคณะกรรมการบริษัทรับทราบ',
              controlDocument: null,
              order: 1001001001002001001,
            },
            {
              no: '1.1.1.1.2.1.1',
              controlFrequency: null,
              lvl: 7,
              controlType: null,
              controlFormat: null,
              name:
                'ทีมงานศึกษาและติดตามความก้าวหน้าของเทคโนโลยีใหม่ (Technology research and development department) รายงานความก้าวหน้าการดำเนินงานอย่างสม่ำเสมอ เช่น ความก้าวหน้าของโครงการเทคโนโลยีที่จัดทำ ผู้รับผิดชอบและผู้สนับสนุนโครงการ แนวทางการพัฒนาใช้ในองค์กร',
              controlDocument: null,
              order: 1001001001002001001,
            },
          ],
          name:
            'กลยุทธ์และการดำเนินงานของบริษัทไม่สอดคล้องกับเทคโนโลยีที่เปลี่ยนแปลงไปตามสภาพแวดล้อมในปัจจุบัน ส่งผลให้บริษัทไม่สามารถสู้กับคู่แข่งได้',
          order: 1001001001002001,
        },
      ],
      name: 'ผู้บริหารเข้าใจเกี่ยวกับเทคโนโลยีในปัจจุบัน',
      order: 1001001001002,
    },
  ],
  name: 'CSA Enhancement',
  order: 1001001001,
};
