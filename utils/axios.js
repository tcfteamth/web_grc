import axios from 'axios';
import config from './config';

const axiosInstance = axios.create({
  baseURL: config.apiGatewayBaseUrl,
  // headers: {
  //   Authorization: `Bearer ${config.requesterToken}`,
  //   'Access-Control-Allow-Origin': '*',
  //   'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',
  //   'Access-Control-Allow-Headers': 'Content-Type, Authorization, Content-Length, X-Requested-With',
  // },

});

export default axiosInstance;
