export const CONTROL_TYPE_OPTIONS = [
  {
    key: 'Preventive control',
    text: 'Preventive control',
    value: 'Preventive control',
  },
  {
    key: 'Detective control',
    text: 'Detective control',
    value: 'Detective control',
  },
];

export const CONTROL_FORMAT_OPTIONS = [
  {
    key: 'Automate Control',
    text: 'Automate Control',
    value: 'Automate Control',
  },
  {
    key: 'IT Dependent Control',
    text: 'IT Dependent Control',
    value: 'IT Dependent Control',
  },
  {
    key: 'Manual Control',
    text: 'Manual Control',
    value: 'Manual Control',
  },
];

export const CONTROL_FREQUENCY_OPTIONS = [
  {
    key: 'Daily',
    text: 'Daily',
    value: 'Daily',
  },
  {
    key: 'Weekly',
    text: 'Weekly',
    value: 'Weekly',
  },
  {
    key: 'Monthly',
    text: 'Monthly',
    value: 'Monthly',
  },
  {
    key: 'Quarterly',
    text: 'Quarterly',
    value: 'Quarterly',
  },
  {
    key: 'Half-yearly',
    text: 'Half-yearly',
    value: 'Half-yearly',
  },
  {
    key: 'Yearly',
    text: 'Yearly',
    value: 'Yearly',
  },
  {
    key: 'Recurring',
    text: 'Recurring',
    value: 'Recurring',
  },
];
