export const BUTTON_NAME = {
  save: 'Save',
  sendToDm: 'Send To DM',
  sendToVp: 'Send To VP',
  accept: 'Accept',
  sendToStaff: 'Send To Staff',
};
