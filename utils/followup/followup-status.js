export const followUpStatus = {
  POSTPONE: 'Postpone',
  CLOSED: 'Closed',
  CANCELLED: 'Cancelled',
};
