// url ที่ใช้ call api ให้ดูในไฟล์ .env
const baseUrl =
  process.env.NEXT_PUBLIC_PRODUCTION_ENV_VARIABLE ||
  process.env.NEXT_PUBLIC_LOCAL_BASE_URL ||
  process.env.NEXT_PUBLIC_DEVELOPMENT_ENV_VARIABLE;

const baseUrlRedirect =
  process.env.NEXT_PUBLIC_PRODUCTION_ENV_VARIABLE_REDIRECT ||
  process.env.NEXT_PUBLIC_DEVELOPMENT_ENV_VARIABLE_REDIRECT;

const config = {
  // Deploy Dev server
  // baseUrl: 'http://34.87.36.5:8080',
  // apiGatewayBaseUrl: 'http://34.87.36.5:8080',
  // defaultGatewayUrl: 'http://34.87.36.5:8080',
  // baseUrl: 'https://192.168.0.133:8443',
  // apiGatewayBaseUrl: 'https://192.168.0.133:8443',
  // defaultGatewayUrl: 'https://192.168.0.133:8443',
  // Deploy Prod server
  // baseUrl: 'https://gcgpecsa.azurewebsites.net',
  // apiGatewayBaseUrl: 'https://gcgpecsa.azurewebsites.net',
  // defaultGatewayUrl: 'https://gcgpecsa.azurewebsites.net',
  // Desktop Local
  //baseUrl: 'http://localhost:8080',
  //apiGatewayBaseUrl: 'http://localhost:8080',
  //defaultGatewayUrl: 'http://localhost:8080',
  // 192.168.0.131
  // Deploy QAS server
  // baseUrl: 'https://gcgpecsa-qas.azurewebsites.net',
  // apiGatewayBaseUrl: 'https://gcgpecsa-qas.azurewebsites.net',
  // defaultGatewayUrl: 'https://gcgpecsa-qas.azurewebsites.net',

  // Desktop Local
  // baseUrl: 'http://35.198.213.240:8080',
  // apiGatewayBaseUrl: 'http://35.198.213.240:8080',
  // defaultGatewayUrl: 'http://35.198.213.240:8080',

  // 192.168.100.177:8080
  // Deploy Prod server
  // baseUrl: 'https://gcgpecsa.azurewebsites.net',
  // apiGatewayBaseUrl: 'https://gcgpecsa.azurewebsites.net',
  // defaultGatewayUrl: 'https://gcgpecsa.azurewebsites.net',
  // baseUrl: `${baseUrl}`,
  apiGatewayBaseUrl: `${baseUrl}`,
  defaultGatewayUrl: `${baseUrl}`,
  baseUrlLogin: `${baseUrlRedirect}`,
};
export default config;
