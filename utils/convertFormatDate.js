import moment from 'moment';

const convertFormatDate = (date) => {
  return date ? moment(date).format('DD/MM/YYYY') : '-';
};

export default convertFormatDate;
