import React, { createContext } from 'react';
import { observable, action, decorate } from 'mobx';
import storejs from 'store';
import _ from 'lodash';

let store = null;

class DataContext {
  @observable loading = false;

  @observable name = '';

  @observable id = null;

  @observable isDelete = false;

  @observable roleId = [];

  @observable mode = 'list'; // list | planing

  @observable page = '';

  @observable status = null;

  constructor() {}

  @action setPage(page) {
    this.page = page;
  }

  @action setName(text) {
    this.name = text;
    console.log(this.name);
  }

  @action getAccessMenu() {
    const result = this.roleId.length === 0 || !this.name;

    return result;
  }
}

export default function initDataContext() {
  if (store === null) {
    store = new DataContext();
  }
  return store;
}
