import {
  decorate, observable, action, computed,
} from 'mobx';
import { initAuthStore } from '.';

class TimeoutStore {
  currentlyCall = null

  recentlyCalled = null

  activeTimeoutModal = false // modal สำหรับ inactive เกิน 5 นาทืี

  activeTokenExpiredModal = false // modal สำหรับ token หมดอายุ

  constructor(authStore) {
    this.authStore = authStore;
  }

  setCurrentlyCall(newRecord) {
    this.recentlyCalled = this.currentlyCall;
    this.currentlyCall = newRecord;

    this.isTimeout();
  }

  isTimeout() {
    // 5 min = 300000 millisecond - test uat
    // 30 min = 1800000 millisecond - on production

    if (this.currentlyCall - this.recentlyCalled > 3000000000) {
      this.activeTimeoutModal = true;
    } else {
      this.activeTimeoutModal = false;
    }
  }

  setActiveTimeoutModal() {
    this.activeTimeoutModal = true;
  }

  setActiveTokenExpiredModal() {
    this.activeTokenExpiredModal = true;
  }
}

decorate(TimeoutStore, {
  currentlyCall: observable,
  recentlyCalled: observable,
  activeTimeoutModal: observable,
  activeTokenExpiredModal: observable,
  setcurrentlyCall: action,
  isTimeout: action,
  setActiveTimeoutModal: action,
  setActiveTokenExpiredModal: action,
});

const authStore = new initAuthStore();

export default new TimeoutStore(authStore);
