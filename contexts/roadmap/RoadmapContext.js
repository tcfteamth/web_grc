import {
  observable, action, decorate, runInAction, computed,
} from 'mobx';
import request from '../../services';
import loadingStore from '../LoadingStore';
import timeoutStore from '../TimeoutStore';

class RoadmapContext {
  year = null

  departmentList = null

  selectedDepartment = null

  departmentId = null

  divisionListById = null

  lvlThreeList = null

  selectedLvlThreeNo = null

  remark = null

  roadmapList = null

  searchRoadmapText = ''

  setRemark(text) {
    this.remark = text;
  }

  async getDepartmentList() {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);

      await request.roadmapServices.getDepartmentList().then(response => {
        runInAction(() => {
          this.departmentList = response.data;

          loadingStore.setIsLoading(false);
        });
      }).catch(e => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  async getDivisionListById(departmentId) {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);

      await request.roadmapServices.getDivisionById(departmentId).then(response => {
        runInAction(() => {
          this.divisionListById = response.data;

          loadingStore.setIsLoading(false);
        });
      }).catch(e => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  async getLvlThreeList() {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);

      await request.roadmapServices.getLvlThreeList().then(response => {
        runInAction(() => {
          this.lvlThreeList = response.data;

          loadingStore.setIsLoading(false);
        });
      }).catch(e => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  async getRoadmapList(token, page) {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      await request.roadmapServices.getRoadmapList(token, page, this.searchRoadmapText).then(response => {
        runInAction(() => {
          this.roadmapList = response.data.roadmaps;
        });
      }).catch(e => {
        console.log(e);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  get departmentOptions() {
    return this.departmentList.map((item) => ({
      value: item.id,
      label: item.departmentName,
      text: item.departmentName,
    }));
  }

  get lvlThreeOptions() {
    return this.lvlThreeList.map((item) => ({
      value: item.no,
      label: `${item.no} ${item.name}`,
      text: `${item.no} ${item.name}`,
    }));
  }

  setSelectedDepartment(department) {
    this.selectedDepartment = department;
  }

  setYear(newYear) {
    this.year = newYear;
  }

  setSearchRoadmapText(text) {
    this.searchRoadmapText = text;
  }

  clearSearchText() {
    this.searchRoadmapText = '';
  }
}

decorate(RoadmapContext, {
  year: observable,
  departmentList: observable,
  selectedDepartment: observable,
  departmentId: observable,
  divisionListById: observable,
  lvlThreeList: observable,
  selectedLvlThreeNo: observable,
  remark: observable,
  roadmapList: observable,
  searchRoadmapText: observable,
  getDepartmentList: action,
  getDivisionListById: action,
  getLvlThreeList: action,
  getRoadmapList: action,
  setSelectedDepartment: action,
  setYear: action,
  departmentOptions: computed,
  lvlThreeOptions: computed,
  setRemark: action,
  setSearchRoadmapText: action,
  clearSearchText: action,
});

export default new RoadmapContext();
