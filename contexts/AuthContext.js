/* eslint-disable class-methods-use-this */
import {
  action,
  observable,
  toJS,
  decorate,
  computed,
  runInAction,
} from 'mobx';
import storejs from 'store';
import _ from 'lodash';
import { config } from '../utils';
import { axiosInstance } from '../helpers/utils';

export const ROLES = {
  admin: 'ADMIN',
  dm: 'DM',
  vp: 'VP',
  staff: 'STAFF',
};

let store = null;

class AuthStore {
  accessToken = null;

  currentUser = null;

  roles = [];

  roleName = null;

  activeModalSelectRole = false;

  constructor(isServer) {
    this.accessToken = storejs.get('accessToken');
    this.currentUser = storejs.get('currentUser');
    this.roles = storejs.get('roles');
    this.roleName = storejs.get('roleName');
  }

  async loginAD(token, roleId, roleSys) {
    try {
      const formData = new FormData();
      formData.append('roleId', roleId);
      formData.append('roleSys', roleSys);

      const currentUser = await axiosInstance.post(
        `${config.apiGatewayBaseUrl}/api/manage/users/me`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      storejs.set('accessToken', token);
      storejs.set('currentUser', currentUser.data.user);
      storejs.set('roles', currentUser.data.roles);
      storejs.set('roleName', currentUser.data.roles.roleName);
      window.location.href = '/databasePage/dashboard';

      return currentUser;
      // console.log('loginAD', token);
    } catch (error) {
      console.log('error', error);
    }
  }

  async login(employeeID, password) {
    try {
      const formData = new FormData();
      formData.append('client_id', 'clientIdPassword');
      formData.append('username', employeeID);
      formData.append('password', password);
      formData.append('grant_type', 'password');
      formData.append('scope', 'user');

      const response = await axiosInstance.post(
        `${config.apiGatewayBaseUrl}/oauth/token`,
        formData,
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Basic ${btoa('clientIdPassword:secret')}`,
          },
        },
      );

      const currentUser = await axiosInstance.get(
        `${config.apiGatewayBaseUrl}/api/manage/users/me`,
        {
          headers: {
            Authorization: `Bearer ${response.data.access_token}`,
          },
        },
      );
      console.log(currentUser.data.roles);
      storejs.set('accessToken', response.data.access_token);
      storejs.set('currentUser', currentUser.data.user);
      storejs.set('roles', currentUser.data.roles);
      storejs.set('roleName', currentUser.data.roles.roleName);
      window.location.href = '/databasePage/dashboard';

      return response;
    } catch (error) {
      console.log('error', error);
    }
  }

  async logout() {
    storejs.remove('accessToken');
    storejs.remove('currentUser');
    storejs.remove('roles');
    storejs.remove('roleName');
    window.location.href = '/login';
  }

  accessMenu(accessAuth) {
    const isAccess = this.roles.authorities.filter(
      (auth) => auth.authority === accessAuth,
    );

    return isAccess.length !== 0;
  }
}

decorate(AuthStore, {
  accessToken: observable,
  currentUser: observable,
  activeModalSelectRole: observable,
  roles: observable,
  loginAD: action,
  login: action,
  logout: action,
  accessMenu: action,
});

export default function initAuthStore(isServer) {
  if (isServer) {
    return new AuthStore(isServer);
  }
  if (store === null) {
    store = new AuthStore(isServer);
  }
  return store;
}
