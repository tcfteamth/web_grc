import { observable, action, runInAction } from 'mobx';
import request from '../services';
import loadingStore from './LoadingStore';
import timeoutStore from './TimeoutStore';

class ProcessDB {
  @observable isLoading = false;

  @observable processList = null; // filter

  @observable processListNew = null;

  @observable managementProcess = null;

  @observable coreProcess = null;

  @observable support = null;

  @observable processDBById = null;

  @observable lvlOneType = 1; // set defalut for management process\

  @observable NewlvlOneType = 1;

  @observable searchProcessText = '';

  @observable selectLvl2No = '';

  @observable selectLvl3No = '';

  @observable selectLvl4No = '';

  @observable selectTag = '';

  @observable loading = false;

  @observable tagList = null;

  @observable selectedProcessAnd = [];

  @observable selectedProcessOr = [];
  @action setField(field, value) {
    this[field] = value;
  }
  @action async getProcessList() {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);
      const processAndStr = this.selectedProcessAnd.map(x => x.value).join(',')
      const processOrStr = this.selectedProcessOr.map(x => x.value).join(',')
      await request.processDBServices
        .getProcessDBList(
          this.searchProcessText,
          this.selectLvl2No,
          this.selectLvl3No,
          this.selectLvl4No,
          processAndStr,
          processOrStr
        )
        .then((response) => {
          runInAction(() => {
            const [
              managementProcessData = null,
              coreProcessData = null,
              supportData = null,
            ] = response.data.processDB;

            if (this.processList === null) {
              this.processList = response.data.processDB;
            }

            if (response.data.processDB.length < 3) {
              response.data.processDB.map((i) => {
                this.NewlvlOneType = i.no;
              });
            }

            if (response.data.processDB.length === 3) {
              this.managementProcess = managementProcessData;
              this.coreProcess = coreProcessData;
              this.support = supportData;
            }

            if (response.data.processDB.length < 3) {
              const newArr = [null, null, null];

              response.data.processDB.map((process) => {
                if (process.no === '1') {
                  newArr[0] = process;
                }

                if (process.no === '2') {
                  newArr[1] = process;
                }

                if (process.no === '3') {
                  newArr[2] = process;
                }
              });

              this.managementProcess = newArr[0];
              this.coreProcess = newArr[1];
              this.support = newArr[2];
            }

            loadingStore.setIsLoading(false);
          });
        })
        .catch((e) => {
          console.log(e);
          loadingStore.setIsLoading(false);
          if (e.response.status === 401) {
            timeoutStore.setActiveTokenExpiredModal();
          }
        });
    }
  }

  @action async getTagList() {
    await request.processDBServices
      .getTagList()
      .then((response) => {
        runInAction(() => {
          this.tagList = response.data.content;
        });
      })
      .catch((e) => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
  }

  @action async getProcessDBListById(id) {
    loadingStore.setIsLoading(true);

    await request.processDBServices
      .getProcessDBById(id)
      .then((response) => {
        runInAction(() => {
          this.processDBById = response.data;

          loadingStore.setIsLoading(false);
        });
      })
      .catch((e) => {
        console.log(e);
        loadingStore.setIsLoading(false);
      });
  }

  // get data
  @action async exportExcelProcessDatabase(token) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.processDBServices.exportExcelProcessDataBase(
        token,
        this.searchProcessText,
        this.selectLvl2No,
        this.selectLvl3No,
        this.selectLvl4No,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async hideSubprocess(token, id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.processDBServices.hideSubprocess(token, id);
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setLvlOneType(newType) {
    this.lvlOneType = newType;
  }

  @action setSearchProcessText(text) {
    this.searchProcessText = text;
  }

  @action setSelectLvl2No(text) {
    this.selectLvl2No = text;
  }

  @action setSelectLvl3No(text) {
    this.selectLvl3No = text;
  }

  @action setSelectLvl4No(text) {
    this.selectLvl4No = text;
  }

  @action clearSearchText() {
    this.searchProcessText = '';
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action clearfiltter() {
    this.selectLvl4No = '';
    this.selectLvl3No = '';
    this.selectLvl2No = '';
    this.searchProcessText = '';
    this.selectTag = '';
    
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }
}

export default new ProcessDB();
