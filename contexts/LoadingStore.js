import { action, observable, decorate } from 'mobx';
import { initProcessDBContext } from '.';

class LoadingStore {
  isLoading = false

  setIsLoading(status) {
    this.isLoading = status;
  }
}

decorate(LoadingStore, {
  isLoading: observable,
  setIsLoading: action,
});

export default new LoadingStore();
