import {
  observable, action, decorate, runInAction,
} from 'mobx';
import request from '../../services';
import loadingStore from '../LoadingStore';
import timeoutStore from '../TimeoutStore';

class RiskAndControlContext {
  riskAndControlData = null;

  riskAndControlByNoLvl = null;

  page = 1;

  pageSize = null;

  totalPage = null;

  totalContent = null;

  mode = 'list'; // list || noLvl

  searchRiskAndControlText = ''

  async getRiskAndControlList(page) {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);

      await request.processDBServices.getRiskAndControlList(page, this.searchRiskAndControlText).then(response => {
        runInAction(() => {
          const {
            contents, size, totalContent, totalPage, page: racPage,
          } = response.data;

          this.riskAndControlData = contents;
          this.pageSize = size;
          this.page = racPage;
          this.totalContent = totalContent;
          this.totalPage = totalPage;

          // this.searchRiskAndControlText = '';
          loadingStore.setIsLoading(false);
        });
      }).catch(e => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  async getRiskAndControlByNoLvl(noLvl) {
    timeoutStore.setCurrentlyCall(new Date().getTime());

    if (!timeoutStore.activeTimeoutModal) {
      loadingStore.setIsLoading(true);

      await request.processDBServices.getRiskAndControlByNoLvl(noLvl).then((response) => {
        runInAction(() => {
          this.riskAndControlByNoLvl = response.data;
          loadingStore.setIsLoading(false);
        });
      }).catch(e => {
        console.log(e);
        loadingStore.setIsLoading(false);
        if (e.response.status === 401) {
          timeoutStore.setActiveTokenExpiredModal();
        }
      });
    }
  }

  setPage(newPage) {
    this.page = newPage;
  }

  setMode(newMode) {
    this.mode = newMode;
  }

  setSearchRiskAndControlText(text) {
    this.searchRiskAndControlText = text;
  }

  clearNoLvl() {
    this.riskAndControlByNoLvl = null;
  }

  clearSearch() {
    this.searchRiskAndControlText = '';
    this.page = 1;
  }
}

decorate(RiskAndControlContext, {
  riskAndControlData: observable,
  riskAndControlByNoLvl: observable,
  page: observable,
  pageSize: observable,
  totalContent: observable,
  totalPage: observable,
  mode: observable,
  searchRiskAndControlText: observable,
  getRiskAndControlList: action,
  getRiskAndControlByNoLvl: action,
  setPage: action,
  setMode: action,
  setSearchRiskAndControlText: action,
  clearNoLvl: action,
  clearSearch: action,
});

export default new RiskAndControlContext();
