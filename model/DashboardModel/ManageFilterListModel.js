import { observable, action } from 'mobx';
import request from '../../services';

class ManageFilterModel {
  @observable filterName = '';

  @observable isDisable = false;

  @observable isDefaultValue = false;

  constructor(filter) {
    this.filterName = filter.filterName;
    this.isDisable = filter.isDisable;
    this.isDefaultValue = filter.isDefaultValue;
  }
}

class ManageFilterListModel {
  @observable list = [];

  @action async getManageFilterList(token) {
    try {
      const result = await request.dashboardServices.getManageFilterList(token);

      this.list = result.data.map((filter) => new ManageFilterModel(filter));
    } catch (e) {
      console.log(e);
    }
  }

  @action async getFollowUpDashboardManageFilterList() {
    try {
      const result = await request.dashboardFollowUpServices.getManageFilterList();

      this.list = result.data.map((filter) => new ManageFilterModel(filter));
    } catch (e) {
      console.log(e);
    }
  }
}

export default ManageFilterListModel;
