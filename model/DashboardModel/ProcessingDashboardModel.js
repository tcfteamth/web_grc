import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

const DONUT_GRAPH_COLORS = [
  '#8c0d83',
  '#7144a8',
  '#467bc4',
  '#20b7e2',
  '#00ebfa',
];

class SummaryStatusModel {
  @observable isReach = false;

  @observable count = 0;

  @observable status = '';

  constructor(summary) {
    this.isReach = summary.isReach;
    this.count = summary.count;
    this.status = summary.status;
  }
}

class DotGraphListModel {
  @observable indicator = '';

  @observable total = 0;

  @observable summaryStatus = null;

  @observable divEn = '';

  @observable shiftEn = '';

  constructor(dgItem) {
    this.indicator = dgItem.indicator;
    this.divEn = dgItem.divEn;
    this.shiftEn = dgItem.shiftEn;
    this.total = dgItem.total;
    this.summaryStatus = dgItem.summaryStatus.map(
      (summary) => new SummaryStatusModel(summary),
    );
  }
}

class DotGraphModel {
  @observable division = '';

  @observable department = '';

  @observable list = null;

  constructor(dg) {
    this.division = dg.division;
    this.department = dg.department;
    this.list = dg.list.map((dgItem) => new DotGraphListModel(dgItem));
  }
}

class DonutGraphModel {
  @observable name = '';

  @observable count = 0;

  @observable percent = 0;

  constructor(donutGraph) {
    this.name = donutGraph.name;
    this.count = donutGraph.count;
    this.percent = donutGraph.percent;
  }
}

class ProcessingDashboardModel {
  @observable donutGraph = null;

  @observable dotGraph = null;

  @action async getProcessingStatus(token, year, bu, departmentNo, divisionNo, processAnd, processOr, objectives) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.dashboardServices.getProcessingStatus(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.donutGraph = result.data.donutGraph.map(
        (dng) => new DonutGraphModel(dng),
      );

      if (result.data.dotGraph) {
        this.dotGraph = new DotGraphModel(result.data.dotGraph);
      } else {
        this.dotGraph = null;
      }

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @computed get percentDonutGraphList() {
    const percentList = [];

    this.donutGraph.map((dg) => percentList.push(dg.percent));

    return percentList;
  }

  @computed get donutGraphNameWithColorsList() {
    const nameWithColorsList = [];

    this.donutGraph.map((dg, dgIndex) => {
      nameWithColorsList.push({
        name: dg.name,
        color: DONUT_GRAPH_COLORS[dgIndex],
        count: dg.count,
      });
    });

    return nameWithColorsList;
  }

  @computed get isAllNoPercent() {
    return this.donutGraph.every((dng) => dng.percent === 0);
  }

  @computed get allProcessTotal() {
    const total = this.donutGraph.reduce((total, pg) => total + pg.count, 0);

    return total;
  }
}

export default ProcessingDashboardModel;
