import { observable, action } from 'mobx';
import request from '../../services';

class AllWaitModel {
  @observable fullPath;

  @observable year;

  @observable ownerTask;

  @observable id;

  @observable nameEn;

  @observable divisionNo;

  @observable isNewAssessment;

  @observable status;

  constructor(allwait) {
    this.fullPath = allwait.fullPath;
    this.year = allwait.year;
    this.ownerTask = allwait.ownerTask;
    this.id = allwait.id;
    this.nameEn = allwait.nameEn;
    this.divisionNo = allwait.divisionNo;
    this.isNewAssessment = allwait.isNewAssessment;
    this.status = allwait.status;
  }
}

class AllWaitDmAcceptListModel {
  @observable list;

  @action async getAllWaitDmAccept(year, bu, departmentNo, divisionNo, processAnd, processOr, objectives) {
    try {

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.dashboardServices.getAllWaitDMAccpetList(
        year,
        bu,
        departmentNo,
        divisionNo,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.list = result.data.map((allwait) => new AllWaitModel(allwait));
    } catch (e) {
      console.log(e);
    }
  }
}

export default AllWaitDmAcceptListModel;
