import { observable, action, computed } from 'mobx';
import request from '../../services';
import { colors } from '../../utils';

class SummaryRateModel {
  @observable name = '';

  @observable count = 0;

  @observable id = null;

  constructor(summary) {
    this.name = summary.name;
    this.count = summary.count;
    this.id = summary.id;
  }

  @computed get renderGraphColor() {
    if (this.name === 'Good') {
      return colors.green;
    }

    if (this.name === 'Fair') {
      return colors.yellow;
    }

    if (this.name === 'Poor') {
      return colors.red;
    }

    return '';
  }
}

class DetailModel {
  @observable indicator = '';

  @observable total = '';

  @observable summaryRate = null;

  @observable divEn = '';

  @observable shiftEn = '';

  constructor(detail) {
    this.indicator = detail.indicator;
    this.divEn = detail.divEn;
    this.shiftEn = detail.shiftEn;
    this.total = detail.total;
    this.summaryRate = detail.summaryRate.map(
      (summary) => new SummaryRateModel(summary),
    );
  }
}

class PieGraphModel {
  @observable name = '';

  @observable count = 0;

  @observable id = null;

  @observable percent = 0;

  constructor(pieGraph) {
    this.name = pieGraph.name;
    this.count = pieGraph.count;
    this.id = pieGraph.id;
    this.percent = pieGraph.percent;
  }
}

class OverAllRatingDashboardModel {
  @observable pieGraph = '';

  @observable detail = '';

  @action async getOverAllRating(token, year, bu, departmentNo, divisionNo, processAnd, processOr, objectives) {
    try {

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.dashboardServices.getOverAllRating(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.pieGraph = result.data.pieGraph.map((pg) => new PieGraphModel(pg));

      if (result.data.detail) {
        this.detail = result.data.detail.map(
          (detail) => new DetailModel(detail),
        );
      } else {
        this.detail = null;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @computed get percentOverAllRatingList() {
    const overAllRatingList = [];

    this.pieGraph.map((pg) => overAllRatingList.push(pg.percent));

    return overAllRatingList;
  }

  @computed get isAllNoPercent() {
    return this.pieGraph.every((pg) => pg.percent === 0);
  }
}

export default OverAllRatingDashboardModel;
