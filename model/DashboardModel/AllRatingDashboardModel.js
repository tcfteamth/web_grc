import { observable, action, computed } from 'mobx';
import request from '../../services';
import { colors } from '../../utils';

class SummaryRateModel {
  @observable name = '';

  @observable count = 0;

  @observable id = null;

  @observable enhance = 0;

  constructor(summary) {
    this.name = summary.name;
    this.count = summary.count;
    this.id = summary.id;
    this.enhance = summary.enhance;
  }

  @computed get renderGraphColor() {
    if (this.name === 'Good') {
      return colors.green;
    }

    if (this.name === 'Fair') {
      return colors.yellow;
    }

    if (this.name === 'Poor') {
      return colors.red;
    }

    return '';
  }

  @computed get goodWithEnhance() {
    return this.enhance > 0 ? this.enhance : '-';
  }
}

class DetailModel {
  @observable indicator = '';

  @observable total = '';

  @observable summaryRate = null;

  @observable divEn = '';

  @observable shiftEn = '';

  @observable noDepartment = false;

  constructor(detail) {
    this.indicator = detail.indicator;
    this.divEn = detail.divEn;
    this.shiftEn = detail.shiftEn;
    this.total = detail.total;
    this.noDepartment = detail.noDepartment;
    this.summaryRate = detail.summaryRate.map(
      (summary) => new SummaryRateModel(summary),
    );
  }

  @computed get goodWithEnhanceList() {
    const goodList = [];

    this.summaryRate.map((summary) => {
      if (summary.name === 'Good') {
        goodList.push(summary.goodWithEnhance);
      }
    });

    return goodList;
  }
}

class PieGraphModel {
  @observable name = '';

  @observable count = 0;

  @observable id = null;

  @observable percent = 0;

  constructor(pieGraph) {
    this.name = pieGraph.name;
    this.count = pieGraph.count;
    this.id = pieGraph.id;
    this.percent = pieGraph.percent;
  }
}

class AllRatingDashboardModel {
  @observable pieGraph = '';

  @observable detail = '';

  @action async getAllRating(token, year, bu, departmentNo, divisionNo, processAnd, processOr, objectives) {
    try {

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.dashboardServices.getAllRating(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.pieGraph = result.data.pieGraph.map((pg) => new PieGraphModel(pg));

      if (result.data.detail) {
        this.detail = result.data.detail.map(
          (detail) => new DetailModel(detail),
        );
      } else {
        this.detail = null;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @computed get percentOverAllRatingList() {
    const overAllRatingList = [];

    this.pieGraph.map((pg) => overAllRatingList.push(pg.percent));

    return overAllRatingList;
  }

  @computed get isAllNoPercent() {
    return this.pieGraph.every((pg) => pg.percent === 0);
  }
}

export default AllRatingDashboardModel;
