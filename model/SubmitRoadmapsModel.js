import { observable, action, computed } from 'mobx';

class SubmitRoadmapsModel {
  @observable roadmapIds = null;

  @computed get isSelected() {
    return this.roadmapIds !== null;
  }

  // @action setRoadmapIds(id, forceCheck) {
  //   if (forceCheck !== undefined) {
  //     this.roadmapIds = forceCheck ? id : null;
  //   } else {
  //     if (this.roadmapIds === id) {
  //       this.roadmapIds = null;
  //     } else {
  //       this.roadmapIds = id;
  //     }
  //   }
  //   // this.roadmapIds = id
  // }

  @action setRoadmapIds(id) {
    if (this.roadmapIds === id) {
      this.roadmapIds = null;
    } else {
      this.roadmapIds = id;
    }
  }
}

export default SubmitRoadmapsModel;
