import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import SubmitIAFindingModel from './SubmitIAFindingModel';

class IAFindingModel {
  @observable id = '';

  @observable actualEnd = null;

  @observable actualStart = null;

  @observable auditEngagementCode = '';

  @observable auditEngagementName = '';

  @observable auditUnitLink = '';

  @observable auditUnits = '';

  @observable categorizedRecommendation = '';

  @observable closed = null;

  @observable closedBy = '';

  @observable comment = '';

  @observable createdDate = null;

  @observable findingDescription = '';

  @observable findingName = '';

  @observable findingNo = '';

  @observable findingRate = '';

  @observable focusObjective = '';

  @observable impact = '';

  @observable isCheck = '';

  @observable issueState = '';

  @observable recommendationDescription = '';

  @observable recommendationState = '';

  @observable remark1 = '';

  @observable remark2 = '';

  @observable revisedTargetDate = '';

  @observable rootCause = '';

  @observable subProcess = '';

  @observable targetDate = null;

  @observable teamLead = '';

  @observable teamMember = '';

  @observable type = '';

  @observable updatedDate = null;

  @observable year = '';


  @observable submitIAFindingModel = new SubmitIAFindingModel();

  constructor(ia) {
    this.id = ia.id;
    this.actualEnd = ia.actualEnd;
    this.actualStart = ia.actualStart;
    this.auditEngagementCode = ia.auditEngagementCode;
    this.auditEngagementName = ia.auditEngagementName;
    this.auditUnitLink = ia.auditUnitLink;
    this.auditUnits = ia.auditUnits;
    this.categorizedRecommendation = ia.categorizedRecommendation;
    this.closed = ia.closed;
    this.closedBy = ia.closedBy;
    // this.comment = ia.comment;
    this.createdDate = ia.createdDate;
    this.findingDescription = ia.findingDescription;
    this.findingName = ia.findingName;
    this.findingNo = ia.findingNo;
    this.findingRate = ia.findingRate;
    this.focusObjective = ia.focusObjective;
    this.impact = ia.impact;
    this.isCheck = ia.isCheck;
    this.issueState = ia.issueState;
    this.recommendationDescription = ia.recommendationDescription;
    this.recommendationState = ia.recommendationState;
    this.remark1 = ia.remark1;
    this.remark2 = ia.remark2;
    this.revisedTargetDate = ia.revisedTargetDate;
    this.rootCause = ia.rootCause;
    this.subProcess = ia.subProcess;
    this.targetDate = ia.targetDate;
    this.teamLead = ia.teamLead;
    this.teamMember = ia.teamMember;
    this.type = ia.type;
    this.updatedDate = ia.updatedDate;
    this.year = ia.year;
    this.submitIAFindingModel = new SubmitIAFindingModel(
      ia.findingNo,
      ia.auditUnits,
      ia.isCheck,
      ia.comment,
    );
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isCheckYes() {
    return this.isCheck !== null && this.isCheck === true;
  }

  @computed get isCheckNo() {
    return this.isCheck !== null && this.isCheck === false;
  }

  @action setCheck(checked) {
    if (this.isCheck === checked) {
      this.isCheck = null;
      this.setSubmitIAFinding(null);
    } else {
      this.isCheck = checked;
      this.setSubmitIAFinding(checked);
    }
  }

  @action setSubmitIAFinding(checked) {
    this.submitIAFindingModel.setField('findingNo', this.findingNo);
    this.submitIAFindingModel.setField('auditUnit', this.auditUnits);
    this.submitIAFindingModel.setField('isCheck', checked);
    // this.submitIAFindingModel.setField('comment', this.comment);
  }
}

class IAFindingDetailModel {
  @observable iafindingDetailList = [];

  @observable check = false;

  @observable comment = '';

  @action setField(field, value) {
    this[field] = value;
  }

  @action async getIAFindingDetail(token, indicator) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentServices.getIAFindingDetail(
        token,
        indicator,
      );

      if (result.data.length > 0) {
        this.iafindingDetailList = result.data.map(
          (ia) => new IAFindingModel(ia),
        );
      } else {
        this.iafindingDetailList = result.data;
      }
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitIAFinding(token) {
    try {
      loadingStore.setIsLoading(true);

      const submitList = [];

      this.iafindingDetailList.forEach((ia) => {
        submitList.push({ ...ia.submitIAFindingModel });
      });

      const result = await request.assessmentServices.submitIAFindingDetail(
        token,
        submitList,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async exportIafindingDetail(token) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.assessmentServices.exportExcelIafinding(
        token,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async importExcelIa(data) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.assessmentServices.importExcelIafinding(
        data,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setCheckYesAll(checked) {
    if (!this.isCheckYesAll) {
      this.iafindingDetailList.forEach((ia) => {
        if (ia.isCheck !== checked) {
          ia.setCheck(checked);
        }
      });
    } else {
      this.iafindingDetailList.forEach((ia) => {
        ia.setCheck(null);
      });
    }
  }

  @action setCheckNoAll(checked) {
    if (!this.isCheckNoAll) {
      this.iafindingDetailList.forEach((ia) => {
        if (ia.isCheck !== checked) {
          ia.setCheck(checked);
        }
      });
    } else {
      this.iafindingDetailList.forEach((ia) => {
        ia.setCheck(null);
      });
    }
  }

  @computed get isCheckYesAll() {
    const allYes = this.iafindingDetailList.every((ia) => ia.isCheck === true);

    return allYes;
  }

  @computed get isCheckNoAll() {
    const allNo = this.iafindingDetailList.every((ia) => ia.isCheck === false);

    return allNo;
  }

  @computed get isOneOfListChecked() {
    const isCheck = this.iafindingDetailList.some((ia) => ia.isCheck !== null);

    return !isCheck;
  }
}

export default IAFindingDetailModel;
