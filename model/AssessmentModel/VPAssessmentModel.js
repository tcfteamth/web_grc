import { observable, computed, action } from 'mobx';

export const VP_ASSESSMENT_STATUS = {
  accept: 'VP_ACCEPT',
  reject: 'VP_REJECT',
};

class VPAssessmentModel {
  @observable status = VP_ASSESSMENT_STATUS.accept

  @observable id = null

  @observable rejectComment = ''

  @computed get isSelectedMajor() {
    return this.id !== '' && this.status === VP_ASSESSMENT_STATUS.reject.major;
  }

  @computed get isSelectedMinor() {
    return this.id !== '' && this.status === VP_ASSESSMENT_STATUS.reject.minor;
  }

  @action setId(status, id) {
    if (this.status === status) {
      this.id = null;
      this.status = VP_ASSESSMENT_STATUS.accept;
    } else {
      this.id = id;
      this.status = status;
    }
  }

  @action setRejectComment(text) {
    this.rejectComment = text;
    console.log(text);
  }

  @action setStatus(status) {
    this.status = status;
  }
}

export default VPAssessmentModel;
