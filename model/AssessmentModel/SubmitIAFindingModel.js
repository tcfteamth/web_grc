import { observable, action, computed } from 'mobx';

class SubmitIAFindingModel {
  @observable findingNo = '';

  @observable auditUnit = '';

  @observable isCheck = false;

  @observable comment = '';

  constructor(findingNo, auditUnit, isCheck, comment) {
    this.findingNo = findingNo;
    this.auditUnit = auditUnit;
    this.isCheck = isCheck;
    this.comment = comment;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default SubmitIAFindingModel;
