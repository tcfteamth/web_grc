import { action, computed, observable } from 'mobx';
import request from '../../services';

class ResultStatusModel {
  @observable isReach = false;

  @observable count = 0;

  @observable status = '';

  constructor(result) {
    this.isReach = result.isReach;
    this.count = result.count;
    this.status = result.status;
  }
}

class ResultAllStatusModel {
  @observable total = 0;

  @observable name = '';

  @observable status = null;

  constructor(resultAll) {
    this.total = resultAll.total;
    this.name = resultAll.name;
    this.status = resultAll.status.map(
      (result) => new ResultStatusModel(result),
    );
  }

  @computed get divisionName() {
    const divisionName = this.name.split('-');

    return divisionName[divisionName.length - 1];
  }
}

class CardResultVPModel {
  @observable list = null;

  @action async getResultVP(token, departmentId) {
    try {
      const result = await request.assessmentServices.getAllStatusCount(
        token,
        departmentId,
      );

      this.list = result.data.map(
        (resultTotal) => new ResultAllStatusModel(resultTotal),
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }
}

export default CardResultVPModel;
