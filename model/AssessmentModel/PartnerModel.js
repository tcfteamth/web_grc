import { observable, action } from 'mobx';

class PartnerModel {
  @observable partnerName = '';

  @observable partnerId = null;

  @action setPartner(partnerObj) {
    this.partnerId = partnerObj.value;
    this.partnerName = partnerObj.label;
  }
}

export default PartnerModel;
