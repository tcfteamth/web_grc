import { action, computed, observable } from 'mobx';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';
class Lv7MappingModel {
  @observable no;

  @observable lvl;

  @observable children;

  @observable name;

  @observable order;

  @observable controlFrequency;

  @observable isStandard;

  @observable controlType;

  @observable controlFormat;

  @observable controlDocument;

  @observable id;

  @observable selectedMapping;

  constructor(lv7) {
    this.no = lv7.no;
    this.lvl = lv7.lvl;
    this.children = lv7.children;
    this.name = lv7.name;
    this.order = lv7.order;
    this.controlFrequency = lv7.controlFrequency;
    this.isStandard = lv7.isStandard;
    this.controlType = lv7.controlType;
    this.controlFormat = lv7.controlFormat;
    this.controlDocument = lv7.controlDocument;
    this.id = lv7.id;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSelected() {
    return this.selectedMapping !== undefined;
  }

  @action setMapping(id) {
    if (this.selectedMapping === id) {
      this.selectedMapping = undefined;
    } else {
      this.selectedMapping = id;
    }
  }
}

class Lv6MappingModel {
  @observable no;

  @observable lvl;

  @observable children;

  @observable name;

  @observable order;

  @observable fullPath;

  constructor(lv6) {
    this.no = lv6.no;
    this.lvl = lv6.lvl;
    this.children = lv6.children.map((lv7) => new Lv7MappingModel(lv7));
    this.name = lv6.name;
    this.order = lv6.order;
    this.fullPath = lv6.fullPath;
  }
}

class Lv5MappingModel {
  @observable no;

  @observable lvl;

  @observable children;

  @observable name;

  @observable order;

  constructor(lv5) {
    this.no = lv5.no;
    this.lvl = lv5.lvl;
    this.children = lv5.children.map((lv6) => new Lv6MappingModel(lv6));
    this.name = lv5.name;
    this.order = lv5.order;
  }

  @computed get rowSpanLv5() {
    const sum = this.children.reduce(
      (accumulator, lv6) => accumulator + lv6.children.length,
      0,
    );

    return sum;
  }
}

class Lv4MappingModel {
  @observable no;

  @observable lvl;

  @observable children;

  @observable name;

  @observable order;

  constructor(lv4) {
    this.no = lv4.no;
    this.lvl = lv4.no;
    this.children = lv4.children.map((lv5) => new Lv5MappingModel(lv5));
    this.name = lv4.name;
    this.order = lv4.order;
  }

  @computed get rowSpanLv4() {
    const sum = this.children.reduce(
      (accumulator, lv5) => accumulator + lv5.rowSpanLv5,
      0,
    );

    return sum;
  }
}

class RiskAndControlMappingModel {
  @observable size;

  @observable contents;

  @observable totalPage;

  @observable totalContent;

  @observable page;

  @observable selectedFullpath;

  @observable selectedSubmitId;

  @observable selectedSubmitFullPath;

  @observable selectedStandardText; // control standard text

  @observable selectedRiskStandardFullpath;

  @observable selectedRiskStandardText;

  @action async getRiskAndControlListMapping(page, size, filterNo, isStandard) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentServices.getRiskAndControlListMapping(
        page,
        size,
        filterNo,
        '',
        isStandard,
      );

      this.size = result.data.size;
      this.contents = result.data.contents.map(
        (lv4) => new Lv4MappingModel(lv4),
      );
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.page = result.data.page;
      // this.selectedSubmitFullPath = '';
      // this.selectedSubmitId = '';
      // this.selectedStandardText = '';
      loadingStore.setIsLoading(false);
    } catch (error) {
      console.log(error);
      loadingStore.setIsLoading(false);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action setMapping(
    id,
    fullpath,
    standardText,
    riskStandardFullpath,
    riskStandardText,
  ) {
    if (this.selectedSubmitId === id) {
      this.selectedSubmitId = null;
      // this.selectedSubmitFullPath = '';
      this.selectedStandardText = null;
      this.selectedRiskStandardId = null;
      this.selectedRiskStandardFullpath = null;
      this.selectedRiskStandardText = null;
    } else {
      this.selectedSubmitId = id;
      this.selectedSubmitFullPath = fullpath;
      this.selectedStandardText = standardText;
      this.selectedRiskStandardFullpath = riskStandardFullpath;
      this.selectedRiskStandardText = riskStandardText;
    }
  }

  @computed get isSelectedMapping() {
    return this.selectedSubmitFullPath && this.selectedSubmitId;
  }

  @action removeMapping() {
    this.selectedSubmitId = null;
    // this.selectedSubmitFullPath = '';
    this.selectedStandardText = null;
  }
}

export default RiskAndControlMappingModel;
