import { observable, action, computed } from 'mobx';

export class ValidateControlFormModel {
  @observable isSetNameTh;

  @observable isSetRate;

  @observable isSetType;

  @observable isSetFormat;

  @observable isSetFrequency;

  @observable isTypeAutomateControl;

  @observable isSetEmployeeController;

  @observable isSetDocumentFile;

  @observable isSetDocument;

  @observable isSetInitialRemark;

  @observable isSetSuggestion;

  @observable isSetPartner;

  @observable isSetEndDate;

  @observable isSetBenefit;

  @observable isSetBenefitText;

  @observable isSetEnhance;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateNameTh() {
    return this.isSetNameTh === false;
  }

  @computed get validateRateId() {
    return this.isSetRate === false;
  }

  @computed get validateType() {
    return this.isSetType === false;
  }

  @computed get validateFormat() {
    return this.isSetFormat === false;
  }

  @computed get validateFrequency() {
    return this.isSetFrequency === false;
  }

  @computed get validateTechControl() {
    return this.isTypeAutomateControl === false;
  }

  @computed get validateEmployeeController() {
    return this.isSetEmployeeController === false;
  }

  @computed get validateDocumentFile() {
    return this.isSetDocumentFile === false;
  }

  @computed get validateDocument() {
    return this.isSetDocument === false;
  }

  @computed get validateInitialRemark() {
    return this.isSetInitialRemark === false;
  }

  @computed get validateSuggestion() {
    return this.isSetSuggestion === false;
  }

  @computed get validatePartner() {
    return this.isSetPartner === false;
  }

  @computed get validateEndDate() {
    return this.isSetEndDate === false;
  }

  @computed get validateBenefit() {
    return this.isSetBenefit === false;
  }

  @computed get validateBenefitText() {
    return this.isSetBenefitText === false;
  }
}

export class ValidateRiskFormModel {
  @observable isSetRiskNameTh;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateRiskName() {
    return this.isSetRiskNameTh === false;
  }
}

export class ValidateObjectFormModel {
  @observable isSetNameTh;

  @observable isSetType;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateNameTh() {
    return this.isSetNameTh === false;
  }

  @computed get validateType() {
    return this.isSetType === false;
  }
}

export class ValidateAssessmentDetailFormModel {
  @observable isSetWorkflowFiles;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateWorkflowFiles() {
    return this.isSetWorkflowFiles === false;
  }
}

export class ValidateCOSODetailFormModel {
  @observable isApproach;

  @observable isDeploy;

  @observable isNewInitiative;

  @observable isEvidence;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSetApproach() {
    return this.isApproach === false;
  }

  @computed get isSetEvidence() {
    return this.isEvidence === false;
  }

  @computed get isSetDeploy() {
    return this.isDeploy === false;
  }

  @computed get isSetNewInitiative() {
    return this.isNewInitiative === false;
  }
}

// export default ValidateFormModel;
