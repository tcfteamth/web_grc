import { observable, action } from 'mobx';

class SideButtonModel {
  @observable compliance = false;

  @observable iaFinding = false;

  @observable rcLibrary = false;

  constructor(data) {
    this.compliance = data.compliance;
    this.iaFinding = data.iaFinding;
    this.rcLibrary = data.rcLibrary;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default SideButtonModel;
