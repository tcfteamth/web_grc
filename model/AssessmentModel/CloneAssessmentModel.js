import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import SubmitCloneAssessmentModel from './SubmitCloneAssessmentModel';

class PrototypeItem {
  @observable id = null;

  @observable linkPage = '';

  @observable name = '';

  @observable no = '';

  @observable order = '';

  @observable position = '';

  @observable worker = '';

  @observable year = null;

  @observable submitCloneAssessmentModel = new SubmitCloneAssessmentModel();

  constructor(prototype) {
    this.id = prototype.id;
    this.linkPage = prototype.linkPage;
    this.name = prototype.name;
    this.no = prototype.no;
    this.order = prototype.order;
    this.position = prototype.position;
    this.worker = prototype.worker;
    this.year = prototype.year;
  }
}

class CloneAssessmentModel {
  @observable departmentId = null;

  @observable page = 1;

  @observable prototypeCopyList = [];

  @observable size = 10;

  @observable totalContent = 10;

  @observable totalPage = 1;

  @observable submit = {};

  @observable yearList = null;

  @observable divisionList = null;

  @observable selectedYear = {};

  @observable selectedDivision = {};

  @action async getCloneAssessmentList(
    token,
    departmentNo,
    page,
    year,
    divisionNo,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentServices.getCloneAssessmentList(
        token,
        departmentNo,
        page,
        year,
        divisionNo,
      );

      this.departmentId = result.data.departmentId;
      this.page = result.data.page;
      this.prototypeCopyList = result.data.prototypeCopyList.map(
        (prototype) => new PrototypeItem(prototype),
      );
      this.size = result.data.size;
      this.totalContent = result.data.totalContent;
      this.totalPage = result.data.totalPage;
      this.submit = {};
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getYearlist(token) {
    try {
      const result = await request.assessmentServices.getYearlist(token);

      this.yearList = result.data;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDivisionList() {
    try {
      const result = await request.roadmapServices.getDivisionById(
        this.departmentId,
      );

      this.divisionList = result.data;
      // return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getSelectedCloneAssessment(token) {
    try {
      const result = await request.assessmentServices.getSelectedCloneAssessment(
        token,
        this.submit,
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @computed get cloneAssessmentOptions() {
    const options =
      this.prototypeCopyList &&
      this.prototypeCopyList.map((prototype) => {
        return {
          value: {
            no: prototype.no,
            id: prototype.id,
          },
          label: `${prototype.no} ${prototype.name}`,
        };
      });

    return options;
  }

  @computed get yearOptions() {
    const options = this.yearList.map((year) => {
      return {
        value: year,
        label: year,
      };
    });

    return options;
  }

  @computed get divisionOptions() {
    const options = this.divisionList.map((division) => {
      return {
        value: division.no,
        label: division.nameEn,
      };
    });

    return options;
  }

  @action setSubmit(id, no) {
    const data = { id, no };

    this.submit = data;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  // @action cloneControl(controls) {
  //   const newControl = controls
  // }
}

export default CloneAssessmentModel;
