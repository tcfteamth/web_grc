import { observable, action } from 'mobx';

class AttachFileModel {
  @observable title = '';

  @observable files = null;

  @action setField(field, value) {
    this[field] = value;
  }

  @action resetAttachFileData() {
    this.title = '';
    this.files = null;
  }
}

export default AttachFileModel;
