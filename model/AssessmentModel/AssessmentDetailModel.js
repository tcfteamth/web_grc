import { observable, action, runInAction, computed, toJS, trace } from 'mobx';
import { isNumber } from 'lodash';
import storejs from 'store';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';
import ApproveAssessmentModel, {
  APPROVE_ASSESSMENT_READY,
  APPROVE_ASSESSMENT_STATUS,
} from './ApproveAssessmentModel';
import VPAssessmentModel from './VPAssessmentModel';
import AttachFileModel from './AttachFileModel';
import SideButtonModel from './SideButtonModel';
import {
  ValidateControlFormModel,
  ValidateRiskFormModel,
  ValidateObjectFormModel,
  ValidateAssessmentDetailFormModel,
} from './ValidateFormModel';
import { BUTTON_NAME } from '../../utils/assessment/bottombarButtonName';
import CloneAssessmentModel from './CloneAssessmentModel';
import NavigationModel from './NavigationModel';
import { config } from '../../utils';

// component ที่ใช้ model นี้เป็นหลัก
// CreateDetailControlAssessment
// AddDetailControlAssessment
// EditDetailCotrolAssessment

export const CONTROL_FORMAT_CONSTANT = {
  automate: 'Automate Control',
  itManual: 'IT Dependent Control',
  manual: 'Manual Control',
};

class ControlsModel {
  @observable fullPath = '';

  @observable nameTh = '';

  @observable no = '';

  @observable rateId = null;

  @observable document = null;

  @observable format = null; // รูปแบบของการควบคุม

  @observable nameEn = '';

  @observable id = null;

  @observable type = null; // ประเภทของการควบคุม

  @observable frequency = null; // ความถี่ของการควบคุม

  @observable isStandard = false;

  @observable filePath = [];

  @observable files = [];

  @observable titles = [];

  @observable techControl = ''; // ระบบเทคโนโลยีสารสนเทศที่เกี่ยวข้องกับการควบคุม

  @observable employeeController = ''; // ผู้ปฏิบัติการควบคุม

  @observable initialRemark = ''; // ข้อสังเกตเบื้องต้น

  @observable suggestion = ''; // ข้อเสนอแนะในการพัฒนา / แก้ไข

  @observable partnerId = null; // ผู้รับผิดชอบ

  @observable endDate = null; // กำหนดแล้วเสร็จ

  @observable benefit = '';

  @observable benefitText = '';

  @observable isEnhance = false;

  @observable partner = null;

  @observable showPartner = null;

  @observable attachFile = new AttachFileModel();

  @observable assessmentFileIds = [];

  @observable validateFormModel = new ValidateControlFormModel();

  @observable fileTitle = [];

  @observable isLocked;

  @observable followUpPath;

  @observable isDuplicate = false;

  @observable standardId;

  @observable standardText;

  constructor(control) {
    this.fullPath = control.fullPath;
    this.nameTh = control.nameTh;
    this.no = control.no;
    this.rateId = control.rate;
    this.document = control.document;
    this.format = control.format;
    this.nameEn = control.nameEn;
    this.id = control.id;
    this.type = control.type;
    this.frequency = control.frequency;
    this.isStandard = control.isStandard;
    this.filePath = control.fileTitle.files.map((file) => file) || [];
    this.isEnhance = control.isEnhance;
    this.techControl = control.techControl;
    this.employeeController = control.employeeController;
    this.initialRemark = control.initialRemark;
    this.suggestion = control.suggestion;
    this.partnerId = control.partnerId;
    this.endDate = control.endDate;
    this.benefit = control.benefit;
    this.benefitText = control.benefitText;
    this.showPartner = {
      label: control.partner ? control.partner.indicator : 'No Co-Responsible',
      value: control.partner ? control.partner.divisionNo : undefined,
    };
    this.partner = control.partner;
    this.titles = control.fileTitle.title || '';
    this.isLocked = control.isLocked;
    this.followUpPath = control.followUpPath;
    this.isDuplicate = control.isDuplicate || false;
    this.standardId = control.standardId;
    this.standardText = control.standardTxt;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action setRate(id) {
    if (this.rateId === id) {
      this.rateId = null;
    } else {
      this.rateId = id;
    }
  }

  @action resetEnhanceData() {
    this.initialRemark = '';
    this.suggestion = '';
    this.endDate = null;
    this.benefit = null;
    this.benefitText = null;
    this.showPartner = null;
    this.partner = null;
  }

  @action setRates(id) {
    if (id === this.rateId) {
      this.isEnhance = false;
      this.rateId = null;

      // reset ค่าใน enhance ใหม่ เมื่อมีการกด rate ออก
      this.resetEnhanceData();
    } else if (id !== 3) {
      this.isEnhance = false;
      this.rateId = id;

      // reset ค่าใน benefit ใหม่เมื่อ rate เป็น poor หรือ fair
      this.benefit = null;
      this.benefitText = null;
    } else {
      this.rateId = id;

      // reset ค่าใน enhance ใหม่เมื่อ rate เป็น good
      this.resetEnhanceData();
    }
  }

  @computed get isTypeAutomateControl() {
    const isAutomateControl =
      this.format === CONTROL_FORMAT_CONSTANT.automate ||
      this.format === CONTROL_FORMAT_CONSTANT.itManual;

    return isAutomateControl;
  }

  // logic เปิด tab assessment detail
  @computed get doNeedImprovement() {
    const need = this.rateId === 1 || this.rateId === 2 || this.isEnhance;

    return need;
  }

  // logic ในการกดปุ่ม พัฒนาให้ดียิ่งขึ้น
  @computed get canCheckImprovement() {
    const canCheck = this.rateId === 1 || this.rateId === 2 || !this.rateId;

    return canCheck;
  }

  @computed get convertTime() {
    let date;
    if (this.endDate) {
      date = new Date(this.endDate);
    } else {
      date = new Date().getTime;
    }

    return date;
  }

  // ใช้ !! เพื่อแปลงจาก string ให้เป็น boolean
  @computed get isSetNameTh() {
    return !!this.nameTh;
  }

  @computed get isSetRate() {
    return !!this.rateId;
  }

  @computed get isSetType() {
    return !!this.type;
  }

  @computed get isSetFormat() {
    return !!this.format;
  }

  @computed get isSetFrequency() {
    return !!this.frequency;
  }

  @computed get isSetTechControl() {
    return !!this.techControl;
  }

  @computed get isSetEmployeeController() {
    return !!this.employeeController;
  }

  // @computed get isSetDocumentFile() {
  //   return this.titles.length > 0 || this.filePath.length > 0;
  // }

  @computed get isSetDocument() {
    return this.titles.length > 0;
  }

  @computed get isSetInitialRemark() {
    return !!this.initialRemark;
  }

  @computed get isSetSuggestion() {
    return !!this.suggestion;
  }

  @computed get isSetPartnerId() {
    return !!this.partner;
  }

  @computed get isSetEndDate() {
    return !!this.endDate;
  }

  @computed get isSetBenefit() {
    return !!this.benefit;
  }

  @computed get isSetBenefitText() {
    return !!this.benefitText;
  }

  @computed get isSetEnhance() {
    // case ในการเลือกพัฒนาให้ดียิ่งขึ้น
    if (this.isEnhance) {
      return true;
    }

    // case ในการเลือก fair หรือ poor
    if (this.rateId !== 3) {
      return true;
    }

    return false;
  }

  @computed get doNeedBenefit() {
    if (this.isEnhance) {
      return true;
    }

    return false;
  }

  @action setValidateForm() {
    const validateFields = [
      'isSetNameTh',
      'isSetRate',
      'isSetType',
      'isSetFormat',
      'isSetFrequency',
      'isTypeAutomateControl',
      'isSetEmployeeController',
      'isSetDocumentFile',
      'isSetDocument',
      'isSetInitialRemark',
      'isSetSuggestion',
      'isSetPartner',
      'isSetEndDate',
      'isSetBenefit',
      'isSetBenefitText',
      'isSetEnhance',
    ];

    validateFields.map((field) =>
      this.validateFormModel.setField(field, this[field]),
    );

    if (this.isTypeAutomateControl) {
      this.validateFormModel.setField(
        'isTypeAutomateControl',
        !!this.techControl,
      );
    } else {
      this.validateFormModel.setField(
        'isTypeAutomateControl',
        this.techControl,
      );
    }
  }

  @action async uploadFiles(token, assessmentId, isDuplicate) {
    try {
      if (this.titles || this.files.length > 0) {
        let pathAll = [];

        if (this.filePath.length > 0) {
          for (const file of this.filePath) {
            pathAll.push(file.filePath);
          }
        }

        const formData = new FormData();
        formData.append('assessmentId', assessmentId);
        formData.append('fullPath', this.fullPath);
        formData.append('title', this.titles);

        if (isDuplicate === true) {
          formData.append('filePaths', pathAll);
        }

        for (const file of this.files) {
          formData.append('files', file);
        }

        const result = await request.assessmentServices.uploadAssessmentFiles(
          token,
          formData,
        );

        return result;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @observable newId = 0;

  @action onAddAttatchFiles(title, files) {
    let newName;
    const newIds = this.newId;
    const newFileId = `NewFile-${newIds}`;
    if (files) {
      newName = `${title}_USER_HAS_ATTACH_FILE`;
      this.titles.push(newName);
      this.files.push(files);

      this.filePath.push({
        title,
        file: files,
        filePath: files.preview.url,
        id: newFileId,
        index: newIds,
      });
    } else {
      this.titles.push(title);
      this.files.push(files);

      this.filePath.push({
        title,
        filePath: false,
        id: newFileId,
        index: newIds,
      });
    }
    this.newId += 1;
  }

  @action deleteAssessmentFiles(id, type) {
    if (type === 'display') {
      const filteredAssessmentFiles = this.filePath.filter(
        (file) => file.id !== id,
      );

      // ส่งไอดีของไฟล์ไปลบที่ service
      this.assessmentFileIds.push(id);

      this.filePath = filteredAssessmentFiles;
    } else {
      this.files = this.files.filter((file) => file.id !== id);
    }
  }

  @computed get displayFiles() {
    const files = [];

    this.filePath.map((file) => files.push({ ...file, filesType: 'display' }));
    this.files.map((file) => files.push(file));

    return files;
  }

  @action async submitDeleteAssessmentFiles(token) {
    try {
      const ids = this.assessmentFileIds.join(',');

      const result = await request.assessmentServices.deleteAssessmentFiles(
        token,
        ids,
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }
}

class RisksModel {
  @observable fullPath = '';

  @observable nameTh = '';

  @observable no = '';

  @observable controls = [];

  @observable nameEn = '';

  @observable id = null;

  @observable isStandard = false;

  @observable validateRiskFormModel = new ValidateRiskFormModel();

  @observable navId = '';

  @observable standardFullPath;

  @observable standardTxt;

  @observable complianceCore = '';

  @observable elegalLibrary = '';

  @observable externalLawSummary = '';

  @observable externalLaws = '';

  @observable internalLawSummary = '';

  @observable internalLaws = '';

  constructor(risk) {
    this.fullPath = risk.fullPath;
    this.nameTh = risk.nameTh;
    this.no = risk.no;
    this.controls = risk.controls.map((control) => new ControlsModel(control));
    this.nameEn = risk.nameEn;
    this.id = risk.id;
    this.isStandard = risk.isStandard;
    this.navId = risk.navId;
    this.standardFullPath = risk.standardFullPath;
    this.standardText = risk.standardTxt;
    this.complianceCore = risk.complianceCore;
    this.elegalLibrary = risk.elegalLibrary;
    this.externalLawSummary = risk.externalLawSummary;
    this.externalLaws = risk.externalLaws;
    this.internalLawSummary = risk.internalLawSummary;
    this.internalLaws = risk.internalLaws;
  }

  // AddDetailControlAssessment comp
  @action addControl() {
    const newFullPath = `${this.fullPath}.${
      parseInt(this.controls.length, 10) + 1
    }`;
    // const newNo = `${parseInt(this.controls.length, 10) + 1}`;
    const newNo = this.controls.length + 1;

    const newControlDetail = {
      fullPath: newFullPath,
      nameTh: '',
      no: newNo,
      rateId: null,
      document: null,
      format: null,
      nameEn: '',
      id: null,
      type: null,
      frequency: null,
      isEnhance: false,
      techControl: '',
      employeeController: '',
      initialRemark: '',
      suggestion: '',
      partnerId: null,
      endDate: null,
      files: [],
      filePath: [],
      fileTitle: { title: '', files: [] },
      standardId: null,
      standardText: '',
    };
    const newControl = new ControlsModel(newControlDetail);

    this.controls.push(newControl);
    console.log(newControl);
  }

  @action deleteControl(fullPath) {
    const filterControl = this.controls.filter(
      (control) => control.fullPath !== fullPath,
    );

    // reorder fullpath and no
    filterControl.forEach((control, index) => {
      const newFullPath = `${this.fullPath}.${index + 1}`;
      const newNo = `${index + 1}`;

      control.setField('fullPath', newFullPath);
      control.setField('no', newNo);
    });

    this.controls = filterControl;
  }

  @action setValidateRiskForm() {
    this.validateRiskFormModel.setField(
      'isSetRiskNameTh',
      this.isSetRiskNameTh,
    );

    this.controls.forEach((control) => control.setValidateForm());
  }

  @computed get isSetAllControlData() {
    const isSetName = this.controls.every((control) => control.isSetNameTh);
    const rated = this.controls.every((control) => control.rateId);
    const isSetType = this.controls.every((control) => control.isSetType);
    const isSetFormat = this.controls.every((control) => control.isSetFormat);
    const isSetFrequency = this.controls.every(
      (control) => control.isSetFrequency,
    );
    const isSetTechControl = this.controls.every(
      (control) => control.isSetTechControl,
    );
    const isSetEmployeeController = this.controls.every(
      (control) => control.isSetEmployeeController,
    );
    // const isSetDocumentFile = this.controls.every(
    //   (control) => control.isSetDocumentFile,
    // );
    const isSetDocument = this.controls.every(
      (control) => control.isSetDocument,
    );

    return (
      isSetName &&
      rated &&
      isSetType &&
      isSetFormat &&
      isSetFrequency &&
      isSetTechControl &&
      isSetEmployeeController &&
      isSetDocument &&
      this.doNeedEnhance
    );
  }

  @computed get doNeedEnhance() {
    const isSetEnhance = this.controls.every((control) => control.isSetEnhance);

    if (isSetEnhance) {
      const isSetInitialRemark = this.controls.every(
        (control) => control.isSetInitialRemark,
      );
      const isSetSuggestion = this.controls.every(
        (control) => control.isSetSuggestion,
      );
      // const isSetPartnerId = this.controls.every(
      //   (control) => control.isSetPartnerId,
      // );
      const isSetEndDate = this.controls.every(
        (control) => control.isSetEndDate,
      );

      return (
        isSetInitialRemark &&
        isSetSuggestion &&
        isSetEndDate &&
        this.doNeedBenefit
      );
    }

    return true;
  }

  @computed get doNeedBenefit() {
    const needBenefit = this.controls.every((control) => control.doNeedBenefit);

    if (needBenefit) {
      const isSetBenefit = this.controls.every(
        (control) => control.isSetBenefit,
      );

      const isSetBenefitText = this.controls.every(
        (control) => control.isSetBenefitText,
      );

      return isSetBenefit && isSetBenefitText;
    }

    return true;
  }

  @computed get isAllControlSetName() {
    const isSet = this.controls.every((control) => control.isSetNameTh);

    return isSet;
  }

  @computed get isSetRiskNameTh() {
    return !!this.nameTh;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get getFiles() {
    let a = [];

    this.controls.forEach((control) => {
      a.push({ fullPath: control.fullPath, files: control.files });
    });

    return a;
  }

  @computed get isAllControlSetStandard() {
    const isAllSet = this.controls.every((control) => control.standardId);

    return isAllSet;
  }
}

class ObjectsModel {
  @observable nameTh = '';

  @observable no = '';

  @observable risks = [];

  @observable nameEn = '';

  @observable id = null;

  @observable type = null;

  @observable isStandard = false;

  @observable isComment = '';

  @observable validateObjectFormModel = new ValidateObjectFormModel();

  @observable navId = '';

  constructor(objectItem) {
    this.nameTh = objectItem.nameTh;
    this.no = objectItem.no;
    this.risks = objectItem.risks.map((risk) => new RisksModel(risk));
    this.nameEn = objectItem.nameEn;
    this.id = objectItem.id;
    this.type = objectItem.type;
    this.isStandard = objectItem.isStandard;
    this.isComment = objectItem.isComment;
    this.navId = objectItem.navId;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSetAllRiskData() {
    const isAllSet = this.risks.every((risk) => risk.isSetAllControlData);
    const isSetRiskName = this.risks.every((risk) => risk.isSetRiskNameTh);

    return isAllSet && isSetRiskName;
  }

  @action addRisk() {
    const newRiskFullPath = `${this.no}.${this.risks.length + 1}`;
    const newNo = `${this.risks.length + 1}`;

    const newControlFullPath = `${newRiskFullPath}.1`;
    const newControlNo = '1';

    const newRiskObject = {
      fullPath: newRiskFullPath,
      nameTh: '',
      no: newNo,
      isStandard: false,
      nameEn: '',
      id: null,
      controls: [
        {
          fullPath: newControlFullPath,
          nameTh: '',
          no: newControlNo,
          rate: null,
          document: null,
          format: null,
          nameEn: '',
          id: null,
          type: null,
          frequency: null,
          isEnhance: false,
          techControl: '',
          employeeController: '',
          initialRemark: '',
          suggestion: '',
          partnerId: null,
          endDate: null,
          files: [],
          filePath: [],
          fileTitle: { title: '', files: [] },
          standardId: null,
          standardText: '',
        },
      ],
    };

    console.log(newRiskObject);

    const newRisk = new RisksModel(newRiskObject);

    this.risks.push(newRisk);
  }

  @action deleteRisk(fullPath) {
    const filteredRisk = this.risks.filter(
      (risk) => risk.fullPath !== fullPath,
    );

    this.risks = filteredRisk;
  }

  @computed get isAllControlInRiskSetName() {
    const getIsAllControlSetName = this.risks.map(
      (risk) => risk.isAllControlSetName,
    );
    const isSet = getIsAllControlSetName.every((control) => control);

    return isSet;
  }

  @computed get isAllControlInRiskSetType() {
    const getIsAllControlSetName = this.risks.map(
      (risk) => risk.isAllControlSetType,
    );
    const isSet = getIsAllControlSetName.every((control) => control);

    return isSet;
  }

  @computed get isAllRiskInObjectSetName() {
    const isSet = this.risks.every((risk) => risk.isSetRiskNameTh);

    return isSet;
  }

  @computed get isSetNameTh() {
    return !!this.nameTh;
  }

  @computed get isSetType() {
    return !!this.type;
  }

  @action setValidateObjectForm() {
    this.validateObjectFormModel.setField('isSetNameTh', this.isSetNameTh);
    this.validateObjectFormModel.setField('isSetType', this.isSetType);
    this.risks.forEach((risk) => risk.setValidateRiskForm());
  }
}

class AssessmentDetailModel {
  @observable button = [];

  @observable buttonBar = [];

  @observable no = '';

  @observable createBy = '';

  @observable canUpdate = false;

  @observable objects = [];

  @observable name = '';

  @observable id = null;

  @observable status = '';

  @observable verifyCompliance = '';

  @observable createDate = null;

  @observable isReadyToSubmit = false;

  @observable divEn = '';

  @observable shiftEn = '';

  @observable commentLv4 = '';

  @observable approveStatus = new ApproveAssessmentModel();

  @observable vpAcceptStatus = new VPAssessmentModel();

  @observable isStandard = false;

  @observable newNameThObject = '';

  @observable newObjectTypes = '';

  @observable info = null;

  @observable lastYearPath = '';

  @observable updateDate = null;

  @observable roadMapText = '';

  @observable roadMapImage = '';

  @observable assessmentComment = '';

  @observable commentList = [];

  @observable workflow = [];

  @observable workflowFiles = [];

  @observable deleteWorkflowFiles = [];

  @observable deleteAssessmentFiles = [];

  @observable tab = 'TODO'; // TODO/SUMMARY

  @observable
  validateAssessmentDetailModel = new ValidateAssessmentDetailFormModel();

  @observable departmentNo = '';

  @observable divisionNo = '';

  @observable yearlist = null;

  @observable cloneAssessmentModel = new CloneAssessmentModel();

  @observable navigationModel = [];

  @observable roadmapType = '';

  @observable lastYearPath = '';

  @observable hasLastYear = '';

  @observable sideButton = [];

  @observable standardFullPath;

  @action async getSelectedCloneAssessment(token) {
    try {
      loadingStore.setIsLoading(true);

      const result = await this.cloneAssessmentModel.getSelectedCloneAssessment(
        token,
      );

      if (result.data.objects.length > 0) {
        result.data.objects.map((object) => {
          const newObjectNo = `${this.no}.${this.objects.length + 1}`;

          const newObjectData = {
            nameTh: object.nameTh,
            no: newObjectNo,
            isStandard: object.isStandard,
            nameEn: object.nameEn,
            id: null,
            type: object.type,
            risks: object.risks.map((risk, riskIndex) => {
              const newRiskFullpath = `${newObjectNo}.${riskIndex + 1}`;

              return {
                fullPath: newRiskFullpath,
                nameTh: risk.nameTh,
                nameEn: risk.nameEn,
                isStandard: risk.isStandard,
                id: null,
                no: `${riskIndex + 1}`,
                controls: risk.controls.map((control, controlIndex) => {
                  const newControlFullpath = `${newRiskFullpath}.${
                    controlIndex + 1
                  }`;

                  console.log('nameTh', control.nameTh);
                  console.log('employeeController', control.employeeController);

                  return {
                    fullPath: newControlFullpath,
                    nameTh: control.nameTh,
                    no: `${controlIndex + 1}`,
                    rate: null,
                    document: control.document,
                    format: control.format,
                    nameEn: control.nameEn,
                    id: null,
                    type: control.type,
                    frequency: control.frequency,
                    isEnhance: control.isEnhance,
                    techControl: control.techControl,
                    employeeController: control.employeeController,
                    initialRemark: control.initialRemark,
                    suggestion: control.suggestion,
                    partnerId: control.partnerId,
                    endDate: control.endDate,
                    files: control.files,
                    filePath: control.filePath,
                    fileTitle: control.fileTitle,
                    isDuplicate: true,
                  };
                }),
              };
            }),
          };

          console.log('newObjectData', newObjectData);
          this.objects.push(new ObjectsModel(newObjectData));
        });
      }

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  }

  @action async getAssessmentComment(token, assesmentId) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentServices.getAssessmentComment(
        token,
        assesmentId,
      );
      console.log('result', result.data);
      // return result;

      this.commentList = result.data;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getAssessmentDetail(token, assesmentId, tab = 'TODO',mode = 'edit') {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentServices.getAssessmentDetail(
        token,
        assesmentId,
        tab,mode
      );

      this.no = result.data.no;
      this.createBy = result.data.createBy;
      this.name = result.data.name;
      this.info = result.data.info;
      this.id = result.data.id;
      this.status = result.data.status;
      this.createDate = result.data.createDate;
      this.commentLv4 = result.data.commentLv4;
      this.isStandard = result.data.isStandard;
      this.isReadyToSubmit = result.data.isReadyToSubmit;
      this.verifyCompliance = result.data.verifyCompliance || false ;
      this.canUpdate = result.data.canUpdate;
      this.updateDate = result.data.updateDate;
      this.roadMapText = result.data.roadMapText;
      this.roadMapImage = result.data.roadMapImage;
      this.assessmentComment = result.data.assessmentComment;
      this.buttonBar = result.data.buttonBar;
      this.workflow = result.data.workflow;
      this.departmentNo = result.data.departmentNo;
      this.roadmapType = result.data.roadMapType;
      this.lastYearPath = result.data.lastYearPath;
      this.hasLastYear = result.data.hasLastYear;
      this.divisionNo = result.data.divisionNo;
      this.sideButton = new SideButtonModel(result.data.sideButton);
      this.standardFullPath = result.data.standardFullPath;
      this.divEn = result.data.divEn;
      this.shiftEn = result.data.shiftEn;

      storejs.set('iafinding', {
        indicator: result.data.info.indicator,
        assessmentNo: result.data.no,
        assessmentName: result.data.name,
      });

      this.approveStatus.status =
        result.data.status === 'Minor Change'
          ? 'MINOR_CHANGE'
          : result.data.status === 'Major Change'
          ? 'MAJOR_CHANGE'
          : result.data.status === 'Completed'
          ? APPROVE_ASSESSMENT_READY.accept
          : null;

      if (result.data.objects.length !== 0) {
        this.objects = result.data.objects.map(
          (objectItem) => new ObjectsModel(objectItem),
        );
      } else if (this.tab === 'TODO') {
        const newObjectNo = `${this.no}.1`;
        const newRiskFullpath = `${newObjectNo}.1`;
        const newControlFullpath = `${newRiskFullpath}.1`;

        const newObjectData = {
          nameTh: '',
          no: newObjectNo,
          isStandard: false,
          nameEn: '',
          id: null,
          type: null,
          risks: [
            {
              fullPath: newRiskFullpath,
              nameTh: '',
              no: '1',
              isStandard: false,
              nameEn: '',
              id: null,
              controls: [
                {
                  fullPath: newControlFullpath,
                  nameTh: '',
                  no: '1',
                  rate: null,
                  document: null,
                  format: null,
                  nameEn: '',
                  id: null,
                  type: null,
                  frequency: null,
                  isEnhance: false,
                  techControl: '',
                  employeeController: '',
                  initialRemark: '',
                  suggestion: '',
                  partnerId: null,
                  endDate: null,
                  files: [],
                  filePath: [],
                  fileTitle: { title: '', files: [] },
                },
              ],
            },
          ],
        };

        const newObject = new ObjectsModel(newObjectData);

        this.objects.push(newObject);
      }

      this.navigationModel = result.data.navigation.map(
        (nav) => new NavigationModel(nav),
      );
      loadingStore.setIsLoading(false);
    } catch (e) {
      loadingStore.setIsLoading(false);

      if (e.response.status === 401) {
        window.location.replace(config.baseUrlLogin);
      }
    }
  }

  @action async updateAssessmentDetail(token) {
    try {
      loadingStore.setIsLoading(true);
      const assessmentData = {
        objects: this.objects,
        id: this.id,
        no: this.no,
        readyTosubmit: this.isReadyToSubmit,
        verifyCompliance: this.verifyCompliance,
        status: this.approveStatus.isSetMajorOrMinorReady,
        comment: this.assessmentComment,
        standardFullPath: this.standardFullPath,
      };

      console.log('updateAssessmentDetail', assessmentData);

      const result = await request.assessmentServices.updateAssessment(
        token,
        assessmentData,
      );
      await this.uploadFiles(token);
      await this.uploadWorkflowFiles(token);
      await this.submitDeleteWorkflowFiles(token);
      await this.deleteAssessmentControlFiles(token);

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async setApproveStatus(token) {
    try {
      loadingStore.setIsLoading(true);

      const objectDetail = [
        {
          status: this.approveStatus.status,
          id: this.id,
          rejectComment: this.approveStatus.rejectComment,
        },
      ];

      const result = await request.roadmapServices.acceptRoadmaps(
        token,
        objectDetail,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitVPAcceptStatus(token) {
    try {
      loadingStore.setIsLoading(true);

      const objectDetail = [
        {
          status: this.vpAcceptStatus.status,
          id: this.id,
          rejectComment: this.vpAcceptStatus.rejectComment,
        },
      ];

      const result = await request.roadmapServices.acceptRoadmaps(
        token,
        objectDetail,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async rejectToStaff(token) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.assessmentServices.rejectToStaff(
        token,
        this.id,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  @action async deleteComment(token, ids) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.assessmentServices.deleteComment(token, ids);

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  @action async uploadFiles(token) {
    try {
      for (const objItem of this.objects) {
        for (const risk of objItem.risks) {
          for (const control of risk.controls) {
            await control.uploadFiles(token, this.id, control.isDuplicate);
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  @action async deleteAssessmentControlFiles(token) {
    try {
      for (const objItem of this.objects) {
        for (const risk of objItem.risks) {
          for (const control of risk.controls) {
            if (control.assessmentFileIds.length > 0) {
              await control.submitDeleteAssessmentFiles(token);
            }
          }
        }
      }

      console.log('deleteAssessmentFiles');
    } catch (e) {
      console.log(e);
    }
  }

  @action async getCloneAssessmentList(token) {
    try {
      loadingStore.setIsLoading(true);
      const result = await this.cloneAssessmentModel.getCloneAssessmentList(
        token,
        this.departmentNo,
      );

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  }

  @action async exportAssessmentDetail() {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.assessmentServices.exportAssessmentDetail(
        this.id,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @computed get isSetWorkflowFiles() {
    return this.workflowList.length !== 0;
  }

  @action setValidateAssessmentForm() {
    this.validateAssessmentDetailModel.setField(
      'isSetWorkflowFiles',
      this.isSetWorkflowFiles,
    );
    this.objects.forEach((objectItem) => objectItem.setValidateObjectForm());
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSetAllObjectData() {
    const isSetAllRiskData = this.objects.every(
      (objectItem) => objectItem.isSetAllRiskData,
    );
    const isSetObjectName = this.objects.every(
      (objectItem) => objectItem.nameTh,
    );
    const isSetObjectType = this.objects.every((objectItem) => objectItem.type);

    const isSetWorkflowFiles = this.workflowList.length !== 0;

    return (
      isSetAllRiskData &&
      isSetObjectName &&
      isSetObjectType &&
      isSetWorkflowFiles
    );
  }

  @computed get canCheckReadyToSubmit() {
    const can = this.isSetAllObjectData;

    return can;
  }

  // ไม่ใช้ action หรือ computed เพราะต้องการให้ set ค่าได้และ return ได้ในเวลาเดียวกัน*
  isSetAllForm() {
    if (!this.isSetAllObjectData) {
      this.isReadyToSubmit = false;
    }

    return this.isReadyToSubmit;
  }

  @action addObject() {
    const getLastNo =
      this.objects.length !== 0
        ? this.objects[this.objects.length - 1].no
        : this.no; // 1.1.2.3
    const splitNo = getLastNo.split('.'); // [1,1,2,3]
    const addNo = parseInt(splitNo[splitNo.length - 1], 10) + 1; // 3 + 1 = 4
    const joinNewNo = splitNo.slice(0, splitNo.length - 1).join('.'); // [1,1,2,4]
    const newNoObject = `${joinNewNo}.${addNo}`; // 1.1.2.4

    const newRiskFullPath = `${newNoObject}.1`;
    const newRiskNo = '1';

    const newControlFullPath = `${newRiskFullPath}.1`;
    const newControlNo = '1';

    const newObjectData = {
      nameTh: this.newNameThObject,
      no: newNoObject,
      isStandard: false,
      nameEn: '',
      id: null,
      type: this.newObjectTypes,
      risks: [
        {
          fullPath: newRiskFullPath,
          nameTh: '',
          no: newRiskNo,
          isStandard: false,
          nameEn: '',
          id: null,
          controls: [
            {
              fullPath: newControlFullPath,
              nameTh: '',
              no: newControlNo,
              rate: null,
              document: null,
              format: null,
              nameEn: '',
              id: null,
              type: null,
              frequency: null,
              isEnhance: false,
              techControl: '',
              employeeController: '',
              initialRemark: '',
              suggestion: '',
              partnerId: null,
              endDate: null,
              files: [],
              filePath: [],
              fileTitle: { title: '', files: [] },
              standardId: null,
              standardText: '',
            },
          ],
        },
      ],
    };

    const newObject = new ObjectsModel(newObjectData);

    this.objects.push(newObject);

    console.log('newObjectData', newObjectData);
  }

  @action deleteObject(no) {
    const filteredObject = this.objects.filter(
      (objectItem) => objectItem.no !== no,
    );

    this.objects = filteredObject;
  }

  @action setNewNameThObject(value) {
    this.newNameThObject = value;
  }

  @action setNewObjectTypes(value) {
    this.newObjectTypes = value;
  }

  @action async submitDeleteWorkflowFiles(token) {
    try {
      if (this.deleteWorkflowFiles.length > 0) {
        const ids = this.deleteWorkflowFiles.join(',');

        console.log('submitDeleteWorkflowFiles', ids);

        const result = await request.assessmentServices.deleteWorkflowFiles(
          token,
          ids,
        );

        return result;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @computed get canSave() {
    const getIsAllControlInRiskSetName = this.objects.every(
      (objectItem) => objectItem.isAllControlInRiskSetName,
    );
    const getIsAllRiskInObjectSetName = this.objects.every(
      (objectItem) => objectItem.isAllRiskInObjectSetName,
    );

    const can = getIsAllControlInRiskSetName && getIsAllRiskInObjectSetName;

    console.log(can);
    return can;
  }

  @computed get isSendToStaffEnabled() {
    const buttonStatus = this.button.filter(
      (btn) => btn.buttonName === BUTTON_NAME.sendToStaff,
    );

    return buttonStatus.enabled;
  }

  // workflow file display
  @computed get workflowList() {
    const workflowList = [];
    this.workflow.map((workflowFile) =>
      workflowList.push({ ...workflowFile, filesType: 'display' }),
    );
    this.workflowFiles.map((workflowFile) => {
      workflowList.push(workflowFile);
    });

    return workflowList;
  }

  @action deleteWorkflowUploadFiles(id, type) {
    console.log('id', id);
    if (type === 'display') {
      this.workflow = this.workflow.filter((files) => files.id !== id);

      this.deleteWorkflowFiles.push(id);
    }

    if (!type) {
      this.workflowFiles = this.workflowFiles.filter(
        (files) => files.id !== id,
      );
    }
  }

  @action async uploadWorkflowFiles(token) {
    try {
      if (this.workflowFiles.length > 0) {
        const formData = new FormData();
        formData.append('assessmentId', this.id);

        for (const file of this.workflowFiles) {
          formData.append('files', file);
        }

        const result = await request.assessmentServices.uploadWorkflowFile(
          token,
          formData,
        );

        return result;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isOneOfAssessmentLockedByFollowup() {
    if (this.objects.length > 0) {
      return this.objects.some((obj) =>
        obj.risks.some((risk) =>
          risk.controls.some((control) => control.isLocked === true),
        ),
      );
    }

    return false;
  }

  @computed get isAllControlSetStandard() {
    const isSet = this.objects.some((obj) =>
      obj.risks.some((risk) =>
        risk.controls.some((control) => control.standardId),
      ),
    );

    if (isSet) {
      return this.standardFullPath;
    }

    return '';
  }
}

export default AssessmentDetailModel;
