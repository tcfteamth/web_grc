import { observable, action, computed } from 'mobx';

class SubmitCloneAssessmentModel {
  @observable id = null;

  @observable no = '';

  @action setSubmit(id, no) {
    if (this.id === null) {
      this.id = id;
      this.no = no;
    } else {
      this.id = null;
      this.no = '';
    }
    // this[field] = value;
  }

  @computed get isSelected() {
    return this.id !== null && !this.no !== '';
  }
}

export default SubmitCloneAssessmentModel;
