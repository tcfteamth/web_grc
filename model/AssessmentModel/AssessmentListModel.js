import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import SubmitRoadmapsModel from '../SubmitRoadmapsModel';
import request from '../../services';
import AcceptAssessmentModel, {
  ACCEPT_ASSESSMENT_STATUS,
} from './AcceptAssessmentModel';
import { ACCEPT_ROADMAP_STATUS } from '../AcceptRoadmapModel';
import { ROLES } from '../../contexts/AuthContext';
import FilterModel from '../FilterModel';
import { config } from '../../utils';
import { yearListDropdownStore } from '../DropdownModel';

class AssessmentChildrenModel {
  @observable owner = '';

  @observable no = '';

  @observable lvl = null;

  @observable lastUpdate = null;

  @observable name = '';

  @observable rejectComment = '';

  @observable id = null;

  @observable status = '';

  @observable remark = '';

  @observable roadMapImage = null;

  @observable depDiv = null;

  @observable divEn = null;

  @observable shiftEn = '';

  @observable submitAssessment = new SubmitRoadmapsModel();

  @observable acceptAssessment = new AcceptAssessmentModel();

  @observable readyToSubmit = null;

  @observable overAllRating = '';

  @observable controlByRating = null;

  @observable roadMapType = '';

  @observable enhance = '';

  @observable verifyCompliance = null;

  @observable objectives = null;

  @observable latestYear = null;
  

  constructor(child) {
    this.owner = child.owner;
    this.no = child.no;
    this.lvl = child.lvl;
    this.lastUpdate = child.lastUpdate;
    this.name = child.name;
    this.rejectComment = child.rejectComment;
    this.id = child.id;
    this.status = child.status;
    this.readyToSubmit = child.readyTosubmit;
    this.overAllRating = child.overAllRating;
    this.controlByRating = child.controlByRating;
    this.remark = child.remark;
    this.roadMapImage = child.roadMapImage;
    this.depDiv = child.depDiv;
    this.divEn = child.divEn;
    this.shiftEn = child.shiftEn;
    this.roadMapType = child.roadMapType;
    this.enhance = child.enhance;
    this.verifyCompliance = child.verifyCompliance;
    this.objectives = child.objectives;
    this.latestYear = child.latestYear;
  }

  canCheck(role) {
    if (role == ROLES.staff) {
      return this.status === ACCEPT_ROADMAP_STATUS.accept && this.readyToSubmit;
    } else if (role === ROLES.dm) {
      return (
        this.status === ACCEPT_ROADMAP_STATUS.staffReady && this.readyToSubmit
      );
    } else if (role === ROLES.vp) {
      return (
        this.status === ACCEPT_ASSESSMENT_STATUS.waitVpAccept &&
        this.readyToSubmit
      );
    }
  }

  @action onSelectedSubmitAssessment() {
    if (this.readyToSubmit) {
      this.submitAssessment.setRoadmapIds(this.id);
    }
  }

  @action onSelectedAcceptAssessment(role, status, forceCheck) {
    // if (this.canCheck(role)) {
    this.acceptAssessment.setNo(status, this.id, forceCheck);
    // }
  }

  @computed get rateStatus() {
    const status = this.overAllRating.split('-');

    return status;
  }
}

class AssessmentModel {
  @observable no = '';

  @observable lvl = null;

  @observable children = [];

  @observable name = '';

  constructor(assessment) {
    this.no = assessment.no;
    this.lvl = assessment.lvl;
    this.children = assessment.children.map(
      (child) => new AssessmentChildrenModel(child),
    );
    this.name = assessment.name;
  }

  @action setRejectAllLevel4() {
    if (!this.isAllLevel4Reject) {
      this.children.forEach((child) => {
        if (child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.reject) {
          child.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.reject,
            child.id,
          );
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptAssessment.setNo('', '');
      });
    }
  }

  @action setAcceptAllLevel4() {
    if (!this.isAllLevel4Accept) {
      this.children.forEach((child) => {
        if (child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.accept) {
          child.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.accept,
            child.id,
          );
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptAssessment.setNo('', '');
      });
    }
  }

  @computed get isAllLevel4Accept() {
    const isChecked = this.children.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.accept,
    );

    return isChecked;
  }

  @computed get isAllLevel4Reject() {
    const isChecked = this.children.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.reject,
    );

    return isChecked;
  }

  @computed get canAllLevel4Check() {
    const check = this.children.map((child) => child.canCheck).some((i) => i);

    return check;
  }

  @computed get getRoadmapIds() {
    const roadmapIdsData = [];
    this.children.forEach((child) => {
      if (child.submitAssessment.roadmapIds) {
        roadmapIdsData.push(child.submitAssessment.roadmapIds);
      }
    });

    return roadmapIdsData;
  }

  @computed get getAssessmentApprovement() {
    const approvementList = [];
    this.children.forEach((child) => {
      if (child.acceptAssessment.status) {
        approvementList.push(child.acceptAssessment);
      }
    });

    return approvementList;
  }

  canCheckAll(role) {
    return this.children.some((child) => child.canCheck(role));
  }

  isSubmitAssessmentCheckedAll(role) {
    let allCheckedCount = 0,
      allCanCheckCount = 0;
    this.children.forEach((child) => {
      // if (child.canCheck(role)) {
      if (child.readyToSubmit) {
        allCanCheckCount += 1;
        if (child.submitAssessment.isSelected) {
          allCheckedCount += 1;
        }
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllSubmitAssessment(role, forceCheck) {
    this.children.forEach((child) => {
      child.onSelectedSubmitAssessment(role, forceCheck);
    });
  }

  isAcceptAssessmentCheckedAll(role, status) {
    let allCheckedCount = 0,
      allCanCheckCount = 0;
    this.children.forEach((child) => {
      allCanCheckCount += 1;
      if (
        status === ACCEPT_ASSESSMENT_STATUS.accept &&
        child.acceptAssessment.isSelectedAccept()
      ) {
        allCheckedCount += 1;
      } else if (
        status === ACCEPT_ASSESSMENT_STATUS.reject &&
        child.acceptAssessment.isSelectedReject()
      ) {
        allCheckedCount += 1;
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllAcceptAssessment(role, status, forceCheck) {
    this.children.forEach((child) => {
      child.onSelectedAcceptAssessment(role, status, forceCheck);
    });
  }

  @action setCheckAllAssessment2() {
    if (!this.isAllAssessmentChecked) {
      this.children.forEach((child) => {
        if (!child.submitAssessment.roadmapIds) {
          child.submitAssessment.setRoadmapIds(child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.submitAssessment.setRoadmapIds(null);
      });
    }
  }

  @computed get isAllAssessmentChecked() {
    const isChecked = this.children.every(
      (child) => child.submitAssessment.roadmapIds,
    );

    return isChecked;
  }

  @action setCheckAllReadyToSubmit() {
    if (!this.isCheckAllReadyToSubmit) {
      this.children.forEach((child) => {
        if (!child.submitAssessment.roadmapIds && child.readyToSubmit) {
          child.submitAssessment.setRoadmapIds(child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.submitAssessment.setRoadmapIds(null);
      });
    }
  }

  @computed get isCheckAllReadyToSubmit() {
    const filteredChildren = this.children.filter(
      (child) => child.readyToSubmit,
    );

    if (filteredChildren.length === 0) {
      return false;
    }
    return filteredChildren.every((i) => i.submitAssessment.isSelected);
  }

  @computed get isOneOfAssessmentCanCheck() {
    const can = this.children.some((child) => child.readyToSubmit);

    return can;
  }
}

class AssessmentListModel {
  @observable assessmentList = [];

  @observable size = 10;

  @observable totalPage = 1;

  @observable totalContent = 1;

  @observable page = 1;

  @observable searchAssessmentText = '';

  @observable tab = 'TODO'; // TODO | SUMMARY

  @observable filterModel = new FilterModel();

  @observable departmentId = null;

  @action async getAssessmentList(
    token,
    page = 1,
    searchValue = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    roadmapType = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    overallRatingId = '',
    year = '',
    processAnd = [],
    processOr = [],
    objectives = []
  ) {
    try {
      loadingStore.setIsLoading(true);

      const yearResult = await yearListDropdownStore.getYearListFillter(token);

      const defaultYear = year || yearResult[1].value;

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.assessmentServices.getAssessmentList(
        token,
        page,
        searchValue,
        lvl3,
        lvl4,
        bu,
        roadmapType,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        overallRatingId,
        defaultYear,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.assessmentList = result.data.assessments.map(
        (assessment) => new AssessmentModel(assessment),
      );

      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.page = result.data.page;
      this.departmentId = result.data.depaertmentId;

      loadingStore.setIsLoading(false);
    } catch (e) {
      loadingStore.setIsLoading(false);

      if (e.response.status === 401) {
        window.location.replace(config.baseUrlLogin);
      }
    }
  }

  @computed get isSelectedDepartmentId() {
    if (this.departmentId || this.filterModel.selectedDepartment) {
      return false;
    }

    return true;
  }

  @action async getAssessmentApprovementWithSearch(token) {
    try {
      const result = await this.getAssessmentList(
        token,
        1,
        this.searchAssessmentText,
        this.filterModel.selectedLvl3,
        this.filterModel.selectedLvl4,
        this.filterModel.selectedBu,
        this.filterModel.selectedRoadmapType,
        this.filterModel.selectedDepartment.no,
        this.filterModel.selectedDivision,
        this.filterModel.selectedShift,
        this.filterModel.selectedStatus,
        this.filterModel.selectedRate,
        this.filterModel.selectedYear,
      );
    } catch (e) {
      console.log(e);
    }
  }

  @computed get acceptRoadmapList() {
    return this.assessmentList.reduce(
      (acceptList, roadmap) => [...acceptList, ...roadmap.getRoadmapIds],
      [],
    );
  }

  // Staff ส่งให้ DM
  // DM ส่งให้ VP ใช้ service เดียวกัน
  @action async sendAssessmentToDmOrVp(token) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.roadmapServices.submitRoadmaps(
        token,
        this.acceptRoadmapList,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  @action async submitAssessmentToAdmin(token) {
    try {
      loadingStore.setIsLoading(true);
      const approvementList = this.assessmentList.reduce(
        (acceptList, roadmap) => [
          ...acceptList,
          ...roadmap.getAssessmentApprovement,
        ],
        [],
      );
      const result = await request.roadmapServices.acceptRoadmaps(
        token,
        approvementList,
      );
      // console.log('approvementList', approvementList);
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setCheckAcceptAll() {
    if (!this.isAllAcceptChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (
            child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.accept
          ) {
            child.acceptAssessment.setNo(
              ACCEPT_ASSESSMENT_STATUS.accept,
              child.no,
            );
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.acceptAssessment.setNo('', '');
        });
      });
    }
  }

  @action setCheckRejecetAll() {
    if (!this.isAllRejectChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (
            child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.reject
          ) {
            child.acceptAssessment.setNo(
              ACCEPT_ASSESSMENT_STATUS.reject,
              child.no,
            );
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.acceptAssessment.setNo('', '');
        });
      });
    }
  }

  @computed get isOneOfAssessmentReadyToSubmit() {
    const isReady = this.assessmentList.every(
      (assessment) => assessment.isAllLevel3ReadyToSubmit,
    );

    return isReady;
  }

  @computed get isOneOfAssessmentChecked() {
    const isChecked = this.assessmentList.every((assessment) =>
      assessment.children.every((child) => !child.submitAssessment.roadmapIds),
    );

    return isChecked;
  }

  @computed get isAllRejectChecked() {
    const isChecked = this.assessmentList.every(
      (assessment) => assessment.isAllLevel4Reject,
    );

    return isChecked;
  }

  @computed get isAllAcceptChecked() {
    const isChecked = this.assessmentList.every(
      (assessment) => assessment.isAllLevel4Accept,
    );

    return isChecked;
  }

  isSubmitAssessmentCheckedAll(role) {
    let allCheckedCount = 0;
    let allCanCheckCount = 0;

    this.assessmentList.forEach((assessment) => {
      if (assessment.isOneOfAssessmentCanCheck) {
        allCanCheckCount += 1;
        if (assessment.isSubmitAssessmentCheckedAll(role)) {
          allCheckedCount += 1;
        }
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @computed get isCheckedAll() {
    const checked = this.assessmentList.map((assessment) =>
      assessment.children.filter((child) => child.readyToSubmit),
    );

    const checkedFalt = checked.flat();

    const isAll = checkedFalt.every((i) => i.submitAssessment.isSelected);

    return isAll;
  }

  @computed get isCheckedForNext() {
    const checked = this.assessmentList.map((assessment) =>
      assessment.children.filter((child) => child.readyToSubmit),
    );

    const checkedFalt = checked.flat();
    const isAll = checkedFalt.some((i) => i.submitAssessment.isSelected);

    return isAll;
  }

  @action setCheckAllSubmitAssessment() {
    if (!this.isCheckedAll) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (!child.submitAssessment.roadmapIds && child.readyToSubmit) {
            child.submitAssessment.setRoadmapIds(child.id);
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.submitAssessment.setRoadmapIds(null);
        });
      });
    }
  }

  isAcceptAssessmentCheckedAll(role, status) {
    let allCheckedCount = 0;
    let allCanCheckCount = 0;

    this.assessmentList.forEach((assessment) => {
      allCanCheckCount += 1;
      if (assessment.isAcceptAssessmentCheckedAll(role, status)) {
        allCheckedCount += 1;
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllAcceptAssessment(role, status, forceCheck) {
    this.assessmentList.forEach((assessment) =>
      assessment.setCheckAllAcceptAssessment(role, status, forceCheck),
    );
  }

  @computed get isNextButtonDisable() {
    const isSelectAccept = this.assessmentList.every((assessment) =>
      assessment.children.every(
        (child) =>
          child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.accept,
      ),
    );

    const isSelectReject = this.assessmentList.every((assessment) =>
      assessment.children.every(
        (child) =>
          child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.reject,
      ),
    );

    // check if reject roadmap set comment or not
    const isVPRejectAssessmentSetComment = this.assessmentList
      .map((roadmapModel) =>
        roadmapModel.children.filter(
          (child) =>
            child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.reject,
        ),
      )
      .flat()
      .every((child) => child.acceptAssessment.rejectComment);

    if (isSelectReject && isSelectAccept) {
      return true;
    }

    if (!isVPRejectAssessmentSetComment) {
      return true;
    }

    return false;
  }

  @computed get canSubmitAssessment() {
    const assessmentFiltered = this.assessmentList.map((assessment) =>
      assessment.children.filter((child) => child.readyToSubmit),
    ); // [[], [{1}], [{2}], [], []]
    const assessmentArrayFlated = assessmentFiltered.flat(); // [{1}, {2}]
    const isReadyToSend = assessmentArrayFlated.some(
      (assessment) => assessment.submitAssessment.isSelected,
    );

    return isReadyToSend;
  }

  @computed get isAllAssessmentChecked() {
    const isChecked = this.assessmentList.every(
      (children) => children.isAllAssessmentChecked,
    );

    return isChecked;
  }

  @computed get isAllReadyToSend() {
    const assessmentFiltered = this.assessmentList.map((assessment) =>
      assessment.children.filter((child) => child.readyToSubmit),
    );
    const assessmentArrayFlated = assessmentFiltered.flat();
    if (assessmentArrayFlated.length > 0) {
      return false;
    }
    return true;
  }

  @computed get isAllReadyToNext() {
    const assessmentFiltered = this.assessmentList.map((assessment) =>
      assessment.children.filter((child) => child.readyToSubmit),
    );

    const assessmentArrayFlated = assessmentFiltered.flat();
    if (assessmentArrayFlated.length > 0) {
      return false;
    }
    return true;
  }

  @action setCheckAllAssessment() {
    if (!this.isAllAssessmentChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (!child.submitAssessment.roadmapIds) {
            child.submitAssessment.setRoadmapIds(child.id);
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.submitAssessment.setRoadmapIds(null);
        });
      });
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default AssessmentListModel;
