import { action, observable } from 'mobx';

class BottomBarAssessmentModel {
  @observable buttonName = ''

  @observable enabled = null

  constructor(buttonData) {
    this.buttonName = buttonData.buttonName;
    this.enabled = buttonData.enabled;
  }
}

export default BottomBarAssessmentModel;
