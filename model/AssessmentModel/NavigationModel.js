import { observable, action, computed } from 'mobx';

class NavigationModel {
  @observable nameTh = '';

  @observable no = '';

  @observable nameEn = '';

  @observable id = null;

  @observable type = '';

  @observable overAllratingId = 0;

  constructor(nav) {
    this.nameTh = nav.nameTh;
    this.nameEn = nav.nameEn;
    this.no = nav.no;
    this.id = nav.id;
    this.type = nav.type;
    this.overAllratingId = nav.overAllratingId;
  }

  @computed get overAllRatingColor() {
    if (this.type === 'O') {
      return '#333333';
    }

    if (this.type === 'R') {
      if (this.overAllratingId === 3) {
        return '#00b140';
      } else if (this.overAllratingId === 2) {
        return '#ffc72c';
      } else {
        return '#d80000';
      }
    }

    return;
  }

  @computed get trackRiskNav() {
    if (this.type === 'R') {
      const objContainer = this.id.indexOf(this.no);
      const trackRiksNav = this.id.slice(0, objContainer);

      return trackRiksNav;
    }

    return '';
  }
}

export default NavigationModel;
