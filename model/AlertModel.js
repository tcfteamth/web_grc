import { observable, action, computed } from 'mobx';
import request from '../services';

class AlertModel {

  @observable CSA_Report = 0;

  @observable CSA_Assessment = 0;

  @observable CSA_RoadMap = 0;

  @observable COSO_Assessment = 0;

  @observable COSO_RoadMap = 0;

  @observable CSA_FollowUp = 0;

  @action async getAlert(token) {
    try {
        const result = await request.clientServices.getAlert(token);
        for(let field in result.data){
            this[field] = result.data[field]
        } 
    } catch (e) {
        console.log(e)
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default AlertModel;
