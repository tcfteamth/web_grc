import { action, observable } from 'mobx';
import DashboardModel from './DashboardModel';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class FollowUpDashboardModel {
  @observable dashboardList;

  @action async getDashboardFollowUp(
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    processAnd = [],
    processOr = [],
    objectives
  ) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives ? objectives.value : ''

      const result = await request.dashboardFollowUpServices.getDashboardFollowUp(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.dashboardList = result.data.map(
        (dashboard) => new DashboardModel(dashboard),
      );
      loadingStore.setIsLoading(false);
    } catch (error) {
      loadingStore.setIsLoading(false);

      console.log(error);
    }
  }
}

export default FollowUpDashboardModel;
