import { observable } from 'mobx';

class TableDTOModel {
  @observable indicator;

  @observable overDue;

  @observable open;

  @observable postpone;

  @observable closed;

  @observable divEn;

  @observable shiftEn = '';

  constructor(table) {
    this.indicator = table.indicator;
    this.divEn = table.divEn;
    this.shiftEn = table.shiftEn;
    this.overDue = table.overDue;
    this.open = table.open;
    this.postpone = table.postpone;
    this.closed = table.closed;
  }
}

export default TableDTOModel;
