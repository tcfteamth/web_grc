import { observable } from 'mobx';

class GraphDTOModel {
  @observable status;

  @observable count;

  @observable percent;

  constructor(graph) {
    this.status = graph.status;
    this.count = graph.count;
    this.percent = graph.percent;
  }
}

export default GraphDTOModel;
