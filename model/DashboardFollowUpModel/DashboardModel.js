import { observable, computed } from 'mobx';
import GraphDTOModel from './GraphDTOModel';
import TableDTOModel from './TableDTOModel';

class DashboardModel {
  @observable name;

  @observable graphDTO;

  @observable tableDTO;

  constructor(dashboard) {
    this.name = dashboard.name;
    this.graphDTO = dashboard.graphDTO?.map(
      (graph) => new GraphDTOModel(graph),
    );
    this.tableDTO = dashboard.tableDTO?.map(
      (table) => new TableDTOModel(table),
    );
  }

  @computed get percentGraphDTO() {
    return this.graphDTO.map((graph) => graph.percent);
  }

  @computed get countGraphDTO() {
    return this.graphDTO.map((graph) => graph.count);
  }

  @computed get sumCountGraphDTO() {
    return this.graphDTO.reduce(
      (sum, currentValue) => sum + currentValue.count,
      0,
    );
  }

  @computed get isAllGraphNoData() {
    return this.graphDTO.every((graph) => graph.count === 0);
  }

  @computed get fullname() {
    if (this.name === 'corrective') {
      return 'Corrective Action';
    }

    return 'Enhancement';
  }
}

export default DashboardModel;
