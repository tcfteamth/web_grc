import { action, observable, computed } from 'mobx';

export const ACCEPT_ROADMAP_STATUS = {
  accept: 'DM_ACCEPT',
  reject: 'DM_REJECT',
  staffReady: 'STAFF_READY',
};

class AcceptRoadmapsModel {
  @observable status = '';

  @observable id = '';

  @observable rejectComment = '';

  @observable userId = 0;

  isSelectedAccept() {
    return this.no !== '' && this.status === ACCEPT_ROADMAP_STATUS.accept;
  }

  isSelectedReject() {
    return this.no !== '' && this.status === ACCEPT_ROADMAP_STATUS.reject;
  }

  @action setNo(status, id) {
    if (this.status === status) {
      this.id = '';
      this.status = '';
    } else {
      this.id = id;
      this.status = status;
      this.rejectComment = '';
    }
  }

  @action setAssessor(assesorId) {
    this.userId = assesorId;
  }

  @action setRejectComment(text) {
    this.rejectComment = text;
  }
}

export default AcceptRoadmapsModel;
