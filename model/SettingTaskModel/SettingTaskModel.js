import { observable, action, computed } from 'mobx';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';

class SubmitEmailTaskModel {
  @observable submitTask;

  @action setField(field, value) {
    this[field] = value;
  }
}
class TaskEmailModel {
  @observable activeDate;

  @observable taskName;

  @observable inProgress;

  @observable active;

  @observable submitEmailTaskModel = new SubmitEmailTaskModel();

  constructor(task) {
    this.activeDate = task.activeDate;
    this.taskName = task.taskName;
    this.inProgress = task.inProgress;
    this.active = task.active;
  }

  @computed get name() {
    if (this.taskName === 'ROADMAP') {
      return 'Roadmap';
    }

    if (this.taskName === 'ASSESSMENT') {
      return 'Assessment';
    }

    if (this.taskName === 'FOLLOW_UP') {
      return 'Follow Up';
    }

    if (this.taskName === 'COSO_ROADMAP') {
      return 'COSO Roadmap';
    }

    if (this.taskName === 'COSO_ASSESSMENT') {
      return 'COSO Assessment';
    }
  }
}
class SettingTaskModel {
  @observable isReAssessmentSuccess;

  @observable isActiveFollowUpSuccess;

  @observable isCOSOReAssessmentSuccess;

  @observable reAssessmentInPorgressStatus;

  @observable mailFollow;

  @observable mailFollowCOSO;

  @observable activeMailCOSO = false;

  @observable taskNameMailCOSO = '';

  @observable submitEmailFollow;

  @action async activeReAssessment() {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.settingTaskServices.submitActiveReAssessment();

      if (result.status === 201) {
        this.isReAssessmentSuccess = 'Success';
        await this.checkReAssessmentInProgress();
      }

      loadingStore.setIsLoading(false);
    } catch (e) {
      this.isReAssessmentSuccess = `Fail - ${e.response.data.message}`;
      loadingStore.setIsLoading(false);
    }
  }

  @action async activeFollowUp(token) {
    try {
      const result = await request.settingTaskServices.submitActiveFollowUp(
        token,
      );

      if (result.status === 200) {
        this.isActiveFollowUpSuccess = 'Success';
      }
    } catch (e) {
      this.isActiveFollowUpSuccess = 'Fail';
      console.log(e);
    }
  }

  @action async activeCOSOReAssessment() {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.settingTaskServices.submitActiveCOSOReAssessment();

      if (result.status === 201) {
        this.isCOSOReAssessmentSuccess = 'Success';
      }

      loadingStore.setIsLoading(false);
    } catch (e) {
      this.isCOSOReAssessmentSuccess = `Fail - ${e.response.data.message}`;
      loadingStore.setIsLoading(false);
    }
  }

  @action async checkReAssessmentInProgress() {
    try {
      const result = await request.settingTaskServices.checkReAssessmentInProgress();

      this.reAssessmentInPorgressStatus = result.data;
    } catch (e) {
      console.log(e);
    }
  }

  @action async checkMailFollow() {
    try {
      const result = await request.settingTaskServices.checkMailFollow();

      this.mailFollow = result.data.map((task) => new TaskEmailModel(task));
    } catch (e) {
      console.log(e);
    }
  }

  @action async checkMailCOSOFollow() {
    try {
      const result = await request.settingTaskServices.checkMailCOSOFollow();

      this.mailFollowCOSO = result.data.map((task) => new TaskEmailModel(task));
    } catch (e) {
      console.log(e);
    }
  }

  @action async toggleMailFollow() {
    try {
      await request.settingTaskServices.toggleMailFollow(
        this.submitEmailFollow,
      );
    } catch (e) {
      console.log(e);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default SettingTaskModel;
