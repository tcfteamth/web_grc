import { observable, action, computed } from 'mobx';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';

class ManualTableModel {
  @observable id = 0;

  @observable employeeId = '';
  @observable employeeName = '';
  @observable orgId = '';
  @observable orgName = '';
  @observable level = '';
  @observable positionName = '';

  constructor(manualTable) {
    this.id = manualTable.id;
    this.employeeId = manualTable.employeeId;
    this.employeeName = manualTable.employeeName;
    this.orgId = manualTable.orgId;
    this.orgName = manualTable.orgName;
    this.level = manualTable.level;
    this.positionName= manualTable.positionName;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

class SettingTaskModel {
  @observable manualTableList = [];

  @observable employeeId = '';
  @observable orgId = '';
  @observable level = '';
  @observable positionName = '';

  @observable id = '';

  @observable page = 1;

  @observable totalPage = 0;

  @observable selectedProcess = [];

  @observable selectedProcessId = [];

  @observable listProcessLv = [];

  @action setField(field, value) {
    this[field] = value;
  }

  @action onAddProcess(value) {
    const procssList = [];
    value.map((process) => procssList.push(process.id));
    this.selectedProcessId = procssList;
  }

  @action async getManualTableList(token,page) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.settingTaskServices.getManualTableList(token, page);
      this.manualTableList = result.data.content.map((manualTable) => new ManualTableModel(manualTable));
      this.page = page;
      this.totalPage = result.data.totalPages;
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  }
  @action async submitDeleteManualAssign(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.settingTaskServices.submitDeleteManualAssign(id);
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
  @action async submitDeleteTag(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.settingTaskServices.submitDeleteTag(id);
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // Edit row tag
  @action async getEditTagById(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.settingTaskServices.editTagById(id);
      this.name = result.data.tag.name;
      this.id = result.data.tag.id;
      const submitList = result.data.listLv3.concat(result.data.listLv4);
      this.listProcessLv = submitList;
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default SettingTaskModel;
