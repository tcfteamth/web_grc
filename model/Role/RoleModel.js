import { observable, action, computed } from 'mobx';
import { toJS } from 'mobx';
import { MenuModel } from '../Role/AuthoritiesModel';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import {
  userRoleListStore,
  userTypeListStore,
  systemListStore,
  positionListStore,
} from '..';

class RoleListModel {
  @observable id = 0;

  @observable amount = 0;

  @observable roleId = '';

  @observable roleName = '';

  @observable roleType = '';

  @observable updateDate = '';

  constructor(requestItem) {
    this.id = requestItem.id;
    this.amount = requestItem.amount;
    this.roleId = requestItem.roleId;
    this.roleName = requestItem.roleName;
    this.roleType = requestItem.roleType;
    this.updateDate = requestItem.updateDate;
  }
}
class RoleModel {
  @observable amount = 0;

  @observable roleList = [];

  @observable authoritiesId = [];

  @observable size = 0;

  @observable totalPage = 0;

  @observable totalContent = 0;

  @observable page = 1;

  @observable roleType = '';

  @observable roleTypeValue = '';

  @observable roleTypeList = [];

  @observable roleUser = [];

  @observable roleUserNew = [];

  @observable roleUserList = [];

  @observable roleSystemList = [];

  @observable positionList = [];

  @observable selectRoleSystem = '';

  @observable selectPosition = '';

  @observable roleUserId = [];

  @observable roleUserGetId = [];

  @observable role = '';

  @observable roleSpacial = [];

  @observable roleName = '';

  @observable roleSystem = '';

  @observable isAdmin = false;

  @observable isIcAgent = false;

  @observable isCompliance = false;

  @observable isIaFinding = false;

  @observable positionLevel = '';

  @observable authoritiesList = [];

  @observable authoritiesId = [];

  @action setField(field, value) {
    this[field] = value;
  }

  @action setFieldId(value) {
    if (value) {
      this.roleUserId.push(value);
    }
  }

  // get List All

  @action async getRoleList(page) {
    try {
      const result = await request.clientServices.getRoleList(page);
      this.roleList = result.data.contents.map(
        (list) => new RoleListModel(list),
      );

      this.totalContent = result.data.totalContent;
      this.totalPage = result.data.totalPage;
    } catch (e) {
      console.log(e);
    }
  }

  // get Authorities

  @action async getAuthoritiesList() {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.clientServices.getAuthoritiesList();
      this.authoritiesList = result.data.menus.map(
        (list) => new MenuModel(list),
      );
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  // get data by ID for Edit
  @action async getRoleListById(roleType, id, positionLevel) {
    try {
      const result = await request.clientServices.getRoleListById(roleType, id, positionLevel);

      this.amount = result.data.amount;
      this.roleId = result.data.id;
      this.roleName = result.data.roleName;
      this.roleType = result.data.roleType;
      this.updateDate = result.data.updateDate;
      this.isAdmin = result.data.isAdmin;
      this.isIcAgent = result.data.isIcAgent;
      this.isCompliance = result.data.isCompliance;
      this.isIaFinding = result.data.isIaFinding;
      this.isDelete = result.data.isDelete;
      this.positionLevel = result.data.positionLevel;
      this.roleSystem = result.data.roleSystem;
      this.authoritiesId = result.data.authorities;
      this.selectPosition = result.data.positionLevel;
      this.selectRoleSystem = result.data.roleSystem;

      if (this.roleType === 'SPECIAL') {
        this.roleUser = result.data.users.map(u => {
          return {
            empId: u.empId,
            id: u.empId,
            label: u.name,
            name: u.name,
            value: u.name
          }
        });
      }
      
      // map เพื่อเชคค่าเริ่มต้น authorities
      result.data.authorities.forEach((list) =>
        this.authoritiesList.find((e) =>
          e.authorities.find((item) => {
            if (item.id === list) {
              return (item.isSelect = true);
            }
          }),
        ),
      );
    } catch (e) {
      console.log(e);
    }
  }

  // get Type

  @action async getRoleType(type) {
    try {
      const optionsResult = await userTypeListStore.getRoleTypeList(type);
      this.roleTypeList = optionsResult;
    } catch (e) {
      console.log(e);
    }
  }

  // get User

  @action async getUserRoleList(token, roleId) {
    try {
      const optionsResult = await userRoleListStore.getUserRoleList(token, roleId);
      this.roleUserList = optionsResult;
    } catch (e) {
      console.log(e);
    }
  }

  // get Position

  @action async getPositionList(token) {
    try {
      const optionsResult = await positionListStore.getPositionList(token);
      this.positionList = optionsResult;
    } catch (e) {
      console.log(e);
    }
  }

  // get System

  @action async getRoleSystemList(token) {
    try {
      const optionsResult = await systemListStore.getRoleSystemList(token);
      this.roleSystemList = optionsResult;
    } catch (e) {
      console.log(e);
    }
  }

  // get Spacial

  @action async getRoleSpacialList() {
    try {
      const result = await request.clientServices.getRoleSpacial();
      this.roleSpacial = result.data.map((list) => ({
        value: list.value,
        label: list.name,
        status: false,
      }));
      return this.roleSpacial;
    } catch (e) {
      console.log(e);
    }
  }

  // map User ใหม่สำหรับการส่งไป สร้าง หรือแก้ไข หลังบ้าน

  @action async getUsetListNew() {
    try {
      this.roleUserNew = this.roleUser.map((list) => ({
        empId: list.empId,
        name: list.name,
      }));
      return this.roleUserNew;
    } catch (e) {
      console.log(e);
    }
  }

  @action setRole(role) {
    // map เพื่อเชคการเลือก role และ return status ที่เลือก
    this.roleSpacial.forEach((item) => {
      if (role === item.value) {
        this.setField(`${item.value}`, true);
        return (item.status = true);
      } else {
        this.setField(`${item.value}`, false);
        return (item.status = false);
      }
    });
  }

  @action async handleCheckboxAuthorityId() {
    // get เพื่อให้เป็นค่าเริ่มต้นเพื่อเตรียม update
    await this.getAuthoritiesList();

    // map เพื่อให้ใส่ค่าใหม่ลงใน array
    this.authoritiesId.forEach((id) =>
      this.authoritiesList.forEach((e) => {
        e.authorities.find((item) => {
          if (item.id === id) {
            return (item.isSelect = true);
          }
        });
      }),
    );
  }
}

export default RoleModel;
