import { action, observable, computed } from 'mobx';

export class AuthoritiesSubModel {
  @observable id = null;

  @observable authority = '';

  @observable isSelect = false;

  constructor(item) {
    this.authority = item && item.authority;
    this.id = item && item.id;
    this.isSelect = item && item.isSelect;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export class MenuModel {
  @observable authorities = [];

  @observable type = '';

  constructor(list) {
    this.type = list.type;
    this.authorities = list.authorities.map(
      (result) => new AuthoritiesSubModel(result),
    );
  }
}

// export default MenuModel;
