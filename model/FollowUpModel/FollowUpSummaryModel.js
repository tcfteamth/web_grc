import { observable, action } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class SummaryDTOS {
  @observable controlName;

  @observable dueDate;

  @observable lastUpdate;

  @observable rateId;

  @observable status;

  @observable partner;

  @observable id;

  @observable locked;

  @observable division;

  @observable divEn;

  @observable shiftEn = '';

  @observable partnerName;

  constructor(summary) {
    this.controlName = summary.controlName;
    this.dueDate = summary.dueDate || '-';
    this.lastUpdate = summary.lastUpdate || '-';
    this.rateId = summary.rateId;
    this.status = summary.status;
    this.partner = summary.partner;
    this.id = summary.id;
    this.locked = summary.locked;
    this.division = summary.division;
    this.divEn = summary.divEn;
    this.shiftEn = summary.shiftEn;
    this.partnerName = summary.partnerName;
  }
}

class ListModel {
  @observable assessmentName;

  @observable count;

  @observable summaryDTOS;

  constructor(list) {
    this.assessmentName = list.assessmentName;
    this.count = list.count;
    this.summaryDTOS =
      list.summaryDTOS.map((summary) => new SummaryDTOS(summary)) || [];
  }
}

class FollowUpSummaryModel {
  @observable page;

  @observable size;

  @observable totalPage;

  @observable totalContent;

  @observable list;

  @action async getFollowUpSummaryList(
    token,
    page,
    size,
    search,
    year,
    lvl3,
    lvl4,
    bu,
    department,
    division,
    shift,
    startDate,
    endDate,
    status,
    processAnd = [],
    processOr = [],
    objectives
  ) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives ? objectives.value : ''

      const result = await request.followUpServices.getFollowUpSummaryList(
        token,
        page,
        size,
        search,
        year,
        lvl3,
        lvl4,
        bu,
        department,
        division,
        shift,
        startDate,
        endDate,
        status,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.page = result.data.page;
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.list = result.data.list.map((l) => new ListModel(l));
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async exportFollowUpSummary(
    token,
    search,
    year,
    lvl3,
    lvl4,
    bu,
    department,
    division,
    shift,
    startDate,
    endDate,
    status,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.followUpServices.exportFollowUpSummary(
        token,
        search,
        year,
        lvl3,
        lvl4,
        bu,
        department,
        division,
        shift,
        startDate,
        endDate,
        status,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (error) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default FollowUpSummaryModel;
