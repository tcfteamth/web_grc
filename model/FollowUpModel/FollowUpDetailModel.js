import { observable, action, computed } from 'mobx';
import request from '../../services';
import WorkFlowModel from './WorkFlowModel';
import DetailModel from './DetailModel';
import InfoModel from './InfoModel';
import UploadFollowUpFileModel from './UploadFollowUpFileModel';
import FollowUpCommentListModel from './FollowUpCommentList';
import loadingStore from '../../contexts/LoadingStore';
import { followUpStatus } from '../../utils/followup/followup-status';

class FollowUpDetailModel {
  @observable no;

  @observable updateDate;

  @observable roadMapText;

  @observable workflowList;

  @observable lastYearPath;

  @observable createBy;

  @observable followUpDetail;

  @observable updateBy;

  @observable hasLastYear;

  @observable name;

  @observable id;

  @observable roadMapType;

  @observable roadMapImage;

  @observable status;

  @observable createDate;

  @observable info;

  @observable canUpdate;

  @observable documents;

  // updatedetail - start

  @observable dueDate;

  @observable correctiveAction;

  @observable selectedStatus;

  // updatedetail - end

  @observable filesUpload = [];

  @observable deleteDocumentFileIds = [];

  @observable assessmentDocuments = [];

  @observable commentText = '';

  @observable commentList;

  @observable madeByPartner;

  @observable partnerName;

  @observable divEn;

  @observable shiftEn = '';

  @action async getFollowUpDetail(token, id) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.followUpServices.getFollowUpDetail(
        token,
        id,
      );

      this.no = result.data.no;
      this.updateDate = result.data.updateDate;
      this.roadMapText = result.data.roadMapText;
      this.workflowList = result.data.workflow.map(
        (workflow) => new WorkFlowModel(workflow),
      );
      this.lastYearPath = result.data.lastYearPath;
      this.createBy = result.data.createBy;
      this.followUpDetail = new DetailModel(result.data.followUpDetail);
      this.updateBy = result.data.updateBy;
      this.hasLastYear = result.data.hasLastYear;
      this.name = result.data.name;
      this.id = result.data.id;
      this.roadMapType = result.data.roadMapType;
      this.roadMapImage = result.data.roadMapImage;
      this.status = result.data.status;
      this.createDate = result.data.createDate;
      this.info = new InfoModel(result.data.info);
      this.canUpdate = result.data.canUpdate;
      this.documents = result.data.documents;
      this.assessmentDocuments = result.data.assessmentDocuments;
      this.madeByPartner = result.data.MadeByPartner;
      this.partnerName = result.data.partnerName;
      this.divEn = result.data.divEn;
      this.shiftEn = result.data.shiftEn;

      loadingStore.setIsLoading(false);
    } catch (e) {
      loadingStore.setIsLoading(false);

      console.log(e);
    }
  }

  @action async saveFollowUpDetail(token, id) {
    try {
      loadingStore.setIsLoading(true);

      const updateDetailData = {
        id: parseInt(id, 10),
        dueDate: this.followUpDetail.dueDate,
        postponeDate: this.followUpDetail.postponeDate ? this.followUpDetail.postponeDate : this.followUpDetail.savedPostpone,
        correctiveAction: this.followUpDetail.correctiveAction,
        status: this.followUpDetail.status,
        rateId: this.followUpDetail.rateId,
        enhance: this.followUpDetail.lv7IsEnhance,
        comment: this.commentText,
      };

      const result = await request.followUpServices.saveFollowUpDetail(
        token,
        updateDetailData,
      );

      if (this.deleteDocumentFileIds.length > 0) {
        await this.submitDeleteDocumentFiles();
      }

      if (this.filesUpload.length > 0) {
        for (const file of this.filesUpload) {
          await this.saveFileFollowDetail(token, id, file);
        }
      }

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async updateFollowUpDetail(token, id) {
    try {
      loadingStore.setIsLoading(true);

      const updateDetailData = {
        id: parseInt(id, 10),
        dueDate: this.followUpDetail.dueDate,
        postponeDate: this.followUpDetail.postponeDate ? this.followUpDetail.postponeDate : this.followUpDetail.savedPostpone,
        correctiveAction: this.followUpDetail.correctiveAction,
        status: this.followUpDetail.status,
        rateId: this.followUpDetail.rateId,
        enhance: this.followUpDetail.lv7IsEnhance,
        comment: this.commentText,
      };

      const result = await request.followUpServices.updateFollowUpDetail(
        token,
        updateDetailData,
      );

      if (this.deleteDocumentFileIds.length > 0) {
        await this.submitDeleteDocumentFiles();
      }

      if (this.filesUpload.length > 0) {
        console.log('file', this.filesUpload);

        for (const file of this.filesUpload) {
          await this.saveFileFollowDetail(token, id, file);
        }
      }

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async saveFileFollowDetail(token, id, file) {
    try {
      const formData = new FormData();
      formData.append('followUpId', id);
      formData.append('title', file.title);
      formData.append('file', file.file);

      await request.followUpServices.saveFileFollowDetail(token, formData);
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitFollowUp(token, id) {
    const formData = new FormData();
    formData.append('approveIds', [parseInt(id, 10)]);
    formData.append('rejectIds', []);

    try {
      loadingStore.setIsLoading(true);

      const result = await request.followUpServices.submitFollowUp(
        token,
        formData,
      );

      if (this.deleteDocumentFileIds.length > 0) {
        await this.submitDeleteDocumentFiles();
      }

      if (this.filesUpload.length > 0) {
        for (const file of this.filesUpload) {
          await this.saveFileFollowDetail(token, id, file);
        }
      }

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitDeleteDocumentFiles() {
    const ids = this.deleteDocumentFileIds.join(',');

    const result = await request.followUpServices.submitDeleteFollowDocFiles(
      ids,
    );

    return result;
  }

  @action async getFollowUpCommentList(id) {
    try {
      const convertIdToNumber = parseInt(id, 10);

      const result = await request.followUpServices.getFollowUpComment(
        convertIdToNumber,
      );

      this.commentList = result.data.map(
        (comment) => new FollowUpCommentListModel(comment),
      );
    } catch (e) {
      console.log(e);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action setFilesUpload(title, file) {
    this.filesUpload.push({
      title,
      id: `${title}${this.filesUpload.length + 1}`,
      file,
      filePath: file.preview.url,
      canUpdate: false,
      type: 'uploadfile',
    });
  }

  @computed get isCalendarDisabled() {
    if (this.selectedStatus === followUpStatus.POSTPONE) {
      return false;
    }

    return true;
  }

  @computed get displayDocumentFiles() {
    return this.documents && [...this.documents, ...this.filesUpload];
  }

  @action deleteDocumentFiles(id, type) {
    if (type === 'uploadfile') {
      const filterFilesUpload = this.filesUpload.filter((doc) => doc.id !== id);

      this.filesUpload = filterFilesUpload;
    } else {
      const filterDocumentFiles = this.documents.filter((doc) => doc.id !== id);

      this.deleteDocumentFileIds.push(id);

      this.documents = filterDocumentFiles;
    }
  }

  @computed get isPostponeSetEndDate() {
    if (
      this.followUpDetail.status === followUpStatus.POSTPONE &&
      (this.followUpDetail.postponeDate)
    ) {
      return true;
    }

    else if (
      this.followUpDetail.status === followUpStatus.CLOSED && this.followUpDetail.dueDate
    ) {
      return true;
    }

    return false;
  }

  @computed get isSubmitButtonDisabled() {
    // if (this.canUpdate) {
    if (this.followUpDetail.correctiveAction && this.isPostponeSetEndDate) {
      return false;
    }

    if (
      this.followUpDetail.correctiveAction &&
      this.followUpDetail.status === followUpStatus.CANCELLED
    ) {
      return false;
    }
    // }

    return true;
  }

  @computed get enableCalendar() {
    if (!this.canUpdate) {
      return [this.followUpDetail.dueDate];
    }

    return undefined;
  }
}

export default FollowUpDetailModel;
