import { observable, action } from 'mobx';

class WorkFlowModel {
  @observable fileName;

  @observable fileSize;

  @observable filePath;

  @observable id;

  constructor(workflow) {
    this.fileName = workflow.fileName;
    this.fileSize = workflow.fileSize;
    this.filePath = workflow.filePath;
    this.id = workflow.id;
  }
}

export default WorkFlowModel;
