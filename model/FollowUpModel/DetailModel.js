import { roundToNearestMinutes } from 'date-fns';
import { observable, action, computed } from 'mobx';
import moment from 'moment';
import { followUpStatus } from '../../utils/followup/followup-status';

class DetailModel {
  @observable lv5Name;

  @observable lv5Type;

  @observable lv6Name;

  @observable lv7Name;

  @observable lv7Type;

  @observable lv7Format;

  @observable lv7Frequency;

  @observable lv7Document;

  @observable lv7TechControl;

  @observable lv7EmployeeController;

  @observable lv7IsEnhance;

  @observable rateId;

  @observable endDate;

  @observable initialRemark;

  @observable suggestion;

  @observable benefit;

  @observable benefitText;

  @observable dueDate;

  @observable correctiveAction;

  @observable status;

  @observable partnerName;

  @observable postponeDate;

  @observable savedPostpone;

  constructor(detail) {
    this.lv5Name = detail.lv5Name || '-';
    this.lv5Type = detail.lv5Type || '-';
    this.lv6Name = detail.lv6Name;
    this.lv7Name = detail.lv7Name;
    this.lv7Type = detail.lv7Type;
    this.lv7Format = detail.lv7Format;
    this.lv7Frequency = detail.lv7Frequency;
    this.lv7Document = detail.lv7Document;
    this.lv7TechControl = detail.lv7TechControl;
    this.lv7EmployeeController = detail.lv7EmployeeController;
    this.lv7IsEnhance = detail.lv7IsEnhance;
    this.rateId = detail.rateId;
    this.endDate = detail.endDate;
    this.initialRemark = detail.initialRemark;
    this.suggestion = detail.suggestion;
    this.benefit = detail.benefit;
    this.benefitText = detail.benefitText;
    this.dueDate = detail.dueDate;
    this.correctiveAction = detail.correctiveAction;
    this.status = detail.status;
    this.partnerName = detail.partnerName;
    this.postponeDate = detail.postponeDate;
    this.savedPostpone = detail.postponeDate;

    // Support old format of postpone data
    if(this.status === followUpStatus.POSTPONE && !this.savedPostpone && this.dueDate){
      this.postponeDate = this.dueDate;
      this.savedPostpone = this.dueDate;
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get doNeedEnhance() {
    if (this.lv7IsEnhance && this.rateId === 3) {
      return true;
    }

    return false;
  }

  @computed get isNotGoodRate() {
    return this.rateId !== 3;
  }

  @computed get isCalendarDisabled() {
    if (
      this.status === followUpStatus.POSTPONE ||
      this.status === followUpStatus.CLOSED
    ) {
      return true;
    }

    return false;
  }

  @computed get minDate() {
    if (this.status === followUpStatus.POSTPONE) {
      return moment(this.endDate).format('YYYY-MM-DD');
    }

    return null;
  }

  @computed get maxDate() {
    if (this.status === followUpStatus.CLOSED) {
      if(this.savedPostpone){
        return moment(this.savedPostpone).format('YYYY-MM-DD');
      }
      else{
        if(this.dueDate && this.dueDate > this.endDate){
          return moment(this.dueDate).format('YYYY-MM-DD');
        }
        return moment(this.endDate).format('YYYY-MM-DD');
      }
    }

    return null;
  }

  @computed get isPostpone() {
    if (this.status === followUpStatus.POSTPONE) {
      return true;
    }
    return false;
  }
}

export default DetailModel;
