import { observable, computed, action } from 'mobx';

class RejectFollowUpModel {
  @observable followUpId;

  @computed get isSelectedReject() {
    return this.followUpId !== null;
  }

  @action setFollowUpId(id) {
    if (this.followUpId === id) {
      this.followUpId = null;
    } else {
      this.followUpId = id;
    }
  }
}

export default RejectFollowUpModel;
