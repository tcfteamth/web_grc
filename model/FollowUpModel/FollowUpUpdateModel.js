import { action, observable } from 'mobx';

class FollowUpUpdateModel {
  @observable id;

  @observable dueDate;

  @observable correctiveAction;

  @observable status;

  @observable rateId;

  @observable isEnhance;

  @action async updateDetail() {
    try {
    } catch (e) {
      console.log(e);
    }
  }
}

export default FollowUpUpdateModel;
