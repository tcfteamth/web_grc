import { observable, computed, action } from 'mobx';

class ApproveFollowUpModel {
  @observable followUpId;

  @observable approveId;

  @observable rejectId;

  @computed get isApprove() {
    return !!this.approveId;
  }

  @computed get isReject() {
    return !!this.rejectId;
  }

  @action setApprove(id) {
    if (this.approveId === id) {
      this.approveId = null;
    } else {
      this.approveId = id;
      this.rejectId = null;
    }
  }

  @action setReject(id) {
    if (this.rejectId === id) {
      this.rejectId = null;
    } else {
      this.rejectId = id;
      this.approveId = null;
    }
  }
}

export default ApproveFollowUpModel;
