import { observable, action } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class FilterModel {
  @observable isDisable;

  @observable filterName;

  @observable isDefaultValue;

  constructor(filter) {
    this.isDisable = filter.isDisable;
    this.filterName = filter.filterName;
    this.isDefaultValue = filter.isDefaultValue;
  }
}

class FollowUpFilterListModel {
  @observable filterList;

  @action async getFilterList(token) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.followUpServices.getFollowUpFilterList(
        token,
      );

      this.filterList = result.data.map((filter) => new FilterModel(filter));
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getDashboardFilterList() {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.dashboardFollowUpServices.getDashboardFilterList();

      this.filterList = result.data.map((filter) => new FilterModel(filter));
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  }

  @action async getReportFollowUpFilterList(token) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.dashboardFollowUpServices.getReportFollowUpFilterList(
        token,
      );

      this.filterList = result.data.map((filter) => new FilterModel(filter));

      loadingStore.setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  }
}

export default FollowUpFilterListModel;
