import { observable } from 'mobx';

class FollowUpCommentListModel {
  @observable commentFrom;

  @observable commentBy;

  @observable comment;

  constructor(commentList) {
    this.commentFrom = commentList.commentFrom;
    this.commentBy = commentList.commentBy;
    this.comment = commentList.comment;
  }
}

export default FollowUpCommentListModel;
