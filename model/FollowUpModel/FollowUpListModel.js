import { observable, action, computed } from 'mobx';
import moment from 'moment';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';
import FilterModel from '../FilterModel';
import ApproveFollowUpModel from './ApproveFollowUpModel';
import RejectFollowUpModel from './RejectFollowUpModel';

class TotalStatusCountModel {
  @observable status;

  @observable count;

  constructor(total) {
    this.status = total.status;
    this.count = total.count;
  }
}

class ListModel {
  @observable assessmentName;

  @observable controlName;

  @observable division;
  @observable shift;

  @observable divEn;

  @observable shiftEn = '';

  @observable dueDate;

  @observable lastUpdate;

  @observable rateId;

  @observable status;

  @observable partner;

  @observable locked;

  @observable id;

  @observable actionPlan;

  @observable approveFollowUp = new ApproveFollowUpModel();

  @observable rejectFollowUp = new RejectFollowUpModel();

  @observable partnerName;

  constructor(list) {
    this.assessmentName = list.assessmentName;
    this.controlName = list.controlName;
    this.division = list.division;
    this.shift = list.shift;
    this.divEn = list.divEn;
    this.shiftEn = list.shiftEn;
    this.dueDate = list.dueDate || '-';
    this.lastUpdate = list.lastUpdate || '-';
    this.rateId = list.rateId;
    this.status = list.status;
    this.partner = list.partner;
    this.id = list.id;
    this.locked = list.locked;
    this.actionPlan = list.actionPlan;
    this.partnerName = list.partnerName;
  }
}

class FollowUpListModel {
  @observable page;

  @observable size;

  @observable totalPage;

  @observable totalContent;

  @observable followUpList;

  @observable totalStatusCount;

  @observable followUpSummaryList;

  @observable filterModel = new FilterModel();

  @action async getFollowUpTodoList(
    token,
    page,
    size,
    search,
    year,
    lvl3,
    lvl4,
    bu,
    department,
    division,
    shift,
    startDate,
    endDate,
    status,
    processAnd = [],
    processOr = [],
    objectives
  ) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives ? objectives.value : ''

      console.log(objectives)

      const result = await request.followUpServices.getFollowUpTodoList(
        token,
        page,
        size,
        search,
        year,
        lvl3,
        lvl4,
        bu,
        department,
        division,
        shift,
        startDate,
        endDate,
        status,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.page = result.data.page;
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.followUpList = result.data.list.map((l) => new ListModel(l));
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getFollowUpTotalStatusCount(
    token,
    search,
    year,
    lvl3,
    lvl4,
    bu,
    department,
    division,
    shift,
    startDate,
    endDate,
    status,
    processAnd = [],
    processOr = [],
    objectives
  ) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives ? objectives.value : ''

      const result = await request.followUpServices.getTotalStatusCount(
        token,
        search,
        year,
        lvl3,
        lvl4,
        bu,
        department,
        division,
        shift,
        startDate,
        endDate,
        status,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.totalStatusCount = result.data.map(
        (status) => new TotalStatusCountModel(status),
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitFollowUp(token) {
    const approveList = [];
    const rejectList = [];

    const filterApproveId = this.followUpList.map((list) => {
      if (list.approveFollowUp.isApprove) {
        approveList.push(list.approveFollowUp.approveId);
      }
    });

    const filterRejectId = this.followUpList.map((list) => {
      if (list.approveFollowUp.isReject) {
        rejectList.push(list.approveFollowUp.rejectId);
      }
    });

    const formData = new FormData();
    formData.append('approveIds', approveList);
    formData.append('rejectIds', rejectList);

    try {
      const result = await request.followUpServices.submitFollowUp(
        token,
        formData,
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isSelectFollowUpList() {
    const app = this.followUpList?.some(
      (list) => list.approveFollowUp.approveId || list.approveFollowUp.rejectId,
    );

    return app;
  }

  @action submitFilter() {
    console.log(
      this.filterModel.selectedYear,
      this.filterModel.selectedLvl3,
      this.filterModel.selectedLvl4,
      this.filterModel.selectedBu,
      this.filterModel.selectedDepartment.no,
      this.filterModel.selectedDivision,
      this.filterModel.selectedShift,
      moment(this.filterModel.selectedStartDueDate).format('DD/MM/YYYY'),
      moment(this.filterModel.selectedEndDueDate).format('DD/MM/YYYY'),
      this.filterModel.selectedStatus,
    );
  }

  @action checkAllApproveFollowUp() {
    if (!this.isCheckAllApproveFollowUp) {
      this.followUpList.forEach((followUp) => {
        if (!followUp.locked && !followUp.approveFollowUp.isApprove) {
          followUp.approveFollowUp.setApprove(followUp.id);
        }
      });
    } else {
      this.followUpList.forEach((followUp) => {
        if (!followUp.locked) {
          followUp.approveFollowUp.setApprove(null);
        }
      });
    }
  }

  @computed get isCheckAllApproveFollowUp() {
    const filterIsNotLocked = this.followUpList.filter(
      (followUp) => !followUp.locked,
    );

    return filterIsNotLocked.every(
      (followUp) => followUp.approveFollowUp.isApprove,
    );
  }

  @action checkAllRejectFollowUp() {
    if (!this.isCheckAllRejectFollowUp) {
      this.followUpList.forEach((followUp) => {
        if (!followUp.locked && !followUp.approveFollowUp.isReject) {
          followUp.approveFollowUp.setReject(followUp.id);
        }
      });
    } else {
      this.followUpList.forEach((followUp) => {
        if (!followUp.locked) {
          followUp.approveFollowUp.setReject(null);
        }
      });
    }
  }

  @computed get isCheckAllRejectFollowUp() {
    const filterIsNotLocked = this.followUpList.filter(
      (followUp) => !followUp.locked,
    );

    return filterIsNotLocked.every(
      (followUp) => followUp.approveFollowUp.isReject,
    );
  }
}

export default FollowUpListModel;
