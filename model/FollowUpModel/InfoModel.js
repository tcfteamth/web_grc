import { observable } from 'mobx';

class PersonModel {
  @observable date;

  @observable name;

  @observable position;

  constructor(person) {
    this.date = person.date;
    this.name = person.name;
    this.position = person.position;
  }
}

class InfoModel {
  @observable approver;

  @observable indicator;

  @observable reviewer;

  @observable worker;

  constructor(info) {
    this.approver = new PersonModel(info.approver);
    this.indicator = info.indicator;
    this.reviewer = new PersonModel(info.reviewer);
    this.worker = new PersonModel(info.worker);
  }
}

export default InfoModel;
