import { observable, action, computed } from 'mobx';

class UploadFollowUpFileModel {
  @observable fileTitle;

  @observable file;

  // constructor(file) {
  //   this.file = file.file;
  //   this.fileTitle = file.fileTitle;
  // }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSetFile() {
    return !this.file;
  }
}

export default UploadFollowUpFileModel;
