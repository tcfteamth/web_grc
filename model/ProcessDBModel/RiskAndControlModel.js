import { action, observable } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import FilterModel from '../FilterModel';

class Lv7ListModel {
  @observable lvl = null;

  @observable name = '';

  @observable no = '';

  @observable order = null;

  @observable isStandard = false;

  @observable id = null;

  @observable controlDocument = [];

  @observable controlFormat = '';

  @observable controlFrequency = '';

  @observable controlType = '';

  constructor(lv7) {
    this.lvl = lv7.lvl;
    this.name = lv7.name;
    this.no = lv7.no;
    this.order = lv7.order;
    this.id = lv7.id;
    this.controlDocument = lv7.controlDocument;
    this.controlFormat = lv7.controlFormat;
    this.controlFrequency = lv7.controlFrequency;
    this.controlType = lv7.controlType;
    this.isStandard = lv7.isStandard;
  }
}

class Lv6ListModel {
  @observable lvl = null;

  @observable name = '';

  @observable no = '';

  @observable order = null;

  @observable children = [];

  @observable complianceCore = '';

  @observable elegalLibrary = '';

  @observable externalLawSummary = '';

  @observable externalLaws = '';

  @observable internalLawSummary = '';

  @observable internalLaws = '';

  constructor(lv6) {
    this.lvl = lv6.lvl;
    this.name = lv6.name;
    this.no = lv6.no;
    this.order = lv6.order;
    this.complianceCore = lv6.complianceCore;
    this.elegalLibrary = lv6.elegalLibrary;
    this.externalLawSummary = lv6.externalLawSummary;
    this.externalLaws = lv6.externalLaws;
    this.internalLawSummary = lv6.internalLawSummary;
    this.internalLaws = lv6.internalLaws;
    this.children =
      lv6.children && lv6.children.map((item) => new Lv7ListModel(item));
  }
}

class Lv5ListModel {
  @observable lvl = null;

  @observable name = '';

  @observable no = '';

  @observable order = null;

  @observable type = '';

  @observable children = [];

  constructor(lv5) {
    this.lvl = lv5.lvl;
    this.name = lv5.name;
    this.no = lv5.no;
    this.order = lv5.order;
    this.type = lv5.type;
    this.children =
      lv5.children && lv5.children.map((item) => new Lv6ListModel(item));
  }
}

class Lv4ListModel {
  @observable lvl = null;

  @observable name = '';

  @observable no = '';

  @observable order = null;

  @observable id = null;

  @observable children = [];

  @observable type = '';

  @observable isDelete = false;

  constructor(lv4) {
    this.lvl = lv4.lvl;
    this.name = lv4.name;
    this.no = lv4.no;
    this.order = lv4.order;
    this.id = lv4.id;
    this.type = lv4.type;
    this.isDelete = lv4.isDelete;
    this.children =
      lv4.children && lv4.children.map((item) => new Lv5ListModel(item));
  }
}

class RiskAndControlModel {
  @observable page = 1;

  @observable size = 10;

  @observable totalPage = 0;

  @observable totalContent = 0;

  @observable list = [];

  @observable searchText = '';

  @observable mode = ''; // list  || noLvl;

  @observable name = '';

  @observable lvl = '';

  @observable lvl4 = '';

  @observable fullPath = '';

  @observable no = '';

  @observable deleteNo = null;

  @observable objectType = '';

  @observable filterModel = new FilterModel();

  @action async getRiskAndControlList(token, page, searchText, fullPath, type) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.processDBServices.getRiskAndControlList(
        token,
        page,
        searchText,
        fullPath,
        type,
      );
      this.list = result.data.contents.map((com) => new Lv4ListModel(com));
      this.page = result.data.page;
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getRiskAndControlByNoLvl(
    noLvl,
    token,
    page,
    searchText,
    fullPath,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.processDBServices.getRiskAndControlByNoLvl(
        noLvl,
        token,
        page,
        searchText,
        fullPath,
      );
      this.list = result.data.children.map((com) => new Lv5ListModel(com));
      this.name = result.data.name;
      this.lvl = result.data.lvl;
      this.no = result.data.no;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async deleteRiskAndControlList(token, id) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.processDBServices.onDeleteRiskAndControlList(
        token,
        id,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async exportExcelRiskAndControl(token) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.processDBServices.exportExcelRiskAndControl(
        token,
        this.lvl4.id,
        this.objectType.value,
        this.searchText,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default RiskAndControlModel;
