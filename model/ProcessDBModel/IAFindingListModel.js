import { action, observable } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import FilterModel from '../FilterModel';

class IAFindingModel {
  @observable id = '';

  @observable actualEnd = null;

  @observable actualStart = null;

  @observable auditEngagementCode = '';

  @observable auditEngagementName = '';

  @observable auditUnitLink = '';

  @observable auditUnits = '';

  @observable categorizedRecommendation = '';

  @observable closed = null;

  @observable closedBy = '';

  @observable comment = '';

  @observable createdDate = null;

  @observable findingDescription = '';

  @observable findingName = '';

  @observable findingNo = '';

  @observable findingRate = '';

  @observable focusObjective = '';

  @observable impact = '';

  @observable isCheck = '';

  @observable issueState = '';

  @observable recommendationDescription = '';

  @observable recommendationState = '';

  @observable remark1 = '';

  @observable remark2 = '';

  @observable revisedTargetDate = '';

  @observable rootCause = '';

  @observable subProcess = '';

  @observable targetDate = null;

  @observable teamLead = '';

  @observable teamMember = '';

  @observable type = '';

  @observable updatedDate = null;

  @observable year = '';

  constructor(ia) {
    this.id = ia.id;
    this.actualEnd = ia.actualEnd;
    this.actualStart = ia.actualStart;
    this.auditEngagementCode = ia.auditEngagementCode;
    this.auditEngagementName = ia.auditEngagementName;
    this.auditUnitLink = ia.auditUnitLink;
    this.auditUnits = ia.auditUnits;
    this.categorizedRecommendation = ia.categorizedRecommendation;
    this.closed = ia.closed;
    this.closedBy = ia.closedBy;
    this.comment = ia.comment;
    this.createdDate = ia.createdDate;
    this.findingDescription = ia.findingDescription;
    this.findingName = ia.findingName;
    this.findingNo = ia.findingNo;
    this.findingRate = ia.findingRate;
    this.focusObjective = ia.focusObjective;
    this.impact = ia.impact;
    this.isCheck = ia.isCheck;
    this.issueState = ia.issueState;
    this.recommendationDescription = ia.recommendationDescription;
    this.recommendationState = ia.recommendationState;
    this.remark1 = ia.remark1;
    this.remark2 = ia.remark2;
    this.revisedTargetDate = ia.revisedTargetDate;
    this.rootCause = ia.rootCause;
    this.subProcess = ia.subProcess;
    this.targetDate = ia.targetDate;
    this.teamLead = ia.teamLead;
    this.teamMember = ia.teamMember;
    this.type = ia.type;
    this.updatedDate = ia.updatedDate;
    this.year = ia.year;
  }
}

class IAFindingListModel {
  @observable page = 1;

  @observable size = 10;

  @observable totalPage = 0;

  @observable totalContent = 0;

  @observable list = [];

  @observable searchText = '';

  @observable filterModel = new FilterModel();

  @action async getIAFindingList(
    token,
    page,
    searchText,
    bu,
    departmentNo,
    divisionNo,
    type,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.processDBServices.getIAFindingList(
        token,
        page,
        searchText,
        bu,
        departmentNo,
        divisionNo,
        type,
      );

      this.page = result.data.page;
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;

      if (result.data.list) {
        this.list = result.data.list.map((ia) => new IAFindingModel(ia));
      } else {
        this.list = result.data.list;
      }

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default IAFindingListModel;
