import { observable, action, computed } from 'mobx';

export class ValidateControlFormModel {
  @observable isSetNameTh;

  @observable isSetType;

  @observable isSetFormat;

  @observable isSetFrequency;

  @observable isSetDocument;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateNameTh() {
    return this.isSetNameTh === false;
  }

  @computed get validateType() {
    return this.isSetType === false;
  }

  @computed get validateFormat() {
    return this.isSetFormat === false;
  }

  @computed get validateFrequency() {
    return this.isSetFrequency === false;
  }

  @computed get validateDocument() {
    return this.isSetDocument === false;
  }
}

export class ValidateRiskFormModel {
  @observable isSetRiskNameTh;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateRiskName() {
    return this.isSetRiskNameTh === false;
  }
}

export class ValidateObjectFormModel {
  @observable isSetNameTh;

  @observable isSetType;

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get validateNameTh() {
    return this.isSetNameTh === false;
  }

  @computed get validateType() {
    return this.isSetType === false;
  }
}

export class ValidateAssessmentDetailFormModel {
  @action setField(field, value) {
    this[field] = value;
  }
}
