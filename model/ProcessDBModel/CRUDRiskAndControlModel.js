import { action, observable, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import FilterModel from '../FilterModel';
import AttachFileModel from '../AssessmentModel/AttachFileModel';
import {
  ValidateControlFormModel,
  ValidateRiskFormModel,
  ValidateObjectFormModel,
} from '../ProcessDBModel/ValidateFormModel';

export const CONTROL_FORMAT_CONSTANT = {
  automate: 'Automate Control',
  itManual: 'IT Dependent Control',
  manual: 'Manual Control',
};

class ControlsModel {
  @observable fullPath = '';

  @observable nameTh = '';

  @observable no = '';

  @observable format = null; // รูปแบบของการควบคุม

  @observable nameEn = '';

  @observable id = null;

  @observable type = null; // ประเภทของการควบคุม

  @observable frequency = null; // ความถี่ของการควบคุม

  @observable isStandard = false;

  @observable document = null;

  @observable validateFormModel = new ValidateControlFormModel();

  constructor(control) {
    this.fullPath = control.fullPath;
    this.nameTh = control.nameTh;
    this.no = control.no;
    this.document = control.document;
    this.format = control.format;
    this.nameEn = control.nameEn;
    this.id = control.id;
    this.type = control.type;
    this.frequency = control.frequency;
    this.isStandard = control.isStandard;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  // ใช้ !! เพื่อแปลงจาก string ให้เป็น boolean
  @computed get isSetNameTh() {
    return !!this.nameTh;
  }

  @computed get isSetType() {
    return !!this.type;
  }

  @computed get isSetFormat() {
    return !!this.format;
  }

  @computed get isSetFrequency() {
    return !!this.frequency;
  }

  @action setValidateForm() {
    const validateFields = [
      'isSetNameTh',
      'isSetType',
      'isSetFormat',
      'isSetFrequency',
    ];

    validateFields.map((field) =>
      this.validateFormModel.setField(field, this[field]),
    );
  }
}

class RisksModel {
  @observable fullPath = '';

  @observable nameTh = '';

  @observable no = '';

  @observable controls = [];

  @observable nameEn = '';

  @observable id = null;

  @observable complianceCore = '';

  @observable elegalLibrary = '';

  @observable externalLawSummary = '';

  @observable externalLaws = '';

  @observable internalLawSummary = '';

  @observable internalLaws = '';

  @observable validateRiskFormModel = new ValidateRiskFormModel();

  constructor(risk) {
    this.fullPath = risk.fullPath;
    this.nameTh = risk.nameTh;
    this.no = risk.no;
    this.controls = risk.controls.map((control) => new ControlsModel(control));
    this.nameEn = risk.nameEn;
    this.id = risk.id;
    this.complianceCore = risk.complianceCore;
    this.elegalLibrary = risk.elegalLibrary;
    this.externalLawSummary = risk.externalLawSummary;
    this.externalLaws = risk.externalLaws;
    this.internalLawSummary = risk.internalLawSummary;
    this.internalLaws = risk.internalLaws;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  // AddDetailControlAssessment comp
  @action addControl() {
    const newFullPath = `${this.fullPath}.${
      parseInt(this.controls.length, 10) + 1
    }`;
    const newNo = `${parseInt(this.controls.length, 10) + 1}`;
    // const newNo = this.controls.length + 1;

    const newControlDetail = {
      id: null,
      no: newNo,
      nameTh: '',
      nameEn: '',
      fullPath: newFullPath,
      format: null,
      frequency: null,
      techControl: '',
      employeeController: '',
      isStandard: false,
    };
    const newControl = new ControlsModel(newControlDetail);

    this.controls.push(newControl);
  }

  @action setValidateRiskForm() {
    this.validateRiskFormModel.setField(
      'isSetRiskNameTh',
      this.isSetRiskNameTh,
    );

    this.controls.forEach((control) => control.setValidateForm());
  }

  @computed get isSetAllControlData() {
    const isSetName = this.controls.every((control) => control.isSetNameTh);
    const isSetType = this.controls.every((control) => control.isSetType);
    const isSetFormat = this.controls.every((control) => control.isSetFormat);
    const isSetFrequency = this.controls.every(
      (control) => control.isSetFrequency,
    );

    // const isSetDocument = this.controls.every(
    //   (control) => control.isSetDocument,
    // );

    return isSetName && isSetType && isSetFormat && isSetFrequency;
  }

  @computed get isSetRiskNameTh() {
    return !!this.nameTh;
  }

  @action deleteControl(fullPath) {
    const filterControl = this.controls.filter(
      (control) => control.fullPath !== fullPath,
    );
    // reorder fullpath and no
    filterControl.forEach((control, index) => {
      const newFullPath = `${this.fullPath}.${index + 1}`;
      const newNo = `${index + 1}`;
      control.setField('fullPath', newFullPath);
      control.setField('no', newNo);
    });
    this.controls = filterControl;
  }
}

class ObjectsModel {
  @observable no = '';

  @observable nameTh = '';

  @observable nameEn = '';

  @observable risks = [];

  @observable id = null;

  @observable type = '';

  @observable validateObjectFormModel = new ValidateObjectFormModel();

  constructor(objectItem) {
    this.no = objectItem.no;
    this.nameTh = objectItem.nameTh;
    this.nameEn = objectItem.nameEn;
    this.id = objectItem.id;
    this.type = objectItem.type;
    this.risks = objectItem.risks.map((risk) => new RisksModel(risk));
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @computed get isSetAllRiskData() {
    const isAllSet = this.risks.every((risk) => risk.isSetAllControlData);
    const isSetRiskName = this.risks.every((risk) => risk.isSetRiskNameTh);

    return isAllSet && isSetRiskName;
  }

  @action addRisk() {
    const newRiskFullPath = `${this.no}.${this.risks.length + 1}`;
    const newNo = `${this.risks.length + 1}`;

    const newControlFullPath = `${newRiskFullPath}.1`;
    const newControlNo = '1';

    const newRiskObject = {
      fullPath: newRiskFullPath,
      nameTh: '',
      no: newNo,
      nameEn: '',
      id: null,
      controls: [
        {
          id: null,
          no: newControlNo,
          nameTh: '',
          nameEn: '',
          fullPath: newControlFullPath,
          format: null,
          frequency: null,
          isStandard: false,
          document: null,
          type: null,
        },
      ],
    };

    const newRisk = new RisksModel(newRiskObject);

    this.risks.push(newRisk);
  }

  @action deleteRisk(fullPath) {
    const filteredRisk = this.risks.filter(
      (risk) => risk.fullPath !== fullPath,
    );
    this.risks = filteredRisk;
  }

  @computed get isAllControlInRiskSetName() {
    const getIsAllControlSetName = this.risks.map(
      (risk) => risk.isAllControlSetName,
    );
    const isSet = getIsAllControlSetName.every((control) => control);

    return isSet;
  }

  @computed get isAllControlInRiskSetType() {
    const getIsAllControlSetName = this.risks.map(
      (risk) => risk.isAllControlSetType,
    );
    const isSet = getIsAllControlSetName.every((control) => control);

    return isSet;
  }

  @computed get isAllRiskInObjectSetName() {
    const isSet = this.risks.every((risk) => risk.isSetRiskNameTh);

    return isSet;
  }

  @computed get isSetNameTh() {
    return !!this.nameTh;
  }

  @computed get isSetType() {
    return !!this.type;
  }

  @action setValidateObjectForm() {
    this.validateObjectFormModel.setField('isSetNameTh', this.isSetNameTh);
    this.validateObjectFormModel.setField('isSetType', this.isSetType);
    this.risks.forEach((risk) => risk.setValidateRiskForm());
  }
}

class CRUDRiskAndControlModel {
  @observable objective = '';

  @observable objectType = '';

  @observable no = '';

  @observable id = null;

  @observable objectType = '';

  @observable objects = [];

  @observable newNameThObject = '';

  @observable newObjectTypes = '';

  @observable canUpdate = '';

  @observable createBy = '';

  @observable createDate = '';

  @observable lvl3 = '';

  @observable lvl4 = '';

  @observable filterModel = new FilterModel();

  @action setField(field, value) {
    this[field] = value;
  }

  @action async getRiskAndControlById(token, id) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.processDBServices.getRiskAndControlById(
        token,
        id,
      );

      this.canUpdate = result.data.canUpdate;
      this.createBy = result.data.createBy;
      this.createDate = result.data.createDate;
      this.id = result.data.id;
      this.newNameThObject = result.data.name;
      this.no = result.data.no;
      this.lvl3 = result.data.lv3No;
      this.lvl4 = result.data.no;

      if (result.data.objects.length !== 0) {
        this.objects = result.data.objects.map(
          (objectItem) => new ObjectsModel(objectItem),
        );
      } else {
        const newObjectNo = `${this.no}.1`;
        const newRiskFullpath = `${newObjectNo}.1`;
        const newControlFullpath = `${newRiskFullpath}.1`;

        const newObjectData = {
          nameTh: '',
          no: newObjectNo,
          isStandard: false,
          nameEn: '',
          id: null,
          type: this.newObjectTypes,
          risks: [
            {
              id: null,
              no: 1,
              nameEn: '',
              nameTh: '',
              fullPath: newRiskFullpath,
              controls: [
                {
                  id: null,
                  no: 1,
                  nameTh: '',
                  nameEn: '',
                  fullPath: newControlFullpath,
                  format: null,
                  frequency: null,
                  isStandard: false,
                  document: null,
                  type: null,
                },
              ],
            },
          ],
        };

        console.log('newObjectData', newObjectData);

        const newObject = new ObjectsModel(newObjectData);

        this.objects.push(newObject);
      }

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async createRiskAndControl(token) {
    loadingStore.setIsLoading(true);

    const objectData = {
      id: this.id,
      no: this.lvl4.value,
      objects: this.objects,
    };

    try {
      const result = await request.processDBServices.createRiskAndControl(
        token,
        objectData,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setNewNameThObject(value) {
    this.newNameThObject = value;
  }

  @action setNewObjectTypes(value) {
    this.newObjectTypes = value;
  }

  @action addObject() {
    const noId = this.lvl4.value;
    const getLastNo =
      this.objects.length !== 0
        ? this.objects[this.objects.length - 1].no
        : noId; // 1.1.2.3

    const newObjFullPath =
      this.objects.length === 0 ? `${getLastNo}.0` : `${getLastNo}`; // 1.1.2.3.0 - // 1.1.2.3.1
    const newNo = `${this.objects.length + 1}`;

    const splitNo = newObjFullPath.split('.'); // [1,1,2,3,1]
    const addNo = parseInt(splitNo[splitNo.length - 1], 10) + 1; // 1 + 1 = 2
    const joinNewNo = splitNo.slice(0, splitNo.length - 1).join('.'); // [1,1,2,3,2]
    const newNoObject = `${joinNewNo}.${addNo}`; // 1.1.2.3.2

    const newRiskFullPath = `${newNoObject}.1`; // 1.1.2.3.2.1
    const newRiskNo = '1';

    const newControlFullPath = `${newRiskFullPath}.1`; // 1.1.2.3.2.1.1
    const newControlNo = '1';

    const newObjectData = {
      nameTh: this.newNameThObject,
      no: newNoObject,
      isStandard: false,
      nameEn: '',
      id: null,
      type: this.newObjectTypes,
      risks: [
        {
          id: null,
          no: newRiskNo,
          nameEn: '',
          nameTh: '',
          fullPath: newRiskFullPath,
          controls: [
            {
              id: null,
              no: newControlNo,
              nameTh: '',
              nameEn: '',
              fullPath: newControlFullPath,
              format: null,
              frequency: null,
              isStandard: false,
              document: null,
              type: null,
            },
          ],
        },
      ],
    };

    const newObject = new ObjectsModel(newObjectData);
    this.objects.push(newObject);
  }

  @action setValidateAssessmentForm() {
    let ischeck = this.objects.forEach((objectItem) =>
      objectItem.setValidateObjectForm(),
    );
  }

  @computed get isSetAllObjectData() {
    const isSetAllRiskData = this.objects.every(
      (objectItem) => objectItem.isSetAllRiskData,
    );
    const isSetObjectName = this.objects.every(
      (objectItem) => objectItem.nameTh,
    );

    const isSetObjectType = this.objects.every((objectItem) => objectItem.type);

    return isSetAllRiskData && isSetObjectName && isSetObjectType;
  }

  @computed get isSetCheckLv4() {
    return !!this.no;
  }

  @action deleteObject(no) {
    const filteredObject = this.objects.filter(
      (objectItem) => objectItem.no !== no,
    );

    this.objects = filteredObject;
  }
}

export default CRUDRiskAndControlModel;
