import { action, observable } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';
import FilterModel from '../FilterModel';

class ComplianceListModel {
  @observable id = '';

  @observable benefit = '';

  @observable coResponsibleDivision = '';

  @observable complianceImpact = '';

  @observable complianceRisk = '';

  @observable controlClassification = '';

  @observable controlRating = null;

  @observable controlType = '';

  @observable createdDate = null;

  @observable division = '';

  @observable documentEvidence = '';

  @observable dueDate = null;

  @observable eLegalLibraryNo = '';

  @observable enhancementCorrective = '';

  @observable executor = '';

  @observable existingControl = '';

  @observable frequency = '';

  @observable grcNo = '';

  @observable impactRating = null;

  @observable information = '';

  @observable likelihoodRating = null;

  @observable measure = '';

  @observable no = null;

  @observable objective = '';

  @observable observationFinding = '';

  @observable process = '';

  @observable regulation = '';

  @observable remark1 = '';

  @observable remark2 = '';

  @observable riskLvRating = null;

  @observable standardOptional = '';

  @observable type = '';

  constructor(com) {
    this.id = com.id;
    this.benefit = com.benefit;
    this.coResponsibleDivision = com.coResponsibleDivision;
    this.complianceImpact = com.complianceImpact;
    this.complianceRisk = com.complianceRisk;
    this.controlClassification = com.controlClassification;
    this.controlRating = com.controlRating;
    this.controlType = com.controlType;
    this.createdDate = com.createdDate;
    this.division = com.division;
    this.documentEvidence = com.documentEvidence;
    this.dueDate = com.dueDate;
    this.eLegalLibraryNo = com.eLegalLibraryNo;
    this.enhancementCorrective = com.enhancementCorrective;
    this.executor = com.executor;
    this.existingControl = com.existingControl;
    this.frequency = com.frequency;
    this.grcNo = com.grcNo;
    this.impactRating = com.impactRating;
    this.information = com.information;
    this.likelihoodRating = com.likelihoodRating;
    this.measure = com.measure;
    this.no = com.no;
    this.objective = com.objective;
    this.observationFinding = com.observationFinding;
    this.process = com.process;
    this.regulation = com.regulation;
    this.remark1 = com.remark1;
    this.remark2 = com.remark2;
    this.riskLvRating = com.riskLvRating;
    this.standardOptional = com.standardOptional;
    this.type = com.type;
  }
}

class ComplianceModel {
  @observable page = 1;

  @observable size = 10;

  @observable totalPage = 0;

  @observable totalContent = 0;

  @observable list = [];

  @observable searchText = '';

  @observable filterModel = new FilterModel();

  @action async getComplianceList(
    token,
    page,
    searchText,
    bu,
    departmentNo,
    divisionNo,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.processDBServices.getComplianceList(
        token,
        page,
        searchText,
        bu,
        departmentNo,
        divisionNo,
      );
      console.log('oin', result);
      this.list = result.data.list.map((com) => new ComplianceListModel(com));
      this.page = result.data.page;
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async importExcelCompliance(data) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.processDBServices.importExcelCompliance(
        data,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data
  @action async exportExcelCompliance(token) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.processDBServices.exportExcelCompliance(
        token,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default ComplianceModel;
