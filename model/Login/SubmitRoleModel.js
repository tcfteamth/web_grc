import { action, observable, computed } from 'mobx';

class SubmitRoleModel {
  @observable roleId = null;

  @observable roleSys = '';

  @action setRoleField(id, roleSys) {
    if (this.roleId === id && this.roleSys === roleSys) {
      this.roleId = null;
      this.roleSys = '';
    } else {
      this.roleId = id;
      this.roleSys = roleSys;
    }
  }
}

export default SubmitRoleModel;
