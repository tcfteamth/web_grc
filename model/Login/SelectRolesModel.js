import { action, observable, computed } from 'mobx';
import storejs from 'store';
import { config } from '../../utils';
import { axiosInstance } from '../../helpers/utils';
import SubmitRoleModel from './SubmitRoleModel';
import { initAuthStore } from '../../contexts';
import loadingStore from '../../contexts/LoadingStore';

const authStore = initAuthStore();

class Role {
  @observable roleId = null;

  @observable roleName = '';

  @observable roleSys = '';

  constructor(roles) {
    this.roleId = roles.roleId;
    this.roleName = roles.roleName;
    this.roleSys = roles.roleSys;
  }
}

class SelectRolesModel {
  @observable roles = [];

  @observable activeModal = false;

  @observable submitRole = new SubmitRoleModel();

  @observable auth = authStore;

  @action async getSelectRoles(token) {
    try {
      const rolesResponse = await axiosInstance.get(
        `${config.apiGatewayBaseUrl}/api/manage/users/selectRoles`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      if (rolesResponse.data.roles.length > 1) {
        this.activeModal = true;
        this.roles = rolesResponse.data.roles.map((role) => new Role(role));
      } else if (rolesResponse.data.roles.length === 1) {
        this.doLoginAD(
          token,
          rolesResponse.data.roles[0].roleId,
          rolesResponse.data.roles[0].roleSys,
        );
        // authStore(token, rolesResponse.data.roles[0].roleId);
      } else {
        window.location.replace(config.baseUrlLogin);
      }
    } catch (e) {
      if (e.response.status === 401) {
        window.location.replace(config.baseUrlLogin);
      }

      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  @action async doLoginAD(token, roleId, roleSys) {
    try {
      const formData = new FormData();
      formData.append('roleId', roleId);
      formData.append('roleSys', roleSys);

      const currentUser = await axiosInstance.post(
        `${config.apiGatewayBaseUrl}/api/manage/users/me`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      storejs.set('accessToken', token);
      storejs.set('currentUser', currentUser.data.user);
      storejs.set('roles', currentUser.data.roles);
      storejs.set('roleName', currentUser.data.roles.roleName);
      window.location.href = '/databasePage/dashboard';

      console.log(currentUser);
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isSelectedRole() {
    return this.submitRole.roleSys;
  }
}

export default SelectRolesModel;
