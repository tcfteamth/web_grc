import { action, observable, computed } from 'mobx';
import moment from 'moment';
import { yearListDropdownStore, manageDepartmentDropdownStore } from './';
import { followUpYearListStore } from './DropdownModel';

class FilterModel {
  @observable selectedLvl2 = '';

  @observable selectedLvl4 = '';

  @observable selectedLvl3 = '';

  @observable selectedRoadmapType = '';

  @observable selectedBu = '';

  @observable selectedDepartment = ''; // {no: <- ใช้ในการส่งไป search, value: <- ใช้ในการยิงไปเรียก division อีกทีนึง}

  @observable selectedDivision = '';

  @observable selectedShift = '';

  @observable selectedArea = '';

  @observable selectedStatus = '';

  @observable selectedRate = '';

  @observable selectedOwner = '';

  @observable selectedTab = 'TODO'; // TODO / SUMMARY

  @observable selectedType = 'ROADMAP'; // ROADMAP / ASSESSMENT

  @observable selectedYear = '';

  @observable selectedRead = '';

  @observable selectedIsYear = '';

  @observable selectedStartDueDate;

  @observable selectedEndDueDate;

  @observable searchText;

  @observable selectedStartCloseDate;

  @observable selectedEndCloseDate;

  @observable selectedIAFindingType;

  @observable selectedProcessAnd = [];

  @observable selectedProcessOr = [];

  @observable selectedObjectives = [];

  @action setField(field, value) {
    this[field] = value;
  }

  @action resetFilter() {
    this.selectedLvl2 = '';
    this.selectedLvl4 = '';
    this.selectedLvl3 = '';
    this.selectedRoadmapType = '';
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStatus = '';
    this.selectedArea = '';
    this.selectedIsYear = '';
    this.selectedRead = '';
    this.selectedRate = '';
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
  }

  @action resetFilterRoadmap() {
    this.selectedLvl2 = '';
    this.selectedRoadmapType = '';
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStatus = '';
    this.selectedArea = '';
    this.selectedIsYear = '';
    this.selectedYear = '';
  }

  @action resetComplianceFilter() {
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
  }

  @action resetTodoListAssessment() {
    this.selectedLvl2 = '';
    this.selectedLvl3 = '';
    this.selectedLvl4 = '';
    this.selectedBu = '';
    this.selectedRoadmapType = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStatus = '';
    this.selectedRate = '';
    this.selectedOwner = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
    // this.selectedIsYear = '';
    // this.selectedYear = '';
  }

  @action resetSummaryAssessment() {
    this.selectedLvl2 = '';
    this.selectedLvl3 = '';
    this.selectedLvl4 = '';
    this.selectedRoadmapType = '';
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStatus = '';
    this.selectedOwner = '';
    this.selectedArea = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetAssessment() {
    this.selectedLvl2 = '';
    this.selectedLvl3 = '';
    this.selectedLvl4 = '';
    this.selectedRoadmapType = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStatus = '';
    this.selectedOwner = '';
    this.selectedArea = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetReAssessment() {
    this.selectedLvl3 = '';
    this.selectedLvl4 = '';
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
  }

  @action resetRequest() {
    this.selectedRoadmapType = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedIsYear = yearListDropdownStore.getFirstOption.value;
    this.selectedRead = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetDashboardAdmin() {
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedBu = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetDashboardDMAndStaff() {
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetDashboardVP() {
    this.selectedDivision = '';
    this.selectedDepartment = '';
    this.selectedShift = '';
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterIAFinding() {
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedLvl4 = '';
    this.selectedLvl3 = '';
    this.selectedRoadmapType = '';
    this.selectedStatus = '';
    this.selectedRate = '';
    this.selectedOwner = '';
    this.selectedYear = '';
    this.selectedIAFindingType = '';
  }

  @action resetFilterReportAdmin() {
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterReportVP() {
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterReportDMAndStaff() {
    this.selectedYear = yearListDropdownStore.getFirstOption.value;
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  // Start - clear filter DBF = Dashboard Follow up
  @action resetFilterDBFAdmin() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterDBFVP() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterDBFDM() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }
  // End - clear filter DBF = Dashboard Follow up

  @action resetFilterFollowUpReportAdmin() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStartDueDate = '';
    this.selectedEndDueDate = '';
    this.selectedStartCloseDate = '';
    this.selectedEndCloseDate = '';
    this.selectedStatus = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterFollowUpReportVP() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStartDueDate = '';
    this.selectedEndDueDate = '';
    this.selectedStartCloseDate = '';
    this.selectedEndCloseDate = '';
    this.selectedStatus = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action resetFilterFollowUpReportDMAndStaff() {
    this.selectedYear = followUpYearListStore.getFirstOption.value;
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedStartDueDate = '';
    this.selectedEndDueDate = '';
    this.selectedStartCloseDate = '';
    this.selectedEndCloseDate = '';
    this.selectedStatus = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @action clearFilterFollowUpList() {
    this.selectedBu = '';
    this.selectedDepartment = '';
    this.selectedDivision = '';
    this.selectedShift = '';
    this.selectedLvl4 = '';
    this.selectedLvl3 = '';
    this.selectedStatus = '';
    this.selectedYear = undefined;
    this.selectedStartDueDate = '';
    this.selectedEndDueDate = '';
    this.selectedProcessAnd = [];
    this.selectedProcessOr = [];
    this.selectedObjectives = [];
  }

  @computed get isBuDisabled() {
    if (this.selectedDepartment && !this.selectedBu) {
      return true;
    }

    if ((this.selectedDivision || this.selectedShift )&& !this.selectedBu && !this.selectedDepartment) {
      return true;
    }

    if (this.selectedDepartment && this.selectedBu) {
      return false;
    }

    return false;
  }

  @computed get isDepartmentDisabled() {
    if (this.selectedDepartment && !this.selectedDivision && !this.selectedShift) {
      return false;
    }

    if (this.selectedDepartment && (this.selectedDivision || this.selectedShift)) {
      return false;
    }

    if (this.selectedDivision || this.selectedShift) {
      return true;
    }

    return false;
  }

  @computed get resetBuMistake() {
    return this.selectedBu && !this.selectedDepartment && !this.selectedShift;
  }

  @computed get isSetDepartment() {
    return !!this.selectedDepartment;
  }

  @action resetDivisionMistake() {
    if (this.selectedBu && !this.selectedDepartment) {
      this.selectedBu = '';
    }
  }

  @action resetShiftMistake() {
    if (this.selectedBu && !this.selectedShift) {
      this.selectedBu = '';
    }
  }

  @computed get isSelectedReportFilter() {
    return this.selectedBu || this.selectedDepartment || this.selectedDivision || this.selectedShift;
  }

  @computed get isSetStartDueDate() {
    if (this.selectedStartDueDate) {
      return moment(this.selectedStartDueDate).format('DD/MM/YYYY');
    }

    return this.selectedStartDueDate;
  }

  @computed get isSetEndDueDate() {
    if (this.selectedEndDueDate) {
      return moment(this.selectedEndDueDate).format('DD/MM/YYYY');
    }

    return this.selectedEndDueDate;
  }

  @computed get convertStartDueDate() {
    return this.selectedStartDueDate
      ? moment(this.selectedStartDueDate).locale('th').format('L')
      : '';
  }

  @computed get convertEndDueDate() {
    return this.selectedEndDueDate
      ? moment(this.selectedEndDueDate).locale('th').format('L')
      : '';
  }

  @computed get convertStartCloseDate() {
    return this.selectedStartCloseDate
      ? moment(this.selectedStartCloseDate).locale('th').format('L')
      : '';
  }

  @computed get convertEndCloseDate() {
    return this.selectedEndCloseDate
      ? moment(this.selectedEndCloseDate).locale('th').format('L')
      : '';
  }
}

export default FilterModel;
