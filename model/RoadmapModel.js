import { observable, action, computed } from 'mobx';
import request from '../services';
import AcceptRoadmapsModel, {
  ACCEPT_ROADMAP_STATUS,
} from './AcceptRoadmapModel';
import SubmitRoadmapsModel from './SubmitRoadmapsModel';
import loadingStore from '../contexts/LoadingStore';
import { assesorDropdownStore } from '.';
import FilterModel from './FilterModel';
import { config } from '../utils';

class RoadmapChildrenModel {
  @observable owner = '';

  @observable no = '';

  @observable lvl = 0;

  @observable depDiv = '';

  @observable divEn = '';

  @observable shiftEn = '';

  @observable hasLastYear = '';

  @observable name = '';

  @observable rejectComment = null;

  @observable roadMap = '';

  @observable remark = '';

  @observable isICAgentPlus = false;

  @observable id = null;

  @observable status = '';

  @observable lastUpdate = null;

  @observable acceptRoadmap = new AcceptRoadmapsModel();

  @observable submitRoadmaps = new SubmitRoadmapsModel();

  @observable roadMapImage = null;

  @observable lastYearPath = '';

  @observable year = null;

  @observable roadMapType = '';

  @observable latestYear = '';

  constructor(data) {
    this.owner = data.owner;
    this.no = data.no;
    this.lvl = data.lvl;
    this.depDiv = data.depDiv;
    this.divEn = data.divEn;
    this.shiftEn = data.shiftEn;
    this.hasLastYear = data.hasLastYear;
    this.name = data.name;
    this.rejectComment = data.rejectComment;
    this.roadMap = data.roadMap;
    this.remark = data.remark;
    this.isICAgentPlus = data.isICAgentPlus;
    this.id = data.id;
    this.status = data.status;
    this.lastUpdate = data.lastUpdate;
    this.acceptRoadmap.userId = data.assesor;
    this.roadMapImage = data.roadMapImage;
    this.lastYearPath = data.lastYearPath;
    this.year = data.year;
    this.roadMapType = data.roadMapType;
    this.latestYear = data.latestYear;
  }

  // ใช้ในการ disabled radio box ของ DM
  @computed get isThisYear() {
    const getThisYear = new Date().getFullYear();

    if (this.year === getThisYear) {
      return false;
    }

    return true;
  }
}

class RoadmapModel {
  @observable no = '';

  @observable lvl = 0;

  @observable children = [];

  @observable name = '';

  @observable submitRoadmapId = [];

  @observable ids = null;

  constructor(data) {
    this.no = data.no;
    this.lvl = data.lvl;
    this.children = data.children.map((item) => new RoadmapChildrenModel(item));
    this.name = data.name;
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action setAllSubmitRoadmapIds() {
    if (!this.isAllSubmitRoadmapIdsCheked) {
      this.children.forEach((child) => {
        if (!child.submitRoadmaps.roadmapIds) {
          child.submitRoadmaps.setRoadmapIds(child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.submitRoadmaps.setRoadmapIds(null);
      });
    }
  }

  @action setAllAcceptChecked() {
    if (!this.isAllAcceptChecked) {
      this.children.forEach((child) => {
        if (
          child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.accept &&
          !child.isThisYear
        ) {
          child.acceptRoadmap.setNo(ACCEPT_ROADMAP_STATUS.accept, child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptRoadmap.setNo('', '');
      });
    }
  }

  @action setAllRejectChecked() {
    if (!this.isAllRejectChecked) {
      this.children.forEach((child) => {
        if (
          child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.reject &&
          !child.isThisYear
        ) {
          child.acceptRoadmap.setNo(ACCEPT_ROADMAP_STATUS.reject, child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptRoadmap.setNo('', '');
      });
    }
  }

  @computed get isAllSubmitRoadmapIdsCheked() {
    const isChecked = this.children.every(
      (child) => child.submitRoadmaps.roadmapIds,
    );

    return isChecked;
  }

  // @computed get isAllRejectChecked() {
  //   const isChecked = this.children.every(
  //     (child) => child.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.reject,
  //   );

  //   return isChecked;
  // }

  // @computed get isAllAcceptChecked() {
  //   const isChecked = this.children.every(
  //     (child) => child.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.accept,
  //   );

  //   return isChecked;
  // }

  @computed get isAllRejectChecked() {
    const isCheck = this.children.filter((child) => !child.isThisYear);

    if (isCheck.length === 0) {
      return false;
    }

    return isCheck.every((i) => i.acceptRoadmap.isSelectedReject());
  }

  @computed get isAllAcceptChecked() {
    const isCheck = this.children.filter((child) => !child.isThisYear);

    if (isCheck.length === 0) {
      return false;
    }

    return isCheck.every((i) => i.acceptRoadmap.isSelectedAccept());
  }

  @computed get isAllCheckedLvl3Disabled() {
    const filteredList = this.children.filter((child) => !child.isThisYear);

    return filteredList.length === 0;
  }

  @computed get isAllCheckedUserId() {
    const roadmapIdsData = [];
    this.children.forEach((roadmapChildren) => {
      if (roadmapChildren.acceptRoadmap.userId) {
        roadmapIdsData.push(roadmapChildren.acceptRoadmap.userId);
      }
    });

    if (roadmapIdsData.length === 0) {
      return true;
    }
    return false;
  }

  // logic การเก็บ object accept/reject roadmap
  @computed get getAcceptRoadmapList() {
    const acceptRoadmapList = [];
    this.children.forEach((roadmapChildren) => {
      if (roadmapChildren.acceptRoadmap.status) {
        acceptRoadmapList.push(roadmapChildren.acceptRoadmap);
      }
    });

    return acceptRoadmapList;
  }

  // logic ในการเก็บค่า roadmap id
  @computed get getRoadmapIds() {
    const roadmapIdsData = [];
    this.children.forEach((roadmapChildren) => {
      if (roadmapChildren.submitRoadmaps.roadmapIds) {
        roadmapIdsData.push(roadmapChildren.submitRoadmaps.roadmapIds);
      }
    });

    return roadmapIdsData;
  }
}

class RoadmapListModel {
  @observable list = [];

  @observable page = 1;

  @observable totalPage = 0;

  @observable totalNewAssessment = 0;

  @observable totalReAssessment = 0;

  @observable totalNextYearAssessment = 0;

  @observable searchRoadmapText = '';

  @observable selectedRoadMapList = [];

  @observable newRequest = '';

  @observable ids = null;

  @observable no = '';

  @observable year = '';

  @observable lvl3 = '';

  @observable lvl4 = '';

  @observable subProcess = '';

  @observable divisionNo = '';

  @observable departmentNo = '';

  @observable departmentId = null;

  @observable remark = '';

  @observable shiftNo = '';

  @observable shiftId = null;

  @observable filterModel = new FilterModel();

  @action setField(field, value) {
    this[field] = value;
  }

  @action async getRoadmapList(
    token,
    page = 1,
    searchValue,
    lvl3,
    lvl4,
    bu,
    roadmapType,
    departmentNo,
    divisionNo,
    shiftNo,
    status,
    year,
    processAnd = [],
    processOr = [],
  ) {
    loadingStore.setIsLoading(true);
    try {
      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const result = await request.roadmapServices.getRoadmapList(
        token,
        page,
        searchValue,
        lvl3,
        lvl4,
        bu,
        roadmapType,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        year,
        processAndStr,
        processOrStr
      );
      const defaultAssesor = assesorDropdownStore.getFirstOption;
      this.list = result.data.roadmaps.map((item) => new RoadmapModel(item));
      this.page = result.data.page;
      this.totalPage = result.data.totalPage;
      this.totalNewAssessment = result.data.totalNewAssessment;
      this.totalReAssessment = result.data.totalReAssessment;
      this.totalNextYearAssessment = result.data.totalNextYearAssessment;
      loadingStore.setIsLoading(false);
    } catch (e) {
      if (e.response.status === 401) {
        window.location.replace(config.baseUrlLogin);
      }
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getAssessmentList(token, page) {
    try {
      const result = await request.assessmentServices.getAssessmentList(
        token,
        page,
      );
      const defaultAssesor = assesorDropdownStore.getFirstOption;

      this.list = result.data.assessments.map(
        (assessment) => new RoadmapModel(assessment, defaultAssesor),
      );
    } catch (e) {
      console.log(e);
    }
  }

  // ใช้ในการโยน roadmap จาก DM ไปให้ staff
  @action async submitAcceptRoadmaps(token) {
    loadingStore.setIsLoading(true);
    const roadmapList = [];

    try {
      const acceptRoadmapList = this.list.reduce(
        (acceptList, roadmap) => [
          ...acceptList,
          ...roadmap.getAcceptRoadmapList,
        ],
        [],
      );
      await acceptRoadmapList.forEach((item) => {
        if (item.userId) {
          roadmapList.push(item);
        }
      });

      const result = await request.roadmapServices.acceptRoadmaps(
        token,
        acceptRoadmapList,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  // ใช้ในการโยน roadmap จาก admin ไปให้ dm
  @action async submitRoadmapsToDM(token) {
    loadingStore.setIsLoading(true);
    try {
      const submitRoadmapIds = this.list.reduce(
        (roadmapIdsData, roadmap) => [
          ...roadmapIdsData,
          ...roadmap.getRoadmapIds,
        ],
        [],
      );
      const result = await request.roadmapServices.submitRoadmaps(
        token,
        submitRoadmapIds,
      );
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitRequest(token) {
    try {
      loadingStore.setIsLoading(true);

      const formData = new FormData();
      formData.append('text', this.newRequest);

      const result = await request.roadmapServices.submitRequest(
        token,
        formData,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  // Edit row roadmap
  @action async editRoadmapById(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.roadmapServices.getEditRoadmapById(id);
      this.id = result.data.roadmapId;
      this.no = result.data.no;
      this.year = result.data.year;
      this.lvl3 = result.data.lv3No;
      this.lvl4 = result.data.lv4No;
      this.divisionNo = result.data.divisionNo;
      this.departmentNo = result.data.departmentNo;
      this.remark = result.data.remark;
      this.subProcess = result.data.subProcess;
      this.departmentId = result.data.departmentId;
      this.shiftId = result.data.shiftId;
      this.shiftNo = result.data.shiftNo;
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action checkedAll() {
    if (!this.isChckedAll) {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          if (!child.submitRoadmaps.roadmapIds) {
            child.submitRoadmaps.setRoadmapIds(child.id);
          }
        });
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          child.submitRoadmaps.setRoadmapIds(null);
        });
      });
    }
  }

  @action checkAcceptAll() {
    if (!this.isAcceptAllChecked) {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          if (
            child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.accept &&
            !child.isThisYear
          ) {
            child.acceptRoadmap.setNo(ACCEPT_ROADMAP_STATUS.accept, child.id);
          }
        });
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          child.acceptRoadmap.setNo('', '');
        });
      });
    }
  }

  @action checkRejectAll() {
    if (!this.isRejectAllChecked) {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          if (
            child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.reject &&
            !child.isThisYear
          ) {
            child.acceptRoadmap.setNo(ACCEPT_ROADMAP_STATUS.reject, child.id);
          }
        });
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.children.forEach((child) => {
          child.acceptRoadmap.setNo('', '');
        });
      });
    }
  }

  @computed get isChckedAll() {
    const isChecked = this.list.every(
      (roadmapModel) => roadmapModel.isAllSubmitRoadmapIdsCheked,
    );

    return isChecked;
  }

  @computed get isSubmitToDmDisabled() {
    return this.list.every((roadmapModel) =>
      roadmapModel.children.every((child) => !child.submitRoadmaps.roadmapIds),
    );
  }

  @computed get isAcceptOrRejectDisabled() {
    // check if user check accept/reject or not
    const isAccept = this.list.every((roadmapModel) =>
      roadmapModel.children.every(
        (child) => child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.accept,
      ),
    );

    const isReject = this.list.every((roadmapModel) =>
      roadmapModel.children.every(
        (child) => child.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.reject,
      ),
    );

    // check if reject roadmap set comment or not
    const isRejectRoadmapSetComment = this.list
      .map((roadmapModel) =>
        roadmapModel.children.filter(
          (child) =>
            child.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.reject,
        ),
      )
      .flat()
      .every((child) => child.acceptRoadmap.rejectComment);

    if (isReject && isAccept) {
      return true;
    }

    if (!isRejectRoadmapSetComment) {
      return true;
    }

    return false;
  }

  @computed get isAcceptAllChecked() {
    const filteredList = this.list
      .map((roadmap) => roadmap.children.filter((child) => !child.isThisYear))
      .flat();

    if (filteredList.length === 0) {
      return false;
    }

    const isChecked = filteredList.every((roadmapModel) =>
      roadmapModel.acceptRoadmap.isSelectedAccept(),
    );

    return isChecked;
  }

  @computed get isRejectAllChecked() {
    const filteredList = this.list
      .map((roadmap) => roadmap.children.filter((child) => !child.isThisYear))
      .flat();

    if (filteredList.length === 0) {
      return false;
    }

    const isChecked = filteredList.every((roadmapModel) =>
      roadmapModel.acceptRoadmap.isSelectedReject(),
    );

    return isChecked;
  }

  @computed get isAllCheckedLvl4Disabled() {
    const filteredRoadmap = this.list
      .map((roadmap) => roadmap.children.filter((child) => !child.isThisYear))
      .flat();

    return filteredRoadmap.length === 0;
  }

  @computed get isAllCheckedUserLvl4() {
    const roadmapIdsData = [];

    this.list.map((roadmap) => {
      roadmap.children.forEach((child) => {
        if (child.acceptRoadmap.userId) {
          roadmapIdsData.push(child.acceptRoadmap.userId);
        }
      });
    });

    if (roadmapIdsData.length === 0) {
      return true;
    }
    return false;
  }

  @action setSearchRoadmapText(text) {
    this.searchRoadmapText = text;
  }
  @action async exportRoadmap(
    token,
    page = 1,
    searchRoadmap = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    roadmapType = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    year = '',
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.roadmapServices.exportRoadmap(
        token,
    page,
    searchRoadmap,
    lvl3 ,
    lvl4 ,
    bu ,
    roadmapType ,
    departmentNo ,
    divisionNo ,
    shiftNo ,
    status,
    year ,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default RoadmapListModel;
