import { observable, action } from 'mobx';
import request from '../../services';

class ManageRoleModel {
  @observable showMainGraph;

  @observable showSummaryBu;

  @observable reportStatus;

  @observable enableSave;

  @observable showPdf;

  @observable showExcel;

  @observable showPublic;

  @action async getManageRole(year, bu) {
    try {
      const result = await request.reportServices.getManageRole(year, bu);

      this.showMainGraph = result.data.showMainGraph;
      this.showSummaryBu = result.data.showSummaryBu;
      this.reportStatus = result.data.reportStatus;
      this.enableSave = result.data.enableSave;
      this.showPdf = result.data.showPdf;
      this.showExcel = result.data.showExcel;
      this.showPublic = result.data.showPublic;
    } catch (e) {
      console.log(e);
    }
  }
}

export default ManageRoleModel;
