import { observable } from 'mobx';

class SubProcessModel {
  @observable lv4No;

  @observable lv4Name;

  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable overAllRatingId;

  @observable enhancement;

  constructor(subProcesses) {
    this.lv4No = subProcesses.lv4No;
    this.lv4Name = subProcesses.lv4Name;
    this.indicator = subProcesses.indicator;
    this.divEn = subProcesses.divEn;
    this.shiftEn = subProcesses.shiftEn;
    this.overAllRatingId = subProcesses.overAllRatingId;
    this.enhancement = subProcesses.enhancement;
  }
}

class CsaResultModel {
  @observable lv3No;

  @observable lv3Name;

  @observable subProcesses;

  constructor(result) {
    this.lv3No = result.lv3No;
    this.lv3Name = result.lv3Name;
    this.subProcesses = result.subProcesses.map(
      (subProcesses) => new SubProcessModel(subProcesses),
    );
  }
}

export default CsaResultModel;
