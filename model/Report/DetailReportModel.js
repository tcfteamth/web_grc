import { observable, action } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class CommentModel {
  @observable comment;

  @observable name;

  @observable role;

  @observable id;

  @observable canDelete;

  constructor(comment) {
    this.comment = comment.comment;
    this.name = comment.name;
    this.role = comment.role;
    this.id = comment.id;
    this.canDelete = comment.canDelete;
  }
}

class DetailReportModel {
  @observable toggle;

  @observable executiveSummary;

  @observable acknowledge;

  @observable acknowledgeSign;

  @observable commentDTOS;

  @observable enableExecutiveSummary;

  @observable enableAcknowledgment;

  @observable enableSave;

  @observable enableComment;

  @observable enableToggle;

  @observable acknowledgeName;

  @observable showButtonSign;

  @observable deleteCommentIds = [];

  @action async getDetailReport(year, bu) {
    try {
      const result = await request.reportServices.getDetailReport(year, bu);

      this.toggle = result.data.toggle;
      this.executiveSummary = result.data.executiveSummary || '';
      this.acknowledge = result.data.acknowledge;
      this.acknowledgeSign = result.data.acknowledgeSign;
      this.commentDTOS = result.data.commentDTOS
        ? result.data.commentDTOS.map((comment) => new CommentModel(comment))
        : [];
      this.enableExecutiveSummary = result.data.enableExecutiveSummary;
      this.enableAcknowledgment = result.data.enableAcknowledgment;
      this.enableSave = result.data.enableSave;
      this.enableComment = result.data.enableComment;
      this.enableToggle = result.data.enableToggle;
      this.acknowledgeName = result.data.acknowledgeName;
      this.showButtonSign = result.data.showButtonSign;
    } catch (e) {
      console.log(e);
    }
  }

  @action deleteComment(id) {
    try {
      this.commentDTOS = this.commentDTOS.filter(
        (comment) => comment.id !== id,
      );

      this.deleteCommentIds.push(id);
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitDeleteComment() {
    try {
      if (this.deleteCommentIds.length !== 0) {
        const ids = this.deleteCommentIds.join(',');

        const result = await request.reportServices.deleteComment(ids);

        return result;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @action async exportExcel(year, bu, departmentNo, divisionNo) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.reportServices.exportReportExcel(
        year,
        bu,
        departmentNo,
        divisionNo,
      );

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);

      console.log(e);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default DetailReportModel;
