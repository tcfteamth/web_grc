import { observable, computed } from 'mobx';

class DetailCorrectiveDTOS {
  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable lv4No;

  @observable lv4Name;

  @observable lv5Type;

  @observable lv6Name;

  @observable lv7Name;

  @observable ratingId;

  @observable initialRemark;

  @observable suggestion;

  @observable partner;

  @observable partnerDivEn;

  @observable partnerShiftEn;

  @observable endDate;

  constructor(detail) {
    this.indicator = detail.indicator;
    this.divEn = detail.divEn;
    this.shiftEn = detail.shiftEn;
    this.lv4No = detail.lv4No;
    this.lv4Name = detail.lv4Name;
    this.lv5Type = detail.lv5Type;
    this.lv6Name = detail.lv6Name;
    this.lv7Name = detail.lv7Name;
    this.ratingId = detail.ratingId;
    this.initialRemark = detail.initialRemark;
    this.suggestion = detail.suggestion;
    this.partner = detail.partner;
    this.partnerDivEn = detail.partnerDivEn;
    this.partnerShiftEn = detail.partnerShiftEn;
    this.endDate = detail.endDate;
  }
}

class CorrectiveAction {
  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable detailCorrectiveDTOS;

  constructor(corrective) {
    this.indicator = corrective.indicator;
    this.divEn = corrective.divEn;
    this.shiftEn = corrective.shiftEn;
    this.detailCorrectiveDTOS = corrective.detailCorrectiveDTOS.map(
      (detail) => new DetailCorrectiveDTOS(detail),
    );
  }

  @computed get newCorrective() {
    const newCorrective = [];
    const chunk = 3; // แบ่งทีละ 3

    for (let i = 0, j = this.detailCorrectiveDTOS.length; i < j; i += chunk) {
      const chunkedArray = this.detailCorrectiveDTOS.slice(i, i + chunk);
      newCorrective.push(chunkedArray);
    }

    return newCorrective;
  }
}

export default CorrectiveAction;
