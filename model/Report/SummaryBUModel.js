import { observable, action, computed } from 'mobx';
import request from '../../services';

export class DepartmentTable {
  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable good;

  @observable fair;

  @observable poor;

  @observable total;

  @observable enhance;

  constructor(summary) {
    this.indicator = summary.indicator;
    this.divEn = summary.divEn;
    this.shiftEn = summary.shiftEn;
    this.good = summary.good;
    this.fair = summary.fair;
    this.poor = summary.poor;
    this.total = summary.total;
    this.enhance = summary.enhance;
  }
}

export class SummaryBUModel {
  @observable departmentList;

  @action async getSummaryBu(year, bu, processAnd, processOr, objectives) {
    try {

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.reportServices.getSummaryBU(year, bu, processAndStr, processOrStr, objectivesStr);

      this.departmentList = result.data.map(
        (summary) => new DepartmentTable(summary),
      );
    } catch (e) {
      console.log(e);
    }
  }
}
