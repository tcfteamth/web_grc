import { observable, computed } from 'mobx';
import { GraphModel, DepartmentTable } from '.';

class DepartmentGraphTableModel {
  @observable name;

  @observable graph;

  @observable table;

  constructor(department) {
    this.name = department.name;
    this.graph = department.graph.map((dg) => new GraphModel(dg));
    this.table = department.table.map((dt) => new DepartmentTable(dt));
  }

  @computed get departmentGraphPercent() {
    const list = [];

    this.graph.map((g) => list.push(g.percent));

    return list;
  }
}

export default DepartmentGraphTableModel;
