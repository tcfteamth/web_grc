import { observable, computed } from 'mobx';

class DetailEnhancesModel {
  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable lv4No;

  @observable lv4Name;

  @observable lv5Type;

  @observable lv6Name;

  @observable lv7Name;

  @observable ratingId;

  @observable initialRemark;

  @observable suggestion;

  @observable partner;

  @observable partnerDivEn;

  @observable partnerShiftEn;

  @observable benefit;

  @observable endDate;

  @observable benefitDetail;

  constructor(detail) {
    this.indicator = detail.indicator;
    this.divEn = detail.divEn;
    this.shiftEn = detail.shiftEn;
    this.lv4No = detail.lv4No;
    this.lv4Name = detail.lv4Name;
    this.lv5Type = detail.lv5Type;
    this.lv6Name = detail.lv6Name;
    this.lv7Name = detail.lv7Name;
    this.ratingId = detail.ratingId;
    this.initialRemark = detail.initialRemark;
    this.suggestion = detail.suggestion;
    this.partner = detail.partner;
    this.partnerDivEn = detail.partnerDivEn;
    this.partnerShiftEn = detail.partnerShiftEn;
    this.benefit = detail.benefit;
    this.endDate = detail.endDate;
    this.benefitDetail = detail.benefitDetail;
  }
}

class EnhancementModel {
  @observable indicator;

  @observable divEn;

  @observable shiftEn = '';

  @observable detailEnhanceDTOS;

  constructor(enhance) {
    this.indicator = enhance.indicator;
    this.divEn = enhance.divEn;
    this.shiftEn = enhance.shiftEn;
    this.detailEnhanceDTOS = enhance.detailEnhanceDTOS.map(
      (detail) => new DetailEnhancesModel(detail),
    );
  }

  @computed get newEnhancement() {
    const newEnhance = [];
    const chunk = 3; // แบ่งทีละ 3

    for (let i = 0, j = this.detailEnhanceDTOS.length; i < j; i += chunk) {
      const chunkedArray = this.detailEnhanceDTOS.slice(i, i + chunk);
      newEnhance.push(chunkedArray);
    }

    return newEnhance;
  }
}

export default EnhancementModel;
