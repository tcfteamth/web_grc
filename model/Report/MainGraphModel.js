import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

export class GraphModel {
  @observable name = '';

  @observable count = 0;

  @observable id = 0;

  @observable percent = 0;

  constructor(graphModel) {
    this.name = graphModel.name;
    this.count = graphModel.count;
    this.id = graphModel.id;
    this.percent = graphModel.percent;
  }
}

export class MainGraphModel {
  @observable BU = null;

  @observable GC = null;

  @action async getMainGraph(bu, year, processAnd, processOr, objectives) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.reportServices.getMainGraph(bu, year, processAndStr, processOrStr, objectivesStr);

      this.BU = result.data.BU.map((buGrapg) => new GraphModel(buGrapg));
      this.GC = result.data.GC.map((gcGraph) => new GraphModel(gcGraph));
      loadingStore.setIsLoading(false);
    } catch (e) {
      loadingStore.setIsLoading(false);

      console.log(e);
    }
  }

  @computed get gcPercentList() {
    const list = [];

    this.GC.map((gc) => list.push(gc.percent));

    return list;
  }

  @computed get buPercentList() {
    const list = [];

    this.BU.map((bu) => list.push(bu.percent));

    return list;
  }
}
