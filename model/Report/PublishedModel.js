import { observable, action } from 'mobx';
import request from '../../services';

class BuDetail {
  @observable bu;

  @observable status;

  constructor(buDetail) {
    this.bu = buDetail.bu;
    this.status = buDetail.status;
  }
}

class PublishedModel {
  @observable year;

  @observable countStatusOn;

  @observable countStatusOff;

  @observable countSign;

  @observable countNotSign;

  @observable statusList;

  @observable signList;

  @action async getPublishedList(year) {
    try {
      const result = await request.reportServices.getPublishedList(year);

      this.year = result.data.year;
      this.countStatusOn = result.data.countStatusOn;
      this.countStatusOff = result.data.countStatusOff;
      this.countSign = result.data.countSign;
      this.countNotSign = result.data.countNotSign;

      this.statusList = result.data.statusList.map(
        (buDetail) => new BuDetail(buDetail),
      );
      this.signList = result.data.signList.map(
        (buDetail) => new BuDetail(buDetail),
      );
    } catch (e) {
      console.log(e);
    }
  }
}

export default PublishedModel;
