import { observable, action, computed } from 'mobx';
import CsaResultModel from './CsaResultModel';
import CorrectiveActionModel from './CorrectiveActionModel';
import EnhancementModel from './EnhancementModel';
import DepartmentGraphTableModel from './DepartmentGraphTableModel';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class TablesModel {
  @observable csaResult;

  @observable correctiveAction;

  @observable enhancement;

  constructor(table) {
    this.csaResult = table.csaResult.map(
      (result) => new CsaResultModel(result),
    );
    this.correctiveAction = table.correctiveAction.map(
      (correction) => new CorrectiveActionModel(correction),
    );
    this.enhancement = table.enhancement.map(
      (enhance) => new EnhancementModel(enhance),
    );
  }
}

class DepartmentListModel {
  @observable tables;

  @observable name;

  @observable department;

  constructor(department) {
    this.tables = new TablesModel(department.tables);
    this.name = department.name;
    this.department = new DepartmentGraphTableModel(department.department);
  }

  @computed get isSetTables() {
    const isSet =
      this.tables.csaResult.length > 0 ||
      this.tables.correctiveAction.length > 0 ||
      this.tables.enhancement.length > 0;

    return isSet;
  }
}

class SummaryDepartmentModel {
  @observable departmentList;

  @action async getSummaryDepartment(year, bu, departmentNo, divisionNo, processAnd, processOr, objectives) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.reportServices.getSummaryDepartment(
        year,
        bu,
        departmentNo,
        divisionNo, 
        processAndStr, 
        processOrStr, 
        objectivesStr
      );

      this.departmentList = result.data.map(
        (department) => new DepartmentListModel(department),
      );
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default SummaryDepartmentModel;
