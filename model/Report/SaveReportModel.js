import { observable, action } from 'mobx';
import FilterModel from '../FilterModel';
import request from '../../services';

class SaveReportModel {
  @observable year;

  @observable bu;

  @observable toggle;

  @observable summary;

  @observable comment;

  @observable acknowledge;

  @observable acknowledgeSign;

  @action async saveReport(token, reportData) {
    try {
      const result = await request.reportServices.saveReport(token, reportData);

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default SaveReportModel;
