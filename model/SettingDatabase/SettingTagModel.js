import { observable, action, computed } from 'mobx';
import request from '../../services';
import loadingStore from '../../contexts/LoadingStore';

class TagModel {
  @observable id = 0;

  @observable name = '';

  constructor(tag) {
    this.id = tag.id;
    this.name = tag.name;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

class SettingTaskModel {
  @observable tagList = [];

  @observable name = '';

  @observable id = '';

  @observable page = 1;

  @observable totalPage = 0;

  @observable selectedProcess = [];

  @observable selectedProcessId = [];

  @observable listProcessLv = [];


  @action setField(field, value) {
    this[field] = value;
  }

  @action onAddProcess(value) {
    const procssList = [];
    value.map((process) => procssList.push(process.id));
    this.selectedProcessId = procssList;
  }

  @action async getTagList(token) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.settingTaskServices.getTagList(token, 1);
      this.tagList = result.data.content.map((tag) => new TagModel(tag));
      this.totalPage = result.data.totalPages;
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitDeleteTag(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.settingTaskServices.submitDeleteTag(id);
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // Edit row tag
  @action async getEditTagById(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.settingTaskServices.editTagById(id);
      this.name = result.data.tag.name;
      this.id = result.data.tag.id;
      const submitList = result.data.listLv3.concat(result.data.listLv4);
      this.listProcessLv = submitList;
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default SettingTaskModel;
