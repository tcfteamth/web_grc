import { observable, action, computed } from 'mobx';
import loadingStore from '../../contexts/LoadingStore';
import SubmitReAssessmentModel from '../SubmitReAssessmentModel';
import request from '../../services';
import AcceptAssessmentModel, {
  ACCEPT_ASSESSMENT_STATUS,
} from './AcceptReAssessmentModel';
import { ACCEPT_ROADMAP_STATUS } from '../AcceptRoadmapModel';
import { ROLES } from '../../contexts/AuthContext';
import FilterModel from '../FilterModel';
import { config } from '../../utils';
import { yearListDropdownStore } from '../DropdownModel';

class AssessmentChildrenModel {
  @observable owner = '';

  @observable no = '';

  @observable lvl = null;

  @observable lastUpdate = null;

  @observable name = '';

  @observable rejectComment = '';

  @observable id = null;

  @observable status = '';

  @observable remark = '';

  @observable roadMapImage = null;

  @observable depDiv = null;

  @observable divEn = null;

  @observable shiftEn = '';

  @observable submitReAssessment = new SubmitReAssessmentModel();

  @observable readyToSubmit = null;

  @observable overAllRating = '';

  @observable controlByRating = null;

  @observable roadMapType = '';

  @observable enhance = '';

  @observable verifyCompliance = null;

  @observable objectives = null;

  constructor(child) {
    this.owner = child.owner;
    this.no = child.no;
    this.lvl = child.lvl;
    this.lastUpdate = child.lastUpdate;
    this.name = child.name;
    this.rejectComment = child.rejectComment;
    this.id = child.id;
    this.status = child.status;
    this.readyToSubmit = child.readyTosubmit;
    this.overAllRating = child.overAllRating;
    this.controlByRating = child.controlByRating;
    this.remark = child.remark;
    this.roadMapImage = child.roadMapImage;
    this.depDiv = child.depDiv;
    this.divEn = child.divEn;
    this.shiftEn = child.shiftEn;
    this.roadMapType = child.roadMapType;
    this.enhance = child.enhance;
    this.verifyCompliance = child.verifyCompliance;
    this.objectives = child.objectives;
    this.latestYear = child.latestYear;
    this.roadmap = child.roadmap;
    this.indicator = child.indicator;
  }


  @action onSelectedSubmitReAssessment() {

   
      this.submitReAssessment.setAssessmentIds(this.id);
    
  }

  @computed get isCreateRoadmapDisabled() {
    return this.list.every((assessmentList) =>
    assessmentList.children.every((child) => !child.submitRoadmaps.roadmapIds),
    );
  }


  @computed get rateStatus() {
    const status = this.overAllRating.split('-');

    return status;
  }
}

class AssessmentModel {
  @observable no = '';

  @observable lvl = null;

  @observable children = [];

  @observable name = '';

  constructor(assessment) {
    this.no = assessment.no;
    this.lvl = assessment.lvl;
    this.children = assessment.children.map(
      (child) => new AssessmentChildrenModel(child),
    );
    this.name = assessment.name;
  }

  @action setRejectAllLevel4() {
    if (!this.isAllLevel4Reject) {
      this.children.forEach((child) => {
        if (child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.reject) {
          child.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.reject,
            child.id,
          );
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptAssessment.setNo('', '');
      });
    }
  }

  @action setAcceptAllLevel4() {
    if (!this.isAllLevel4Accept) {
      this.children.forEach((child) => {
        if (child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.accept) {
          child.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.accept,
            child.id,
          );
        }
      });
    } else {
      this.children.forEach((child) => {
        child.acceptAssessment.setNo('', '');
      });
    }
  }

  @computed get isAllLevel4Accept() {
    const isChecked = this.children.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.accept,
    );

    return isChecked;
  }

  @computed get isAllLevel4Reject() {
    const isChecked = this.children.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.reject,
    );

    return isChecked;
  }


  @computed get getAssessmentIds() {
    const assessmentIdsData = [];
    this.children.forEach((child) => {
      if (child.submitReAssessment.assessmentIds) {
        assessmentIdsData.push(child.submitReAssessment.assessmentIds);
      }
    });

    return assessmentIdsData;
  }


   isSubmitAssessmentCheckedAll() {
    let allCheckedCount = 0,
      allCanCheckCount = 0;
    this.children.forEach((child) => {
      // if (child.canCheck(role)) {
      // if (child.readyToSubmit) {
        allCanCheckCount += 1;
        if (child.submitReAssessment.isSelected) {
          allCheckedCount += 1;
        }
      // }
    });
    console.log(allCanCheckCount, allCheckedCount);
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllSubmitReAssessment(role, forceCheck) {
    this.children.forEach((child) => {
      child.onSelectedSubmitReAssessment(role, forceCheck);
    });
  }

  isAcceptAssessmentCheckedAll(role, status) {
    let allCheckedCount = 0,
      allCanCheckCount = 0;
    this.children.forEach((child) => {
      allCanCheckCount += 1;
      if (
        status === ACCEPT_ASSESSMENT_STATUS.accept &&
        child.acceptAssessment.isSelectedAccept()
      ) {
        allCheckedCount += 1;
      } else if (
        status === ACCEPT_ASSESSMENT_STATUS.reject &&
        child.acceptAssessment.isSelectedReject()
      ) {
        allCheckedCount += 1;
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllAcceptAssessment(role, status, forceCheck) {
    this.children.forEach((child) => {
      child.onSelectedAcceptAssessment(role, status, forceCheck);
    });
  }

  @action setCheckAllAssessment2() {
    if (!this.isAllAssessmentChecked) {
      this.children.forEach((child) => {
        if (!child.submitAssessment.roadmapIds) {
          child.submitAssessment.setRoadmapIds(child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.submitAssessment.setRoadmapIds(null);
      });
    }
  }

  @computed get isAllAssessmentChecked() {
    const isChecked = this.children.every(
      (child) => child.submitAssessment.roadmapIds,
    );

    return isChecked;
  }

  @action setCheckAllReadyToSubmit() {

    if (!this.isCheckAllReadyToSubmit) {
      this.children.forEach((child) => {
        if (!child.submitReAssessment.assessmentIds) {
          child.submitReAssessment.setAssessmentIds(child.id);
        }
      });
    } else {
      this.children.forEach((child) => {
        child.submitReAssessment.setAssessmentIds(null);
      });
    }
  }

  @computed get isCheckAllReadyToSubmit() {
    // const filteredChildren = this.children.filter(
    //   (child) => child.readyToSubmit,
    // );

    // if (filteredChildren.length === 0) {
    //   return false;
    // }
    // return filteredChildren.every((i) => i.submitReAssessment.isSelected);

    return this.children.every((i) => i.submitReAssessment.isSelected);
  }

  @computed get isOneOfAssessmentCanCheck() {
    // const can = this.children.some((child) => child.readyToSubmit);

    return true;
  }

}

class ReAssessmentListModel {
  @observable assessmentList = [];

  @observable size = 10;

  @observable totalPage = 1;

  @observable totalContent = 1;

  @observable page = 1;

  @observable searchAssessmentText = '';

  @observable filterModel = new FilterModel();

  @observable departmentId = null;

  @action async getReAssessmentList(
    token,
    page = 1,
    searchValue = '',
    lvl3 = '',
    lvl4 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = ''
  ) {
    try {
      loadingStore.setIsLoading(true);
      const yearResult = await yearListDropdownStore.getYearListFillter(token);

      const defaultYear = yearResult[1].value;

      const result = await request.reassessmentServices.getReAssessmentList(
        token,
        page,
        searchValue,
        lvl3,
        lvl4,
        bu,
        departmentNo,
        divisionNo,
        shiftNo
      );

      this.assessmentList = result.data.assessments.map(
        (assessment) => new AssessmentModel(assessment),
      );

      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.page = result.data.page;
      this.departmentId = result.data.depaertmentId;

      loadingStore.setIsLoading(false);
    } catch (e) {
      loadingStore.setIsLoading(false);

      if (e.response.status === 401) {
        window.location.replace(config.baseUrlLogin);
      }
    }
  }

  @computed get isSelectedDepartmentId() {
    if (this.departmentId || this.filterModel.selectedDepartment) {
      return false;
    }

    return true;
  }
  @computed get acceptReAssessmentList() {
    const acceptReAssessmenList = [];
    this.assessmentList.forEach((assessment) => {
      assessment.children.forEach((assessmentLv4) => {
      if (assessmentLv4.submitReAssessment.isSelected) {
        acceptReAssessmenList.push(assessmentLv4.submitReAssessment.assessmentIds);
      }
    });
    });

    return acceptReAssessmenList;
  }

  @action async getAssessmentApprovementWithSearch(token) {
    try {
      const result = await this.getReAssessmentList(
        token,
        1,
        this.searchAssessmentText,
        this.filterModel.selectedLvl3,
        this.filterModel.selectedLvl4,
        this.filterModel.selectedBu,
        this.filterModel.selectedDepartment.no,
        this.filterModel.selectedDivision,
        this.filterModel.selectedShift
      );
    } catch (e) {
      console.log(e);
    }
  }

  @action setCheckAcceptAll() {
    if (!this.isAllAcceptChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (
            child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.accept
          ) {
            child.acceptAssessment.setNo(
              ACCEPT_ASSESSMENT_STATUS.accept,
              child.no,
            );
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.acceptAssessment.setNo('', '');
        });
      });
    }
  }

  @action setCheckRejecetAll() {
    if (!this.isAllRejectChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (
            child.acceptAssessment.status !== ACCEPT_ASSESSMENT_STATUS.reject
          ) {
            child.acceptAssessment.setNo(
              ACCEPT_ASSESSMENT_STATUS.reject,
              child.no,
            );
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.acceptAssessment.setNo('', '');
        });
      });
    }
  }

  @computed get isOneOfAssessmentReadyToSubmit() {
    const isReady = this.assessmentList.every(
      (assessment) => assessment.isAllLevel3ReadyToSubmit,
    );

    return isReady;
  }

  @computed get isOneOfAssessmentChecked() {
    const isChecked = this.assessmentList.every((assessment) =>
      assessment.children.every((child) => !child.submitAssessment.roadmapIds),
    );

    return isChecked;
  }

  @computed get isAllRejectChecked() {
    const isChecked = this.assessmentList.every(
      (assessment) => assessment.isAllLevel4Reject,
    );

    return isChecked;
  }

  @computed get isAllAcceptChecked() {
    const isChecked = this.assessmentList.every(
      (assessment) => assessment.isAllLevel4Accept,
    );

    return isChecked;
  }

  isSubmitAssessmentCheckedAll(role) {
    let allCheckedCount = 0;
    let allCanCheckCount = 0;

    this.assessmentList.forEach((assessment) => {
      if (assessment.isOneOfAssessmentCanCheck) {
        allCanCheckCount += 1;
        if (assessment.isSubmitAssessmentCheckedAll(role)) {
          allCheckedCount += 1;
        }
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @computed get isCheckedAll() {
    // const checked = this.assessmentList.map((assessment) =>
    //   assessment.children.filter((child) => child.readyToSubmit),
    // );

    // const checkedFalt = checked.flat();

    // const isAll = checkedFalt.every((i) => i.submitReAssessment.isSelected);

    const checked = this.assessmentList;
    const checkedFalt = checked.flat();
    const isAll = checkedFalt.every((i) => i.children.every((child) => child.submitReAssessment.isSelected));
    return isAll;
  }

  @computed get isCheckedForNext() {
    // const checked = this.assessmentList.map((assessment) =>
    //   assessment.children.filter((child) => child.readyToSubmit),
    // );

    // const checkedFalt = checked.flat();
    // const isAll = checkedFalt.some((i) => i.submitReAssessment.isSelected);

        const checked = this.assessmentList;

    const checkedFalt = checked.flat();
    const isAll = checkedFalt.every((i) => i.children.some((child) => child.submitReAssessment.isSelected));

    return isAll;
  }

  @action setCheckAllSubmitAssessment() {
    if (!this.isCheckedAll) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          // if (!child.submitReAssessment.assessmentIds && child.readyToSubmit) {
          if (!child.submitReAssessment.assessmentIds ) {
            child.submitReAssessment.setAssessmentIds(child.id);
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.submitReAssessment.setAssessmentIds(null);
        });
      });
    }
  }

  isAcceptAssessmentCheckedAll(role, status) {
    let allCheckedCount = 0;
    let allCanCheckCount = 0;

    this.assessmentList.forEach((assessment) => {
      allCanCheckCount += 1;
      if (assessment.isAcceptAssessmentCheckedAll(role, status)) {
        allCheckedCount += 1;
      }
    });
    return allCanCheckCount === 0
      ? false
      : allCheckedCount === allCanCheckCount;
  }

  @action setCheckAllAcceptAssessment(role, status, forceCheck) {
    this.assessmentList.forEach((assessment) =>
      assessment.setCheckAllAcceptAssessment(role, status, forceCheck),
    );
  }

  @computed get isAllAssessmentChecked() {
    const isChecked = this.assessmentList.every(
      (children) => children.isAllAssessmentChecked,
    );

    return isChecked;
  }

  @action setCheckAllAssessment() {
    if (!this.isAllAssessmentChecked) {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          if (!child.submitAssessment.roadmapIds) {
            child.submitAssessment.setRoadmapIds(child.id);
          }
        });
      });
    } else {
      this.assessmentList.forEach((assessment) => {
        assessment.children.forEach((child) => {
          child.submitAssessment.setRoadmapIds(null);
        });
      });
    }
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action async createReAssessment(token) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.reassessmentServices.createReAssessment(
        token,
        this.acceptReAssessmentList,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }
}

export default ReAssessmentListModel;
