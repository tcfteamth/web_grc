import { action, observable, computed } from 'mobx';

export const ACCEPT_ASSESSMENT_STATUS = {
  accept: 'VP_ACCEPT',
  reject: 'VP_REJECT',
  waitVpAccept: 'WAIT_VP_ACCEPT',
};

class AcceptAssessmentModel {
  @observable status = '';

  @observable id = null;

  @observable rejectComment = '';

  isSelectedAccept() {
    return this.no !== '' && this.status === ACCEPT_ASSESSMENT_STATUS.accept;
  }

  isSelectedReject() {
    return this.no !== '' && this.status === ACCEPT_ASSESSMENT_STATUS.reject;
  }

  // @action setNo(status, id, forceCheck) {
  //   if (forceCheck !== undefined) {
  //     this.id = forceCheck ? id : null;
  //     this.status = forceCheck ? status : '';
  //   } else {
  //     if (this.status === status) {
  //       this.id = null;
  //       this.status = '';
  //     } else {
  //       this.id = id;
  //       this.status = status;
  //       this.rejectComment = '';
  //     }
  //   }
  // }

  @action setNo(status, id) {
    if (this.status === status) {
      this.id = null;
      this.status = '';
    } else {
      this.id = id;
      this.status = status;
      this.rejectComment = '';
    }
  }

  @action setRejectComment(text) {
    this.rejectComment = text;
  }
}

export default AcceptAssessmentModel;
