import { action, observable } from 'mobx';
import SummaryReportModel from './SummaryReportModel';
import request from '../../services';

class SummaryReportListModel {
  @observable summaryReportList;

  @action async getSummaryReportList(
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    startDueDate,
    endDueDate,
    startCloseDate,
    endCloseDate,
    status,
    processAnd = [],
    processOr = [],
    objectives = []
  ) {
    try {

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives.map(x => x.value).join(',')

      const result = await request.followUpReportServices.getFollowUpReportSummary(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        startDueDate,
        endDueDate,
        startCloseDate,
        endCloseDate,
        status,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.summaryReportList = result.data.map(
        (summary) => new SummaryReportModel(summary),
      );
    } catch (error) {
      console.log(error);
    }
  }
}

export default SummaryReportListModel;
