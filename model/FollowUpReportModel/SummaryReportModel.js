import { computed, observable } from 'mobx';
import DTOModel from './DTOModel';

class SummaryReportModel {
  @observable name;

  @observable correctiveDTOS;

  @observable enhancementDTOS;

  constructor(summary) {
    this.name = summary.name;
    this.correctiveDTOS = summary.correctiveDTOS.map(
      (corrective) => new DTOModel(corrective),
    );
    this.enhancementDTOS = summary.enhanceDTOS.map(
      (enhance) => new DTOModel(enhance),
    );
  }

  @computed get newCorrective() {
    const newEnhance = [];
    const chunk = 3; // แบ่งทีละ 3

    for (let i = 0, j = this.correctiveDTOS.length; i < j; i += chunk) {
      const chunkedArray = this.correctiveDTOS.slice(i, i + chunk);
      newEnhance.push(chunkedArray);
    }

    return newEnhance;
  }

  @computed get newEnhancement() {
    const newEnhance = [];
    const chunk = 3; // แบ่งทีละ 3

    for (let i = 0, j = this.enhancementDTOS.length; i < j; i += chunk) {
      const chunkedArray = this.enhancementDTOS.slice(i, i + chunk);
      newEnhance.push(chunkedArray);
    }

    return newEnhance;
  }
}

export default SummaryReportModel;
