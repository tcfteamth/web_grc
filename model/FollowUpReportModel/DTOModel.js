import { computed, observable } from 'mobx';
import DetailDTOModel from './DetailDTOModel';

class DTOModel {
  @observable lv5Type;

  @observable detailDTOModel;

  constructor(dto) {
    this.lv5Type = dto.lv5Type;
    this.detailDTOModel =
      dto.detailCorrectiveDTOS?.map((detail) => new DetailDTOModel(detail)) ??
      dto.detailEnhanceDTOS?.map((detail) => new DetailDTOModel(detail));
  }

  @computed get newDetailDTO() {
    const newEnhance = [];
    const chunk = 3; // แบ่งทีละ 3

    if (this.detailDTOModel.length > 0) {
      for (let i = 0, j = this.detailDTOModel.length; i < j; i += chunk) {
        const chunkedArray = this.detailDTOModel.slice(i, i + chunk);
        newEnhance.push(chunkedArray);
      }
    }

    return newEnhance;
  }
}

export default DTOModel;
