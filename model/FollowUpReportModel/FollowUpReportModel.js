import { action, observable } from 'mobx';
import DashboardModel from '../DashboardFollowUpModel/DashboardModel';
import loadingStore from '../../contexts/LoadingStore';
import request from '../../services';

class FollowUpReportModel {
  @observable reportGraphs;

  @action async getReportGraphs(
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    startDueDate,
    endDueDate,
    startCloseDate,
    endCloseDate,
    status,
    processAnd = [],
    processOr = [],
    objectives
  ) {
    try {
      loadingStore.setIsLoading(true);

      const processAndStr = processAnd.map(x => x.value).join(',')
      const processOrStr = processOr.map(x => x.value).join(',')
      const objectivesStr = objectives ? objectives.value : ''

      const result = await request.followUpReportServices.getFollowUpReportGraph(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        startDueDate,
        endDueDate,
        startCloseDate,
        endCloseDate,
        status,
        processAndStr,
        processOrStr,
        objectivesStr
      );

      this.reportGraphs = result.data.map((graph) => new DashboardModel(graph));
      loadingStore.setIsLoading(false);
    } catch (error) {
      loadingStore.setIsLoading(false);

      console.log(error);
    }
  }

  @action async exportFollowUpReport(
    token,
    year,
    bu,
    departmentNo,
    divisionNo,
    status,
  ) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.followUpReportServices.exportFollowUpReport(
        token,
        year,
        bu,
        departmentNo,
        divisionNo,
        status,
      );
      loadingStore.setIsLoading(false);

      return result;
    } catch (error) {
      loadingStore.setIsLoading(false);

      conosole.log(error);
    }
  }
}

export default FollowUpReportModel;
