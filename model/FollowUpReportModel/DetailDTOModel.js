import { action, observable } from 'mobx';

class DetailDTOModel {
  @observable indicator;

  @observable lv4No;

  @observable lv4Name;

  @observable lv6Name;

  @observable lv7Name;

  @observable initialRemark;

  @observable suggestion;

  @observable progress;

  @observable benefit;

  @observable benefitDetail;

  @observable partner;

  @observable partnerDivEn;

  @observable partnerShiftEn;

  @observable endDate;

  @observable dueDate;

  @observable status;

  @observable divEn;

  @observable shiftEn = '';

  constructor(detail) {
    this.indicator = detail.indicator;
    this.divEn = detail.divEn;
    this.shiftEn = detail.shiftEn;
    this.lv4No = detail.lv4No;
    this.lv4Name = detail.lv4Name;
    this.lv6Name = detail.lv6Name;
    this.lv7Name = detail.lv7Name;
    this.initialRemark = detail.initialRemark;
    this.suggestion = detail.suggestion;
    this.progress = detail.progress;
    this.benefit = detail.benefit;
    this.benefitDetail = detail.benefitDetail;
    this.partner = detail.partner;
    this.partnerDivEn = detail.partnerDivEn;
    this.partnerShiftEn = detail.partnerShiftEn;
    this.endDate = detail.endDate;
    this.dueDate = detail.dueDate;
    this.status = detail.status;
  }
}

export default DetailDTOModel;
