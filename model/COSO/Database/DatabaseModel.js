import { observable, action, computed } from 'mobx';
import request from '../../../services';
import loadingStore from '../../../contexts/LoadingStore';

class DatabaseLv3Model {
  @observable area = '';

  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable children = [];

  @observable order = '';

  constructor(database) {
    this.area = database.area;
    this.no = database.no;
    this.lvl = database.lvl;
    this.name = database.name;
  }
}

class DatabaseLv2Model {
  @observable area = '';

  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable children = [];

  @observable order = '';

  constructor(database) {
    this.area = database.area;
    this.no = database.no;
    this.lvl = database.lvl;
    this.children =
      database.children &&
      database.children.map((child) => new DatabaseLv3Model(child));
    this.name = database.name;
  }
}

class DatabaseLv1Model {
  @observable area = '';

  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable children = [];

  constructor(database) {
    this.area = database.area;
    this.no = database.no;
    this.lvl = database.lvl;
    this.children =
      database.children &&
      database.children.map((child) => new DatabaseLv2Model(child));
    this.name = database.name;
  }
}

class DatabaseListModel {
  @observable cosoDB = [];

  @observable searchDatabaseText = '';

  @action async getCOSODatabaseList() {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.databaseCosoServices.getCosoDBList(
        this.searchDatabaseText,
      );
      this.cosoDB = result.data.cosoDB.map(
        (database) => new DatabaseLv1Model(database),
      );

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // get data database
  @action async exportCOSODatabase(token) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.databaseCosoServices.exportExcelDatabase(
        token,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setSearchDatabaseText(text) {
    this.searchDatabaseText = text;
  }
}

export default DatabaseListModel;
