import { observable, action, computed } from 'mobx';
import request from '../../../services';
import AcceptRoadmapsModel, {
  ACCEPT_ROADMAP_STATUS,
} from '../../AcceptRoadmapModel';
import SubmitRoadmapsModel from '../../SubmitRoadmapsModel';
import loadingStore from '../../../contexts/LoadingStore';
import { assesorDropdownStore } from '../../index';
import FilterModel from '../../FilterModel';

class RoadmapLv3Model {
  @observable area = '';

  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable order = '';

  constructor(database) {
    this.area = database.area;
    this.no = database.no;
    this.lvl = database.lvl;
    this.name = database.name;
  }
}

class RoadmapModel {
  @observable id = '';

  @observable no = '';

  @observable lvl = 0;

  @observable children = [];

  @observable name = '';

  @observable submitRoadmapId = [];

  @observable submitRoadmaps = new SubmitRoadmapsModel();

  @observable acceptRoadmap = new AcceptRoadmapsModel();

  @observable area = '';

  @observable depDiv = '';

  @observable remark = '';

  @observable name = '';

  @observable year = '';

  @observable depDepNo = '';

  @observable divisionNo = '';
  @observable shiftNo = '';

  @observable rejectComment = '';

  @observable roadmap = '';

  @observable lastYearPath = '';

  @observable hasLastYear = '';

  @observable csaPdcIds = '';

  @observable depId = '';

  @observable csaPdcs = [];

  @observable divEn = '';

  @observable shiftEn = '';

  constructor(data) {
    this.id = data.id;
    this.no = data.no;
    this.lvl = data.lvl;
    this.name = data.name;
    this.remark = data.remark;
    this.status = data.status;
    this.rejectComment = data.rejectComment;
    this.depDiv = data.depDiv;
    this.divEn = data.divEn;
    this.shiftEn = data.shiftEn;
    this.area = data.area;
    this.year = data.year;
    this.depDepNo = data.depDepNo;
    this.divisionNo = data.divisionNo;
    this.shiftNo = data.shiftNo;
    this.roadmap = data.isNewAssessment;
    this.lastYearPath = data.lastYearPath;
    this.hasLastYear = data.hasLastYear;
    this.children =
      data.children && data.children.map((child) => new RoadmapLv3Model(child));
    this.depId = data.depId;
    this.csaPdcs = data.csaPdcs;
  }

  // logic การเก็บ object accept/reject roadmap
  @computed get getAcceptRoadmapList() {
    const acceptRoadmapList = [];
    if (this.acceptRoadmap.status) {
      acceptRoadmapList.push(this.acceptRoadmap);
    }

    return acceptRoadmapList;
  }

  // logic ในการเก็บค่า roadmap id
  @computed get getRoadmapIds() {
    const roadmapIdsData = [];
    if (this.submitRoadmaps.roadmapIds) {
      roadmapIdsData.push(this.submitRoadmaps.roadmapIds);
    }

    return roadmapIdsData;
  }

  // delete row roadmap
  @action async submitDeleteRoadmap(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.roadmapCosoServices.submitDeleteRoadmap(id);
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // Edit row roadmap
  @action async editRoadmapById(id) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.roadmapCosoServices.getEditRoadmapById(id);
      this.list = new RoadmapModel(result.data);
      this.csaPdcIds = result.data.csaPdcIds;

      if (result.data.csaPdcs != null) {
        this.csaPdcs = result.data.csaPdcs.map((c) => {
          return {
            csaPdcId: c.csaPdcId,
            label: c.name,
            name: c.name,
            value: c.csaPdcId,
          };
        });
      }

      this.divisionNo = result.data.divisionNo;
      this.shiftNo = result.data.shiftNo;
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

class RoadmapCOSOListModel {
  @observable list = [];

  @observable page = 1;

  @observable totalPage = 0;

  @observable searchRoadmapText = '';

  @observable selectedRoadMapList = [];

  @observable subProcessList = [];

  @observable filterModel = new FilterModel();

  @action setField(field, value) {
    this[field] = value;
  }

  @action setFieldList(field, value) {
    this[field].push(value);
  }

  // @observable listEdit = [];

  // get data roadMap
  @action async getRoadmapCOSOList(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = new Date().getFullYear(),
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.roadmapCosoServices.getRoadmapCOSOList(
        token,
        page,
        searchValue,
        lvl2,
        bu,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        area,
        year,
      );
      const defaultAssesor = assesorDropdownStore.getFirstOption;

      this.list = result.data.roadmaps.map(
        (item) => new RoadmapModel(item, defaultAssesor),
      );
      this.page = result.data.page;
      this.totalPage = result.data.totalPage;
      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async updateRoadmapCOSODetail(
    token,
    id,
    year,
    divisionId,
    noLvl2,
    remark,
  ) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.roadmapCosoServices.updateRoadmapCoso(
        token,
        id,
        year,
        divisionId,
        noLvl2,
        remark,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // ใช้ในการโยน roadmap จาก admin ไปให้ dm
  @action async submitRoadmapsCosoToDM(token) {
    loadingStore.setIsLoading(true);
    try {
      const submitRoadmapIds = this.list.reduce(
        (roadmapIdsData, roadmap) => [
          ...roadmapIdsData,
          ...roadmap.getRoadmapIds,
        ],
        [],
      );
      const result = await request.roadmapCosoServices.submitRoadmaps(
        token,
        submitRoadmapIds,
      );
      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // ใช้ในการโยน roadmap จาก DM ไปให้ staff
  @action async submitAcceptCosoRoadmaps(token) {
    loadingStore.setIsLoading(true);
    try {
      const acceptRoadmapList = this.list.reduce(
        (acceptList, roadmap) => [
          ...acceptList,
          ...roadmap.getAcceptRoadmapList,
        ],
        [],
      );

      const result = await request.roadmapCosoServices.acceptRoadmaps(
        token,
        acceptRoadmapList,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  // checkedAll on click Admin
  @action checkedAll() {
    if (!this.isChckedAll) {
      this.list.forEach((roadmapModel) => {
        if (!roadmapModel.submitRoadmaps.roadmapIds) {
          roadmapModel.submitRoadmaps.setRoadmapIds(roadmapModel.id);
        }
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.submitRoadmaps.setRoadmapIds(null);
      });
    }
  }

  // checkedAll Accept on click DM
  @action setAllAcceptChecked() {
    if (!this.isAllAcceptChecked) {
      this.list.forEach((roadmapModel) => {
        if (
          roadmapModel.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.accept
        ) {
          roadmapModel.acceptRoadmap.setNo(
            ACCEPT_ROADMAP_STATUS.accept,
            roadmapModel.id,
          );
        }
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.acceptRoadmap.setNo('', '');
      });
    }
  }

  // checkedAll Reject on click DM
  @action setAllRejectChecked() {
    if (!this.isAllRejectChecked) {
      this.list.forEach((roadmapModel) => {
        if (
          roadmapModel.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.reject
        ) {
          roadmapModel.acceptRoadmap.setNo(
            ACCEPT_ROADMAP_STATUS.reject,
            roadmapModel.id,
          );
        }
      });
    } else {
      this.list.forEach((roadmapModel) => {
        roadmapModel.acceptRoadmap.setNo('', '');
      });
    }
  }

  // get data roadMap
  @action async exportCOSOPageRoadmap(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = '',
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.roadmapCosoServices.exportCosoRoadmap(
        token,
        page,
        searchValue,
        lvl2,
        bu,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        area,
        year,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  // checkedAll on click Admin
  @computed get isChckedAll() {
    const isChecked = this.list.every(
      (roadmapModel) => roadmapModel.submitRoadmaps.roadmapIds,
    );

    return isChecked;
  }

  // checkedAll Accept on click DM
  @computed get isAllAcceptChecked() {
    const isChecked = this.list.every(
      (child) => child.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.accept,
    );
    return isChecked;
  }

  // checkedAll Reject on click DM
  @computed get isAllRejectChecked() {
    const isChecked = this.list.every(
      (child) => child.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.reject,
    );
    return isChecked;
  }

  // check btn submit admin to admin
  @computed get isSubmitToDmDisabled() {
    return this.list.every(
      (roadmapModel) => !roadmapModel.submitRoadmaps.roadmapIds,
    );
  }

  // check btn submit admin to admin
  @computed get isCheckAssessorDisabled() {
    const isCheckAssessor = this.list.every(
      (roadmapModel) => roadmapModel.acceptRoadmap.userId === 0,
    );
    return isCheckAssessor;
  }

  // check btn submit accept dm
  @computed get isAcceptOrRejectDisabled() {
    // check if user check accept/reject or not
    const isAccept = this.list.every(
      (roadmapModel) =>
        roadmapModel.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.accept,
    );

    const isReject = this.list.every(
      (roadmapModel) =>
        roadmapModel.acceptRoadmap.status !== ACCEPT_ROADMAP_STATUS.reject,
    );

    // check if reject roadmap set comment or not
    const iRoadmapCheckKey = this.list
      .filter(
        (roadmapModel) =>
          roadmapModel.acceptRoadmap.status === ACCEPT_ROADMAP_STATUS.reject,
      )
      .flat()
      .every((child) => child.acceptRoadmap.rejectComment);

    if (isReject && isAccept) {
      return true;
    }

    if (!iRoadmapCheckKey) {
      return true;
    }

    return false;
  }

  //  key search
  @action setSearchRoadmapText(text) {
    this.searchRoadmapText = text;
  }
}

export default RoadmapCOSOListModel;
