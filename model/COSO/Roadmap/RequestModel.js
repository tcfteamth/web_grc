import { observable, action, computed } from 'mobx';
import loadingStore from '../../../contexts/LoadingStore';
import request from '../../../services';

class RequestDetailModel {
  @observable id = 0;

  @observable detail = '';

  @observable firstName = '';

  @observable lastName = '';

  @observable deptDiv = '';

  @observable createdDate = null;

  constructor(requestItem) {
    this.id = requestItem.id;
    this.detail = requestItem.detail;
    this.firstName = requestItem.firstName;
    this.lastName = requestItem.lastName;
    this.deptDiv = requestItem.deptDiv;
    this.createdDate = requestItem.createdDate;
  }
}

class RequestModel {
  @observable requests = [];

  @observable size = 0;

  @observable totalPage = 0;

  @observable totalContent = 0;

  @observable page = 0;

  @observable newRequest = '';

  @action setField(field, value) {
    this[field] = value;
  }

  @action async getRequestList(token, page, searchRequestValue) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.roadmapServices.getRequestList(
        token,
        page,
        searchRequestValue,
      );

      this.requests = result.data.requests.map(
        (requestItem) => new RequestDetailModel(requestItem),
      );
      this.size = result.data.size;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.page = result.data.page;

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitNewRequest(token, newRequest) {
    try {
      loadingStore.setIsLoading(true);

      const formData = new FormData();
      formData.append('text', newRequest);

      const result = request.roadmapServices.submitRequest(token, formData);

      loadingStore.setIsLoading(false);

      return result;
    } catch (e) {
      console.log(e);
    }
  }
}

export default RequestModel;
