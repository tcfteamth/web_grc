import { observable, action, computed } from 'mobx';
import request from '../../../services';
import loadingStore from '../../../contexts/LoadingStore';
import ApproveAssessmentCOSOModel from './ApproveAssessmentCOSOModel';
import AttachFileModel from './AttachFileModel';
// import  from '../../AssessmentModel/Vli';
import { ValidateCOSODetailFormModel } from '../../AssessmentModel/ValidateFormModel';

class CommentModel {
  @observable comment = '';

  @observable commentBy = '';

  @observable commentFrom = '';

  @observable type = '';

  @observable ids = '';

  constructor(objectItem) {
    this.comment = objectItem.comment;
    this.commentBy = objectItem.commentBy;
    this.commentFrom = objectItem.commentFrom;
    this.ids = objectItem.id;
  }
}

class FilesModel {
  @observable fullPath = '';

  @observable id = null;

  @observable title = '';

  constructor(item) {
    this.title = item.title;
    this.fullPath = item.filePath;
    this.id = item.id;
  }
}

class DocumentStorageModel {
  @observable fileId = null;

  @observable fileSize = null;

  @observable title = '';

  @observable filePath = '';

  @observable createdDate = null;

  @observable createdBy = null;

  @observable canDelete = true;

  constructor(item) {
    this.fileId = item.id;
    this.fileSize = item.fileSize;
    this.title = item.title;
    this.filePath = item.filePath;
    this.createdDate = item.createdDate;
    this.createdBy = item.createdBy;
    this.canDelete = item.canDelete;
  }
  @computed get humanFileSize() {
    var bytes = this.fileSize
    var dp = 1
    const thresh = 1024;
  
    if (Math.abs(bytes) < thresh) {
      return bytes + 'B';
    }
  
    const units = ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] ;
    let u = -1;
    const r = 10**dp;
  
    do {
      bytes /= thresh;
      ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);
  
  
    return bytes.toFixed(dp) + units[u];
  }
}


class ObjectsModel {
  @observable name = '';

  @observable no = '';

  @observable area = '';

  @observable fullPath = '';

  @observable id = null;

  @observable parent = '';

  @observable lvl = null;

  @observable type = '';

  constructor(objectItem) {
    this.name = objectItem.name;
    this.no = objectItem.no;
    this.area = objectItem.area;
    this.fullPath = objectItem.fullPath;
    this.id = objectItem.id;
    this.parent = objectItem.parent;
    this.lvl = objectItem.lvl;
    this.type = objectItem.type;
  }
}
class AssessmentDetailModel {
  // @observable button = [];

  @observable buttonBar = [];

  @observable no = '';

  @observable createBy = '';

  @observable canUpdate = false;

  @observable area = '';

  @observable name = '';

  @observable id = null;

  @observable status = '';

  @observable createDate = null;

  @observable isReadyToSubmit = false;

  @observable updateDate = null;

  @observable approach = '';

  @observable deploy = '';

  @observable newInitiative = '';

  @observable info = '';

  @observable comment = '';

  @observable lastEditBy = '';

  @observable approveStatus = new ApproveAssessmentCOSOModel();

  @observable attachFile = new AttachFileModel();

  @observable validateCOSO = new ValidateCOSODetailFormModel();

  @observable fullPath = '';

  @observable filePath = [];

  @observable filePathMaster = []; //Use to detect change on file by comparing with original value

  @observable files = [];

  @observable tempFiles = []; //Temporary state to handle file selection in document storage modal

  @observable documentStorageFiles = [];

  @observable titles = [];

  @observable documentList = [];

  @observable objects = [];

  @observable commentList = [];

  @observable tab = 'TODO'; // TODO/SUMMARY

  @observable role = '';

  @observable assessmentFileIds = [];

  @observable assessmentFileFull = [];

  @observable checkApproach = true;

  @observable checkDeploy = true;

  @observable checkNewInitiative = true;

  @action async getAssessmentComment(token, assesmentId) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentCosoServices.getAssessmentComment(
        token,
        assesmentId,
      );

      // return result;

      if (result.data.length !== 0) {
        this.commentList = result.data.map(
          (objectItem) => new CommentModel(objectItem),
        );
      }
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getAssessmentDetail(token, assesmentId, tab = 'TODO') {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentCosoServices.getAssessmentCosoDetail(
        token,
        assesmentId,
        tab,
      );

      this.id = result.data.id;
      this.no = result.data.no;
      this.name = result.data.name;
      this.info = result.data.info;
      this.createBy = result.data.createBy;
      this.createDate = result.data.createDate;
      this.area = result.data.area;
      this.status = result.data.status;
      this.isReadyToSubmit = result.data.isReadyToSubmit;
      this.canUpdate = result.data.canUpdate;
      this.buttonBar = result.data.buttonBar;
      this.updateDate = result.data.updateDate;
      this.approach = result.data.approach;
      this.deploy = result.data.deploy;
      this.newInitiative = result.data.newInitiative;
      this.comment = result.data.comment;
      this.filePathMaster = result.data.files.map((file) => file) || [];
      this.filePath = result.data.files.map((file) => file) || [];
      this.lastEditBy = result.data.lastEditBy;
      console.log('result.data.files', result.data.files);
      // if (result.data.files.length !== 0) {
      //   this.files = result.data.files.map((item) => new FilesModel(item));
      // }

      if (result.data.objects.length !== 0) {
        this.objects = result.data.objects.map(
          (objectItem) => new ObjectsModel(objectItem),
        );
      }

      const storageResult = await request.assessmentCosoServices.getDocumentStorage(token,this.info.divisionNo);
      console.log('Get Files in document storage')

      if (storageResult.data.length !== 0) {
        this.documentStorageFiles = storageResult.data.map(
          (storageItem) => new DocumentStorageModel(storageItem),
        );
      }

      console.log(this.documentStorageFiles)

      this.approveStatus.status =
        result.data.status === 'Minor Change'
          ? 'MINOR_CHANGE'
          : result.data.status === 'Major Change'
          ? 'MAJOR_CHANGE'
          : result.data.status === 'Completed'
          ? APPROVE_ASSESSMENT_READY.accept
          : null;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action setValidateAssessmentForm() {
    this.validateCOSO.setField('isApproach', this.isApproach);
    this.validateCOSO.setField('isEvidence', this.isEvidence);
    this.validateCOSO.setField('isDeploy', this.isDeploy);
    this.validateCOSO.setField('isNewInitiative', this.isNewInitiative);
  }

  @action setField(field, value) {
    this[field] = value;
  }

  @action async uploadFiles(token, assessmentId, fileId) {
    try {
      const formData = new FormData();
      formData.append('assessmentId', assessmentId);
      formData.append('fileId', fileId);

      // Display the key/value pairs
      for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
      }

      const result = await request.assessmentCosoServices.uploadAssessmentFiles(
        token,
        formData,
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async uploadFilesToStorage(token, title, newFile) {
    try {
      console.log(newFile)
      const formData = new FormData();
      formData.append('title', title);
      if (newFile) {
        var filename = newFile.name;
        var fileExt = filename.split('.')[filename.split('.').length-1]
        const newFilename = `${title}.${fileExt}`

        formData.append('file', newFile, newFilename);
      }

      // Display the key/value pairs
      for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
      }

      const result = await request.assessmentCosoServices.addDocumentStorageFile(
        token,
        formData,
        this.info.divisionNo
      );

      console.log('Upload file to Document Storage + Update Strage by response')

      if (result.data.length !== 0) {
        this.documentStorageFiles = result.data.map(
          (storageItem) => new DocumentStorageModel(storageItem),
        );
      }

      // return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async deleteFilesFromStorage(token,id) {
    try {
      const result = await request.assessmentCosoServices.deleteDocumentStorageFile(
        token,
        id,
        this.info.divisionNo
      );

      this.tempFiles = this.tempFiles.filter(
        (file) => file.id !== id,
      );

      this.filePath = this.filePath.filter(
        (file) => file.id !== id,
      );

      console.log(`Delete file id:${id} Document Storage + Update Strage by response`)

      if (result.data.length !== 0) {
        this.documentStorageFiles = result.data.map(
          (storageItem) => new DocumentStorageModel(storageItem),
        );
      }
      else {
        this.documentStorageFiles = []
      }

      // return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async updateAssessmentDetail(token) {
    try {
      loadingStore.setIsLoading(true);

      const assessmentData = {
        readyTosubmit: this.isReadyToSubmit,
        name: this.name,
        comment: this.comment,
        id: this.id,
        no: this.no,
        approach: this.approach,
        deploy: this.deploy,
        newInitiative: this.newInitiative,
        status: this.approveStatus.isSetMajorOrMinorReady,
        icEffective: 1,
        correctiveActionPlan: '',
        expectedComplete: ''
      };
      const result = await request.assessmentCosoServices.updateAssessmentCosoDetail(
        token,
        assessmentData,
      );

      if (this.fileIdsToAdd.length > 0) {
        for (const id of this.fileIdsToAdd) {
          await this.uploadFiles(token, this.id, id);
        }
      }

      if (this.fileIdsToDelete.length > 0) {
        await this.submitDeleteAssessmentFiles(token);
      }

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async cosoRejectToStaff(token) {
    try {
      const result = await request.assessmentCosoServices.rejectToStaff(
        token,
        this.id,
      );

      console.log(this.id);
      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @observable newId = 1;

  @action onAddAttatchFiles(title, files) {
    const newIds = this.newId;
    const newFileId = `${newIds}`;
    if (files) {
      // this.titles.push(title);
      // this.files.push(files);

      this.files.push({
        title,
        file: files,
        filePath: files.preview.url,
        id: newFileId,
        index: newIds,
      });
    } else {
      // this.titles.push(title);
      // this.files.push(files);

      this.files.push({
        title,
        filePath: false,
        id: newFileId,
        index: newIds,
      });
    }
    this.newId += 1;
  }

  @action deleteAssessmentFiles(id, type) {
    if (type === 'display') {
      const filteredAssessmentFiles = this.filePath.filter(
        (file) => file.id !== id,
      );
      const toDeleteAssessmentFile = this.filePath.find(
        (file) => file.id === id,
      );
      // ส่งไอดีของไฟล์ไปลบที่ service
      this.assessmentFileIds.push(id);
      if(toDeleteAssessmentFile.cosoFileMasterId){
        this.assessmentFileFull.push(toDeleteAssessmentFile)
      }

      this.filePath = filteredAssessmentFiles;
    } else {
      this.files = this.files.filter((file) => file.id !== id);
    }
  }

  /* TODO 
  1) Open modal -> init temp files
  2) On file select checkbox (if true: add temp file)(else: remove temp file)
  3) On cancel: clear temp files
  4) On save: save selected files
  5) On saving assessment detail: compare filePathMaster with filePath
    and decide which file should be add or delete from assessment
    5.1) If file present on filePathMaster but not on filePath ==> add ID to delete list
    5.2) If file present on filePath but not on filePathMaster ==> add ID to add list
  */

  @action initTempFiles(){
    console.log('initing temp files')
    this.tempFiles = [...this.filePath]
  }

  @action clearTempFiles(){
    this.tempFiles = [];
  }

  @action addTempFile( item){
    console.log('adding temp file')
    const newItem = {title: item.title, cosoFileMasterId: item.fileId, filePath: item.filePath}
    this.tempFiles.push(newItem)
  }

  @action removeTempFile(id){
    console.log('removing temp file')
    this.tempFiles = this.tempFiles.filter(
      (file) => file.cosoFileMasterId !== id,
    );
  }

  @action saveSelectedFiles(){
    let filePathIds = this.filePath.map(x => x.cosoFileMasterId)
    let tempFilesIds = this.tempFiles.map(x => x.cosoFileMasterId)
    let cosoFileMasterIds = tempFilesIds.filter(value => !filePathIds.includes(value));
    for(let fileMasterId of cosoFileMasterIds){
      const inDeleteList = this.assessmentFileFull.find(x => x.cosoFileMasterId === fileMasterId)
      if(inDeleteList){
        this.assessmentFileIds = this.assessmentFileIds.filter(x => x !== inDeleteList.id)
        this.assessmentFileFull = this.assessmentFileFull.filter(x => x.cosoFileMasterId !== inDeleteList.cosoFileMasterId)
      }
    }

    this.filePath = [...this.tempFiles]
    this.tempFiles = [];
  }

  @action async downloadAllFiles(token) {
    try {
      loadingStore.setIsLoading(true);
      const result = await request.assessmentCosoServices.downloadAllAsZip(token,this.info.divisionNo)
      console.log(result)
      if(result){
        let fileName =  `${this.info.indicator}.zip`;
        let blob = new Blob([result.data]);
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
      }
      loadingStore.setIsLoading(false);
      // return result;
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @computed get displayFiles() {
    const files = [];

    this.filePath.map((file) => files.push({ ...file, filesType: 'display' }));
    this.files.map((file) => files.push(file));

    return files;
  }

  @computed get selectedFileIds() {
    const ids = this.tempFiles.map(x => x.cosoFileMasterId)
    return ids;
  }

  @computed get selectedFileInFormIds() {
    const ids = this.filePath.map(x => x.cosoFileMasterId)
    return ids;
  }

  @computed get fileIdsToDelete(){
    let cosoFileMasterIds = [];
    const currentIds = this.filePath.map(x => x.cosoFileMasterId)
    const masterIds = this.filePathMaster.map(x => x.cosoFileMasterId)
    cosoFileMasterIds = masterIds.filter(value => !currentIds.includes(value));
    let ids = [];
    ids = this.filePathMaster.filter(x => cosoFileMasterIds.includes(x.cosoFileMasterId)).map(x => x.id)
    ids = ids.concat(this.assessmentFileIds).filter((value, index, self) => {
      return self.indexOf(value) === index;
    }).filter(x => x !== null && x !== undefined)
    return ids;
  }

  @computed get fileIdsToAdd(){
    let ids = [];
    const currentIds = this.filePath.map(x => x.cosoFileMasterId)
    const masterIds = this.filePathMaster.map(x => x.cosoFileMasterId)
    ids = currentIds.filter(value => !masterIds.includes(value));
    return ids;
  }

  // @computed get displayFiles() {
  //   const files = [];

  //   this.filePath.map((file) => files.push({ ...file, filesType: 'display' }));
  //   this.files.map((file) => files.push(file));

  //   // ในกรณีที่ ไฟล์ไม่แสดงนามสกุล  (return อาร์เรย์ชุดใหม่กลับไป โดยจะต้องไม่มีค่าซ้ำกัน หมายถึงไม่ให้ id ซ้ำกัน)
  //   return [...new Set(files)];
  //   // หรือ  return files;
  // }

  @action async submitDeleteAssessmentFiles(token) {
    try {
      const ids = this.fileIdsToDelete.join(',');

      const result = await request.assessmentCosoServices.deleteCosoAssessmentFiles(
        token,
        ids,
      );

      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitDeleteComment(token, id) {
    try {
      const result = await request.assessmentCosoServices.deleteCosoComment(
        token,
        id,
      );
      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @action async submitExportAssessment(token, id) {
    try {
      const result = await request.assessmentCosoServices.exportCosoAssessment(
        token,
        id,
      );
      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isApproach() {
    return !!this.approach;
  }

  @computed get isEvidence() {
    return !!this.selectedFileInFormIds.length;
  }

  @computed get isDeploy() {
    return !!this.deploy;
  }

  @computed get isNewInitiative() {
    return !!this.newInitiative;
  }

  @computed get isSetAllData() {
    const isSetApproach = this.approach;
    const isSetDeploy = this.deploy;
    const isSetEvidence = this.filePath.length ;
    // const isSetNewInitiative = this.newInitiative;
    return isSetApproach && isSetDeploy && isSetEvidence;
    // return isSetApproach && isSetDeploy;
  }

  @computed get canCheckReadyToSubmit() {
    const can = this.isSetAllData;
    return can;
  }

  // ไม่ใช้ action หรือ computed เพราะต้องการให้ set ค่าได้และ return ได้ในเวลาเดียวกัน*
  isSetAllForm() {
    if (!this.isSetAllData) {
      this.isReadyToSubmit = false;
    }

    return this.isReadyToSubmit;
  }
}

export default AssessmentDetailModel;
