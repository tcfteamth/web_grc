import { observable, computed, action } from 'mobx';

// Draft Status
export const APPROVE_ASSESSMENT_STATUS = {
  accept: 'CLOSE_DRAFT',
  reject: {
    minor: 'MINOR_CHANGE',
    major: 'MAJOR_CHANGE',
  },
};

// Submit Status
export const APPROVE_ASSESSMENT_READY = {
  accept: 'CLOSE_READY',
  reject: {
    minor: 'MINOR_CHANGE_READY',
    major: 'MAJOR_CHANGE_READY',
  },
};

class ApproveAssessmentModel {
  @observable status = '';

  @observable id = null;

  @observable rejectComment = '';

  @observable isReadyToSubmit = false;

  @computed get isSelectedMajor() {
    return (
      this.id !== '' &&
      (this.status === APPROVE_ASSESSMENT_STATUS.reject.major ||
        this.status === APPROVE_ASSESSMENT_READY.reject.major)
    );
  }

  @computed get isSelectedMinor() {
    return (
      this.id !== '' &&
      (this.status === APPROVE_ASSESSMENT_STATUS.reject.minor ||
        this.status === APPROVE_ASSESSMENT_READY.reject.minor)
    );
  }

  @computed get isSelectAccept() {
    return this.id !== '' && this.status === APPROVE_ASSESSMENT_STATUS.accept;
  }

  @action setId(status, id) {
    if (this.status === status) {
      this.id = null;
      this.status = '';
    } else {
      this.id = id;
      this.status = status;
    }
  }

  @action setRejectComment(text) {
    this.rejectComment = text;
    console.log(text);
  }

  @action setStatus(status) {
    this.status = status;
  }

  @action setIsReadyToSubmit(status) {
    this.isReadyToSubmit = status;
  }

  @computed get isSetMajorOrMinorReady() {
    if (
      this.isReadyToSubmit &&
      this.status === APPROVE_ASSESSMENT_STATUS.reject.major
    ) {
      return APPROVE_ASSESSMENT_READY.reject.major;
    }

    if (
      this.isReadyToSubmit &&
      this.status === APPROVE_ASSESSMENT_STATUS.reject.minor
    ) {
      return APPROVE_ASSESSMENT_READY.reject.minor;
    }

    // ส่ง empty string ไปเพราะหลังบ้านรับเป็น string
    // หลังบ้านจะเอา empty string ไปเปลี่ยน status ให้อีกทีนึง
    return this.status || '';
  }

  @action setSubmitApproveStatus(readyStatus) {
    if (readyStatus && this.status === APPROVE_ASSESSMENT_STATUS.reject.major) {
      this.isReadyToSubmit = readyStatus;
      this.status = APPROVE_ASSESSMENT_READY.reject.major;
    }

    if (readyStatus && this.status === APPROVE_ASSESSMENT_STATUS.reject.minor) {
      this.isReadyToSubmit = readyStatus;
      this.status = APPROVE_ASSESSMENT_READY.reject.minor;
    }
  }
}

export default ApproveAssessmentModel;
