import { observable, action, computed } from 'mobx';
import request from '../../../services';
import loadingStore from '../../../contexts/LoadingStore';
import SubmitRoadmapsModel from '../../SubmitRoadmapsModel';
import FilterModel from '../../FilterModel';

class AssessmentLv1Model {
  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable lastUpdate = '';

  @observable depDiv = '';

  @observable divEn = '';

  @observable shiftEn = '';

  @observable owner = '';

  @observable order = '';

  @observable readyTosubmit = '';

  @observable remark = '';

  @observable status = '';

  @observable id = '';

  @observable summaryStatus = [];

  @observable submitAssessment = new SubmitRoadmapsModel();

  constructor(assessment) {
    this.no = assessment.no;
    this.lvl = assessment.lvl;
    this.name = assessment.name;
    this.lastUpdate = assessment.lastUpdate;
    this.remark = assessment.remark;
    this.status = assessment.status;
    this.id = assessment.id;
    this.owner = assessment.owner;
    this.depDiv = assessment.depDiv;
    this.divEn = assessment.divEn;
    this.shiftEn = assessment.shiftEn;
    this.readyTosubmit = assessment.readyTosubmit;
    this.summaryStatus = assessment.summaryStatus;
  }
}

class AssessmentSummaryCOSOModel {
  @observable assessments = [];

  @observable searchDatabaseText = '';

  @observable filterModel = new FilterModel();

  @action async getAssessmentSummary(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    status = '',
    area = '',
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.assessmentCosoServices.getAssessmentCosoSummaryList(
        token,
        page,
        searchValue,
        lvl2,
        bu,
        departmentNo,
        divisionNo,
        status,
        area,
      );
      this.assessments = result.data.assessments.map(
        (assessment) => new AssessmentLv1Model(assessment),
      );

      this.page = result.data.page;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }
}

export default AssessmentSummaryCOSOModel;
