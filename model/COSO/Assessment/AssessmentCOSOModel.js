import { observable, action, computed } from 'mobx';
import request from '../../../services';
import loadingStore from '../../../contexts/LoadingStore';
import SubmitRoadmapsModel from '../../SubmitRoadmapsModel';
import AcceptAssessmentCOSOModel, {
  ACCEPT_ASSESSMENT_STATUS,
} from './AcceptAssessmentCOSOModel';
import FilterModel from '../../FilterModel';

class AssessmentLv2Model {
  @observable area = '';

  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable order = '';

  constructor(database) {
    this.area = database.area;
    this.no = database.no;
    this.lvl = database.lvl;
    this.name = database.name;
  }
}

class AssessmentLv1Model {
  @observable no = '';

  @observable lvl = '';

  @observable name = '';

  @observable lastUpdate = '';

  @observable depDiv = '';

  @observable divEn = '';

  @observable shiftEn = '';

  @observable owner = '';

  @observable order = '';

  @observable readyTosubmit = '';

  @observable remark = '';

  @observable status = '';

  @observable id = '';

  @observable children = [];

  @observable summaryStatus = [];

  @observable roadmap = '';

  @observable lastYearPath = '';

  @observable hasLastYear = '';

  @observable submitAssessment = new SubmitRoadmapsModel();

  @observable acceptAssessment = new AcceptAssessmentCOSOModel();


  @observable isReadyToSubmit = false;
  
  constructor(assessment) {
    this.no = assessment.no;
    this.lvl = assessment.lvl;
    this.name = assessment.name;
    this.lastUpdate = assessment.lastUpdate;
    this.remark = assessment.remark;
    this.status = assessment.status;
    this.id = assessment.id;
    this.depDiv = assessment.depDiv;
    this.divEn = assessment.divEn;
    this.shiftEn = assessment.shiftEn;
    this.owner = assessment.owner;
    this.readyTosubmit = assessment.readyTosubmit;
    this.isReadyToSubmit = assessment.isReadyToSubmit;
    this.summaryStatus = assessment.summaryStatus;
    this.roadmap = assessment.roadMapType;
    this.lastYearPath = assessment.lastYearPath;
    this.hasLastYear = assessment.hasLastYear;
    this.children =
      assessment.children &&
      assessment.children.map((child) => new AssessmentLv2Model(child));
  }

  // logic การเก็บ object accept/reject Assessment
  @computed get getAcceptAssessmentList() {
    const acceptRoadmapList = [];
    if (this.acceptAssessment.status) {
      acceptRoadmapList.push(this.acceptAssessment);
    }
    return acceptRoadmapList;
  }
}

class AssessmentCOSOModel {
  @observable assessments = [];

  @observable assessmentsSummary = [];

  @observable totalPage = 1;

  @observable totalPageSummary = 1;

  @observable tab = 'TODO'; // TODO | SUMMARY

  @observable searchAssessmentText = '';

  @observable filterModel = new FilterModel();

  @observable departmentId = null;
  
  @action async getAssessmentCOSOTodoList(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = new Date().getFullYear(),
    owner = '',
  ) {
    loadingStore.setIsLoading(true);
    try {
      const result = await request.assessmentCosoServices.getAssessmentCosoList(
        token,
        page,
        searchValue,
        lvl2,
        roadmapType,
        bu,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        area,
        year,
        owner,
      );

      this.assessments = result.data.assessments.map(
        (assessment) => new AssessmentLv1Model(assessment),
      );

      this.page = result.data.page;
      this.totalPage = result.data.totalPage;
      this.totalContent = result.data.totalContent;
      this.departmentId = result.data.depaertmentId;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async getAssessmentSummary(
    token,
    page = 1,
    searchValue = '',
    lvl2 = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    area = '',
    year = new Date().getFullYear(),
    owner = '',
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.assessmentCosoServices.getAssessmentCosoSummaryList(
        token,
        page,
        searchValue,
        lvl2,
        roadmapType,
        bu,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        area,
        year,
        owner,
      );
      this.assessmentsSummary = result.data.assessments.map(
        (assessment) => new AssessmentLv1Model(assessment),
      );
      this.page = result.data.page;
      this.pageSummary = result.data.page;
      this.totalPageSummary = result.data.totalPage;
      this.totalContentSummary = result.data.totalContent;

      loadingStore.setIsLoading(false);
    } catch (e) {
      console.log(e);
      loadingStore.setIsLoading(false);
    }
  }

  @action async submitCOSOExportSummary(
    token,
    searchValue,
    year = '',
    questionNo = '',
    component = '',
    roadmapType = '',
    bu = '',
    departmentNo = '',
    divisionNo = '',
    shiftNo = '',
    status = '',
    ownerTask = '',
  ) {
    loadingStore.setIsLoading(true);

    try {
      const result = await request.assessmentCosoServices.exportCOSOSummary(
        token,
        searchValue,
        year,
        questionNo,
        component,
        roadmapType,
        bu,
        departmentNo,
        divisionNo,
        shiftNo,
        status,
        ownerTask,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      console.log(e);
    }
  }

  @computed get getRoadmapIds() {
    const roadmapIdsData = [];
    this.assessments.forEach((assessment) => {
      if (assessment.submitAssessment.roadmapIds !== null) {
        roadmapIdsData.push(
          assessment !== null && assessment.submitAssessment.roadmapIds,
        );
      }
    });
    return roadmapIdsData;
  }

  // Staff ส่งให้ DM
  // DM ส่งให้ VP ใช้ service เดียวกัน
  @action async sendCosoAssessmentToDmOrVp(token) {
    try {
      loadingStore.setIsLoading(true);

      const result = await request.assessmentCosoServices.submitAssessment(
        token,
        this.getRoadmapIds,
      );

      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  // ใช้ในการโยน roadmap จาก DM ไปให้ staff
  @action async submitAcceptCosoRoadmaps(token) {
    loadingStore.setIsLoading(true);

    try {
      const acceptAssessmentList = this.assessments.reduce(
        (acceptList, assessment) => [
          ...acceptList,
          ...assessment.getAcceptAssessmentList,
        ],
        [],
      );
      const result = await request.roadmapCosoServices.acceptRoadmaps(
        token,
        acceptAssessmentList,
      );
      loadingStore.setIsLoading(false);
      return result;
    } catch (e) {
      loadingStore.setIsLoading(false);
      console.log(e);
    }
  }

  // checkedAll on click Admin
  @action setCheckAll() {
    if (!this.isChckedAll) {
      this.assessments.forEach((roadmapModel) => {
        if (!roadmapModel.submitAssessment.roadmapIds) {
          roadmapModel.submitAssessment.setRoadmapIds(roadmapModel.id);
        }
      });
    } else {
      this.assessments.forEach((roadmapModel) => {
        roadmapModel.submitAssessment.setRoadmapIds(null);
      });
    }
  }

  // checkedAll Accept on click DM
  @action setAllAcceptChecked() {
    if (!this.isAllAcceptChecked) {
      this.assessments.forEach((assessmentsModel) => {
        if (
          assessmentsModel.acceptAssessment.status !==
          ACCEPT_ASSESSMENT_STATUS.accept
        ) {
          assessmentsModel.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.accept,
            assessmentsModel.id,
          );
        }
      });
    } else {
      this.assessments.forEach((assessmentsModel) => {
        assessmentsModel.acceptAssessment.setNo('', '');
      });
    }
  }

  // checkedAll Reject on click DM
  @action setAllRejectChecked() {
    if (!this.isAllRejectChecked) {
      this.assessments.forEach((assessmentsModel) => {
        if (
          assessmentsModel.acceptAssessment.status !==
          ACCEPT_ASSESSMENT_STATUS.reject
        ) {
          assessmentsModel.acceptAssessment.setNo(
            ACCEPT_ASSESSMENT_STATUS.reject,
            assessmentsModel.id,
          );
        }
      });
    } else {
      this.assessments.forEach((assessmentsModel) => {
        assessmentsModel.acceptAssessment.setNo('', '');
      });
    }
  }

  @action setCheckAllAssessmentList() {
    if (!this.isAllAssessmentChecked) {
      this.assessments.forEach((assessment) => {
        assessment.submitAssessment.setRoadmapIds(assessment.roadmapIds);
      });
    } else {
      this.assessments.forEach((assessment) => {
        assessment.submitAssessment.setRoadmapIds(null);
      });
    }
  }

  @action setSearchDatabaseText(text) {
    this.searchDatabaseText = text;
  }

  @computed get isAllAssessmentChecked() {
    const isChecked = this.assessments.every(
      (assessment) => assessment.submitAssessment.roadmapIds,
    );

    return isChecked;
  }

  @computed get isChckedAll() {
    const isChecked = this.assessments.every(
      (assessment) => assessment.submitAssessment.roadmapIds,
    );

    return isChecked;
  }

  // check btn submit Dm to vp
  @computed get isSubmitToDmDisabled() {
    return this.assessments.every(
      (assessment) => !assessment.submitAssessment.roadmapIds,
    );
  }

  // checkedAll Accept on click DM
  @computed get isAllAcceptChecked() {
    const isChecked = this.assessments.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.accept,
    );
    return isChecked;
  }

  // checkedAll Reject on click DM
  @computed get isAllRejectChecked() {
    const isChecked = this.assessments.every(
      (child) =>
        child.acceptAssessment.status === ACCEPT_ASSESSMENT_STATUS.reject,
    );
    return isChecked;
  }

  // check btn submit admin to admin
  @computed get isCheckReadyDisabled() {
    const isCheckAssessor = this.assessments.every(
      (roadmapModel) => !roadmapModel.readyTosubmit,
    );
    return isCheckAssessor;
  }

  // check btn submit accept dm
  @computed get isAcceptOrRejectDisabled() {
    // check if user check accept/reject or not
    const isAccept = this.assessments.every(
      (roadmapModel) =>
        roadmapModel.acceptAssessment.status !==
        ACCEPT_ASSESSMENT_STATUS.accept,
    );

    const isReject = this.assessments.every(
      (roadmapModel) =>
        roadmapModel.acceptAssessment.status !==
        ACCEPT_ASSESSMENT_STATUS.reject,
    );

    // check if reject roadmap set comment or not
    const iRoadmapCheckKey = this.assessments
      .filter(
        (roadmapModel) =>
          roadmapModel.acceptAssessment.status ===
          ACCEPT_ASSESSMENT_STATUS.reject,
      )
      .flat()
      .every((child) => child.acceptAssessment.rejectComment);

    if (isReject && isAccept) {
      return true;
    }

    if (!iRoadmapCheckKey) {
      return true;
    }

    return false;
  }

  //  key search
  @action setSearchAssessmentText(text) {
    this.searchAssessmentText = text;
  }

  @action setField(field, value) {
    this[field] = value;
  }
}

export default AssessmentCOSOModel;
