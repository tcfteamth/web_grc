import { observable, action, computed } from 'mobx';

class SubmitReAssessmentModel {
  @observable assessmentIds = null;

  @computed get isSelected() {
    return this.assessmentIds !== null;
  }

 

  @action setAssessmentIds(id) {
    if (this.assessmentIds === id) {
      this.assessmentIds = null;
    } else {
      this.assessmentIds = id;
    }

  }
}

export default SubmitReAssessmentModel;
