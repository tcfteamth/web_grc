import { observable, action, computed, runInAction } from 'mobx';
import request from '../services';

class DropdownModel {
  @observable options = [];

  @observable isLoaded = false;

  @action async getAssesor(token) {
    try {
      if (!this.isLoaded) {
        this.isLoaded = true;
        const result = await request.roadmapServices.getAssesor(token);

        runInAction(() => {
          this.options = result.data.listUser.map((assesor) => ({
            value: assesor.id,
            text: `${assesor.firstName} ${String(assesor.lastName).charAt(0)}.`,
          }));
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  @action async getPartners(token, searchText) {
    try {
      const result = await request.assessmentServices.getPartnerList(
        token,
        searchText,
      );

      this.options = result.data.map((partner) => ({
        value: partner.divisionNo,
        label: partner.indicator,
      }));

      this.options.unshift({ value: '', label: 'No Co-Responsible Division' });
    } catch (e) {
      console.log(e);
    }
  }

  @action async getBUList(token) {
    try {
      const result = await request.roadmapServices.getBUList(token);
      this.options = result.data.map((buItem) => ({
        value: buItem.bu,
        label: buItem.buFull,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getBUOldList(token,year) {
    try {
      const result = await request.roadmapServices.getBUOldList(token,year);
      this.options = result.data.map((buItem) => ({
        value: buItem.bu,
        label: buItem.buFull,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }


  @action async getRoadmapTypeList(token) {
    try {
      const result = await request.roadmapServices.getRoadmapTypeList(token);

      this.options = result.data.map((type) => ({
        value: type,
        label: type,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getOwnerTaskList(token) {
    try {
      const result = await request.roadmapServices.getOwnerTaskList(token);

      this.options = result.data.map((owner) => ({
        value: owner,
        label: owner,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRateList(token) {
    try {
      const result = await request.roadmapServices.getRateList(token);

      this.options = result.data.map((rate) => ({
        value: rate.id,
        label: rate.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoadmapLvl2List() {
    try {
      const result = await request.roadmapServices.getLvlTwoList();

      this.options = result.data.map((lvl2) => ({
        value: lvl2.no,
        label: `${lvl2.no} ${lvl2.name}`,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoadmapLvl3List() {
    try {
      const result = await request.roadmapServices.getLvlThreeList();
      this.options = result.data.map((lvl3) => ({
        value: lvl3.no,
        label: `${lvl3.no} ${lvl3.name}`,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoadmapLvl4List(lvl3No) {
    try {
      const result = await request.roadmapServices.getLvlFourList(lvl3No);
      console.log('result', result);
      this.options = result.data.map((lvl4) => ({
        value: lvl4.no,
        label: `${lvl4.no} ${lvl4.name}`,
        id: lvl4.id,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDepartmentList(bu) {
    try {
      const result = await request.roadmapServices.getDepartmentList(bu);

      this.options = result.data.map((department) => ({
        value: department.id,
        label: department.departmentNameEn,
        no: department.departmentNo,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDepartmentOldList(bu,year) {
    try {
      const result = await request.roadmapServices.getDepartmentOldList(bu,year);

      this.options = result.data.map((department) => ({
        value: department.id,
        label: department.departmentNameEn,
        no: department.departmentNo,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDivisionList(departmentId) {
    try {
      const result = await request.roadmapServices.getDivisionById(
        departmentId,
      );

      this.options = result.data.map((division) => ({
        value: division.no,
        label: division.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDivisionOldList(departmentNo,year) {
    try {
      const result = await request.roadmapServices.getDivisionOldByNoAndYear(
        departmentNo,year
      );

      this.options = result.data.map((division) => ({
        value: division.no,
        label: division.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getShiftList(departmentId) {
    try {
      const result = await request.roadmapServices.getShiftByDepartmentId(
        departmentId,
      );

      this.options = result.data.map((shift) => ({
        value: shift.no,
        label: shift.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getShiftOldList(departmentNo,year) {
    try {
      const result = await request.roadmapServices.getShiftOldByDepartmentNo(
        departmentNo,year
      );

      this.options = result.data.map((shift) => ({
        value: shift.no,
        label: shift.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getAllStatus(token, tab, type) {
    try {
      const result = await request.roadmapServices.getAllStatus(
        token,
        tab,
        type,
      );

      this.options = result.data.map((status) => ({
        value: status,
        label: status,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getDivisionWithoutId(token) {
    try {

      const result = await request.roadmapServices.getDivisionWithoutId(token);

      this.options = result.data.map((division) => ({
        value: division.no,
        label: division.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getShiftWithoutId(token) {
    try {

      const result = await request.roadmapServices.getShiftWithoutId(token);

      this.options = result.data.map((shift) => ({
        value: shift.no,
        label: shift.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getYearList(token) {
    try {
      const result = await request.roadmapServices.getYearList(token);

      const reverseYealResult = result.data.reverse();

      this.options = reverseYealResult.map((year) => ({
        value: year,
        label: year,
      }));

      // this.options.unshift({ value: '', label: 'All' });

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getYearListFillter(token) {
    try {
      const result = await request.roadmapServices.getYearList(token);

      this.options = result.data
        .map((year) => ({
          value: year,
          label: year,
        }))
        .reverse();

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getManageFilterBUList(token) {
    try {
      const result = await request.dashboardServices.getManageFilterBUList(
        token,
      );

      this.options = result.data.map((bu) => ({
        value: bu.bu,
        label: bu.buFull,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getAreaListDropdownList(token) {
    try {
      const result = await request.dashboardServices.getAreaList(token);

      this.options = result.data.map((area) => ({
        value: area,
        label: area,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getManageFilterDepartmentList(token, bu) {
    try {
      const result = await request.dashboardServices.getManageFilterDepartmentList(
        token,
        bu,
      );

      this.options = result.data.map((department) => ({
        value: department.id,
        label: department.departmentNameEn,
        no: department.departmentNo,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getManageFilterDivisionList(bu, departmentId) {
    try {
      const result = await request.dashboardServices.getManageFilterDivisionList(
        bu,
        departmentId,
      );

      this.options = result.data.map((division) => ({
        value: division.no,
        label: division.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getManageFilterShiftList(bu, departmentId) {
    try {
      const result = await request.dashboardServices.getManageFilterShiftList(
        bu,
        departmentId,
      );

      this.options = result.data.map((shift) => ({
        value: shift.no,
        label: shift.nameEn,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }
  @action async getFollowUpDetailStatus(token, follwUpId) {
    try {
      const result = await request.followUpServices.getFollowUpDetailStatus(
        token,
        follwUpId,
      );

      this.options = result.data.map((status) => ({
        value: status,
        label: status,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getFollowUpYearList() {
    try {
      const result = await request.followUpServices.getFollowUpYearList();

      this.options = result.data.map((year) => ({
        value: year,
        label: year,
      }));
      // this.options.unshift({ value: '2019', label: '2019' });

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getFollowUpStatusList(tab) {
    try {
      const result = await request.followUpServices.getFollowUpStatusList(tab);

      this.options = result.data.map((status) => ({
        value: status,
        label: status,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getUserRoleList(token, roleId) {
    try {
      if (roleId === undefined) {
        roleId = null;
      }

      const result = await request.clientServices.getUser(roleId);
      this.options = result.data.map((user) => ({
        value: user.name,
        label: user.name,
        id: user.empId,
        empId: user.empId,
        name: user.name,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoleTypeList(type) {
    try {
      const result = await request.clientServices.getRoleType(type);
      this.options = result.data.map((type) => ({
        value: type,
        label: type,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoleSpacialList() {
    try {
      const result = await request.clientServices.getRoleSpacial();
      this.options = result.data.map((list) => ({
        value: list.value,
        label: list.name,
        status: false,
      }));
      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getPositionList() {
    try {
      const result = await request.clientServices.getPositionList();
      this.options = result.data.map((list) => ({
        value: list,
        label: list,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRoleSystemList() {
    try {
      const result = await request.clientServices.getRoleSystemList();
      this.options = result.data.map((list) => ({
        value: list,
        label: list,
      }));

      return this.options;
    } catch (e) {
      console.log(e);
    }
  }

  @action async getRCMappingList(page, size, filterNo, search, isStandard) {
    try {
      const result = await request.assessmentServices.getRiskAndControlListMapping(
        page,
        size,
        filterNo,
        search,
        isStandard,
      );

      // lv4 = 1.2.3.4
      // lv6 = 1.2.3.4.5.6
      // ดักจาก length ของ filterNo ที่เข้ามาว่าเป็น lv ไหน
      if (filterNo.split('.').length <= 4) {
        this.options = result.data.contents.map((rc) => ({
          value: rc.no,
          label: `${rc.no} ${rc.name}`,
        }));
      } else {
        this.options = result.data.contents.map((lv4) =>
          lv4.children
            .map((lv5) =>
              lv5.children
                .map((lv6) => ({
                  label: `${lv6.fullPath} ${lv6.name}`,
                  value: lv6.fullPath,
                }))
                .flat(),
            )
            .flat(),
        );
      }

      return this.options;
    } catch (error) {
      console.log(error);
    }
  }

  @action async getFollowUpReportStatusList() {
    try {
      const result = await request.followUpReportServices.getFollowUpReportStatusList();

      this.options = result.data.map((status) => ({
        value: status,
        label: status,
      }));

      return this.options;
    } catch (error) {
      console.log(error);
    }
  }

  @action getIAFindingTypelist() {
    try {
      const options = [
        { value: 'IA_FINDING', label: 'IA Finding' },
        { value: 'IA_FEEDBACK', label: 'IA Feedback' },
      ];

      return options;
    } catch (error) {
      console.log(error);
    }
  }

  @action async getTagList() {
    try {
      const result = await request.processDBServices.getTagList();

      this.options = result.data.content.map((content) => ({
        value: content.id,
        label: content.name,
      }));

      return this.options;
    } catch (error) {
      console.log(error);
    }
  }

  @action async getDepartmentFollowUpVP(token) {
    try {
      const result = await request.followUpServices.getDepartmentFollowUpVP(
        token,
      );

      this.options = result.data.map((department) => ({
        value: department.id,
        label: department.departmentNameEn,
        no: department.departmentNo,
      }));

      return this.options;
    } catch (error) {
      console.log(error);
    }
  }

  @computed get getFirstOption() {
    return this.options.length >= 0 ? this.options[0] : null;
  }
}

export const assesorDropdownStore = new DropdownModel();
export const partnerlistDropdownStore = new DropdownModel();
export const bulistDropdownStore = new DropdownModel();
export const roadmapTypeListDropdownStore = new DropdownModel();
export const ownerTaskListDropdownStore = new DropdownModel();
export const rateListDropdownStore = new DropdownModel();
export const areaListDropdownStore = new DropdownModel();
export const lvl2ListDropdownStore = new DropdownModel();
export const lvl3ListDropdownStore = new DropdownModel();
export const lvl4ListDropdownStore = new DropdownModel();
export const departmentListDropdownStore = new DropdownModel();
export const divisionListDropdownStore = new DropdownModel();
export const shiftListDropdownStore = new DropdownModel();
export const statusListDropdownStore = new DropdownModel();
export const yearListDropdownStore = new DropdownModel();
export const manageDepartmentDropdownStore = new DropdownModel();
export const manageBUDropdownStore = new DropdownModel();
export const followUpDetailStatusStore = new DropdownModel();
export const followUpYearListStore = new DropdownModel();
export const followUpStatusListStore = new DropdownModel();
export const userRoleListStore = new DropdownModel();
export const userTypeListStore = new DropdownModel();
export const roleSpacialStore = new DropdownModel();
export const positionListStore = new DropdownModel();
export const systemListStore = new DropdownModel();
export const rcMappingList = new DropdownModel();
export const followUpReportStatusListStore = new DropdownModel();
export const iaFindingTypeList = new DropdownModel();
export const taglistDropdownStore = new DropdownModel();
export const departmentFollowUpVp = new DropdownModel();
